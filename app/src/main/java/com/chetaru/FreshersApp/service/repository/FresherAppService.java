package com.chetaru.FreshersApp.service.repository;

import com.chetaru.FreshersApp.service.model.PostRequest;
import com.chetaru.FreshersApp.service.model.User;
import com.chetaru.FreshersApp.service.model.UserAddPostResponse;
import com.chetaru.FreshersApp.service.model.UserRequest;
import com.chetaru.FreshersApp.service.model.UserResponse;
import com.chetaru.FreshersApp.viewModel.CountryResponse;
import com.google.gson.JsonObject;

import java.util.HashMap;
import java.util.Map;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.HeaderMap;
import retrofit2.http.Headers;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface FresherAppService {




    @POST("userLogin")
    Call<UserResponse> getLogin(@Body UserRequest request);
    @POST("getCountryList")
    Call<CountryResponse> getCountry();
    @POST("userLogout")
    Call<UserResponse> getLogOut();


    /*
    {
  "name":"shweta",
  "userName":"shweta@mailinator.com",
  "password":"123",
  "dob":"1995-07-22",
  "bio":"test",
  "gender":2,
  "type":1,
  "deviceId":"7C4A9817-775F-43EC-BC84-0BDBF3F6A3E7",
  "fcmToken":"fcg7s6u0x9s:APA91bFy57Q6Yyn-fUSUoY1297oxkFqxiFsdvulcBCT5nj5kiuOiCTP-lyB7-XbdYvEQ_op-mybiKKGNnFaqxD1WjB9jbecU6pYDHJV8xGLtDQBc-iqJlejYUr_4OeusRa9BJ6QHo42M"
}
@HeaderMap Map<String, String> headers,
    * */
    @POST("usersignup")
    Call<ResponseBody> getSignUp(@Body UserRequest request);

    @POST("verifyUserName")
    Call<ResponseBody> getVerifyUser(@Body UserRequest jsonObject);
    @POST("verifyUserName")
    Call<UserResponse> getVerify(@Body UserRequest jsonObject);
    @POST("forgotPassword")
    Call<UserResponse> getForgetMail(@Body UserRequest request);
    @POST("resetPasswordForPhone")
    Call<UserResponse> getPassReset( @Body UserRequest request);

    @POST("userLogout")
    Call<ResponseBody> getLogout(@HeaderMap Map<String, String> headers);

    @POST("getDataForPost")
    Call<UserAddPostResponse> getDataForPost(@HeaderMap Map<String, String> headers);
    @POST("userAddPost")
    Call<ResponseBody> getPost(@Body PostRequest request);

}
