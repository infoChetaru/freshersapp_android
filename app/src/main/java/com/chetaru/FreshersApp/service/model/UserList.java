package com.chetaru.FreshersApp.service.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class UserList implements Serializable {

    //{"id":32,"name":"Ritesh email","tagUserConnectionStatus":2,"connectionId":23}
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("imageUrl")
    @Expose
    private String imageUrl;
    @SerializedName("tagUserConnectionStatus")
    @Expose
    private Integer tagUserConnectionStatus;
    @SerializedName("connectionId")
    @Expose
    private Integer connectionId;

    private boolean isSelected=false;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public boolean getSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }


    public boolean isSelected() {
        return isSelected;
    }

    public Integer getTagUserConnectionStatus() {
        return tagUserConnectionStatus;
    }

    public void setTagUserConnectionStatus(Integer tagUserConnectionStatus) {
        this.tagUserConnectionStatus = tagUserConnectionStatus;
    }

    public Integer getConnectionId() {
        return connectionId;
    }

    public void setConnectionId(Integer connectionId) {
        this.connectionId = connectionId;
    }
}
