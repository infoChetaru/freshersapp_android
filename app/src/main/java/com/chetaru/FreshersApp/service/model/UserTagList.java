package com.chetaru.FreshersApp.service.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class UserTagList {
    @SerializedName("userList")
    @Expose
    private List<UserList> userList = null;
    @SerializedName("hashTagsList")
    @Expose
    private List<TagList> tagsList = null;

    public List<UserList> getUserList() {
        return userList;
    }

    public void setUserList(List<UserList> userList) {
        this.userList = userList;
    }

    public List<TagList> getTagsList() {
        return tagsList;
    }

    public void setTagsList(List<TagList> tagsList) {
        this.tagsList = tagsList;
    }
}
