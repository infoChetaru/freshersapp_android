package com.chetaru.FreshersApp.service.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class UserViewPost implements Serializable {
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("userId")
    @Expose
    private Integer userId;
    @SerializedName("userImage")
    @Expose
    private String userImage;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("caption")
    @Expose
    private String caption;
    @SerializedName("imageUrl")
    @Expose
    private String imageUrl;
    @SerializedName("userTagList")
    @Expose
    private List<UserList> userTagList = null;
    @SerializedName("hashTagList")
    @Expose
    private List<TagList> hashTagList = null;
    @SerializedName("likedStatus")
    @Expose
    private Boolean likedStatus;
    @SerializedName("roleId")
    @Expose
    private Integer roleId;
    @SerializedName("postedDate")
    @Expose
    private String postedDate;
    @SerializedName("likedCount")
    @Expose
    private Integer likedCount;
    @SerializedName("sensitivePostStatus")
    @Expose
    private Integer sensitivePostStatus;
    @SerializedName("originalPostId")
    @Expose
    private Integer originalPostId;

    @SerializedName("userConnectionStatus")
    @Expose
    private Integer userConnectionStatus;
    @SerializedName("connectionId")
    @Expose
    private Integer connectionId;

    /*@SerializedName("replyPostId")
    @Expose
    private Integer replyPostId;*/

    @SerializedName("created_at")
    @Expose
    private String created_at;
    @SerializedName("postExpired")
    @Expose
    private Boolean postExpired;





    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getUserImage() {
        return userImage;
    }

    public void setUserImage(String userImage) {
        this.userImage = userImage;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCaption() {
        return caption;
    }

    public void setCaption(String caption) {
        this.caption = caption;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public List<UserList> getUserTagList() {
        return userTagList;
    }

    public void setUserTagList(List<UserList> userTagList) {
        this.userTagList = userTagList;
    }

    public List<TagList> getHashTagList() {
        return hashTagList;
    }

    public void setHashTagList(List<TagList> hashTagList) {
        this.hashTagList = hashTagList;
    }

    public Boolean getLikedStatus() {
        return likedStatus;
    }

    public void setLikedStatus(Boolean likedStatus) {
        this.likedStatus = likedStatus;
    }

    public Integer getRoleId() {
        return roleId;
    }

    public void setRoleId(Integer roleId) {
        this.roleId = roleId;
    }

    public String getPostedDate() {
        return postedDate;
    }

    public void setPostedDate(String postedDate) {
        this.postedDate = postedDate;
    }

    public Integer getLikedCount() {
        return likedCount;
    }

    public void setLikedCount(Integer likedCount) {
        this.likedCount = likedCount;
    }

    public Integer getSensitivePostStatus() {
        return sensitivePostStatus;
    }

    public void setSensitivePostStatus(Integer sensitivePostStatus) {
        this.sensitivePostStatus = sensitivePostStatus;
    }

    public Integer getOriginalPostId() {
        return originalPostId;
    }

    public void setOriginalPostId(Integer originalPostId) {
        this.originalPostId = originalPostId;
    }

    public Integer getUserConnectionStatus() {
        return userConnectionStatus;
    }

    public void setUserConnectionStatus(Integer userConnectionStatus) {
        this.userConnectionStatus = userConnectionStatus;
    }

    public Integer getConnectionId() {
        return connectionId;
    }

    public void setConnectionId(Integer connectionId) {
        this.connectionId = connectionId;
    }

   /* public Integer getReplyPostId() {
        return replyPostId;
    }

    public void setReplyPostId(Integer replyPostId) {
        this.replyPostId = replyPostId;
    }*/

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public Boolean getPostExpired() {
        return postExpired;
    }

    public void setPostExpired(Boolean postExpired) {
        this.postExpired = postExpired;
    }
}
