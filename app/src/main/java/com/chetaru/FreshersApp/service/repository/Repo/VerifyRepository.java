package com.chetaru.FreshersApp.service.repository.Repo;

import android.util.Log;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.chetaru.FreshersApp.service.model.UserRequest;
import com.chetaru.FreshersApp.service.model.UserResponse;
import com.chetaru.FreshersApp.service.repository.FresherAppService;
import com.chetaru.FreshersApp.service.retrofit.ApiClient;
import com.chetaru.FreshersApp.utility.Validations;
import com.chetaru.FreshersApp.viewModel.CountryResponse;
import com.google.gson.Gson;

import org.json.JSONObject;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class VerifyRepository {
    private FresherAppService apiService;
    private static VerifyRepository verifyRepository;

    private VerifyRepository() {
        //apiService = ApiClient.getClient().create(FresherAppService.class);
        apiService = ApiClient.getInstance();
        /*I would also change this slightly. Instead of observing fetch,
        I would observe the LiveData directly.
        * */
        //user = LoginRepository.getInstance().getUser();

        /*
        Later, you can request updated data from the server at any point.
        * */
        //LoginRepository.getInstance().fetchUser2();
        /*
        You could also call fetchUser2() on the first construction of UserRepository.
         Then only updates would call fetchUser2() directly.
        * */
        //fetchUser2();
    }
    public synchronized static VerifyRepository getInstance() {
        if (verifyRepository == null) verifyRepository = new VerifyRepository();
        return verifyRepository;
    }
    public LiveData<CountryResponse> fetchCountry() {
        final MutableLiveData<CountryResponse> data = new MutableLiveData<>();
        Call<CountryResponse> call = apiService.getCountry();
        call.enqueue(new Callback<CountryResponse>() {
            @Override
            public void onResponse(@NonNull Call<CountryResponse> call, @NonNull Response<CountryResponse> response) {
                if (response.body() != null) {
                    try {
                        data.postValue(response.body());
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                    Log.d("countrydata","countryName:- "+response.body().getCountryList().toString());
                }
            }

            @Override
            public void onFailure(@NonNull Call<CountryResponse> call, @NonNull Throwable t) {
                data.postValue(null);
                t.printStackTrace();
            }
        });
        return data;
    }
    public LiveData<JSONObject> fetchVeriCode(Integer type,String value, String countryId) {
        final MutableLiveData<JSONObject> data = new MutableLiveData<>();
        UserRequest request=new UserRequest();

        request.setType(type);
        request.setValue(value);
        if (!Validations.isEmptyString(countryId))
        request.setCountryId(countryId);

        Call<ResponseBody> call = apiService.getVerifyUser(request);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    JSONObject json = new JSONObject(response.body().string());

                    //Gson gson = new Gson(); // Or use new GsonBuilder().create();
                    //String jsonData = gson.toJson(json); // serializes target to Json
                    //UserResponse responseData = gson.fromJson(response.body().toString(), UserResponse.class);
                    data.setValue(json);
                    //{"nameValuePairs":{"code":200,"status":true,"service_name":"verify-username","message":"Verification code send to email.","data":{"nameValuePairs":{"randomNumber":6892,"email":"ritesh.sonar111@gmail.com"}}}}
                    //repoCall = null;
                }catch (Exception e){
                    e.printStackTrace();
                    data.setValue(null);
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.e(getClass().getSimpleName(), "Error loading repos", t);
                data.setValue(null);
            }
        });
        return data;
    }

}
