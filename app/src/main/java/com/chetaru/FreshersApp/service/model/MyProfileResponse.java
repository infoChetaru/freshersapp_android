package com.chetaru.FreshersApp.service.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.math.BigInteger;

public class MyProfileResponse implements Serializable {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("userName")
    @Expose
    private String userName;
    @SerializedName("dob")
    @Expose
    private String dob;
    @SerializedName("bio")
    @Expose
    private String bio;
    @SerializedName("gender")
    @Expose
    private Integer gender;
    @SerializedName("latitude")
    @Expose
    private String latitude;
    @SerializedName("longitude")
    @Expose
    private String longitude;
    @SerializedName("localeId")
    @Expose
    private Integer localeId;
    @SerializedName("localeName")
    @Expose
    private String localeName;
    @SerializedName("profileImage")
    @Expose
    private String profileImage;
    @SerializedName("postId")
    @Expose
    private String postId;
    @SerializedName("postImageUrl")
    @Expose
    private String postImageUrl;
    @SerializedName("likedUserCount")
    @Expose
    private Integer likedUserCount;
    @SerializedName("expiryDate")
    @Expose
    private String expiryDate;
    @SerializedName("purchaseDate")
    @Expose
    private String purchaseDate;
    @SerializedName("premieUser")
    @Expose
    private Boolean premieUser;
    @SerializedName("connectionStatus")
    @Expose
    private Integer connectionStatus;
    @SerializedName("sensitivePostsAllowed")
    @Expose
    private Integer sensitivePostsAllowed;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getBio() {
        return bio;
    }

    public void setBio(String bio) {
        this.bio = bio;
    }

    public Integer getGender() {
        return gender;
    }

    public void setGender(Integer gender) {
        this.gender = gender;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public Integer getLocaleId() {
        return localeId;
    }

    public void setLocaleId(Integer localeId) {
        this.localeId = localeId;
    }

    public String getLocaleName() {
        return localeName;
    }

    public void setLocaleName(String localeName) {
        this.localeName = localeName;
    }

    public String getProfileImage() {
        return profileImage;
    }

    public void setProfileImage(String profileImage) {
        this.profileImage = profileImage;
    }

    public String getPostId() {
        return postId;
    }

    public void setPostId(String postId) {
        this.postId = postId;
    }

    public String getPostImageUrl() {
        return postImageUrl;
    }

    public void setPostImageUrl(String postImageUrl) {
        this.postImageUrl = postImageUrl;
    }

    public Integer getLikedUserCount() {
        return likedUserCount;
    }

    public void setLikedUserCount(Integer likedUserCount) {
        this.likedUserCount = likedUserCount;
    }

    public String getExpiryDate() {
        return expiryDate;
    }

    public void setExpiryDate(String expiryDate) {
        this.expiryDate = expiryDate;
    }

    public String getPurchaseDate() {
        return purchaseDate;
    }

    public void setPurchaseDate(String purchaseDate) {
        this.purchaseDate = purchaseDate;
    }

    public Boolean getPremieUser() {
        return premieUser;
    }

    public void setPremieUser(Boolean premieUser) {
        this.premieUser = premieUser;
    }

    public Integer getConnectionStatus() {
        return connectionStatus;
    }

    public void setConnectionStatus(Integer connectionStatus) {
        this.connectionStatus = connectionStatus;
    }

    public Integer getSensitivePostsAllowed() {
        return sensitivePostsAllowed;
    }

    public void setSensitivePostsAllowed(Integer sensitivePostsAllowed) {
        this.sensitivePostsAllowed = sensitivePostsAllowed;
    }
}
