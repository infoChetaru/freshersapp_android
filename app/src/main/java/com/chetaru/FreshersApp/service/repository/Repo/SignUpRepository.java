package com.chetaru.FreshersApp.service.repository.Repo;

import android.util.Log;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.chetaru.FreshersApp.service.model.UserRequest;
import com.chetaru.FreshersApp.service.model.UserResponse;
import com.chetaru.FreshersApp.service.repository.FresherAppService;
import com.chetaru.FreshersApp.service.retrofit.ApiClient;
import com.chetaru.FreshersApp.utility.Utility;

import org.json.JSONObject;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SignUpRepository {
    private FresherAppService apiService;
    private static SignUpRepository userRepository;


    private SignUpRepository() {
        //apiService = ApiClient.getClient().create(FresherAppService.class);
        apiService = ApiClient.getInstance();
    }

    public synchronized static SignUpRepository getInstance() {
        if (userRepository == null) userRepository = new SignUpRepository();
        return userRepository;
    }
    /*
       {
     "name":"shweta",
     "userName":"45343785372",
     "password":"123",
     "dob":"1995-07-22",
     "bio":"test",
     "gender":2,
     "type":2,
     "countryId":"54",
     "profileImage":"",
     "deviceId":"7C4A9817-775F-43EC-BC84-0BDBF3F6A3E7",
     "fcmToken":"fcg7s6u0x9s:APA91bFy57Q6Yyn-fUSUoY1297oxkFqxiFsdvulcBCT5nj5kiuOiCTP-lyB7-XbdYvEQ_op-mybiKKGNnFaqxD1WjB9jbecU6pYDHJV8xGLtDQBc-iqJlejYUr_4OeusRa9BJ6QHo42M"
   }
       * */
    public LiveData<JSONObject> signUpUser(String name,String userName, String password,
                                             String dob,String bio,Integer gender,Integer type,
                                             String countryId,String profileImage,String deviceId, String myToken) {
        final MutableLiveData<JSONObject> data = new MutableLiveData<>();
        UserRequest request=new UserRequest();

        request.setUserName(userName);
        request.setName(name);
        request.setPassword(password);
        request.setDob(dob);
        request.setBio(bio);
        request.setGender(gender);
        request.setType(type);
        request.setCountryId(countryId);
        request.setProfileImage(profileImage);
        request.setDeviceId(deviceId);
        request.setFcmToken(myToken);
        Log.d("request",request.toString());
        Call<ResponseBody> call = apiService.getSignUp(request);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(@NonNull Call<ResponseBody> call, @NonNull Response<ResponseBody> response) {
                if (response.body() != null) {
                   /* try {
                        data.postValue(response.body());

                    }catch (Exception e){
                        e.printStackTrace();
                    }*/
                    try {
                        JSONObject json = new JSONObject(response.body().string());

                        //Gson gson = new Gson(); // Or use new GsonBuilder().create();
                        //String jsonData = gson.toJson(json); // serializes target to Json
                        //UserResponse responseData = gson.fromJson(response.body().toString(), UserResponse.class);
                        data.setValue(json);
                        //repoCall = null;
                    }catch (Exception e){
                        e.printStackTrace();
                        data.setValue(null);
                    }

                }

            }

            @Override
            public void onFailure(@NonNull Call<ResponseBody> call, @NonNull Throwable t) {
                data.postValue(null);
                t.printStackTrace();
            }
        });
        return data;
    }

}
