package com.chetaru.FreshersApp.service.repository.Repo;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.chetaru.FreshersApp.service.model.UserAddPostResponse;
import com.chetaru.FreshersApp.service.repository.FresherAppService;
import com.chetaru.FreshersApp.service.retrofit.ApiClient;
import com.chetaru.FreshersApp.viewModel.CountryResponse;

import java.util.HashMap;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UploadRepository  {
    private FresherAppService apiService;
    private static UploadRepository uploadRepository;

    private UploadRepository() {
        //apiService = ApiClient.getClient().create(FresherAppService.class);
        apiService = ApiClient.getInstance();
    }
    public synchronized static UploadRepository getInstance() {
        if (uploadRepository == null) uploadRepository = new UploadRepository();
        return uploadRepository;
    }
    public LiveData<UserAddPostResponse> festUserPost(String tokenId) {
        final MutableLiveData<UserAddPostResponse> data = new MutableLiveData<>();
        Map<String, String> map = new HashMap<>();
        map.put("token", String.valueOf(tokenId));
        Call<UserAddPostResponse> call = apiService.getDataForPost(map);
        call.enqueue(new Callback<UserAddPostResponse>() {
            @Override
            public void onResponse(@NonNull Call<UserAddPostResponse> call, @NonNull Response<UserAddPostResponse> response) {
                if (response.body() != null) {
                    try {
                        data.postValue(response.body());
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<UserAddPostResponse> call, @NonNull Throwable t) {
                data.postValue(null);
                t.printStackTrace();
            }
        });
        return data;
    }
}
