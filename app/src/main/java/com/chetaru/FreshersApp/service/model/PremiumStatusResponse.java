package com.chetaru.FreshersApp.service.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PremiumStatusResponse {
    @SerializedName("expiryDate")
    @Expose
    private String expiryDate;
    @SerializedName("purchaseDate")
    @Expose
    private String purchaseDate;
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("primeUser")
    @Expose
    private Integer primeUser;

    public String getExpiryDate() {
        return expiryDate;
    }

    public void setExpiryDate(String expiryDate) {
        this.expiryDate = expiryDate;
    }

    public String getPurchaseDate() {
        return purchaseDate;
    }

    public void setPurchaseDate(String purchaseDate) {
        this.purchaseDate = purchaseDate;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getPrimeUser() {
        return primeUser;
    }

    public void setPrimeUser(Integer primeUser) {
        this.primeUser = primeUser;
    }

}
