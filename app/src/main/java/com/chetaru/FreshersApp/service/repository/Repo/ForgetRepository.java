package com.chetaru.FreshersApp.service.repository.Repo;

import android.util.Log;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.chetaru.FreshersApp.service.model.UserRequest;
import com.chetaru.FreshersApp.service.model.UserResponse;
import com.chetaru.FreshersApp.service.repository.FresherAppService;
import com.chetaru.FreshersApp.service.repository.LoginRepository;
import com.chetaru.FreshersApp.service.retrofit.ApiClient;

import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ForgetRepository {
    private FresherAppService apiService;
    private static ForgetRepository userRepository;

    private ForgetRepository() {
        //apiService = ApiClient.getClient().create(FresherAppService.class);
        apiService = ApiClient.getInstance();
    }

    public synchronized static ForgetRepository getInstance() {
        if (userRepository == null) userRepository = new ForgetRepository();
        return userRepository;
    }
    public LiveData<UserResponse> getMail(String userName) {
        final MutableLiveData<UserResponse> data = new MutableLiveData<>();
        UserRequest request=new UserRequest();

        request.setUserName(userName);
        Call<UserResponse> call = apiService.getForgetMail(request);
        call.enqueue(new Callback<UserResponse>() {
            @Override
            public void onResponse(@NonNull Call<UserResponse> call, @NonNull Response<UserResponse> response) {
                if (response.body() != null) {
                    //hideLoader();
                    try {
                        data.postValue(response.body());
                        Log.d("forgetPass",response.body().getMessage());
                    }catch (Exception e){
                        e.printStackTrace();
                        data.setValue(null);
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<UserResponse> call, @NonNull Throwable t) {
                //hideLoader();
                data.postValue(null);
                t.printStackTrace();
            }
        });
        return data;
    }
    public LiveData<UserResponse> getPasswordReset(String userName,String password,String token) {

        HashMap<String, String> headers = new HashMap<String, String>();
        headers.put("token","Bearer " + token);


        final MutableLiveData<UserResponse> data = new MutableLiveData<>();
        UserRequest request=new UserRequest();

        request.setUserName(userName);
        request.setPassword(password);
        request.setToken(token);
        Call<UserResponse> call = apiService.getPassReset(request);
        call.enqueue(new Callback<UserResponse>() {
            @Override
            public void onResponse(@NonNull Call<UserResponse> call, @NonNull Response<UserResponse> response) {
                if (response.body() != null) {
                    //hideLoader();
                    try {
                        data.postValue(response.body());
                        Log.d("forgetPass",response.body().getMessage());
                    }catch (Exception e){
                        e.printStackTrace();
                        data.setValue(null);
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<UserResponse> call, @NonNull Throwable t) {
                //hideLoader();
                data.postValue(null);
                t.printStackTrace();
            }
        });
        return data;
    }
}
