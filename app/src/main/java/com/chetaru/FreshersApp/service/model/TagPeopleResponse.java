package com.chetaru.FreshersApp.service.model;

import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.widget.ImageView;

public class TagPeopleResponse {

    private boolean isSelected=false;
    private String people;
    private Drawable peopleImage;

    public String getPeople() {
        return people;
    }

    public void setPeople(String people) {
        this.people = people;
    }

    public boolean getSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }


    public boolean isSelected() {
        return isSelected;
    }

    public Drawable getPeopleImage() {
        return peopleImage;
    }

    public void setPeopleImage(Drawable peopleImage) {
        this.peopleImage = peopleImage;
    }
}
