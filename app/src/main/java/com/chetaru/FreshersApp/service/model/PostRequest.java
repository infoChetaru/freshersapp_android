package com.chetaru.FreshersApp.service.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class PostRequest {
    @SerializedName("caption")
    @Expose
    private String caption;
    @SerializedName("tagUserId")
    @Expose
    private List<Integer> tagUserId = null;
    @SerializedName("existingHashTagId")
    @Expose
    private List<Integer> existingHashTagId = null;
    @SerializedName("newHashTags")
    @Expose
    private List<String> newHashTags = null;
    @SerializedName("postImage")
    @Expose
    private String postImage;
    @SerializedName("latitude")
    @Expose
    private String latitude;
    @SerializedName("longitude")
    @Expose
    private String longitude;

    public String getCaption() {
        return caption;
    }

    public void setCaption(String caption) {
        this.caption = caption;
    }

    public List<Integer> getTagUserId() {
        return tagUserId;
    }

    public void setTagUserId(List<Integer> tagUserId) {
        this.tagUserId = tagUserId;
    }

    public List<Integer> getExistingHashTagId() {
        return existingHashTagId;
    }

    public void setExistingHashTagId(List<Integer> existingHashTagId) {
        this.existingHashTagId = existingHashTagId;
    }

    public List<String> getNewHashTags() {
        return newHashTags;
    }

    public void setNewHashTags(List<String> newHashTags) {
        this.newHashTags = newHashTags;
    }

    public String getPostImage() {
        return postImage;
    }

    public void setPostImage(String postImage) {
        this.postImage = postImage;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

}
