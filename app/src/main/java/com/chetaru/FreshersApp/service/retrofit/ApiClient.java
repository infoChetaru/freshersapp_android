package com.chetaru.FreshersApp.service.retrofit;

import android.app.Dialog;
import android.content.Context;
import android.util.Base64;
import android.view.View;

import com.chetaru.FreshersApp.service.repository.FresherAppService;
import com.chetaru.FreshersApp.utility.SessionParam;
import com.google.gson.JsonObject;

import java.util.HashMap;
import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ApiClient {

//    private static final String AUTH = "Basic " + Base64.encodeToString((token).getBytes(), Base64.NO_WRAP);

   //public static final String BASE_URL = "https://fresherappdemo.chetaru.co.uk/api/"; //production
    public static final String BASE_URL = "https://freshersapp.chetaru.co.uk/api/";  //live URL
    //test blue data

    private static FresherAppService appService;
    private static Retrofit retrofit = null;
    private static OkHttpClient okHttpClient = null;



    public static FresherAppService getInstance(){
        if(appService != null ){
            return appService;
        }

        if (retrofit == null){
            getClient();
        }


        appService = retrofit.create(FresherAppService.class);
        return appService;
    }
    
    public static Retrofit getClient() {

        if (retrofit==null) {
            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .client(getRequestHeader())
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
            return retrofit;
    }




    public static Retrofit getCustomClient(String baseURL) {
        Retrofit retrofit_ = new Retrofit.Builder()
                .baseUrl(baseURL)
                .client(getRequestHeader())
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        return retrofit_;
    }



    private static OkHttpClient getRequestHeader() {
        if (null == okHttpClient) {
            okHttpClient = new OkHttpClient.Builder()
                    .readTimeout(60, TimeUnit.SECONDS)
                    .connectTimeout(60, TimeUnit.SECONDS)
                    .build();

        }
        return okHttpClient;
    }


    public static HashMap<String, String> getHashMapObject(String... nameValuePair) {
        HashMap<String, String> hashMap = null;

        if (null != nameValuePair && nameValuePair.length % 2 == 0) {

            hashMap = new HashMap<>();

            int i = 0;
            while (i < nameValuePair.length) {
                hashMap.put(nameValuePair[i], nameValuePair[i + 1]);
                i += 2;
            }

        }

        return hashMap;
    }

    public static JsonObject getJsonMapObject(String... nameValuePair) {
        JsonObject jsonObject = null;

        if (null != nameValuePair && nameValuePair.length % 2 == 0) {

            jsonObject = new JsonObject();

            int i = 0;
            while (i < nameValuePair.length) {
                jsonObject.addProperty(nameValuePair[i], nameValuePair[i + 1]);
                i += 2;
            }

        }

        return jsonObject;
    }
}
