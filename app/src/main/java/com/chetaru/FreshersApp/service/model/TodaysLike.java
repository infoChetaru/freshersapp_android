package com.chetaru.FreshersApp.service.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class TodaysLike {
    @SerializedName("postIds")
    @Expose
    private Integer postId;
    @SerializedName("profileImages")
    @Expose
    private String profileImage;

    public Integer getPostId() {
        return postId;
    }

    public void setPostId(Integer postId) {
        this.postId = postId;
    }

    public String getProfileImage() {
        return profileImage;
    }

    public void setProfileImage(String profileImage) {
        this.profileImage = profileImage;
    }
}
