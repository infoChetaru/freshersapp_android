package com.chetaru.FreshersApp.service.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class NotificationList implements Serializable {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("postId")
    @Expose
    private Integer postId;

    @SerializedName("image")
    @Expose
    private String image;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("notificationType")
    @Expose
    private String notificationType;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("isRead")
    @Expose
    private Boolean isRead;
    @SerializedName("locale")
    @Expose
    private Boolean locale;
    @SerializedName("postExpired")
    @Expose
    private Boolean postExpired;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getNotificationType() {
        return notificationType;
    }

    public void setNotificationType(String notificationType) {
        this.notificationType = notificationType;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public Boolean getIsRead() {
        return isRead;
    }

    public void setIsRead(Boolean isRead) {
        this.isRead = isRead;
    }

    public Boolean getLocale() {
        return locale;
    }

    public void setLocale(Boolean locale) {
        this.locale = locale;
    }

    public Integer getPostId() {
        return postId;
    }

    public void setPostId(Integer postId) {
        this.postId = postId;
    }

    public Boolean getPostExpired() {
        return postExpired;
    }

    public void setPostExpired(Boolean postExpired) {
        this.postExpired = postExpired;
    }
}
