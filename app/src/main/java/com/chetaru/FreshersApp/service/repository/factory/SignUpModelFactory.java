package com.chetaru.FreshersApp.service.repository.factory;

import android.app.Application;

import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import com.chetaru.FreshersApp.viewModel.SignUpViewModel;

public class SignUpModelFactory implements ViewModelProvider.Factory {
    private Application mApplication;
    private Object[] mParams;

    public SignUpModelFactory(Application application, Object... params) {
        mApplication = application;
        mParams = params;
    }
    @Override
    public <T extends ViewModel> T create(Class<T> modelClass) {
        return (T) new SignUpViewModel(mApplication, mParams);

    }

}
