package com.chetaru.FreshersApp.service.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ConnectionStatus {

    /**
     * "message": "Connection status.",
     * "data":{
     * "connectionStatus": 2
     * }
     * }
     *
     * connectionStatus :- 1 for pending, 2 for accepted, 3 for decline, 4 for unfriend, 5 for blocked
     * */

    @SerializedName("connectionStatus")
    @Expose
    private Integer connectStatus;


}
