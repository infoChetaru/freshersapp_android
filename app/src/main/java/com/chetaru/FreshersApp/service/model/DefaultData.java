package com.chetaru.FreshersApp.service.model;

public class DefaultData {

    public int image;
    public String profileName;

    public DefaultData(int image, String profileName) {
        this.image = image;
        this.profileName = profileName;
    }
}
