package com.chetaru.FreshersApp.service.repository.factory;

import android.app.Application;

import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import com.chetaru.FreshersApp.utility.Validations;
import com.chetaru.FreshersApp.viewModel.VerificationViewModel;
import com.chetaru.FreshersApp.viewModel.VerifyViewModel;

public class VerifyModelFactory implements ViewModelProvider.Factory {
    private Application mApplication;
    private Object[] mParams;

    public VerifyModelFactory(Application application, Object... params) {
        mApplication = application;
        mParams = params;
    }

    @Override
    public <T extends ViewModel> T create(Class<T> modelClass) {
        /*if (modelClass == LoginViewModel.class) {
            return (T) new LoginViewModel(mApplication, (String) mParams[0]);
        }  else {
            return super.create(modelClass);
        }*/
       /* try {
            if (!Validations.isEmptyString((String) mParams[1])) {
               return  (T) new VerifyViewModel(mApplication, (String) mParams[0], (String) mParams[1]);
            } else if (!Validations.isEmptyString((String) mParams[2])) {
                return (T) new VerifyViewModel(mApplication, (Integer) mParams[0], (String) mParams[1],(String) mParams[2]);
            }
        }catch (Exception e){
            e.printStackTrace();
            return (T) new VerifyViewModel(mApplication);
        }*/
        return (T) new VerifyViewModel(mApplication, mParams);
    }
}
