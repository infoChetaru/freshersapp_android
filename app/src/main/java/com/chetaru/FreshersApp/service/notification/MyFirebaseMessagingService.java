package com.chetaru.FreshersApp.service.notification;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.util.Log;

import androidx.core.app.NotificationCompat;

import com.chetaru.FreshersApp.R;
import com.chetaru.FreshersApp.utility.BaseActivity;
import com.chetaru.FreshersApp.utility.SessionParam;
import com.chetaru.FreshersApp.view.ui.Activity.ChatActivity;
import com.chetaru.FreshersApp.view.ui.Activity.HomeActivity;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

public class MyFirebaseMessagingService extends FirebaseMessagingService {
    private static final String TAG = "MyFirebaseMsgService";
    Bitmap bitmap;
    String changeit_id="";
    String body="";
    String title="";
    String type="";
    String icon="";
    String image="";
    String que="";
    Context context;
    SessionParam sessionParam;
    BaseActivity baseActivity;
    NotificationCompat.Builder notificationBuilder;
    NotificationChannel Channel;
    NotificationManager notificationManager;

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        // TODO(developer): Handle FCM messages here.
        // Not getting messages here? See why this may be: https://goo.gl/39bRNJ
        Log.d(TAG, "From: " + remoteMessage.getFrom().toString());
        //I/FIAM.Headless: Starting InAppMessaging runtime with Instance ID YOUR_APP_ID

        if (remoteMessage.getData()!=null){
            Log.d(TAG, "Message  Body: " + remoteMessage.getData());
            changeit_id = remoteMessage.getData().get("changeit_id");
            que = remoteMessage.getData().get("que");
            body = remoteMessage.getData().get("body");
            title = remoteMessage.getData().get("title");
            type = remoteMessage.getData().get("type");

            //displayCustomNotificationForOrders(title,body);
           /* icon=remoteMessage.getData().get("icon");
            sendFCMNotification(this,body,icon);*/
           getSubPubFunction();
        }

       /* {
        "message": {
            "attributes": {
                "key": "value"
            },
            "data": "SGVsbG8gQ2xvdWQgUHViL1N1YiEgSGVyZSBpcyBteSBtZXNzYWdlIQ==",
                    "messageId": "136969346945"
        },
        "subscription": "projects/myproject/subscriptions/mysubscription"
    }*/

        // Check if message contains a data payload.
        if (remoteMessage.getData().size() > 0) {
            Log.d(TAG, "Message data payload: " + remoteMessage.getData());
            if (/* Check if data needs to be processed by long running job */ true) {
                // For long-running tasks (10 seconds or more) use Firebase Job Dispatcher.
                //scheduleJob();
            } else {
                // Handle message within 10 seconds
               //    handleNow();
            }
        }

        if (remoteMessage.getNotification() != null) {
            body = remoteMessage.getNotification().getBody();
            title = remoteMessage.getNotification().getTitle();
            try {
               // type="";
               // type = remoteMessage.getNotification().get();
            }catch (Exception e){
                e.printStackTrace();
            }

            try {
                Log.d(TAG, "Message Notification Body: " + remoteMessage.getNotification().toString());
                Log.d(TAG, "Message Notification Body: " + remoteMessage.getNotification().getBody().toString());
               /* icon = remoteMessage.getNotification().getIcon();
                if (!icon.isEmpty()){
                    Log.d(TAG, "Message image Body: " + icon.toString());
                }*/
                //sendNotification(body,title);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        sendNotification(body,title,type);
    }

    private void getSubPubFunction() {
        try{
           /* exports.YOUR_FUNCTION_NAME = functions.pubsub.topic('YOUR_TOPIC_NAME').onPublish((message) => {

            // Decode the PubSub Message body.
  const messageBody = message.data ? Buffer.from(message.data, 'base64').toString() : null;

            // Print the message in the logs.
            console.log(`Hello ${messageBody || 'World'}!`);
            return null;


});*/
        }catch (Exception e){
            e.printStackTrace();
        }
    }


    @Override
    public void onNewToken(String s) {
        super.onNewToken(s);
        SessionParam sessionParam = new SessionParam(this);
        sessionParam.saveFCM_Token(this,s);
        Log.e("NEW_TOKEN",s);
    }

    /*
    * tagUser	When tag user.
hashTagUser	When tag hashtag user.
postLike	When user liked post.
pendingRequest	When connection request comes.
acceptRequest	When connection request accepted by user.
sendMessage	When user send message.*/
    private void sendNotification(String body, String title,String type) {
        Intent intent=null;

             intent = new Intent(this, HomeActivity.class)
                    .putExtra("path", "noti");

        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent,
                PendingIntent.FLAG_ONE_SHOT);
        String channelId = getString(R.string.default_notification_channel_id);
        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder =
                new NotificationCompat.Builder(this, channelId)
                        .setSmallIcon(R.drawable.ic_launcher)
                        .setLargeIcon( BitmapFactory.decodeResource(getResources(),R.drawable.ic_launcher))
                        .setContentTitle(title)
                        .setContentText(body)
                        .setAutoCancel(true)
                        .setSound(defaultSoundUri)
                        //.setStyle(bigText)
                       // .setBadgeIconType(R.drawable.close_circle)
                        //.setPriority(Notification.PRIORITY_MAX)
                        .setPriority(NotificationCompat.PRIORITY_MAX)
                        .setContentIntent(pendingIntent);



        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel notificationChannel = new NotificationChannel(channelId, "Your Notifications",
                    NotificationManager.IMPORTANCE_HIGH);
            notificationChannel.setDescription(body);
            // notificationChannel.enableLights(true);
            notificationChannel.enableVibration(true);
            notificationManager.createNotificationChannel(notificationChannel);
        }
        //Since android Oreo notification channel is needed.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            sessionParam = new SessionParam(getApplicationContext());
            /*Intent intent1=null;
            if (type.equals("tagUser")){
                intent1 = new Intent(this, HomeActivity.class)
                        .putExtra("action", "tagUser");
            }else if (type.equals("hashTagUser")){
                intent1 = new Intent(this, HomeActivity.class)
                        .putExtra("action", "hashTagUser");
            }else if (type.equals("postLike")){
                intent1 = new Intent(this, HomeActivity.class)
                        .putExtra("action", "postLike");
            }else if(type.equals("pendingRequest")){
                intent1 = new Intent(this, HomeActivity.class)
                        .putExtra("action", "pendingRequest");
            }else if (type.equals("acceptRequest")){
                intent1 = new Intent(this, HomeActivity.class)
                        .putExtra("action", "acceptRequest");
            }else if (type.equals("sendMessage")){
                intent1 = new Intent(this, ChatActivity.class)
                        .putExtra("action", "sendMessage");
               // this.sendBroadcast(intent);
            }
            this.sendBroadcast(intent1);*/
            if(type.equals("sendMessage")){
                Intent myIntent = new Intent("sendMessage");
                myIntent.putExtra("action","chat");
                this.sendBroadcast(myIntent);
            }
            NotificationChannel channel = new NotificationChannel(channelId,
                    "Channel title",
                    NotificationManager.IMPORTANCE_MIN);
            //code Added on 10 march
            notificationBuilder.setChannelId(channelId);
            notificationManager.createNotificationChannel(channel);
        }
        notificationManager.notify(0 /* ID of notification */, notificationBuilder.build());

    }


}
