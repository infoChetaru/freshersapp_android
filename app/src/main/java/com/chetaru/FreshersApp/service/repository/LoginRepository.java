package com.chetaru.FreshersApp.service.repository;

import android.content.Intent;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.chetaru.FreshersApp.service.model.User;
import com.chetaru.FreshersApp.service.model.UserRequest;
import com.chetaru.FreshersApp.service.model.UserResponse;
import com.chetaru.FreshersApp.service.retrofit.ApiClient;
import com.chetaru.FreshersApp.utility.SessionParam;

import org.json.JSONObject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginRepository {

    private FresherAppService apiService;
    private static LoginRepository userRepository;



    private LoginRepository() {
        //apiService = ApiClient.getClient().create(FresherAppService.class);
        apiService = ApiClient.getInstance();
        /*I would also change this slightly. Instead of observing fetch,
        I would observe the LiveData directly.
        * */
        //user = LoginRepository.getInstance().getUser();

        /*
        Later, you can request updated data from the server at any point.
        * */
        //LoginRepository.getInstance().fetchUser2();
        /*
        You could also call fetchUser2() on the first construction of UserRepository.
         Then only updates would call fetchUser2() directly.
        * */
        //fetchUser2();

    }



    public synchronized static LoginRepository getInstance() {
        if (userRepository == null) userRepository = new LoginRepository();
        return userRepository;
    }
    public LiveData<UserResponse> fetchUser(String userName,String password,String deviceId,String myToken) {
        final MutableLiveData<UserResponse> data = new MutableLiveData<>();
        UserRequest request=new UserRequest();

        request.setUserName(userName);
        request.setPassword(password);
        request.setDeviceId(deviceId);
        request.setFcmToken(myToken);
        request.setRole(2);
        Call<UserResponse> call = apiService.getLogin(request);
        call.enqueue(new Callback<UserResponse>() {
            @Override
            public void onResponse(@NonNull Call<UserResponse> call, @NonNull Response<UserResponse> response) {
                if (response.body() != null) {
                    //hideLoader();
                    try {
                        data.postValue(response.body());
                    }catch (Exception e){
                        e.printStackTrace();
                        data.postValue(null);
                    }
                }
                /*try {
                    Log.d("codeJson",repos.toString());
                    JSONObject jsonObject = (JSONObject) repos;
                    String message = jsonObject.optString("message");
                    String code = jsonObject.optString("code");

                    utility.showToast(SignUpActivity.this,message);
                    //Toast.makeText(Verification_Activity.this, message, Toast.LENGTH_SHORT).show();
                    JSONObject object=new JSONObject();
                    object=repos.getJSONObject("data");
                    sessionParam = new SessionParam((JSONObject) object);
                    sessionParam.persist(SignUpActivity.this);
                    Intent intent=new Intent(SignUpActivity.this, HomeActivity.class);
                    startActivity(intent);
                    finish();


                }catch (Exception e){
                    e.printStackTrace();
                }*/
            }

            @Override
            public void onFailure(@NonNull Call<UserResponse> call, @NonNull Throwable t) {
                //hideLoader();
                data.postValue(null);
                t.printStackTrace();
            }
        });
        return data;
    }
    // My alterations below:
    private MutableLiveData<User> userLiveData = new MutableLiveData<>();

    public LiveData<User> getUser() {
        return userLiveData;
    }

    public LiveData<User> fetchUser2() {
        UserRequest response=new UserRequest();
        Call<UserResponse> call = apiService.getLogin(response);
        call.enqueue(new Callback<UserResponse>() {
            @Override
            public void onResponse(@NonNull Call<UserResponse> call, @NonNull Response<UserResponse> response) {
                try {
                    if (response.body() != null) {
                        userLiveData.postValue(response.body().getUserData());
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }

                // TODO: Consider a fallback response to the LiveData here, in the case that bad data is returned. Perhaps null?
            }

            @Override
            public void onFailure(@NonNull Call<UserResponse> call, @NonNull Throwable t) {
                userLiveData.postValue(null);
                t.printStackTrace();
            }
        });
        return userLiveData;
    }
}
