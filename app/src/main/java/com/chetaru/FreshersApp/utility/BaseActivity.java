package com.chetaru.FreshersApp.utility;

import android.app.Activity;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;

import android.text.TextUtils;
import android.util.Config;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.chetaru.FreshersApp.R;
import com.chetaru.FreshersApp.service.retrofit.BaseRequest;

import com.chetaru.FreshersApp.service.retrofit.Functions;
import com.chetaru.FreshersApp.service.retrofit.RequestReciever;
import com.chetaru.FreshersApp.view.ui.Activity.HomeActivity;
import com.chetaru.FreshersApp.view.ui.Activity.RootActivity;
import com.chetaru.FreshersApp.view.ui.LoginActivity;
import com.google.gson.JsonObject;

import java.util.ArrayList;
import java.util.List;


/**
 * Base activity class. This may be useful in<br/>
 * Implementing google analytics or <br/>
 * Any app wise implementation
 */


public abstract class BaseActivity extends AppCompatActivity {
    private static List<Activity> sActivities = new ArrayList<Activity>();
    public Activity currentAct;
    public Context mContext;
    SessionParam sessionParam;
   // private FirebaseAnalytics mFirebaseAnalytics;
    private BaseRequest baseRequest;
    Utility utility;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
        }
        try {
            requestWindowFeature(Window.FEATURE_NO_TITLE);
            this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        }catch (Exception e){
            e.printStackTrace();
        }

        mContext = this;
        currentAct = this;
        sessionParam = new SessionParam(mContext);
        sActivities.add(this);
       // mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);
//        Crashlytics.getInstance().crash();
        Button crashButton = new Button(this);
        // crashButton.setText("Crash!");
//        crashButton.setOnClickListener(new View.OnClickListener() {
//            public void onClick(View view) {
//                Crashlytics.getInstance().crash(); // Force a crash
//            }
//        });
        addContentView(crashButton, new ViewGroup.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT));
    }

    public void setAppBar(String title, boolean isBackVisible) {
        /*Toolbar toolbar = findViewById(R.id.toolbar_default);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(isBackVisible);
        getSupportActionBar().setDisplayShowHomeEnabled(isBackVisible);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
                hideSoftKeyboard();
            }
        });
        getSupportActionBar().setTitle(title);*/
        //existingMsg
    }

    public void callHomeAct(Context context) {
        if (sessionParam.role.equals("3")) {
            finishAllActivities();
            startActivity(new Intent(context, HomeActivity.class));
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        registerReceiver(myReceiver, new IntentFilter("Notification"));
        //hideSoftKeyboard();
    }

    @Override
    protected void onPause() {
        super.onPause();
        unregisterReceiver(myReceiver);
        //hideSoftKeyboard();
    }

    public void hideSoftKeyboard() {
        //getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        try {
            InputMethodManager inputMethodManager = (InputMethodManager) this
                    .getSystemService(Activity
                            .INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(this.getCurrentFocus().getWindowToken
                    (), 0);
        } catch (Exception e) {
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        try {
            if (sActivities != null) {
                sActivities.remove(this);
            }
            super.onDestroy();
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public void finishAllActivities() {
        try {
            if (sActivities != null) {
                for (Activity activity : sActivities) {
                    if (Config.DEBUG) {
                        Log.d("BaseActivity", "finishAllActivities activity=" + activity.getClass()
                                .getSimpleName());
                    }
                    activity.finish();
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public static void finishAllActivitiesStatic() {
        if (sActivities != null) {
            for (Activity activity : sActivities) {
                if (Config.DEBUG) {
                    Log.d("BaseActivity", "finishAllActivities activity=" + activity.getClass()
                            .getSimpleName());
                }
                activity.finish();
            }
        }
    }

    public String getAppString(int id) {
        String str = "";
        if (!TextUtils.isEmpty(this.getResources().getString(id))) {
            str = this.getResources().getString(id);
        } else {
            str = "";
        }
        return str;
    }

    public BaseActivity() {
    }



    public BroadcastReceiver myReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getStringExtra("action");
            //if (sessionParam.role.equals("3")) {
               // dialogBaseActivity();
           // }
        }
    };



    public void logoutDialog() {
        final Dialog dialog = new Dialog(mContext);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_logout);
        dialog.setCanceledOnTouchOutside(true);
        TextView tv_cancel = dialog.findViewById(R.id.tv_cancel);
        TextView tv_logout = dialog.findViewById(R.id.tv_logout);
        tv_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        tv_logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(mContext);
                SharedPreferences.Editor editor = prefs.edit();
                editor.clear();
                editor.commit();
                api_logout();
            }
        });
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = dialog.getWindow();
        lp.copyFrom(window.getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);
        dialog.show();
    }

    public void api_logout() {
        baseRequest = new BaseRequest(mContext);
        baseRequest.setBaseRequestListner(new RequestReciever() {
            @Override
            public void onSuccess(int requestCode, String Json, Object object) {
                sessionParam.clearPreferences(mContext);
                startActivity(new Intent(mContext, RootActivity.class));
                finishAllActivities();
            }

            @Override
            public void onFailure(int requestCode, String errorCode, String message) {
               // utility.showToast(mContext, message);
            }

            @Override
            public void onNetworkFailure(int requestCode, String message) {
                utility.showToast(mContext, message);
            }
        });
        JsonObject main_object = new JsonObject();
        main_object.addProperty("fcmId", sessionParam.fcmId);
        baseRequest.callAPIPost(1, main_object, getString(R.string.api_logout));

    }

}