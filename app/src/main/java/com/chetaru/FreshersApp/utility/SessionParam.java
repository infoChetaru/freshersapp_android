package com.chetaru.FreshersApp.utility;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.chetaru.FreshersApp.service.model.LocaleData;
import com.chetaru.FreshersApp.service.model.MyProfileResponse;
import com.chetaru.FreshersApp.service.model.User;
import com.chetaru.FreshersApp.service.model.UserLoginRequest;
import com.chetaru.FreshersApp.service.model.UserRequest;
import com.chetaru.FreshersApp.service.model.UserViewPost;
import com.google.gson.Gson;
import com.google.gson.internal.bind.JsonTreeReader;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/*import com.facebook.AccessToken;
import com.facebook.login.LoginManager;*/


public class SessionParam {


    String PREF_NAME = "MyPref";
    String FCM_NAME = "FcmToken";
    String FEED_NAME = "FeedSave";
    String LOGIN_PREF_NAME = "LoginData";
    String key = "LoginKey";
    ArrayList<UserLoginRequest> ModelArrayList=new ArrayList();
    Context _context;




    public void SessionParam() {
    }


    public String signupStage = "0", loginSession = "n";
    public String countryId="";
    public Integer signupType;
    public int id=0,localeId;
    public String  isDot, name, email, officeId, departmentId, orgId, office, department,  organisation_logo, role;
    public String lastName = "";
    public String dob,bio,expiryDate,purchaseDate,localeName;
    public boolean premieUser=false;
    public int gender,fcmId,connectionStatus,profileLocaleId;
    public String token = "";
    public String companyList = "";
    public String password = "";
    public String companyOrgId = "";
    public Integer verificationCode;
    public String userName;
    public String deviceId = "";
    public String fcm_token = "";
    public String orgname = "", profileImage = "";
    public Boolean isOnHomeScreen = false;
    public String loginId="",loginPass="",loginUserName="",loginUserImage="";
    public int type,sensitivePostsAllowed;
    public String logintellsid = ""; //creating this string to check if tellsid login api worked properly or not.

    public String  orderId ;
   public String packageName;
    public String productId;
   public Long purchaseTime ;
    public String purchaseToken ;
    public String developerPayload ;
    public int purchaseState ;
   public boolean acknowledged ;
    public boolean autoRenewing;
    public int homePostId,homeCurrentPage,homeTotalPage,homePostPosition;


    public SessionParam(Context context, String signupStage) {
        this.signupStage = signupStage;
        SharedPreferences sharedPreferences = context.getSharedPreferences(
                PREF_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor prefsEditor = sharedPreferences.edit();
        prefsEditor.putString("signupStage", signupStage);
        prefsEditor.commit();
    }

    public void SavePost(Context context,int postId, int viewPage,int totalPage ,int position) {
        this.homePostId = postId;
        this.homeCurrentPage = viewPage;
        this.homeTotalPage = totalPage;
        this.homePostPosition = position;
        SharedPreferences sharedPreferences = context.getSharedPreferences(
                PREF_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor prefsEditor = sharedPreferences.edit();
        prefsEditor.putInt("homePostId", homePostId);
        prefsEditor.putInt("homeCurrentPage", homeCurrentPage);
        prefsEditor.putInt("homeTotalPage", homeTotalPage);
        prefsEditor.putInt("position", homePostPosition);
        // prefsEditor.putString("viewPost", viewPost);
        prefsEditor.commit();
    }

    public SessionParam(Context context,LocaleData locale) {
        this.localeName = locale.getName();
        this.localeId = locale.getId();
        SharedPreferences sharedPreferences = context.getSharedPreferences(
                PREF_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor prefsEditor = sharedPreferences.edit();
        prefsEditor.putInt("localeId", localeId);
        prefsEditor.putString("localeName", localeName);
        prefsEditor.commit();
    }
    public SessionParam(Context context,boolean premieValue) {
        this.premieUser = premieValue;
        SharedPreferences sharedPreferences = context.getSharedPreferences(
                PREF_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor prefsEditor = sharedPreferences.edit();
        prefsEditor.putBoolean("premieUser", premieUser);

        prefsEditor.commit();
    }
    public void  saveFeed(Context context, List<UserViewPost> viewPosts){
        SharedPreferences sharedPreferences = context.getSharedPreferences(FEED_NAME,
                Context.MODE_PRIVATE);
       /* List<UserViewPost> getListValueFromKey =
                sharedPreferences.getStringSet("yourKey",new HashSet<UserViewPost>());
        getListValueFromKey.add("Hahaha");
        getListValueFromKey.add("Kakaka");
        getListValueFromKey.add("Hohoho");
        *//*  Add value or Do nothing …… *//*
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.clear(); //[important] Clearing your editor before using it.
        editor.putStringSet("yourKey", getListValueFromKey);
        editor.commit();*/
    }

    //by calling this method from api response we can directly save our data to shared preference
    public  SessionParam(JSONObject jsonObject) {
        if (jsonObject != null) {
            //userId = jsonObject.optString("userId", "");
            id = jsonObject.optInt("id", 0);
            name = jsonObject.optString("name", "");
            userName = jsonObject.optString("userName", "");
            role = jsonObject.optString("role", "");
            profileImage = jsonObject.optString("profileImage", "");
            token = jsonObject.optString("token", "");
            type = jsonObject.optInt("type", 0);
            dob = jsonObject.optString("dob", "");
            bio = jsonObject.optString("bio", "");
            gender = jsonObject.optInt("gender", 0);
            sensitivePostsAllowed = jsonObject.optInt("sensitivePostsAllowed", 0);

            fcmId = jsonObject.optInt("fcmId", 0);
            expiryDate = jsonObject.optString("expiryDate", "");
            purchaseDate = jsonObject.optString("purchaseDate", "");
            localeId = jsonObject.optInt("localeId", 0);
           premieUser = jsonObject.optBoolean("premieUser", false);

            localeName = jsonObject.optString("localeName", "");


           /* lastName = jsonObject.optString("lastName", "");
            email = jsonObject.optString("email", "");
            officeId = jsonObject.optString("officeId", "");
            departmentId = jsonObject.optString("departmentId", "");
            countryId = jsonObject.optString("departmentId", "");
            office = jsonObject.optString("office", "");
            department = jsonObject.optString("department", "");
            token = jsonObject.optString("token", "");
            role = jsonObject.optString("role", "");
            orgId = jsonObject.optString("orgId", "");
            isDot = jsonObject.optString("isDot", "");
            organisation_logo = jsonObject.optString("organisation_logo", "");
            deviceId = jsonObject.optString("deviceId", "");
            orgname = jsonObject.optString("orgname", "");
            profileImage = jsonObject.optString("profileImage", "");*/



        }
    }
    public  SessionParam(Context context, String subOrderId, String subPackageName, String subProductId
            , Long subPurchaseTime, String subPurchaseToken, String subDeveloperPayload,
                         int subPurchaseState,boolean subAcknowledged,boolean subAutoRenewing ) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(
                PREF_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor prefsEditor = sharedPreferences.edit();



            //userId = jsonObject.optString("userId", "");
            orderId = subOrderId;
            packageName = subPackageName;
            productId = subProductId;
            purchaseTime = subPurchaseTime;
             purchaseToken = subPurchaseToken;
             developerPayload = subDeveloperPayload;
            purchaseState = subPurchaseState;
            acknowledged =subAcknowledged;
            autoRenewing = subAutoRenewing;

            prefsEditor.putString("orderId", orderId);
            prefsEditor.putString("packageName", packageName);
            prefsEditor.putString("productId", productId);
            prefsEditor.putLong("purchaseTime", purchaseTime);
            prefsEditor.putString("purchaseToken", purchaseToken);
            prefsEditor.putString("developerPayload", developerPayload);
            prefsEditor.putInt("purchaseState", purchaseState);
            prefsEditor.putBoolean("acknowledged", acknowledged);
            prefsEditor.putBoolean("autoRenewing", autoRenewing);
            Log.d("HomeActivity","purchases Data: orderId:"+orderId+"packageName: "+packageName+"purchasesToken: "+token+"developerPayload: "+developerPayload);
            prefsEditor.commit();

    }
    public SessionParam(Context mContext,MyProfileResponse myProfile) {
        if (myProfile!=null){
            id=myProfile.getId();
            name=myProfile.getName();
            userName=myProfile.getUserName();
            dob=myProfile.getDob();
            bio=myProfile.getBio();
            gender=myProfile.getGender();
            sensitivePostsAllowed=myProfile.getSensitivePostsAllowed();
            localeId=myProfile.getLocaleId();
            localeName=myProfile.getLocaleName();
            profileImage=myProfile.getProfileImage();
            expiryDate=myProfile.getExpiryDate();
            purchaseDate=myProfile.getPurchaseDate();
            premieUser=myProfile.getPremieUser();
            connectionStatus=myProfile.getConnectionStatus();
            SharedPreferences sharedPreferences = mContext.getSharedPreferences(
                    PREF_NAME, Context.MODE_PRIVATE);
            SharedPreferences.Editor prefsEditor = sharedPreferences.edit();

            prefsEditor.putInt("id", id);
            prefsEditor.putString("name", name);
            prefsEditor.putString("userName", userName);
            prefsEditor.putString("dob", dob);
            prefsEditor.putString("bio", bio);
            prefsEditor.putInt("gender", gender);
            prefsEditor.putInt("sensitivePostsAllowed", sensitivePostsAllowed);
            prefsEditor.putInt("localeId", localeId);
            prefsEditor.putString("localeName", localeName);
            prefsEditor.putString("profileImage", profileImage);
            prefsEditor.putString("expiryDate", expiryDate);
            prefsEditor.putString("purchaseDate", purchaseDate);
            prefsEditor.putBoolean("premieUser", premieUser);
            prefsEditor.putInt("connectionStatus", connectionStatus);

            prefsEditor.commit();

        }
    }

    public void companyList(Context context, String list, String companyOrgId) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(
                PREF_NAME, Context.MODE_PRIVATE);
        companyList = list;
        SharedPreferences.Editor prefsEditor = sharedPreferences.edit();
        if (!list.equals("")) {
            prefsEditor.putString("companyList", companyList);
        }
        prefsEditor.putString("companyOrgId", companyOrgId);
        prefsEditor.commit();

    }
    public void loginUserListSave(Context context,List<UserLoginRequest> requestList){
        SharedPreferences shref;
        SharedPreferences.Editor editor;
        shref = context.getSharedPreferences(LOGIN_PREF_NAME, Context.MODE_PRIVATE);
        for (int i=0;i<requestList.size();i++) {
            UserLoginRequest request = new UserLoginRequest();
            request.setEmail(requestList.get(i).getEmail());
            request.setName(requestList.get(i).getName());
            request.setProfileImage(requestList.get(i).getProfileImage());
            request.setPassword(requestList.get(i).getPassword());
            ModelArrayList.add(i,request);
        }

        Gson gson = new Gson();
        String json = gson.toJson(ModelArrayList);

        editor = shref.edit();
        //editor.remove(key).commit();
        editor.putString(key, json);
        editor.commit();
    }

    public void loginSave(Context context,String emailId,String password,JSONObject jsonObject){
        SharedPreferences shref;
        SharedPreferences.Editor editor;
        shref = context.getSharedPreferences(LOGIN_PREF_NAME, Context.MODE_PRIVATE);
        if (jsonObject!=null) {
            loginUserName = jsonObject.optString("name", "");
            loginUserImage = jsonObject.optString("profileImage", "");
            loginId = emailId;
            loginPass = password;
        }
        UserLoginRequest request=new UserLoginRequest();
        request.setEmail(loginId);
        request.setName(loginUserName);
        request.setProfileImage(loginUserImage);
        request.setPassword(password);
        ModelArrayList.add(request);

        Gson gson = new Gson();
        String json = gson.toJson(ModelArrayList);

        editor = shref.edit();
        //editor.remove(key).commit();
        editor.putString(key, json);
        editor.commit();
    }

    public List<UserLoginRequest> retriveLoginData(Context context){
        SharedPreferences shref;
        try {
            shref = context.getSharedPreferences(LOGIN_PREF_NAME, Context.MODE_PRIVATE);
        Gson gson = new Gson();
        String response=shref.getString(key , "");
        ArrayList<UserLoginRequest> lstArrayList = gson.fromJson(response,
                new TypeToken<List<UserLoginRequest>>(){}.getType());
        if (lstArrayList!=null) {
            if (lstArrayList.size() > 0)
                Log.d("loginData", lstArrayList.get(0).getName().toString());
        }

            return lstArrayList;
        }catch (Exception e){
            e.printStackTrace();
            return null;
        }

        /*String json = shref.getString(key, "");
        UserLoginRequest obj = gson.fromJson(json, UserLoginRequest.class);*/
    }
    public void saveLoginInformation(Context context,String emailId,String password,JSONObject jsonObject) {
        SharedPreferences shared = context.getSharedPreferences(LOGIN_PREF_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = shared.edit();
        if (jsonObject!=null) {
            loginUserName = jsonObject.optString("name", "");
            loginId = emailId;
            loginPass = password;

           /* editor.putString("loginId", loginId);
            editor.putString("loginName", loginUserName);
            editor.putString("password", loginPass);*/
        }
       // editor.commit();

        try {
            HashMap<Integer, String> hash = new HashMap<>();
            JSONArray arr = new JSONArray();
            for(Integer index : hash.keySet()) {
                JSONObject json = new JSONObject();
                json.put("id", index);
                json.put("name", loginUserName);
                json.put("email",loginId);
                json.put("password",password);
                arr.put(json);
            }
            editor.putString("login",arr.toString());
            editor.commit();
            //getSharedPreferences(INSERT_YOUR_PREF).edit().putString("savedData", arr.toString()).apply();
        } catch (JSONException exception) {
            // Do something with exception
        }
       // Map<String, String> map = new HashMap<>();
        //map.put("token", String.valueOf(tokenId));
    }

    /**
     * Get stored session data
     * */
    public HashMap<String, String> getUserLoginDetails(Context context){
        SharedPreferences pref = context.getSharedPreferences(
                LOGIN_PREF_NAME, Context.MODE_PRIVATE);
        //Use hashmap to store user credentials
        HashMap<String, String> user = new HashMap<String, String>();

        // user name
        user.put(loginUserName, pref.getString("loginName",loginUserName ));
        user.put(loginId, pref.getString("loginId", loginId));
        user.put(loginPass, pref.getString("password", loginPass));

        try {
           // String data = getSharedPreferences(INSERT_YOUR_PREF).getString("savedData");

            HashMap<Integer, String> hash = new HashMap<>();
            JSONArray arr = new JSONArray(pref.toString());
            for(int i = 0; i < arr.length(); i++) {
                JSONObject json = arr.getJSONObject(i);
                hash.put(json.getInt("id"), json.getString("name"));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        // return user
        return user;
    }


    //this method is used to save device id
    public void saveDeviceId(Context context, String deviceId) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(
                PREF_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor prefsEditor = sharedPreferences.edit();

        prefsEditor.putString("deviceId", deviceId);
        prefsEditor.commit();
    }

    //this method is used to initialize this class and then we will get access to all saved data
    public SessionParam(Context context) {

        SharedPreferences sharedPreferences = context.getSharedPreferences(
                PREF_NAME, Context.MODE_PRIVATE);
        SharedPreferences sharedPreferencesFcm = context.getSharedPreferences(
                FCM_NAME, Context.MODE_PRIVATE);
        this.logintellsid = sharedPreferences.getString("logintellsid", "");
        this.name = sharedPreferences.getString("name", "");
        this.lastName = sharedPreferences.getString("lastName", "");
        this.email = sharedPreferences.getString("email", "");
        this.officeId = sharedPreferences.getString("officeId", "");
        this.departmentId = sharedPreferences.getString("departmentId", "");
        this.office = sharedPreferences.getString("office", "");
        this.department = sharedPreferences.getString("department", "");
        this.token = sharedPreferences.getString("token", "");
        this.role = sharedPreferences.getString("role", "");
        this.orgId = sharedPreferences.getString("orgId", "");
        this.isDot = sharedPreferences.getString("isDot", "");
        this.orgname = sharedPreferences.getString("orgname", "");
        this.profileImage = sharedPreferences.getString("profileImage", "");
        this.password = sharedPreferences.getString("password", "");
        this.userName = sharedPreferences.getString("userName", "");
        this.countryId = sharedPreferences.getString("countryId", "");
        try {
            this.id = sharedPreferences.getInt("id", 0);
        }catch (Exception e){
            e.printStackTrace();
        }

        this.verificationCode=sharedPreferences.getInt("veriCode",0);
        this.companyList = sharedPreferences.getString("companyList", "");
        this.companyOrgId = sharedPreferences.getString("companyOrgId", "");
        this.organisation_logo = sharedPreferences.getString("organisation_logo", "");

        this.deviceId = sharedPreferences.getString("deviceId", "");
        this.type = sharedPreferences.getInt("type", 0);
        //type = jsonObject.optString("type", "");
        this.dob = sharedPreferences.getString("dob", "");
        this.bio = sharedPreferences.getString("bio", "");
        this.gender = sharedPreferences.getInt("gender", 0);
        this.fcmId = sharedPreferences.getInt("fcmId", 0);
        this.sensitivePostsAllowed = sharedPreferences.getInt("sensitivePostsAllowed", 0);
        this.connectionStatus = sharedPreferences.getInt("connectionStatus", 0);
        this.expiryDate = sharedPreferences.getString("expiryDate", "");
        this.purchaseDate = sharedPreferences.getString("purchaseDate", "");
        this.localeId = sharedPreferences.getInt("localeId", 0);
        this.premieUser = sharedPreferences.getBoolean("premieUser", false);
        this.localeName = sharedPreferences.getString("localeName", "");

        this.homePostId = sharedPreferences.getInt("homePostId", 0);
        this.homeCurrentPage = sharedPreferences.getInt("homeCurrentPage", 0);
        this.homeTotalPage = sharedPreferences.getInt("homeTotalPage", 0);
        this.homePostPosition = sharedPreferences.getInt("homePostPosition", 0);



        this.fcm_token = sharedPreferencesFcm.getString("fcm_token", "");
        this.isOnHomeScreen = sharedPreferencesFcm.getBoolean("isOnHomeScreen", false);
          orderId = sharedPreferences.getString("orderId", "");
         packageName = sharedPreferences.getString("packageName", "");
         this.productId = sharedPreferences.getString("productId", "");
         purchaseTime = sharedPreferences.getLong("purchaseTime", 0);
         purchaseToken = sharedPreferences.getString("purchaseToken", "");
         developerPayload = sharedPreferences.getString("developerPayload", "");
         purchaseState = sharedPreferences.getInt("purchaseState", 0);
         acknowledged = sharedPreferences.getBoolean("acknowledged", false);
         autoRenewing = sharedPreferences.getBoolean("autoRenewing", false);




    }

    //as name suggested this method write the data which we want to save in shared Preference
    public void persist(Context context) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(
                PREF_NAME, Context.MODE_PRIVATE);
        SharedPreferences sharedPreferenceFcm = context.getSharedPreferences(
                FCM_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor prefsEditor = sharedPreferences.edit();
        SharedPreferences.Editor prefsEditorFCM = sharedPreferences.edit();

            if (id>0) {
                prefsEditor.putInt("id", id);
            }

        if (!name.isEmpty()) {
            prefsEditor.putString("name", name);
        }
        if (!userName.isEmpty()){
            prefsEditor.putString("userName", userName);
        }

        if (!dob.isEmpty()){
            prefsEditor.putString("dob",dob);
        }
        if (!bio.isEmpty()) {
            prefsEditor.putString("bio", bio);
        }
        if (gender>0) {
            prefsEditor.putInt("gender", gender);
        }

        if(sensitivePostsAllowed>0){
            prefsEditor.putInt("sensitivePostsAllowed",sensitivePostsAllowed);
        }

        if (!profileImage.isEmpty()) {
            prefsEditor.putString("profileImage", profileImage);
        }

        if (premieUser) {
            prefsEditor.putBoolean("premieUser", premieUser);
        }

        if (localeId>0) {
            prefsEditor.putInt("localeId", localeId);
        }

        if (!purchaseDate.isEmpty()) {
            prefsEditor.putString("purchaseDate", purchaseDate);
        }
        if (!expiryDate.isEmpty()) {
            prefsEditor.putString("expiryDate", expiryDate);
        }

        try {

            if (!localeName.isEmpty()) {
                prefsEditor.putString("localeName", localeName);
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        try {
            if (type>0){
                prefsEditor.putInt("type",type);
            }
            if (!fcm_token.isEmpty()) {
                prefsEditorFCM.putString("fcm_token", fcm_token);
            }
            if (!token.isEmpty()) {
                prefsEditor.putString("token", token);
            }
            if (!role.isEmpty()) {
                prefsEditor.putString("role", role);
            }

            if (!logintellsid.isEmpty()) {
                prefsEditor.putString("logintellsid", logintellsid);
            }
            if (fcmId>0){
                prefsEditor.putInt("fcmId",fcmId);
            }
        }catch (Exception e){
            e.printStackTrace();
        }

        prefsEditor.commit();
    }

    // to check login status we have used this method
    public void saveToken(Context context, String Token) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(
                PREF_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor prefsEditor = sharedPreferences.edit();
        prefsEditor.putString("logintellsid", Token);
        prefsEditor.commit();
    }

    //this method is used to save fcm token
    public void saveFCM_Token(Context context, String fcm_token) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(
                FCM_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor prefsEditor = sharedPreferences.edit();
        prefsEditor.putString("fcm_token", fcm_token);
        prefsEditor.commit();
    }

    public void inOnHomeScreen(Context context, Boolean status) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(
                FCM_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor prefsEditor = sharedPreferences.edit();
        prefsEditor.putBoolean("isOnHomeScreen", status);
        prefsEditor.commit();
    }

    //this method will clear all the saved data in shared preference
    public void clearPreferences(Context context) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(
                PREF_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor prefsEditor = sharedPreferences.edit();
        //AccessToken.setCurrentAccessToken(null);
       // LoginManager.getInstance().logOut();
        prefsEditor.clear();
        prefsEditor.commit();
    }

    @Override
    public String toString() {
        return "SessionParam [name=" + "]";
    }
}
