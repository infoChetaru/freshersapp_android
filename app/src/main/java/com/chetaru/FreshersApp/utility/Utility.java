package com.chetaru.FreshersApp.utility;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.net.Uri;
import android.os.Build;
import android.provider.MediaStore;
import android.provider.Settings;
import android.util.Base64;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.RequiresApi;

import com.androidadvance.topsnackbar.TSnackbar;
import com.chetaru.FreshersApp.R;
import com.chetaru.FreshersApp.view.customView.GravityCompoundDrawable;
import com.google.android.gms.common.util.Strings;
import com.google.gson.JsonObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.math.RoundingMode;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.spec.ECField;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.TimeZone;

import id.zelory.compressor.Compressor;


public class Utility {
    String result = "";
    StringBuilder sb;
    String startdate="";
    ProgressDialog loading = null;


    //in this class we will writing code which we need to use more often
    //for eg: fetching current date or showing toast


    public void showToast(Context context, String message) {
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();

    }
    public static void disableDoubleClick(final View view) {
        view.setEnabled(false);
        view.postDelayed(new Runnable() {
            @Override
            public void run() {
                view.setEnabled(true);
            }
        }, 400);
    }
    public void showCustomToast(Context context,String message){
        try {


        int duration = Toast.LENGTH_LONG;

       /* Toast toast = Toast.makeText(context, message , duration);
// Set TOP LEFT in the screen
        toast.setGravity(Gravity.TOP|Gravity.CENTER_HORIZONTAL, 0, 0);
            toast.show();*/
         /* LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.your_custom_layout, null);
        toast.setView(view);*/
        /*View view = toast.getView();
        view.setBackgroundResource(R.color.red);
        TextView text = (TextView) view.findViewById(android.R.id.message);
        text.setTextColor(Color.WHITE);
        toast.setView(view);*/
        /*View view =toast.getView();
        view.setBackgroundColor(Color.WHITE);
        TextView toastMessage = (TextView) toast.getView().findViewById(android.R.id.message);
        toastMessage.setTextColor(Color.BLACK);*/
            LayoutInflater inflater = ((Activity)context).getLayoutInflater();
            View layout = inflater.inflate(R.layout.your_custom_layout,
                    (ViewGroup) ((Activity) context).findViewById(R.id.showCustom));
            // set a message
            TextView text = (TextView) layout.findViewById(R.id.message_txt);
            text.setText(message);

            // Toast...
            Toast toast = new Toast(context);
           // toast.setGravity(Gravity.BOTTOM|Gravity.CENTER_HORIZONTAL, 0, 0);
            toast.setDuration(Toast.LENGTH_LONG);

            toast.setView(layout);
            toast.show();

        }catch (Exception e){
            e.printStackTrace();
        }
    }


    public void getProgerss(Context mContext) {
        loading = new ProgressDialog(mContext);
        loading.setCancelable(false);
        loading.setMessage("Loading....");
        loading.setProgressStyle(ProgressDialog.STYLE_SPINNER);
    }
    public void loading(){
        loading.show();
    }
    public void dismiss(){
            loading.dismiss();
    }
    public boolean showDialog(final Context context, String heading, String message) {


        AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);

        // Setting Dialog Title
        alertDialog.setTitle(heading);

        // Setting Dialog Message
        alertDialog.setMessage(message);

        // Setting Icon to Dialog
        //alertDialog.setIcon(R.drawable.delete);

        // Setting Positive "Yes" Button
        alertDialog.setPositiveButton("YES", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                //return true;
                result = "yes";

                // Write your code here to invoke YES event
                //Toast.makeText(context, "You clicked on YES", Toast.LENGTH_SHORT).show();
            }
        });

        // Setting Negative "NO" Button
        alertDialog.setNegativeButton("NO", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                result = "no";
                // Write your code here to invoke NO event
                //Toast.makeText(getApplicationContext(), "You clicked on NO", Toast.LENGTH_SHORT).show();
                dialog.cancel();
            }
        });

        // Showing Alert Message
        alertDialog.show();
        if (result.equals("yes")) {
            return true;
        } else {
            return false;
        }

    }
    public String getAndroidID(Context context) {
        String android_id= Settings.System.getString(context.getContentResolver(),
                Settings.Secure.ANDROID_ID);


        return  android_id;
    }


    // Put the image file path into this method
    public static String getFileToByte(String filePath){
        Bitmap bmp = null;
        ByteArrayOutputStream bos = null;
        byte[] bt = null;
        String encodeString = null;
        try{
            bmp = BitmapFactory.decodeFile(filePath);
            bos = new ByteArrayOutputStream();
            bmp.compress(Bitmap.CompressFormat.JPEG, 100, bos);
            bt = bos.toByteArray();
            encodeString = Base64.encodeToString(bt, Base64.DEFAULT);
        }
        catch (Exception e){
            e.printStackTrace();
        }
        return encodeString;
    }
    public  String encodeTobase64(Bitmap image) {
        Bitmap immagex = image;
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        immagex.compress(Bitmap.CompressFormat.PNG, 30, baos);
        byte[] b = baos.toByteArray();
        String imageEncoded = Base64.encodeToString(b, Base64.DEFAULT);
        return imageEncoded;
    }
    public String getBase64ImageString(Bitmap photo) {
        String imgString;
        if(photo != null) {
            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            photo.compress(Bitmap.CompressFormat.PNG, 100, outputStream);
            byte[] profileImage = outputStream.toByteArray();

            imgString = Base64.encodeToString(profileImage,
                    Base64.NO_WRAP);
        }else{
            imgString = "";
        }

        return imgString;
    }

    public static String image_to_Base64(Context context, String filePath) {
        Bitmap bmp = null;
        ByteArrayOutputStream bos = null;
        byte[] bt = null;
        String encodeString = null;
        File sourceFile;
        Compressor compressedImageBitmap;

        try {
            sourceFile = new File(filePath);

            bmp = BitmapFactory.decodeFile(filePath);
            bmp = new Compressor(context).compressToBitmap(sourceFile);
            bos = new ByteArrayOutputStream();
            bmp.compress(Bitmap.CompressFormat.JPEG, 50, bos);
            bt = bos.toByteArray();
            encodeString = Base64.encodeToString(bt, Base64.DEFAULT);
        } catch (Exception e) {
            e.printStackTrace();
        } catch (OutOfMemoryError ome) {

        }
        return encodeString;
    }

    public static Bitmap getRoundedCornerBitmap(Bitmap bitmap) {
        Bitmap output = Bitmap.createBitmap(bitmap.getWidth(),
                bitmap.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(output);

        final int color = 0xff424242;
        final Paint paint = new Paint();
        final Rect rect = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());
        final RectF rectF = new RectF(rect);
        final float roundPx = 12;

        paint.setAntiAlias(true);
        canvas.drawARGB(0, 0, 0, 0);
        paint.setColor(color);
        canvas.drawRoundRect(rectF, roundPx, roundPx, paint);

        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
        canvas.drawBitmap(bitmap, rect, rect, paint);

        return output;
    }



    //public TextView showCalender(Context context,TextView tv) {
    public String showCalender(Context context) {
        //here we are calling calendar
        final Calendar c = Calendar.getInstance();
        int mYear = c.get(Calendar.YEAR);
        int mMonth = c.get(Calendar.MONTH);
        int mDay = c.get(Calendar.DAY_OF_MONTH);
        //tv.setText("");


        DatePickerDialog datePickerDialog = new DatePickerDialog(context,
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {
                        //tv_startdate.setText(dayOfMonth + "-" + (monthOfYear + 1) + "-" + year);
                        startdate = dayOfMonth + "-" + (monthOfYear + 1) + "-" + year;
                    }
                }, mYear, mMonth, mDay);
        datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
        datePickerDialog.show();
        //tv.setText(startdate);
        //return tv;
        return startdate;
    }

   /* public String getAddressFromLatlong(Context context,double lat, double longi) {

        Geocoder geocoder = new Geocoder(context, Locale.getDefault());
        if (InternetDetect.isConnected(getApplicationContext())) {
            //locationPresenter.runGeocodeAPI(latLng.latitude + "," + latLng.longitude, AppConstants.GEOCODE_API_KEY);
            List<Address> addresses = new ArrayList<>();
            try {
                addresses = geocoder.getFromLocation(lat, longi, 1);

                //latitude= latLng.latitude;
                //longitude = latLng.longitude;
            } catch (IOException e) {
                e.printStackTrace();
            }

                Address address = addresses.get(0);


            if (address != null) {
                sb = new StringBuilder();
                if(address.getMaxAddressLineIndex()>0){
                    for (int i = 0; i < address.getMaxAddressLineIndex(); i++) {
                        sb.append(address.getAddressLine(i) + "\n");
                    }
                }else {
                    try {
                        sb.append(address.getAddressLine(0));
                    } catch (Exception ignored) {}
                }
            }
        }
        return sb.toString();

    }*/

    public String meterToKM(float meters) {
        float km;
        km = meters / 1000;
        return format(km);
    }

    public static String format(Number n) {
        NumberFormat format = DecimalFormat.getInstance();
        format.setRoundingMode(RoundingMode.FLOOR);
        format.setMinimumFractionDigits(0);
        format.setMaximumFractionDigits(2);
        return format.format(n);
    }

    public static Bitmap decodeSampledBitmapFromResource(String path, int reqWidth, int reqHeight) {

        // First decode with inJustDecodeBounds=true to check dimensions
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(path, options);

        // Calculate inSampleSize
        options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);

        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false;
        return BitmapFactory.decodeFile(path, options);
    }

    public static int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {

            // Calculate ratios of height and width to requested height and
            // width
            final int heightRatio = Math.round((float) height / (float) reqHeight);
            final int widthRatio = Math.round((float) width / (float) reqWidth);

            // Choose the smallest ratio as inSampleSize value, this will
            // guarantee
            // a final image with both dimensions larger than or equal to the
            // requested height and width.
            inSampleSize = heightRatio < widthRatio ? heightRatio : widthRatio;
        }
        return inSampleSize;
    }

    public Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
    }

    public String getPath(Uri uri, Context context) {
        String[] projection = {MediaStore.Images.Media.DATA};
        Cursor cursor = context.getContentResolver().query(uri, projection, null, null, null);
        if (cursor == null) return null;
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        String s = cursor.getString(column_index);
        cursor.close();
        return s;
    }

    public String changeDateYMDtoDMY(String date) {
        Date myDate;
        String outputDateStr = "";
        try {
            DateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd");
            DateFormat outputFormat = new SimpleDateFormat("dd-MM-yyyy");
            myDate = inputFormat.parse(date);
            outputDateStr = outputFormat.format(myDate);
            System.out.println(outputDateStr);
        } catch (Exception e) {
            //java.text.ParseException: Unparseable date: Geting error
            System.out.println("Excep" + e);
        }
        return outputDateStr;
    }
    public  String timeChange(String startdate){
        String timeChange="";
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        try{
            Date date3 = sdf.parse(startdate);
            //new format
            SimpleDateFormat sdf2 = new SimpleDateFormat("hh.mm aa");
            //formatting the given time to new format with AM/PM
            System.out.println("Given time in AM/PM: "+sdf2.format(date3));
        }catch(ParseException e){
            e.printStackTrace();
        }
        return  timeChange;
    }

    public String onlyTime(String date){
        String timeOnly="";
        try {
            String fmt = "yyyy-MM-dd hh:mm:ss ";
            TimeZone utc = TimeZone.getTimeZone("etc/UTC");
            DateFormat df = new SimpleDateFormat(fmt,Locale.ENGLISH);
            df.setTimeZone(utc);
            Date dt = df.parse(date);

            DateFormat tdf = new SimpleDateFormat("hh:mm:ss a");
            DateFormat dfmt = new SimpleDateFormat("MM/dd/yyyy");


             timeOnly = tdf.format(dt);
            String dateOnly = dfmt.format(dt);
        }catch (Exception e){
            e.printStackTrace();
            timeOnly="";
        }
        return    timeOnly;
    }

    //2020-07-03 07:29:49
    public String changeDatemonthName(String date) {
        Date myDate;
        String outputDateStr = "";
        try {
            DateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH);
            DateFormat outputFormat = new SimpleDateFormat("hh:mm a");
            inputFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
            myDate = inputFormat.parse(date);
            outputDateStr = outputFormat.format(myDate);
            System.out.println(outputDateStr);
        } catch (Exception e) {
            //java.text.ParseException: Unparseable date: Geting error
            System.out.println("Excep" + e);
        }
        return outputDateStr;
    }
    @RequiresApi(api = Build.VERSION_CODES.O)
    public String dateJava(String date){
        String dateString="";
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        LocalDateTime dateTime = LocalDateTime.parse(date, formatter);
        DateTimeFormatter formatter2 = DateTimeFormatter.ofPattern("dd MMM yyyy");
        //System.out.println(dateTime.format(formatter2));
        dateString=dateTime.format(formatter2);
        return dateString;
    }
    public String changeDateFormat(String currentFormat,String requiredFormat,String dateString){
        String result="";
        if (Strings.isEmptyOrWhitespace(dateString)){
            return result;
        }
        SimpleDateFormat formatterOld = new SimpleDateFormat(currentFormat, Locale.getDefault());
        SimpleDateFormat formatterNew = new SimpleDateFormat(requiredFormat, Locale.getDefault());
        Date date=null;
        try {
            date = formatterOld.parse(dateString);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        if (date != null) {
            result = formatterNew.format(date);
        }
        return result;
    }
    public String changeDateName(String dateStr) {
        Date myDate;
        String outputDateStr = "";
        try {
            SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss", Locale.ENGLISH);
            df.setTimeZone(TimeZone.getTimeZone("UTC"));
            myDate = df.parse(dateStr);
            df.setTimeZone(TimeZone.getDefault());
            outputDateStr= df.format(myDate);
            Log.d("dateChange",outputDateStr);
        } catch (Exception e) {
            //java.text.ParseException: Unparseable date: Geting error
            System.out.println("Excep" + e);
        }
        return outputDateStr;
    }


    public static String formatToYesterdayOrToday(String date) throws ParseException {
        String outputDateStr = "";
       // Date dateTime = new SimpleDateFormat("EEE hh:mma MMM d, yyyy").parse(date);
        DateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss",Locale.ENGLISH);
        //DateFormat inputFormat = new SimpleDateFormat("MMM dd, yyyy HH:mm a",Locale.ENGLISH);
        DateFormat outputFormat = new SimpleDateFormat("dd/MM/yyyy");
        inputFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
         // Date myDate = inputFormat.parse(date);
        Date  dateTime = inputFormat.parse(date);
        inputFormat.setTimeZone(TimeZone.getDefault());
//        Date newdateTime = new SimpleDateFormat("yyy-mm-dd hh:mma").parse(date);
        outputDateStr = outputFormat.format(dateTime);
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(dateTime);
        Calendar today = Calendar.getInstance();
        Calendar yesterday = Calendar.getInstance();
        yesterday.add(Calendar.DATE, -1);
        DateFormat timeFormatter = new SimpleDateFormat("hh:mm aa");
        //timeFormatter.setTimeZone(TimeZone.getTimeZone("UTC"));
       // inputFormat.setTimeZone(TimeZone.getDefault());

        if (calendar.get(Calendar.YEAR) == today.get(Calendar.YEAR) && calendar.get(Calendar.DAY_OF_YEAR) == today.get(Calendar.DAY_OF_YEAR)) {
            return  timeFormatter.format(dateTime);
        } else if (calendar.get(Calendar.YEAR) == yesterday.get(Calendar.YEAR) && calendar.get(Calendar.DAY_OF_YEAR) == yesterday.get(Calendar.DAY_OF_YEAR)) {
            //return "Yesterday " + timeFormatter.format(dateTime);
            return "Yesterday " ;
        } else {
            return outputDateStr;
        }
    }
    public static String formatTodayYesterday(String date) throws ParseException {
        String outputDateStr = "";
        // Date dateTime = new SimpleDateFormat("EEE hh:mma MMM d, yyyy").parse(date);
        DateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss",Locale.ENGLISH);
        //DateFormat inputFormat = new SimpleDateFormat("MMM dd, yyyy HH:mm a",Locale.ENGLISH);
        DateFormat outputFormat = new SimpleDateFormat("dd/MM/yyyy");
        inputFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
        // Date myDate = inputFormat.parse(date);
        Date  dateTime = inputFormat.parse(date);
        inputFormat.setTimeZone(TimeZone.getDefault());
//        Date newdateTime = new SimpleDateFormat("yyy-mm-dd hh:mma").parse(date);
        outputDateStr = outputFormat.format(dateTime);
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(dateTime);
        Calendar today = Calendar.getInstance();
        Calendar yesterday = Calendar.getInstance();
        yesterday.add(Calendar.DATE, -1);
        DateFormat timeFormatter = new SimpleDateFormat("hh:mm aa");
        //timeFormatter.setTimeZone(TimeZone.getTimeZone("UTC"));
        // inputFormat.setTimeZone(TimeZone.getDefault());

        if (calendar.get(Calendar.YEAR) == today.get(Calendar.YEAR) && calendar.get(Calendar.DAY_OF_YEAR) == today.get(Calendar.DAY_OF_YEAR)) {
            //return  timeFormatter.format(dateTime);
            return  "Today";
        } else if (calendar.get(Calendar.YEAR) == yesterday.get(Calendar.YEAR) && calendar.get(Calendar.DAY_OF_YEAR) == yesterday.get(Calendar.DAY_OF_YEAR)) {
            //return "Yesterday " + timeFormatter.format(dateTime);
            return "Yesterday " ;
        } else {
            return outputDateStr;
        }
    }
    public static String formatToShowOnlyTime(String date) throws ParseException {
        String outputDateStr = "";
        // Date dateTime = new SimpleDateFormat("EEE hh:mma MMM d, yyyy").parse(date);
        DateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss",Locale.ENGLISH);
        //DateFormat inputFormat = new SimpleDateFormat("MMM dd, yyyy HH:mm a",Locale.ENGLISH);
        DateFormat outputFormat = new SimpleDateFormat("dd/MM/yyyy");
        inputFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
        // Date myDate = inputFormat.parse(date);
        Date  dateTime = inputFormat.parse(date);
        inputFormat.setTimeZone(TimeZone.getDefault());
//        Date newdateTime = new SimpleDateFormat("yyy-mm-dd hh:mma").parse(date);
        outputDateStr = outputFormat.format(dateTime);
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(dateTime);
        Calendar today = Calendar.getInstance();
        Calendar yesterday = Calendar.getInstance();
        yesterday.add(Calendar.DATE, -1);
        DateFormat timeFormatter = new SimpleDateFormat("hh:mm aa");
        //timeFormatter.setTimeZone(TimeZone.getTimeZone("UTC"));
        // inputFormat.setTimeZone(TimeZone.getDefault());

        if (calendar.get(Calendar.YEAR) == today.get(Calendar.YEAR) && calendar.get(Calendar.DAY_OF_YEAR) == today.get(Calendar.DAY_OF_YEAR)) {
            return  timeFormatter.format(dateTime);
        } else if (calendar.get(Calendar.YEAR) == yesterday.get(Calendar.YEAR) && calendar.get(Calendar.DAY_OF_YEAR) == yesterday.get(Calendar.DAY_OF_YEAR)) {
            return  timeFormatter.format(dateTime);
        } else {
            return  timeFormatter.format(dateTime);
        }
    }
    public String getPostUploadDate(String ourDate)
    {
        try
        {
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            formatter.setTimeZone(TimeZone.getTimeZone("UTC"));
            Date value = formatter.parse(ourDate);

            SimpleDateFormat dateFormatter = new SimpleDateFormat("MMM dd | hh:ss a"); //this format changeable
            dateFormatter.setTimeZone(TimeZone.getDefault());
            ourDate = dateFormatter.format(value);

            //Log.d("ourDate", ourDate);
        }
        catch (Exception e)
        {
            ourDate = "00-00-0000 00:00";
        }
        return ourDate;
    }
    public String getDate(String ourDate)
    {
        try
        {
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            formatter.setTimeZone(TimeZone.getTimeZone("UTC"));
            Date value = formatter.parse(ourDate);

            SimpleDateFormat dateFormatter = new SimpleDateFormat("dd MMM, yyyy"); //this format changeable
            dateFormatter.setTimeZone(TimeZone.getDefault());
            ourDate = dateFormatter.format(value);

            //Log.d("ourDate", ourDate);
        }
        catch (Exception e)
        {
            ourDate = "00-00-0000 00:00";
        }
        return ourDate;
    }
    public String changeDateDMYtoYMD(String date) {

        Date myDate;
        String outputDateStr = "";
        try {
            DateFormat inputFormat = new SimpleDateFormat("dd/MM/yyyy");
            DateFormat outputFormat = new SimpleDateFormat("yyyy-MM-dd");
            myDate = inputFormat.parse(date);
            outputDateStr = outputFormat.format(myDate);
            System.out.println(outputDateStr);

        } catch (Exception e) {
            //java.text.ParseException: Unparseable date: Geting error
            System.out.println("Excep" + e);
        }
        return outputDateStr;
    }
    public String dateChangeYMDtoDMY(String date){
        Date myDate;
        String outputDateStr="";
        try{
            DateFormat inputFormat =new SimpleDateFormat("yyyy-MM-dd");
            DateFormat outputFormat=new SimpleDateFormat("dd/MM/yyyy");
            myDate=inputFormat.parse(date);
            outputDateStr=outputFormat.format(myDate);

        }catch (Exception e){
            e.printStackTrace();
        }
        return outputDateStr;
    }

    public final static boolean isValidEmail(CharSequence target) {
        if (target == null) {
            return false;
        } else {
            return android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
        }
    }
    public static Bitmap convert(String base64Str) throws IllegalArgumentException
    {
        byte[] decodedBytes = Base64.decode(
                base64Str.substring(base64Str.indexOf(",")  + 1),
                Base64.DEFAULT
        );

        return BitmapFactory.decodeByteArray(decodedBytes, 0, decodedBytes.length);
    }

    public static String convert(Bitmap bitmap)
    {
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, outputStream);

        return Base64.encodeToString(outputStream.toByteArray(), Base64.DEFAULT);
    }
    public String BitMapToString(Bitmap bitmap){
        ByteArrayOutputStream baos=new  ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG,50, baos);
        byte [] b=baos.toByteArray();
        String temp=Base64.encodeToString(b, Base64.DEFAULT);
        return temp;
    }
    public String BitMapToStringBlur(Bitmap bitmap){
        ByteArrayOutputStream baos=new  ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG,10, baos);
        byte [] b=baos.toByteArray();
        String temp=Base64.encodeToString(b, Base64.DEFAULT);
        return temp;
    }
    public Bitmap convertBase64ToBitmap(String b64) {
        byte[] imageAsBytes = Base64.decode(b64.getBytes(), Base64.DEFAULT);
        return BitmapFactory.decodeByteArray(imageAsBytes, 0, imageAsBytes.length);
    }
    public Bitmap getBitmap(String path) {
        Bitmap bitmap=null;
        try {
            File f= new File(path);
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inPreferredConfig = Bitmap.Config.ARGB_8888;
            bitmap = BitmapFactory.decodeStream(new FileInputStream(f), null, options);
            //image.setImageBitmap(bitmap);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return bitmap ;
    }
    public HashMap<String, String> getHashMapObject(String... nameValuePair) {
        HashMap<String, String> hashMap = null;

        if (null != nameValuePair && nameValuePair.length % 2 == 0) {

            hashMap = new HashMap<>();

            int i = 0;
            while (i < nameValuePair.length) {
                hashMap.put(nameValuePair[i], nameValuePair[i + 1]);
                i += 2;
            }

        }

        return hashMap;
    }
    public static Bitmap getResizedBitmap(Bitmap bm, int newWidth, int newHeight) {
        int width = bm.getWidth();
        int height = bm.getHeight();
        float scaleWidth = ((float) newWidth) / width;
        float scaleHeight = ((float) newHeight) / height;
        // CREATE A MATRIX FOR THE MANIPULATION
        Matrix matrix = new Matrix();
        // RESIZE THE BIT MAP
        matrix.postScale(scaleWidth, scaleHeight);

        // "RECREATE" THE NEW BITMAP
        Bitmap resizedBitmap = Bitmap.createBitmap(
                bm, 0, 0, width, height, matrix, false);
        bm.recycle();
        return resizedBitmap;
    }
    public static Bitmap getBitmapFromURL(String src) {
        try {
            URL url = new URL(src);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            Bitmap myBitmap = BitmapFactory.decodeStream(input);
            return myBitmap;
        } catch (IOException e) {
            // Log exception
            e.printStackTrace();
            return null;
        }catch (OutOfMemoryError e){
            e.printStackTrace();
            return null;
        }
    }


    public static JsonObject getJsonMapObject(String... nameValuePair) {
        JsonObject jsonObject = null;

        if (null != nameValuePair && nameValuePair.length % 2 == 0) {

            jsonObject = new JsonObject();

            int i = 0;
            while (i < nameValuePair.length) {
                jsonObject.addProperty(nameValuePair[i], nameValuePair[i + 1]);
                i += 2;
            }

        }

        return jsonObject;
    }

}

