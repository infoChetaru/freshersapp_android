package com.chetaru.FreshersApp.view.callback;

import com.chetaru.FreshersApp.service.model.UserViewPost;

public interface PaginationListenerCallback {
    public void onPaginationClick(UserViewPost viewPost, Boolean likeValue, int position, String value);
    public void onPaginationLongClick(UserViewPost viewPost,int position,String postUserDetail);
    public void autoLinkListener(int linkType,String autoLink,String matchedText,UserViewPost viewPost);
    public  void ResetClickListener(int position ,String changeLocation);
}
