package com.chetaru.FreshersApp.view.ui.Fragment;

import android.content.Context;
import android.os.Bundle;

import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.chetaru.FreshersApp.R;
import com.chetaru.FreshersApp.service.model.MyProfileResponse;
import com.chetaru.FreshersApp.service.model.UserViewPost;
import com.chetaru.FreshersApp.service.retrofit.BaseRequest;
import com.chetaru.FreshersApp.service.retrofit.Functions;
import com.chetaru.FreshersApp.service.retrofit.RequestReciever;
import com.chetaru.FreshersApp.utility.SessionParam;
import com.chetaru.FreshersApp.utility.Utility;
import com.chetaru.FreshersApp.utility.Validations;
import com.google.gson.Gson;
import com.google.gson.JsonObject;

import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import de.hdodenhof.circleimageview.CircleImageView;
import im.delight.android.location.SimpleLocation;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link UserProfileFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class UserProfileFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private static final String VIEWPOST = "viewPost";

    // TODO: Rename and change types of parameters
    private Integer personId;
    private Integer mParam2;

    //initlize view Bind
    private Unbinder unbinder;
    final int sdk = android.os.Build.VERSION.SDK_INT;



    //class initligation
    Utility utility;
    SessionParam sessionParam;
    //get location
    private SimpleLocation location;
    //assign a layout id class
    @BindView(R.id.user_profile_address)
    TextView userProfileAddress;

    @BindView(R.id.user_profile_logo)
    CircleImageView userProfileLogo;
    @BindView(R.id.user_profile_name_txt)
    TextView userProfileName;
    @BindView(R.id.user_profile_date_of_birth_et)
    TextView userProfileDateOfBirth;

    @BindView(R.id.user_profile_bio)
    TextView userProfileBio;
    @BindView(R.id.user_profile_male_image)
    ImageView maleIconImage;
    @BindView(R.id.user_profile_female_image)
    ImageView femaleIconImage;
    @BindView(R.id.user_profile_other_image)
    ImageView otherIconImage;
    @BindView(R.id.user_profile_male_txt)
            TextView maleTextView;
    @BindView(R.id.user_profile_female_txt)
            TextView femaleTextView;
    @BindView(R.id.user_profile_other_txt)
            TextView otherTextView;

    //call api Request
    BaseRequest baseRequest;
    //value initilization
   // String personId;
    Integer gender;
    //model instance
    MyProfileResponse myProfile;
    otherProfileListener mListener;
    String profileImageString="";



    public UserProfileFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment UserProfileFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static UserProfileFragment newInstance(UserViewPost viewPost,Integer param1, Integer param2) {
        UserProfileFragment fragment = new UserProfileFragment();
        Bundle args = new Bundle();
        args.putSerializable(VIEWPOST,viewPost);
        args.putInt(ARG_PARAM1, param1);
        args.putInt(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            personId = getArguments().getInt(ARG_PARAM1);
            mParam2 = getArguments().getInt(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_user_profile, container, false);
        unbinder = ButterKnife.bind(this, view);
        utility=new Utility();
        sessionParam=new SessionParam(getContext());
        location=new SimpleLocation(getContext());
        maleIconImage.setVisibility(View.GONE);
        femaleIconImage.setVisibility(View.GONE);
        otherIconImage.setVisibility(View.GONE);
        maleTextView.setVisibility(View.GONE);
        femaleTextView.setVisibility(View.GONE);
        otherTextView.setVisibility(View.GONE);
        userProfileLogo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mListener.otherProfileClick(view,profileImageString);
            }
        });

        if (personId!=null)
        getUserProfile(personId);
        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof otherProfileListener) {
            mListener = (otherProfileListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement TagChildFragment");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    private void selectOther(){
        gender=3;
        if (sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
            otherIconImage.setBackgroundDrawable(ContextCompat.getDrawable(getContext(), R.drawable.other));
            maleIconImage.setBackgroundDrawable(ContextCompat.getDrawable(getContext(), R.drawable.malegray));
            femaleIconImage.setBackgroundDrawable(ContextCompat.getDrawable(getContext(), R.drawable.femalegray));
        } else {
            otherIconImage.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.other));
            maleIconImage.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.malegray));
            femaleIconImage.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.femalegray));
        }
    }

    private void selectMale() {
        gender=1;
        if (sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
            otherIconImage.setBackgroundDrawable(ContextCompat.getDrawable(getContext(), R.drawable.other_gray));
            maleIconImage.setBackgroundDrawable(ContextCompat.getDrawable(getContext(), R.drawable.male));
            femaleIconImage.setBackgroundDrawable(ContextCompat.getDrawable(getContext(), R.drawable.femalegray));
        } else {
            otherIconImage.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.other_gray));
            maleIconImage.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.male));
            femaleIconImage.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.femalegray));
        }
    }
    private void selectFemale() {
        gender=2;
        if (sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
            otherIconImage.setBackgroundDrawable(ContextCompat.getDrawable(getContext(), R.drawable.other_gray));
            maleIconImage.setBackgroundDrawable(ContextCompat.getDrawable(getContext(), R.drawable.malegray));
            femaleIconImage.setBackgroundDrawable(ContextCompat.getDrawable(getContext(), R.drawable.female));
        } else {
            otherIconImage.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.other_gray));
            maleIconImage.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.malegray));
            femaleIconImage.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.female));
        }
    }

    private void getUserProfile(int personId) {
        baseRequest = new BaseRequest(getContext());
        baseRequest.setBaseRequestListner(new RequestReciever() {
            @Override
            public void onSuccess(int requestCode, String Json, Object object) {
                try {

                    Gson gson = new Gson();
                    JSONObject jsonObject = new JSONObject(Json);
                    JSONObject response= (JSONObject) jsonObject.get("data");
                  
                    myProfile = gson.fromJson(response.toString(), MyProfileResponse.class);
                    profileImageString=myProfile.getProfileImage();
                    Glide.with(getContext()).load(myProfile.getProfileImage()).apply(new RequestOptions().centerCrop().placeholder(R.drawable.profile_pics)).into(userProfileLogo);
                    userProfileAddress.setText(myProfile.getLocaleName());
                    userProfileName.setText(myProfile.getName());
                    userProfileBio.setText(myProfile.getBio());
                    userProfileDateOfBirth.setText(myProfile.getDob());
                    gender= myProfile.getGender();
                   // latitude= Double.parseDouble(myProfile.getLatitude());
                   // longitude= Double.parseDouble(myProfile.getLongitude());
                    if (gender==1){
                        maleIconImage.setVisibility(View.VISIBLE);
                        maleTextView.setVisibility(View.VISIBLE);
                        selectMale();
                    }else if (gender==2){
                        femaleIconImage.setVisibility(View.VISIBLE);
                        femaleTextView.setVisibility(View.VISIBLE);
                        selectFemale();
                    }else if (gender==3){
                        otherIconImage.setVisibility(View.VISIBLE);
                        otherTextView.setVisibility(View.VISIBLE);
                        selectOther();
                    }

                }catch (Exception e){
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int requestCode, String errorCode, String message) {
               // utility.showToast(getContext(),message);
            }

            @Override
            public void onNetworkFailure(int requestCode, String message) {
                utility.showToast(getContext(),message);
            }
        });

        JsonObject main_object = new JsonObject();
        main_object.addProperty("userId", personId);

        /*JsonObject object = Functions.getClient().getJsonMapObject(
                "personId",personId);*/
        baseRequest.callAPIPostWOLoader(1, main_object, getString(R.string.api_getUserProfile));


    }

    @Override
    public void onPause() {
        super.onPause();
       /* try {
            if (myProfile.getId() == null) {
                getUserProfile(personId);
            }
        }catch (Exception e){
            e.printStackTrace();
        }*/
    }

    @Override public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }
   public interface otherProfileListener{
        public void otherProfileClick(View view,String imageString);
    }
}
