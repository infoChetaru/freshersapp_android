package com.chetaru.FreshersApp.view.adapter;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.chetaru.FreshersApp.R;
import com.chetaru.FreshersApp.service.model.LikeByUser;
import com.chetaru.FreshersApp.service.model.LocaleData;
import com.chetaru.FreshersApp.utility.SessionParam;
import com.chetaru.FreshersApp.utility.Validations;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;

public class LocaleAdapter extends RecyclerView.Adapter<LocaleAdapter.ViewHolder> implements Filterable {

    List<LocaleData> localeList;
    private Context mContext;
    private List<LocaleData>  ListFiltered;
    int selectedPosition=-1;
    SessionParam sessionParam;
    int userId;
    int localeId;
    String  DefaultLocaleName="";
    public LocaleListener mListener;

    public class ViewHolder extends RecyclerView.ViewHolder{

        @BindView(R.id.locale_name_tv)
        TextView localeName;
        @BindView(R.id.locale_location_tv)
        TextView locationTxt;
        @BindView(R.id.locale_layout)
        CardView localeLayout;
        @BindView(R.id.location_image)
        ImageView locationLogo;

        public ViewHolder(@NonNull View view) {
            super(view);
            ButterKnife.bind(this,view);
        }
    }
    public LocaleAdapter(String defaultName,List<LocaleData> localeList, Context mContext,LocaleListener mListener) {
        this.DefaultLocaleName = defaultName;
        this.localeList = localeList;
        this.ListFiltered=localeList;
        this.mContext = mContext;
        this.sessionParam=new SessionParam(mContext);
        localeId=sessionParam.localeId;
        this.mListener=mListener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.locale_list,parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        try {
            holder.localeName.setText(localeList.get(position).getName());
            holder.locationTxt.setText(localeList.get(position).getCountry());
               if (localeId==(localeList.get(position).getId())) {
                   selectLocale(holder);
               } else {
                   unSelectLocale(holder);
               }
               /*if (!Validations.isEmptyString(DefaultLocaleName)){
                   if (DefaultLocaleName.trim().equalsIgnoreCase(localeList.get(position).getName().trim())){
                       selectLocale(holder);
                   }
               }*/
           
            holder.locationTxt.setText(localeList.get(position).getCountry());
            holder.localeLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    selectLocale(holder);
                    mListener.updateLocale(localeList.get(position));
                }
            });
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private void selectLocale(ViewHolder mholder) {
        final int sdk = android.os.Build.VERSION.SDK_INT;
        Drawable img = mContext.getResources().getDrawable( R.drawable.location_purple);
        //Picasso.with(mContext).load(R.drawable.location_purple).into(mholder.locationLogo);
        Glide.with(mContext).load(R.drawable.location_purple).into(mholder.locationLogo);
        if(sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
            mholder.localeLayout.setBackground(ContextCompat.getDrawable(mContext, R.drawable.rounded_corner_white_solid));
            mholder.localeName.setTextColor(ContextCompat.getColor(mContext, R.color.colorPrimary) );
            mholder.locationTxt.setTextColor(ContextCompat.getColor(mContext, R.color.colorPrimary) );
            //mholder.locationTxt.setCompoundDrawables(img,null,null,null);
           // mholder.locationTxt.setCompoundDrawablesWithIntrinsicBounds( img, null, null, null);

        } else {
            mholder.localeLayout.setBackground(ContextCompat.getDrawable(mContext, R.drawable.rounded_corner_white_solid));
            mholder.localeName.setTextColor(ContextCompat.getColor(mContext, R.color.colorPrimary) );
            mholder.locationTxt.setTextColor(ContextCompat.getColor(mContext, R.color.colorPrimary) );
            //mholder.locationTxt.setCompoundDrawables(img,null,null,null);
            //mholder.locationTxt.setCompoundDrawablesWithIntrinsicBounds( img, null, null, null);
        }
    }


    private void unSelectLocale(ViewHolder mholder) {
        final int sdk = android.os.Build.VERSION.SDK_INT;
        Drawable img = mContext.getResources().getDrawable( R.drawable.locationwhite);
        //Picasso.with(mContext).load(R.drawable.locationwhite).into(mholder.locationLogo);
        Glide.with(mContext).load(R.drawable.locationwhite).into(mholder.locationLogo);

        if(sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
            mholder.localeLayout.setBackground(ContextCompat.getDrawable(mContext, R.drawable.white_per_corner_solid));
            //mholder.localeLayout.setBackgroundDrawable(ContextCompat.getDrawable(mContext, R.drawable.primary_rounded_bg) );
            mholder.localeName.setTextColor(ContextCompat.getColor(mContext, R.color.white) );
            mholder.locationTxt.setTextColor(ContextCompat.getColor(mContext, R.color.white) );
            //mholder.locationTxt.setCompoundDrawables(img,null,null,null);
            //mholder.locationTxt.setCompoundDrawablesWithIntrinsicBounds( img, null, null, null);

        } else {
            mholder.localeLayout.setBackgroundDrawable(ContextCompat.getDrawable(mContext, R.drawable.white_per_corner_solid) );
            mholder.localeName.setTextColor(ContextCompat.getColor(mContext, R.color.white) );
            mholder.locationTxt.setTextColor(ContextCompat.getColor(mContext, R.color.white) );
            //mholder.locationTxt.setCompoundDrawables(img,null,null,null);
            //mholder.locationTxt.setCompoundDrawablesWithIntrinsicBounds( img, null, null, null);


        }
    }


    @Override
    public int getItemCount() {
        return localeList.size();
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    localeList = ListFiltered;
                } else {
                    List<LocaleData> filteredList = new ArrayList<>();
                    for (LocaleData row : ListFiltered) {

                        // name match condition. this might differ depending on your requirement
                        // here we are looking for name or phone number match
                        if (row.getName().toLowerCase().contains(charString.toLowerCase()) ) {
                            filteredList.add(row);
                        }
                    }

                    localeList = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = localeList;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                localeList = (ArrayList<LocaleData>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }

   public interface LocaleListener{
        public void updateLocale(LocaleData locale);
    }




}
