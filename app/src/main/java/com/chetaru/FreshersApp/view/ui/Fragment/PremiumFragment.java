package com.chetaru.FreshersApp.view.ui.Fragment;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;

import android.preference.PreferenceManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

import com.chetaru.FreshersApp.R;
import com.chetaru.FreshersApp.service.retrofit.ApiClient;
import com.chetaru.FreshersApp.service.retrofit.BaseRequest;
import com.chetaru.FreshersApp.service.retrofit.Functions;
import com.chetaru.FreshersApp.service.retrofit.RequestReciever;
import com.chetaru.FreshersApp.utility.BaseActivity;
import com.chetaru.FreshersApp.utility.SessionParam;
import com.chetaru.FreshersApp.utility.Utility;
import com.chetaru.FreshersApp.utility.Validations;
import com.chetaru.FreshersApp.view.customView.GravityCompoundDrawable;
import com.chetaru.FreshersApp.view.ui.Activity.HomeActivity;
import com.chetaru.FreshersApp.view.ui.Activity.RootActivity;
import com.google.gson.Gson;
import com.google.gson.JsonObject;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link PremiumFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class PremiumFragment extends Fragment implements View.OnClickListener{
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private static final String ARG_PARAM3 = "param3";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private String titleBack;

    Context mContext;
    Button logoutButton;
    Utility utility;
    SessionParam sessionParam;
    String tokenId="";
    BaseRequest baseRequest;
    Button upgradeButton,cancelButton;
    TextView locationMsg;

    GoPremiumListener mListener;
    public PremiumFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment PremiumFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static PremiumFragment newInstance(String param1, String param2,String titleBack) {
        PremiumFragment fragment = new PremiumFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        args.putString(ARG_PARAM3, titleBack);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
            titleBack = getArguments().getString(ARG_PARAM3);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_premium, container, false);

        sessionParam=new SessionParam(getContext());
        tokenId=sessionParam.token;
        utility=new Utility();
        utility.getProgerss(getContext());
        getDefaultLocale();
        upgradeButton=view.findViewById(R.id.upgrade_button_go_premium);
        cancelButton=view.findViewById(R.id.cancel_button_go_premium);
        TextView firstTxt=view.findViewById(R.id.first_bullet_msg);
        TextView sceondTxt=view.findViewById(R.id.sceond_bullet_msg);
        TextView thirdTxt=view.findViewById(R.id.third_bullet_msg);
        TextView fourthTxt=view.findViewById(R.id.fourth_bullet_msg);
        TextView fifthTxt=view.findViewById(R.id.fifth_bullet_msg);
         locationMsg=view.findViewById(R.id.location_msg_text);

        upgradeButton.setOnClickListener(this);
        cancelButton.setOnClickListener(this);

        if (Validations.isEmptyString(mParam2)){

            mParam2="You must be a premium user to use this feature.";
        }
        firstTxt.setText("See who has liked your Post!");
        sceondTxt.setText("See posts that you have previously liked!");
        thirdTxt.setText("See other user's previous posts!");
        fourthTxt.setText("See the top liked image from the last 24 hours every day!");
        fifthTxt.setText("change your location!");
        Drawable innerDrawable = getResources().getDrawable(R.drawable.custom_bullet);

        GravityCompoundDrawable gravityDrawable = new GravityCompoundDrawable(innerDrawable);
// NOTE: next 2 lines are important!
        //innerDrawable.setBounds(0, 0, innerDrawable.getIntrinsicWidth(), innerDrawable.getIntrinsicHeight());
       // gravityDrawable.setBounds(0, 0, innerDrawable.getIntrinsicWidth(), innerDrawable.getIntrinsicHeight());
        innerDrawable.setBounds(0, 0, innerDrawable.getIntrinsicWidth(), innerDrawable.getIntrinsicHeight());
        gravityDrawable.setBounds(0, innerDrawable.getIntrinsicHeight(), innerDrawable.getIntrinsicWidth(),0);
        firstTxt.setCompoundDrawables(gravityDrawable, null, null, null);
        sceondTxt.setCompoundDrawables(gravityDrawable, null, null, null);
        thirdTxt.setCompoundDrawables(gravityDrawable, null, null, null);
        fourthTxt.setCompoundDrawables(gravityDrawable, null, null, null);
        fifthTxt.setCompoundDrawables(gravityDrawable, null, null, null);
        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof GoPremiumListener) {
            mListener = (GoPremiumListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement TagChildFragment");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onClick(View view) {
        if (Validations.isEmptyString(titleBack)){
            titleBack="Upgrade To Premium";
        }

        switch (view.getId()){
            case R.id.upgrade_button_go_premium:
                mListener.goPremiumButton("upgrade",mParam2,titleBack);
                break;
            case R.id.cancel_button_go_premium:
                if (!Validations.isEmptyString(mParam1)) {
                    if (mParam1.equals("profileBack")) {
                        mListener.goPremiumButton("profileBack",mParam2,titleBack);
                    } else  if (mParam1.equals("upgradeBack")){
                        mListener.goPremiumButton("upgradeBack",mParam2,titleBack);

                    }else if (mParam1.equals("settingBack")) {
                        mListener.goPremiumButton("settingBack",mParam2,titleBack);
                    }else if(mParam1.equals("HomeBack")){
                        mListener.goPremiumButton("HomeBack",mParam2,titleBack);

                    } else {
                        mListener.goPremiumButton("cancel",mParam2,titleBack);
                    }
                }else {
                    mListener.goPremiumButton("cancel",mParam2,titleBack);
                }
                break;
        }
    }
    private void getDefaultLocale() {
        baseRequest = new BaseRequest(getContext());
        baseRequest.setBaseRequestListner(new RequestReciever() {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onSuccess(int requestCode, String Json, Object object) {
                try {

                    Gson gson = new Gson();
                    JSONObject jsonObject = new JSONObject(Json);
                    String newLocation= jsonObject.getString("data");
                    if (!Validations.isEmptyString(newLocation)) {
                      //  defaultLocaleName = localeName;
                        locationMsg.setText("*"+newLocation.toString()+ " is currently the only available location" );

                    }else {
                        locationMsg.setText("");
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int requestCode, String errorCode, String message) {
                utility.showToast(getContext(),message);
            }

            @Override
            public void onNetworkFailure(int requestCode, String message) {

                utility.showToast(getContext(), message);
            }
        });

        JsonObject object = Functions.getClient().getJsonMapObject(
                "", "");
        baseRequest.callAPIPostWOLoader(1, object, getString(R.string.api_getDefaultLocale));

    }

    public interface GoPremiumListener{
        void goPremiumButton(String premium,String textValue,String titleBack);
    }
}
