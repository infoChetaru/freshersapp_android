package com.chetaru.FreshersApp.view.adapter;

import android.animation.ObjectAnimator;
import android.content.Context;
import android.graphics.Bitmap;
import android.os.Build;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.chetaru.FreshersApp.R;
import com.chetaru.FreshersApp.service.model.UserViewPost;
import com.chetaru.FreshersApp.utility.SessionParam;
import com.chetaru.FreshersApp.utility.Utility;
import com.chetaru.FreshersApp.utility.Validations;
import com.chetaru.FreshersApp.view.callback.PaginationListenerCallback;
import com.chetaru.FreshersApp.view.customView.BlurCustom;
import com.chetaru.FreshersApp.view.customView.ExpandableTextView;
import com.jackandphantom.blurimage.BlurImage;
import com.squareup.picasso.Picasso;
import com.tylersuehr.socialtextview.SocialTextView;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class AdminHomeAdapter extends RecyclerView.Adapter<BaseViewHolder> {
    private static final int VIEW_TYPE_LOADING = 0;
    public static final int VIEW_TYPE_NORMAL = 1;
    private boolean isLoaderVisible = false;
    private List<UserViewPost> mNewsItemList;
    private Callback mCallback;
    Context mContext;
    Boolean click=true,expand=false;
    Utility utility;
    SessionParam sessionParam;
    //private PaginationListener listener;
    PaginationListenerCallback listener;
    ExpandableTextView expandableTextView;
    boolean isLocationShow = false;
    int maxline=0,height=0;
    private int screenWidth, screenHeight;
    Display display=null;
    public static final int selected_position = 0;
    Integer userId;
    Boolean premiumUser;
    String userRole;
    Bitmap blurredBitmap;
    private BlurCustom blurCustom;
    private static final float BLUR_RADIUS = 25F;
    public AdminHomeAdapter(List<UserViewPost> news, Context mContext, PaginationListenerCallback listener) {
        this.mNewsItemList = news;
        this.mContext=mContext;
        utility=new Utility();
        this.listener=listener;
        this.expandableTextView=new ExpandableTextView(mContext);
        sessionParam=new SessionParam(mContext);
        userId=(sessionParam.id);
        userRole=sessionParam.role;
        //premiumUser=sessionParam.premieUser;
        premiumUser=true;
        blurCustom = new BlurCustom(mContext, BLUR_RADIUS);
      /* display =mContext.getWindowManager().getDefaultDisplay();
        screenWidth = display.getWidth();
        screenHeight = display.getHeight();*/

    }
    public void setCallback(Callback callback) {
        mCallback = callback;
    }



    private  boolean isPositionHeader(int position) {
        return position == 0;
    }

    private  boolean isPositionFooter(int position) {
        return position == mNewsItemList.size()-1;
    }

    @NonNull
    @Override
    public BaseViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (isLocationShow){

            switch (viewType){
                case VIEW_TYPE_NORMAL:
                    return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.admin_post_list,parent,false));
                case VIEW_TYPE_LOADING:
                    return new EmptyViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.home_location_layout,parent,false));
                default:
                    return null;
            }
        }else {
            return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.home_location_layout,parent,false));
        }
    }

    @Override
    public void onBindViewHolder(@NonNull BaseViewHolder holder, int position) {

        holder.onBind(position);
    }

    @Override
    public int getItemCount() {
        try {
            if (isLocationShow) {
                return mNewsItemList == null ? 0 : mNewsItemList.size() + 1;
            }else {
                return mNewsItemList == null ? 0 : mNewsItemList.size();

            }
        }catch (Exception e){
            e.printStackTrace();
            return 0;
        }
    }
    @Override
    public int getItemViewType(int position) {
       /* if (isLoaderVisible) {
                return position == mNewsItemList.size() ? VIEW_TYPE_LOADING : VIEW_TYPE_NORMAL;
        } else {
            return VIEW_TYPE_NORMAL;
        }*/
        if (isLocationShow) {
            return (position == mNewsItemList.size()) ? VIEW_TYPE_LOADING : VIEW_TYPE_NORMAL;
        }else {
            return VIEW_TYPE_NORMAL;
        }

    }

    public void setLike(UserViewPost postItems) {
        // postItems.getId().compareTo(mNewsItemList.get(AdapterPo))

    }
    public void add(UserViewPost response) {
        mNewsItemList.add(response);
        notifyItemInserted(mNewsItemList.size() - 1);
    }
    public void addAll(List<UserViewPost> postItems) {
        for (UserViewPost response : postItems) {
            add(response);
        }
    }
    private void remove(UserViewPost postItems) {
        int position = mNewsItemList.indexOf(postItems);
        if (position > -1) {
            mNewsItemList.remove(position);
            notifyItemRemoved(position);
        }
    }
    public void showLocation(){
        isLocationShow=true;
    }
    public void removeLocation(){
        isLocationShow=true;
    }
    public void addLoading() {
        isLoaderVisible = true;
        // add(new UserViewPost());
    }
    public void removeLoading() {
        isLoaderVisible = false;
        int position = mNewsItemList.size() - 1;
        UserViewPost item = getItem(position);
        if (item != null) {
            mNewsItemList.remove(position);
            notifyItemRemoved(position);
        }
    }
    public void clear() {
        try {
            while (getItemCount() > 0) {
                remove(getItem(0));
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }
    UserViewPost getItem(int position) {
        return mNewsItemList.get(position);

    }
    public interface Callback {
        void onRepoEmptyViewRetryClick();
    }


    public class ViewHolder extends BaseViewHolder {

        SocialTextView hashTextView;
        ImageView newsImage,imageUpper,imageSceond,linkConnectionImage;
        CircleImageView likeImage;
        public ImageView imageShowMore;
        TextView showMoreTxt;
        CircleImageView iv_logo;
        TextView homeUserTextView,dateTextView,likeCountTxt;
        LinearLayout layoutMoreTxt,changeLayout,socialParentLayout;
        LinearLayout root_layout,headerLayout,like_layout,like_parent_layout;
        public ViewHolder(View itemView) {
            super(itemView);
            root_layout = itemView.findViewById(R.id.root_layout);
            headerLayout = itemView.findViewById(R.id.header_top_layout);
            iv_logo = itemView.findViewById(R.id.iv_logo);
            homeUserTextView = itemView.findViewById(R.id.home_admin_name_text);
            newsImage = itemView.findViewById(R.id.upload_image_public);
            imageUpper = itemView.findViewById(R.id.upload_image_upper);
            imageSceond = itemView.findViewById(R.id.upload_image_sceond);
            changeLayout = itemView.findViewById(R.id.admin_bottom_parent_layout);
            socialParentLayout = itemView.findViewById(R.id.admin_social_text_parent_layout);



            dateTextView = itemView.findViewById(R.id.admin_post_date_txt);
            hashTextView = itemView.findViewById(R.id.admin_social_text_view);
            like_layout = itemView.findViewById(R.id.like_layout);
            imageShowMore = itemView.findViewById(R.id.image_show_more);
            like_parent_layout = itemView.findViewById(R.id.like_parent_layout);
            likeImage = itemView.findViewById(R.id.like_image);
            likeCountTxt = itemView.findViewById(R.id.like_count_txt);




            //newTitle.getViewTreeObserver().removeOnGlobalLayoutListener(this::clear);
            /*final View content = itemView.findViewById(android.R.id.content).getRootView();
            if (content.getWidth() > 0) {
                Bitmap image = BlurBuilder.blur(content);
                imageSceond.setBackgroundDrawable(new BitmapDrawable(mContext.getResources(), image));
            } else {
                content.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                    @Override
                    public void onGlobalLayout() {
                        Bitmap image = BlurBuilder.blur(content);
                        imageSceond.setBackgroundDrawable(new BitmapDrawable(mContext.getResources(), image));
                    }
                });
            }*/


        }
        protected void clear() {
        }
        public void onBind(int position) {
            super.onBind(position);
            try {
                UserViewPost mNewsItem = mNewsItemList.get(position);
                if (mNewsItem.getId()!=null) {


                    try {
                        //Picasso.with(mContext).load(mNewsItem.getUserImage()).into(iv_logo);
                        Glide.with(mContext).load(mNewsItem.getUserImage()).placeholder(R.drawable.profile_pics).into(iv_logo);

                        /*Picasso.with(mContext).load(mNewsItem.getImageUrl())
                                .into(imageUpper);*/
                        Glide.with(mContext).load(mNewsItem.getImageUrl()).placeholder(R.drawable.profile_pics)
                                .into(imageUpper);

                        Thread thread = new Thread(new Runnable(){
                            @Override
                            public void run() {
                                Bitmap bitmapFromURL=Utility.getBitmapFromURL(mNewsItem.getImageUrl());
                                BlurImage.with(mContext).load(bitmapFromURL).intensity(25).Async(true).into(imageSceond);
                                BlurImage.with(mContext).load(bitmapFromURL).intensity(25).Async(true).into(newsImage);

                            }
                        });

                        thread.start();

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    try{
                        String changeDate=utility.getPostUploadDate(mNewsItem.getPostedDate());
                        dateTextView.setText(changeDate);
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                    try {
                        if (!Validations.isEmptyString(mNewsItem.getName())) {
                            homeUserTextView.setText(mNewsItem.getName());
                            homeUserTextView.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    listener.onPaginationClick(mNewsItem, click, position, "tagName");
                                }
                            });
                        }

                        hashTextView.setLinkText(mNewsItem.getCaption());
                        hashTextView.setOnLinkClickListener(new SocialTextView.OnLinkClickListener() {
                            @Override
                            public void onLinkClicked(int linkType, String matchedText) {
                                //linkType 1=hashTag,2==mention
                                listener.autoLinkListener(linkType, "", matchedText, mNewsItem);
                            }
                        });


                        changeLayout.post(new Runnable() {
                            @Override
                            public void run() {
                                //maybe also works height = ll.getLayoutParams().height;
                                height = changeLayout.getHeight();
                            }
                        });

                        hashTextView.post(new Runnable() {

                            @Override
                            public void run() {
                                Log.v("Line count: ", hashTextView.getLineCount() + "");
                                maxline = hashTextView.getLineCount();
                                /*if (maxline > 3) {
                                    imageShowMore.setVisibility(View.VISIBLE);
                                    //showMoreTxt.setVisibility(View.VISIBLE);
                                } else {
                                    imageShowMore.setVisibility(View.GONE);
                                    //showMoreTxt.setVisibility(View.GONE);
                                }*/
                            }
                        });


                        imageShowMore.setOnClickListener(new View.OnClickListener() {
                            public void onClick(View v) {
                                if (!expand){
                                    expand=true;
                                    dateTextView.setVisibility(View.VISIBLE);
                                    imageShowMore.setImageResource(R.drawable.ic_expand_more_white_24dp);

                                    ObjectAnimator animation = ObjectAnimator.ofInt(hashTextView, "maxLines", 40);
                                    animation.setDuration(100).start();
                                    int height_in_pixels = hashTextView.getLineCount() * hashTextView.getLineHeight();
                                    LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) changeLayout.getLayoutParams();
                                     /*if (height > height_in_pixels) {
                                        params.height = height +height_in_pixels;
                                    } else {
                                       // params.height = (height*2)+((height_in_pixels+100)/2);
                                        params.height = (height)+((height_in_pixels+100)/2);
                                    }*/
                                    final int sdk = android.os.Build.VERSION.SDK_INT;
                                    if (sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
                                        params.height=height*2;
                                        if (params.height<height_in_pixels){
                                            params.height=params.height+height_in_pixels;
                                        }

                                    }else if (sdk< Build.VERSION_CODES.O){
                                        if (params.height<0){
                                            params.height=height_in_pixels+((height_in_pixels + 100) / 2);

                                        }else {
                                            if (height > height_in_pixels) {
                                                params.height = height +height_in_pixels;
                                            } else if (params.height>height_in_pixels){
                                                params.height = (params.height) + ((height_in_pixels + 100) / 2);
                                            }else {
                                                params.height=height+(height_in_pixels + 150);
                                            }
                                        }
                                    }else {
                                        if (height > height_in_pixels) {
                                            params.height = height +height_in_pixels;
                                        } else {
                                            // params.height = (height*2)+((height_in_pixels+100)/2);
                                            if (params.height<0){
                                                params.height=height_in_pixels+((height_in_pixels + 100) / 2);
                                            }else {
                                                params.height = (height) + ((height_in_pixels + 100) / 2);
                                            }
                                            if (height<(height_in_pixels+100)){
                                                params.height=height+height_in_pixels;
                                            }
                                        }
                                    }

                                    changeLayout.setLayoutParams(params);
                                }else{
                                    expand=false;
                                    dateTextView.setVisibility(View.GONE);
                                    imageShowMore.setImageResource(R.drawable.ic_expand_less_white_24dp);

                                    ObjectAnimator animation = ObjectAnimator.ofInt(hashTextView, "maxLines", 3);
                                    animation.setDuration(100).start();
                                    ViewGroup.LayoutParams params = (ViewGroup.LayoutParams) changeLayout.getLayoutParams();
                                    int height_in_pixels = hashTextView.getLineCount() * hashTextView.getLineHeight();

                                    final int sdk = android.os.Build.VERSION.SDK_INT;
                                    if (sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
                                        params.height=params.height+height_in_pixels;

                                    }else if (sdk< Build.VERSION_CODES.O){
                                        params.height=height_in_pixels+5;
                                    }else {
                                        params.height = height;
                                    }
                                    changeLayout.setLayoutParams(params);

                                }
                               /* expand = !expand;
                                advancedView.setVisibility( isExpanded ?  View.VISIBLE : View.GONE );
                                expandCtrl.setCompoundDrawablesWithIntrinsicBounds( isExpanded ? icon_collapse : icon_expand, 0, 0, 0 );
*/

                            }
                        });
                        if (premiumUser){
                            likeCountTxt.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {

                                    listener.onPaginationClick(mNewsItem, click, position, "likeCount");
                                }
                            });
                            likeImage.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    listener.onPaginationClick(mNewsItem, click, position, "likeCount");
                                }
                            });
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }


                    try {
                        Log.d("userName", "session: " + userId + " " + "viewPost: " + mNewsItem.getName().toLowerCase().trim());

                        if (!(userId.equals(mNewsItem.getUserId()))) {
                            linkConnectionImage.setVisibility(View.VISIBLE);
                            linkConnectionImage.setVisibility(View.VISIBLE);
                        }else {
                            linkConnectionImage.setVisibility(View.GONE);
                        }

                        final int sdk = android.os.Build.VERSION.SDK_INT;

                        /*if (mNewsItem.getLikedStatus()) {
                            click = false;
                            if (sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
                                likeImage.setBackgroundDrawable(ContextCompat.getDrawable(mContext, R.drawable.like_true));
                            } else {
                                likeImage.setBackground(ContextCompat.getDrawable(mContext, R.drawable.like_true));
                            }
                        } else {
                            click = true;
                            if (sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
                                likeImage.setBackgroundDrawable(ContextCompat.getDrawable(mContext, R.drawable.like));
                            } else {
                                likeImage.setBackground(ContextCompat.getDrawable(mContext, R.drawable.like));
                            }
                        }*/

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    try{
                       /* if (mNewsItem.getLikedCount()>0) {
                            likeCountTxt.setVisibility(View.VISIBLE);
                            likeCountTxt.setText(mNewsItem.getLikedCount().toString() + " Likes");
                        }else {
                            likeCountTxt.setVisibility(View.VISIBLE);
                            likeCountTxt.setText( "No Likes");
                        }*/
                        likeCountTxt.setVisibility(View.GONE);
                    }catch (Exception e){
                        e.printStackTrace();
                    }

                    linkConnectionImage.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            listener.onPaginationClick(mNewsItem, click, position, "connection");
                        }
                    });
                  /*  likeImage.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {

                            //likeImage.setBackground(click ?ContextCompat.getDrawable(mContext, R.drawable.like_true):ContextCompat.getDrawable(mContext, R.drawable.like));
                            listener.onPaginationClick(mNewsItem, click, position, "like");
                            final int sdk = android.os.Build.VERSION.SDK_INT;
                            if (click) {
                                click = false;
                                if (sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
                                    likeImage.setBackgroundDrawable(ContextCompat.getDrawable(mContext, R.drawable.like_true));
                                } else {
                                    likeImage.setBackground(ContextCompat.getDrawable(mContext, R.drawable.like_true));
                                }
                            } else {
                                click = true;
                                if (sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
                                    likeImage.setBackgroundDrawable(ContextCompat.getDrawable(mContext, R.drawable.like));
                                } else {
                                    likeImage.setBackground(ContextCompat.getDrawable(mContext, R.drawable.like));
                                }
                            }
                        }
                    });*/

                    itemView.setOnLongClickListener(new View.OnLongClickListener() {
                        @Override
                        public boolean onLongClick(View view) {
                            listener.onPaginationLongClick(mNewsItem, position,"admin");
                            return false;
                        }
                    });


                    //newTitle.setText(mNewsItem.getCaption());


                }
            }catch (Exception e){
                e.printStackTrace();
            }

        }
        private void setUserBackground() {
            final int sdk = android.os.Build.VERSION.SDK_INT;

            root_layout.setBackgroundColor(ContextCompat.getColor(mContext, R.color.colorPrimary));
            headerLayout.setBackgroundColor(ContextCompat.getColor(mContext, R.color.colorPrimary));
            homeUserTextView.setTextColor(ContextCompat.getColor(mContext, R.color.white));

            hashTextView.setTextColor(ContextCompat.getColor(mContext, R.color.white));

            if (sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
                //imageShowMore.setBackgroundDrawable(ContextCompat.getDrawable(mContext, R.drawable.ic_expand_less_white_24dp));
                imageShowMore.setImageResource(R.drawable.ic_expand_less_white_24dp);

                likeImage.setBackgroundDrawable(ContextCompat.getDrawable(mContext, R.drawable.like));
            } else {
                //imageShowMore.setBackground(ContextCompat.getDrawable(mContext, R.drawable.ic_expand_less_white_24dp));
                imageShowMore.setImageResource(R.drawable.ic_expand_less_white_24dp);
                likeImage.setBackground(ContextCompat.getDrawable(mContext, R.drawable.like));
            }

        }

        //set Admin background
        private void setAdminBackground() {
            root_layout.setBackgroundColor(ContextCompat.getColor(mContext, R.color.white));
            headerLayout.setBackgroundColor(ContextCompat.getColor(mContext, R.color.white));
            like_layout.setBackgroundColor(ContextCompat.getColor(mContext, R.color.white));
            like_parent_layout.setBackgroundColor(ContextCompat.getColor(mContext, R.color.white));
            homeUserTextView.setTextColor(ContextCompat.getColor(mContext, R.color.black));
            hashTextView.setBackgroundColor(ContextCompat.getColor(mContext, R.color.white));
            hashTextView.setTextColor(ContextCompat.getColor(mContext, R.color.black));

            final int sdk = android.os.Build.VERSION.SDK_INT;

            if (sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
                //imageShowMore.setBackgroundDrawable(ContextCompat.getDrawable(mContext, R.drawable.ic_baseline_expand_less_24));
                imageShowMore.setImageResource(R.drawable.ic_baseline_expand_less_24);
                likeImage.setBackgroundColor(ContextCompat.getColor(mContext, R.color.colorPrimaryDark));
                //Picasso.with(mContext).load(R.drawable.like).into(likeImage);
                Glide.with(mContext).load(R.drawable.like).into(likeImage);
            } else {
                // likeImage.setBackground(ContextCompat.getDrawable(mContext, R.drawable.like_true));
                /*Picasso.with(mContext).load("http//drawable.data").placeholder(R.mipmap.like_primary_color)   // optional
                        .error(R.mipmap.like_primary_color) .into(likeImage);*/
                Glide.with(mContext).load("http//drawable.data").placeholder(R.mipmap.like_primary_color)   // optional
                        .error(R.mipmap.like_primary_color) .into(likeImage);
                likeImage.setBackgroundColor(ContextCompat.getColor(mContext, R.color.colorPrimaryDark));
                //imageShowMore.setBackground(ContextCompat.getDrawable(mContext, R.drawable.ic_baseline_expand_less_24));
                imageShowMore.setImageResource(R.drawable.ic_baseline_expand_less_24);
            }


        }

    }





    public void showImageOne(View v) {
        String url = "https://www.gstatic.com/webp/gallery3/1.sm.png";
        // new AsyncTaskLoadImage(imageView).execute(url);
    }
    /*public void showImageTwo(View v) {
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        try {
            URL url = new URL("https://www.gstatic.com/webp/gallery/4.sm.jpg");
            v.setImageBitmap(BitmapFactory.decodeStream((InputStream)url.getContent()));
        } catch (IOException e) {
            //Log.e(TAG, e.getMessage());
        }
    }*/


    public class EmptyViewHolder extends BaseViewHolder {
        Button restButton,locationButton;
        public EmptyViewHolder(View itemView) {
            super(itemView);
            restButton=itemView.findViewById(R.id.pagination_restart_button);
            locationButton=itemView.findViewById(R.id.pagination_location_button);
        }

        @Override
        public void onBind(int position) {
            super.onBind(position);
            restButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    // view.position=selected_position;
                    listener.ResetClickListener(position,"");
                }
            });
            locationButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    listener.ResetClickListener(position,"locationChange");
                }
            });
        }

        @Override
        protected void clear() {
        }
    }
}
