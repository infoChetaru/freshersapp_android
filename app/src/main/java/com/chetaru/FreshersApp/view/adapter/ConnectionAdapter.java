package com.chetaru.FreshersApp.view.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;
import com.chetaru.FreshersApp.CustomImage.ImageCropView;
import com.chetaru.FreshersApp.R;
import com.chetaru.FreshersApp.service.model.ConnListResponse;
import com.chetaru.FreshersApp.service.model.NotificationList;
import com.chetaru.FreshersApp.service.model.UserViewPost;
import com.chetaru.FreshersApp.utility.SessionParam;
import com.chetaru.FreshersApp.utility.Utility;
import com.chetaru.FreshersApp.utility.Validations;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;


public class ConnectionAdapter extends RecyclerView.Adapter<ConnectionAdapter.ViewHolder> {

    private List<ConnListResponse> mConnList=new ArrayList<>();
    List<ConnListResponse> mFilteredList;
    Context mContext;

    private boolean isLoadingAdded = false;
    // View Types
    private static final int ITEM = 0;
    private static final int LOADING = 1;
    private boolean retryPageLoad = false;
    private String errorMsg;
    Utility utility;
    SessionParam sessionParam;
    ConnClickListener mListener;
    String fileType="2";


    public ConnectionAdapter(List<ConnListResponse> mConnList, Context mContext,ConnClickListener mListener){
        this.mContext=mContext;
        this.mConnList=mConnList;
        notifyDataSetChanged();
        this.mFilteredList=mConnList;
        utility=new Utility();
        sessionParam=new SessionParam(mContext);
        this.mListener=mListener;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder{
        CircleImageView connLogo;
        ImageCropView cameraShow;
        TextView connName,connDate,connDescription;
        View view;
        TextView connCount;
        public ViewHolder(@NonNull View view) {
            super(view);
            this.view=view;
            connLogo=view.findViewById(R.id.iv_logo);
            connName=view.findViewById(R.id.tv_conn_Name);
            connDescription=view.findViewById(R.id.tv_conn_dicrip);
            connDate=view.findViewById(R.id.tv_conn_date);
            cameraShow=view.findViewById(R.id.camera_show_list);
            connCount=view.findViewById(R.id.conn_count_text);

        }
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ViewHolder viewHolder=null;
        LayoutInflater inflater=LayoutInflater.from(parent.getContext());
        //View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.connection_list,parent,false);
       switch(viewType){
           case ITEM:
               View viewItem=inflater.inflate(R.layout.connection_list,parent,false);
               viewHolder=new ViewHolder(viewItem);

               break;
           case LOADING:
               View viewLoading=inflater.inflate(R.layout.item_progress,parent,false);
               viewHolder=new LoadingVH(viewLoading);
               break;

       }
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        switch (getItemViewType(position)){
            case ITEM:
                //final ViewHolder mholder = (ViewHolder) holder;

                ConnListResponse connList = mFilteredList.get(position); //data

               /* Glide.with(mContext)
                        .asBitmap().load(connList.getUserImageUrl())
                        .into(new SimpleTarget<Bitmap>() {
                            @Override
                            public void onResourceReady(Bitmap resource, Transition<? super Bitmap> transition) {
                                holder.connLogo.setImageBitmap(resource);
                            }
                        });*/
                holder.connName.setText(connList.getUserName());
                Glide.with(mContext).load(mFilteredList.get(position).getUserImageUrl()).apply(new RequestOptions().centerCrop().placeholder(R.drawable.profile_pics)).into(holder.connLogo);
                holder.connLogo.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        mListener.logoClickListener(holder.connLogo,mFilteredList.get(position).getUserImageUrl());
                    }
                });
                holder.view.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        mListener.itemClickListener(connList);
                    }
                });
                //holder.connCount.setText("202");
                if (connList.getMessageCount()>0) {
                    holder.connCount.setText(connList.getMessageCount().toString());
                }else {
                    holder.connCount.setVisibility(View.GONE);
                }

                try {
                    if (!Validations.isEmptyString(connList.getCreatedAt())) {
                       // String getDate=utility.changeDateName();
                        String dateChange = utility.formatToYesterdayOrToday(mFilteredList.get(position).getCreatedAt());
                        holder.connDate.setText(dateChange);
                    }else {
                        if (!Validations.isEmptyString(connList.getLastMessage())){
                            //String getDate=utility.changeDateName(mFilteredList.get(position).getLastMsgDate());
                            String dateChange = utility.formatToYesterdayOrToday(mFilteredList.get(position).getCreatedAt());
                            holder.connDate.setText(dateChange);
                        }
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
                try {
                    if (!Validations.isEmptyString(connList.getFileType())){
                        if (connList.getFileType().equals("1")){
                            holder.connDescription.setText(connList.getLastMessage());
                        }
                        if (connList.getFileType().equals("2")){
                            holder.connDescription.setCompoundDrawablesWithIntrinsicBounds(R.drawable.camera_custom_size, 0, 0, 0);
                            holder.connDescription.setText("photo");
                        }
                    }else {
                        holder.connDescription.setText("Say Hi to your new connection.");
                    }


                        /*if (fileType.equals("1")){
                            holder.connDescription.setText("last message");
                            holder.cameraShow.setVisibility(View.GONE);
                        }else  if (fileType.equals("2")){
                            holder.cameraShow.setVisibility(View.GONE);
                            holder.connDescription.setCompoundDrawablesWithIntrinsicBounds(R.drawable.camera_custom_size, 0, 0, 0);
                            holder.connDescription.setText(" photo");
                        }else {
                        holder.connDescription.setText("Say Hi to your new connection.");
                            holder.cameraShow.setVisibility(View.GONE);
                             }*/
                }catch (Exception e){
                    e.printStackTrace();
                }


                break;
            case LOADING:
                LoadingVH loadingVH = (LoadingVH) holder;

                if (retryPageLoad) {
                    loadingVH.mErrorLayout.setVisibility(View.VISIBLE);
                    loadingVH.mProgressBar.setVisibility(View.GONE);

                    loadingVH.mErrorTxt.setText(
                            errorMsg != null ?
                                    errorMsg :
                                    mContext.getString(R.string.error_msg_unknown));

                } else {
                   /* if(mFilteredList.get(mFilteredList.size()-1).getSelected()==null){
                        loadingVH.mErrorLayout.setVisibility(View.GONE);
                        loadingVH.mProgressBar.setVisibility(View.GONE);
                    }else{
                        loadingVH.mErrorLayout.setVisibility(View.GONE);
                        loadingVH.mProgressBar.setVisibility(View.VISIBLE);
                    }*/
                }
                break;
        }
    }

    protected class LoadingVH extends ViewHolder implements View.OnClickListener {
        private ProgressBar mProgressBar;
        private ImageButton mRetryBtn;
        private TextView mErrorTxt;
        private LinearLayout mErrorLayout;

        public LoadingVH(View itemView) {
            super(itemView);
            mProgressBar = itemView.findViewById(R.id.loadmore_progress);
            mRetryBtn = itemView.findViewById(R.id.loadmore_retry);
            mErrorTxt = itemView.findViewById(R.id.loadmore_errortxt);
            mErrorLayout = itemView.findViewById(R.id.loadmore_errorlayout);
            mRetryBtn.setOnClickListener(this);
            mErrorLayout.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.loadmore_retry:
                case R.id.loadmore_errorlayout:
                    showRetry(false, null);
                    //mCallback.retryPageLoad();
                    break;
            }
        }
    }
    public void showRetry(boolean show, @Nullable String errorMsg) {
        retryPageLoad = show;
        notifyItemChanged(mFilteredList.size() - 1);

        if (errorMsg != null) this.errorMsg = errorMsg;
    }


    @Override
    public int getItemCount() {

        return mFilteredList == null ? 0 : mFilteredList.size();
    }



    public Filter getFilter() {

        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {

                String charString = charSequence.toString();

                if (charString.isEmpty()) {

                    mFilteredList = mConnList;
                } else {
                    ArrayList<ConnListResponse> filteredList = new ArrayList<>();

                    for (ConnListResponse addActionUser : mConnList) {

                        if (addActionUser.getLastMessage().toLowerCase().contains(charString.toLowerCase()) ||
                                addActionUser.getLastMessage().toUpperCase().contains(charString.toUpperCase())) {
                            filteredList.add(addActionUser);
                        }
                    }
                    mFilteredList = filteredList;

                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = mFilteredList;
                return filterResults;

            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                mFilteredList = (ArrayList<ConnListResponse>) filterResults.values;
                notifyDataSetChanged();


            }
        };
    }

    public void add(ConnListResponse r) {
        if (r.getConnectionId()!=null) {
            mFilteredList.add(r);
            notifyItemInserted(mFilteredList.size() - 1);
        }
    }

    public void addAll(List<ConnListResponse> noti_Results) {
        for (ConnListResponse result : noti_Results) {
            add(result);
        }
    }

    public void remove(ConnListResponse r) {
        int position = mFilteredList.indexOf(r);
        if (position > -1) {
            mFilteredList.remove(position);
            notifyItemRemoved(position);
        }
    }
    public void clear() {
        try {
            while (getItemCount() > 0) {
                remove(getItem(0));
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public void addLoadingFooter() {
        isLoadingAdded = true;
        add(new ConnListResponse());
    }

    public void removeLoadingFooter() {
        isLoadingAdded = false;

        int position = mFilteredList.size() - 1;
        ConnListResponse result = getItem(position);

        if (result != null) {
            mFilteredList.remove(position);
            notifyItemRemoved(position);
        }
    }


    @Override
    public int getItemViewType(int position) {
        return (position == mFilteredList.size() - 1 && isLoadingAdded) ? LOADING : ITEM;
//        if(mFilteredList.size()<5){
//            return mFilteredList.size() - 1;
//        }else{
//            return (position == mFilteredList.size() - 1 && isLoadingAdded) ? LOADING : ITEM;
//        }
    }

    public ConnListResponse getItem(int position) {
        return mFilteredList.get(position);
    }
   public interface ConnClickListener{
        public void itemClickListener(ConnListResponse connList);
        public void logoClickListener(View view,String imageString);
    }


}
