package com.chetaru.FreshersApp.view.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.chetaru.FreshersApp.R;
import com.chetaru.FreshersApp.service.model.ChatListResponse;
import com.chetaru.FreshersApp.service.model.NotificationList;
import com.chetaru.FreshersApp.utility.SessionParam;
import com.chetaru.FreshersApp.utility.Utility;
import com.chetaru.FreshersApp.utility.Validations;
import com.google.android.material.imageview.ShapeableImageView;
import com.squareup.picasso.Picasso;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class MessageListAdapter extends RecyclerView.Adapter {
    public Context mContext;
    private List<ChatListResponse> mMessageList;
    private boolean isLoadingAdded = false;
    private static final int VIEW_TYPE_MESSAGE_SENT = 1;
    private static final int VIEW_TYPE_MESSAGE_RECEIVED = 2;
    SessionParam sessionParam;
    Utility utility;
    public int userId=0;
    public chatClickListener mListener;


    public MessageListAdapter(int otherUserId, List<ChatListResponse> mMessageList,Context mContext,chatClickListener mListener) {
        this.mContext = mContext;
        this.mMessageList = mMessageList;
        this.utility=new Utility();
        this.sessionParam=new SessionParam(mContext);
        this.userId=sessionParam.id;
        this.mListener=mListener;
    }
    @Override
    public int getItemCount() {

        return mMessageList == null ? 0 : mMessageList.size();
    }

    // Determines the appropriate ViewType according to the sender of the message.
    @Override
    public int getItemViewType(int position) {
        ChatListResponse message = (ChatListResponse) mMessageList.get(position);

       // String messageUserId=String.valueOf(message.getUserId());

       if (message.getUserId()==(userId)) {
            // If the current user is the sender of the message
            return VIEW_TYPE_MESSAGE_SENT;
        } else {
            // If some other user sent the message
            return VIEW_TYPE_MESSAGE_RECEIVED;
        }

    }
    // Inflates the appropriate layout according to the ViewType.
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;

        if (viewType == VIEW_TYPE_MESSAGE_RECEIVED) {
            view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.item_message_received, parent, false);
            return new ReceivedMessageHolder(view);
        }else  if (viewType == VIEW_TYPE_MESSAGE_SENT) {
            view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.item_message_sent, parent, false);
            return new SentMessageHolder(view);
        }

        return null;
    }
    // Passes the message object to a ViewHolder so that the contents can be bound to UI.
    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ChatListResponse message = (ChatListResponse) mMessageList.get(position);

        String groupDate="";
        try {
            if (!Validations.isEmptyString(message.getMsgDate())) {
                String dateChange = Utility.formatTodayYesterday(message.getMsgDate());
                // holder.groupDateTxt.setText(dateChange);
                if (position > 0) {
                    String newDate = Utility.formatTodayYesterday(mMessageList.get(position).getMsgDate());
                    String oldDate = Utility.formatTodayYesterday(mMessageList.get(position - 1).getMsgDate());
                    Log.d("dateConvertor", "newDate: " + newDate + "  " + "oldDate:" + oldDate);
                    if (newDate.equalsIgnoreCase(oldDate)) {
                        // holder.dateLayout.setVisibility(View.GONE);
                        groupDate="";
                    } else {
                        // holder.dateLayout.setVisibility(View.VISIBLE);
                        groupDate=dateChange;
                    }

                } else {
                    // holder.dateLayout.setVisibility(View.VISIBLE);
                    groupDate=dateChange;
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        switch (holder.getItemViewType()) {
            case VIEW_TYPE_MESSAGE_SENT:
                ((SentMessageHolder) holder).bind(mContext,message,groupDate,mListener);
                break;
            case VIEW_TYPE_MESSAGE_RECEIVED:
                ((ReceivedMessageHolder) holder).bind(mContext,message,groupDate,mListener);

        }
    }

    public void addLoadingFooter() {
        isLoadingAdded = true;
        add(new ChatListResponse());
    }
    public void removeLoadingFooter() {
        isLoadingAdded = false;

       // int position = mMessageList.size() - 1;
       /* ChatListResponse result = getItem(position);

        if (result != null) {
            mMessageList.remove(position);
            notifyItemRemoved(position);
        }*/
    }

    private static class SentMessageHolder extends RecyclerView.ViewHolder {
        TextView rightMessage, rightDate,rightImageDate;
        CircleImageView userRightImageView;
        LinearLayout rightUserLayout,right_image_layout,right_message_layout;;

        ShapeableImageView rightImagePost;
        LinearLayout timeTextLayout;
        TextView groupsDate;
        SentMessageHolder(View view) {
            super(view);

            right_image_layout=view.findViewById(R.id.right_image_layout);
            right_message_layout=view.findViewById(R.id.right_message_layout);
            rightUserLayout=view.findViewById(R.id.right_user_layout);
            userRightImageView=view.findViewById(R.id.iv_logo_right);
            rightImagePost=view.findViewById(R.id.right_user_post_image);
            rightMessage=view.findViewById(R.id.user_right_message_txt_view);
            rightDate=view.findViewById(R.id.user_right_date_txt);
            rightImageDate=view.findViewById(R.id.user_right_image_date_txt);

            //For GroupDate
            timeTextLayout=view.findViewById(R.id.timeTextLayout);
            groupsDate=view.findViewById(R.id.group_date_txt);

        }

        void bind(Context mContext, ChatListResponse message, String groupDate, chatClickListener mListener) {
            //messageText.setText(message.getMessage());
            ChatListResponse chatData=message;

            if (!Validations.isEmptyString(groupDate)){
                timeTextLayout.setVisibility(View.VISIBLE);
                groupsDate.setText(groupDate);
            }else {
                timeTextLayout.setVisibility(View.GONE);
            }


            rightUserLayout.setVisibility(View.VISIBLE);
            userRightImageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                   mListener.onUserImageView(userRightImageView,chatData.getUserImage());
                }
            });
            rightImagePost.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mListener.onUserImageView(rightImagePost,chatData.getMessage());
                }
            });
            if (chatData.getFileType().equals(1)) {
                right_message_layout.setVisibility(View.VISIBLE);
                right_image_layout.setVisibility(View.GONE);
                rightMessage.setVisibility(View.VISIBLE);
                rightImagePost.setVisibility(View.GONE);
                rightMessage.setText(chatData.getMessage());
            }else {
                right_message_layout.setVisibility(View.GONE);
                right_image_layout.setVisibility(View.VISIBLE);
                rightMessage.setVisibility(View.GONE);
                rightImagePost.setVisibility(View.VISIBLE);
                               /* Glide.with(mContext).load(chatData.getMessage())
                                        .diskCacheStrategy(DiskCacheStrategy.DATA)
                                        .into(holder.rightImagePost);*/
                //Picasso.with(mContext).load(chatData.getMessage()).into(rightImagePost);
                Glide.with(mContext).load(chatData.getMessage()).placeholder(R.drawable.profile_pics).into(rightImagePost);

            }
            try {
                if (!Validations.isEmptyString(chatData.getMsgDate())) {
                    String dateChange = Utility.formatToShowOnlyTime(chatData.getMsgDate());
                    rightDate.setText(dateChange);
                    rightImageDate.setText(dateChange);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
    private  class DateViewHolder extends RecyclerView.ViewHolder{

        TextView dateTextView;
        public DateViewHolder(@NonNull View view) {
            super(view);
            dateTextView=view.findViewById(R.id.group_date_txt);

        }

        public void bind(ChatListResponse message, int position) {
            String groupDate="";
            try {
                if (!Validations.isEmptyString(message.getMsgDate())) {
                    String dateChange = Utility.formatTodayYesterday(message.getMsgDate());
                    // holder.groupDateTxt.setText(dateChange);
                    if (position > 0) {
                        String newDate = Utility.formatTodayYesterday(mMessageList.get(position).getMsgDate());
                        String oldDate = Utility.formatTodayYesterday(mMessageList.get(position - 1).getMsgDate());
                        Log.d("dateConvertor", "newDate: " + newDate + "  " + "oldDate:" + oldDate);
                        if (newDate.equalsIgnoreCase(oldDate)) {
                            // holder.dateLayout.setVisibility(View.GONE);
                            groupDate="";
                        } else {
                            // holder.dateLayout.setVisibility(View.VISIBLE);
                            groupDate=dateChange;
                        }

                    } else {
                        // holder.dateLayout.setVisibility(View.VISIBLE);
                        groupDate=dateChange;
                    }
                }
            }catch (Exception e){
                e.printStackTrace();
            }
            dateTextView.setText(groupDate);
        }
    }

    private static class ReceivedMessageHolder extends RecyclerView.ViewHolder {
        TextView messageText, timeText, nameText;
        ImageView profileImage;

        CircleImageView userLeftImageView;
        ShapeableImageView leftImagePost;
        // RoundRectCornerImageView leftImagePost;
        TextView leftMessage,leftDate;
        TextView leftImageDate;
        LinearLayout leftUserLayout;
        LinearLayout left_image_layout,left_message_layout;

        LinearLayout timeTextLayout;
        TextView groupsDate;
        ReceivedMessageHolder(View view) {
            super(view);

            left_image_layout=view.findViewById(R.id.left_image_layout);
            left_message_layout=view.findViewById(R.id.left_message_layout);
            leftUserLayout=view.findViewById(R.id.left_user_layout);
            userLeftImageView=view.findViewById(R.id.iv_logo_left);
            leftImagePost=view.findViewById(R.id.left_user_post_image);
            leftMessage=view.findViewById(R.id.user_left_message_txt);
            leftDate=view.findViewById(R.id.user_left_date_txt);
            leftImageDate=view.findViewById(R.id.user_left_image_date_txt);
            //For GroupDate
            timeTextLayout=view.findViewById(R.id.timeTextLayout);
            groupsDate=view.findViewById(R.id.group_date_txt);
        }

        void bind(Context mContext, ChatListResponse message, String groupDate, chatClickListener mListener) {
            //messageText.setText(message.getMessage());
            if (!Validations.isEmptyString(groupDate)){
                timeTextLayout.setVisibility(View.VISIBLE);
                groupsDate.setText(groupDate);
            }else {
                timeTextLayout.setVisibility(View.GONE);
            }
            leftUserLayout.setVisibility(View.VISIBLE);
            userLeftImageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mListener.onUserImageView(userLeftImageView,message.getUserImage());

                }
            });
            leftImagePost.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mListener.onUserImageView(leftImagePost,message.getMessage());
                }
            });

            if (message.getFileType().equals(1)) {
                left_message_layout.setVisibility(View.VISIBLE);
                left_image_layout.setVisibility(View.GONE);
                leftMessage.setVisibility(View.VISIBLE);
                leftImagePost.setVisibility(View.GONE);
                leftMessage.setText(message.getMessage());

            }else
            {
                left_message_layout.setVisibility(View.GONE);
                left_image_layout.setVisibility(View.VISIBLE);
                leftMessage.setVisibility(View.GONE);
                leftImagePost.setVisibility(View.VISIBLE);
                                /*Glide.with(mContext).load(chatData.getMessage())
                                        .diskCacheStrategy(DiskCacheStrategy.DATA)
                                    .into(holder.leftImagePost);*/
                //Picasso.with(mContext).load(message.getMessage()).into(leftImagePost);
                Glide.with(mContext).load(message.getMessage()).placeholder(R.drawable.profile_pics).into(leftImagePost);
            }
            try {
                if (!Validations.isEmptyString(message.getMsgDate())) {
                    String dateChange = Utility.formatToShowOnlyTime(message.getMsgDate());
                    leftDate.setText(dateChange);
                    leftImageDate.setText(dateChange);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            // Format the stored timestamp into a readable String using method.
            //timeText.setText(Utils.formatDateTime(message.getCreatedAt()));

           // nameText.setText(message.getSender().getNickname());

            // Insert the profile image from the URL into the ImageView.
           // Utils.displayRoundImageFromUrl(mContext, message.getSender().getProfileUrl(), profileImage);
        }
    }
    public void add(ChatListResponse r) {
        if (r.getMessageId()!=null) {
            mMessageList.add(r);
            notifyItemInserted(mMessageList.size() - 1);
        }
    }


    public void addAll(List<ChatListResponse> viewPost_Results) {
        for (ChatListResponse result : viewPost_Results) {
            add(result);
        }
    }
    public void addFirst(ChatListResponse r) {
        if (r.getMessageId()!=null) {
            mMessageList.add(0,r);
            notifyItemInserted(0);
        }
    }
    public void addFirstAll(List<ChatListResponse> viewPost_Results) {
        for (ChatListResponse result : viewPost_Results) {
            addFirst(result);
        }
    }
    public void remove(ChatListResponse r) {
        try {
            int position = mMessageList.indexOf(r);
            if (position > -1) {
                mMessageList.remove(position);
                notifyItemRemoved(position);
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }
    public void refreshData(ChatListResponse r) {
        if (r.getMessageId()!=null) {
            mMessageList.add(r);
           // notifyItemInserted(0);
            notifyItemInserted(mMessageList.size());


            notifyDataSetChanged();

        }

    }
    public void clear() {
        try {
            while (getItemCount() > 0) {
                remove(getItem(0));
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }
    public ChatListResponse getItem(int position) {
        try {
            return mMessageList.get(position);
        }catch (Exception e){
            e.printStackTrace();
            return mMessageList.get(position);
        }

    }

    public interface chatClickListener{
      public void onUserImageView(View view,String imageString);
    }

}
