package com.chetaru.FreshersApp.view.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;

import com.chetaru.FreshersApp.R;
import com.chetaru.FreshersApp.service.model.TagList;
import com.otaliastudios.autocomplete.RecyclerViewPresenter;

import java.util.ArrayList;
import java.util.List;

public class HashPresenter extends RecyclerViewPresenter<TagList> {
    @SuppressWarnings("WeakerAccess")
    protected Adapter adapter;
    Context mContext;
    List<TagList> hashList;

    public HashPresenter(Context context, List<TagList> hash) {
        super(context);
        this.mContext=context;
        this.hashList=hash;
    }

    @Override
    protected RecyclerView.Adapter instantiateAdapter() {
        adapter = new Adapter();
        return adapter;
    }
    public  List<TagList> getTagLists() {
        return hashList;
    }

    @Override
    protected void onQuery(@Nullable CharSequence query) {
        List<TagList> all = hashList;
        if (TextUtils.isEmpty(query)) {
            adapter.setData(all);
        } else {
            query = query.toString().toLowerCase();
            List<TagList> list = new ArrayList<>();
            for (TagList u : all) {
                if (u.getTagName().toLowerCase().contains(query) ||
                        u.getTagName().toLowerCase().contains(query)) {
                    list.add(u);
                }
            }
            adapter.setData(list);
            Log.e("UserPresenter", "found "+list.size()+" users for query "+query);
        }
        adapter.notifyDataSetChanged();
    }

    protected class Adapter extends RecyclerView.Adapter<Adapter.Holder> {

        private List<TagList> tagData;

        @SuppressWarnings("WeakerAccess")
        protected class Holder extends RecyclerView.ViewHolder {
            private View root;
            private TextView fullname;
            private TextView username;
            Holder(View itemView) {
                super(itemView);
                root = itemView;
                fullname = itemView.findViewById(R.id.fullname);
                username = itemView.findViewById(R.id.username);
            }
        }

        @SuppressWarnings("WeakerAccess")
        public void setData(@Nullable List<TagList> data) {
            this.tagData = data;
        }


        @Override
        public int getItemCount() {
            //return (isEmpty()) ? 1 : data.size();
            try {
                return tagData.size();
            }catch (Exception e){
                e.printStackTrace();
                return 0;
            }
        }

        @NonNull
        @Override
        public Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            return new Holder(LayoutInflater.from(getContext()).inflate(R.layout.user, parent, false));
        }

        private boolean isEmpty() {
            return tagData == null || tagData.isEmpty();
        }

        @SuppressLint("SetTextI18n")
        @Override
        public void onBindViewHolder(@NonNull Holder holder, int position) {
            if (isEmpty()) {
                holder.fullname.setText("No user here!");
                holder.username.setText("Sorry!");
                holder.root.setOnClickListener(null);
                return;
            }
            final TagList tagList = tagData.get(position);
            //holder.fullname.setText(tagList.getTagName());
            holder.username.setText("#" + tagList.getTagName());
            holder.root.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    tagList.setSelected(!tagList.isSelected());
                    dispatchClick(tagList);

                }
            });
        }
    }
}
