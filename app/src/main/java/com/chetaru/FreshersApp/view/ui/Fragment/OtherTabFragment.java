package com.chetaru.FreshersApp.view.ui.Fragment;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.chetaru.FreshersApp.R;
import com.chetaru.FreshersApp.service.model.UserViewPost;
import com.chetaru.FreshersApp.service.retrofit.BaseRequest;
import com.chetaru.FreshersApp.service.retrofit.RequestReciever;
import com.chetaru.FreshersApp.utility.SessionParam;
import com.chetaru.FreshersApp.utility.Utility;
import com.chetaru.FreshersApp.utility.Validations;
import com.google.android.material.tabs.TabLayout;
import com.google.gson.Gson;
import com.google.gson.JsonObject;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link OtherTabFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class OtherTabFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    String connection="",headerCall="";
    UserViewPost viewPost;
    int personId=0,position=0,connectionStatus=0;

    public OtherTabFragment() {
        // Required empty public constructor
    }

    Utility utility;
    SessionParam sessionParam;
    String tokenId="";
    String userId="";
    BaseRequest baseRequest;
    private TabLayout tabLayout;
    private ViewPager viewPager;

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment OtherTabFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static OtherTabFragment newInstance(String param1, String param2) {
        OtherTabFragment fragment = new OtherTabFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_other_tab, container, false);
        tabLayout=view.findViewById(R.id.other_tab_layout);
        viewPager=view.findViewById(R.id.other_pager);
        tabLayout.setupWithViewPager(viewPager);
        sessionParam=new SessionParam(getContext());
        utility=new Utility();
        utility.getProgerss(getContext());
        //userId=sessionParam.id;
        Bundle args = getArguments();
        if (args != null) {
            viewPost= (UserViewPost) getArguments().getSerializable("viewPost");
            personId=getArguments().getInt("personId");

            getStatus(personId);
            position=getArguments().getInt("position");
            connection=getArguments().getString("connection");
            connectionStatus=getArguments().getInt("connectionStatus");
            headerCall=getArguments().getString("header");
            if (viewPost!=null)
            if (viewPost.getUserConnectionStatus()==2){
                connectionStatus=2;
            }
        }
        //setupTabTitle();
        //
        if (connectionStatus==2){
            setupViewPager(viewPager);
        }
        initView();
        if (personId!=0) {
            viewPager.setCurrentItem(0);
        }
        if (!Validations.isEmptyString(connection)){
            if (connection.equals("headerLayout")){
                viewPager.setCurrentItem(0);
            }else {
                viewPager.setCurrentItem(2);
            }
        }
        if (position>0){
            viewPager.setCurrentItem(1);
        }

        /*if (viewPost==null){
            viewPager.setCurrentItem(1);
        }*/
        return view;
    }

    private void getStatus(int personId) {
        utility.loading();
        baseRequest = new BaseRequest(getContext());
        baseRequest.setBaseRequestListner(new RequestReciever() {
            @Override
            public void onSuccess(int requestCode, String Json, Object object) {
                try {

                    Gson gson = new Gson();
                    JSONObject jsonObject = new JSONObject(Json);
                    JSONObject response= (JSONObject) jsonObject.get("data");
                    try {
                        connectionStatus  =response.getInt("connectionStatus");
                        utility.dismiss();
                        setupViewPager(viewPager);

                    }catch (Exception e){
                        e.printStackTrace();
                        utility.dismiss();
                        setupViewPager(viewPager);
                    }

                    // JSONArray jsonArray = new JSONArray(object.toString());
                    //localeList = baseRequest.getDataList(jsonArray, LocaleList.class);
                    /*connectionStatus :- 1 for pending, 2 for accepted, 3 for decline, 4 for unfriend, 5 for blocked*/



                }catch (Exception e){
                    utility.dismiss();
                    setupViewPager(viewPager);
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int requestCode, String errorCode, String message) {
                //utility.showToast(getBaseContext(),message);
                utility.dismiss();
                setupViewPager(viewPager);
            }

            @Override
            public void onNetworkFailure(int requestCode, String message) {
                utility.showToast(getContext(),message);
                utility.dismiss();
            }
        });



        JsonObject main_object = new JsonObject();
        main_object.addProperty("userId",personId );
        baseRequest.callAPIPostWOLoader(1, main_object, getString(R.string.api_ConnectionStatus));

    }

    private void initView() {
    }

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getChildFragmentManager());
        adapter.addFrag( UserProfileFragment.newInstance(viewPost,personId,position), "User Profile");
       // if (sessionParam.premieUser||viewPost.getUserConnectionStatus()>0){
       if (connectionStatus==2){
           adapter.addFrag(OtherUserUploadFragment.newInstance(viewPost, personId, position), "User Uploads");
       }else {
           if (sessionParam.premieUser) {
               adapter.addFrag(OtherUserUploadFragment.newInstance(viewPost, personId, position), "User Uploads");
           } else {
               adapter.addFrag(PremiumMsgFragment.newInstance("otherTabBack",
                       "You must have a premium membership to view their previous posts.",
                       "You are not yet connected to this user."), "User Uploads");
           }
       }
       // adapter.addFrag( OtherUserUploadFragment.newInstance(viewPost,personId,position), "User Uploads");

        adapter.addFrag( ConnectFragment.newInstance(viewPost,personId,""), "Connect");
        viewPager.setAdapter(adapter);
        if (personId!=0) {
            viewPager.setCurrentItem(0);
        }
        if (!Validations.isEmptyString(connection)){
            if (connection.equals("headerLayout")){
                viewPager.setCurrentItem(0);
            }else {
                viewPager.setCurrentItem(2);
            }
        }
        if (position>0){
            viewPager.setCurrentItem(1);
        }
    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFrag(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }
}
