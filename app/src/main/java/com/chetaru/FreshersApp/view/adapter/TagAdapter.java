package com.chetaru.FreshersApp.view.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.chetaru.FreshersApp.R;
import com.chetaru.FreshersApp.service.model.TagList;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class TagAdapter extends RecyclerView.Adapter<TagAdapter.ViewHolder>
        implements Filterable {

    private Context context;
    private List<TagList> tagLists;
    private List<TagList> tagListFiltered;
    private TagAdapterListener listener;
    // View Types
    private static final int ITEM = 0;
    private static final int LOADING = 1;
    private boolean isLoadingAdded = false;
    String newHash;

    public TagAdapter(ArrayList<TagList> tagLists, Context context) {
        this.context = context;
        this.tagLists = tagLists;
        this.tagListFiltered = tagLists;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        //public TextView name, phone;
        //public ImageView thumbnail;
        ImageView checkSelectImage;
        TextView hashTagName;
        LinearLayout newHashLinear;
        LinearLayout tagHashLinear;

        public ViewHolder(View view) {
            super(view);
            checkSelectImage=view.findViewById(R.id.check_selection_image);
             hashTagName=view.findViewById(R.id.hash_tag_name);
             newHashLinear=view.findViewById(R.id.new_hash_layout);
             tagHashLinear=view.findViewById(R.id.tag_list_layout);
            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    // send selected contact in callback
                   // listener.onTagSelected(tagLists.get(getAdapterPosition()));
                }
            });
        }
    }

    public TagAdapter( List<TagList> contactList,Context context, TagAdapterListener listener) {
        this.context = context;
        this.listener = listener;
        this.tagLists = contactList;
        this.tagListFiltered = contactList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        ViewHolder viewHolder = null;
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        /*View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.hash_tag_row_item, parent, false);

        return new MyViewHolder(itemView);*/

        switch (viewType) {
            case ITEM:
                View viewItem = inflater.inflate(R.layout.hash_tag_row_item, parent, false);
                viewHolder = new ViewHolder(viewItem);
                break;
            case LOADING:
                View viewLoading = inflater.inflate(R.layout.new_hash_tag_msg, parent, false);
                viewHolder = new LoadingVH(viewLoading);
                break;
        }
        return viewHolder;
    }
    @Override
    public void onBindViewHolder(ViewHolder mholder, final int position) {
        //final TagList contact = contactListFiltered.get(position);
        switch (getItemViewType(position)) {


            case ITEM:
                final ViewHolder holder = (ViewHolder) mholder;
                holder.hashTagName.setText(tagLists.get(position).getTagName());
                holder.checkSelectImage.setVisibility(tagLists.get(position).isSelected() ? View.VISIBLE : View.INVISIBLE);

                holder.newHashLinear.setVisibility(View.GONE);
                holder.tagHashLinear.setVisibility(View.VISIBLE);
                holder.itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        tagLists.get(position).setSelected(!tagLists.get(position).isSelected());
                        // holder.view.setBackgroundColor(model.isSelected() ? Color.CYAN : Color.WHITE);
                        holder.checkSelectImage.setVisibility(tagLists.get(position).isSelected() ? View.VISIBLE : View.INVISIBLE);
                        listener.onTagSelected(tagLists.get(position));
                    }
                });

                break;

            case LOADING:
               final LoadingVH loadingVH = (LoadingVH) mholder;
                loadingVH.hashButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        //tagLists.clear();
                        TagList newValue=new TagList();
                        newValue.setTagName(newHash);
                        newValue.setSelected(true);
                        tagLists.add(newValue);
                        tagLists.addAll(tagListFiltered);
                        tagListFiltered.add(newValue);
                        notifyDataSetChanged();
                      listener.onNewTagSelected(newHash);
                      listener.onTagSelected(newValue);

                        }
                });
                break;
        }

    }

    public List<TagList> getTagLists() {
        return tagLists;
    }

    @Override
    public int getItemViewType(int position) {
        //return (position == tagLists.size() - 1 && isLoadingAdded) ? LOADING : ITEM;
        if (tagLists.size()>0) {
            return ITEM;

        } else {
            return LOADING;
        }
//        if(mFilteredList.size()<5){
//            return mFilteredList.size() - 1;
//        }else{
//            return (position == mFilteredList.size() - 1 && isLoadingAdded) ? LOADING : ITEM;
//        }
    }
    @Override
    public int getItemCount() {

        //return contactListFiltered.size();
            if (tagLists.size()>0) {
                return tagLists.size();
            }else {
                return 1;
            }

        //return tagLists == null ? 0 : tagLists.size();

    }



    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    tagLists = tagListFiltered;
                } else {
                    List<TagList> filteredList = new ArrayList<>();
                    for (TagList row : tagListFiltered) {

                        // name match condition. this might differ depending on your requirement
                        // here we are looking for name or phone number match
                        if (row.getTagName().toLowerCase().contains(charString.toLowerCase()) ) {
                            filteredList.add(row);
                        }else{
                            //isLoadingAdded = true;
                            //getItemViewType(LOADING);
                            newHash=charString.toLowerCase();
                        }
                    }

                    tagLists = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = tagLists;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                tagLists = (ArrayList<TagList>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }


    public interface TagAdapterListener {
        void onTagSelected(TagList tagList);
        void onNewTagSelected(String newHash);
    }

    protected class LoadingVH extends ViewHolder implements View.OnClickListener {
        Button hashButton;

        public LoadingVH(View itemView) {
            super(itemView);
            hashButton = itemView.findViewById(R.id.new_hash_button);

        }

        @Override
        public void onClick(View view) {
            switch (view.getId()) {

            }
        }
    }

}
