package com.chetaru.FreshersApp.view.ui;

import androidx.annotation.Nullable;

import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;


import com.chetaru.FreshersApp.R;
import com.chetaru.FreshersApp.service.model.User;
import com.chetaru.FreshersApp.service.model.UserResponse;
import com.chetaru.FreshersApp.service.retrofit.BaseRequest;
import com.chetaru.FreshersApp.service.retrofit.Functions;
import com.chetaru.FreshersApp.service.retrofit.RequestReciever;
import com.chetaru.FreshersApp.utility.BaseActivity;
import com.chetaru.FreshersApp.utility.SessionParam;
import com.chetaru.FreshersApp.utility.Utility;
import com.chetaru.FreshersApp.utility.Validations;

import com.chetaru.FreshersApp.viewModel.ForgetPassModel;
import com.google.gson.JsonObject;


import org.json.JSONArray;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ForgetPassword_Activity extends BaseActivity implements View.OnClickListener{

    ForgetPassModel viewModel;

    @Nullable
    @BindView(R.id.user_name_for_pass_et)
    EditText userNameEdit;
    @Nullable
    @BindView(R.id.ver_code_for_pass_et)
    EditText verCodeEdit;
    @Nullable
    @BindView(R.id.new_pass_for_pass_et)
    EditText newPassEdit;
    @Nullable
    @BindView(R.id.con_pass_for_pass_et)
    EditText confrimPassEdit;
    @Nullable
    @BindView(R.id.user_name_forget_button)
    Button userNameFPButton;
    @Nullable
    @BindView(R.id.verification_forget_button)
    Button verificationFPButton;
    @Nullable
    @BindView(R.id.password_forget_button)
    Button passwordFPButton;
    SessionParam sessionParam;
    Utility utility;
    Integer code;
    String userNameString,verCodeString,newPasswordString,conPasswordString,tokenId="";
    BaseRequest baseRequest;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try {
            requestWindowFeature(Window.FEATURE_NO_TITLE);
            this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        }catch (Exception e){
            e.printStackTrace();
        }
        setContentView(R.layout.activity_forget_password_);
        ButterKnife.bind(this);
        sessionParam=new SessionParam(this);
        utility=new Utility();
        utility.getProgerss(this);
        userNameFPButton.setOnClickListener(this);
        verificationFPButton.setOnClickListener(this);
        passwordFPButton.setOnClickListener(this);
        viewModel = ViewModelProviders.of(this).get(ForgetPassModel.class);
        utility.getProgerss(this);
    }

    @OnClick({R.id.user_name_forget_button,R.id.verification_forget_button,R.id.password_forget_button})
    @Override
    public void onClick(View view) {

        switch (view.getId()){
            case R.id.user_name_forget_button:
                hideKeyboard();
                userNameString=userNameEdit.getText().toString().trim();
                if (Validations.isEmptyString(userNameString)){
                    utility.showToast(this,"Please enter Phone Number/Email.");
                    return;
                }
                forgetpassUserName(userNameString);
                break;
            case R.id.verification_forget_button:
                hideKeyboard();
                verCodeString=verCodeEdit.getText().toString().trim();
                if (Validations.isEmptyString(verCodeString)){
                    utility.showToast(this,"Please enter valid code.");
                    return;
                }
                verifyCode(verCodeString);
                break;
            case R.id.password_forget_button:
                hideKeyboard();
                newPasswordString=newPassEdit.getText().toString().trim();
                conPasswordString=confrimPassEdit.getText().toString().trim();
                if (Validations.isEmptyString(newPasswordString)){
                    utility.showToast(this,"Please enter Password.");
                    return;
                }
                if (Validations.isEmptyString(conPasswordString)){
                    utility.showToast(this,"Please enter Confirm Password.");
                    return;
                }
                if (newPasswordString.equals(conPasswordString)) {
                    //step 1
                    passwordFPButton.setVisibility(View.GONE);
                    confrimPassEdit.setVisibility(View.GONE);
                    newPassEdit.setVisibility(View.GONE);
                    userNameFPButton.setVisibility(View.VISIBLE);
                     passReset(userNameString,conPasswordString,tokenId);

                }else {
                    utility.showToast(ForgetPassword_Activity.this, "Password and Confirm Password did not match.");
                }
                break;

        }

    }

    private void forgetpassUser(String userNameString) {

            baseRequest = new BaseRequest(this);
            baseRequest.setBaseRequestListner(new RequestReciever() {
                @Override
                public void onSuccess(int requestCode, String Json, Object object) {
                    JSONArray jsonArray;
                    jsonArray = (JSONArray)object;
                   // User userData = baseRequest.getDataList(jsonArray,User.class);
                   /* User userData = repos.getUserData();
                    Integer type = userData.getType();
                    if (repos.getStatus()) {
                        utility.showToast(ForgetPassword_Activity.this, repos.getMessage());
                        User userData = repos.getUserData();
                        Integer type = userData.getType();
                        code = userData.getRandomNumber();
                        tokenId = userData.getToken();
                        if (type == 2) {
                            Log.d("codeJson", repos.getUserData().getRandomNumber().toString());
                            userNameFPButton.setVisibility(View.GONE);
                            userNameEdit.setClickable(false);
                            userNameEdit.setEnabled(false);
                            userNameEdit.getNextFocusUpId();
                            verificationFPButton.setVisibility(View.VISIBLE);
                            verCodeEdit.setVisibility(View.VISIBLE);

                        }
                    }else {
                        utility.showToast(ForgetPassword_Activity.this, repos.getMessage());
                    }*/
                }

                @Override
                public void onFailure(int requestCode, String errorCode, String message) {
                    utility.showToast(ForgetPassword_Activity.this, message);
                }

                @Override
                public void onNetworkFailure(int requestCode, String message) {
                    utility.showToast(ForgetPassword_Activity.this, message);
                }
            });
            JsonObject object = Functions.getClient().getJsonMapObject("userName", userNameString);
            baseRequest.callAPIPost(1, object, getString(R.string.api_logout));

    }


    private void verifyCode(String verCodeString) {
        Integer inputCode=1;
        try {
             inputCode = Integer.valueOf(verCodeString);
        }catch (Exception e){
            e.printStackTrace();
        }

        if (code.equals(inputCode)) {

            verCodeEdit.setVisibility(View.GONE);
            verificationFPButton.setVisibility(View.GONE);
            passwordFPButton.setVisibility(View.VISIBLE);
            newPassEdit.setVisibility(View.VISIBLE);
            confrimPassEdit.setVisibility(View.VISIBLE);
            utility.showToast(ForgetPassword_Activity.this, "Code Verified. Please set your password.");
        } else {
            utility.showToast(ForgetPassword_Activity.this, "Please enter valid code.");
        }

    }
    private void passReset(String userNameString, String conPasswordString, String tokenId) {

        utility.loading();
        viewModel.getpasswordReset(userNameString,conPasswordString,tokenId).observe(this, new Observer<UserResponse>() {
            @Override
            public void onChanged(UserResponse repos) {
                utility.dismiss();
                if (repos!=null) {
                    if (repos.getStatus()) {

                        utility.showToast(ForgetPassword_Activity.this, repos.getMessage());
                        Intent intent = new Intent(ForgetPassword_Activity.this, LoginActivity.class);
                        startActivity(intent);
                        finish();
                    }else {
                        utility.showToast(ForgetPassword_Activity.this, repos.getMessage());
                    }
                }
            }

        });
    }

    private void forgetpassUserName(String userName) {
        utility.loading();
        viewModel.getpassword(userName).observe(this, new Observer<UserResponse>() {
            @Override
            public void onChanged(UserResponse repos) {
                utility.dismiss();
                if (repos!=null) {

                    if (repos.getStatus()) {
                        utility.showToast(ForgetPassword_Activity.this, repos.getMessage());
                        User userData = repos.getUserData();
                        Integer type = userData.getType();
                        code = userData.getRandomNumber();
                        tokenId = userData.getToken();
                        if (type == 2) {
                            Log.d("codeJson", repos.getUserData().getRandomNumber().toString());
                            userNameFPButton.setVisibility(View.GONE);
                            userNameEdit.setClickable(false);
                            userNameEdit.setEnabled(false);
                            userNameEdit.getNextFocusUpId();
                            verificationFPButton.setVisibility(View.VISIBLE);
                            verCodeEdit.setVisibility(View.VISIBLE);

                        }
                    }else {
                        utility.showToast(ForgetPassword_Activity.this, repos.getMessage());
                    }
                }
            }

        });
    }
    protected void hideKeyboard()
    {
        View view = this.getCurrentFocus();
        if (view != null)
        {
            ((InputMethodManager) this.getSystemService(Context.INPUT_METHOD_SERVICE))
                    .hideSoftInputFromWindow(view.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        }
    }
}
