package com.chetaru.FreshersApp.view.callback;

import android.view.View;

import com.chetaru.FreshersApp.service.model.ConnListResponse;

public interface ConnectionListCallback {
    void ConnectionChatOpen(ConnListResponse connData);
    void userLogoClick(View view,String imageString);
}
