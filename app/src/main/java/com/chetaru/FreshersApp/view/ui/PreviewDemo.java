package com.chetaru.FreshersApp.view.ui;


import androidx.core.app.ActivityCompat;

import androidx.fragment.app.FragmentTransaction;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.database.Cursor;
import android.graphics.Bitmap;

import android.graphics.BitmapFactory;
import android.graphics.ImageFormat;
import android.graphics.Matrix;



import android.hardware.Camera;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.util.Log;
import android.view.Display;
import android.view.KeyEvent;
import android.view.Surface;


import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.chetaru.FreshersApp.R;
import com.chetaru.FreshersApp.utility.BaseActivity;
import com.chetaru.FreshersApp.utility.CameraPreview;
import com.chetaru.FreshersApp.utility.FileCompressor;

import com.chetaru.FreshersApp.utility.Utility;
import com.chetaru.FreshersApp.view.ui.Activity.HomeActivity;
import com.chetaru.FreshersApp.view.ui.Fragment.UploadFragment;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import id.zelory.compressor.Compressor;


public class PreviewDemo extends BaseActivity implements View.OnClickListener {

    private static float PREVIEW_SIZE_FACTOR = 0.80f;
    public static final int MEDIA_TYPE_IMAGE = 1;
    private Camera mCamera;
    private CameraPreview mPreview;
    private static PreviewDemo mInstance;
    private ImageView button_capture, button_switch,button_flash,button_gallary;

    private int currentCameraId = Camera.CameraInfo.CAMERA_FACING_BACK;
    private FrameLayout previewFrame;
    private static final int REQUEST_CAMERA_CAPTURE = 1;
    private int SELECT_FILE = 2, RESULT_OK = -1;
    private int requestCode=101;
    Utility utility;
    String FilePathString;
    private Bitmap bitmap = null;
    String imgBase64 = "";
    Boolean flashValue=true;
    File sourceFile;

    private Bitmap bmp,bmp1;
    private ByteArrayOutputStream bos;
    private BitmapFactory.Options options,o,o2;
    private FileInputStream fis;
    ByteArrayInputStream fis2;
    private FileOutputStream fos;
    private File dir_image2,dir_image;
    private RelativeLayout CamView;
    FileCompressor mCompressor;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try {
            requestWindowFeature(Window.FEATURE_NO_TITLE);
            this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        }catch (Exception e){
            e.printStackTrace();
        }
        setContentView(R.layout.activity_preview_demo);

        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, requestCode);

    utility=new Utility();
        mCompressor = new FileCompressor(this);
        mInstance = this;
        mCamera = getCameraInstance(currentCameraId);// Create an instance of Camera

        if(mCamera==null){
            finish();
        }

        // Create our Preview view and set it as the content of our activity.
        mPreview = new CameraPreview(this, mCamera);
        previewFrame = (FrameLayout) findViewById(R.id.camera_preview);
        previewFrame.addView(mPreview);

        // Add a listener to the Capture button
        button_capture = (ImageView) findViewById(R.id.button_capture);
        button_switch = (ImageView) findViewById(R.id.button_switch);
        button_flash = (ImageView) findViewById(R.id.button_flash);
        button_gallary = (ImageView) findViewById(R.id.image_gallary);

        if(!isFrontCameraAvailable())
            button_switch.setVisibility(View.GONE);

        button_capture.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Utility.disableDoubleClick(v);
                        // get an image from the camera
                        mCamera.takePicture(null, null, mPicture);
                    }
                }
        );
        button_flash.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Utility.disableDoubleClick(v);
                if (flashValue) {
                    setFlashOnOff(flashValue);
                    flashValue=false;
                }else{
                    setFlashOnOff(flashValue);
                    flashValue=true;
                }
            }
        });
        button_gallary.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               /* String path = Environment.getExternalStorageDirectory().toString()+"/Pictures";
                Log.d("Files", "Path: " + path);
                File directory = new File(path);
                File[] files = directory.listFiles();
                Log.d("Files", "Size: "+ files.length);
                for (int i = 0; i < files.length; i++)
                {
                    Log.d("Files", "FileName:" + files[i].getName());
                }*/
                selectFromFile();
            }
        });
        button_switch.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Utility.disableDoubleClick(v);
                        // get an switch image to front camera
                        if(mCamera==null)
                            return;

                        mCamera.stopPreview();
                        mCamera.release();
                        mCamera = null;
                        mPreview = null;
                        previewFrame.removeAllViews();

                        //swap the id of the camera to be used
                        if(currentCameraId == Camera.CameraInfo.CAMERA_FACING_BACK){
                            currentCameraId = Camera.CameraInfo.CAMERA_FACING_FRONT;
                        }
                        else {
                            currentCameraId = Camera.CameraInfo.CAMERA_FACING_BACK;
                        }
                        mCamera = getCameraInstance(currentCameraId);
                        // Create our Preview view and set it as the content of our activity.
                        mPreview = new CameraPreview(PreviewDemo.this, mCamera);
                        previewFrame.addView(mPreview);
                    }
                }
        );
    }

    private void selectFromFile() {
        Intent intent = new Intent(
                Intent.ACTION_PICK,
                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        intent.setType("image/*");
        startActivityForResult(Intent.createChooser(intent, "Select File"),
                SELECT_FILE);
    }

    private void setFlashOnOff(boolean flash) {
        if (flash) {
            Camera.Parameters params = mCamera.getParameters();
            params.setFlashMode(Camera.Parameters.FLASH_MODE_TORCH);

            mCamera.setParameters(params);
        } else {
            Camera.Parameters params = mCamera.getParameters();
            params.setFlashMode(Camera.Parameters.FLASH_MODE_OFF);
            mCamera.setParameters(params);

        }
    }
    /** A safe way to get an instance of the Camera object. */
    public static Camera getCameraInstance(int cameraId){
        Camera c = null;
        try {
            c = Camera.open(cameraId); // attempt to get a Camera instance
            final Camera.Parameters params = c.getParameters();
            List<String> focusModes = params.getSupportedFocusModes();
            if (focusModes.contains(Camera.Parameters.FOCUS_MODE_AUTO)) {
                // Autofocus mode is supported
                params.setFocusMode(Camera.Parameters.FOCUS_MODE_AUTO);
                c.setParameters(params);
            }
            final Camera.Size size = getOptimalSize(c);
            params.setPreviewSize(size.width, size.height);
            params.setPictureFormat(ImageFormat.JPEG);
            params.setJpegQuality(100);
            c.setParameters(params);
            setCameraDisplayOrientation(mInstance, cameraId, c);
        }
        catch (Exception e){
            // Camera is not available (in use or does not exist)
        }
        return c; // returns null if camera is unavailable
    }

    public static void setCameraDisplayOrientation(Activity activity, int cameraId, android.hardware.Camera camera) {

        if(camera==null)
            return;

        android.hardware.Camera.CameraInfo info =
                new android.hardware.Camera.CameraInfo();

        android.hardware.Camera.getCameraInfo(cameraId, info);

        int rotation = activity.getWindowManager().getDefaultDisplay().getRotation();
        int degrees = 0;

        switch (rotation) {
            case Surface.ROTATION_0: degrees = 0; break;
            case Surface.ROTATION_90: degrees = 90; break;
            case Surface.ROTATION_180: degrees = 180; break;
            case Surface.ROTATION_270: degrees = 270; break;
        }

        int result;
        if (info.facing == Camera.CameraInfo.CAMERA_FACING_FRONT) {
            result = (info.orientation + degrees) % 360;
            result = (360 - result) % 360;  // compensate the mirror
        } else {  // back-facing
            result = (info.orientation - degrees + 360) % 360;
        }
        camera.setDisplayOrientation(result);
    }

    private static Camera.Size getOptimalSize(Camera camera) {
        Camera.Size result = null;
        final Camera.Parameters parameters = camera.getParameters();
        Log.i(TAG, "window width: " + getWidth() + ", height: " + getHeight());
        for (final Camera.Size size : parameters.getSupportedPreviewSizes()) {
            if (size.width <= getWidth() * PREVIEW_SIZE_FACTOR && size.height <= getHeight() * PREVIEW_SIZE_FACTOR) {
                if (result == null) {
                    result = size;
                } else {
                    final int resultArea = result.width * result.height;
                    final int newArea = size.width * size.height;

                    if (newArea > resultArea) {
                        result = size;
                    }
                }
            }
        }
        if (result == null) {
            result = parameters.getSupportedPreviewSizes().get(0);
        }
        Log.i(TAG, "Using PreviewSize: " + result.width + " x " + result.height);
        return result;
    }

    private BitmapFactory.Options getBitmapOption(Camera camera){
        BitmapFactory.Options opt;

        opt = new BitmapFactory.Options();
        opt.inTempStorage = new byte[16 * 1024];
        Camera.Parameters parameters = camera.getParameters();
        Camera.Size size = parameters.getPictureSize();

        int height11 = size.height;
        int width11 = size.width;
        float mb = (width11 * height11) / 1024000;

        if (mb > 4f)
            opt.inSampleSize = 4;
        else if (mb > 3f)
            opt.inSampleSize = 2;

        return opt;
    }

    private static float getWidth() {
        return mInstance.getWindow().getDecorView().getWidth();
    }

    private static float getHeight() {
        return mInstance.getWindow().getDecorView().getHeight();
    }

    private static String TAG = PreviewDemo.class.getSimpleName();
    private Camera.PictureCallback mPicture = new Camera.PictureCallback() {

        @Override
        public void onPictureTaken(byte[] data, Camera camera) {


            FileOutputStream outStream = null;
            try {
                System.out.println("jpegCallback begin");
                camera.startPreview();
                String dir = Environment.getExternalStorageDirectory().getAbsolutePath();
               /* File storagePath =  new File(dir+
                        File.separator+"/mnt/sdcard/");*/


                File myImage = new File(dir, "fresher.jpg");
                if (! myImage.exists()){
                    myImage.mkdirs();
                }
                //create the file
               // myImage.createNewFile();

                try {
                    FileOutputStream fos = new FileOutputStream(myImage);
                    if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT ) {


                        // Notice that width and height are reversed
                        Bitmap bm = BitmapFactory.decodeByteArray(data , 0, data.length, getBitmapOption(camera));
                        Bitmap scaled = bm;
                        int w = scaled.getWidth();
                        int h = scaled.getHeight();
                        // Setting post rotate to 90

                        Matrix mtx = new Matrix();
                        try{
                            //int rotate = rotateAngle(file.getPath(), currentCameraId);
                            android.hardware.Camera.CameraInfo info =
                                    new android.hardware.Camera.CameraInfo();
                            android.hardware.Camera.getCameraInfo(currentCameraId, info);
                            mtx.postRotate((w>h)?info.orientation:90);
                        }
                        catch (Exception ioe){
                            ioe.printStackTrace();
                        }

                        // Rotating Bitmap
                        bm = Bitmap.createBitmap(scaled, 0, 0, w, h, mtx, true);
                       // bm = Bitmap.createBitmap(scaled, 0, 0, w, h );
                        bm.compress(Bitmap.CompressFormat.JPEG, 80, fos);
                        fos.close();

                    }
                    else {
                        fos.write(data);
                        fos.close();
                    }
                } catch (FileNotFoundException e) {
                    Log.d(TAG, "File not found: " + e.getMessage());
                } catch (IOException e) {
                    Log.d(TAG, "Error accessing file: " + e.getMessage());
                }
                /*outStream = new FileOutputStream(myImage);
                outStream.write(data);
                outStream.close();*/
                refreshGallery(myImage);
            } catch (Exception e) {
                System.out.println("jpegCallback FileNotFoundException");
                e.printStackTrace();
            } finally {
                //resetCam();
                //camera.stopPreview();
                //camera.release();
            }


            /*File pictureFile = new File(Environment.getExternalStorageDirectory(), "pickImageResult.jpeg");
            File mSaveBit;
            if (pictureFile == null){
                Log.d(TAG, "Error creating media file, check storage permissions: ");
                return;
            }

            try {
                FileOutputStream fos = new FileOutputStream(pictureFile);
                if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT ) {
                    // Notice that width and height are reversed
                    Bitmap bm = BitmapFactory.decodeByteArray(data , 0, data.length, getBitmapOption(camera));
                    Bitmap scaled = bm;
                    int w = scaled.getWidth();
                    int h = scaled.getHeight();
                    // Setting post rotate to 90
                    Matrix mtx = new Matrix();
                    try{
                        //int rotate = rotateAngle(file.getPath(), currentCameraId);
                        android.hardware.Camera.CameraInfo info =
                                new android.hardware.Camera.CameraInfo();
                        android.hardware.Camera.getCameraInfo(currentCameraId, info);
                        mtx.postRotate((w>h)?info.orientation:0);
                    }
                    catch (Exception ioe){
                        ioe.printStackTrace();
                    }
                    // Rotating Bitmap
                    bm = Bitmap.createBitmap(scaled, 0, 0, w, h, mtx, true);
                    bm.compress(Bitmap.CompressFormat.JPEG, 60, fos);
                    fos.close();

                }
                else {
                    fos.write(data);
                    fos.close();
                }
            } catch (FileNotFoundException e) {
                Log.d(TAG, "File not found: " + e.getMessage());
            } catch (IOException e) {
                Log.d(TAG, "Error accessing file: " + e.getMessage());
            }*/
           // new SaveImageTask().execute(data);

           /* File pictureFile = new File(Environment.getExternalStorageDirectory(), "pickImageResult.jpeg");
            try {
                Camera.Parameters parameters = camera.getParameters();
                FileOutputStream fos = new FileOutputStream(pictureFile);
                ByteArrayOutputStream out = new ByteArrayOutputStream();
                YuvImage yuvImage = new YuvImage(data, parameters.getPreviewFormat(), parameters.getPreviewSize().width, parameters.getPreviewSize().height, null);
                yuvImage.compressToJpeg(new Rect(0, 0, parameters.getPreviewSize().width, parameters.getPreviewSize().height), 90, out);
                byte[] imageBytes = out.toByteArray();
                 Bitmap bitmap = BitmapFactory.decodeByteArray(imageBytes, 0, imageBytes.length);

                    Bitmap scaled = bitmap;
                    int w = scaled.getWidth();
                    int h = scaled.getHeight();
                    // Setting post rotate to 90
                    Matrix mtx = new Matrix();
                    try{
                        //int rotate = rotateAngle(file.getPath(), currentCameraId);
                        android.hardware.Camera.CameraInfo info =
                                new android.hardware.Camera.CameraInfo();
                        android.hardware.Camera.getCameraInfo(currentCameraId, info);
                        mtx.postRotate((w>h)?info.orientation:0);
                    }
                    catch (Exception ioe){
                        ioe.printStackTrace();
                    }
                    // Rotating Bitmap
                    bitmap = Bitmap.createBitmap(scaled, 0, 0, w, h, mtx, true);
                    bitmap.compress(Bitmap.CompressFormat.JPEG, 80, fos);
                out.flush();
                out.close();
                fos.close();
            }catch (Exception e){
                e.printStackTrace();
            }
            Bitmap bit=   getBitmap(pictureFile.getAbsolutePath());
             sourceFile=bitmapConvertToFile(bit);
        Intent intent = new Intent();
        intent.setData(Uri.fromFile(sourceFile));
        setResult(RESULT_OK, intent);
        finish();*/

            /*File file = new File(Environment.getExternalStorageDirectory() + "/" + File.separator + "test.txt");
            file.createNewFile();
            byte[] data1={1,1,0,0};
            //write the bytes in file
            if(file.exists())
            {
                OutputStream fo = new FileOutputStream(file);
                fo.write(data1);
                fo.close();
                System.out.println("file created: "+file);
                url = upload.upload(file);
            }

            //deleting the file
            file.delete();*/


        }
    };
    public Bitmap getBitmap(String path) {
        Bitmap bitmap=null;
        try {
            File f= new File(path);
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inPreferredConfig = Bitmap.Config.ARGB_8888;
            bitmap = BitmapFactory.decodeStream(new FileInputStream(f), null, options);
            //image.setImageBitmap(bitmap);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return bitmap ;
    }

    private void resetCam() {
        mCamera.startPreview();
    }
    public File bitmapConvertToFile(Bitmap bitmap) {

        FileOutputStream fileOutputStream = null;
        File bitmapFile = null;
        try {
            bitmapFile = new File(getExternalFilesDir(null), "pic_crop.jpg");
            if (!bitmapFile.exists()) {
                Log.i(TAG, "bitmapConvertToFile: file not exist");
            }

            fileOutputStream = new FileOutputStream(bitmapFile);
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, fileOutputStream);
            MediaScannerConnection.scanFile(getApplicationContext(), new String[] {bitmapFile.getAbsolutePath()},
                    null, null);
           /* MediaScannerConnection.scanFile(this, new String[]{bitmapFile.getAbsolutePath()}, null, new MediaScannerConnection.MediaScannerConnectionClient() {
                @Override
                public void onMediaScannerConnected() {

                }
                @Override
                public void onScanCompleted(final String path, final Uri uri) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {

                            Intent data = new Intent();
                            data.setData(uri);
                            setResult(RESULT_OK, data);
                            finish();
                        }
                    });
                }
            });*/
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (fileOutputStream != null) {
                try {
                    fileOutputStream.flush();
                    fileOutputStream.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }

        return bitmapFile;
    }
    public Bitmap decodeFile(File f) {
        Bitmap b = null;
        try {
            // Decode image size
            o = new BitmapFactory.Options();
            o.inJustDecodeBounds = true;

            fis = new FileInputStream(f);
            BitmapFactory.decodeStream(fis, null, o);
            fis.close();
            int IMAGE_MAX_SIZE = 1000;
            int scale = 1;
            if (o.outHeight > IMAGE_MAX_SIZE || o.outWidth > IMAGE_MAX_SIZE) {
                scale = (int) Math.pow(
                        2,
                        (int) Math.round(Math.log(IMAGE_MAX_SIZE
                                / (double) Math.max(o.outHeight, o.outWidth))
                                / Math.log(0.5)));
            }

            // Decode with inSampleSize
            o2 = new BitmapFactory.Options();
            o2.inSampleSize = scale;
            fis = new FileInputStream(f);
            b = BitmapFactory.decodeStream(fis, null, o2);
            fis.close();

        } catch (IOException e) {
            e.printStackTrace();
        }

        return b;
    }

    private boolean isFrontCameraAvailable(){
        Camera.CameraInfo ci = new Camera.CameraInfo();
        for (int i = 0; i < Camera.getNumberOfCameras(); i++) {
            Camera.getCameraInfo(i, ci);
            if (ci.facing == Camera.CameraInfo.CAMERA_FACING_FRONT)
                return true;
        }
        return false;
    }

    @Override
    public void onDestroy() {
        Log.d(TAG,"<<onDestroy");
        if (mCamera != null) {
            mCamera.stopPreview();
            mCamera.release();
            mCamera = null;
        }
        super.onDestroy();
    }


    @Override
    public void onClick(View view) {

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(data==null) {
            Log.i(TAG, "onActivityResult: data==null");
            return;
        }
        if (requestCode == SELECT_FILE) {
            try {
               // bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), data.getData());
              File   newFile = new File(utility.getPath(data.getData(), this));
                bitmap = new Compressor(this).compressToBitmap(newFile);
                /*imgBase64 = utility.image_to_Base64(mContext, utility.getPath(data.getData(), mContext));
                profilePics.setImageBitmap(bitmap);*/
                sourceFile=bitmapConvertToFile(bitmap);

                Intent intent = new Intent();
                intent.setData(Uri.fromFile(sourceFile));
                setResult(RESULT_OK, intent);
                finish();
            } catch (Exception e) {
                e.printStackTrace();
                Toast.makeText(this, "Image not set please try again", Toast.LENGTH_SHORT).show();
            }


        }

        Log.i(TAG, "onActivityResult: requestcode="+requestCode);
        switch (requestCode){

            case REQUEST_CAMERA_CAPTURE: {

               /* Uri imageUri = getPickImageResultUri(data);
                File imageFile = new File(imageUri.getPath());
                Intent intent = new Intent(this, CropActivity.class);
                intent.putExtra("file", imageFile.getAbsolutePath());
                startActivityForResult(intent, REQUEST_CROP);*/

                Uri imageUri = getPickImageResultUri(data);
                //bitmap = (Bitmap) data.getExtras().get("data");
                FilePathString=utility.getPath(imageUri,this);
                ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
                // profilePics.setImageBitmap(bitmap);

                imgBase64 = utility.image_to_Base64(this, utility.getPath(utility.getImageUri(this, bitmap), this));

                UploadFragment fragment=UploadFragment.newInstance(FilePathString,0);
                FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                transaction.replace(R.id.nav_host_fragment, fragment);
                transaction.addToBackStack(null);
                transaction.commit();
                /*CropImage.activity(imageUri)
                        .setGuidelines(CropImageView.Guidelines.ON)
                        .start(getActivity(), SettingsFragment.this);*/

                break;
            }

            default: {
                Log.i(TAG, "onActivityResult: default");
                break;
            }
        }
    }
    public Uri getPickImageResultUri(Intent data) {
        boolean isCamera = true;
        if (data != null && data.getData() != null) {
            String action = data.getAction();
            isCamera = action != null && action.equals(MediaStore.ACTION_IMAGE_CAPTURE);
        }
        return isCamera ? getCaptureImageOutputUri() : data.getData();
    }

    private Uri getCaptureImageOutputUri() {
        Uri outputFileUri = null;
        File getImage = getExternalCacheDir();
        if (getImage != null) {
            outputFileUri = Uri.fromFile(new File(getImage.getPath(), "pickImageResult.jpeg"));
        }
        return outputFileUri;
    }

    private String uriToPath(Uri imageUri) {

        String uriPath = null;

        String[] filePathColumn = {MediaStore.Images.Media.DATA};
        Cursor cursor = getContentResolver().query(imageUri,filePathColumn, null, null, null);
        cursor.moveToFirst();
        int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
        uriPath = cursor.getString(columnIndex);
        cursor.close();
        if (!TextUtils.isEmpty(uriPath)) {
            Log.v(TAG, "SELECT_PICTURE... " + uriPath);
        }

        return uriPath;
    }
    private class SaveImageTask extends AsyncTask<byte[], Void, Void> {

        @Override
        protected Void doInBackground(byte[]... data) {
            FileOutputStream outStream = null;

            // Write to SD Card
            try {
                File sdCard = Environment.getExternalStorageDirectory();
                File dir = new File (sdCard.getAbsolutePath() + "/camtest");
                dir.mkdirs();

                String fileName = String.format("%d.jpg", System.currentTimeMillis());
                File outFile = new File(dir, fileName);

                outStream = new FileOutputStream(outFile);
                outStream.write(data[0]);
                outStream.flush();
                outStream.close();

                Log.d(TAG, "onPictureTaken - wrote bytes: " + data.length + " to " + outFile.getAbsolutePath());

                refreshGallery(outFile);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
            }
            return null;
        }

    }
    private void refreshGallery(File file) {
     Bitmap bit=   getBitmap(file.getAbsolutePath());
        sourceFile=bitmapConvertToFile(bit);
        Intent intent = new Intent();
        intent.setData(Uri.fromFile(sourceFile));
        setResult(RESULT_OK, intent);
        finish();
       /* Intent mediaScanIntent = new Intent( Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
        mediaScanIntent.setData(Uri.fromFile(file));
        sendBroadcast(mediaScanIntent);*/
    }

    @SuppressLint("SimpleDateFormat")
    private File getOutputMediaFile(int type){

        File mediaStorageDir = getFileStorageDir(this, "Layout_test");

        /*
         *  Create the storage directory if it does not exist
         */
        if (! mediaStorageDir.exists()){
            if (!mediaStorageDir.mkdirs()){
                return null;
            }
        }

        /*
         * Create a media file name
         */
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        File mediaFile;
        if (type == MEDIA_TYPE_IMAGE){
            mediaFile = new File(mediaStorageDir.getPath() + File.separator +
                    "IMG_"+ timeStamp +".jpg");
        } else {
            return null;
        }

        return mediaFile;
    }

    public static File getFileStorageDir(Context context, String name) {

        String state = Environment.getExternalStorageState();
        File file = null;

        if (Environment.MEDIA_MOUNTED.equals(state)){

            file = new File(Environment.getExternalStorageDirectory()+"/layout", name);
            if ((!file.mkdirs()) && (!file.isDirectory())){
                Log.v(TAG, "Directory Creation Failed");
                return null;
            }

            Log.v(TAG, "Directory Created = " + file.getAbsolutePath());
        }else{
            Log.v(TAG, "External Storage Not Mounted! Problem!!!");
        }
        return file;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
       // finishAllActivities();
        Intent intent=new Intent(PreviewDemo.this, HomeActivity.class);
        startActivity(intent);
        finish();
        BaseActivity.finishAllActivitiesStatic();
    }
}
