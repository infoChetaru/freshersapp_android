package com.chetaru.FreshersApp.view.ui.camera;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.FrameLayout;

import com.chetaru.FreshersApp.R;

public class MainCameraActivity extends AppCompatActivity {

    private FrameLayout container;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_camera);
        container = findViewById(R.id.fragment_container);
    }


    @Override
    protected void onResume() {
        super.onResume();
        container.postDelayed((Runnable)(new Runnable() {
            public final void run() {
               // MainCameraActivity.getContaner(MainCameraActivity.this).setSystemUiVisibility(4871);
            }
        }), 500L);
    }
}
