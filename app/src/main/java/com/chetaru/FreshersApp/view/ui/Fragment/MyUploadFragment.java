package com.chetaru.FreshersApp.view.ui.Fragment;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;

import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.preference.PreferenceManager;
import android.provider.Settings;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.chetaru.FreshersApp.R;
import com.chetaru.FreshersApp.service.model.MyProfileResponse;
import com.chetaru.FreshersApp.service.model.MyUploads;
import com.chetaru.FreshersApp.service.model.UserViewPost;
import com.chetaru.FreshersApp.service.retrofit.BaseRequest;
import com.chetaru.FreshersApp.service.retrofit.Functions;
import com.chetaru.FreshersApp.service.retrofit.RequestReciever;
import com.chetaru.FreshersApp.utility.BaseActivity;
import com.chetaru.FreshersApp.utility.SessionParam;
import com.chetaru.FreshersApp.utility.Utility;
import com.chetaru.FreshersApp.view.adapter.MyUploadAdapter;
import com.chetaru.FreshersApp.view.adapter.TagImageAdapter;
import com.chetaru.FreshersApp.view.customView.ItemOffsetDecoration;
import com.chetaru.FreshersApp.view.ui.Activity.RootActivity;
import com.google.gson.Gson;
import com.google.gson.JsonObject;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link MyUploadFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class MyUploadFragment extends Fragment implements View.OnClickListener{
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private Unbinder unbinder;
    private BaseRequest baseRequest;
    Utility utility;
    SessionParam sessionParam;
    Context mContext;

    /*@BindView(R.id.post_button)
    ImageView postButton;*/

    //assign a layout id class
    @BindView(R.id.my_upload_recyclerview)
    RecyclerView myUploadRecyclerView;
    @BindView(R.id.my_upload_swap_refresh)
    SwipeRefreshLayout myUploadRefresh;

    @BindView(R.id.my_upload_layout)
    LinearLayout myUploadLayout;
    @BindView(R.id.recycler_my_upload)
    LinearLayout recyclerLayout;
    @BindView(R.id.back_upload_image)
    ImageView backImage;

    //initialize adapter and all
    LinearLayoutManager layoutManager;
    MyUploadAdapter uploadAdapter;
    MyUploadListenerFragment mListener;


    //List<UserViewPost> uploadList;
    List<UserViewPost> uploadList;
    int  userId=0;
    private static final int PAGE_START = 1;
    private boolean isLoading = false;
    private int currentPage = PAGE_START;
    private  int TOTAL_PAGES = 1;
    private boolean isLastPage = false;
    Date currentDate;
    String dateTime;


    public MyUploadFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment MyUploadFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static MyUploadFragment newInstance(String param1, String param2) {
        MyUploadFragment fragment = new MyUploadFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_my_upload, container, false);
        unbinder = ButterKnife.bind(this, view);
        mContext=getContext();
        uploadList=new ArrayList<>();
        utility=new Utility();
        sessionParam=new SessionParam(mContext);
        userId=sessionParam.id;
        backImage.setOnClickListener(this);
       currentDate = Calendar.getInstance().getTime();
        //dateTime = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).format(new Date());

        uploadAdapter = new MyUploadAdapter(uploadList, getContext(),
                new MyUploadAdapter.MyUploadsListener() {
                    @Override
                    public void MyUploadCancelClick(UserViewPost myUploads,int position) {
                        AlertDeleteMessage(myUploads,position);
                    }

                    @Override
                    public void MyUploadViewPost(List<UserViewPost> myUploads, int position) {

                        mListener.mySinglePost(myUploads,position);
                    }
                },"MyUploads");
        myUploadRecyclerView.setAdapter(uploadAdapter);
        layoutManager = new GridLayoutManager(getContext(),3);
        myUploadRecyclerView.setLayoutManager(layoutManager);
        myUploadRecyclerView.setHasFixedSize(true);
        ItemOffsetDecoration itemDecoration = new ItemOffsetDecoration(getContext(), R.dimen.item_offset_medium);
        myUploadRecyclerView.addItemDecoration(itemDecoration);

        //uploadList.add();
        getMyUpload();
        myUploadRecyclerView.addOnScrollListener(new PaginationScrollList(layoutManager) {
            @Override
            protected void loadMoreItems() {
                isLastPage=true;
                currentPage+=1;
                api_loadNextPage();
            }

            @Override
            public boolean isLastPage() {
                return isLastPage;
            }

            @Override
            public boolean isLoading() {
                return isLoading;
            }
        });
        /*for (int i=0;i<20;i++){
            MyUploads uploads=new MyUploads();

            if (i>10){
                uploads.setImageUrl("https://fresherappdemo.chetaru.co.uk/public/uploads/postImages/post_5ebeb606988961589556742.png");
                uploadList.add(uploads);
            }else{
                uploads.setImageUrl("https://fresherappdemo.chetaru.co.uk/public/uploads/user_profile/user_1591093946.png");
                uploadList.add(uploads);
            }
        }*/
        //uploadAdapter.addAll(uploadList);

        myUploadRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh()  {
                myUploadRefresh.setRefreshing(true);
                MyUploadRefresh();
                myUploadRefresh.setRefreshing(false);
            }
        });
        myUploadLayout.setVisibility(View.GONE);
        recyclerLayout.setVisibility(View.GONE);

        return  view;
    }

    public void AlertDeleteMessage(UserViewPost myUploads,int position){

        AlertDialog.Builder builder =new AlertDialog.Builder(getContext());
        builder.setMessage(getString(R.string.Are_you_sure_you_want_to_delete));
        builder.setCancelable(false);
        builder.setPositiveButton("yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                getCancelPost(myUploads,position);
            }
        });
        builder.setNegativeButton("no", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int i) {

                dialog.cancel();
            }
        });
        builder.show();
    }


    @Override
    public void onResume() {
        super.onResume();
        /*if (uploadList.size()<=0){
            getMyUpload();
        }*/
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }
    @Override public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    private void getMyUpload() {
        uploadList.clear();
        uploadAdapter.clear();
        baseRequest = new BaseRequest(getContext());
        baseRequest.setBaseRequestListner(new RequestReciever() {
            @Override
            public void onSuccess(int requestCode, String Json, Object object) {
                try {
                    Gson gson = new Gson();
                    JSONObject jsonObject = new JSONObject(Json);
                    TOTAL_PAGES = jsonObject.optInt("totalPageCount");
                   // currentPage = jsonObject.optInt("currentPage");
                    //ArrayList myUploadList= (ArrayList) jsonObject.get("data");
                    JSONArray userJsonArray = jsonObject.getJSONArray("data");

                    uploadList = baseRequest.getDataList(userJsonArray, UserViewPost.class);
                    if (uploadList.size()>0){
                        myUploadLayout.setVisibility(View.GONE);
                        recyclerLayout.setVisibility(View.VISIBLE);
                    }else {
                        myUploadLayout.setVisibility(View.VISIBLE);
                        recyclerLayout.setVisibility(View.GONE);
                    }
                   /* uploadAdapter = new MyUploadAdapter(uploadList, getContext(),
                    new MyUploadAdapter.MyUploadsListener() {
                        @Override
                        public void MyUploadCancelClick(MyUploads myUploads,int position) {
                            getCancelPost(myUploads,position);
                        }

                        @Override
                        public void MyUploadViewPost(MyUploads myUploads, int position) {
                            mListener.mySinglePost(myUploads);
                        }
                    });*/
                    myUploadRecyclerView.setAdapter(uploadAdapter);
                   uploadAdapter.addAll(uploadList);
                    if (currentPage < TOTAL_PAGES)
                        uploadAdapter.addLoadingFooter();
                    else
                        isLastPage = true;
                    try {
                        if (uploadList.size() > 0) {
                            dateTime = uploadList.get(0).getCreated_at();
                        }
                    }catch (Exception e){
                        e.printStackTrace();
                    }

                }catch (Exception e){
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int requestCode, String errorCode, String message) {
              //  utility.showToast(mContext,message);
                myUploadLayout.setVisibility(View.VISIBLE);
                recyclerLayout.setVisibility(View.GONE);
            }

            @Override
            public void onNetworkFailure(int requestCode, String message) {
                utility.showToast(getContext(),message);
            }
        });

        JsonObject main_object = new JsonObject();
        main_object.addProperty("userId", userId);
        main_object.addProperty("page", currentPage);
        baseRequest.callAPIPostWOLoader(1, main_object, getString(R.string.api_getUserUploads));
    }

    private void getCancelPost(UserViewPost myPost,int position) {
        baseRequest=new BaseRequest(getContext());
        baseRequest.setBaseRequestListner(new RequestReciever() {
            @Override
            public void onSuccess(int requestCode, String Json, Object object) {

                try{
                    Gson gson=new Gson();
                    //JSONObject jsonObject = new JSONObject(Json);
                    //uploadList.remove(position);
                    uploadList.clear();
                    uploadAdapter.clear();
                    isLastPage=false;
                    currentPage=PAGE_START;
                    TOTAL_PAGES=1;
                    getMyUpload();

                }catch (Exception  e){
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int requestCode, String errorCode, String message) {
               // utility.showToast(mContext,message);
            }

            @Override
            public void onNetworkFailure(int requestCode, String message) {
                utility.showToast(mContext,message);
            }
        });
        JsonObject main_object = new JsonObject();
       // main_object.addProperty("userId", userId);
        main_object.addProperty("postId", myPost.getId());
        baseRequest.callAPIPost(1, main_object, getString(R.string.api_deleteUserPosts));
    }

    private void api_loadNextPage() {
        baseRequest=new BaseRequest(getContext());
        baseRequest.setBaseRequestListner(new RequestReciever() {
            @Override
            public void onSuccess(int requestCode, String Json, Object object) {
                // utility.showToast(mContext,);
                try {
                    Gson gson = new Gson();
                    JSONObject jsonObject = new JSONObject(Json);
                    TOTAL_PAGES = jsonObject.optInt("totalPageCount");
                   // currentPage = jsonObject.optInt("currentPage");
                    //ArrayList myUploadList= (ArrayList) jsonObject.get("data");
                    JSONArray userJsonArray = jsonObject.getJSONArray("data");
                    uploadList = baseRequest.getDataList(userJsonArray, UserViewPost.class);
                    uploadAdapter.removeLoadingFooter();
                    isLoading = false;
                    uploadAdapter.addAll(uploadList);
                    //rv_list_archive.setAdapter(ad_notificationList);
                    if (currentPage != TOTAL_PAGES) uploadAdapter.addLoadingFooter();
                    else isLastPage = true;

                }catch (Exception e){
                    e.printStackTrace();
                }
            }
            @Override
            public void onFailure(int requestCode, String errorCode, String message) {
               // utility.showToast(mContext,message);
            }

            @Override
            public void onNetworkFailure(int requestCode, String message) {
                utility.showToast(mContext,message);
            }
        });
        JsonObject main_object = new JsonObject();
        main_object.addProperty("userId", userId);
        main_object.addProperty("page", currentPage);

        baseRequest.callAPIPostWOLoader(1, main_object, getString(R.string.api_getUserUploads));
    }

    private void MyUploadRefresh(){
        baseRequest=new BaseRequest(getContext());
        baseRequest.setBaseRequestListner(new RequestReciever() {
            @Override
            public void onSuccess(int requestCode, String Json, Object object) {

                try{
                    myUploadRefresh.setRefreshing(false);
                    Gson gson=new Gson();
                    JSONObject jsonObject = new JSONObject(Json);
                    //ArrayList myUploadList= (ArrayList) jsonObject.get("data");
                    JSONArray userJsonArray = jsonObject.getJSONArray("data");

                    List<UserViewPost>   newUploadList = baseRequest.getDataList(userJsonArray, UserViewPost.class);

                    if (newUploadList.size()>0) {
                        for (int i = 0; i < newUploadList.size(); i++) {
                            uploadList.add(i, newUploadList.get(i));
                        }
                    }
                    // uploadAdapter.addAll(uploadList);
                }catch (Exception  e){
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int requestCode, String errorCode, String message) {
                //utility.showToast(mContext,message);
            }

            @Override
            public void onNetworkFailure(int requestCode, String message) {

                utility.showToast(mContext,message);
            }
        });
        JsonObject main_object = new JsonObject();

        main_object.addProperty("userId", userId);
        main_object.addProperty("page", 0);
        main_object.addProperty("date", dateTime);
        baseRequest.callAPIPost(1, main_object, getString(R.string.api_getUserUploads));
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.back_upload_image:
                mListener.myUploadsBack();
                break;
        }

    }


    public abstract class PaginationScrollList extends RecyclerView.OnScrollListener {

        private LinearLayoutManager layoutManager;

        public PaginationScrollList(LinearLayoutManager layoutManager) {
            this.layoutManager = layoutManager;
        }

        @Override
        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
            super.onScrolled(recyclerView, dx, dy);

            int visibleItemCount = layoutManager.getChildCount();
            int totalItemCount = layoutManager.getItemCount();
            int firstVisibleItemPosition = layoutManager.findFirstVisibleItemPosition();

            if (!isLoading() && !isLastPage()) {
                if ((visibleItemCount + firstVisibleItemPosition) >= totalItemCount
                        && firstVisibleItemPosition >= 0) {
                    loadMoreItems();
                }
            }
        }

        protected abstract void loadMoreItems();

        public abstract boolean isLastPage();

        public abstract boolean isLoading();

    }
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof MyUploadListenerFragment) {
            mListener = (MyUploadListenerFragment) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement TagChildFragment");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }
   public  interface MyUploadListenerFragment{
        void mySinglePost(List<UserViewPost> postList,int position);
        void myUploadsBack();
    }

}
