package com.chetaru.FreshersApp.view.adapter;

import android.animation.ObjectAnimator;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Paint;
import android.os.Build;
import android.text.Html;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;
import com.chetaru.FreshersApp.R;
import com.chetaru.FreshersApp.service.model.UserViewPost;
import com.chetaru.FreshersApp.utility.SessionParam;
import com.chetaru.FreshersApp.utility.Utility;
import com.chetaru.FreshersApp.utility.Validations;
import com.chetaru.FreshersApp.view.callback.PaginationAdapterCallback;
import com.chetaru.FreshersApp.view.callback.PaginationListenerCallback;
import com.chetaru.FreshersApp.view.customView.ExpandableTextView;
import com.jackandphantom.blurimage.BlurImage;
import com.luseen.autolinklibrary.AutoLinkMode;
import com.luseen.autolinklibrary.AutoLinkOnClickListener;
import com.luseen.autolinklibrary.AutoLinkTextView;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;
import com.tylersuehr.socialtextview.SocialTextView;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Handler;

import de.hdodenhof.circleimageview.CircleImageView;
import jp.wasabeef.picasso.transformations.BlurTransformation;
import jp.wasabeef.picasso.transformations.gpu.BrightnessFilterTransformation;
import jp.wasabeef.picasso.transformations.gpu.KuwaharaFilterTransformation;
import jp.wasabeef.picasso.transformations.gpu.PixelationFilterTransformation;
import jp.wasabeef.picasso.transformations.gpu.SwirlFilterTransformation;
import jp.wasabeef.picasso.transformations.gpu.ToonFilterTransformation;
import jp.wasabeef.picasso.transformations.gpu.VignetteFilterTransformation;

import static com.bumptech.glide.request.RequestOptions.bitmapTransform;


public class HomeSingleAdapter extends RecyclerView.Adapter  {

    public static final int VIEW_TYPE_USER = 2;
    public static final int VIEW_TYPE_ADMIN = 1;
    private boolean isLoaderVisible = false;

    private List<UserViewPost> mNewsItemList=new ArrayList<>();
    private HomePaginationAdapter.Callback mCallback;
    Context mContext;
    Boolean click=true,expand=false;
    Utility utility;
    SessionParam sessionParam;

    public PaginationListenerCallback listener;
    Integer userId;
    int maxline=0,height=0;
    Boolean premiumUser;
    int sensitivePostsAllowed=2;
    int sensitivePost=2;


    public HomeSingleAdapter(List<UserViewPost> news, Context mContext, PaginationListenerCallback listener) {
        this.mNewsItemList = news;
        this.mContext=mContext;
        utility=new Utility();
        this.listener=listener;
        //this.expandableTextView=new ExpandableTextView(mContext);
        sessionParam=new SessionParam(mContext);
        sensitivePostsAllowed=sessionParam.sensitivePostsAllowed;
        userId=(sessionParam.id);
        premiumUser=sessionParam.premieUser;
        //premiumUser=true;
        notifyDataSetChanged();
      /* display =mContext.getWindowManager().getDefaultDisplay();
        screenWidth = display.getWidth();
        screenHeight = display.getHeight();*/

    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view;
        if (viewType==VIEW_TYPE_ADMIN){
            view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.admin_post_list,parent,false);
            return new AdminViewHolder(view);
        }else if (viewType==VIEW_TYPE_USER){
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.home_userpost_list,parent,false);
            return new UserViewHolder(view);
        }
        return null;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {


        UserViewPost viewPost=mNewsItemList.get(position);
        switch (holder.getItemViewType()){
            case VIEW_TYPE_ADMIN:
                ((AdminViewHolder)holder).bind(viewPost,position);
                break;
            case VIEW_TYPE_USER:
                ((UserViewHolder)holder).bind(viewPost,position);
                break;
        }
    }

    public class UserViewHolder extends RecyclerView.ViewHolder {

        SocialTextView hashTextView;
        ImageView newsImage,imageUpper,imageSceond,linkConnectionImage;
        public ImageView imageShowMore;

        ImageView likeImage,replyImage;
        CircleImageView iv_logo;
        TextView senPostMsg,originalPostTextView;
        TextView homeUserTextView,dateTextView,likeCountTxt;
        LinearLayout layoutMoreTxt,changeLayout,connection_link_layout,OriginalPostLayout;
        LinearLayout headerLayout,likeCountLayout,reply_link_layout;
        public UserViewHolder(View itemView) {
            super(itemView);
            iv_logo = itemView.findViewById(R.id.iv_logo);
            headerLayout = itemView.findViewById(R.id.post_top_layout);
            hashTextView = itemView.findViewById(R.id.social_text_view);
            replyImage = itemView.findViewById(R.id.reply_image);
            reply_link_layout = itemView.findViewById(R.id.reply_link_layout);
            homeUserTextView = itemView.findViewById(R.id.home_user_name_text);
            changeLayout = itemView.findViewById(R.id.set_height_layout);
            imageShowMore = itemView.findViewById(R.id.image_show_more);
            layoutMoreTxt = itemView.findViewById(R.id.layout_showmore_txt);
            newsImage = itemView.findViewById(R.id.upload_image_public);
            imageUpper = itemView.findViewById(R.id.upload_image_upper);
            imageSceond = itemView.findViewById(R.id.upload_image_sceond);
            likeImage = itemView.findViewById(R.id.like_image);
            linkConnectionImage = itemView.findViewById(R.id.link_image);
            dateTextView = itemView.findViewById(R.id.upload_image_date_txt);
            likeCountTxt = itemView.findViewById(R.id.like_count_txt);
            connection_link_layout = itemView.findViewById(R.id.connection_link_layout);
            senPostMsg = itemView.findViewById(R.id.post_msg_view);
            likeCountLayout = itemView.findViewById(R.id.like_count_layout);
            originalPostTextView = itemView.findViewById(R.id.view_original_post_txt);
            OriginalPostLayout = itemView.findViewById(R.id.view_original_post_layout);
           /* newTitle.addAutoLinkMode(
                    AutoLinkMode.MODE_HASHTAG,
                    AutoLinkMode.MODE_MENTION,
                    AutoLinkMode.MODE_CUSTOM);
            newTitle.setHashtagModeColor(ContextCompat.getColor(mContext, R.color.colorHashTag));
            newTitle.setMentionModeColor(ContextCompat.getColor(mContext, R.color.colorHashTag));*/
            //newTitle.getViewTreeObserver().removeOnGlobalLayoutListener(this::clear);

           // originalPostTextView.setPaintFlags(originalPostTextView.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);

            /*String text="VIEW ORIGINAL POST";
            SpannableString content = new SpannableString(text);
            content.setSpan(new UnderlineSpan(), 0, text.length(), 0);
            originalPostTextView.setText(content);*/
            //originalPostTextView.setText(Html.fromHtml("<p><u>VIEW ORIGINAL POST</u></p>"));


        }
        protected void clear() {
        }

        public void bind(UserViewPost mNewsItem, int position) {

            try {
                if (mNewsItem.getId()!=null) {
                    try {
                        sensitivePost=mNewsItem.getSensitivePostStatus();
                        //Picasso.with(mContext).load(mNewsItem.getUserImage()).into(iv_logo);
                        Glide.with(mContext).load(mNewsItem.getUserImage()).placeholder(R.drawable.profile_pics).into(iv_logo);
                        homeUserTextView.setText(mNewsItem.getName());
                        likeImage.setVisibility(View.VISIBLE);
                        senPostMsg.setVisibility(View.GONE);
                        /*Picasso.with(mContext).load(mNewsItem.getImageUrl())
                                .into(imageUpper);*/
                        if (userId!=mNewsItem.getUserId()) {
                            if (sensitivePostsAllowed == 1) {
                                senPostMsg.setVisibility(View.GONE);
                                /*Picasso.with(mContext).load(mNewsItem.getImageUrl())
                                        .into(imageUpper);*/
                                Glide.with(mContext).load(mNewsItem.getImageUrl())
                                        .placeholder(R.drawable.profile_pics)
                                        .into(imageUpper);
                                Thread thread = new Thread(new Runnable() {
                                    @Override
                                    public void run() {
                                        Bitmap bitmapFromURL = Utility.getBitmapFromURL(mNewsItem.getImageUrl());
                                        BlurImage.with(mContext).load(bitmapFromURL).intensity(25).Async(true).into(imageSceond);
                                        BlurImage.with(mContext).load(bitmapFromURL).intensity(25).Async(true).into(newsImage);

                                    }
                                });

                                thread.start();
                            } else {
                                if (sensitivePost == 1) {
                                    senPostMsg.setVisibility(View.VISIBLE);
                                    Thread thread = new Thread(new Runnable() {
                                        @Override
                                        public void run() {
                                            Bitmap bitmapFromURL = Utility.getBitmapFromURL(mNewsItem.getImageUrl());
                                            BlurImage.with(mContext).load(bitmapFromURL).intensity(25).Async(true).into(imageSceond);
                                            BlurImage.with(mContext).load(bitmapFromURL).intensity(25).Async(true).into(newsImage);

                                        }
                                    });

                                    thread.start();

                                } else {
                                    senPostMsg.setVisibility(View.GONE);
                                    /*Picasso.with(mContext).load(mNewsItem.getImageUrl())
                                            .into(imageUpper);*/
                                    Glide.with(mContext).load(mNewsItem.getImageUrl()).placeholder(R.drawable.profile_pics)
                                            .into(imageUpper);
                                    Thread thread = new Thread(new Runnable() {
                                        @Override
                                        public void run() {
                                            Bitmap bitmapFromURL = Utility.getBitmapFromURL(mNewsItem.getImageUrl());
                                            BlurImage.with(mContext).load(bitmapFromURL).intensity(25).Async(true).into(imageSceond);
                                            BlurImage.with(mContext).load(bitmapFromURL).intensity(25).Async(true).into(newsImage);

                                        }
                                    });

                                    thread.start();
                                }
                            }
                        }else {
                            senPostMsg.setVisibility(View.GONE);
                                /*Picasso.with(mContext).load(mNewsItem.getImageUrl())
                                        .into(imageUpper);*/
                            Glide.with(mContext).load(mNewsItem.getImageUrl()).placeholder(R.drawable.profile_pics)
                                    .into(imageUpper);
                            Thread thread = new Thread(new Runnable() {
                                @Override
                                public void run() {
                                    Bitmap bitmapFromURL = Utility.getBitmapFromURL(mNewsItem.getImageUrl());
                                    BlurImage.with(mContext).load(bitmapFromURL).intensity(25).Async(true).into(imageSceond);
                                    BlurImage.with(mContext).load(bitmapFromURL).intensity(25).Async(true).into(newsImage);

                                }
                            });

                            thread.start();
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    try{
                        if (mNewsItem.getOriginalPostId()!=0){
                            originalPostTextView.setVisibility(View.VISIBLE);
                            OriginalPostLayout.setVisibility(View.VISIBLE);
                        }else {
                            originalPostTextView.setVisibility(View.GONE);
                            OriginalPostLayout.setVisibility(View.GONE);
                        }
                        originalPostTextView.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                listener.onPaginationClick(mNewsItem, click, position, "originalPost");
                            }
                        });
                        headerLayout.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                listener.onPaginationClick(mNewsItem,false,position,"headerLayout");
                            }
                        });

                    }catch (Exception e){
                        e.printStackTrace();
                    }
                    try {

                        hashTextView.setLinkText(mNewsItem.getCaption());
                        hashTextView.setOnLinkClickListener(new SocialTextView.OnLinkClickListener() {
                            @Override
                            public void onLinkClicked(int linkType, String matchedText) {
                                //linkType 1=hashTag,2==mention
                                listener.autoLinkListener(linkType, "", matchedText, mNewsItem);
                            }
                        });


                        changeLayout.post(new Runnable() {
                            @Override
                            public void run() {
                                //maybe also works height = ll.getLayoutParams().height;
                                height = changeLayout.getHeight();
                            }
                        });

                        hashTextView.post(new Runnable() {

                            @Override
                            public void run() {
                                Log.v("Line count: ", hashTextView.getLineCount() + "");
                                maxline = hashTextView.getLineCount();
                            /*if (maxline > 3) {
                                imageShowMore.setVisibility(View.VISIBLE);
                            } else {
                                imageShowMore.setVisibility(View.GONE);
                            }*/
                            }
                        });

                        //hashTextView.setMovementMethod(ScrollingMovementMethod.getInstance());
                    /*hashTextView.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                        @Override
                        public void onGlobalLayout() {
                            if (expand) {
                                expand = false;
                                if (maxline > 3) {
                                    btShowMore.setVisibility(View.GONE);
                                    ObjectAnimator animation = ObjectAnimator.ofInt(hashTextView, "maxLines", 40);
                                    animation.setDuration(0).start();

                                *//*LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
                                params.height = 250;
                                changeLayout.setLayoutParams(params);*//*
                                    int height_in_pixels = hashTextView.getLineCount() * hashTextView.getLineHeight();
                                    LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) changeLayout.getLayoutParams();
                                    if (height > height_in_pixels) {
                                        params.height = height * 2;
                                    } else {
                                        params.height = height_in_pixels;
                                    }
                                    changeLayout.setLayoutParams(params);
                                    //newTitle.setMovementMethod(ScrollingMovementMethod.getInstance());

                                } else {
                                    btShowMore.setVisibility(View.GONE);
                                    ViewGroup.LayoutParams params = (ViewGroup.LayoutParams) changeLayout.getLayoutParams();
                                    params.height = height;
                                    changeLayout.setLayoutParams(params);
                                }
                            }
                        }
                    });*/

                        hashTextView.setLines(2);
                        imageShowMore.setOnClickListener(new View.OnClickListener() {
                            public void onClick(View v) {
                                if (!expand){
                                    expand=true;
                                    dateTextView.setVisibility(View.VISIBLE);
                                    imageShowMore.setImageResource(R.drawable.ic_expand_more_white_24dp);

                                    ObjectAnimator animation = ObjectAnimator.ofInt(hashTextView, "maxLines", 40);
                                    animation.setDuration(100).start();
                                    int height_in_pixels = hashTextView.getLineCount() * hashTextView.getLineHeight();
                                    LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) changeLayout.getLayoutParams();
                                    int maxLineCount=hashTextView.getMaxLines();
                                    int lineCount=hashTextView.getLineCount();
                                    int minLineCount=hashTextView.getMinLines();
                                    if (height > height_in_pixels) {
                                        params.height = height +height_in_pixels;
                                    } else {
                                        // params.height = (height*2)+((height_in_pixels+100)/2);
                                        params.height = (height)+((height_in_pixels+100)/2);
                                    }
                                    final int sdk = android.os.Build.VERSION.SDK_INT;
                                    if (sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
                                        params.height=height*2;
                                        if (params.height<height_in_pixels){
                                            params.height=params.height+height_in_pixels;
                                        }

                                    }else if (sdk< Build.VERSION_CODES.O){
                                        if (params.height<0){
                                            params.height=height_in_pixels+((height_in_pixels + 100) / 2);

                                        }else {
                                            if (height > height_in_pixels) {
                                                params.height = height +height_in_pixels;
                                            } else if (params.height>height_in_pixels){
                                                params.height = (params.height) + ((height_in_pixels + 100) / 2);
                                            }else {
                                                params.height=height+(height_in_pixels + 150);
                                            }
                                        }
                                    }else {
                                        if (height > height_in_pixels) {
                                            params.height = height +height_in_pixels;
                                        } else {
                                            // params.height = (height*2)+((height_in_pixels+100)/2);
                                            params.height=height_in_pixels+((height_in_pixels + 100) / 2);

                                        }
                                    }

                                    changeLayout.setLayoutParams(params);
                                }else{
                                    expand=false;
                                    dateTextView.setVisibility(View.GONE);
                                    imageShowMore.setImageResource(R.drawable.ic_expand_less_white_24dp);

                                    ObjectAnimator animation = ObjectAnimator.ofInt(hashTextView, "maxLines", 3);
                                    animation.setDuration(100).start();
                                    ViewGroup.LayoutParams params = (ViewGroup.LayoutParams) changeLayout.getLayoutParams();
                                    int height_in_pixels = hashTextView.getLineCount() * hashTextView.getLineHeight();
                                    int maxLineCount=hashTextView.getMaxLines();
                                    int lineCount=hashTextView.getLineCount();
                                    int minLineCount=hashTextView.getMinLines();

                                    final int sdk = android.os.Build.VERSION.SDK_INT;
                                    if (sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
                                        params.height=params.height+height_in_pixels;

                                    }else if (sdk< Build.VERSION_CODES.O){
                                        params.height=height_in_pixels+5;
                                        if (params.height<0){
                                            params.height=height_in_pixels;

                                        }else {
                                            if (height > height_in_pixels) {
                                                params.height = height +height_in_pixels;
                                            } else {
                                                if(height<=0){
                                                    params.height=height_in_pixels;
                                                }else {
                                                    params.height = (height);
                                                }
                                            }
                                        }
                                    }else {
                                        params.height = height;
                                    }
                                    changeLayout.setLayoutParams(params);

                                }
                                // expand = !expand;
                                //advancedView.setVisibility( isExpanded ?  View.VISIBLE : View.GONE );
                                //expandCtrl.setCompoundDrawablesWithIntrinsicBounds( isExpanded ? icon_collapse : icon_expand, 0, 0, 0 );


                            }
                        });


                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    try {
                        Log.d("userName", "session: " + userId + " " + "viewPost: " + mNewsItem.getName().toLowerCase().trim());

                        if (!(userId.equals(mNewsItem.getUserId()))) {
                            linkConnectionImage.setVisibility(View.VISIBLE);
                            replyImage.setVisibility(View.VISIBLE);
                            connection_link_layout.setVisibility(View.VISIBLE);
                            reply_link_layout.setVisibility(View.VISIBLE);
                            likeCountLayout.setVisibility(View.GONE);
                            likeCountTxt.setVisibility(View.GONE);


                        }else {
                            linkConnectionImage.setVisibility(View.GONE);
                            replyImage.setVisibility(View.GONE);
                            connection_link_layout.setVisibility(View.GONE);
                            reply_link_layout.setVisibility(View.GONE);
                            likeCountTxt.setVisibility(View.VISIBLE);
                            likeCountLayout.setVisibility(View.VISIBLE);

                           /* ViewGroup.MarginLayoutParams marginParams = new ViewGroup.MarginLayoutParams(imageShowMore.getLayoutParams());
                            marginParams.setMargins(0, 0, 4, 0);
                            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(marginParams);
                            imageShowMore.setLayoutParams(layoutParams);*/

                            //show like count user list
                            likeCountTxt.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    if (premiumUser) {
                                        listener.onPaginationClick(mNewsItem, click, position, "likeCount");
                                    }else {
                                        listener.onPaginationClick(mNewsItem,click,position,"premiumUpgrade");
                                    }
                                }
                            });


                        }
                        if (mNewsItem.getLikedCount()>0) {
                            likeCountTxt.setVisibility(View.VISIBLE);
                            likeCountTxt.setText(mNewsItem.getLikedCount().toString() + " Likes");
                        }else {
                            likeCountTxt.setVisibility(View.VISIBLE);
                            likeCountTxt.setText("No Likes");
                        }
                        linkConnectionImage.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                listener.onPaginationClick(mNewsItem, click, position, "connection");
                            }
                        });
                        replyImage.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                listener.onPaginationClick(mNewsItem,click,position,"reply");
                            }
                        });
                        likeImage.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {


                                //likeImage.setBackground(click ?ContextCompat.getDrawable(mContext, R.drawable.like_true):ContextCompat.getDrawable(mContext, R.drawable.like));
                                if (userId.equals(mNewsItem.getUserId())){
                                    if (premiumUser) {
                                        listener.onPaginationClick(mNewsItem, click, position, "likeCount");
                                    }else {
                                        listener.onPaginationClick(mNewsItem,click,position,"premiumUpgrade");
                                    }

                                }else {
                                    listener.onPaginationClick(mNewsItem, click, position, "like");
                                    final int sdk = android.os.Build.VERSION.SDK_INT;
                                    if (click) {
                                        click = false;
                                        if (sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
                                            likeImage.setBackgroundDrawable(ContextCompat.getDrawable(mContext, R.mipmap.admin_post_like));
                                        } else {
                                            likeImage.setBackground(ContextCompat.getDrawable(mContext, R.mipmap.admin_post_like));
                                        }
                                    } else {
                                        click = true;
                                        if (sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
                                            likeImage.setBackgroundDrawable(ContextCompat.getDrawable(mContext, R.mipmap.like_primary_color));
                                        } else {
                                            likeImage.setBackground(ContextCompat.getDrawable(mContext, R.mipmap.like_primary_color));
                                        }
                                    }
                                }
                            }
                        });

                        final int sdk = android.os.Build.VERSION.SDK_INT;
                        if (mNewsItem.getLikedStatus()) {
                            click = false;
                            if (sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
                                likeImage.setBackgroundDrawable(ContextCompat.getDrawable(mContext, R.mipmap.admin_post_like));
                            } else {
                                likeImage.setBackground(ContextCompat.getDrawable(mContext, R.mipmap.admin_post_like));
                            }
                        } else {
                            click = true;
                            if (sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
                                likeImage.setBackgroundDrawable(ContextCompat.getDrawable(mContext, R.mipmap.like_primary_color));
                            } else {
                                likeImage.setBackground(ContextCompat.getDrawable(mContext, R.mipmap.like_primary_color));
                            }
                        }
                   /* } else {
                        likeImage.setVisibility(View.GONE);
                        linkConnectionImage.setVisibility(View.GONE);
                    }*/



                    } catch (Exception e) {
                        e.printStackTrace();
                    }

               /* linkConnectionImage.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        listener.onPaginationClick(mNewsItem, click, position, "connection");
                    }
                });
                likeImage.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        //likeImage.setBackground(click ?ContextCompat.getDrawable(mContext, R.drawable.like_true):ContextCompat.getDrawable(mContext, R.drawable.like));
                        listener.onPaginationClick(mNewsItem, click, position, "like");
                        final int sdk = android.os.Build.VERSION.SDK_INT;
                        if (click) {
                            click = false;
                            if (sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
                                likeImage.setBackgroundDrawable(ContextCompat.getDrawable(mContext, R.drawable.like_true));
                            } else {
                                likeImage.setBackground(ContextCompat.getDrawable(mContext, R.drawable.like_true));
                            }
                        } else {
                            click = true;
                            if (sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
                               likeImage.setBackgroundDrawable(ContextCompat.getDrawable(mContext, R.drawable.like));
                            } else {
                                likeImage.setBackground(ContextCompat.getDrawable(mContext, R.drawable.like));
                            }
                        }
                    }
                });*/
                    itemView.setOnLongClickListener(new View.OnLongClickListener() {
                        @Override
                        public boolean onLongClick(View view) {
                            //utility.showToast(mContext,"Long Click");
                            if (!userId.equals(mNewsItem.getUserId()))
                                listener.onPaginationLongClick(mNewsItem, position,"user");
                            return false;
                        }
                    });
                    try{
                        String changeDate=utility.getPostUploadDate(mNewsItem.getPostedDate());
                        dateTextView.setText(changeDate);
                    }catch (Exception e){
                        e.printStackTrace();
                    }



                        likeCountTxt.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {

                                if(premiumUser) {
                                    listener.onPaginationClick(mNewsItem, click, position, "likeCount");
                                }else {
                                    listener.onPaginationClick(mNewsItem,click,position,"premiumUpgrade");
                                }
                            }
                        });
                       /* likeImage.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                if (premiumUser) {
                                    listener.onPaginationClick(mNewsItem, click, position, "likeCount");
                                }else {
                                    listener.onPaginationClick(mNewsItem,click,position,"premiumUpgrade");
                                }

                            }
                        });*/




                }
            }catch (Exception e){
                e.printStackTrace();
            }
        }


    }

    public class AdminViewHolder extends RecyclerView.ViewHolder{

        SocialTextView AdminHashTextView;
        ImageView AdminNewsImage,AdminImageUpper,AdminImageSceond;
        public CircleImageView AdminLikeImage;
        public ImageView adminImageShowMore;
        TextView showMoreTxt;
        CircleImageView admin_iv_logo;
        TextView adminHeaderTextView,adminDateTextView,adminLikeCountTxt;
        LinearLayout layoutMoreTxt,changeLayout,socialParentLayout;
        LinearLayout root_layout,headerLayout,like_layout,like_parent_layout;

        public AdminViewHolder(@NonNull View itemView) {
            super(itemView);
            root_layout = itemView.findViewById(R.id.root_layout);
            headerLayout = itemView.findViewById(R.id.header_top_layout);
            admin_iv_logo = itemView.findViewById(R.id.iv_logo);
            adminHeaderTextView = itemView.findViewById(R.id.home_admin_name_text);
            AdminNewsImage = itemView.findViewById(R.id.upload_image_public);
            AdminImageUpper = itemView.findViewById(R.id.upload_image_upper);
            AdminImageSceond = itemView.findViewById(R.id.upload_image_sceond);
            changeLayout = itemView.findViewById(R.id.admin_bottom_parent_layout);
            socialParentLayout = itemView.findViewById(R.id.admin_social_text_parent_layout);


            adminDateTextView = itemView.findViewById(R.id.admin_post_date_txt);
            AdminHashTextView = itemView.findViewById(R.id.admin_social_text_view);
            like_layout = itemView.findViewById(R.id.like_layout);
            adminImageShowMore = itemView.findViewById(R.id.image_show_more);
            like_parent_layout = itemView.findViewById(R.id.like_parent_layout);
            AdminLikeImage = itemView.findViewById(R.id.like_image);
            adminLikeCountTxt = itemView.findViewById(R.id.like_count_txt);
        }

        public void bind(UserViewPost mNewsItem, int position) {
            try {
                if (mNewsItem.getId()!=null) {


                    try {
                        //Picasso.with(mContext).load(mNewsItem.getUserImage()).into(admin_iv_logo);
                        Glide.with(mContext).load(mNewsItem.getUserImage()).placeholder(R.drawable.profile_pics).into(admin_iv_logo);
                        /*Picasso.with(mContext).load(mNewsItem.getImageUrl())
                                .into(AdminImageUpper);*/
                        Glide.with(mContext).load(mNewsItem.getImageUrl()).placeholder(R.drawable.profile_pics)
                                .into(AdminImageUpper);

                        Thread thread = new Thread(new Runnable(){
                            @Override
                            public void run() {
                                Bitmap bitmapFromURL=Utility.getBitmapFromURL(mNewsItem.getImageUrl());
                                BlurImage.with(mContext).load(bitmapFromURL).intensity(25).Async(true).into(AdminImageSceond);
                                BlurImage.with(mContext).load(bitmapFromURL).intensity(25).Async(true).into(AdminNewsImage);

                            }
                        });

                        thread.start();

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    try{
                        String changeDate=utility.getPostUploadDate(mNewsItem.getPostedDate());
                        adminDateTextView.setText(changeDate);
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                    try {
                        if (!Validations.isEmptyString(mNewsItem.getName())) {
                            adminHeaderTextView.setText(mNewsItem.getName());
                            adminHeaderTextView.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    listener.onPaginationClick(mNewsItem, click, position, "tagName");
                                }
                            });
                        }

                        AdminHashTextView.setLinkText(mNewsItem.getCaption());
                        AdminHashTextView.setOnLinkClickListener(new SocialTextView.OnLinkClickListener() {
                            @Override
                            public void onLinkClicked(int linkType, String matchedText) {
                                //linkType 1=hashTag,2==mention
                                listener.autoLinkListener(linkType, "", matchedText, mNewsItem);
                            }
                        });


                        changeLayout.post(new Runnable() {
                            @Override
                            public void run() {
                                //maybe also works height = ll.getLayoutParams().height;
                                height = changeLayout.getHeight();
                            }
                        });

                        AdminHashTextView.post(new Runnable() {

                            @Override
                            public void run() {
                                Log.v("Line count: ", AdminHashTextView.getLineCount() + "");
                                maxline = AdminHashTextView.getLineCount();
                                /*if (maxline > 3) {
                                    imageShowMore.setVisibility(View.VISIBLE);
                                    //showMoreTxt.setVisibility(View.VISIBLE);
                                } else {
                                    imageShowMore.setVisibility(View.GONE);
                                    //showMoreTxt.setVisibility(View.GONE);
                                }*/
                            }
                        });

                       // AdminHashTextView.setLines(2);

                        adminImageShowMore.setOnClickListener(new View.OnClickListener() {
                            public void onClick(View v) {
                                if (!expand){
                                    expand=true;
                                    adminDateTextView.setVisibility(View.VISIBLE);
                                    adminImageShowMore.setImageResource(R.drawable.ic_baseline_expand_more_24);

                                    ObjectAnimator animation = ObjectAnimator.ofInt(AdminHashTextView, "maxLines", 40);
                                    animation.setDuration(100).start();
                                    int height_in_pixels = AdminHashTextView.getLineCount() * AdminHashTextView.getLineHeight();
                                    LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) changeLayout.getLayoutParams();
                                     /*if (height > height_in_pixels) {
                                        params.height = height +height_in_pixels;
                                    } else {
                                       // params.height = (height*2)+((height_in_pixels+100)/2);
                                        params.height = (height)+((height_in_pixels+100)/2);
                                    }*/
                                    final int sdk = android.os.Build.VERSION.SDK_INT;
                                    if (sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
                                        params.height=height*2;
                                        if (params.height<height_in_pixels){
                                            params.height=params.height+height_in_pixels;
                                        }

                                    }else if (sdk< Build.VERSION_CODES.O){
                                        if (params.height<0){
                                            params.height=height_in_pixels+((height_in_pixels + 100) / 2);

                                        }else {
                                            if (height > height_in_pixels) {
                                                params.height = height +height_in_pixels;
                                            } else if (params.height>height_in_pixels){
                                                params.height = (params.height) + ((height_in_pixels + 100) / 2);
                                            }else {
                                                params.height=height+(height_in_pixels + 150);
                                            }
                                        }
                                    }else {
                                        if (height > height_in_pixels) {
                                            params.height = height +height_in_pixels;
                                        } else {
                                            // params.height = (height*2)+((height_in_pixels+100)/2);
                                            if (params.height<0){
                                                params.height=height_in_pixels+((height_in_pixels + 100) / 2);
                                            }else {
                                                params.height = (height) + ((height_in_pixels + 100) / 2);
                                            }
                                            if (height<(height_in_pixels+100)){
                                                params.height=height+height_in_pixels;
                                            }
                                        }
                                    }

                                    changeLayout.setLayoutParams(params);
                                }else{
                                    expand=false;
                                    adminDateTextView.setVisibility(View.GONE);
                                    adminImageShowMore.setImageResource(R.drawable.ic_baseline_expand_less_24);
                                    ObjectAnimator animation = ObjectAnimator.ofInt(AdminHashTextView, "maxLines", 3);
                                    animation.setDuration(100).start();
                                    ViewGroup.LayoutParams params = (ViewGroup.LayoutParams) changeLayout.getLayoutParams();
                                    int height_in_pixels = AdminHashTextView.getLineCount() * AdminHashTextView.getLineHeight();

                                    final int sdk = android.os.Build.VERSION.SDK_INT;
                                    if (sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
                                        params.height=params.height+height_in_pixels;

                                    }else if (sdk< Build.VERSION_CODES.O){
                                        params.height=height_in_pixels+5;
                                    }else {
                                        params.height = height;
                                    }
                                    changeLayout.setLayoutParams(params);

                                }
                               /* expand = !expand;
                                advancedView.setVisibility( isExpanded ?  View.VISIBLE : View.GONE );
                                expandCtrl.setCompoundDrawablesWithIntrinsicBounds( isExpanded ? icon_collapse : icon_expand, 0, 0, 0 );
*/

                            }
                        });
                    } catch (Exception e) {
                        e.printStackTrace();
                    }


                    try {
                        Log.d("userName", "session: " + userId + " " + "viewPost: " + mNewsItem.getName().toLowerCase().trim());



                        final int sdk = android.os.Build.VERSION.SDK_INT;

                        if (mNewsItem.getLikedStatus()) {
                            click = false;
                            if (sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
                                // AdminLikeImage.setBackgroundDrawable(ContextCompat.getDrawable(mContext, R.drawable.like_true));
                                AdminLikeImage.setImageResource( R.mipmap.admin_post_like);
                            } else {
                                //AdminLikeImage.setBackground(ContextCompat.getDrawable(mContext, R.drawable.like_true));
                                AdminLikeImage.setImageResource( R.mipmap.admin_post_like);
                            }
                        } else {
                            click = true;
                            if (sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
                                AdminLikeImage.setImageResource( R.mipmap.like_primary_color);
                                // AdminLikeImage.setBackgroundDrawable(ContextCompat.getDrawable(mContext, R.mipmap.like_primary_color));
                            } else {
                                AdminLikeImage.setImageResource( R.mipmap.like_primary_color);
                                // AdminLikeImage.setBackground(ContextCompat.getDrawable(mContext, R.mipmap.like_primary_color));
                            }
                        }


                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    try{
                       /* if (mNewsItem.getLikedCount()>0) {
                            likeCountTxt.setVisibility(View.VISIBLE);
                            likeCountTxt.setText(mNewsItem.getLikedCount().toString() + " Likes");
                        }else {
                            likeCountTxt.setVisibility(View.VISIBLE);
                            likeCountTxt.setText( "No Likes");
                        }*/
                        adminLikeCountTxt.setVisibility(View.GONE);
                    }catch (Exception e){
                        e.printStackTrace();
                    }

                   /* linkConnectionImage.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            listener.onPaginationClick(mNewsItem, click, position, "connection");
                        }
                    });*/
                    AdminLikeImage.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {

                            //likeImage.setBackground(click ?ContextCompat.getDrawable(mContext, R.drawable.like_true):ContextCompat.getDrawable(mContext, R.drawable.like));
                            listener.onPaginationClick(mNewsItem, click, position, "like");
                           // AdminHashTextView.setLines(2);
                            final int sdk = android.os.Build.VERSION.SDK_INT;
                            if (click) {
                                click = false;
                                if (sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
                                    // AdminLikeImage.setBackgroundDrawable(ContextCompat.getDrawable(mContext, R.drawable.like_true));
                                    AdminLikeImage.setImageResource( R.mipmap.admin_post_like);

                                } else {
                                    // AdminLikeImage.setBackground(ContextCompat.getDrawable(mContext, R.drawable.like_true));
                                    AdminLikeImage.setImageResource( R.mipmap.admin_post_like);

                                }
                            } else {
                                click = true;
                                if (sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
                                    AdminLikeImage.setBackgroundDrawable(ContextCompat.getDrawable(mContext,  R.mipmap.like_primary_color));
                                } else {
                                    AdminLikeImage.setBackground(ContextCompat.getDrawable(mContext,  R.mipmap.like_primary_color));
                                }
                            }
                        }
                    });

                    itemView.setOnLongClickListener(new View.OnLongClickListener() {
                        @Override
                        public boolean onLongClick(View view) {
                            listener.onPaginationLongClick(mNewsItem, position,"admin");
                            return false;
                        }
                    });


                    //newTitle.setText(mNewsItem.getCaption());


                }
            }catch (Exception e){
                e.printStackTrace();
            }

        }
    }


    @Override
    public int getItemCount() {
        try {
            return mNewsItemList.size();
        }catch (Exception e){
            e.printStackTrace();
            return 0;
        }
    }

    @Override
    public int getItemViewType(int position) {
        UserViewPost viewPost=mNewsItemList.get(position);
        if (viewPost.getRoleId()==1){
            return VIEW_TYPE_ADMIN;
        }else {
            return VIEW_TYPE_USER;
        }

    }

    public void add(UserViewPost response) {
        mNewsItemList.add(response);
        notifyItemInserted(mNewsItemList.size() - 1);
    }
    public void addAll(List<UserViewPost> postItems) {
        for (UserViewPost response : postItems) {
            add(response);
        }
    }
    private void remove(UserViewPost postItems) {
        int position = mNewsItemList.indexOf(postItems);
        if (position > -1) {
            mNewsItemList.remove(position);
            notifyItemRemoved(position);
        }
    }

    public void clear() {
        try {
            while (getItemCount() > 0) {
                remove(getItem(0));
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }
    UserViewPost getItem(int position) {
        return mNewsItemList.get(position);

    }

}
