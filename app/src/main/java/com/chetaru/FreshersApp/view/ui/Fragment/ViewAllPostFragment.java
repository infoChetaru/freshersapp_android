package com.chetaru.FreshersApp.view.ui.Fragment;

import android.app.Dialog;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.media.AudioManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.PagerSnapHelper;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.SnapHelper;

import android.os.VibrationEffect;
import android.os.Vibrator;
import android.provider.Settings;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.chetaru.FreshersApp.R;
import com.chetaru.FreshersApp.service.model.TagList;
import com.chetaru.FreshersApp.service.model.UserList;
import com.chetaru.FreshersApp.service.model.UserViewPost;
import com.chetaru.FreshersApp.service.retrofit.BaseRequest;
import com.chetaru.FreshersApp.service.retrofit.RequestReciever;
import com.chetaru.FreshersApp.utility.SessionParam;
import com.chetaru.FreshersApp.utility.Utility;
import com.chetaru.FreshersApp.utility.Validations;
import com.chetaru.FreshersApp.view.adapter.MyUploadsHomeAdapter;
import com.chetaru.FreshersApp.view.callback.HomepageCallback;
import com.chetaru.FreshersApp.view.callback.PaginationListenerCallback;
import com.google.gson.JsonObject;

import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link ViewAllPostFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ViewAllPostFragment extends Fragment implements PaginationListenerCallback, View.OnClickListener {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private static final String ARG_PARAM3 = "param3";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;




    private Unbinder unbinder;
    Context mContext;
    Utility utility;
    SessionParam sessionParam;
    private BaseRequest baseRequest;
    MyUploadsHomeAdapter myUploadAdapter;
    // layout initialization
    @BindView(R.id.home_image_layout)
    LinearLayout showDataLayout;
    @BindView(R.id.home_long_click_layout)
    LinearLayout longClickLayout;
    @BindView(R.id.tag_image_layout)
    LinearLayout tagImageLayout;
    @BindView(R.id.block_user_layout)
    LinearLayout blockLayout;
    @BindView(R.id.report_layout)
    LinearLayout reportLayout;
    @BindView(R.id.mRecyclerView)
    RecyclerView mRecyclerView;
    @BindView(R.id.block_user_image)
    ImageView blockUserImage;
    @BindView(R.id.terms_of_use_image)
    ImageView termsImage;
    @BindView(R.id.privacy_policy_image)
    ImageView privacyPolicyImage;
    @BindView(R.id.contact_us_image)
    ImageView contactUsImage;
    @BindView(R.id.cancel_image)
    ImageView cancelImage;



    //handel param on newInstance
    int position=0;
    String otherUser=null;
    List<UserViewPost> myUploadList= new ArrayList<>();
    Fragment childFragment=null;
    int userId;
    UserViewPost selectedViewPost;

    //handel viewAllPost callback to home Activity
    HomepageCallback mListener;
    String parentClassName;
    int ON_DO_NOT_DISTURB_CALLBACK_CODE=101;



    public ViewAllPostFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment ViewAllPostFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static ViewAllPostFragment newInstance(int position, List<UserViewPost> postList,String otherUser) {
        ViewAllPostFragment fragment = new ViewAllPostFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_PARAM1, position);
        args.putSerializable(ARG_PARAM2, (Serializable) postList);
        args.putString(ARG_PARAM3,otherUser);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            position = getArguments().getInt(ARG_PARAM1);
            myUploadList = (List<UserViewPost>) getArguments().getSerializable(ARG_PARAM2);
            otherUser=getArguments().getString(ARG_PARAM3);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_view_all_post, container, false);
        unbinder = ButterKnife.bind(this, view);
        mContext=getContext();
        //uploadList=new ArrayList<>();
        utility=new Utility();
        this.mListener = (HomepageCallback) getContext();
        sessionParam=new SessionParam(mContext);
        userId=sessionParam.id;
        selectedViewPost=new UserViewPost ();

        initView();
        showDataLayout.setVisibility(View.VISIBLE);
        longClickLayout.setVisibility(View.GONE);
        tagImageLayout.setVisibility(View.GONE);

        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false);
        SnapHelper snapHelper = new PagerSnapHelper();
        mRecyclerView.setLayoutManager(layoutManager);
        snapHelper.attachToRecyclerView(mRecyclerView);
        myUploadAdapter = new MyUploadsHomeAdapter(myUploadList,getContext(),this);
        mRecyclerView.setAdapter(myUploadAdapter);

        if (!Validations.isEmptyString(otherUser)){
            parentClassName=otherUser;
        }
        try{
            mRecyclerView.getLayoutManager().scrollToPosition(position);
        }catch (Exception e){
            e.printStackTrace();
        }
        try{
            if (childFragment!=null) {
               /* FragmentTransaction transaction = getActivity().getSupportFragmentManager()
                        .beginTransaction();

                transaction.remove(childFragment);

                transaction.commit();*/
                FragmentManager manager = getActivity().getSupportFragmentManager();
                FragmentTransaction trans = manager.beginTransaction();
                trans.remove(childFragment);
                trans.commit();
                manager.popBackStack();
            }
        }catch(Exception e){
            e.printStackTrace();
        }
        return view;
    }

    private void initView() {
        blockUserImage.setOnClickListener(this);
        termsImage.setOnClickListener(this);
        privacyPolicyImage.setOnClickListener(this);
        contactUsImage.setOnClickListener(this);
        cancelImage.setOnClickListener(this);
        blockLayout.setOnClickListener(this);
        reportLayout.setOnClickListener(this);
    }

    @Override
    public void onPaginationClick(UserViewPost viewPost, Boolean likeValue, int position, String enterValue) {

        if(enterValue.equals("like")||enterValue.equals("connection")) {
            try {


                Vibrator vibe = (Vibrator) getContext().getSystemService(Context.VIBRATOR_SERVICE);

                // Vibrate for 500 milliseconds
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    vibe.vibrate(VibrationEffect.createOneShot(500, VibrationEffect.DEFAULT_AMPLITUDE));
                } else {
                    //deprecated in API 26
                    vibe.vibrate(500);
                }
                try {
                    /*NotificationManager n = (NotificationManager) getContext().getSystemService(Context.NOTIFICATION_SERVICE);
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        if(n.isNotificationPolicyAccessGranted()) {
                            AudioManager audioManager = (AudioManager) getContext().getSystemService(Context.AUDIO_SERVICE);
                            audioManager.setRingerMode(AudioManager.RINGER_MODE_NORMAL);
                        }else{
                            // Ask the user to grant access
                            Intent intent = new Intent(Settings.ACTION_NOTIFICATION_POLICY_ACCESS_SETTINGS);
                            startActivityForResult(intent,ON_DO_NOT_DISTURB_CALLBACK_CODE);
                        }
                    }*/
                    AudioManager audio_mngr = (AudioManager) getContext().getSystemService(Context.AUDIO_SERVICE);
                    switch (audio_mngr.getRingerMode()) {
                        case AudioManager.RINGER_MODE_SILENT:
                            Log.i("MyApp","Silent mode");
                            audio_mngr.setRingerMode(AudioManager.RINGER_MODE_VIBRATE);
                            break;
                        case AudioManager.RINGER_MODE_VIBRATE:
                            Log.i("MyApp","Vibrate mode");
                            audio_mngr.setRingerMode(AudioManager.RINGER_MODE_VIBRATE);
                            break;
                        case AudioManager.RINGER_MODE_NORMAL:
                            Log.i("MyApp","Normal mode");
                            break;
                    }


                    audio_mngr.setVibrateSetting(AudioManager.VIBRATE_TYPE_RINGER,
                            AudioManager.VIBRATE_SETTING_ON);
                }catch (Exception e){
                    e.printStackTrace();
                }

            }catch (Exception e){
                e.printStackTrace();
            }
        }
        if (enterValue.equals("reply")){
            mListener.onConnectionCall(viewPost,enterValue,parentClassName);
        }
        if (enterValue.equals("originalPost")){
           // change single view post call add listener to call home Fragment with single value
           // userSingleValue(viewPost.getOriginalPostId(),this);
            mListener.onConnectionCall(viewPost,enterValue,parentClassName);
        }
        if (enterValue.equals("headerLayout")){
            mListener.onConnectionCall(viewPost,enterValue,parentClassName);
        }

       /* if(enterValue.equals("like")||enterValue.equals("connection")||enterValue.equals("reply")) {
            Vibrator vibe = (Vibrator) getContext().getSystemService(Context.VIBRATOR_SERVICE);
            vibe.vibrate(500);
        }*/
        if (enterValue.equals("connection")) {
            mListener.onConnectionCall(viewPost,enterValue,parentClassName);
        }
        if (enterValue.equals("like")) {
            getLikeClick(viewPost, likeValue, position);
        }
        if (enterValue.equals("tagName")){
            mListener.onTagName(viewPost,parentClassName);
        }
        if (enterValue.equals("likeCount")){
            mListener.onLikeCountUser(viewPost,parentClassName);
        }
        if (enterValue.equals("premiumUpgrade")){
            mListener.onButtonClick("premiumUpgrade","allPostBack");
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        // Check which request we're responding to
        if (requestCode == ON_DO_NOT_DISTURB_CALLBACK_CODE ) {
           // this.requestForDoNotDisturbPermissionOrSetDoNotDisturbForApi23AndUp();
        }
    }

    @Override
    public void onPaginationLongClick(UserViewPost viewPost, int position, String viewPostDetail) {

        showDataLayout.setVisibility(View.VISIBLE);
        longClickLayout.setVisibility(View.VISIBLE);
        tagImageLayout.setVisibility(View.GONE);
        Log.d("userName","session: "+userId+" "+"viewPost: "+viewPost.getName().toLowerCase().trim());
        if ((userId==(viewPost.getUserId()))){
            blockLayout.setVisibility(View.GONE);
        }else {
            blockLayout.setVisibility(View.VISIBLE);
        }
        if (viewPostDetail.equals("admin")){
            blockLayout.setVisibility(View.GONE);
        }else {
            blockLayout.setVisibility(View.VISIBLE);
        }
        selectedViewPost= viewPost;
    }

    @Override
    public void autoLinkListener(int linkType, String autoLink, String matchedString, UserViewPost viewPost) {

      String  matchedText=matchedString.trim();
        matchedText=matchedText.replace("#","");
        //strTag.set(i,strTag.get(i).replace("#", ""));.
        //LinkType 1=HashTag,2=Mention
        if (linkType==1){
            if (!Validations.isEmptyString(matchedText)) {
                List<TagList> tagList = new ArrayList<>();

                for (int i = 0; i < viewPost.getHashTagList().size(); i++) {
                    if ((matchedText.toLowerCase().trim()).contains(viewPost.getHashTagList().get(i).getName().toLowerCase().trim())) {
                        tagList.add(viewPost.getHashTagList().get(i));
                        break;
                    }
                }
                if (tagList.size()>0) {
                    mListener.onHashTagCall(tagList.get(0).getId(),tagList.get(0).getName(),parentClassName);
                }

            }
        }
        if (linkType==2){
            if (!Validations.isEmptyString(matchedText.trim())) {
                List<UserList> userList = new ArrayList<>();
                UserList personData=new UserList();
                matchedText=matchedText.replace("@", "");
                matchedText=matchedText.replace("_"," ");
                for(UserList playerName : viewPost.getUserTagList()){
                    if(playerName.getName().trim().equalsIgnoreCase(matchedText.trim())){
                        personData.setId(playerName.getId());
                        userList.add(playerName);
                        break;
                    }
                }
                if (userList.size()>0) {
                    mListener.onMentionCall(viewPost, personData.getId(), 1,parentClassName);
                }
            }


        }
    }

    @Override
    public void ResetClickListener(int position, String changeLocation) {

    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        try{
            if (childFragment!=null) {
               /* FragmentTransaction transaction = getActivity().getSupportFragmentManager()
                        .beginTransaction();

                transaction.remove(childFragment);

                transaction.commit();*/
                FragmentManager manager = getActivity().getSupportFragmentManager();
                FragmentTransaction trans = manager.beginTransaction();
                trans.remove(childFragment);
                trans.commit();
                manager.popBackStack();
            }
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    private void getLikeClick(UserViewPost viewPost,final Boolean likeValue,int position) {
        baseRequest = new BaseRequest(getContext());
        baseRequest.setBaseRequestListner(new RequestReciever() {
            @Override
            public void onSuccess(int requestCode, String Json, Object object) {
                try {

                    JSONObject jsonObject = new JSONObject(Json);

                    // utility.showToast(getContext(),object.toString());

                    modifyItem(position,likeValue,viewPost);

                }catch (Exception e){
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int requestCode, String errorCode, String message) {
                //utility.showToast(getContext(),message);
                boolean value=false;
                if (likeValue){
                    value=false;
                }else {
                    value=true;
                }
                if (message.equals("Post already liked by user."))
                modifyItem(position,true,viewPost);
            }

            @Override
            public void onNetworkFailure(int requestCode, String message) {

                utility.showToast(getContext(),message);
                try {
                    boolean value = false;
                    if (likeValue) {
                        value = false;
                    } else {
                        value = true;
                    }
                    modifyItem(position, value, viewPost);
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        });

        Integer status;
        if (likeValue){
            status=1;
        }else {
            status=2;
        }
        JsonObject main_object = new JsonObject();
        main_object.addProperty("postId", viewPost.getId());
        main_object.addProperty("likeStatus", status);
       /* JsonObject object = Functions.getClient().getJsonMapObject(
                "postId",
                "likeStatus",status );*/
        baseRequest.callAPIPostWOLoader(1, main_object, getString(R.string.api_userLikedPost));
    }
    public void modifyItem(final int position, Boolean likeValue, final UserViewPost  updateList) {
        updateList.setLikedStatus(likeValue);
        myUploadList.set(position, updateList);
        myUploadAdapter.notifyItemChanged(position);
    }

    private void feedbackDialog(UserViewPost selectedViewPost) {

        //SearchView searchView;

        final Dialog dialog = new Dialog(getContext());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.feedback_dialog);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCanceledOnTouchOutside(true);

        final Button submitButton = dialog.findViewById(R.id.submit_feed_button);
        final EditText feedEt = dialog.findViewById(R.id.feed_et_comment);
        final TextView titleName = dialog.findViewById(R.id.title_name);
        titleName.setText("Report");

        submitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String feedComment=feedEt.getText().toString().trim();
                if (!Validations.isEmptyString(feedComment)) {
                    contactUsApi(feedComment,selectedViewPost);
                    dialog.cancel();
                }else {
                    utility.showToast(getContext(),"Please Enter feedback");
                }
            }
        });



        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = dialog.getWindow();
        lp.copyFrom(window.getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);
        dialog.show();

    }
    private void contactUsApi(String feedComment, UserViewPost selectedViewPost) {
        baseRequest = new BaseRequest(getContext());
        baseRequest.setBaseRequestListner(new RequestReciever() {
            @Override
            public void onSuccess(int requestCode, String Json, Object object) {
                try {

                    JSONObject jsonObject = new JSONObject(Json);
                    String message=jsonObject.getString("message");
                    utility.showToast(getContext(),message);

                }catch (Exception e){
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int requestCode, String errorCode, String message) {
                //utility.showToast(getContext(),message);
            }

            @Override
            public void onNetworkFailure(int requestCode, String message) {

                utility.showToast(getContext(),message);
            }
        });

        /*{
            "postId":"10",
                "summary":"donated to was uploaded upon the single's release"
        }*/


        JsonObject main_object = new JsonObject();
        main_object.addProperty("postId", selectedViewPost.getId());
        main_object.addProperty("summary", feedComment);
        main_object.addProperty("userId", userId);
       /* JsonObject object = Functions.getClient().getJsonMapObject(
                "postId",
                "likeStatus",status );*/
        baseRequest.callAPIPostWOLoader(1, main_object, getString(R.string.api_userReportPost));

    }

    private void privacyApi() {
        try{
            Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://freshersapp.chetaru.co.uk/privacyPolicy"));
            startActivity(browserIntent);
        }catch (Exception e){
            e.printStackTrace();
        }

    }



    private void termsAndCondition() {
        try{
            Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://freshersapp.chetaru.co.uk/termsOfServices"));
            startActivity(browserIntent);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private void getUserBlock(UserViewPost selectedViewPost) {
        baseRequest = new BaseRequest(getContext());
        baseRequest.setBaseRequestListner(new RequestReciever() {
            @Override
            public void onSuccess(int requestCode, String Json, Object object) {
                try {

                    JSONObject jsonObject = new JSONObject(Json);
                    String message=jsonObject.getString("message");

                    utility.showToast(getContext(),message);

                }catch (Exception e){
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int requestCode, String errorCode, String message) {
                // utility.showToast(getContext(),message);
            }

            @Override
            public void onNetworkFailure(int requestCode, String message) {

                utility.showToast(getContext(),message);
            }
        });

       /* {
            "blockUserId":"6",
                "type":1
        }
        "type" - 1 for block , 2 for unblock*/
       /*if (blockUser){
           blockUser=false;
           type=1;
       }else {
           blockUser=true;
           type=2;
       }*/

        JsonObject main_object = new JsonObject();
        main_object.addProperty("blockUserId",selectedViewPost.getUserId());
        main_object.addProperty("type", 1);
       /* JsonObject object = Functions.getClient().getJsonMapObject(
                "postId",
                "likeStatus",status );*/
        baseRequest.callAPIPostWOLoader(1, main_object, getString(R.string.api_blockUser));
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        try {
            if (childFragment != null) {
                FragmentManager mFragmentMgr = getActivity().getSupportFragmentManager();
                FragmentTransaction mTransaction = mFragmentMgr.beginTransaction();
                Fragment childFragment = mFragmentMgr.findFragmentByTag("child");
                mTransaction.remove(childFragment);
                mTransaction.commit();
            }
        }catch (Exception e){
            e.printStackTrace();
        }

        unbinder.unbind();
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.block_user_layout:
            case R.id.block_user_image:
                getUserBlock(selectedViewPost);
                break;
            case R.id.terms_of_use_image:
                termsAndCondition();
                break;
            case R.id.privacy_policy_image:
                privacyApi();
                break;
            case R.id.report_layout:
            case R.id.contact_us_image:
                feedbackDialog(selectedViewPost);
                break;
            case R.id.cancel_image:
                showDataLayout.setVisibility(View.VISIBLE);
                longClickLayout.setVisibility(View.GONE);
                tagImageLayout.setVisibility(View.GONE);
                break;
        }
    }
}