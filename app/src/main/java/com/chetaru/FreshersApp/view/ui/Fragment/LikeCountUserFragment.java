package com.chetaru.FreshersApp.view.ui.Fragment;

import android.content.Context;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.SearchView;
import android.widget.TextView;

import com.chetaru.FreshersApp.R;
import com.chetaru.FreshersApp.service.model.LikeByUser;
import com.chetaru.FreshersApp.service.model.NotificationList;
import com.chetaru.FreshersApp.service.retrofit.BaseRequest;
import com.chetaru.FreshersApp.service.retrofit.Functions;
import com.chetaru.FreshersApp.service.retrofit.RequestReciever;
import com.chetaru.FreshersApp.utility.SessionParam;
import com.chetaru.FreshersApp.utility.Utility;
import com.chetaru.FreshersApp.view.adapter.LikeUserAdapter;
import com.google.gson.Gson;
import com.google.gson.JsonObject;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link LikeCountUserFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class LikeCountUserFragment extends Fragment  {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    int postId;
    Context mContext;
    Utility utility;
    SessionParam sessionParam;
    String tokenId="";
    BaseRequest baseRequest;

    List<LikeByUser> likeByUserList;
    LikeUserAdapter likeAdapter;

    private Unbinder unbinder;
    @BindView(R.id.no_like_layout)
    LinearLayout noLikeLayout;
    @BindView(R.id.recycler_view_layout)
    LinearLayout recyclerLayout;
    @BindView(R.id.like_mRecyclerView)
    RecyclerView likeRecycler;
    @BindView(R.id.like_user_search_view)
    SearchView likeSearchView;
    likeCountListener mListener;



    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    public LikeCountUserFragment() {
        // Required empty public constructor
    }


    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment LikeCountUserFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static LikeCountUserFragment newInstance(int param1, String param2) {
        LikeCountUserFragment fragment = new LikeCountUserFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            postId = getArguments().getInt(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_like_count_user, container, false);
        unbinder = ButterKnife.bind(this, view);
        //likeRecycler = view.findViewById(R.id.like_mRecyclerView);

        mContext=getContext();
        sessionParam=new SessionParam(getContext());
        tokenId=sessionParam.token;
        utility=new Utility();
        likeByUserList=new ArrayList<>();
        //postId

      //  recyclerLayout.setVisibility(View.VISIBLE);
       // noLikeLayout.setVisibility(View.GONE);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        likeRecycler.setLayoutManager(linearLayoutManager);
        likeRecycler.setItemAnimator(new DefaultItemAnimator());
        getLikeByUser(postId);
        search(likeSearchView);
        return view;
    }

    private void search(SearchView likeSearchView) {
        likeSearchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                likeAdapter.getFilter().filter(s);
                return true;
            }
        });
    }

    @Override public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    private void getLikeByUser(int postId) {
        baseRequest = new BaseRequest(mContext);
        baseRequest.setBaseRequestListner(new RequestReciever() {
            @Override
            public void onSuccess(int requestCode, String Json, Object object) {
                Gson gson = new Gson();
                try {

                    JSONArray jsonArray = new JSONArray(object.toString());
                    likeByUserList = baseRequest.getDataList(jsonArray, LikeByUser.class);
                    if (likeByUserList.size()>0) {
                        recyclerLayout.setVisibility(View.VISIBLE);
                        noLikeLayout.setVisibility(View.GONE);
                        likeAdapter = new LikeUserAdapter(likeByUserList, mContext, new LikeUserAdapter.likeUserListener() {
                            @Override
                            public void otherLikeByUser(LikeByUser likeByUser) {

                                mListener.likeByUser(likeByUser,postId);
                            }
                        });
                        likeRecycler.setAdapter(likeAdapter);
                    }else {
                        recyclerLayout.setVisibility(View.GONE);
                        noLikeLayout.setVisibility(View.VISIBLE);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int requestCode, String errorCode, String message) {
                //errorLayout.showError(message);
               // utility.showToast(mContext, message);
                recyclerLayout.setVisibility(View.GONE);
                noLikeLayout.setVisibility(View.VISIBLE);
            }

            @Override
            public void onNetworkFailure(int requestCode, String message) {
                //errorLayout.showError(message);
                utility.showToast(mContext, message);
            }
        });

        JsonObject object = Functions.getClient().getJsonMapObject(
                "postId", postId + ""
        );
        baseRequest.callAPIPost(1, object, getString(R.string.api_postLikedByUser));

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof likeCountListener) {
            mListener = (likeCountListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement TagChildFragment");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }



    public interface likeCountListener{
        public  void likeByUser(LikeByUser like,int postId);
    }
}