package com.chetaru.FreshersApp.view.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.chetaru.FreshersApp.R;
import com.chetaru.FreshersApp.service.model.UserLoginRequest;
import com.squareup.picasso.Picasso;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class LoginDataAdapter extends RecyclerView.Adapter<LoginDataAdapter.MyViewHolder> {
    List<UserLoginRequest> mList;
    Context mContext;
    loginListener mListener;

    public LoginDataAdapter(List<UserLoginRequest> mList, Context mContext,loginListener mListener) {
         this.mList = mList;
        this.mContext = mContext;
        this.mListener=mListener;
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {
        CircleImageView profileImage;
        TextView userName;
        ImageView cancelImage;
        public MyViewHolder(@NonNull View view) {
            super(view);
            profileImage=view.findViewById(R.id.iv_logo);
            userName=view.findViewById(R.id.login_user_Name);
            cancelImage=view.findViewById(R.id.cancel_image);
        }
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view= LayoutInflater.from(mContext).inflate(R.layout.login_user_list,parent,false);

        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        UserLoginRequest loginUser=mList.get(position);
        holder.userName.setText("Log in as "+ loginUser.getName());
        //Picasso.with(mContext).load(loginUser.getProfileImage()).into(holder.profileImage);
        Glide.with(mContext).load(loginUser.getProfileImage()).placeholder(R.drawable.profile_pics).into(holder.profileImage);
        holder.cancelImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mListener.removeRecord(loginUser);
            }
        });
        holder.userName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mListener.loginUserWith(loginUser);
            }
        });

    }

    @Override
    public int getItemCount() {
        try {
            return mList.size();
        }catch (Exception e) {
        return 0;
        }

    }

    public interface  loginListener{
        public void removeRecord(UserLoginRequest  loginUser);
        public void loginUserWith(UserLoginRequest  loginUser);

    }


}
