package com.chetaru.FreshersApp.view.ui.Fragment;

import android.content.Context;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.chetaru.FreshersApp.R;

import com.chetaru.FreshersApp.service.model.UserViewPost;
import com.chetaru.FreshersApp.service.retrofit.BaseRequest;
import com.chetaru.FreshersApp.service.retrofit.RequestReciever;
import com.chetaru.FreshersApp.utility.SessionParam;
import com.chetaru.FreshersApp.utility.Utility;
import com.chetaru.FreshersApp.view.adapter.MyUploadAdapter;
import com.chetaru.FreshersApp.view.adapter.OtherUploadAdapter;
import com.chetaru.FreshersApp.view.customView.ItemOffsetDecoration;
import com.google.gson.Gson;
import com.google.gson.JsonObject;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link OtherUserUploadFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class OtherUserUploadFragment extends Fragment {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private static final String VIEWPOST = "viewPost";

    //initlize view Bind
    private Unbinder unbinder;
    final int sdk = android.os.Build.VERSION.SDK_INT;

    // TODO: Rename and change types of parameters
    private int personId;
    private int mParam2;

    public OtherUserUploadFragment() {
        // Required empty public constructor
    }

    @BindView(R.id.other_upload_recycler_view)
    RecyclerView uploadRecyclerView;
    @BindView(R.id.other_upload_swap_refresh)
    SwipeRefreshLayout otherSwipeRefresh;
    @BindView(R.id.recycler_other_upload)
    LinearLayout recyclerLayout;
    @BindView(R.id.other_upload_layout)
    LinearLayout otherLayout;

    //initialize adapter and all
    LinearLayoutManager layoutManager;
    //OtherUploadAdapter uploadAdapter;
    MyUploadAdapter uploadAdapter;

    //class initligation
    Utility utility;
    SessionParam sessionParam;
    private BaseRequest baseRequest;

    //List<UserViewPost> uploadList;
    List<UserViewPost> uploadList;
    int  userId=0;
    private static final int PAGE_START = 1;
    private boolean isLoading = false;
    private int currentPage = PAGE_START;
    private  int TOTAL_PAGES = 1;
    private boolean isLastPage = false;
    Date currentDate;
    String dateTime;
    int connectionStatus=0;

    OtherUploadListenerFragment mListener;

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment OtherUserUploadFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static OtherUserUploadFragment newInstance(UserViewPost viewPost, Integer param1, Integer param2) {
        OtherUserUploadFragment fragment = new OtherUserUploadFragment();
        Bundle args = new Bundle();
        args.putSerializable(VIEWPOST,viewPost);
        args.putInt(ARG_PARAM1, param1);
        args.putInt(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            personId = getArguments().getInt(ARG_PARAM1);
            mParam2 = getArguments().getInt(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_other_user_upload, container, false);
        unbinder = ButterKnife.bind(this, view);
        utility=new Utility();
        uploadList=new ArrayList<>();
        sessionParam=new SessionParam(getContext());


        uploadAdapter = new MyUploadAdapter(uploadList, getContext(),
                new MyUploadAdapter.MyUploadsListener() {
                    @Override
                    public void MyUploadCancelClick(UserViewPost myUploads,int position) {
                        //getCancelPost(myUploads,position);
                    }

                    @Override
                    public void MyUploadViewPost(List<UserViewPost> myUploads, int position) {
                       // mListener.userSinglePost(myUploads.get(position),personId);
                        mListener.otherUserPost(myUploads,position,"otherUserPost");
                    }
                },"otherUploads");
        uploadRecyclerView.setAdapter(uploadAdapter);
        layoutManager = new GridLayoutManager(getContext(),3);
        uploadRecyclerView.setLayoutManager(layoutManager);
        uploadRecyclerView.setHasFixedSize(true);
        ItemOffsetDecoration itemDecoration = new ItemOffsetDecoration(getContext(), R.dimen.item_offset_medium);
        uploadRecyclerView.addItemDecoration(itemDecoration);

        if (personId>0)
            getUserUploads(personId);
        getConnectionStatus(personId);

        uploadRecyclerView.addOnScrollListener(new PaginationScrollList(layoutManager) {
            @Override
            protected void loadMoreItems() {
                isLastPage=true;
                currentPage+=1;
                api_loadNextPage();
            }

            @Override
            public boolean isLastPage() {
                return isLastPage;
            }

            @Override
            public boolean isLoading() {
                return isLoading;
            }
        });
        otherSwipeRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                otherSwipeRefresh.setRefreshing(true);
                otherUplaodRefresh();
                otherSwipeRefresh.setRefreshing(false);
            }
        });
        otherLayout.setVisibility(View.GONE);
        recyclerLayout.setVisibility(View.GONE);
        return view;
    }

    private void getConnectionStatus(int personId) {
        baseRequest = new BaseRequest(getContext());
        baseRequest.setBaseRequestListner(new RequestReciever() {
            @Override
            public void onSuccess(int requestCode, String Json, Object object) {
                try {

                    Gson gson = new Gson();
                    JSONObject jsonObject = new JSONObject(Json);
                    JSONObject response= (JSONObject) jsonObject.get("data");
                    try {
                        connectionStatus  =response.getInt("connectionStatus");
                        if (connectionStatus==2){

                        }else {
                        }
                    }catch (Exception e){
                        e.printStackTrace();
                    }

                    // JSONArray jsonArray = new JSONArray(object.toString());
                    //localeList = baseRequest.getDataList(jsonArray, LocaleList.class);
                    /*connectionStatus :- 1 for pending, 2 for accepted, 3 for decline, 4 for unfriend, 5 for blocked*/



                }catch (Exception e){
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int requestCode, String errorCode, String message) {
                //utility.showToast(getBaseContext(),message);
            }

            @Override
            public void onNetworkFailure(int requestCode, String message) {
                utility.showToast(getContext(),message);
            }
        });



        JsonObject main_object = new JsonObject();
        main_object.addProperty("userId",personId );
        baseRequest.callAPIPostWOLoader(1, main_object, getString(R.string.api_ConnectionStatus));

    }
    private void getUserUploads(int personId) {
        uploadList.clear();
        uploadAdapter.clear();
        baseRequest = new BaseRequest(getContext());
        baseRequest.setBaseRequestListner(new RequestReciever() {
            @Override
            public void onSuccess(int requestCode, String Json, Object object) {
                try {
                    Gson gson = new Gson();
                    JSONObject jsonObject = new JSONObject(Json);
                    TOTAL_PAGES = jsonObject.optInt("totalPageCount");
                    // currentPage = jsonObject.optInt("currentPage");
                    //ArrayList myUploadList= (ArrayList) jsonObject.get("data");
                    JSONArray userJsonArray = jsonObject.getJSONArray("data");

                    uploadList = baseRequest.getDataList(userJsonArray, UserViewPost.class);
                    if (uploadList.size()>0){
                        otherLayout.setVisibility(View.GONE);
                        recyclerLayout.setVisibility(View.VISIBLE);
                    }else {
                        otherLayout.setVisibility(View.VISIBLE);
                        recyclerLayout.setVisibility(View.GONE);
                    }

                    uploadRecyclerView.setAdapter(uploadAdapter);
                    uploadAdapter.addAll(uploadList);
                    if (currentPage < TOTAL_PAGES)
                        uploadAdapter.addLoadingFooter();
                    else
                        isLastPage = true;
                    try {
                        if (uploadList.size() > 0) {
                            dateTime = uploadList.get(0).getCreated_at();
                        }
                    }catch (Exception e){
                        e.printStackTrace();
                    }

                }catch (Exception e){
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int requestCode, String errorCode, String message) {
                //  utility.showToast(mContext,message);
               otherLayout.setVisibility(View.VISIBLE);
                recyclerLayout.setVisibility(View.GONE);
            }

            @Override
            public void onNetworkFailure(int requestCode, String message) {
                utility.showToast(getContext(),message);
            }
        });

        JsonObject main_object = new JsonObject();
        main_object.addProperty("userId", personId);
        main_object.addProperty("page", currentPage);
        baseRequest.callAPIPostWOLoader(1, main_object, getString(R.string.api_getUserUploads));

    }
    private void otherUplaodRefresh(){
        baseRequest=new BaseRequest(getContext());
        baseRequest.setBaseRequestListner(new RequestReciever() {
            @Override
            public void onSuccess(int requestCode, String Json, Object object) {

                try{
                    otherSwipeRefresh.setRefreshing(false);
                    Gson gson=new Gson();
                    JSONObject jsonObject = new JSONObject(Json);
                    //ArrayList myUploadList= (ArrayList) jsonObject.get("data");
                    JSONArray userJsonArray = jsonObject.getJSONArray("data");

                    List<UserViewPost>   newUploadList = baseRequest.getDataList(userJsonArray, UserViewPost.class);

                    if (newUploadList.size()>0) {
                        for (int i = 0; i < newUploadList.size(); i++) {
                            uploadList.add(i, newUploadList.get(i));
                        }
                    }
                    // uploadAdapter.addAll(uploadList);
                }catch (Exception  e){
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int requestCode, String errorCode, String message) {
                //utility.showToast(mContext,message);
            }

            @Override
            public void onNetworkFailure(int requestCode, String message) {

                utility.showToast(getContext(),message);
            }
        });
        JsonObject main_object = new JsonObject();

        main_object.addProperty("userId", personId);
        main_object.addProperty("page", 0);
        main_object.addProperty("date", dateTime);
        baseRequest.callAPIPost(1, main_object, getString(R.string.api_getUserUploads));
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }
    @Override public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }


    private void api_loadNextPage() {
        baseRequest=new BaseRequest(getContext());
        baseRequest.setBaseRequestListner(new RequestReciever() {
            @Override
            public void onSuccess(int requestCode, String Json, Object object) {
                // utility.showToast(mContext,);
                try {
                    Gson gson = new Gson();
                    JSONObject jsonObject = new JSONObject(Json);
                    TOTAL_PAGES = jsonObject.optInt("totalPageCount");
                    // currentPage = jsonObject.optInt("currentPage");
                    //ArrayList myUploadList= (ArrayList) jsonObject.get("data");
                    JSONArray userJsonArray = jsonObject.getJSONArray("data");
                    uploadList = baseRequest.getDataList(userJsonArray, UserViewPost.class);
                    uploadAdapter.removeLoadingFooter();
                    isLoading = false;
                    uploadAdapter.addAll(uploadList);
                    //rv_list_archive.setAdapter(ad_notificationList);
                    if (currentPage != TOTAL_PAGES) uploadAdapter.addLoadingFooter();
                    else isLastPage = true;

                }catch (Exception e){
                    e.printStackTrace();
                }
            }
            @Override
            public void onFailure(int requestCode, String errorCode, String message) {
                // utility.showToast(mContext,message);
            }

            @Override
            public void onNetworkFailure(int requestCode, String message) {
                utility.showToast(getContext(),message);
            }
        });
        JsonObject main_object = new JsonObject();
        main_object.addProperty("userId", personId);
        main_object.addProperty("page", currentPage);

        baseRequest.callAPIPostWOLoader(1, main_object, getString(R.string.api_getUserUploads));
    }

    public abstract class PaginationScrollList extends RecyclerView.OnScrollListener {

        private LinearLayoutManager layoutManager;

        public PaginationScrollList(LinearLayoutManager layoutManager) {
            this.layoutManager = layoutManager;
        }

        @Override
        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
            super.onScrolled(recyclerView, dx, dy);

            int visibleItemCount = layoutManager.getChildCount();
            int totalItemCount = layoutManager.getItemCount();
            int firstVisibleItemPosition = layoutManager.findFirstVisibleItemPosition();

            if (!isLoading() && !isLastPage()) {
                if ((visibleItemCount + firstVisibleItemPosition) >= totalItemCount
                        && firstVisibleItemPosition >= 0) {
                    loadMoreItems();
                }
            }
        }

        protected abstract void loadMoreItems();

        public abstract boolean isLastPage();

        public abstract boolean isLoading();

    }
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OtherUploadListenerFragment) {
            mListener = (OtherUploadListenerFragment) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement TagChildFragment");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public  interface OtherUploadListenerFragment{
        void userSinglePost(UserViewPost post,int personId);
        void otherUserPost(List<UserViewPost> postList,int postPosition,String otherUser);
    }
}