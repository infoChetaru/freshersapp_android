package com.chetaru.FreshersApp.view.ui.Activity;

import androidx.appcompat.app.AppCompatActivity;







import android.os.Bundle;


import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.bumptech.glide.Glide;
import com.chetaru.FreshersApp.R;
import com.squareup.picasso.Picasso;


public class UploadImageActivity extends AppCompatActivity implements View.OnClickListener {


    String imageString;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try {
            requestWindowFeature(Window.FEATURE_NO_TITLE);
            this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        }catch (Exception e){
            e.printStackTrace();
        }
        setContentView(R.layout.activity_upload_image);
        /*if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            setSharedElementEnterTransition(TransitionInflater.from(getContext()).inflateTransition(android.R.transition.move));
        }*/
       // prepareTransitions()
        Bundle bundle = getIntent().getExtras();

        //Extract the data…
        if (bundle!=null)
            imageString = bundle.getString("imageString");

        ImageView imageView=findViewById(R.id.my_profile_logo);
        //Picasso.with(this).load(imageString).into(imageView);
        Glide.with(this).load(imageString).placeholder(R.drawable.profile_pics).into(imageView);
       /* Fragment fragment= imageZoomProfile.newInstance(imageString,"");
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.nav_host_fragment, fragment);
        transaction.addToBackStack(null);
        transaction.commit();*/

    }

    @Override
    public void onClick(View v) {

    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                supportFinishAfterTransition();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
