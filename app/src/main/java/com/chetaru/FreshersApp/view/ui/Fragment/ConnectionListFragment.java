package com.chetaru.FreshersApp.view.ui.Fragment;

import android.content.Context;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.chetaru.FreshersApp.R;
import com.chetaru.FreshersApp.service.model.ConnListResponse;
import com.chetaru.FreshersApp.service.model.NotificationList;
import com.chetaru.FreshersApp.service.retrofit.BaseRequest;
import com.chetaru.FreshersApp.service.retrofit.Functions;
import com.chetaru.FreshersApp.service.retrofit.RequestReciever;
import com.chetaru.FreshersApp.utility.SessionParam;
import com.chetaru.FreshersApp.utility.Utility;
import com.chetaru.FreshersApp.view.adapter.ConnectionAdapter;
import com.chetaru.FreshersApp.view.callback.ConnectionListCallback;
import com.google.gson.Gson;
import com.google.gson.JsonObject;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link ConnectionListFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ConnectionListFragment extends Fragment implements ConnectionAdapter.ConnClickListener {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private static final String ARG_PARAM3 = "param3";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;



    private Unbinder unbinder;
    List<ConnListResponse> connList;

    Utility utility;
    SessionParam sessionParam;
    String tokenId="";
    BaseRequest baseRequest;
    Context mContext;
    private static final int PAGE_START = 1;
    private boolean isLoading = false;
    private int currentPage = PAGE_START;
    private static int TOTAL_PAGES = 1;
    private boolean isLastPage = false;
    String dateTime;
    //Initilaize view
    @BindView(R.id.conn_recycler_view)
    RecyclerView connRecyclerView;
    @BindView(R.id.connection_refresh)
    SwipeRefreshLayout connRefresh;

    @BindView(R.id.no_connection_layout)
    LinearLayout connectionLayout;
    @BindView(R.id.recycler_layout)
    LinearLayout recyclerLayout;
    ConnectionAdapter connAdapter;
    //Call back
    ConnectionListCallback mListener;

    boolean valueChange=false;
    public ConnectionListFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment ConnectionListFeagment.
     */
    // TODO: Rename and change types and number of parameters
    public static ConnectionListFragment newInstance(String param1, String param2,String chatOpenMsg) {
        ConnectionListFragment fragment = new ConnectionListFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        args.putString(ARG_PARAM3, chatOpenMsg);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_connection_list_feagment, container, false);
        unbinder = ButterKnife.bind(this, view);
        utility=new Utility();
        sessionParam=new SessionParam(getContext());
        connList=new ArrayList<>();
        mContext=getContext();
        this.mListener= (ConnectionListCallback) getContext();
        connAdapter = new ConnectionAdapter(connList, getContext(),this);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        connRecyclerView.setLayoutManager(linearLayoutManager);
        connRecyclerView.setItemAnimator(new DefaultItemAnimator());
        connRecyclerView.setAdapter(connAdapter);
        api_connectionList();

        connRecyclerView.addOnScrollListener(new PaginationScrollList(linearLayoutManager) {
            @Override
            protected void loadMoreItems() {
                isLastPage=true;
                currentPage+=1;
                api_loadNextPage();
            }

            @Override
            public boolean isLastPage() {
                return isLastPage;
            }

            @Override
            public boolean isLoading() {
                return isLoading;
            }
        });
        connRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                connRefresh.setRefreshing(true);
                if(connList.size()>0) {
                    MyUploadRefresh();
                }else{
                    api_connectionList();
                }
                connRefresh.setRefreshing(false);
            }
        });
        connectionLayout.setVisibility(View.VISIBLE);
        recyclerLayout.setVisibility(View.GONE);
        return view;
    }

    @Override
    public void onPause() {
        super.onPause();
        valueChange=true;
      //  api_connectionList();
    }


    @Override
    public void onResume() {
        super.onResume();
        if (valueChange){
            valueChange=false;
            api_connectionList();
        }


    }

    private void api_connectionList() {
        currentPage=PAGE_START;
        connList.clear();
        connAdapter.clear();
        baseRequest=new BaseRequest(mContext);
        baseRequest.setBaseRequestListner(new RequestReciever() {
            @Override
            public void onSuccess(int requestCode, String Json, Object object) {
               // connList.clear();
                Gson gson=new Gson();
                try{
                    JSONObject jsonObject=new JSONObject(Json);
                    TOTAL_PAGES=jsonObject.optInt("totalPageCount");
                    JSONArray jsonArray=new JSONArray(object.toString());
                    JSONArray userJsonArray = jsonObject.getJSONArray("data");
                    connList=baseRequest.getDataList(userJsonArray,ConnListResponse.class);

                    if (connList.size()>0){
                        connectionLayout.setVisibility(View.GONE);
                        recyclerLayout.setVisibility(View.VISIBLE);
                    }else {
                        connectionLayout.setVisibility(View.VISIBLE);
                        recyclerLayout.setVisibility(View.GONE);
                    }
                    /*for (int i=1;i<2;i++){
                        ConnListResponse viewPost=new ConnListResponse();
                        viewPost.setConnectionId(i+1);
                        viewPost.setUserName("Ritesh");
                        viewPost.setLastMessage("dsfds");
                        viewPost.setUserImageUrl("https://production.chetaru.co.uk/freshersApp/public/uploads/postImages/post_5eaa73782290c1588228984.png");
                    connList.add(viewPost);
                    }*/
                    connAdapter.addAll(connList);
                   /* connAdapter = new ConnectionAdapter(connList, getContext(), new ConnectionAdapter.ConnClickListener() {
                        @Override
                        public void itemClickListener(ConnListResponse connList) {
                            mListener.ConnectionChatOpen(connList);

                        }

                        @Override
                        public void logoClickListener(View view, String imageString) {
                            mListener.userLogoClick(view,imageString);
                        }
                    });*/


                        if (currentPage < TOTAL_PAGES) connAdapter.addLoadingFooter();
                        else isLastPage = true;

                    try {
                        if (connList.size() > 0) {
                            dateTime = connList.get(0).getUpdatedAt();
                        }
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int requestCode, String errorCode, String message) {
                //utility.showToast(mContext,message);
                try {
                    connectionLayout.setVisibility(View.VISIBLE);
                    recyclerLayout.setVisibility(View.GONE);
                }catch (Exception e){
                    e.printStackTrace();
                }
            }

            @Override
            public void onNetworkFailure(int requestCode, String message) {
                utility.showToast(mContext,message);
               // connectionLayout.setVisibility(View.VISIBLE);
                //recyclerLayout.setVisibility(View.GONE);
            }
        });
        JsonObject main_object = new JsonObject();
        main_object.addProperty("page", currentPage);
        baseRequest.callAPIPost(1, main_object, getString(R.string.api_acceptedConnectionList));

    }
    private void api_loadNextPage() {
        baseRequest=new BaseRequest(mContext);
        baseRequest.setBaseRequestListner(new RequestReciever() {
            @Override
            public void onSuccess(int requestCode, String Json, Object object) {
                // connList.clear();
                Gson gson=new Gson();
                try{
                    JSONObject jsonObject=new JSONObject(Json);
                    TOTAL_PAGES=jsonObject.optInt("totalPageCount");
                    JSONArray jsonArray=new JSONArray(object.toString());
                    connList=baseRequest.getDataList(jsonArray,ConnListResponse.class);
                    //connAdapter.addAll(connList);
                    connAdapter.removeLoadingFooter();
                    isLoading = false;
                    connAdapter.addAll(connList);
                    //rv_list_archive.setAdapter(ad_notificationList);
                    if (currentPage != TOTAL_PAGES) connAdapter.addLoadingFooter();
                    else isLastPage = true;
                }catch (Exception e){
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int requestCode, String errorCode, String message) {
               // utility.showToast(mContext,message);
            }

            @Override
            public void onNetworkFailure(int requestCode, String message) {
                utility.showToast(mContext,message);
            }
        });
        JsonObject main_object = new JsonObject();
        main_object.addProperty("page", currentPage);
        baseRequest.callAPIPost(1, main_object, getString(R.string.api_acceptedConnectionList));

    }

    private void MyUploadRefresh(){
        baseRequest=new BaseRequest(getContext());
        baseRequest.setBaseRequestListner(new RequestReciever() {
            @Override
            public void onSuccess(int requestCode, String Json, Object object) {

                try{
                    connRefresh.setRefreshing(false);
                    Gson gson=new Gson();
                    JSONObject jsonObject = new JSONObject(Json);
                    JSONArray userJsonArray = jsonObject.getJSONArray("data");

                    List<ConnListResponse>   newUploadList = baseRequest.getDataList(userJsonArray, ConnListResponse.class);

                    JSONArray idsArray=new JSONArray();
                    JSONArray userIdsArray=new JSONArray();
                    try{
                         idsArray=jsonObject.getJSONArray("connectionList");

                       // connectionIds = (List<Integer>) idsArray;
                        //connectionIds= (List<Integer>) jsonObject.get("connectionList");

                    }catch (Exception e){
                        e.printStackTrace();
                    }
                    try {
                        userIdsArray=jsonObject.getJSONArray("deletedUsersList");
                    }catch (Exception e)
                    {
                        e.printStackTrace();
                    }
                    if (newUploadList.size()>0) {
                        for (int i = 0; i < newUploadList.size(); i++) {
                            for (int j = 0; j < connList.size(); j++) {
                                if (newUploadList.get(i).getConnectionId().equals(connList.get(j).getConnectionId())) {
                                    connList.remove(j);
                                    connRecyclerView.removeViewAt(j);
                                    connAdapter.notifyItemRemoved(j);
                                }
                                /*if (newUploadList.get(i).getUserId().equals(connList.get(j).getUserId())) {
                                    connList.remove(j);
                                }*/
                            }
                        }


                    }
                    if (idsArray.length()>0) {
                        for (int p = 0; p < idsArray.length(); p++) {
                            for (int q = 0; q < connList.size(); q++) {
                               /* if (idsArray.length()<=1) {
                                    if (idsArray.equals(connList.get(q).getConnectionId())) {
                                        connList.remove(q);
                                        connRecyclerView.removeViewAt(q);
                                        connAdapter.notifyItemRemoved(q);
                                    }
                                }else*/
                               if (idsArray.get(p).equals(connList.get(q).getConnectionId())) {
                                    connList.remove(q);
                                    connRecyclerView.removeViewAt(q);
                                    connAdapter.notifyItemRemoved(q);
                                }


                            }
                        }
                    }
                    if (userIdsArray.length()>0) {
                        for (int p = 0; p < userIdsArray.length(); p++) {
                            for (int q = 0; q < connList.size(); q++) {
                               /* if (idsArray.length()<=1) {
                                    if (idsArray.equals(connList.get(q).getConnectionId())) {
                                        connList.remove(q);
                                        connRecyclerView.removeViewAt(q);
                                        connAdapter.notifyItemRemoved(q);
                                    }
                                }else*/
                                if (userIdsArray.get(p).equals(connList.get(q).getUserId())) {
                                    connList.remove(q);
                                    connRecyclerView.removeViewAt(q);
                                    connAdapter.notifyItemRemoved(q);
                                }


                            }
                        }
                    }
                    if (newUploadList.size()>0){
                        for (int k=0;k<newUploadList.size();k++){
                            if (connList.size()>0) {
                                connList.add(k, newUploadList.get(k));
                            }else {
                                connList.add(newUploadList.get(k));
                            }
                        }
                    }
                    // uploadAdapter.addAll(uploadList);
                    if(connList.size()>0){
                        connAdapter.clear();
                    connAdapter.addAll(connList);
                    }
                    connAdapter.notifyDataSetChanged();
                }catch (Exception  e){
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int requestCode, String errorCode, String message) {
               // utility.showToast(mContext,message);
            }

            @Override
            public void onNetworkFailure(int requestCode, String message) {

                utility.showToast(mContext,message);
            }
        });
        JsonObject main_object = new JsonObject();

        main_object.addProperty("page", 0);
        main_object.addProperty("date", dateTime);
        baseRequest.callAPIPost(1, main_object, getString(R.string.api_acceptedConnectionList));
    }


    //remove view From unbind
    @Override public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void itemClickListener(ConnListResponse connList) {
        mListener.ConnectionChatOpen(connList);
    }

    @Override
    public void logoClickListener(View view, String imageString) {
        mListener.userLogoClick(view,imageString);

    }

    public abstract class PaginationScrollList extends RecyclerView.OnScrollListener {

        private LinearLayoutManager layoutManager;

        public PaginationScrollList(LinearLayoutManager layoutManager) {
            this.layoutManager = layoutManager;
        }

        @Override
        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
            super.onScrolled(recyclerView, dx, dy);

            int visibleItemCount = layoutManager.getChildCount();
            int totalItemCount = layoutManager.getItemCount();
            int firstVisibleItemPosition = layoutManager.findFirstVisibleItemPosition();

            if (!isLoading() && !isLastPage()) {
                if ((visibleItemCount + firstVisibleItemPosition) >= totalItemCount
                        && firstVisibleItemPosition >= 0) {
                    loadMoreItems();
                }
            }
        }

        protected abstract void loadMoreItems();

        public abstract boolean isLastPage();

        public abstract boolean isLoading();

    }

}
