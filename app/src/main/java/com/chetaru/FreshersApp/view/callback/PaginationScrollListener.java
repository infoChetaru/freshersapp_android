package com.chetaru.FreshersApp.view.callback;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.chetaru.FreshersApp.utility.SessionParam;

public abstract class PaginationScrollListener extends RecyclerView.OnScrollListener {

    LinearLayoutManager layoutManager;
    private static final int PAGE_SIZE = 5;
    /**
     * Supporting only LinearLayoutManager for now.
     *
     * @param layoutManager
     */
    public PaginationScrollListener(LinearLayoutManager layoutManager) {
        this.layoutManager = layoutManager;
    }
    @Override
    public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
        super.onScrolled(recyclerView, dx, dy);
        try {
            int scrollPre=0;
            scrollPre=dx;
        int visibleItemCount = layoutManager.getChildCount();
        int totalItemCount = layoutManager.getItemCount();
        int firstVisibleItemPosition = layoutManager.findFirstVisibleItemPosition();

            if (!isLoading() && !isLastPage()) {
                if ((visibleItemCount + firstVisibleItemPosition) >= totalItemCount
                        && firstVisibleItemPosition >= 0
                        && totalItemCount >= PAGE_SIZE) {
                    loadMoreItems();
                }
            }
            if (!isLoading()&&!isLastPage()){
                if (firstVisibleItemPosition<=0
                        &&visibleItemCount<2
                        && scrollPre<0
                && firstVisibleItemPosition>=0
                && totalItemCount>=PAGE_SIZE){
                    loadPreviousItems();
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }
    protected abstract void loadMoreItems();
    protected abstract void loadPreviousItems();
    public abstract int getTotalPageCount();
    public abstract boolean isLastPage();
    public abstract boolean isLoading();
}
