package com.chetaru.FreshersApp.view.ui.Fragment;

import android.content.Context;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.chetaru.FreshersApp.R;
import com.chetaru.FreshersApp.utility.Validations;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link PremiumMsgFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class PremiumMsgFragment extends Fragment implements View.OnClickListener{
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private static final String ARG_PARAM3 = "param3";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private String title;
    private PremiumMsgListenerFragment mListener;
    Button upgradeButton,cancelButton;
    TextView extraSpaceText;
    TextView premiumMsgText,premiumTitleTxt;
    View headingView;

    public PremiumMsgFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment premiumMsgFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static PremiumMsgFragment newInstance(String param1, String param2,String title) {
        PremiumMsgFragment fragment = new PremiumMsgFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        args.putString(ARG_PARAM3, title);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
            title = getArguments().getString(ARG_PARAM3);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_premium_msg, container, false);
        ImageView backImage=view.findViewById(R.id.back_image_premium);
        backImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               if (!mParam1.equals("")){
                   if (mParam1.equals("settingBack")) {
                       mListener.BackPremiumFragment();
                   }
               }
            }
        });
        upgradeButton=view.findViewById(R.id.premium_upgrade_button);
        premiumMsgText=view.findViewById(R.id.premium_msg_text);
        cancelButton=view.findViewById(R.id.premium_cancel_button);
        premiumTitleTxt=view.findViewById(R.id.premium_title_text);
        headingView=view.findViewById(R.id.premium_heading_view);
        headingView.setVisibility(View.GONE);
        if (!Validations.isEmptyString(mParam2)){
            premiumMsgText.setText(mParam2);
        }
        if (!Validations.isEmptyString(title)){
            premiumTitleTxt.setText(title);
        }
        if (!Validations.isEmptyString(mParam1)){
            if (mParam1.equals("otherTabBack")){
                cancelButton.setVisibility(View.GONE);
                headingView.setVisibility(View.VISIBLE);
            }
        }else {
            headingView.setVisibility(View.GONE);
        }
        upgradeButton.setOnClickListener(this);
        cancelButton.setOnClickListener(this);
        return view;
    }



    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof PremiumMsgListenerFragment) {
            mListener = (PremiumMsgListenerFragment) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement TagChildFragment");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()){
            case R.id.premium_upgrade_button:
                mListener.upgradeButton("upgrade",mParam2,title);
                break;
            case R.id.premium_cancel_button:
                try {
                    if (!mParam1.equals("")) {
                        if (mParam1.equals("settingBack")) {
                            mListener.premiumButton("settingBack",mParam2);
                        } else if (mParam1.equals("homeBack")){
                            mListener.premiumButton("homeBack",mParam2);
                        }else if (mParam1.equals("allPostBack")){
                            mListener.premiumButton("allPostBack",mParam2);
                        }else if (mParam1.equals("notiBack")){
                            mListener.premiumButton("notiBack",mParam2);
                        }else if (mParam1.equals("otherTabBack")){
                            mListener.premiumButton("otherTabBack",mParam2);
                        }else if (mParam1.equals("tagBack")){
                            mListener.premiumButton("tagBack",mParam2);
                        }else {
                            mListener.premiumButton("cancel",mParam2);
                        }
                    } else {
                        mListener.premiumButton("cancel",mParam2);
                    }
                }catch (Exception e){
                    mListener.premiumButton("cancel",mParam2);
                    e.printStackTrace();
                }

                break;
        }
    }

    public interface PremiumMsgListenerFragment{
        void BackPremiumFragment();
        void premiumButton(String premium,String premiumDesc);
        void upgradeButton(String premium,String premiumDesc,String title);
    }
}
