package com.chetaru.FreshersApp.view.ui;

import android.Manifest;
import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Paint;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.AppCompatCheckBox;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.chetaru.FreshersApp.R;
import com.chetaru.FreshersApp.service.repository.factory.SignUpModelFactory;
import com.chetaru.FreshersApp.service.retrofit.BaseRequest;
import com.chetaru.FreshersApp.service.retrofit.Functions;
import com.chetaru.FreshersApp.service.retrofit.RequestReciever;
import com.chetaru.FreshersApp.utility.BaseActivity;
import com.chetaru.FreshersApp.utility.FileCompressor;
import com.chetaru.FreshersApp.utility.MarshMallowPermission;
import com.chetaru.FreshersApp.utility.SessionParam;
import com.chetaru.FreshersApp.utility.Utility;
import com.chetaru.FreshersApp.utility.Validations;
import com.chetaru.FreshersApp.view.ui.Activity.HomeActivity;
import com.chetaru.FreshersApp.viewModel.SignUpViewModel;
import com.google.gson.JsonObject;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;
import com.shagi.materialdatepicker.date.DatePickerFragmentDialog;

import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Optional;
import id.zelory.compressor.Compressor;

public class SignUpActivity extends BaseActivity implements View.OnClickListener{

    final Calendar myCalendar = Calendar.getInstance();
    SignUpViewModel viewModel;

    @Nullable
    @BindView(R.id.sing_up_user_name_txt)
    TextView userNameText;
    @Nullable
    @BindView(R.id.sign_up_full_name_et)
    EditText fullNameEdit;
    @Nullable
    @BindView(R.id.sign_date_of_birth_et)
    TextView dateOfBirthEdit;
    @Nullable
    @BindView(R.id.sign_bio_et)
    EditText bioEditText;
    @Nullable
    @BindView(R.id.sign_male_image)
    ImageView maleIconImage;
    @Nullable
    @BindView(R.id.sign_female_image)
    ImageView femaleIconImage;
    @Nullable
    @BindView(R.id.sign_other_image)
    ImageView otherIconImage;
    @Nullable
    @BindView(R.id.sign_up_image)
    ImageView selectImageGalary;
    @Nullable
    @BindView(R.id.sign_up_select_imageView)
    ImageView profilePics;
    @BindView(R.id.sign_up_check_box)
    CheckBox checkboxValue;
    @BindView(R.id.sign_up_terms_condition)
    TextView termsConditionTxt;
    @BindView(R.id.sign_up_privacy_policy)
    TextView privacyPolicyTxt;

    @Nullable
    @BindView(R.id.sign_up_button)
    Button signUpButton;

    /*@OnClick({R.id.sign_male_image,R.id.sign_female_image,R.id.sign_up_button})*/
    DatePickerDialog.OnDateSetListener date;
    SessionParam sessionParam;
    String userName="";
    Integer  mYear, mMonth, mDay;
    private int TakePicture = 1, SELECT_FILE = 2, RESULT_OK = -1;
    final private int REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS = 124;
    MarshMallowPermission marshMallowPermission;
    private Bitmap bitmap = null;
    String imgBase64 = "";
    File sourceFile;
    Utility utility;
    static final int REQUEST_TAKE_PHOTO = 1;
    static final int REQUEST_GALLERY_PHOTO = 2;
    File mPhotoFile;
    FileCompressor mCompressor;
    int sensitivePostsAllowed=2;

    String name="";
    String countryId="";
    String password="";
    String dob="";
    String bio="";
    Integer gender=1;
    Integer type;
    String profileImage="";
    String deviceId="";
    String myToken="";
    String userValue="";
    final int sdk = android.os.Build.VERSION.SDK_INT;
    private BaseRequest baseRequest;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try {
            requestWindowFeature(Window.FEATURE_NO_TITLE);
            this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        }catch (Exception e){
            e.printStackTrace();
        }
        setContentView(R.layout.activity_sign_up);
        ButterKnife.bind(this);
         sessionParam=new SessionParam(this);
        mCompressor = new FileCompressor(this);

        utility = new Utility();
        utility.getProgerss(this);
        selectMaleFemale();

        Bundle bundle = getIntent().getExtras();
        userName= bundle.getString("userName");
        userValue= bundle.getString("userValue");
        countryId= bundle.getString("countryId");
        password= bundle.getString("password");
        type= bundle.getInt("type");

        permission();
        userNameText.setText(userName);
        marshMallowPermission = new MarshMallowPermission(this);
        if (!marshMallowPermission.checkPermissionForCamera() && !marshMallowPermission.checkPermissionForExternalStorage()) {
            marshMallowPermission.requestPermissionForCamera();
            marshMallowPermission.requestPermissionForExternalStorage();
        } else if (!marshMallowPermission.checkPermissionForCamera()) {
            marshMallowPermission.requestPermissionForCamera();
        } else if (!marshMallowPermission.checkPermissionForExternalStorage()) {
            marshMallowPermission.requestPermissionForExternalStorage();
        }

       // userName=sessionParam.userName;

        fullNameEdit.addTextChangedListener(new MyTextWatcher(fullNameEdit));
        viewModel = ViewModelProviders.of(this, new SignUpModelFactory(this.getApplication())).get(SignUpViewModel.class);

        //set onClick
        dateOfBirthEdit.setOnClickListener(this);
        bioEditText.setOnClickListener(this);
        maleIconImage.setOnClickListener(this);
        femaleIconImage.setOnClickListener(this);
        otherIconImage.setOnClickListener(this);
        selectImageGalary.setOnClickListener(this);
        signUpButton.setOnClickListener(this);
        checkboxValue.setOnClickListener(this);
        termsConditionTxt.setOnClickListener(this);
        privacyPolicyTxt.setOnClickListener(this);
       // termsConditionTxt.setPaintFlags(termsConditionTxt.getPaintFlags() |   Paint.UNDERLINE_TEXT_FLAG);
        //privacyPolicyTxt.setPaintFlags(privacyPolicyTxt.getPaintFlags() |   Paint.UNDERLINE_TEXT_FLAG);


        selectMaleFemale();
        TelephonyManager TelephonyMgr = (TelephonyManager) getSystemService(TELEPHONY_SERVICE);

        //drawerLayout.closeDrawer(GravityCompat.START);

           deviceId= utility.getAndroidID(this);

    }

    /*request user for certain permission*/
    private void permission() {
        //datafinish = true;
        List<String> permissionsNeeded = new ArrayList<String>();
        final List<String> permissionsList = new ArrayList<String>();
        if (!addPermission(permissionsList, Manifest.permission.ACCESS_FINE_LOCATION))
            permissionsNeeded.add("GPS");
        if (!addPermission(permissionsList, Manifest.permission.ACCESS_NETWORK_STATE))
            permissionsNeeded.add("Network state");
        if (!addPermission(permissionsList, Manifest.permission.ACCESS_COARSE_LOCATION))
            permissionsNeeded.add("Location");
        if (!addPermission(permissionsList, Manifest.permission.INTERNET))
            permissionsNeeded.add("Internet");
        if (!addPermission(permissionsList, Manifest.permission.READ_PHONE_STATE))
            permissionsNeeded.add("phone status");
        if (!addPermission(permissionsList, Manifest.permission.CAMERA))
            permissionsNeeded.add("Camera");
        if (!addPermission(permissionsList, Manifest.permission.WRITE_EXTERNAL_STORAGE))
            permissionsNeeded.add("Write storage");
        if (permissionsList.size() > 0) {
            if (permissionsNeeded.size() > 0) {
                // Need Rationale
                String message = "You need to grant access to " + permissionsNeeded.get(0);
                for (int i = 1; i < permissionsNeeded.size(); i++)
                    message = message + ", " + permissionsNeeded.get(i);
                /*showMessageOKCancel(message, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            requestPermissions(permissionsList.toArray(new String[permissionsList.size()]), REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS);
                        }
                    }
                });*/
                return;
            }
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermissions(permissionsList.toArray(new String[permissionsList.size()]), REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS);
            }
            return;
        }
        //init();
    }

    private boolean addPermission(List<String> permissionsList, String permission) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(permission) != PackageManager.PERMISSION_GRANTED) {
                permissionsList.add(permission);
                //Check for Rationale Optiong
                if (!shouldShowRequestPermissionRationale(permission))
                    return false;
            }
        }
        return true;
    }

    private void selectMaleFemale() {
        if (sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
            maleIconImage.setImageResource( R.drawable.male);
            femaleIconImage.setImageResource( R.drawable.femalegray);
            otherIconImage.setImageResource( R.drawable.other_gray);
        } else {
            maleIconImage.setImageResource( R.drawable.male);
            femaleIconImage.setImageResource( R.drawable.femalegray);
            otherIconImage.setImageResource( R.drawable.other_gray);
        }
    }

    //call on text change for validation
    private class MyTextWatcher implements TextWatcher {

        private View view;

        private MyTextWatcher(View view) {
            this.view = view;
        }

        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void afterTextChanged(Editable editable) {
            switch (view.getId()) {
                case R.id.sign_up_full_name_et:
                    Validations.isValidEmail(fullNameEdit.getText().toString().trim());
                    break;

            }
        }
    }

    private void requestFocus(View view) {
        if (view.requestFocus()) {
            if (this!=null)
                this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }

    private void SelectDatefromCircle() {
        final Calendar c = Calendar.getInstance();
        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH);

        android.app.DatePickerDialog datePickerDialog = new android.app.DatePickerDialog(this,
                new android.app.DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {

                        //dateOfBirthEdit.setText(String.format("%04d-%02d-%02d", year, (monthOfYear + 1), dayOfMonth));

                    }
                }, mYear, mMonth, mDay);
        datePickerDialog.show();

}

    private void updateLabel() {
        String myFormat = "MM/dd/yy"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);

       // dateOfBirthEdit.setText(sdf.format(myCalendar.getTime()));
    }

    private void SelectDate() {
        // TODO Auto-generated method stub
        new DatePickerDialog(this, date, myCalendar
                .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                myCalendar.get(Calendar.DAY_OF_MONTH)).show();
    }


    private void showDatePickerDialog(){
        final Calendar c = Calendar.getInstance();
       // c.add(Calendar.YEAR, -18);
        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH);


        DatePickerFragmentDialog datePickerFragmentDialog=DatePickerFragmentDialog.newInstance(new DatePickerFragmentDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePickerFragmentDialog view, int year, int monthOfYear, int dayOfMonth) {

                Calendar userAge = new GregorianCalendar(year,monthOfYear,dayOfMonth);
                Calendar minAdultAge = new GregorianCalendar();
                minAdultAge.add(Calendar.YEAR, -18);
                if (minAdultAge.before(userAge)) {
                    utility.showToast(SignUpActivity.this,"Age Must me 18+");
                    dateOfBirthEdit.setText("");
                }else {
                    dateOfBirthEdit.setText(dayOfMonth + "/" + (monthOfYear + 1) + "/" + year);
                }


            }
        },mYear, mMonth, mDay);
        Calendar maxDate = Calendar.getInstance();
        maxDate.set(Calendar.DAY_OF_MONTH, mDay);
        maxDate.set(Calendar.MONTH, mMonth);
        maxDate.set(Calendar.YEAR, mYear-18);

        datePickerFragmentDialog.show(getSupportFragmentManager(),null);
        datePickerFragmentDialog.setMaxDate(maxDate.getTimeInMillis());
        datePickerFragmentDialog.setYearRange(1900,mYear-18);
        datePickerFragmentDialog.setCancelColor(getResources().getColor(R.color.colorPrimaryDark));
        datePickerFragmentDialog.setOkColor(getResources().getColor(R.color.colorPrimary));
        datePickerFragmentDialog.setAccentColor(getResources().getColor(R.color.colorAccent));
        datePickerFragmentDialog.setOkText(getResources().getString(R.string.ok_dob));
        datePickerFragmentDialog.setCancelText(getResources().getString(R.string.cancel_dob));
    }








    /*@Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        String imageFileName = System.currentTimeMillis() + ".png";
        if (resultCode == RESULT_OK) {
            if (requestCode == REQUEST_TAKE_PHOTO) {

                try {
                    mPhotoFile = mCompressor.compressToFile(mPhotoFile);
                    bitmap=mCompressor.compressToBitmap(mPhotoFile);
                    profileImage=utility.BitMapToString(bitmap);


                    //imgBase64 = Utility.image_to_Base64(this, utility.getPath(data.getData(), this));

                    //profileImage=imgBase64;
                } catch (IOException e) {
                    e.printStackTrace();
                }
                Glide.with(SignUpActivity.this).load(mPhotoFile).apply(new RequestOptions().placeholder(R.drawable.profile)).into(profilePics);
            } else if (requestCode == REQUEST_GALLERY_PHOTO) {
                Uri selectedImage = data.getData();
                try {
                    mPhotoFile = mCompressor.compressToFile(new File(getRealPathFromUri(selectedImage)));
                    bitmap=mCompressor.compressToBitmap(new File(getRealPathFromUri(selectedImage)));
                    //imgBase64 = Utility.image_to_Base64(this, utility.getPath(data.getData(), this));
                    //profileImage=imgBase64;
                    profileImage=utility.BitMapToString(bitmap);


                } catch (IOException e) {
                    e.printStackTrace();
                }
                Glide.with(SignUpActivity.this).load(mPhotoFile).apply(new RequestOptions().placeholder(R.drawable.profile)).into(profilePics);

                *//*try {
                    //bitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), data.getData());
                    sourceFile = new File(utility.getPath(data.getData(), this));
                    bitmap = new Compressor(this).compressToBitmap(sourceFile);
                    imgBase64 = Utility.image_to_Base64(this, utility.getPath(data.getData(), this));
                } catch (IOException e) {
                    e.printStackTrace();
                }*//*
            }
           *//* profileImage = Utility.convert(bitmap);
            profileImage=utility.BitMapToString(bitmap);*//*
        }
        *//*if (resultCode == RESULT_OK) {
            if (requestCode == TakePicture) {
                bitmap = (Bitmap) data.getExtras().get("data");
                ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
                //iv_show_photo.setImageBitmap(bitmap);
                imgBase64 = Utility.image_to_Base64(this, utility.getPath(utility.getImageUri(this, bitmap), this));
            }
            if (requestCode == SELECT_FILE) {
                try {
                    //bitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), data.getData());
                    sourceFile = new File(utility.getPath(data.getData(), this));
                    bitmap = new Compressor(this).compressToBitmap(sourceFile);
                    imgBase64 = Utility.image_to_Base64(this, utility.getPath(data.getData(), this));
                } catch (IOException e) {
                    e.printStackTrace();
                }
              profilePics.setImageBitmap(bitmap);
                profileImage = Utility.convert(bitmap);

            }
            //bitmap = BitmapFactory.decodeStream(getContentResolver().openInputStream(file_uri));

           *//**//* bitmap = Utility.decodeSampledBitmapFromResource(data.getExtras().get("data").toString(), 100, 100);
                profilePics.setImageBitmap(bitmap);*//**//*
        }*//*
    }

*/


    @Optional
    @OnClick({R.id.sign_up_full_name_et,R.id.sign_date_of_birth_et,R.id.sign_bio_et,R.id.sign_other_image,
                R.id.sign_male_image,R.id.sign_female_image,R.id.sign_up_button,R.id.sign_up_image})
    @Override
    public void onClick(View view) {
        switch (view.getId()) {
           /* case R.id.sign_up_full_name_et:
                break;*/
            case R.id.sign_date_of_birth_et:
                // SelectDate();
                // SelectDatefromCircle();
                showDatePickerDialog();
                break;
            case R.id.sign_male_image:
                selectMale();
                break;
            case R.id.sign_female_image:
                selectFemale();
                break;
            case R.id.sign_other_image:
                selectOther();
                break;
            case R.id.sign_up_image:
                selectImage();
                //selectImageFromGelary();
                break;
            case R.id.sign_up_check_box:
                if (checkboxValue.isChecked()){
                    sensitivePostsAllowed=1;
                }else{
                    sensitivePostsAllowed=2;
                }
                break;
            case R.id.sign_up_privacy_policy:
                privacyApi();
                break;
            case R.id.sign_up_terms_condition:
                termsAndCondition();
                break;
            case R.id.sign_up_button:
                name=fullNameEdit.getText().toString().trim();
                bio=bioEditText.getText().toString().trim();
                String dateOfBirth=dateOfBirthEdit.getText().toString().trim();
                dob=utility.changeDateDMYtoYMD(dateOfBirth);

               /* password=sessionParam.password;
                countryId=sessionParam.countryId;
                type=sessionParam.signupType;*/
                /*Please enter Username.
                Please select Date Of Birth.
        Please enter Bio.*/

                if (Validations.isEmptyString(name)){
                    utility.showToast(this,"Please enter Username.");
                    return;
                }
                if (Validations.isEmptyString(bio)){
                    utility.showToast(this,"Please enter Bio.");
                    return;
                }
                if (Validations.isEmptyString(dob)){
                    utility.showToast(this,"Please select Date Of Birth.");
                    return;
                }
                /*if (Validations.isEmptyString(profileImage)){
                    utility.showToast(this,"please Enter profileImage");
                    return;
                }*/


                loginApi(name,userValue,password,dob
                        ,bio,gender,type,countryId,imgBase64,deviceId,myToken);
                /*getSignupOldMethod(name,userValue,password,dob
                        ,bio,gender,type,countryId,profileImage,deviceId,myToken);*/
                break;
        }

    }

    private void getSignupOldMethod(String name, String userValue, String password, String dob, String bio, Integer gender, Integer type, String countryId, String profileImage, String deviceId, String myToken) {
       utility.loading();
        baseRequest = new BaseRequest(this);
        baseRequest.setBaseRequestListner(new RequestReciever() {
            @Override
            public void onSuccess(int requestCode, String Json, Object object) {
                try {
                    utility.dismiss();
                    sessionParam = new SessionParam((JSONObject) object);
                    sessionParam.persist(mContext);
                    finishAllActivities();
                    sessionParam.saveToken(SignUpActivity.this, "y");
                    String token=sessionParam.token;
                    startActivity(new Intent(mContext, DefalutActivity.class));
                    //apiTellSidLogin();



                }catch (Exception e){
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int requestCode, String errorCode, String message) {
                //errorLayout.showError(message);
                utility.dismiss();
                utility.showToast(SignUpActivity.this, message);
            }

            @Override
            public void onNetworkFailure(int requestCode, String message) {
                //errorLayout.showError(message);
                utility.dismiss();
                utility.showToast(SignUpActivity.this, message);
            }
        });
        /*
        *
"name":"shweta",
"userName":"45343785372",
"password":"123",
"dob":"1995-07-22",
"bio":"test",
"gender":2,
"type":2,
"countryId":"54",
"profileImage":"E1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENDIChXaW5kb3dzKSIgeG1wTU06SW5zdGFuY2VJRD0ieG1wLmlpZDo3MEE2MDc5QTY0QUUxMUU4OUM4MkEwNTkxMDhFRERBRiIgeG1wTU06RG9jdW1lbnRJRD0ieG1wLmRpZDo3MEE2MDc5QjY0QUUxMUU4OUM4MkEwNTkxMDhFRERBRiI+IDx4bXBNTTpEZXJpdmVkRnJvbSBzdFJlZjppbnN0YW5jZUlEPSJ4bXAuaWlkOjcwQTYwNzk4NjRBRTExRTg5QzgyQTA1OTEwOEVEREFGIiBzdFJlZjpkb2N1bWVudElEPSJ4bXAuZGlkOjcwQTYwNzk5NjRBRTExRTg5QzgyQTA1OTEwOEVEREFGIi8+IDwvcmRmOkRlc2NyaXB0aW9uPiA8L3JkZjpSREY+IDwveDp4bXBtZXRhPiA8P3hwYWNrZXQgZW5kPSJyIj8+zbdQTQAADnFJREFUeNrUWwmQFcUZ7nnHsizLtesCS6mAgoDgiUeCYoAopDyiScQjJTFqvEpjNBoSrcQzRkolnpSo8UiVZkEtSzQWgopnIhFXxAMNFK4iosu1CMiyx3uT73/9ta/fMD1v3rJQm6731/TMdPf08f1n9/OWKqWalU4JULnOlrUqdT6uF3hKLcX76cOVWrYHy30BWg2q4P02pdJppc5D2Qtx+yGuN7UrtXy0VSYsfc52KkGrQD1BffHLKDXGV2oybkeCBoCyoC9B7+H5K6AP0NemDPuMb3soMNzXt771Cbnfyk/lkkcyCW1U4TLQy2WVXSyBhj4agfY26QZ6YG6G4dl40MEosLfubq79lUmlFm9RamGbUp9WK9WC8as+oGtBc9R3jee+pXb8lvR5eeC5PYaRLOPb/eP0bVJdJ2Ep1LDC6VUruH5xkj1Wkz4poX5k8mywJwXlWFQA/W1k9/esHgOIp+yn1NxkAOzoRQV69h/QaFU4yskHKbWgWwywC4N9q1QNvn0VaJpfrNP6uzNQ7jZQo6/7/jGej3CUr/A4zJC2X8WzHwQfCrMCvMOB6ATG8lvkb0vG6BdoPcqfhev8FOfuSN2epHtBlziq14K+DnkuMmado85N5KeukoYS3HYC76uNMevXgNYGnvXtLIZO2DdY9N5turP729JPFq1JqWfAYlMM+1LM9ATolgeBzjS/QamTigHX18Ad3wuD7BYD6KYO+nclOv816JgKPZCzfTdjjGjBtcUSnex/Jej7Ye33UOpUMLi3QalnBeipmP3Ct/bAN19A9g4A3OuNzKX5IreBJoJm8X496FzQBNAGR7Oy0MeDfg5q5bOZoB+D7lddK4n8OlRpDS9pu0NbuZIwxcGgMwPaQXWqZAdYeoP+LUB3FRaw9MPioNwjYL/ukMKLsMAHusrLSLHYZ0AvzUm6JftxkOwLOjoAXzPfIMzIaj+nIIwlVgD2y/H+LsPdFocfAno32CbKboPtVL0Z2gxzU5foYN/w3Ysg1u5fS7Q25V+NBf2LKvqAvOAvKpi+odUnDLpIdd0kFulm5vuw3/aSnERNVmeVs5OMcQvzvaz8zkl2SvBqMQOigK6IIrDew9CpVwNgC6OALqm77uXsZeDUtvDVE7W1YKe4FYS2XwVA0wDXzV64FjhB7Po0TTU//+4EB6pmwEFIglnvTzi+mdCM/hiy70egcxbmq6cY90+DBhXatiG8V3SotrLtyimqf7IEc6ndhkTY/p2exAnria+v8TWnFZWiST2Sv+DyvTjlu2l7/B8QYad+KN4r6CMSgDAzwqYXxXA2VngI2tkbdJpDCkgaAi0zCqZHnUPnHQdG6J2hTqS5IYx+QZj22lOpx/cCI7ZpqRKU+mLiTILEPgr5qWjrEDjoY7OF5tF3hG9MER/nMN1uGHC9DoDd6+Jgj+pfxrLrt+7OsaZatTQs21WjppkhptKTyNf42k6V1B+MMyUZ4jR62nYb5dNh8/QMfQFAzU/rqEX/gLkgAzgXIJwGblibyFlbO6RhaGexAXxal9krROxsBOBXAOhHJxwiC0b0+xhEtlwDPztQqbegpycB9EOTHI6XH/+SNPVw2+4FXA9tRebk03YCq7nENnqxDZ+CZnMEKOO4WykaCGVWH+2IVFhqt8r0oYnTxr5sK9WMOXt3zDw1woFpgh80PuEYIZ5d7AciEwT8ZgD7d2EzCO1xcSMmAbN4u0NUHN9Gd/8gPeM/DItnoX4d2snCW8ymHcyLfj+X1tqih1kl9O1F5O/D81lCKRLyS7q50ZAtIawWt9wRoNkE95eM6q4lMMRvGRyjDVFEj9PWXsUAnOQXgsawzGCaI89JSJjOt+eYMkn7so2vLCHzLOgNmoJDIqAzgt/YyP58pZc8ZwrVxpbsoOcD3u+uTMus/KSIcgtD7O6cOPC0+dtC7vb5vgzPWzEr7cjPA90aMuvnAy03AahZgu8sB6M9mNLXT3z37B8uvgbebwKilm7QDvhLqLcamnI7fBV/PUMn/Rk7nMHVigBDzGBPpAMrjH4F718HvcXorlhQvwRdRjqLYHaFM//IvFidLzIiJGHFqaB3GG1ppqNpTM63i/Rf1uu/dOWGsr/9eV+hdrQuspTqsxiJkim9gyaQAPxX7IfQqAC2wm2jeqWqfB3HTexioE9L6Li4WZh6X4eZgqmBnNwaRINnAT/C0Ev5WqL1C0FLP8z4OgCzMqnHXB4M84nTDCndTt35AAB8folG4xw0Ogui9A2Inkw1V/RyikCm8aBXqIr/SkB2j2izlUrpegqoYygR7fQw6ByZV9DPlLWRZklIiYLeafwY0EuBMiKdr2L+WAodv1Dx5RjhT9QclZTup9nCR+nNsw2OaEyKUSiR9IeTeZKBEGMVtZJZH5m+uwN9kTqi5W/h/FRR2kdK9o10AleoXWS7o+2rPT2RNnhd6uczzwrF+RbIY8bgpe5DoKuDXhFmfRxm5OmVcCpRrjwExNMhotpli7Nai6tLP9e+w9gSAH866p0OY7Qe6D0Fq7B6DcWQiJ6VO9rE13fCFB9IoL/HsGarwzG8i9MpEnI+JWoL3w+3gH4ApXoY011L3Jj5/dbxvTjmWFOMWPqd7HfYeKYzUHIy6GLlMGFt1adoB+0b4R3vjFs+zdOdCgK2NWb9XJ02UnuRWczq8o86ipxoSbUwRnkiy+9UavOjFcCfgPvr0I9tpUh4oGEMxFIDxE9VhkZwzY7FRHqNpFSqiaBqqv7tjs+ZRT4nxrzeS7MkQcYw6Tpeb3UAXQXKNloS1isNErFCjAabM4q0Z8Z+WrF+2KaLbMoMojPSWRL9Mn9HiW6yaxzVBuNlOhjCK8WozWgB2hjU4dCl+8BGqgYQjw6ZFdFsq0Rvimf8MRGB+1aUvRFt1gL0xyb07mWsvqBOCtL9qRqqzBCDewu/20Sb1EXimH3q4PUeZF4pszRG19otAJ3Ca3eaPpLui9GGyINHQzDUmclYGd8UKbfKcoC7lRL838hDTh+W4uU62PdctPVIBBZE5R4ZUm8IQFIJaklaM+nlr/Lop1bI3IwjJdv0KY3RDPK324zGVdkTz2/wA9/l/sFdeJ9hWDKHnG2cvbReXYkEvexpuhKPBuL+RE87T0e4Fh1tT4A47gtvrikbLmxSKt6WuOtojvFNhE9fKBLGM2A3En2i1Ybhx3Uxl1h8ht93gZi+mdaKiDkKBbviOo+iozOkgx0Qz//vKjoo+7LKn6EIljsCNM8CeK6jXEWJLDzp+K7YnZuSegbmqgDYhft9x0EsX0elckjwNXJkwy0JwGdTlr7NaGC2oC8NoHtwc09WH2Kb6fPsUMhqDMgWnBbo1JS2FntSzDqNdGDN7m83y7SMexalWXWtlChmxri2dZsI+FfCpG+RNAWL/pQXEjoMkQyhIh9lH8IKjkB+c0BEeZ7bhtuEd59Z8bkG+oP7xtBCSwDyz9tVwQ7rJUl9xHmrXwgsiTLIUZecFuirkbJujT5CMcWxCu27MNRlq/ka3nsxwZGxzCmjHcoifAM77aP+z1LUGjT7+jRefQntCQieCtO/xiQxhPtGT8dww1Ktr09TTvD0Dl4PahmJDZ8appcBuNmHQjKNYfvNOub+UJxV93Us97udTy8fZRhNb9/QGErPGwUUqOABGQmI1HKg5jcRccP1vYsZlB1PGyyn1PjyrTFou8pv6orpYo7WxtXmU0OiKx1w63Z58uKA3aiqcSreCbvJvmV62PGhMsbYKi3CvQ9JeGGEzuzv6TivSOytnnbQzoyI2Ny8hSLKzw/s2ZhG39O26C7T9esiVuIavGtG2ZfhPb72GfIQib92lF0M769pQaHn73dgwV112q3w5fSYbQ1juLK71cbfSmhD6h4TMYZEAALB5UoUYRS/gwwRLGuOO4jGS8TRriLhxxmb1gG2cV7E6cWEJTFtwvMG0Hk7y7qwxa7ArK2WQzPm7z7cBf3YLx5dejOj/3CRq9eaD3M2g87MuG2CBDXf0UW0xy8kbj/HChuofGw7W4KNnFXusO3dvEo4f0qRds7gNC0NhP5u4fVHdDy9CId4cSAyE6Zs7WhRUP4Z87k6ItoT1lZYanGUFaDvT2dcTPLRcU1JaeQnoGdC3kkk4k1Xj/tQjMgx10EW7U1vcqRSDwM0F3VUn6Hen0F3muME6cJwpQDkgSJNTLfO6+TEQJ88zYbEvmYn/j0wtVapT+TA/Ly8TS3DNtvsEl+fzEVJRURh5DjPBCscJ/sFh7K+MbnMbvQTSm8a7UnJXcbrYKXNtTrOi5xPsQ92bWWbRrqLOTrUsuMryUiNvJ9rBQWkXv9ASLWB+T+wn5WW9DX+mvRzP9Y1lu5BKr8fojhX+4VEWtKE0WRLpk5U+ZPUQ9mHQ6hUK7x6qwd+wMjxA3EsT4f35vg69CdJ/iv6rq2TEtZ1O0cR5SEKkJborHTqQS9/0KhYWu9rKSWhwNzIeVgs931rDBK3b3CZP2DIKhMpSZJz7bROi8HJ5TqMWhuTAevR5oVARL3Y6dfmHRmRwC5zZ4AK7A0wRf0t7wZVuAMrAH4sYGIEk3TlIuX+Z9Qo8uZejvdrCUgB4GvW8y8INAOXowJCcIEFzCoDQDtSpvTeS3PM+RF/6gNHWcOgsv/wDoX18JLAzlOKnkhSiKE6LOiibMAAC4K9H1kswnnLxb9oZyfxvaGof4anGapvoLg0+c+EZrj3MIIWYyKlHZEf6W+7Ph4QPGQp39rsWQe+MmTOQYEY3Qq9KnLYbDjKnOzpfwoNU/mzG+IML5P/nYJh5qFMA8aV6UVkTMw3N1Ars4JzJCYqssihslP0mzKBJSrjYjeGWI1DuNBjqU1Wsn0B5+oYdnCazvjpBK20+Tq1Rj3734sS3WxNyF5c8DBWLYcvEvh5VfhHF9E2h5Gk/Zk0SQ4PmHYJzssHgfkpp2zyLBPPzOVbvA7gkgrMlv9PgAEAachIu3P65GIAAAAASUVORK5CYII=",
"deviceId":"7C4A9817-775F-43EC-BC84-0BDBF3F6A3E7",
"fcmToken":""


}*/
       // name,userValue,password,dob
         //       ,bio,gender,type,countryId,profileImage,deviceId,myToken
        JsonObject object = Functions.getClient().getJsonMapObject(
                "name", name,
                "userName", userValue,
                "password", password,
                "dob", dob,
                "bio", bio,
                "gender", gender+"",
                "type", type+"",
                "countryId", countryId,
                "profileImage", profileImage,
                "deviceId", deviceId,
                "fcmToken", myToken
                //JsonObject object = Functions.getClient().getJsonMapObject("orgId", "9"
        );
        baseRequest.callAPIPost(1, object, getString(R.string.api_sign_up));
    }


    private void getSignup(String name,String userName, String password,
                           String dob,String bio,Integer gender,Integer type,
                           String countryId,String profileImage,String deviceId, String myToken) {
        utility.loading();
        viewModel.getUserSignUp(name,userName,password,dob
                ,bio,gender,type,countryId,profileImage,deviceId,myToken).observe(this, new Observer<JSONObject>() {
            @Override
            public void onChanged(JSONObject repos) {

                utility.dismiss();
                //{"code":200,"status":true,"service_name":"verify-username","message":"Verification code send to email.",
                // "data":{"randomNumber":2125,"email":"ritesh.sonar111@gmail.com"}}
                try {
                    Log.d("codeJson",repos.toString());
                    JSONObject jsonObject = (JSONObject) repos;
                    String message = jsonObject.optString("message");
                    String code = jsonObject.optString("code");
                    Boolean status = jsonObject.optBoolean("status");

                    if (status) {
                        utility.showToast(SignUpActivity.this, message);
                        //Toast.makeText(Verification_Activity.this, message, Toast.LENGTH_SHORT).show();
                        JSONObject object = new JSONObject();
                        object = repos.getJSONObject("data");
                        sessionParam = new SessionParam((JSONObject) object);
                        sessionParam.persist(SignUpActivity.this);
                        sessionParam.saveToken(SignUpActivity.this, "y");

                        Intent intent = new Intent(SignUpActivity.this, HomeActivity.class);
                        startActivity(intent);
                        finish();
                    }else{
                        utility.showToast(SignUpActivity.this, message);
                    }


                }catch (Exception e){
                    e.printStackTrace();
                }

                        /*if (repos.getMessage()!=null) {
                            Integer code = repos.getCode();
                            Toast.makeText(Verification_Activity.this, repos.getMessage(), Toast.LENGTH_SHORT).show();
                            Log.d("getverificationCode", "code " + code);
                        }*/
            }
        });

    }

    private void privacyApi() {
        try{
            Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://freshersapp.chetaru.co.uk/privacyPolicy"));
            startActivity(browserIntent);
        }catch (Exception e){
            e.printStackTrace();
        }

    }



    private void termsAndCondition() {
        try{
            Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://freshersapp.chetaru.co.uk/termsOfServices"));
            startActivity(browserIntent);
        }catch (Exception e){
            e.printStackTrace();
        }
    }
    /*is api call used for calling login API
     */
    public void loginApi(String name,String userName, String password,
                         String dob,String bio,Integer gender,Integer type,
                         String countryId,String profileImage,String deviceId, String myToken) {
        baseRequest = new BaseRequest(this);
        baseRequest.setBaseRequestListner(new RequestReciever() {
            @Override
            public void onSuccess(int requestCode, String Json, Object object) {
                try {
                    sessionParam = new SessionParam((JSONObject) object);
                    sessionParam.persist(mContext);
                    finishAllActivities();
                    sessionParam.saveToken(SignUpActivity.this, "y");
                    String token=sessionParam.token;
                    Log.d("signUp",token);
                    Intent intent=new Intent(SignUpActivity.this,HomeActivity.class);
                    startActivity(intent);
                    finish();
                    BaseActivity.finishAllActivitiesStatic();
                    //startActivity(new Intent(mContext, HomeActivity.class));

                    //apiTellSidLogin();


                }catch (Exception e){
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int requestCode, String errorCode, String message) {
                //errorLayout.showError(message);
                //utility.showToast(SignUpActivity.this,message);
            }

            @Override
            public void onNetworkFailure(int requestCode, String message) {
                // errorLayout.showError(message);
                utility.showToast(SignUpActivity.this,message);
            }
        });
        /*
        * name,userName,password,dob
                ,bio,gender,type,countryId,profileImage,deviceId,myToken*/
        JsonObject main_object = new JsonObject();
        main_object.addProperty("name",name );
        main_object.addProperty("userName",userName );
        main_object.addProperty("password",password );
        main_object.addProperty("dob",dob );
        main_object.addProperty("bio",bio );
        main_object.addProperty("gender",gender );
        main_object.addProperty("type",type );
        main_object.addProperty("countryId",countryId );
        main_object.addProperty("sensitivePostsAllowed",sensitivePostsAllowed );
        main_object.addProperty("profileImage",profileImage );
        main_object.addProperty("deviceId",deviceId );
        main_object.addProperty("fcmToken",sessionParam.fcm_token );

        /*JsonObject object = Functions.getClient().getJsonMapObject(
                "name", name,
                "userName", userName,
                "password", password,
                "dob", dob ,
                "bio", bio ,
                "gender", gender+"",
                "type", type+"",
                "countryId", countryId,
                "sensitivePostsAllowed", 1+"",
                "profileImage", profileImage,
                "fcmToken", sessionParam.fcm_token
        );*/
        baseRequest.callAPIPost(1, main_object, getString(R.string.api_usersignup));
    }



    private void selectOther(){
        gender=3;
        if (sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {

            otherIconImage.setImageResource( R.drawable.other);
            maleIconImage.setImageResource( R.drawable.malegray);
            femaleIconImage.setImageResource( R.drawable.femalegray);
        } else {
            otherIconImage.setImageResource( R.drawable.other);
            maleIconImage.setImageResource( R.drawable.malegray);
            femaleIconImage.setImageResource( R.drawable.femalegray);
        }
    }

    private void selectMale() {
        gender=1;
        if (sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
            otherIconImage.setImageResource( R.drawable.other_gray);
            maleIconImage.setImageResource( R.drawable.male);
            femaleIconImage.setImageResource( R.drawable.femalegray);
        } else {
            otherIconImage.setImageResource( R.drawable.other_gray);
            maleIconImage.setImageResource( R.drawable.male);
            femaleIconImage.setImageResource( R.drawable.femalegray);
        }
    }
    private void selectFemale() {
        gender=2;
        if (sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
            otherIconImage.setImageResource( R.drawable.other_gray);
            maleIconImage.setImageResource( R.drawable.malegray);
            femaleIconImage.setImageResource( R.drawable.female);
        } else {
            otherIconImage.setImageResource( R.drawable.other_gray);
            maleIconImage.setImageResource( R.drawable.malegray);
            femaleIconImage.setImageResource( R.drawable.female);
        }
    }

    ///Select Image from Galery
    /**
     * Alert dialog for capture or select from galley
     */
    private void selectImageFromGelary() {
        final CharSequence[] items = {"Take Photo", "Choose from Library",
                "Cancel"};
        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(SignUpActivity.this);
        builder.setItems(items, (dialog, item) -> {
            if (items[item].equals("Take Photo")) {
                requestStoragePermission(true);
            } else if (items[item].equals("Choose from Library")) {
                requestStoragePermission(false);
            } else if (items[item].equals("Cancel")) {
                dialog.dismiss();
            }
        });
        builder.show();
    }

    /**
     * Capture image from camera
     */
    private void dispatchTakePictureIntent() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            // Create the File where the photo should go
            File photoFile = null;
            try {
                photoFile = createImageFile();
            } catch (IOException ex) {
                ex.printStackTrace();
                // Error occurred while creating the File
            }
            if (photoFile != null) {
              Uri   photoURI=FileProvider.getUriForFile(Objects.requireNonNull(getApplicationContext()),
                        "com.chetaru.FreshersApp"+ ".provider", photoFile);
               /* Uri photoURI = FileProvider.getUriForFile(this,
                        BuildConfig.APPLICATION_ID + ".provider",
                        photoFile);*/

                mPhotoFile = photoFile;
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                startActivityForResult(takePictureIntent, REQUEST_TAKE_PHOTO);

            }
        }
    }


    /**
     * Select image fro gallery
     */
    private void dispatchGalleryIntent() {
        Intent pickPhoto = new Intent(Intent.ACTION_PICK,
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        pickPhoto.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        startActivityForResult(pickPhoto, REQUEST_GALLERY_PHOTO);
    }

    /**
     * Requesting multiple permissions (storage and camera) at once
     * This uses multiple permission model from dexter
     * On permanent denial opens settings dialog
     */
    private void requestStoragePermission(boolean isCamera) {
        Dexter.withActivity(this).withPermissions(Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA)
                .withListener(new MultiplePermissionsListener() {
                    @Override
                    public void onPermissionsChecked(MultiplePermissionsReport report) {
                        // check if all permissions are granted
                        if (report.areAllPermissionsGranted()) {
                            if (isCamera) {
                                dispatchTakePictureIntent();
                            } else {
                                dispatchGalleryIntent();
                            }
                        }
                        // check for permanent denial of any permission
                        if (report.isAnyPermissionPermanentlyDenied()) {
                            // show alert dialog navigating to Settings
                            showSettingsDialog();
                        }
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(List<com.karumi.dexter.listener.PermissionRequest> permissions, PermissionToken token) {
                        token.continuePermissionRequest();
                    }


                }).withErrorListener(error -> Toast.makeText(getApplicationContext(), "Error occurred! ", Toast.LENGTH_SHORT).show())
                .onSameThread()
                .check();
    }


    /**
     * Showing Alert Dialog with Settings option
     * Navigates user to app settings
     * NOTE: Keep proper title and message depending on your app
     */
    private void showSettingsDialog() {
        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(this);
        builder.setTitle("Need Permissions");
        builder.setMessage("This app needs permission to use this feature. You can grant them in app settings.");
        builder.setPositiveButton("GOTO SETTINGS", (dialog, which) -> {
            dialog.cancel();
            openSettings();
        });
        builder.setNegativeButton("Cancel", (dialog, which) -> dialog.cancel());
        builder.show();

    }

    // navigating user to app settings
    private void openSettings() {
        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        Uri uri = Uri.fromParts("package", getPackageName(), null);
        intent.setData(uri);
        startActivityForResult(intent, 101);
    }

    /**
     * Create file with current timestamp name
     *
     * @return
     * @throws IOException
     */
    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMddHHmmss").format(new Date());
        String mFileName = "JPEG_" + timeStamp + "_";
        File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File mFile = File.createTempFile(mFileName, ".jpg", storageDir);
        return mFile;
    }

    /**
     * Get real file path from URI
     *
     * @param contentUri
     * @return
     */
    public String getRealPathFromUri(Uri contentUri) {
        Cursor cursor = null;
        try {
            String[] proj = {MediaStore.Images.Media.DATA};
            cursor = getContentResolver().query(contentUri, proj, null, null, null);
            assert cursor != null;
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            return cursor.getString(column_index);
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
    }


    private void selectImage() {
        //here user will get options to choose image
        final CharSequence[] items = {"Take Photo", "Choose from Library", "Cancel"};
        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        builder.setTitle("Add Photo!");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (items[item].equals("Take Photo")) {
                    if (!marshMallowPermission.checkPermissionForCamera() && !marshMallowPermission.checkPermissionForExternalStorage()) {
                        marshMallowPermission.requestPermissionForCamera();
                        marshMallowPermission.requestPermissionForExternalStorage();
                    } else if (!marshMallowPermission.checkPermissionForCamera()) {
                        marshMallowPermission.requestPermissionForCamera();
                    } else if (!marshMallowPermission.checkPermissionForExternalStorage()) {
                        marshMallowPermission.requestPermissionForExternalStorage();
                    } else {
                        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
                            startActivityForResult(takePictureIntent, TakePicture);
                        }
                    }
//                startActivityForResult(intent, actionCode);
                } else if (items[item].equals("Choose from Library")) {
                    if (!marshMallowPermission.checkPermissionForExternalStorage()) {
                        marshMallowPermission.requestPermissionForExternalStorage();
                    } else {
                        Intent intent = new Intent(
                                Intent.ACTION_PICK,
                                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                        intent.setType("image/*");
                        startActivityForResult(Intent.createChooser(intent, "Select File"),
                                SELECT_FILE);
                    }
                } else if (items[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
       // resultImage=true;
        String imageFileName = System.currentTimeMillis() + ".png";
        if (resultCode == RESULT_OK) {
            if (requestCode == TakePicture) {
                bitmap = (Bitmap) data.getExtras().get("data");
                ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
                profilePics.setImageBitmap(bitmap);

                imgBase64 = utility.image_to_Base64(mContext, utility.getPath(utility.getImageUri(mContext, bitmap), mContext));
            }
            if (requestCode == SELECT_FILE) {
                try {
                    //bitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), data.getData());
                    sourceFile = new File(utility.getPath(data.getData(), mContext));
                    bitmap = new Compressor(mContext).compressToBitmap(sourceFile);
                    //imgBase64=utility.encodeTobase64(bitmap);
                    imgBase64 = utility.image_to_Base64(mContext, utility.getPath(data.getData(), mContext));
                    profilePics.setImageBitmap(bitmap);

                } catch (IOException e) {
                    e.printStackTrace();
                    Toast.makeText(this, "Image not set please try again", Toast.LENGTH_SHORT).show();
                }


            }
                /*bitmap = utility.decodeSampledBitmapFromResource(data.getExtras().get("data").toString(), 100, 100);
                img_1.setVisibility(View.VISIBLE);
                img_1.setImageBitmap(bitmap);*/
        }
    }
}
