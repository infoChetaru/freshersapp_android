package com.chetaru.FreshersApp.view.ui.Fragment;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.location.Address;
import android.location.Geocoder;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.core.app.ActivityOptionsCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.LocaleList;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.text.Html;
import android.transition.TransitionInflater;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.chetaru.FreshersApp.CustomImage.RoundRectCornerImageView;
import com.chetaru.FreshersApp.R;
import com.chetaru.FreshersApp.service.model.MyProfileResponse;
import com.chetaru.FreshersApp.service.model.TodaysLike;
import com.chetaru.FreshersApp.service.model.UserViewPost;
import com.chetaru.FreshersApp.service.retrofit.BaseRequest;
import com.chetaru.FreshersApp.service.retrofit.Functions;
import com.chetaru.FreshersApp.service.retrofit.RequestReciever;
import com.chetaru.FreshersApp.utility.BaseActivity;
import com.chetaru.FreshersApp.utility.MarshMallowPermission;
import com.chetaru.FreshersApp.utility.SessionParam;
import com.chetaru.FreshersApp.utility.Utility;
import com.chetaru.FreshersApp.utility.Validations;
import com.chetaru.FreshersApp.view.adapter.TodayLikeAdapter;
import com.chetaru.FreshersApp.view.customView.ItemOffsetDecoration;
import com.chetaru.FreshersApp.view.ui.Activity.RootActivity;
import com.chetaru.FreshersApp.view.ui.Activity.UploadImageActivity;
import com.chetaru.FreshersApp.view.ui.SignUpActivity;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.mikhaellopez.circularimageview.CircularImageView;
import com.shagi.materialdatepicker.date.DatePickerFragmentDialog;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Optional;
import butterknife.Unbinder;
import de.hdodenhof.circleimageview.CircleImageView;
import id.zelory.compressor.Compressor;
import im.delight.android.location.SimpleLocation;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link MyProfileFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class MyProfileFragment extends Fragment implements View.OnClickListener, TodayLikeAdapter.todayLikeListener {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String BULLET_SYMBOL = "&#8226";

    private boolean zoomOut =  false;

    //check sdk version
    final int sdk = android.os.Build.VERSION.SDK_INT;
    //set constant value for image click
    private int TakePicture = 1, SELECT_FILE = 2, RESULT_OK = -1;
    //check permission
    MarshMallowPermission marshMallowPermission;
    //handel image bitmap and string value
    private Bitmap bitmap = null;
    private String imgBase64 = "";
    private File sourceFile;

    //variable initilization
   private Boolean resultImage=false;
   private int userId;
   private BaseRequest baseRequest;
    Integer  mYear, mMonth, mDay;
    //model instance
    MyProfileResponse myProfile;

    double longitude=0.0;
    double latitude=0.0;

    //handel class
    Utility utility;
    SessionParam sessionParam;
    SimpleLocation location;
    List<Locale> localeList;
    List<TodaysLike> todayList;
    Boolean expand=false;


    //assign a layout id class
    private Unbinder unbinder;
    //initilize imageView
    @BindView(R.id.my_profile_logo)
    CircleImageView profileLogo;
    @BindView(R.id.my_profile_image_logout)
    ImageView logoutImage;
    @BindView(R.id.my_profile_image_cancel)
    ImageView cancelImage;
    @BindView(R.id.my_profile_image_done)
    ImageView doneImage;
    @BindView(R.id.my_profile_image_edit)
    ImageView editImage;
    @BindView(R.id.my_profile_premium_image)
    ImageView premiumImage;

    @BindView(R.id.profile_userId_txt)
    TextView profileUserName;



    @BindView(R.id.profile_name_txt)
    EditText profileName;
    @BindView(R.id.profile_address)
    TextView profileAddress;
    @BindView(R.id.profile_bio)
    TextView profileBio;
    @BindView(R.id.my_profile_layout)
    LinearLayout profileLayout;

    @Nullable
    @BindView(R.id.profile_male_image)
    ImageView maleIconImage;
    @Nullable
    @BindView(R.id.profile_female_image)
    ImageView femaleIconImage;
    @Nullable
    @BindView(R.id.profile_other_image)
    ImageView otherIconImage;
    @BindView(R.id.profile_male_text)
            TextView maleText;
    @BindView(R.id.profile_female_text)
            TextView femaleText;
    @BindView(R.id.profile_other_text)
            TextView otherText;
    @BindView(R.id.show_premium_layout)
    LinearLayout premuimLayout;
    @BindView(R.id.show_user_layout)
    LinearLayout userLayout;
    @BindView(R.id.upgrade_button_profile)
    Button upgradeButton;
    @BindView(R.id.cancel_button_profile)
    Button cancelButton;
    @BindView(R.id.expend_image_view)
    ImageView expendImage;


    @BindView(R.id.today_msg_txt)
    TextView todayMsgText;
    @BindView(R.id.no_top_like_layout)
    LinearLayout noTopLikeLayout;
    @BindView(R.id.today_like_recycler_layout)
    LinearLayout todayRecyclerLayout;
    @BindView(R.id.like_image_recyclerView)
    RecyclerView likeRecyclerView;
    TodayLikeAdapter mAdapter;


    Animation zoomin,zoomout;

    myProfileListener mListener;
    String profileImageString="";

    boolean editable=false;
    Integer gender;
    int likeCount;



    public MyProfileFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     *
     * @return A new instance of fragment MyProfileFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static MyProfileFragment newInstance(MyProfileResponse profile) {
        MyProfileFragment fragment = new MyProfileFragment();
        Bundle args = new Bundle();
        args.putSerializable(ARG_PARAM1,profile);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {

            myProfile = (MyProfileResponse) getArguments().getSerializable(ARG_PARAM1);
        }
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_my_profile, container, false);
        unbinder = ButterKnife.bind(this, view);
        utility=new Utility();
        sessionParam=new SessionParam(getContext());
        location=new SimpleLocation(getContext());
        maleIconImage.setOnClickListener(this);
        femaleIconImage.setOnClickListener(this);
        otherIconImage.setOnClickListener(this);
        editImage.setOnClickListener(this);
        premiumImage.setOnClickListener(this);
        cancelButton.setOnClickListener(this);
        upgradeButton.setOnClickListener(this);
        profileLogo.setOnClickListener(this);
        expendImage.setOnClickListener(this);

        userLayout.setVisibility(View.VISIBLE);
        premuimLayout.setVisibility(View.GONE);

        localeList=new ArrayList<>();
        todayList=new ArrayList<>();
        userId=sessionParam.id;
        //menu option in profile
        /* zoomin = AnimationUtils.loadAnimation(getContext(), R.anim.zoom_in);
         zoomout = AnimationUtils.loadAnimation(getContext(), R.anim.zoom_out);
        profileLogo.setAnimation(zoomin);
        profileLogo.setAnimation(zoomout);*/
        profileName.setEnabled(false);
        if (!location.hasLocationEnabled()){
            SimpleLocation.openSettings(getContext());
        }
        Double lat=location.getLatitude();
        Double log=location.getLongitude();
        defaultSelect();
        premiumImage.setVisibility(View.VISIBLE);
        editImage.setVisibility(View.VISIBLE);
        logoutImage.setVisibility(View.VISIBLE);
       // Set Horizontal Layout Manager
        // for Recycler view
        LinearLayoutManager layoutManager = new LinearLayoutManager( getContext(),LinearLayoutManager.HORIZONTAL,false);
        likeRecyclerView.setLayoutManager(layoutManager);
        likeRecyclerView.setHasFixedSize(true);
        ItemOffsetDecoration itemDecoration = new ItemOffsetDecoration(getContext(), R.dimen.item_offset_medium);
        likeRecyclerView.addItemDecoration(itemDecoration);
        mAdapter=new TodayLikeAdapter(todayList,getContext(),likeCount,this);
        likeRecyclerView.setAdapter(mAdapter);

        marshMallowPermission=new MarshMallowPermission(getActivity());
        if (!marshMallowPermission.checkPermissionForCamera() && !marshMallowPermission.checkPermissionForExternalStorage() &&marshMallowPermission.checkPermissionForFineLoaction()){
            marshMallowPermission.requestPermissionForCamera();
            marshMallowPermission.requestPermissionForExternalStorage();
            marshMallowPermission.requestPermissionForFineLoaction();
        }else if(!marshMallowPermission.checkPermissionForCamera()){
            marshMallowPermission.requestPermissionForCamera();
        }else if (!marshMallowPermission.checkPermissionForExternalStorage()){
            marshMallowPermission.requestPermissionForExternalStorage();
        }else if (!marshMallowPermission.checkPermissionForFineLoaction()){
            marshMallowPermission.checkPermissionForFineLoaction();
        }

        //check profile data handel
        if (myProfile.getId()==null){
            //call api for get data
            geProfile();
        }else {
            setUserDetail(myProfile);
        }
        getLocale();
        profileName.setEnabled(false);
        profileAddress.setEnabled(false);
        profileBio.setEnabled(false);
        maleIconImage.setEnabled(false);
        femaleIconImage.setEnabled(false);
        otherIconImage.setEnabled(false);
        getPremium(view);
        return view;
    }

    private void getPremium(View view) {
        TextView firstTv = (TextView) view.findViewById(R.id.first_bullet_msg);
        TextView sceondTxt = (TextView) view.findViewById(R.id.sceond_bullet_msg);
        TextView thirdTxt = (TextView) view.findViewById(R.id.third_bullet_msg);
        TextView fourthTxt = (TextView) view.findViewById(R.id.fourth_bullet_msg);
        TextView fifthTxt = (TextView) view.findViewById(R.id.fifth_bullet_msg);

        /*firstTv.setText(Html.fromHtml(BULLET_SYMBOL )
                + System.getProperty("line.separator")//this takes you to the next Line
        );
        sceondTxt.setText(Html.fromHtml(BULLET_SYMBOL )
                + System.getProperty("line.separator")
                +"See posts that you have previously liked!"
        );
        thirdTxt.setText(Html.fromHtml(BULLET_SYMBOL + " ")
                + System.getProperty("line.separator")//this takes you to the next Line
        );
        fourthTxt.setText(Html.fromHtml(BULLET_SYMBOL + " ")
                + System.getProperty("line.separator")//this takes you to the next Line
        );
        fifthTxt.setText(Html.fromHtml(BULLET_SYMBOL + " ")
                + System.getProperty("line.separator")//this takes you to the next Line
        );*/
    }

    @Override
    public void onPause() {
        super.onPause();
        if (myProfile==null){
            geProfile();
        }
    }

    private void defaultSelect() {
        if (sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
            otherIconImage.setBackgroundDrawable(ContextCompat.getDrawable(getContext(), R.drawable.other_gray));
            maleIconImage.setBackgroundDrawable(ContextCompat.getDrawable(getContext(), R.drawable.male));
            femaleIconImage.setBackgroundDrawable(ContextCompat.getDrawable(getContext(), R.drawable.femalegray));
        } else {
            otherIconImage.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.other_gray));
            maleIconImage.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.male));
            femaleIconImage.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.femalegray));
        }
    }

    private void selectOther(){
        gender=3;
        if (sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {

            otherIconImage.setBackgroundDrawable(ContextCompat.getDrawable(getContext(), R.drawable.other));
            maleIconImage.setBackgroundDrawable(ContextCompat.getDrawable(getContext(), R.drawable.malegray));
            femaleIconImage.setBackgroundDrawable(ContextCompat.getDrawable(getContext(), R.drawable.femalegray));
        } else {
            otherIconImage.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.other));
            maleIconImage.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.malegray));
            femaleIconImage.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.femalegray));
        }
    }

    private void selectMale() {
        gender=1;
        if (sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
            otherIconImage.setBackgroundDrawable(ContextCompat.getDrawable(getContext(), R.drawable.other_gray));
            maleIconImage.setBackgroundDrawable(ContextCompat.getDrawable(getContext(), R.drawable.male));
            femaleIconImage.setBackgroundDrawable(ContextCompat.getDrawable(getContext(), R.drawable.femalegray));
        } else {
            otherIconImage.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.other_gray));
            maleIconImage.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.male));
            femaleIconImage.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.femalegray));
        }
    }
    private void selectFemale() {
        gender=2;
        if (sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
            otherIconImage.setBackgroundDrawable(ContextCompat.getDrawable(getContext(), R.drawable.other_gray));
            maleIconImage.setBackgroundDrawable(ContextCompat.getDrawable(getContext(), R.drawable.malegray));
            femaleIconImage.setBackgroundDrawable(ContextCompat.getDrawable(getContext(), R.drawable.female));
        } else {
            otherIconImage.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.other_gray));
            maleIconImage.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.malegray));
            femaleIconImage.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.female));
        }
    }

    private void geProfile(){
        baseRequest = new BaseRequest(getContext());
        baseRequest.setBaseRequestListner(new RequestReciever() {
            @Override
            public void onSuccess(int requestCode, String Json, Object object) {
                try {

                    Gson gson = new Gson();
                    JSONObject jsonObject = new JSONObject(Json);
                    JSONObject response= (JSONObject) jsonObject.get("data");


                    myProfile = gson.fromJson(response.toString(), MyProfileResponse.class);
                    sessionParam=new SessionParam(getContext(),myProfile);
                   // sessionParam.persist(getContext());
                    setUserDetail(myProfile);
                }catch (Exception e){
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int requestCode, String errorCode, String message) {
               // utility.showToast(getContext(),message);
            }

            @Override
            public void onNetworkFailure(int requestCode, String message) {

                utility.showToast(getContext(),message);
            }
        });




        JsonObject main_object = new JsonObject();
        main_object.addProperty("userId",sessionParam.id );
        baseRequest.callAPIPostWOLoader(1, main_object, getString(R.string.api_getUserProfile));

    }

    private void setUserDetail(MyProfileResponse myProfile) {
        profileImageString=myProfile.getProfileImage();
        //Glide.with(getContext()).load(myProfile.getProfileImage()).apply(new RequestOptions().centerCrop().placeholder(R.drawable.profile_pics)).into(profileLogo);
        //Picasso.with(getContext()).load(myProfile.getProfileImage()).into(profileLogo);
        Glide.with(getContext()).load(myProfile.getProfileImage()).placeholder(R.drawable.profile_pics).into(profileLogo);

        profileUserName.setText(myProfile.getUserName());
        profileName.setText(myProfile.getName().toString());
        profileBio.setText(myProfile.getBio());
        profileAddress.setText(myProfile.getLocaleName());
        gender= myProfile.getGender();
        latitude= Double.parseDouble(myProfile.getLatitude());
        longitude= Double.parseDouble(myProfile.getLongitude());
        if (gender==1){
            maleIconImage.setVisibility(View.VISIBLE);
            maleText.setVisibility(View.VISIBLE);
            selectMale();
            femaleIconImage.setVisibility(View.INVISIBLE);
            femaleText.setVisibility(View.INVISIBLE);
            otherIconImage.setVisibility(View.INVISIBLE);
            otherText.setVisibility(View.INVISIBLE);
        }else if (gender==2){
            femaleIconImage.setVisibility(View.VISIBLE);
            femaleText.setVisibility(View.VISIBLE);
            selectFemale();
            maleIconImage.setVisibility(View.INVISIBLE);
            maleText.setVisibility(View.INVISIBLE);
            otherIconImage.setVisibility(View.INVISIBLE);
            otherText.setVisibility(View.INVISIBLE);
        }else if (gender==3){
            otherIconImage.setVisibility(View.VISIBLE);
            otherText.setVisibility(View.VISIBLE);
            selectOther();
            maleIconImage.setVisibility(View.INVISIBLE);
            maleText.setVisibility(View.INVISIBLE);
            femaleIconImage.setVisibility(View.INVISIBLE);
            femaleText.setVisibility(View.INVISIBLE);
        }
        likeCount=myProfile.getLikedUserCount();

        mAdapter.setCount(likeCount);
        if (!myProfile.getPremieUser()){
            profileLayout.setVisibility(View.GONE);
            //profileAddress.setVisibility(View.GONE);
        }else{
            profileLayout.setVisibility(View.VISIBLE);
            profileAddress.setVisibility(View.VISIBLE);

        }

            profileLayout.setVisibility(View.VISIBLE);


       /* if (!Validations.isEmptyString(myProfile.getPostImageUrl())){
           // ArrayList aList= new ArrayList(Arrays.asList(myProfile.getPostImageUrl().split(",")));
           // List<String> items = Arrays.asList(myProfile.getPostImageUrl().split(","));
             todayList = Arrays.asList(myProfile.getPostImageUrl().split("\\s*,\\s*"));
             mAdapter.addAll(todayList);
        }*/



        if (Validations.isEmptyString(myProfile.getPostId())){
            noTopLikeLayout.setVisibility(View.VISIBLE);
            todayRecyclerLayout.setVisibility(View.GONE);
            todayMsgText.setText("No post for the day.");
        }else if (myProfile.getLikedUserCount()==0){
            noTopLikeLayout.setVisibility(View.VISIBLE);
            todayRecyclerLayout.setVisibility(View.GONE);
            todayMsgText.setText("No top liked posts for the day.");
        }else {
            todayRecyclerLayout.setVisibility(View.VISIBLE);
            noTopLikeLayout.setVisibility(View.GONE);
        }
        mAdapter.notifyDataSetChanged();

        String dateChange=utility.dateChangeYMDtoDMY(myProfile.getDob());
        // imgBase64 = utility.image_to_Base64(getContext(),myProfile.getProfileImage());

        //imgBase64=utility.getFileToByte(myProfile.getProfileImage());
        //utility.showToast(getContext(),myProfileResponse.toString());
        //get address from latitude and longitude
        String address=getCompleteAddressString(Double.valueOf(myProfile.getLatitude()),Double.valueOf(myProfile.getLongitude()));

    }
    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
        inflater.inflate(R.menu.setting_menu, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    private List<String> imageSapretor(String postImageUrl) {
        List<String> list = new ArrayList<String>(Arrays.asList(postImageUrl.split(" , ")));


        return list;
    }

    private void getLocale() {

        baseRequest = new BaseRequest(getContext());
        baseRequest.setBaseRequestListner(new RequestReciever() {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onSuccess(int requestCode, String Json, Object object) {
                try {

                    Gson gson = new Gson();
                    JSONObject jsonObject = new JSONObject(Json);
                    JSONObject response= (JSONObject) jsonObject.get("data");
                    JSONArray jsonArray = new JSONArray(object.toString());
                    localeList = baseRequest.getDataList(jsonArray, LocaleList.class);

                }catch (Exception e){
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int requestCode, String errorCode, String message) {
                //utility.showToast(getBaseContext(),message);
            }

            @Override
            public void onNetworkFailure(int requestCode, String message) {

                utility.showToast(getContext(),message);
            }
        });

        JsonObject object = Functions.getClient().getJsonMapObject(
                "","");
        baseRequest.callAPIPostWOLoader(1, object, getString(R.string.api_getAllLocales));

    }

    private void showDatePickerDialog(){
        final Calendar c = Calendar.getInstance();
        // c.add(Calendar.YEAR, -18);
        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH);


        DatePickerFragmentDialog datePickerFragmentDialog=DatePickerFragmentDialog.newInstance(new DatePickerFragmentDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePickerFragmentDialog view, int year, int monthOfYear, int dayOfMonth) {

                Calendar userAge = new GregorianCalendar(year,monthOfYear,dayOfMonth);
                Calendar minAdultAge = new GregorianCalendar();
                minAdultAge.add(Calendar.YEAR, -18);



            }
        },mYear, mMonth, mDay);
        Calendar maxDate = Calendar.getInstance();
        maxDate.set(Calendar.DAY_OF_MONTH, mDay);
        maxDate.set(Calendar.MONTH, mMonth);
        maxDate.set(Calendar.YEAR, mYear-18);

        datePickerFragmentDialog.show(getFragmentManager(),null);
        datePickerFragmentDialog.setMaxDate(maxDate.getTimeInMillis());
        datePickerFragmentDialog.setYearRange(1900,mYear-18);
        datePickerFragmentDialog.setCancelColor(getResources().getColor(R.color.colorPrimaryDark));
        datePickerFragmentDialog.setOkColor(getResources().getColor(R.color.colorPrimary));
        datePickerFragmentDialog.setAccentColor(getResources().getColor(R.color.colorAccent));
        datePickerFragmentDialog.setOkText(getResources().getString(R.string.ok_dob));
        datePickerFragmentDialog.setCancelText(getResources().getString(R.string.cancel_dob));
    }


    @Override public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    private String getCompleteAddressString(double LATITUDE, double LONGITUDE) {
        String strAdd = "";
        Geocoder geocoder = new Geocoder(getContext(), Locale.getDefault());
        try {
            List<Address> addresses = geocoder.getFromLocation(LATITUDE, LONGITUDE, 1);
            if (addresses != null) {
                Address returnedAddress = addresses.get(0);
                StringBuilder strReturnedAddress = new StringBuilder("");

                for (int i = 0; i <= returnedAddress.getMaxAddressLineIndex(); i++) {
                    strReturnedAddress.append(returnedAddress.getAddressLine(i)).append("\n");
                }
                strAdd = strReturnedAddress.toString();

                if (addresses != null && addresses.size() > 0) {
                    String address = addresses.get(0).getAddressLine(0);
                    String city = addresses.get(0).getLocality();
                    String state = addresses.get(0).getAdminArea();
                    String country = addresses.get(0).getCountryName();
                    String postalCode = addresses.get(0).getPostalCode();
                    String knownName = addresses.get(0).getFeatureName();

                   // profileAddress.setText(address + " " + city + " " + country);
                }
                Log.w("My  location address", strReturnedAddress.toString());
            } else {
                Log.w("My  location address", "No Address returned!");
            }
        } catch (Exception e) {
            e.printStackTrace();
            Log.w("My  location address", "Cannot get Address!");
        }
        return strAdd;
    }

    /*public LatLng getLocationFromAddress(Context context, String strAddress) {

        Geocoder coder = new Geocoder(context);
        List<Address> address;
        LatLng p1 = null;


        try {
            // May throw an IOException
            address = coder.getFromLocationName(strAddress, 5);
            if (address == null) {
                return null;
            }

            Address location = address.get(0);
            p1 = new LatLng(location.getLatitude(), location.getLongitude() );

        } catch (IOException ex) {

            ex.printStackTrace();
        }

        return p1;
    }*/
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof myProfileListener) {
            mListener = (myProfileListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement TagChildFragment");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        resultImage=true;
        if (resultCode == RESULT_OK) {
            if (requestCode == TakePicture) {
                bitmap = (Bitmap) data.getExtras().get("data");
                ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
                profileLogo.setImageBitmap(bitmap);

                imgBase64 = utility.image_to_Base64(getContext(), utility.getPath(utility.getImageUri(getContext(), bitmap), getContext()));
            }
            if (requestCode == SELECT_FILE) {
                try {
                    //bitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), data.getData());
                    sourceFile = new File(utility.getPath(data.getData(), getContext()));
                    bitmap = new Compressor(getContext()).compressToBitmap(sourceFile);
                    imgBase64 = utility.image_to_Base64(getContext(), utility.getPath(data.getData(), getContext()));
                    profileLogo.setImageBitmap(bitmap);

                } catch (IOException e) {
                    e.printStackTrace();
                    utility.showToast(getContext(),"Image not set please try again");
                }


            }
                /*bitmap = utility.decodeSampledBitmapFromResource(data.getExtras().get("data").toString(), 100, 100);
                img_1.setVisibility(View.VISIBLE);
                img_1.setImageBitmap(bitmap);*/
        }
    }

    @Optional
    @OnClick({ R.id.my_profile_premium_image,R.id.my_profile_image_edit,R.id.cancel_button_profile,R.id.upgrade_button_profile,
            R.id.profile_male_image,R.id.profile_female_image,R.id.profile_other_image,R.id.expend_image_view,
            R.id.my_profile_logo,R.id.my_profile_image_logout,R.id.my_profile_image_done,R.id.my_profile_image_cancel})
    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.my_profile_logo:
                if (editable) {
                    selectImage();
                }else {

                    if (!Validations.isEmptyString(profileImageString))
                    mListener.myProfileClick(view,profileImageString);

                   /* Intent intent = new Intent(getContext(), UploadImageActivity.class);
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        ActivityOptionsCompat options = ActivityOptionsCompat.
                                makeSceneTransitionAnimation(getActivity(),profileLogo, "MyTransition");
                        startActivity(intent, options.toBundle());
                    } else {
                        startActivity(intent);
                    }*/
                    /*if(zoomOut) {
                        //Toast.makeText(getApplicationContext(), "NORMAL SIZE!", Toast.LENGTH_LONG).show();
                        profileLogo.startAnimation(zoomin);
                        zoomOut =false;
                    }else{
                       // Toast.makeText(getApplicationContext(), "FULLSCREEN!", Toast.LENGTH_LONG).show();
                        profileLogo.startAnimation(zoomout);
                        zoomOut = true;
                    }*/
                }

                break;
            case R.id.my_profile_image_edit:
                cancelImage.setVisibility(View.VISIBLE);
                doneImage.setVisibility(View.VISIBLE);
                premiumImage.setVisibility(View.GONE);
                editImage.setVisibility(View.GONE);
                editable=true;
                if (!marshMallowPermission.checkPermissionForCamera() && !marshMallowPermission.checkPermissionForExternalStorage()) {
                    marshMallowPermission.requestPermissionForCamera();
                    marshMallowPermission.requestPermissionForExternalStorage();
                } else if (!marshMallowPermission.checkPermissionForCamera()) {
                    marshMallowPermission.requestPermissionForCamera();
                } else if (!marshMallowPermission.checkPermissionForExternalStorage()) {
                    marshMallowPermission.requestPermissionForExternalStorage();
                } else {
                    maleIconImage.setVisibility(View.VISIBLE);
                    maleText.setVisibility(View.VISIBLE);
                    femaleIconImage.setVisibility(View.VISIBLE);
                    femaleText.setVisibility(View.VISIBLE);
                    otherIconImage.setVisibility(View.VISIBLE);
                    otherText.setVisibility(View.VISIBLE);

                    profileName.setEnabled(true);
                    profileBio.setEnabled(true);
                    //et_email.setEnabled(true);
                    profileAddress.setEnabled(true);
                    profileLogo.setEnabled(true);
                    maleIconImage.setEnabled(true);

                    femaleIconImage.setEnabled(true);
                    otherIconImage.setEnabled(true);


                }

                break;
            case R.id.my_profile_image_done:
                String profileNameChange=profileName.getText().toString().trim();
                String profileAddressChange=profileAddress.getText().toString().trim();
                String profileBioChange=profileBio.getText().toString().trim();
               if (Validations.isEmptyString(profileNameChange)){
                   utility.showToast(getContext(),"Please Enter name first");
                   return;
               }else
               if (Validations.isEmptyString(profileBioChange)){
                   utility.showToast(getContext(),"Please enter bio");
                   return;
               }else
               {
                   profileChange(profileNameChange, "dob", profileBioChange);

                   profileName.setEnabled(false);
                   profileAddress.setEnabled(false);
                   profileBio.setEnabled(false);
                   maleIconImage.setEnabled(false);
                   femaleIconImage.setEnabled(false);
                   otherIconImage.setEnabled(false);
                   cancelImage.setVisibility(View.GONE);
                   doneImage.setVisibility(View.GONE);
                   premiumImage.setVisibility(View.VISIBLE);
                   editImage.setVisibility(View.VISIBLE);
               }
                break;
            case R.id.my_profile_image_cancel:
                editable = false;
                profileName.setEnabled(false);
                profileAddress.setEnabled(false);
                profileBio.setEnabled(false);
                maleIconImage.setEnabled(false);
                femaleIconImage.setEnabled(false);
                otherIconImage.setEnabled(false);
                cancelImage.setVisibility(View.GONE);
                doneImage.setVisibility(View.GONE);
                premiumImage.setVisibility(View.VISIBLE);
                editImage.setVisibility(View.VISIBLE);

                if (gender==1){
                    maleIconImage.setVisibility(View.VISIBLE);
                    maleText.setVisibility(View.VISIBLE);
                    selectMale();
                    femaleIconImage.setVisibility(View.INVISIBLE);
                    femaleText.setVisibility(View.INVISIBLE);
                    otherIconImage.setVisibility(View.INVISIBLE);
                    otherText.setVisibility(View.INVISIBLE);
                }else if (gender==2){
                    femaleIconImage.setVisibility(View.VISIBLE);
                    femaleText.setVisibility(View.VISIBLE);
                    selectFemale();
                    maleIconImage.setVisibility(View.INVISIBLE);
                    maleText.setVisibility(View.INVISIBLE);
                    otherIconImage.setVisibility(View.INVISIBLE);
                    otherText.setVisibility(View.INVISIBLE);
                }else if (gender==3){
                    otherIconImage.setVisibility(View.VISIBLE);
                    otherText.setVisibility(View.VISIBLE);
                    selectOther();
                    maleIconImage.setVisibility(View.INVISIBLE);
                    maleText.setVisibility(View.INVISIBLE);
                    femaleIconImage.setVisibility(View.INVISIBLE);
                    femaleText.setVisibility(View.INVISIBLE);
                }
                break;
            case R.id.my_profile_premium_image:
                userLayout.setVisibility(View.GONE);
                premuimLayout.setVisibility(View.VISIBLE);

                break;
            case R.id.profile_male_image:
                selectMale();
                break;
            case R.id.profile_female_image:
                selectFemale();
                break;
            case R.id.profile_other_image:
                selectOther();
                break;

            case R.id.my_profile_image_logout:
                logoutDialog();
                break;
            case R.id.cancel_button_profile:
                userLayout.setVisibility(View.VISIBLE);
                premuimLayout.setVisibility(View.GONE);
                break;
            case R.id.upgrade_button_profile:
                mListener.myPremiumClick(myProfile);
                break;
            case R.id.expend_image_view:
                //myProfile.getProfileImage();
                if (true){
                    getExpendClick();
                }
                break;

        }

    }

    private void getExpendClick() {
        if (!expand) {
            expand = true;
            expendImage.setImageResource(R.drawable.ic_expand_more_white_24dp);
            if (Validations.isEmptyString(myProfile.getPostId())){
                noTopLikeLayout.setVisibility(View.VISIBLE);
                todayRecyclerLayout.setVisibility(View.GONE);
                todayMsgText.setText("No post for the day.");
            }else if (myProfile.getLikedUserCount()==0){
                noTopLikeLayout.setVisibility(View.VISIBLE);
                todayRecyclerLayout.setVisibility(View.GONE);
                todayMsgText.setText("No top liked posts for the day.");
            }else {
                todayRecyclerLayout.setVisibility(View.VISIBLE);
                noTopLikeLayout.setVisibility(View.GONE);
            }

        }else {
            if (Validations.isEmptyString(myProfile.getPostId())){
                todayRecyclerLayout.setVisibility(View.GONE);
                noTopLikeLayout.setVisibility(View.GONE);
                todayMsgText.setText("No post for the day.");
            }else if (myProfile.getLikedUserCount()==0){
                todayMsgText.setText("No top liked posts for the day.");
            }else {
                noTopLikeLayout.setVisibility(View.GONE);
                todayRecyclerLayout.setVisibility(View.GONE);
            }
            expand=false;
            expendImage.setImageResource(R.drawable.ic_expand_less_white_24dp);
        }
    }


    private void selectImage() {
        //here user will get options to choose image
        final CharSequence[] items = {"Take Photo", "Choose from Library", "Cancel"};
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle("Add Photo!");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (items[item].equals("Take Photo")) {
                    if (!marshMallowPermission.checkPermissionForCamera() && !marshMallowPermission.checkPermissionForExternalStorage()) {
                        marshMallowPermission.requestPermissionForCamera();
                        marshMallowPermission.requestPermissionForExternalStorage();
                    } else if (!marshMallowPermission.checkPermissionForCamera()) {
                        marshMallowPermission.requestPermissionForCamera();
                    } else if (!marshMallowPermission.checkPermissionForExternalStorage()) {
                        marshMallowPermission.requestPermissionForExternalStorage();
                    } else {
                        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                        if (takePictureIntent.resolveActivity(getActivity().getPackageManager()) != null) {
                            startActivityForResult(takePictureIntent, TakePicture);
                        }
                    }
//                startActivityForResult(intent, actionCode);
                } else if (items[item].equals("Choose from Library")) {
                    if (!marshMallowPermission.checkPermissionForExternalStorage()) {
                        marshMallowPermission.requestPermissionForExternalStorage();
                    } else {
                        Intent intent = new Intent(
                                Intent.ACTION_PICK,
                                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                        intent.setType("image/*");
                        startActivityForResult(Intent.createChooser(intent, "Select File"),
                                SELECT_FILE);
                    }
                } else if (items[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }


    private void profileChange(String profileNameChange, String bod, String profileBioChange) {
        baseRequest = new BaseRequest(getContext());
        baseRequest.setBaseRequestListner(new RequestReciever() {
            @Override
            public void onSuccess(int requestCode, String Json, Object object) {
                try {

                    editable=false;
                    Gson gson = new Gson();
                    JSONObject jsonObject = new JSONObject(Json);

                    JSONObject response= (JSONObject) jsonObject.get("data");
                   /* try {
                        sessionParam = new SessionParam((JSONObject) response);
                        sessionParam.persist(getContext());
                    }catch (Exception e){
                        e.printStackTrace();
                    }*/
                    myProfile = gson.fromJson(response.toString(), MyProfileResponse.class);
                    sessionParam=new SessionParam(getContext(),myProfile);

                    profileImageString=myProfile.getProfileImage();
                   // Glide.with(getContext()).load(myProfile.getProfileImage()).apply(new RequestOptions().centerCrop().placeholder(R.drawable.profile_pics)).into(profileLogo);
                    setUserDetail(myProfile);


                   /* if (!myProfile.getPremieUser()){
                        profileLayout.setVisibility(View.GONE);
                        profileAddress.setVisibility(View.GONE);
                        noTopLikeLayout.setVisibility(View.GONE);
                    }else{
                        profileLayout.setVisibility(View.VISIBLE);
                        profileAddress.setVisibility(View.VISIBLE);
                        noTopLikeLayout.setVisibility(View.VISIBLE);
                    }*/
                    String changeDate=utility.dateChangeYMDtoDMY(myProfile.getDob());
                    // imgBase64 = utility.image_to_Base64(getContext(),myProfile.getProfileImage());

                    //imgBase64=utility.getFileToByte(myProfile.getProfileImage());
                    //utility.showToast(getContext(),myProfileResponse.toString());
                    //get address from lat long
                    String address=getCompleteAddressString(Double.valueOf(myProfile.getLatitude()),Double.valueOf(myProfile.getLongitude()));
                    //profileAddress.setText(address);
                }catch (Exception e){
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int requestCode, String errorCode, String message) {
                //utility.showToast(getContext(),message);
            }

            @Override
            public void onNetworkFailure(int requestCode, String message) {

                utility.showToast(getContext(),message);
            }
        });



        JsonObject object = Functions.getClient().getJsonMapObject(
                "name",profileNameChange,
                "bio",profileBioChange,
                "gender",gender+"",
                "type","",
                "countryId","",
               /* "latitude",latitude+"",
                "longitude",longitude+"",*/
                "profileImage",imgBase64+"" );
        baseRequest.callAPIPostWOLoader(1, object, getString(R.string.api_updateUserProfile));

    }
    public void logoutDialog() {
        final Dialog dialog = new Dialog(getContext());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_logout);
        dialog.setCanceledOnTouchOutside(true);
        TextView tv_cancel = dialog.findViewById(R.id.tv_cancel);
        TextView tv_logout = dialog.findViewById(R.id.tv_logout);
        tv_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        tv_logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getContext());
                SharedPreferences.Editor editor = prefs.edit();
                editor.clear();
                editor.commit();
                //apiTellSidLogout();
                api_logout();
            }
        });

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = dialog.getWindow();
        lp.copyFrom(window.getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);
        dialog.show();
    }
    public void api_logout() {
        baseRequest = new BaseRequest(getContext());
        baseRequest.setBaseRequestListner(new RequestReciever() {
            @Override
            public void onSuccess(int requestCode, String Json, Object object) {
                sessionParam.clearPreferences(getContext());
                startActivity(new Intent(getContext(), RootActivity.class));
                BaseActivity.finishAllActivitiesStatic();
            }

            @Override
            public void onFailure(int requestCode, String errorCode, String message) {
               // utility.showToast(getContext(), message);
            }

            @Override
            public void onNetworkFailure(int requestCode, String message) {
                utility.showToast(getContext(), message);
            }
        });
        JsonObject main_object = new JsonObject();
        main_object.addProperty("fcmId", sessionParam.fcmId);
        baseRequest.callAPIPost(1, main_object, getString(R.string.api_logout));
    }



    @Override
    public void todayLikeImage(String images,int postId) {

    }

    public interface myProfileListener{
        public void myProfileClick(View view,String imageString);
        public void myPremiumClick(MyProfileResponse myProfile);
    }
}
