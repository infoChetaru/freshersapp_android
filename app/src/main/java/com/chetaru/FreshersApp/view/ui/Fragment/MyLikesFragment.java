package com.chetaru.FreshersApp.view.ui.Fragment;

import android.content.Context;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.chetaru.FreshersApp.R;

import com.chetaru.FreshersApp.service.model.UserViewPost;
import com.chetaru.FreshersApp.service.retrofit.BaseRequest;
import com.chetaru.FreshersApp.service.retrofit.Functions;
import com.chetaru.FreshersApp.service.retrofit.RequestReciever;
import com.chetaru.FreshersApp.utility.SessionParam;
import com.chetaru.FreshersApp.utility.Utility;
import com.chetaru.FreshersApp.view.adapter.MyLikeAdapter;
import com.chetaru.FreshersApp.view.adapter.MyUploadAdapter;
import com.chetaru.FreshersApp.view.customView.ItemOffsetDecoration;
import com.google.gson.Gson;
import com.google.gson.JsonObject;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link MyLikesFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class MyLikesFragment extends Fragment implements View.OnClickListener {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private Unbinder unbinder;

    Context mContext;
    Utility utility;
    SessionParam sessionParam;
    String tokenId="";
    BaseRequest baseRequest;
    private static final int PAGE_START = 1;
    private boolean isLoading = false;
    private int currentPage = PAGE_START;
    private static int TOTAL_PAGES = 1;
    private boolean isLastPage = false;
    String dateTime;
    int userId=0;
    boolean premiumUser=false;

    //assign a layout id class
    @BindView(R.id.my_like_recyclerview)
    RecyclerView myLikeRecyclerView;
    @BindView(R.id.premium_upgrade_button)
    Button upgradeButton;

    @BindView(R.id.my_like_swap_refresh)
    SwipeRefreshLayout myLikeRefresh;
    //initialize adapter and all
    LinearLayoutManager layoutManager;
    MyLikeAdapter uploadAdapter;
    MyLikeFragmentListener mListener;

    List<UserViewPost> uploadList;
    @BindView(R.id.my_like_layout)
    LinearLayout noLikeLayout;
    @BindView(R.id.recycler_layout)
    LinearLayout recyclerLayout;
    @BindView(R.id.premium_user_layout)
    LinearLayout premiumLayout;
    @BindView(R.id.back_my_like_image)
    ImageView backMyLikes;


    public MyLikesFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment MyLikesFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static MyLikesFragment newInstance(String param1, String param2) {
        MyLikesFragment fragment = new MyLikesFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_my_likes, container, false);
        unbinder = ButterKnife.bind(this, view);
        mContext=getContext();
        uploadList=new ArrayList<>();
        utility=new Utility();
        sessionParam=new SessionParam(mContext);
        userId=sessionParam.id;
        premiumUser=sessionParam.premieUser;
        backMyLikes.setOnClickListener(this);
       // currentDate = Calendar.getInstance().getTime();
        //dateTime = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).format(new Date());


        uploadAdapter = new MyLikeAdapter(uploadList, getContext(),new MyLikeAdapter.MyLikeFragmentListener() {
            @Override
            public void myLikeClick(List<UserViewPost> likePost,int position) {
                mListener.myLikeFragmentClick(likePost,position);
            }
        });
        upgradeButton.setOnClickListener(this);
        myLikeRecyclerView.setAdapter(uploadAdapter);
        layoutManager = new GridLayoutManager(getContext(),3);
        myLikeRecyclerView.setLayoutManager(layoutManager);
        myLikeRecyclerView.setHasFixedSize(true);
        ItemOffsetDecoration itemDecoration = new ItemOffsetDecoration(getActivity(), R.dimen.item_offset_medium);
        myLikeRecyclerView.addItemDecoration(itemDecoration);
        myLikeRecyclerView.addOnScrollListener(new PaginationScrollList(layoutManager) {
            @Override
            protected void loadMoreItems() {
                isLastPage=true;
                currentPage+=1;
                api_loadNextPage();
            }

            @Override
            public boolean isLastPage() {
                return isLastPage;
            }

            @Override
            public boolean isLoading() {
                return isLoading;
            }
        });

        myLikeRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh()  {
                myLikeRefresh.setRefreshing(true);
                MyLikeRefresh();
                myLikeRefresh.setRefreshing(false);
            }
        });
        premiumLayout.setVisibility(View.GONE);
        noLikeLayout.setVisibility(View.GONE);
        recyclerLayout.setVisibility(View.GONE);
       /* if (premiumUser) {
            //call Api for likes
            getUserLikes();
        }else {
            premiumLayout.setVisibility(View.VISIBLE);
        }*/
        getUserLikes();

        return view;
    }


    @Override
    public void onResume() {
        super.onResume();
       /* if (uploadList.size()<=0){
            getUserLikes();
        }*/
    }

    @Override
    public void onPause() {
        super.onPause();
        /*if (uploadList.size()<=0){
            getUserLikes();
        }*/
    }


    private void getUserLikes() {
        uploadList.clear();
        //uploadAdapter.clear();
        baseRequest = new BaseRequest(getContext());
        baseRequest.setBaseRequestListner(new RequestReciever() {
            @Override
            public void onSuccess(int requestCode, String Json, Object object) {
                try {
                    Gson gson = new Gson();
                    JSONObject jsonObject = new JSONObject(Json);
                    TOTAL_PAGES = jsonObject.optInt("totalPageCount");
                    // currentPage = jsonObject.optInt("currentPage");
                    //ArrayList myUploadList= (ArrayList) jsonObject.get("data");
                    JSONArray userJsonArray = jsonObject.getJSONArray("data");

                    uploadList = baseRequest.getDataList(userJsonArray, UserViewPost.class);
                    if (uploadList.size()>0){
                        noLikeLayout.setVisibility(View.GONE);
                        recyclerLayout.setVisibility(View.VISIBLE);
                    }else {
                        noLikeLayout.setVisibility(View.VISIBLE);
                        recyclerLayout.setVisibility(View.GONE);
                    }
                    uploadAdapter = new MyLikeAdapter(uploadList, getContext(), new MyLikeAdapter.MyLikeFragmentListener() {
                        @Override
                        public void myLikeClick(List<UserViewPost> likePost,int position) {

                            mListener.myLikeFragmentClick(likePost,position);
                        }
                    });
                    myLikeRecyclerView.setAdapter(uploadAdapter);
                    // uploadAdapter.addAll(uploadList);
                    if (currentPage < TOTAL_PAGES)
                        uploadAdapter.addLoadingFooter();
                    else
                        isLastPage = true;
                    try {
                        if (uploadList.size() > 0) {
                            dateTime = uploadList.get(0).getCreated_at();
                        }
                    }catch (Exception e){
                        e.printStackTrace();
                    }

                }catch (Exception e){
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int requestCode, String errorCode, String message) {
                // utility.showToast(mContext,message);
                noLikeLayout.setVisibility(View.VISIBLE);
                recyclerLayout.setVisibility(View.GONE);
            }

            @Override
            public void onNetworkFailure(int requestCode, String message) {
                utility.showToast(getContext(),message);
            }
        });

        JsonObject main_object = new JsonObject();
        main_object.addProperty("userId", userId);
        main_object.addProperty("page", currentPage);
        baseRequest.callAPIPostWOLoader(1, main_object, getString(R.string.api_getUserLikedPost));
    }

    private void api_loadNextPage() {
        baseRequest = new BaseRequest(getContext());
        baseRequest.setBaseRequestListner(new RequestReciever() {
            @Override
            public void onSuccess(int requestCode, String Json, Object object) {
                try {

                    JSONObject jsonObject = new JSONObject(object.toString());
                    TOTAL_PAGES = jsonObject.optInt("totalPageCount");
                    JSONArray userJsonArray = jsonObject.getJSONArray("data");
                    // ArrayList<ModelTribeometerGraph> list = new ArrayList<>();
                    uploadList = baseRequest.getDataList(userJsonArray, UserViewPost.class);

                    uploadAdapter.removeLoadingFooter();
                    isLoading = false;
                    uploadAdapter.addAll(uploadList);
                    //rv_list_archive.setAdapter(ad_notificationList);
                    if (currentPage != TOTAL_PAGES) uploadAdapter.addLoadingFooter();
                    else isLastPage = true;

                }catch (Exception e){
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int requestCode, String errorCode, String message) {
                //  utility.showToast(mContext,message);
            }

            @Override
            public void onNetworkFailure(int requestCode, String message) {
                utility.showToast(getContext(),message);
            }
        });

        JsonObject main_object = new JsonObject();
        main_object.addProperty("userId", userId);
        main_object.addProperty("page", 0);

        baseRequest.callAPIPostWOLoader(1, main_object, getString(R.string.api_getUserLikedPost));

    }
    private void MyLikeRefresh() {
        baseRequest = new BaseRequest(getContext());
        baseRequest.setBaseRequestListner(new RequestReciever() {
            @Override
            public void onSuccess(int requestCode, String Json, Object object) {
                try {

                    myLikeRefresh.setRefreshing(false);
                    JSONObject jsonObject = new JSONObject(object.toString());
                    JSONArray userJsonArray = jsonObject.getJSONArray("data");
                    // ArrayList<ModelTribeometerGraph> list = new ArrayList<>();
                    List<UserViewPost>  newUploadList = baseRequest.getDataList(userJsonArray, UserViewPost.class);
                    if (newUploadList.size()>0) {
                        for (int i = 0; i < newUploadList.size(); i++) {
                            uploadList.add(i, newUploadList.get(i));
                        }
                        // uploadAdapter.addAll(uploadList);
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int requestCode, String errorCode, String message) {
                //  utility.showToast(mContext,message);
            }

            @Override
            public void onNetworkFailure(int requestCode, String message) {
                utility.showToast(getContext(),message);
            }
        });

        JsonObject main_object = new JsonObject();
        main_object.addProperty("userId", userId);
        main_object.addProperty("page", 0);
        main_object.addProperty("date", dateTime);

        baseRequest.callAPIPostWOLoader(1, main_object, getString(R.string.api_getUserLikedPost));

    }

    //remove view From unbind
    @Override public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof MyLikeFragmentListener) {
            mListener = (MyLikeFragmentListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement TagChildFragment");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()){
            case R.id.premium_upgrade_button:
                mListener.likeUpgradeClick();
                break;
            case R.id.back_my_like_image:
                mListener.myLikeBack();
                break;
        }
    }

    public abstract class PaginationScrollList extends RecyclerView.OnScrollListener {

        private LinearLayoutManager layoutManager;

        public PaginationScrollList(LinearLayoutManager layoutManager) {
            this.layoutManager = layoutManager;
        }

        @Override
        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
            super.onScrolled(recyclerView, dx, dy);

            int visibleItemCount = layoutManager.getChildCount();
            int totalItemCount = layoutManager.getItemCount();
            int firstVisibleItemPosition = layoutManager.findFirstVisibleItemPosition();

            if (!isLoading() && !isLastPage()) {
                if ((visibleItemCount + firstVisibleItemPosition) >= totalItemCount
                        && firstVisibleItemPosition >= 0) {
                    loadMoreItems();
                }
            }
        }

        protected abstract void loadMoreItems();

        public abstract boolean isLastPage();

        public abstract boolean isLoading();

    }
    public interface MyLikeFragmentListener{
        void myLikeFragmentClick(List<UserViewPost> likePost,int position);
        void likeUpgradeClick();
        void myLikeBack();
    }

}
