package com.chetaru.FreshersApp.view.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;
import com.chetaru.FreshersApp.R;

import com.chetaru.FreshersApp.service.model.UserViewPost;
import com.chetaru.FreshersApp.utility.SessionParam;
import com.chetaru.FreshersApp.utility.Utility;
import com.chetaru.FreshersApp.view.customView.SquareImageView;

import java.util.ArrayList;
import java.util.List;

public class MyLikeAdapter extends RecyclerView.Adapter<MyLikeAdapter.MyViewHolder> {
    List<UserViewPost> list = new ArrayList<>();
    private List<UserViewPost> mFilteredList;
    private Context context;
    private boolean isLoadingAdded = false;
    // View Types
    private static final int ITEM = 0;
    private static final int LOADING = 1;
    private boolean retryPageLoad = false;
    private String errorMsg;
    Utility utility;
    SessionParam sessionParam;
    MyLikeFragmentListener mListener;


    public static class MyViewHolder extends RecyclerView.ViewHolder{

        SquareImageView likeImages;
        public MyViewHolder(@NonNull View view) {
            super(view);
            likeImages=view.findViewById(R.id.tag_image_list);

        }
    }

    public MyLikeAdapter(List<UserViewPost> list, Context context,MyLikeFragmentListener mListener) {
        this.list = list;
        this.mFilteredList = list;
        this.mListener=mListener;
        notifyDataSetChanged();
        this.context = context;
        utility=new Utility();
        sessionParam = new SessionParam(context);
    }

    protected class LoadingVH extends MyViewHolder implements View.OnClickListener {
        private ProgressBar mProgressBar;
        private ImageButton mRetryBtn;
        private TextView mErrorTxt;
        private LinearLayout mErrorLayout;

        public LoadingVH(View itemView) {
            super(itemView);
            mProgressBar = itemView.findViewById(R.id.loadmore_progress);
            mRetryBtn = itemView.findViewById(R.id.loadmore_retry);
            mErrorTxt = itemView.findViewById(R.id.loadmore_errortxt);
            mErrorLayout = itemView.findViewById(R.id.loadmore_errorlayout);
            mRetryBtn.setOnClickListener(this);
            mErrorLayout.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.loadmore_retry:
                case R.id.loadmore_errorlayout:
                    showRetry(false, null);
                    // mCallback.retryPageLoad();
                    break;
            }
        }
    }

    public void showRetry(boolean show, @Nullable String errorMsg) {
        retryPageLoad = show;
        notifyItemChanged(mFilteredList.size() - 1);

        if (errorMsg != null) this.errorMsg = errorMsg;
    }
    @Override
    public int getItemCount() {

        return mFilteredList == null ? 0 : mFilteredList.size();
    }

    public void add(UserViewPost r) {
        mFilteredList.add(r);
        notifyItemInserted(mFilteredList.size()-1);
    }

    public void addAll(List<UserViewPost> viewPost_Results) {
        for (UserViewPost result : viewPost_Results) {
            add(result);
        }
    }
    public void remove(UserViewPost r) {
        int position = mFilteredList.indexOf(r);
        if (position > -1) {
            mFilteredList.remove(position);
            notifyItemRemoved(position);
        }
    }

    public void addLoadingFooter() {
        isLoadingAdded = true;
        add(new UserViewPost());
    }

    public void removeLoadingFooter() {
        isLoadingAdded = false;

        int position = mFilteredList.size() - 1;
        UserViewPost result = getItem(position);

        if (result != null) {
            mFilteredList.remove(position);
            notifyItemRemoved(position);
        }
    }


    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        MyViewHolder viewHolder=null;
        LayoutInflater inflater=LayoutInflater.from(parent.getContext());
        switch (viewType){
            case ITEM:
                View viewItem=inflater.inflate(R.layout.tag_image_list,parent,false);
                viewHolder=new MyViewHolder(viewItem);
                break;
            case LOADING:
                View viewLoading=inflater.inflate(R.layout.item_progress,parent,false);
                viewHolder=new LoadingVH(viewLoading);
                break;
        }
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        switch (getItemViewType(position)){

            case ITEM:
                UserViewPost viewPost=mFilteredList.get(position);
                try {
                    Glide.with(context)
                            .asBitmap().load(viewPost.getImageUrl())
                            .into(new SimpleTarget<Bitmap>() {
                                @Override
                                public void onResourceReady(Bitmap resource, Transition<? super Bitmap> transition) {
                                    holder.likeImages.setImageBitmap(resource);
                                }
                            });
                    holder.likeImages.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            mListener.myLikeClick(mFilteredList,position);
                        }
                    });

                    // Glide.with(mContext).load(mNewsItem.getImageUrl()).into(newsImage);
                    // Glide.with(mContext).load(mNewsItem.getImageUrl()).apply(new RequestOptions().placeholder(R.drawable.profile_pics)).into(holder.newsImage);

                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
            case LOADING:
                LoadingVH loadingVH = (LoadingVH) holder;

                if (retryPageLoad) {
                    loadingVH.mErrorLayout.setVisibility(View.VISIBLE);
                    loadingVH.mProgressBar.setVisibility(View.GONE);

                    loadingVH.mErrorTxt.setText(
                            errorMsg != null ?
                                    errorMsg :
                                    context.getString(R.string.error_msg_unknown));

                } else {
                   /* if(mFilteredList.get(mFilteredList.size()-1).getSelected()==null){
                        loadingVH.mErrorLayout.setVisibility(View.GONE);
                        loadingVH.mProgressBar.setVisibility(View.GONE);
                    }else{
                        loadingVH.mErrorLayout.setVisibility(View.GONE);
                        loadingVH.mProgressBar.setVisibility(View.VISIBLE);
                    }*/
                }
                break;
        }

    }

    @Override
    public int getItemViewType(int position) {
        return (position == mFilteredList.size() - 1 && isLoadingAdded) ? LOADING : ITEM;
//        if(mFilteredList.size()<5){
//            return mFilteredList.size() - 1;
//        }else{
//            return (position == mFilteredList.size() - 1 && isLoadingAdded) ? LOADING : ITEM;
//        }
    }

    public UserViewPost getItem(int position) {
        return mFilteredList.get(position);
    }

    public interface MyLikeFragmentListener{
        void myLikeClick(List<UserViewPost> likePost,int position);
    }
}
