package com.chetaru.FreshersApp.view.callback;

import android.view.View;

import com.chetaru.FreshersApp.service.model.PendingRequestList;

public interface RequestListCallback {
    void requestLogoClick(View view,String stringImage);
    void requestUserProfile(PendingRequestList requestList);
}
