package com.chetaru.FreshersApp.view.adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.core.content.res.ResourcesCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.chetaru.FreshersApp.R;
import com.chetaru.FreshersApp.service.model.DefaultData;
import com.chetaru.FreshersApp.utility.Utility;

import java.util.List;

public class ProfileMenuAdapter extends RecyclerView.Adapter<ProfileMenuAdapter.MyViewHolder> {

    List<DefaultData> menuData;
    Context mContext;
    Utility utility;
    profileMenuListener menuListener;

    private static int currentPosition = 0;

    public ProfileMenuAdapter(Context mContext,List<DefaultData> menuData,profileMenuListener menuListener) {
        this.menuData = menuData;
        this.mContext = mContext;
        this.utility=new Utility();
        this.menuListener=menuListener;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.profile_menu_list,parent,false);
        MyViewHolder viewHolder=new MyViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {

        DefaultData menu = menuData.get(position);
        holder.mTextView.setText(menu.profileName);
       /* Typeface face = Typeface.createFromAsset(mContext.getAssets(),
                "fonts/poppins_regular.ttf");
        holder.mTextView.setTypeface(face);*/
        Typeface typeface = ResourcesCompat.getFont(mContext, R.font.poppins_regular);
        holder.mTextView.setTypeface(typeface);
        holder.imageView.setImageResource(menu.image);
        if (position==4){
            holder.expandImage.setVisibility(View.VISIBLE);
        }
        //if the position is equals to the item position which is to be expanded
        if (currentPosition == position) {
            //creating an animation
            Animation slideDown = AnimationUtils.loadAnimation(mContext, R.anim.slide_down);

            //toggling visibility
           // holder.linearLayout.setVisibility(View.VISIBLE);

            //adding sliding effect
          //  holder.linearLayout.startAnimation(slideDown);
        }
        holder.expandImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //getting the position of the item to expand it
                currentPosition = position;

                //reloding the list
                notifyDataSetChanged();
            }
        });

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int position = holder.getAdapterPosition();
                /*final Intent intent;
                if (position == 0){
                    intent =  new Intent(context, ShowDetails.class);
                } else if (position == 1){
                    intent =  new Intent(context, AllDetails.class);
                } else {
                    intent =  new Intent(context, RecentDetails.class);
                }
                context.startActivity(intent);*/
                //utility.showToast(mContext,"profileName: "+menu.profileName);
                if (position==0) {
                    menuListener.menuClick("EditProfile");
                }else if (position==1){
                    menuListener.menuClick("MyUploads");
                }else if (position==2){
                    menuListener.menuClick("MyLikes");
                }else if (position==3){
                    menuListener.menuClick("PremiumUser");
                }else if (position==4){
                    menuListener.menuClick("TodayLike");
                }else if (position==5){
                    menuListener.menuClick("BlockedUsers");
                }else if (position==6){
                    menuListener.menuClick("TermsOfUse");
                }else if (position==7){
                    menuListener.menuClick("PrivacyPolicy");
                }else if (position==8){
                    menuListener.menuClick("ContactUs");
                }else if (position==9){
                    menuListener.menuClick("Logout");
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return menuData.size();
    }


    public static class MyViewHolder extends RecyclerView.ViewHolder{

        public CardView mCardView;
        public TextView mTextView;
        public ImageView imageView,expandImage;

        public MyViewHolder(View v){
            super(v);

            mCardView = (CardView) v.findViewById(R.id.profile_menu_layout);
            mTextView = (TextView) v.findViewById(R.id.text_name_tv);
            imageView = (ImageView) v.findViewById(R.id.menu_image);
            expandImage=(ImageView) v.findViewById(R.id.expand_imageView);
        }
    }
    public interface profileMenuListener{
        public Void menuClick(String menuName);
    }
}
