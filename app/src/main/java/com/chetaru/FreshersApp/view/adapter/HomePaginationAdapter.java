package com.chetaru.FreshersApp.view.adapter;

import android.animation.ObjectAnimator;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.os.Build;
import android.os.StrictMode;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.target.Target;
import com.bumptech.glide.request.transition.Transition;
import com.chetaru.FreshersApp.R;
import com.chetaru.FreshersApp.service.model.UserViewPost;
import com.chetaru.FreshersApp.utility.SessionParam;
import com.chetaru.FreshersApp.utility.Utility;
import com.chetaru.FreshersApp.utility.Validations;
import com.chetaru.FreshersApp.view.callback.PaginationListenerCallback;
import com.chetaru.FreshersApp.view.customView.BlurBuilder;
import com.chetaru.FreshersApp.view.customView.BlurCustom;
import com.chetaru.FreshersApp.view.customView.ExpandableTextView;
import com.jackandphantom.blurimage.BlurImage;
import com.luseen.autolinklibrary.AutoLinkMode;
import com.luseen.autolinklibrary.AutoLinkOnClickListener;
import com.luseen.autolinklibrary.AutoLinkTextView;
import com.squareup.picasso.Picasso;
import com.tylersuehr.socialtextview.SocialTextView;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.List;
import java.util.Objects;


import de.hdodenhof.circleimageview.CircleImageView;
import jp.wasabeef.picasso.transformations.BlurTransformation;
import jp.wasabeef.picasso.transformations.gpu.BrightnessFilterTransformation;
import jp.wasabeef.picasso.transformations.gpu.KuwaharaFilterTransformation;
import jp.wasabeef.picasso.transformations.gpu.PixelationFilterTransformation;
import jp.wasabeef.picasso.transformations.gpu.SwirlFilterTransformation;
import jp.wasabeef.picasso.transformations.gpu.ToonFilterTransformation;
import jp.wasabeef.picasso.transformations.gpu.VignetteFilterTransformation;

import static com.bumptech.glide.request.RequestOptions.bitmapTransform;
import static com.chetaru.FreshersApp.utility.Utility.getBitmapFromURL;



public class HomePaginationAdapter extends RecyclerView.Adapter<BaseViewHolder> {
    private static final int VIEW_TYPE_LOADING = 0;
    public static final int VIEW_TYPE_USER = 2;
    public static final int VIEW_TYPE_ADMIN = 1;
    private boolean isLoaderVisible = false;
    private List<UserViewPost> mNewsItemList;
    private Callback mCallback;
    Context mContext;
    Boolean click=true,expand=false;
    Utility utility;
    SessionParam sessionParam;
    //private PaginationListener listener;
    PaginationListenerCallback listener;
    ExpandableTextView expandableTextView;
    boolean isLocationShow = false;
    int maxline=0,height=0;
    private int screenWidth, screenHeight;
    Display display=null;
    public static final int selected_position = 0;
    Integer userId;
    Boolean premiumUser;
    String userRole;
    Bitmap blurredBitmap;
    private BlurCustom blurCustom;
    int sensitivePostsAllowed=2;
    int sensitivePost=2;
    private static final float BLUR_RADIUS = 25F;


    public HomePaginationAdapter(List<UserViewPost> news, Context mContext, PaginationListenerCallback listener) {
        this.mNewsItemList = news;
        this.mContext=mContext;
        utility=new Utility();
        this.listener=listener;
        this.expandableTextView=new ExpandableTextView(mContext);
        sessionParam=new SessionParam(mContext);
        userId=(sessionParam.id);
        userRole=sessionParam.role;
        sensitivePostsAllowed=sessionParam.sensitivePostsAllowed;
        premiumUser=sessionParam.premieUser;
        //premiumUser=true;
        blurCustom = new BlurCustom(mContext, BLUR_RADIUS);
      /* display =mContext.getWindowManager().getDefaultDisplay();
        screenWidth = display.getWidth();
        screenHeight = display.getHeight();*/

    }
    public void setCallback(Callback callback) {
        mCallback = callback;
    }

    @NonNull
    @Override
    public BaseViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (isLocationShow) {

            switch (viewType) {

                case VIEW_TYPE_USER:
                    return new ViewHolder(
                            LayoutInflater.from(parent.getContext()).inflate(R.layout.home_userpost_list, parent, false));
                case VIEW_TYPE_LOADING:
                    return new EmptyViewHolder(
                            LayoutInflater.from(parent.getContext()).inflate(R.layout.home_location_layout, parent, false));
                case VIEW_TYPE_ADMIN:
                    return new AdminViewHolder(
                            LayoutInflater.from(parent.getContext()).inflate(R.layout.admin_post_list, parent, false));
                default:
                    return null;
            }
        }else {
            return new ViewHolder(
                    LayoutInflater.from(parent.getContext()).inflate(R.layout.home_userpost_list, parent, false));
        }
    }
    @Override
    public void onBindViewHolder(@NonNull BaseViewHolder holder, int position) {
        holder.onBind(position);
    }
    @Override
    public int getItemViewType(int position) {
       /* if (isLoaderVisible) {
                return position == mNewsItemList.size() ? VIEW_TYPE_LOADING : VIEW_TYPE_NORMAL;
        } else {
            return VIEW_TYPE_NORMAL;
        }*/
       try {
           if (mNewsItemList.get(position).getRoleId()!=null) {
               if (mNewsItemList.get(position).getRoleId() == 1) {
                   return VIEW_TYPE_ADMIN;
               } else if (mNewsItemList.get(position).getRoleId() == 2) {
                   return VIEW_TYPE_USER;
               } else {
                   return (position == mNewsItemList.size()) ? VIEW_TYPE_LOADING : VIEW_TYPE_USER;
               }
           }else {
               return (position == mNewsItemList.size()) ? VIEW_TYPE_LOADING : VIEW_TYPE_USER;
           }
       }catch (Exception e){
           return VIEW_TYPE_LOADING;
       }

    }
   /* @Override
    public int getItemViewType(int position) {
       if (isPositionFooter(position)) {
            return VIEW_TYPE_LOADING;
        }
        return VIEW_TYPE_NORMAL;
    }*/

    private  boolean isPositionHeader(int position) {
        return position == 0;
    }

    private  boolean isPositionFooter(int position) {
        return position == mNewsItemList.size()-1;
    }

    @Override
    public int getItemCount() {
        try {
            return mNewsItemList == null ? 0 : mNewsItemList.size() + 1;

           /* if (isLocationShow) {
            }else {
                return mNewsItemList == null ? 0 : mNewsItemList.size();

            }*/
        }catch (Exception e){
            e.printStackTrace();
            return 0;
        }
    }

    public void setLike(UserViewPost postItems) {
       // postItems.getId().compareTo(mNewsItemList.get(AdapterPo))

    }
    public void addFirst(UserViewPost userViewPost) {
        mNewsItemList.add(0,userViewPost);
    }
    public void add(UserViewPost response) {
        mNewsItemList.add(response);
        notifyItemInserted(mNewsItemList.size() - 1);
    }
    public void addAll(List<UserViewPost> postItems) {
        for (UserViewPost response : postItems) {
            add(response);
        }
    }
    private void remove(UserViewPost postItems) {
        int position = mNewsItemList.indexOf(postItems);
        if (position > -1) {
            mNewsItemList.remove(position);
            notifyItemRemoved(position);
        }
    }
    public void showLocation(){
        isLocationShow=true;
    }
    public void removeLocation(){
        isLocationShow=true;
    }
    public void addLoading() {
        isLoaderVisible = true;
       // add(new UserViewPost());
        //notifyItemInserted(mNewsItemList.size() - 1);
    }
    public void removeLoading() {
        isLoaderVisible = false;
       /* int position = mNewsItemList.size() - 1;
        UserViewPost item = getItem(position);
        if (item != null) {
            mNewsItemList.remove(position);
            notifyItemRemoved(position);
        }*/
    }
    public void clear() {
        try {
            while (getItemCount() > 0) {
                remove(getItem(0));
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }
    UserViewPost getItem(int position) {
            return mNewsItemList.get(position);

    }



    public interface Callback {
        void onRepoEmptyViewRetryClick();
    }


    //handel user post Layout view
    public class ViewHolder extends BaseViewHolder {

        SocialTextView hashTextView;
        ImageView newsImage,imageUpper,imageSceond,linkConnectionImage;
        ImageView likeImage,replyImage;
       public ImageView imageShowMore;
        CircleImageView iv_logo;
        TextView senPostMsg;
       TextView homeUserTextView,dateTextView,likeCountTxt,originalPostTextView;
        LinearLayout layoutMoreTxt,changeLayout,connection_link_layout,likeCountLayout,reply_link_layout;
        LinearLayout root_layout,headerLayout,like_layout,OriginalPostLayout;
        public ViewHolder(View itemView) {
            super(itemView);
            iv_logo = itemView.findViewById(R.id.iv_logo);
            hashTextView = itemView.findViewById(R.id.social_text_view);
            homeUserTextView = itemView.findViewById(R.id.home_user_name_text);
            changeLayout = itemView.findViewById(R.id.set_height_layout);
            imageShowMore = itemView.findViewById(R.id.image_show_more);
            layoutMoreTxt = itemView.findViewById(R.id.layout_showmore_txt);
            newsImage = itemView.findViewById(R.id.upload_image_public);
            imageUpper = itemView.findViewById(R.id.upload_image_upper);
            imageSceond = itemView.findViewById(R.id.upload_image_sceond);
            likeImage = itemView.findViewById(R.id.like_image);
            linkConnectionImage = itemView.findViewById(R.id.link_image);
            replyImage = itemView.findViewById(R.id.reply_image);
            dateTextView = itemView.findViewById(R.id.upload_image_date_txt);
            senPostMsg = itemView.findViewById(R.id.post_msg_view);
            likeCountLayout = itemView.findViewById(R.id.like_count_layout);
            OriginalPostLayout = itemView.findViewById(R.id.view_original_post_layout);
            originalPostTextView = itemView.findViewById(R.id.view_original_post_txt);

            likeCountTxt = itemView.findViewById(R.id.like_count_txt);
            connection_link_layout = itemView.findViewById(R.id.connection_link_layout);
            reply_link_layout = itemView.findViewById(R.id.reply_link_layout);
            root_layout = itemView.findViewById(R.id.root_layout);
            headerLayout = itemView.findViewById(R.id.post_top_layout);
            like_layout = itemView.findViewById(R.id.like_layout);
            //newTitle.getViewTreeObserver().removeOnGlobalLayoutListener(this::clear);
            /*final View content = itemView.findViewById(android.R.id.content).getRootView();
            if (content.getWidth() > 0) {
                Bitmap image = BlurBuilder.blur(content);
                imageSceond.setBackgroundDrawable(new BitmapDrawable(mContext.getResources(), image));
            } else {
                content.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                    @Override
                    public void onGlobalLayout() {
                        Bitmap image = BlurBuilder.blur(content);
                        imageSceond.setBackgroundDrawable(new BitmapDrawable(mContext.getResources(), image));
                    }
                });
            }*/


        }
        protected void clear() {
        }
        public void onBind(int position) {
            super.onBind(position);
            try {
                UserViewPost mNewsItem = mNewsItemList.get(position);
                if (mNewsItem.getId()!=null) {

                    if(!Validations.isEmptyString(mNewsItem.getCaption())) {
                        hashTextView.setLinkText(mNewsItem.getCaption());
                    }
                   /* if(userRole.equals("2")){
                        setUserBackground();
                    }else{
                        setAdminBackground();
                    }*/

                    try {
                        //1 sensitive , 2 not sensitive
                        sensitivePost=mNewsItem.getSensitivePostStatus();
                        Glide.with(mContext).load(mNewsItem.getUserImage()).placeholder(R.drawable.profile_pics).into(iv_logo);
                        homeUserTextView.setText(mNewsItem.getName());
                        likeImage.setVisibility(View.VISIBLE);
                        senPostMsg.setVisibility(View.GONE);
                       // Picasso.with(mContext).load(mNewsItem.getUserImage()).into(iv_logo);
                        if (userId!=mNewsItem.getUserId()) {
                            if (sensitivePostsAllowed == 1) {
                                senPostMsg.setVisibility(View.GONE);
                                /*Picasso.with(mContext).load(mNewsItem.getImageUrl())
                                        .into(imageUpper);*/
                                Glide.with(mContext).load(mNewsItem.getImageUrl())
                                        .placeholder(R.drawable.profile_pics)
                                        .into(imageUpper);
                                Thread thread = new Thread(new Runnable() {
                                    @Override
                                    public void run() {
                                        Bitmap bitmapFromURL = Utility.getBitmapFromURL(mNewsItem.getImageUrl());
                                        BlurImage.with(mContext).load(bitmapFromURL).intensity(25).Async(true).into(imageSceond);
                                        BlurImage.with(mContext).load(bitmapFromURL).intensity(25).Async(true).into(newsImage);

                                    }
                                });

                                thread.start();
                            } else {
                                if (sensitivePost == 1) {
                                    senPostMsg.setVisibility(View.VISIBLE);
                                    Thread thread = new Thread(new Runnable() {
                                        @Override
                                        public void run() {
                                            Bitmap bitmapFromURL = Utility.getBitmapFromURL(mNewsItem.getImageUrl());
                                            BlurImage.with(mContext).load(bitmapFromURL).intensity(25).Async(true).into(imageSceond);
                                            BlurImage.with(mContext).load(bitmapFromURL).intensity(25).Async(true).into(newsImage);

                                        }
                                    });

                                    thread.start();

                                } else {
                                    senPostMsg.setVisibility(View.GONE);
                                    /*Picasso.with(mContext).load(mNewsItem.getImageUrl())
                                            .into(imageUpper);*/
                                    Glide.with(mContext).load(mNewsItem.getImageUrl()).placeholder(R.drawable.profile_pics)
                                            .into(imageUpper);
                                    Thread thread = new Thread(new Runnable() {
                                        @Override
                                        public void run() {
                                            Bitmap bitmapFromURL = Utility.getBitmapFromURL(mNewsItem.getImageUrl());
                                            BlurImage.with(mContext).load(bitmapFromURL).intensity(25).Async(true).into(imageSceond);
                                            BlurImage.with(mContext).load(bitmapFromURL).intensity(25).Async(true).into(newsImage);

                                        }
                                    });

                                    thread.start();
                                }
                            }
                        }else {
                            senPostMsg.setVisibility(View.GONE);
                                /*Picasso.with(mContext).load(mNewsItem.getImageUrl())
                                        .into(imageUpper);*/
                            Glide.with(mContext).load(mNewsItem.getImageUrl()).placeholder(R.drawable.profile_pics)
                                    .into(imageUpper);
                            Thread thread = new Thread(new Runnable() {
                                @Override
                                public void run() {
                                    Bitmap bitmapFromURL = Utility.getBitmapFromURL(mNewsItem.getImageUrl());
                                    BlurImage.with(mContext).load(bitmapFromURL).intensity(25).Async(true).into(imageSceond);
                                    BlurImage.with(mContext).load(bitmapFromURL).intensity(25).Async(true).into(newsImage);

                                }
                            });

                            thread.start();
                        }


                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    try{
                        if (mNewsItem.getOriginalPostId()!=0){
                            originalPostTextView.setVisibility(View.VISIBLE);
                            OriginalPostLayout.setVisibility(View.VISIBLE);
                        }else {
                            originalPostTextView.setVisibility(View.GONE);
                            OriginalPostLayout.setVisibility(View.GONE);
                        }
                        originalPostTextView.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                listener.onPaginationClick(mNewsItem, click, position, "originalPost");
                            }
                        });
                        headerLayout.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                listener.onPaginationClick(mNewsItem,false,position,"headerLayout");
                            }
                        });
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                    try{
                        String changeDate=utility.getPostUploadDate(mNewsItem.getPostedDate());
                        dateTextView.setText(changeDate);
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                    try {
                        if (!Validations.isEmptyString(mNewsItem.getName())) {
                            homeUserTextView.setText(mNewsItem.getName());
                            homeUserTextView.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    listener.onPaginationClick(mNewsItem, click, position, "tagName");
                                }
                            });
                        }


                        hashTextView.setOnLinkClickListener(new SocialTextView.OnLinkClickListener() {
                            @Override
                            public void onLinkClicked(int linkType, String matchedText) {
                                //linkType 1=hashTag,2==mention
                                listener.autoLinkListener(linkType, "", matchedText, mNewsItem);
                            }
                        });


                        changeLayout.post(new Runnable() {
                            @Override
                            public void run() {
                                //maybe also works height = ll.getLayoutParams().height;
                                height = changeLayout.getHeight();
                            }
                        });

                        hashTextView.setLines(2);
                        hashTextView.post(new Runnable() {
                            @Override
                            public void run() {
                                Log.v("Line count: ", hashTextView.getLineCount() + "");
                                maxline = hashTextView.getLineCount();
                               /* if (maxline > 3) {
                                    imageShowMore.setVisibility(View.VISIBLE);
                                    //showMoreTxt.setVisibility(View.VISIBLE);
                                } else {
                                    imageShowMore.setVisibility(View.GONE);
                                    //showMoreTxt.setVisibility(View.GONE);
                                }*/
                            }
                        });
                        /*imageShowMore.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                if (!expand){
                                    expand=true;
                                    dateTextView.setVisibility(View.VISIBLE);
                                    imageShowMore.setImageResource(R.drawable.ic_expand_more_white_24dp);

                                    ObjectAnimator animation = ObjectAnimator.ofInt(hashTextView, "maxLines", hashTextView.getMaxLines());
                                    animation.setDuration(100).start();
                                    ViewGroup.LayoutParams params = (ViewGroup.LayoutParams) changeLayout.getLayoutParams();
                                    int height_in_pixels = hashTextView.getLineCount() * hashTextView.getLineHeight();
                                    int maxLineCount=hashTextView.getMaxLines();
                                    int lineCount=hashTextView.getLineCount();
                                    int minLineCount=hashTextView.getMinLines();
                                    int minLineHeight=hashTextView.getLineHeight();
                                    int minMaxHeight=hashTextView.getMaxHeight();
                                    int minMinHeight=hashTextView.getMinHeight();
                                    final int sdk = android.os.Build.VERSION.SDK_INT;
                                    if (sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
                                        params.height=height*2;
                                        if (params.height<height_in_pixels){
                                            params.height=params.height+height_in_pixels;
                                        }

                                    }else if (sdk< Build.VERSION_CODES.O){
                                        if (params.height<0){
                                            params.height=height_in_pixels+((height_in_pixels + 100) / 2);

                                        }else {
                                            if (height > height_in_pixels) {
                                                params.height = height +height_in_pixels;
                                            } else if (params.height>height_in_pixels){
                                                params.height = (params.height) + ((height_in_pixels + 100) / 2);
                                            }else {
                                                params.height=height+(height_in_pixels + 150);
                                            }
                                        }
                                    }else {
                                        if (height > height_in_pixels) {
                                            params.height = height +height_in_pixels;
                                        } else {
                                            // params.height = (height*2)+((height_in_pixels+100)/2);
                                            params.height=height_in_pixels+((height_in_pixels + 100) / 2);

                                        }
                                    }

                                    changeLayout.setLayoutParams(params);


                                }else {
                                    expand=false;
                                    dateTextView.setVisibility(View.GONE);
                                    imageShowMore.setImageResource(R.drawable.ic_expand_less_white_24dp);

                                    ObjectAnimator animation = ObjectAnimator.ofInt(hashTextView, "maxLines", 2);
                                    animation.setDuration(100).start();
                                    ViewGroup.LayoutParams params = (ViewGroup.LayoutParams) changeLayout.getLayoutParams();
                                    int height_in_pixels = hashTextView.getLineCount() * hashTextView.getLineHeight();
                                    int maxLineCount=hashTextView.getMaxLines();
                                    int lineCount=hashTextView.getLineCount();
                                    int minLineCount=hashTextView.getMinLines();
                                    params.height=height_in_pixels;

                                    changeLayout.setLayoutParams(params);
                                }
                            }
                        });*/


                        imageShowMore.setOnClickListener(new View.OnClickListener() {
                            public void onClick(View v) {
                                if (!expand){
                                    expand=true;
                                    dateTextView.setVisibility(View.VISIBLE);
                                    imageShowMore.setImageResource(R.drawable.ic_expand_more_white_24dp);

                                    ObjectAnimator animation = ObjectAnimator.ofInt(hashTextView, "maxLines", 40);
                                    animation.setDuration(100).start();
                                    int height_in_pixels = hashTextView.getLineCount() * hashTextView.getLineHeight();
                                    LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) changeLayout.getLayoutParams();
                                    int maxLineCount=hashTextView.getMaxLines();
                                    int lineCount=hashTextView.getLineCount();
                                    int minLineCount=hashTextView.getMinLines();
                                     if (height > height_in_pixels) {
                                        params.height = height +height_in_pixels;
                                    } else {
                                       // params.height = (height*2)+((height_in_pixels+100)/2);
                                        params.height = (height)+((height_in_pixels+100)/2);
                                    }
                                    final int sdk = android.os.Build.VERSION.SDK_INT;
                                    if (sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
                                        params.height=height*2;
                                        if (params.height<height_in_pixels){
                                            params.height=params.height+height_in_pixels;
                                        }

                                    }else if (sdk< Build.VERSION_CODES.O){
                                        if (params.height<0){
                                            params.height=height_in_pixels+((height_in_pixels + 100) / 2);

                                        }else {
                                            if (height > height_in_pixels) {
                                                params.height = height +height_in_pixels;
                                            } else if (params.height>height_in_pixels){
                                                params.height = (params.height) + ((height_in_pixels + 100) / 2);
                                            }else {
                                                params.height=height+(height_in_pixels + 150);
                                            }
                                        }
                                    }else {
                                         if (height > height_in_pixels) {
                                            params.height = height +height_in_pixels;
                                        } else {
                                            // params.height = (height*2)+((height_in_pixels+100)/2);
                                             params.height=height_in_pixels+((height_in_pixels + 100) / 2);

                                         }
                                    }

                                    changeLayout.setLayoutParams(params);
                                }else{
                                    expand=false;
                                    dateTextView.setVisibility(View.GONE);
                                    imageShowMore.setImageResource(R.drawable.ic_expand_less_white_24dp);

                                    ObjectAnimator animation = ObjectAnimator.ofInt(hashTextView, "maxLines", 3);
                                    animation.setDuration(100).start();
                                    ViewGroup.LayoutParams params = (ViewGroup.LayoutParams) changeLayout.getLayoutParams();
                                    int height_in_pixels = hashTextView.getLineCount() * hashTextView.getLineHeight();
                                    int maxLineCount=hashTextView.getMaxLines();
                                    int lineCount=hashTextView.getLineCount();
                                    int minLineCount=hashTextView.getMinLines();

                                    final int sdk = android.os.Build.VERSION.SDK_INT;
                                    if (sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
                                        params.height=params.height+height_in_pixels;

                                    }else if (sdk< Build.VERSION_CODES.O){
                                        params.height=height_in_pixels+5;
                                        if (params.height<0){
                                            params.height=height_in_pixels;

                                        }else {
                                            if (height > height_in_pixels) {
                                                params.height = height +height_in_pixels;
                                            } else {
                                                if(height<=0){
                                                    params.height=height_in_pixels;
                                                }else {
                                                    params.height = (height);
                                                }
                                            }
                                        }
                                    }else {
                                        params.height = height;
                                    }
                                    changeLayout.setLayoutParams(params);

                                }
                               // expand = !expand;
                                //advancedView.setVisibility( isExpanded ?  View.VISIBLE : View.GONE );
                                //expandCtrl.setCompoundDrawablesWithIntrinsicBounds( isExpanded ? icon_collapse : icon_expand, 0, 0, 0 );


                            }
                        });

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
           /* if (newTitle.getLineCount() > 3) {
                btShowMore.setVisibility(View.VISIBLE);
            }else {
                btShowMore.setVisibility(View.GONE);
            }

            btShowMore.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (!expand) {
                        if (newTitle.getLineCount() > 3) {
                            ObjectAnimator animation = ObjectAnimator.ofInt(newTitle, "maxLines", 4);
                            animation.setDuration(0).start();
                        }
                        expand = true;
                        newTitle.setMaxLines(Integer.MAX_VALUE);
                    }else{
                        expand = false;
                        newTitle.setMaxLines(3);

                    }


                }
            });*/
                    try {
                        Log.d("userName", "session: " + userId + " " + "viewPost: " + mNewsItem.getName().toLowerCase().trim());

                        if (!(userId.equals(mNewsItem.getUserId()))) {
                            linkConnectionImage.setVisibility(View.VISIBLE);
                            replyImage.setVisibility(View.VISIBLE);
                            connection_link_layout.setVisibility(View.VISIBLE);
                            reply_link_layout.setVisibility(View.VISIBLE);
                            likeCountLayout.setVisibility(View.GONE);
                            likeCountTxt.setVisibility(View.GONE);


                        }else {
                            linkConnectionImage.setVisibility(View.GONE);
                            replyImage.setVisibility(View.GONE);
                            connection_link_layout.setVisibility(View.GONE);
                            reply_link_layout.setVisibility(View.GONE);
                            likeCountTxt.setVisibility(View.VISIBLE);
                            likeCountLayout.setVisibility(View.VISIBLE);

                           /* ViewGroup.MarginLayoutParams marginParams = new ViewGroup.MarginLayoutParams(imageShowMore.getLayoutParams());
                            marginParams.setMargins(0, 0, 4, 0);
                            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(marginParams);
                            imageShowMore.setLayoutParams(layoutParams);*/

                                //show like count user list
                                likeCountTxt.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        if (premiumUser) {
                                            listener.onPaginationClick(mNewsItem, click, position, "likeCount");
                                        }else {
                                            listener.onPaginationClick(mNewsItem,click,position,"premiumUpgrade");
                                        }
                                    }
                                });


                        }


                            final int sdk = android.os.Build.VERSION.SDK_INT;

                            if (mNewsItem.getLikedStatus()) {
                                click = false;
                                if (sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
                                    likeImage.setBackgroundDrawable(ContextCompat.getDrawable(mContext, R.mipmap.admin_post_like));
                                } else {
                                    likeImage.setBackground(ContextCompat.getDrawable(mContext, R.mipmap.admin_post_like));
                                }
                            } else {
                                click = true;
                                if (sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
                                    likeImage.setBackgroundDrawable(ContextCompat.getDrawable(mContext, R.mipmap.like_primary_color));
                                } else {
                                    likeImage.setBackground(ContextCompat.getDrawable(mContext, R.mipmap.like_primary_color));
                                }
                            }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    try{
                        if (mNewsItem.getLikedCount()>0) {
                            likeCountTxt.setVisibility(View.VISIBLE);
                            likeCountTxt.setText(mNewsItem.getLikedCount().toString() + " Likes");
                        }else {
                            likeCountTxt.setVisibility(View.VISIBLE);
                            likeCountTxt.setText( "No Likes");
                        }
                    }catch (Exception e){
                        e.printStackTrace();
                    }

                    linkConnectionImage.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            listener.onPaginationClick(mNewsItem, click, position, "connection");
                        }
                    });
                    replyImage.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            listener.onPaginationClick(mNewsItem,click,position,"reply");
                        }
                    });
                    likeImage.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {


                            //likeImage.setBackground(click ?ContextCompat.getDrawable(mContext, R.drawable.like_true):ContextCompat.getDrawable(mContext, R.drawable.like));
                            if (userId.equals(mNewsItem.getUserId())){
                                if (premiumUser) {
                                    listener.onPaginationClick(mNewsItem, click, position, "likeCount");
                                }else {
                                    listener.onPaginationClick(mNewsItem,click,position,"premiumUpgrade");
                                }

                            }else {
                                listener.onPaginationClick(mNewsItem, click, position, "like");
                                final int sdk = android.os.Build.VERSION.SDK_INT;
                                if (click) {
                                    click = false;
                                    if (sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
                                        likeImage.setBackgroundDrawable(ContextCompat.getDrawable(mContext, R.mipmap.admin_post_like));
                                    } else {
                                        likeImage.setBackground(ContextCompat.getDrawable(mContext, R.mipmap.admin_post_like));
                                    }
                                } else {
                                    click = true;
                                    if (sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
                                        likeImage.setBackgroundDrawable(ContextCompat.getDrawable(mContext, R.mipmap.like_primary_color));
                                    } else {
                                        likeImage.setBackground(ContextCompat.getDrawable(mContext, R.mipmap.like_primary_color));
                                    }
                                }
                            }
                        }
                    });


                    itemView.setOnLongClickListener(new View.OnLongClickListener() {
                        @Override
                        public boolean onLongClick(View view) {
                            //utility.showToast(mContext,"Long Click");
                            if (!userId.equals(mNewsItem.getUserId()))
                            listener.onPaginationLongClick(mNewsItem, position,"user");
                            return false;
                        }
                    });


                    //newTitle.setText(mNewsItem.getCaption());


                }
            }catch (Exception e){
                e.printStackTrace();
            }

        }
        private void setUserBackground() {
            final int sdk = android.os.Build.VERSION.SDK_INT;

            root_layout.setBackgroundColor(ContextCompat.getColor(mContext, R.color.colorPrimary));
            headerLayout.setBackgroundColor(ContextCompat.getColor(mContext, R.color.colorPrimary));
            homeUserTextView.setTextColor(ContextCompat.getColor(mContext, R.color.white));

            hashTextView.setTextColor(ContextCompat.getColor(mContext, R.color.white));

            if (sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
                //imageShowMore.setBackgroundDrawable(ContextCompat.getDrawable(mContext, R.drawable.ic_expand_less_white_24dp));
                imageShowMore.setImageResource(R.drawable.ic_expand_less_white_24dp);

                likeImage.setBackgroundDrawable(ContextCompat.getDrawable(mContext, R.mipmap.like_primary_color));
            } else {
                //imageShowMore.setBackground(ContextCompat.getDrawable(mContext, R.drawable.ic_expand_less_white_24dp));
                imageShowMore.setImageResource(R.drawable.ic_expand_less_white_24dp);
                likeImage.setBackground(ContextCompat.getDrawable(mContext, R.mipmap.like_primary_color));
            }

        }

        //set Admin background
        private void setAdminBackground() {
            root_layout.setBackgroundColor(ContextCompat.getColor(mContext, R.color.white));
            headerLayout.setBackgroundColor(ContextCompat.getColor(mContext, R.color.white));
            like_layout.setBackgroundColor(ContextCompat.getColor(mContext, R.color.white));
            homeUserTextView.setTextColor(ContextCompat.getColor(mContext, R.color.black));
            hashTextView.setBackgroundColor(ContextCompat.getColor(mContext, R.color.white));
            hashTextView.setTextColor(ContextCompat.getColor(mContext, R.color.black));

            final int sdk = android.os.Build.VERSION.SDK_INT;

            if (sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
                //imageShowMore.setBackgroundDrawable(ContextCompat.getDrawable(mContext, R.drawable.ic_baseline_expand_less_24));
                imageShowMore.setImageResource(R.drawable.ic_baseline_expand_less_24);
                likeImage.setBackgroundColor(ContextCompat.getColor(mContext, R.color.colorPrimaryDark));
                //Picasso.with(mContext).load(R.drawable.like).into(likeImage);
                Glide.with(mContext).load(R.drawable.like).placeholder(R.drawable.profile_pics).into(likeImage);
            } else {
               // likeImage.setBackground(ContextCompat.getDrawable(mContext, R.drawable.like_true));
                /*Picasso.with(mContext).load("http//drawable.data").placeholder(R.mipmap.like_primary_color)   // optional
                        .error(R.mipmap.like_primary_color) .into(likeImage);*/
                Glide.with(mContext).load("http//drawable.data").placeholder(R.mipmap.like_primary_color)   // optional
                        .error(R.mipmap.like_primary_color) .into(likeImage);
                likeImage.setBackgroundColor(ContextCompat.getColor(mContext, R.color.colorPrimaryDark));
                //imageShowMore.setBackground(ContextCompat.getDrawable(mContext, R.drawable.ic_baseline_expand_less_24));
                imageShowMore.setImageResource(R.drawable.ic_baseline_expand_less_24);
            }


        }

    }

    //handel location layout view
    public class EmptyViewHolder extends BaseViewHolder {
        Button restButton,locationButton;
        public EmptyViewHolder(View itemView) {
            super(itemView);
            restButton=itemView.findViewById(R.id.pagination_restart_button);
            locationButton=itemView.findViewById(R.id.pagination_location_button);
        }

        @Override
        public void onBind(int position) {
            super.onBind(position);
            restButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    // view.position=selected_position;
                    listener.ResetClickListener(position,"");
                }
            });
            locationButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    listener.ResetClickListener(position,"locationChange");
                }
            });
        }

        @Override
        protected void clear() {
        }
    }

    //handel Admin post layout view
    public  class AdminViewHolder extends BaseViewHolder {


        SocialTextView AdminHashTextView;
        ImageView AdminNewsImage,AdminImageUpper,AdminImageSceond;
        public CircleImageView AdminLikeImage;
        public ImageView adminImageShowMore;

        TextView senPostMsg;
        CircleImageView admin_iv_logo;
        TextView adminHeaderTextView,adminDateTextView,adminLikeCountTxt;
        LinearLayout layoutMoreTxt,changeLayout,socialParentLayout;
        LinearLayout root_layout,headerLayout,like_layout;

        public AdminViewHolder(View itemView) {
            super(itemView);
            root_layout = itemView.findViewById(R.id.root_layout);
            headerLayout = itemView.findViewById(R.id.header_top_layout);
            admin_iv_logo = itemView.findViewById(R.id.iv_logo);
            adminHeaderTextView = itemView.findViewById(R.id.home_admin_name_text);
            AdminNewsImage = itemView.findViewById(R.id.upload_image_public);
            AdminImageUpper = itemView.findViewById(R.id.upload_image_upper);
            AdminImageSceond = itemView.findViewById(R.id.upload_image_sceond);
            changeLayout = itemView.findViewById(R.id.admin_bottom_parent_layout);
            socialParentLayout = itemView.findViewById(R.id.admin_social_text_parent_layout);



            adminDateTextView = itemView.findViewById(R.id.admin_post_date_txt);
            AdminHashTextView = itemView.findViewById(R.id.admin_social_text_view);
            like_layout = itemView.findViewById(R.id.like_layout);
            adminImageShowMore = itemView.findViewById(R.id.image_show_more);
            AdminLikeImage = itemView.findViewById(R.id.like_image);
            adminLikeCountTxt = itemView.findViewById(R.id.like_count_txt);
            senPostMsg = itemView.findViewById(R.id.admin_post_msg_view);
        }

        @Override
        public void onBind(int position) {
            super.onBind(position);
            try {
                UserViewPost mNewsItem = mNewsItemList.get(position);
                if (mNewsItem.getId()!=null) {

                    try {
                        sensitivePost=mNewsItem.getSensitivePostStatus();
                        //Picasso.with(mContext).load(mNewsItem.getUserImage()).into(admin_iv_logo);
                        Glide.with(mContext).load(mNewsItem.getUserImage()).placeholder(R.drawable.profile_pics).into(admin_iv_logo);
                        senPostMsg.setVisibility(View.GONE);

                       /* Picasso.with(mContext).load(mNewsItem.getImageUrl())
                                .into(AdminImageUpper);*/
                        Glide.with(mContext).load(mNewsItem.getImageUrl()).placeholder(R.drawable.profile_pics)
                                .into(AdminImageUpper);
                        Thread thread = new Thread(new Runnable(){
                            @Override
                            public void run() {
                                Bitmap bitmapFromURL=Utility.getBitmapFromURL(mNewsItem.getImageUrl());
                                BlurImage.with(mContext).load(bitmapFromURL).intensity(25).Async(true).into(AdminImageSceond);
                                BlurImage.with(mContext).load(bitmapFromURL).intensity(25).Async(true).into(AdminNewsImage);

                            }
                        });

                        thread.start();
                        /*if (sensitivePostsAllowed==1) {
                            senPostMsg.setVisibility(View.GONE);

                            Picasso.get().load(mNewsItem.getImageUrl())
                                    .into(AdminImageUpper);
                            Thread thread = new Thread(new Runnable(){
                                @Override
                                public void run() {
                                    Bitmap bitmapFromURL=Utility.getBitmapFromURL(mNewsItem.getImageUrl());
                                    BlurImage.with(mContext).load(bitmapFromURL).intensity(25).Async(true).into(AdminImageSceond);
                                    BlurImage.with(mContext).load(bitmapFromURL).intensity(25).Async(true).into(AdminNewsImage);

                                }
                            });

                            thread.start();
                        }else{
                            if (sensitivePost==2){
                                senPostMsg.setVisibility(View.VISIBLE);
                                *//*Picasso.get().load(mNewsItem.getImageUrl()).centerCrop()
                                        .into(AdminImageUpper);*//*
                                Thread thread = new Thread(new Runnable(){
                                    @Override
                                    public void run() {
                                        Bitmap bitmapFromURL=Utility.getBitmapFromURL(mNewsItem.getImageUrl());
                                        BlurImage.with(mContext).load(bitmapFromURL).intensity(25).Async(true).into(AdminImageSceond);
                                        BlurImage.with(mContext).load(bitmapFromURL).intensity(25).Async(true).into(AdminNewsImage);
                                        BlurImage.with(mContext).load(bitmapFromURL).intensity(25).Async(true).into(AdminImageUpper);
                                        BlurImage.with(mContext).load(bitmapFromURL).intensity(25).Async(true).into(AdminImageUpper);

                                    }
                                });

                                thread.start();

                            }else {
                                senPostMsg.setVisibility(View.GONE);

                                Picasso.get().load(mNewsItem.getImageUrl())
                                        .into(AdminImageUpper);
                                Thread thread = new Thread(new Runnable(){
                                    @Override
                                    public void run() {
                                        Bitmap bitmapFromURL=Utility.getBitmapFromURL(mNewsItem.getImageUrl());
                                        BlurImage.with(mContext).load(bitmapFromURL).intensity(25).Async(true).into(AdminImageSceond);
                                        BlurImage.with(mContext).load(bitmapFromURL).intensity(25).Async(true).into(AdminNewsImage);

                                    }
                                });

                                thread.start();
                            }
                        }*/





                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    try{
                        String changeDate=utility.getPostUploadDate(mNewsItem.getPostedDate());
                        adminDateTextView.setText(changeDate);
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                    try {
                        if (!Validations.isEmptyString(mNewsItem.getName())) {
                            adminHeaderTextView.setText(mNewsItem.getName());
                            adminHeaderTextView.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    //listener.onPaginationClick(mNewsItem, click, position, "tagName");
                                }
                            });
                        }

                        AdminHashTextView.setLinkText(mNewsItem.getCaption());
                        AdminHashTextView.setOnLinkClickListener(new SocialTextView.OnLinkClickListener() {
                            @Override
                            public void onLinkClicked(int linkType, String matchedText) {
                                //linkType 1=hashTag,2==mention
                                listener.autoLinkListener(linkType, "", matchedText, mNewsItem);
                            }
                        });


                        changeLayout.post(new Runnable() {
                            @Override
                            public void run() {
                                //maybe also works height = ll.getLayoutParams().height;
                                height = changeLayout.getHeight();
                            }
                        });

                        AdminHashTextView.post(new Runnable() {

                            @Override
                            public void run() {
                                Log.v("Line count: ", AdminHashTextView.getLineCount() + "");
                                maxline = AdminHashTextView.getLineCount();
                                /*if (maxline > 3) {
                                    imageShowMore.setVisibility(View.VISIBLE);
                                    //showMoreTxt.setVisibility(View.VISIBLE);
                                } else {
                                    imageShowMore.setVisibility(View.GONE);
                                    //showMoreTxt.setVisibility(View.GONE);
                                }*/
                            }
                        });

                        AdminHashTextView.setLines(2);
                        adminImageShowMore.setOnClickListener(new View.OnClickListener() {
                            public void onClick(View v) {
                                if (!expand){
                                    expand=true;
                                    adminDateTextView.setVisibility(View.VISIBLE);
                                    adminImageShowMore.setImageResource(R.drawable.ic_baseline_expand_more_24);

                                    ObjectAnimator animation = ObjectAnimator.ofInt(AdminHashTextView, "maxLines", 40);
                                    animation.setDuration(100).start();
                                    int height_in_pixels = AdminHashTextView.getLineCount() * AdminHashTextView.getLineHeight();
                                    LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) changeLayout.getLayoutParams();
                                     /*if (height > height_in_pixels) {
                                        params.height = height +height_in_pixels;
                                    } else {
                                       // params.height = (height*2)+((height_in_pixels+100)/2);
                                        params.height = (height)+((height_in_pixels+100)/2);
                                    }*/
                                    final int sdk = android.os.Build.VERSION.SDK_INT;
                                    if (sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
                                        params.height=height*2;
                                        if (params.height<height_in_pixels){
                                            params.height=params.height+height_in_pixels;
                                        }

                                    }else if (sdk< Build.VERSION_CODES.O){
                                        if (params.height<0){
                                            params.height=height_in_pixels+((height_in_pixels + 100) / 2);

                                        }else {
                                            if (height > height_in_pixels) {
                                                params.height = height +height_in_pixels;
                                            } else if (params.height>height_in_pixels){
                                                params.height = (params.height) + ((height_in_pixels + 100) / 2);
                                            }else {
                                                params.height=height+(height_in_pixels + 150);
                                            }
                                        }
                                    }else {
                                        if (height > height_in_pixels) {
                                            params.height = height +height_in_pixels;
                                        } else {
                                            // params.height = (height*2)+((height_in_pixels+100)/2);
                                            if (params.height<0){
                                                params.height=height_in_pixels+((height_in_pixels + 100) / 2);
                                            }else {
                                                params.height = (height) + ((height_in_pixels + 100) / 2);
                                            }
                                            if (height<(height_in_pixels+100)){
                                                params.height=height+height_in_pixels;
                                            }
                                        }
                                    }

                                    changeLayout.setLayoutParams(params);
                                }else{
                                    expand=false;
                                    adminDateTextView.setVisibility(View.GONE);
                                    adminImageShowMore.setImageResource(R.drawable.ic_baseline_expand_less_24);
                                    ObjectAnimator animation = ObjectAnimator.ofInt(AdminHashTextView, "maxLines", 3);
                                    animation.setDuration(100).start();
                                    ViewGroup.LayoutParams params = (ViewGroup.LayoutParams) changeLayout.getLayoutParams();
                                    int height_in_pixels = AdminHashTextView.getLineCount() * AdminHashTextView.getLineHeight();

                                    final int sdk = android.os.Build.VERSION.SDK_INT;
                                    params.height=height;
                                    /*if (sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
                                        params.height=params.height+height_in_pixels;

                                    }else if (sdk< Build.VERSION_CODES.O){
                                        params.height=height_in_pixels+5;
                                    }else {
                                        params.height = height;
                                    }*/
                                    changeLayout.setLayoutParams(params);

                                }
                               /* expand = !expand;
                                advancedView.setVisibility( isExpanded ?  View.VISIBLE : View.GONE );
                                expandCtrl.setCompoundDrawablesWithIntrinsicBounds( isExpanded ? icon_collapse : icon_expand, 0, 0, 0 );
*/

                            }
                        });

                        /*adminImageShowMore.setOnClickListener(new View.OnClickListener() {
                            public void onClick(View v) {
                                if (!expand){
                                    expand=true;
                                    adminDateTextView.setVisibility(View.VISIBLE);

                                    adminImageShowMore.setImageResource(R.drawable.ic_baseline_expand_more_24);
                                    ObjectAnimator animation = ObjectAnimator.ofInt(AdminHashTextView, "maxLines", AdminHashTextView.getMaxLines());
                                    animation.setDuration(100).start();

                                    LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) changeLayout.getLayoutParams();
                                    int height_in_pixels = AdminHashTextView.getLineCount() * AdminHashTextView.getLineHeight();
                                    int height_in =  AdminHashTextView.getMaxLines();
                                        click = false;
                                    final int sdk = android.os.Build.VERSION.SDK_INT;
                                    if (sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
                                            params.height=height*2;
                                    } else {
                                    if (params.height<=height){
                                        params.height=height;
                                    }else if (height > height_in_pixels) {
                                        params.height = height * 2;
                                    } else {
                                        // params.height = (height*2)+((height_in_pixels+100)/2);
                                        params.height = (height)+((height_in_pixels+100)/2);
                                    }
                                    *//*if(height<(height_in_pixels+100)){
                                        params.height=height+height_in_pixels;
                                    }*//*
                                        }

                                    changeLayout.setLayoutParams(params);
                                }else{
                                    expand=false;
                                    adminDateTextView.setVisibility(View.GONE);
                                    adminImageShowMore.setImageResource(R.drawable.ic_baseline_expand_less_24);
                                    ObjectAnimator animation = ObjectAnimator.ofInt(AdminHashTextView, "minLines", AdminHashTextView.getMinLines());
                                    animation.setDuration(100).start();
                                    ViewGroup.LayoutParams params = (ViewGroup.LayoutParams) changeLayout.getLayoutParams();
                                    //params.height = height;
                                    int height_in_pixels = AdminHashTextView.getLineCount() * AdminHashTextView.getLineHeight();
                                    final int sdk = android.os.Build.VERSION.SDK_INT;
                                    if (sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
                                        params.height=params.height+height_in_pixels;

                                    }else if (sdk< Build.VERSION_CODES.O){
                                        params.height=height_in_pixels+5;
                                    }else {
                                        params.height = height;
                                    }
                                    changeLayout.setLayoutParams(params);

                                }
                               *//* expand = !expand;
                                advancedView.setVisibility( isExpanded ?  View.VISIBLE : View.GONE );
                                expandCtrl.setCompoundDrawablesWithIntrinsicBounds( isExpanded ? icon_collapse : icon_expand, 0, 0, 0 );
*//*

                            }
                        });*/

                    } catch (Exception e) {
                        e.printStackTrace();
                    }


                    try {
                        Log.d("userName", "session: " + userId + " " + "viewPost: " + mNewsItem.getName().toLowerCase().trim());



                        final int sdk = android.os.Build.VERSION.SDK_INT;

                        if (mNewsItem.getLikedStatus()) {
                            click = false;
                            if (sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
                               // AdminLikeImage.setBackgroundDrawable(ContextCompat.getDrawable(mContext, R.drawable.like_true));
                                AdminLikeImage.setImageResource( R.mipmap.admin_post_like);
                            } else {
                                //AdminLikeImage.setBackground(ContextCompat.getDrawable(mContext, R.drawable.like_true));
                                AdminLikeImage.setImageResource( R.mipmap.admin_post_like);
                            }
                        } else {
                            click = true;
                            if (sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
                                AdminLikeImage.setImageResource( R.mipmap.like_primary_color);
                               // AdminLikeImage.setBackgroundDrawable(ContextCompat.getDrawable(mContext, R.mipmap.like_primary_color));
                            } else {
                                AdminLikeImage.setImageResource( R.mipmap.like_primary_color);
                               // AdminLikeImage.setBackground(ContextCompat.getDrawable(mContext, R.mipmap.like_primary_color));
                            }
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    try{
                       /* if (mNewsItem.getLikedCount()>0) {
                            likeCountTxt.setVisibility(View.VISIBLE);
                            likeCountTxt.setText(mNewsItem.getLikedCount().toString() + " Likes");
                        }else {
                            likeCountTxt.setVisibility(View.VISIBLE);
                            likeCountTxt.setText( "No Likes");
                        }*/
                        adminLikeCountTxt.setVisibility(View.VISIBLE);
                    }catch (Exception e){
                        e.printStackTrace();
                    }

                    /*linkConnectionImage.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            listener.onPaginationClick(mNewsItem, click, position, "connection");
                        }
                    });*/
                    AdminLikeImage.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {

                            //likeImage.setBackground(click ?ContextCompat.getDrawable(mContext, R.drawable.like_true):ContextCompat.getDrawable(mContext, R.drawable.like));
                            listener.onPaginationClick(mNewsItem, click, position, "like");
                            final int sdk = android.os.Build.VERSION.SDK_INT;
                            if (click) {
                                click = false;
                                if (sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
                                   // AdminLikeImage.setBackgroundDrawable(ContextCompat.getDrawable(mContext, R.drawable.like_true));
                                    AdminLikeImage.setImageResource( R.mipmap.admin_post_like);

                                } else {
                                   // AdminLikeImage.setBackground(ContextCompat.getDrawable(mContext, R.drawable.like_true));
                                    AdminLikeImage.setImageResource( R.mipmap.admin_post_like);

                                }
                            } else {
                                click = true;
                                if (sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
                                    AdminLikeImage.setBackgroundDrawable(ContextCompat.getDrawable(mContext,  R.mipmap.like_primary_color));
                                } else {
                                    AdminLikeImage.setBackground(ContextCompat.getDrawable(mContext,  R.mipmap.like_primary_color));
                                }
                            }
                        }
                    });

                    itemView.setOnLongClickListener(new View.OnLongClickListener() {
                        @Override
                        public boolean onLongClick(View view) {
                            listener.onPaginationLongClick(mNewsItem, position,"admin");
                            return false;
                        }
                    });


                    //newTitle.setText(mNewsItem.getCaption());


                }
            }catch (Exception e){
                e.printStackTrace();
            }
        }

        @Override
        protected void clear() {

        }
    }







}
