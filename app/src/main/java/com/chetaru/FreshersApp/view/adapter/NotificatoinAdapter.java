package com.chetaru.FreshersApp.view.adapter;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.bitmap.FitCenter;
import com.bumptech.glide.load.resource.bitmap.RoundedCorners;
import com.bumptech.glide.request.RequestOptions;
import com.chauthai.swipereveallayout.SwipeRevealLayout;
import com.chetaru.FreshersApp.R;
import com.chetaru.FreshersApp.service.model.NotificationList;
import com.chetaru.FreshersApp.service.model.UserList;
import com.chetaru.FreshersApp.service.model.UserViewPost;
import com.chetaru.FreshersApp.utility.SessionParam;
import com.chetaru.FreshersApp.utility.Utility;
import com.chetaru.FreshersApp.utility.Validations;
import com.chetaru.FreshersApp.view.callback.PaginationAdapterCallback;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class NotificatoinAdapter extends RecyclerView.Adapter<NotificatoinAdapter.ViewHolder> {

    List<NotificationList> list = new ArrayList<>();
    public List<NotificationList> mFilteredList;
    private Context context;
    private boolean isLoadingAdded = false;
    // View Types
    private static final int ITEM = 0;
    private static final int LOADING = 1;
    private boolean retryPageLoad = false;
    private String errorMsg;
    private PaginationAdapterCallback mCallback;
    Utility utility;
    SessionParam sessionParam;
    NotificationListener listener;
    NotificationList updateList;




    public static class ViewHolder extends RecyclerView.ViewHolder {
        TextView tv_notiName,tv_date,tv_noti_desc;
        SwipeRevealLayout swipe_layout;
        LinearLayout ll_mainBlock,notiLayout;
        CircleImageView iv_logo;
        ImageView admin_image_logo;
        Button link_button;

        public ViewHolder(View v) {
            super(v);

            tv_notiName = v.findViewById(R.id.tv_notiName);
            notiLayout = v.findViewById(R.id.noti_background_layout);
            tv_date = v.findViewById(R.id.tv_date);
            tv_noti_desc = v.findViewById(R.id.tv_noti_desc);
           // swipe_layout = v.findViewById(R.id.swipe_layout);
           iv_logo = v.findViewById(R.id.iv_logo);
            admin_image_logo = v.findViewById(R.id.admin_image_logo);

        }
    }
    public NotificatoinAdapter(List<NotificationList> list, Context context,NotificationListener listener) {
        this.list = list;
        this.mFilteredList = list;
        this.context = context;
        utility=new Utility();
        this.listener=listener;
        sessionParam = new SessionParam(context);
    }

    public NotificatoinAdapter(Context context) {
        this.list = new ArrayList<>();
        this.mFilteredList = list;
        this.mCallback = (PaginationAdapterCallback) context;
        this.context = context;
        sessionParam = new SessionParam(context);
    }
    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        ViewHolder viewHolder = null;
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
//        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_notification_list, parent, false);
//        return new ViewHolder(view);
        switch (viewType) {
            case ITEM:
                View viewItem = inflater.inflate(R.layout.row_notification_list, parent, false);
                viewHolder = new ViewHolder(viewItem);
                break;
            case LOADING:
                View viewLoading = inflater.inflate(R.layout.item_progress, parent, false);
                viewHolder = new LoadingVH(viewLoading);
                break;
        }
        return viewHolder;
    }
    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        switch (getItemViewType(position)) {
            case ITEM:
                NotificationList notiList = mFilteredList.get(position); //data

                final ViewHolder mholder = (ViewHolder) holder;

                if(notiList.getId()!=null) {
                    if (!Validations.isEmptyString(notiList.getNotificationType()) && notiList.getNotificationType().equals("customNotification")) {
                        mholder.tv_noti_desc.setVisibility(View.VISIBLE);
                        mholder.admin_image_logo.setVisibility(View.VISIBLE);
                        mholder.iv_logo.setVisibility(View.GONE);
                        // Picasso.get().load(R.drawable.logo).into(holder.iv_logo);
                        holder.admin_image_logo.setImageDrawable(context.getResources().getDrawable(R.drawable.logo));

                        mholder.tv_notiName.setText("Admin Notification");
                        String dateChange = utility.getDate(mFilteredList.get(position).getCreatedAt());
                        mholder.tv_date.setText(dateChange);
                        String desString = mFilteredList.get(position).getDescription();
                        mholder.tv_noti_desc.setText(desString);
                   /* if (!Validations.isEmptyString(desString)) {
                        mholder.tv_noti_desc.setText(desString.substring(0,1));
                    }*/
                    } else {
                        mholder.tv_noti_desc.setVisibility(View.GONE);
                        mholder.admin_image_logo.setVisibility(View.GONE);
                        mholder.iv_logo.setVisibility(View.VISIBLE);
                        mholder.tv_notiName.setText(mFilteredList.get(position).getDescription());
                        String dateChange = utility.getDate(mFilteredList.get(position).getCreatedAt());
                        mholder.tv_date.setText(dateChange);
                        Glide.with(context).load(mFilteredList.get(position).getImage()).apply(new RequestOptions().centerCrop().placeholder(R.drawable.profile_pics)).into(holder.iv_logo);


                    }
                    mholder.itemView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            updateList = notiList;
                            listener.notiCall(mFilteredList, notiList, position);
                            setBackGroundRead(mholder);

                        }
                    });
                    try {
                        if (notiList.getIsRead()) {
                            setBackGroundRead(mholder);
                        } else {
                            setBackGroundUnRead(mholder);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }
                break;

            case LOADING:
                LoadingVH loadingVH = (LoadingVH) holder;

                if (retryPageLoad) {
                    loadingVH.mErrorLayout.setVisibility(View.VISIBLE);
                    loadingVH.mProgressBar.setVisibility(View.GONE);

                    loadingVH.mErrorTxt.setText(
                            errorMsg != null ?
                                    errorMsg :
                                    context.getString(R.string.error_msg_unknown));

                } else {
                   /* if(mFilteredList.get(mFilteredList.size()-1).getSelected()==null){
                        loadingVH.mErrorLayout.setVisibility(View.GONE);
                        loadingVH.mProgressBar.setVisibility(View.GONE);
                    }else{
                        loadingVH.mErrorayout.setVisibility(View.GONE);
                        loadingVH.mProgressBar.setVisibility(View.VISIBLE);
                    }*/
                }
                break;
        }


    }



    private void setBackGroundUnRead( ViewHolder mholder) {
        final int sdk = android.os.Build.VERSION.SDK_INT;
        if(sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
            mholder.notiLayout.setBackgroundColor(ContextCompat.getColor(context, R.color.colorPrimary) );
            mholder.tv_date.setTextColor(ContextCompat.getColor(context, R.color.notificationBg) );
            mholder.tv_notiName.setTextColor(ContextCompat.getColor(context, R.color.white) );
            mholder.tv_noti_desc.setTextColor(ContextCompat.getColor(context, R.color.white) );
        } else {
            mholder.notiLayout.setBackgroundColor(ContextCompat.getColor(context, R.color.colorPrimary) );
            mholder.tv_date.setTextColor(ContextCompat.getColor(context, R.color.notificationBg) );
            mholder.tv_notiName.setTextColor(ContextCompat.getColor(context, R.color.white) );
            mholder.tv_noti_desc.setTextColor(ContextCompat.getColor(context, R.color.white) );
        }
    }

    private void setBackGroundRead( ViewHolder mholder) {
        final int sdk = android.os.Build.VERSION.SDK_INT;
        if(sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
            mholder.notiLayout.setBackgroundColor(ContextCompat.getColor(context, R.color.notificationBg) );
            mholder.tv_date.setTextColor(ContextCompat.getColor(context, R.color.colorPrimary) );
            mholder.tv_notiName.setTextColor(ContextCompat.getColor(context, R.color.black) );
            mholder.tv_noti_desc.setTextColor(ContextCompat.getColor(context, R.color.black) );
        } else {
            mholder.notiLayout.setBackgroundColor(ContextCompat.getColor(context, R.color.notificationBg) );
            mholder.tv_date.setTextColor(ContextCompat.getColor(context, R.color.colorPrimary) );
            mholder.tv_notiName.setTextColor(ContextCompat.getColor(context, R.color.black) );
            mholder.tv_noti_desc.setTextColor(ContextCompat.getColor(context, R.color.black) );
        }
    }

    protected class LoadingVH extends ViewHolder implements View.OnClickListener {
        private ProgressBar mProgressBar;
        private ImageButton mRetryBtn;
        private TextView mErrorTxt;
        private LinearLayout mErrorLayout;

        public LoadingVH(View itemView) {
            super(itemView);
            mProgressBar = itemView.findViewById(R.id.loadmore_progress);
            mRetryBtn = itemView.findViewById(R.id.loadmore_retry);
            mErrorTxt = itemView.findViewById(R.id.loadmore_errortxt);
            mErrorLayout = itemView.findViewById(R.id.loadmore_errorlayout);
            mRetryBtn.setOnClickListener(this);
            mErrorLayout.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.loadmore_retry:
                case R.id.loadmore_errorlayout:
                    showRetry(false, null);
                    mCallback.retryPageLoad();
                    break;
            }
        }
    }

    public void showRetry(boolean show, @Nullable String errorMsg) {
        retryPageLoad = show;
        notifyItemChanged(mFilteredList.size() - 1);

        if (errorMsg != null) this.errorMsg = errorMsg;
    }


    @Override
    public int getItemCount() {

        return mFilteredList == null ? 0 : mFilteredList.size();
    }



    public Filter getFilter() {

        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {

                String charString = charSequence.toString();

                if (charString.isEmpty()) {

                    mFilteredList = list;
                } else {
                    ArrayList<NotificationList> filteredList = new ArrayList<>();

                    for (NotificationList addActionUser : list) {

                        if (addActionUser.getNotificationType().toLowerCase().contains(charString.toLowerCase()) ||
                                addActionUser.getNotificationType().toUpperCase().contains(charString.toUpperCase())) {
                            filteredList.add(addActionUser);
                        }
                    }
                    mFilteredList = filteredList;

                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = mFilteredList;
                return filterResults;

            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                mFilteredList = (ArrayList<NotificationList>) filterResults.values;
                notifyDataSetChanged();


            }
        };
    }

    public void add(NotificationList r) {
        mFilteredList.add(r);
        notifyItemInserted(mFilteredList.size() - 1);
    }

    public void addAll(List<NotificationList> noti_Results) {
        for (NotificationList result : noti_Results) {
            add(result);
        }
    }

    public void remove(NotificationList r) {
        int position = mFilteredList.indexOf(r);
        if (position > -1) {
            mFilteredList.remove(position);
            notifyItemRemoved(position);
        }
    }
    public void clear() {
        try {
            while (getItemCount() > 0) {
                remove(getItem(0));
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public void addLoadingFooter() {
        isLoadingAdded = true;
        add(new NotificationList());
    }

    public void removeLoadingFooter() {
        isLoadingAdded = false;

        int position = mFilteredList.size() - 1;
        NotificationList result = getItem(position);

        if (result != null) {
            mFilteredList.remove(position);
            notifyItemRemoved(position);
        }
    }


    @Override
    public int getItemViewType(int position) {
        return (position == mFilteredList.size() - 1 && isLoadingAdded) ? LOADING : ITEM;
//        if(mFilteredList.size()<5){
//            return mFilteredList.size() - 1;
//        }else{
//            return (position == mFilteredList.size() - 1 && isLoadingAdded) ? LOADING : ITEM;
//        }
    }

    public NotificationList getItem(int position) {
        return mFilteredList.get(position);
    }


    public  interface NotificationListener{
        void notiCall(List<NotificationList> notiList,NotificationList notificationList,int position);
    }

}
