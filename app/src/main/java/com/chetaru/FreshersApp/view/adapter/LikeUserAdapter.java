package com.chetaru.FreshersApp.view.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.chetaru.FreshersApp.R;
import com.chetaru.FreshersApp.service.model.LikeByUser;

import com.chetaru.FreshersApp.service.model.UserList;
import com.chetaru.FreshersApp.utility.SessionParam;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

public class LikeUserAdapter extends RecyclerView.Adapter<LikeUserAdapter.ViewHolder> implements Filterable {

    List<LikeByUser> likeList;
   private Context mContext;
    private List<LikeByUser>  ListFiltered;
    int selectedPosition=-1;
    SessionParam sessionParam;
    String userId;
    likeUserListener mListener;


    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.user_name_tv)
        TextView userName;
        @BindView(R.id.user_logo_image)
        CircleImageView userLogo;
        public ViewHolder(@NonNull View view) {
            super(view);
            ButterKnife.bind(this,view);
        }
    }

    public LikeUserAdapter(List<LikeByUser> likeList, Context mContext,likeUserListener mListener) {
        this.likeList = likeList;
        this.ListFiltered=likeList;
        this.mContext = mContext;
        this.sessionParam=new SessionParam(mContext);
        this.mListener=mListener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.like_user_count_list,parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        final LikeByUser likeUser=likeList.get(position);
        //Picasso.with(mContext).load(likeUser.getImageUrl()).into(holder.userLogo);
        Glide.with(mContext).load(likeUser.getImageUrl()).placeholder(R.drawable.profile_pics).into(holder.userLogo);
        holder.userName.setText(likeUser.getUserName());
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mListener.otherLikeByUser(likeUser);
            }
        });
    }

    @Override
    public int getItemCount() {
        try {
            return likeList.size();
        }catch (Exception e){
            e.printStackTrace();
            return 0;
        }
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    likeList = ListFiltered;
                } else {
                    List<LikeByUser> filteredList = new ArrayList<>();
                    for (LikeByUser row : ListFiltered) {

                        // name match condition. this might differ depending on your requirement
                        // here we are looking for name or phone number match
                        if (row.getUserName().toLowerCase().contains(charString.toLowerCase()) ) {
                            filteredList.add(row);
                        }
                    }

                    likeList = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = likeList;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                likeList = (ArrayList<LikeByUser>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }
    public interface likeUserListener{
        public void otherLikeByUser(LikeByUser likeByUser);
    }
}
