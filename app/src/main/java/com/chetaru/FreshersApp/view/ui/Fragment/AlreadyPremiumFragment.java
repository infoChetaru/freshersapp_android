package com.chetaru.FreshersApp.view.ui.Fragment;

import android.content.Context;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.chetaru.FreshersApp.R;
import com.chetaru.FreshersApp.service.model.MyProfileResponse;
import com.chetaru.FreshersApp.service.model.PremiumStatusResponse;
import com.chetaru.FreshersApp.service.retrofit.BaseRequest;
import com.chetaru.FreshersApp.service.retrofit.RequestReciever;
import com.chetaru.FreshersApp.utility.SessionParam;
import com.chetaru.FreshersApp.utility.Utility;
import com.chetaru.FreshersApp.utility.Validations;
import com.google.gson.Gson;
import com.google.gson.JsonObject;

import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link AlreadyPremiumFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class AlreadyPremiumFragment extends Fragment {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String purchaseMsg;
    private String expiryMsg;

    @BindView(R.id.back_go_premium_tv)
    TextView premiumBackTv;
    @BindView(R.id.purchase_date_text)
    TextView purchaseDateTxt;
    @BindView(R.id.expiry_date_text)
    TextView expiryDateTxt;
    private BaseRequest baseRequest;
    PremiumStatusResponse premStatus;



    String purchaseDate,expiryDate;

    private Unbinder unbinder;
    //handel class
    Utility utility;
    SessionParam sessionParam;
    alreadyPremiumListener mListener;

    public AlreadyPremiumFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment AlreadyPremiumFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static AlreadyPremiumFragment newInstance(String param1, String param2) {
        AlreadyPremiumFragment fragment = new AlreadyPremiumFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            purchaseMsg = getArguments().getString(ARG_PARAM1);
            expiryMsg = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_already_premium, container, false);
        unbinder = ButterKnife.bind(this, view);
        utility = new Utility();
        sessionParam = new SessionParam(getContext());
        premStatus=new PremiumStatusResponse();
        if (!Validations.isEmptyString(purchaseMsg)){
            purchaseDateTxt.setText(purchaseMsg);
            expiryDateTxt.setText(expiryMsg);
        }else {
            getPremiumStatus();
        }
        premiumBackTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!Validations.isEmptyString(purchaseMsg)){
                    mListener.PremiumBackHome();
                }else {
                    mListener.alreadyPremiumBack();
                }
            }
        });

        return view;
    }

    private void getPremiumStatus() {
        baseRequest = new BaseRequest(getContext());
        baseRequest.setBaseRequestListner(new RequestReciever() {
            @Override
            public void onSuccess(int requestCode, String Json, Object object) {
                try {

                    Gson gson = new Gson();
                    JSONObject jsonObject = new JSONObject(Json);
                    JSONObject response = (JSONObject) jsonObject.get("data");
                    premStatus = gson.fromJson(response.toString(), PremiumStatusResponse.class);
                    if (premStatus!=null) {
                        purchaseDate = premStatus.getPurchaseDate();
                        expiryDate = premStatus.getExpiryDate();

                        if (!Validations.isEmptyString(purchaseDate)) {
                            purchaseDate = utility.getDate(purchaseDate);
                            purchaseDateTxt.setText(String.format("Purchase Date: %s", purchaseDate));
                        }

                        if (!Validations.isEmptyString(expiryDate)) {
                            expiryDate = utility.getDate(expiryDate);
                            expiryDateTxt.setText(String.format("Expiry Date: %s", expiryDate));
                        }

                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int requestCode, String errorCode, String message) {
                //utility.showToast(getContext(),message);
            }

            @Override
            public void onNetworkFailure(int requestCode, String message) {

                utility.showToast(getContext(),message);
            }
        });

        /*{
            "postId":"10",
                "summary":"donated to was uploaded upon the single's release"
        }*/


        JsonObject main_object = new JsonObject();
        //main_object.addProperty("postId", selectedViewPost.getId());
        baseRequest.callAPIPostWOLoader(1, main_object, getString(R.string.api_getPremiumUserStatus));

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        unbinder.unbind();
    }
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof alreadyPremiumListener) {
            mListener = (alreadyPremiumListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement TagChildFragment");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }
    public interface alreadyPremiumListener{
       public void alreadyPremiumBack();
       public void PremiumBackHome();
    }
}