package com.chetaru.FreshersApp.view.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;
import com.chetaru.FreshersApp.CustomImage.RoundRectCornerImageView;
import com.chetaru.FreshersApp.R;
import com.chetaru.FreshersApp.service.model.ChatListResponse;
import com.chetaru.FreshersApp.utility.SessionParam;
import com.chetaru.FreshersApp.utility.Utility;
import com.chetaru.FreshersApp.utility.Validations;
import com.google.android.material.imageview.ShapeableImageView;
import com.squareup.picasso.Picasso;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class ChatAdapter extends RecyclerView.Adapter<ChatAdapter.ViewHolder> {
    List<ChatListResponse> list=new ArrayList<>();
    private List<ChatListResponse> mFilteredList;
    private boolean isLoadingAdded = false;
    // View Types
    private static final int ITEM = 0;
    private static final int LOADING = 1;
    private static final int DATETIME = 2;
    private boolean retryPageLoad = false;
    private String errorMsg;
    private Context mContext;
    Utility utility;
    SessionParam sessionParam;
    int userId=0;
    int otherUserId;
    ChatClickListener mListener;
    public ChatAdapter(int otherUserId,List<ChatListResponse> list, Context mContext,ChatClickListener mListener) {
        this.list = list;
        this.mFilteredList=list;
        this.mContext = mContext;
        this.utility=new Utility();
        this.sessionParam=new SessionParam(mContext);
        userId=sessionParam.id;
        this.otherUserId=otherUserId;
        this.mListener=mListener;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder{

        CircleImageView userLeftImageView,userRightImageView;
        ShapeableImageView rightImagePost,leftImagePost;
       // RoundRectCornerImageView leftImagePost;
        TextView leftMessage,rightMessage,leftDate,rightDate;
        TextView groupDateTxt,leftImageDate,rightImageDate;
        LinearLayout leftUserLayout,rightUserLayout,dateLayout;
        LinearLayout left_image_layout,left_message_layout,right_image_layout,right_message_layout;
        public ViewHolder(@NonNull View view) {
            super(view);
            //Left user
            left_image_layout=view.findViewById(R.id.left_image_layout);
            left_message_layout=view.findViewById(R.id.left_message_layout);
            leftUserLayout=view.findViewById(R.id.left_user_layout);
            userLeftImageView=view.findViewById(R.id.iv_logo_left);
            leftImagePost=view.findViewById(R.id.left_user_post_image);
            leftMessage=view.findViewById(R.id.user_left_message_txt);
            leftDate=view.findViewById(R.id.user_left_date_txt);
            leftImageDate=view.findViewById(R.id.user_left_image_date_txt);
            //Right user
            right_image_layout=view.findViewById(R.id.right_image_layout);
            right_message_layout=view.findViewById(R.id.right_message_layout);
            rightUserLayout=view.findViewById(R.id.right_user_layout);
            userRightImageView=view.findViewById(R.id.iv_logo_right);
            rightImagePost=view.findViewById(R.id.right_user_post_image);
            rightMessage=view.findViewById(R.id.user_right_message_txt_view);
            rightDate=view.findViewById(R.id.user_right_date_txt);
            rightImageDate=view.findViewById(R.id.user_right_image_date_txt);
            //date layout
            dateLayout=view.findViewById(R.id.timeTextLayout);
            groupDateTxt=view.findViewById(R.id.group_date_txt);

        }
    }



    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
       // View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.row_chat_msg,parent,false);
        ViewHolder viewHolder=null;
        LayoutInflater layoutInflater=LayoutInflater.from(parent.getContext());
        switch (viewType){
            case ITEM:
                View viewItem=layoutInflater.inflate(R.layout.row_chat_msg,parent,false);
                viewHolder=new ViewHolder(viewItem);
                break;
           /* case LOADING:
                View viewLoading=layoutInflater.inflate(R.layout.item_progress,parent,false);
                viewHolder=new LoadingVH(viewLoading);
                break;*/
           /* case DATETIME:
                View view=layoutInflater.inflate(R.layout.chat_date_time,parent,false);
                viewHolder=new DateLoadingVH(view);*/
        }
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        switch (getItemViewType(position)){
            case DATETIME:

                break;
            case ITEM:
               final ChatListResponse chatData=mFilteredList.get(position);

                //2020-07-03 07:29:49
                try {
                    if (!Validations.isEmptyString(chatData.getMsgDate())) {
                        String dateChange = Utility.formatTodayYesterday(chatData.getMsgDate());
                        holder.groupDateTxt.setText(dateChange);
                        if (position > 0) {
                                 String newDate = Utility.formatTodayYesterday(mFilteredList.get(position).getMsgDate());
                                 String oldDate = Utility.formatTodayYesterday(mFilteredList.get(position-1).getMsgDate());
                            Log.d("dateConvertor","newDate: "+newDate+"  "+"oldDate:"+oldDate);
                                 if (newDate.equalsIgnoreCase(oldDate)) {
                                   holder.dateLayout.setVisibility(View.GONE);
                               } else {
                                   holder.dateLayout.setVisibility(View.VISIBLE);
                               }

                        } else {
                            holder.dateLayout.setVisibility(View.VISIBLE);
                        }
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }

                try {

                        if (otherUserId == chatData.getUserId()) {
                            // Glide.with(mContext).load(chatData.getUserImage()).apply(new RequestOptions().centerCrop().placeholder(R.drawable.profile_pics)).into(holder.userLeftImageView);
                    /*Glide.with(mContext).load(chatData.getUserImage())
                            .into(holder.userLeftImageView);*/
                            /*holder.rightUserLayout.setVisibility(View.GONE);*/
                            holder.leftUserLayout.setVisibility(View.VISIBLE);
                            holder.userLeftImageView.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    mListener.onUserImageView(holder.userLeftImageView,chatData.getUserImage());

                                }
                            });
                            holder.leftImagePost.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    mListener.onUserImageView(holder.leftImagePost,chatData.getMessage());
                                }
                            });

                            if (chatData.getFileType().equals(1)) {
                                holder.left_message_layout.setVisibility(View.VISIBLE);
                                holder.left_image_layout.setVisibility(View.GONE);
                                holder.leftMessage.setVisibility(View.VISIBLE);
                                holder.leftImagePost.setVisibility(View.GONE);
                                holder.leftMessage.setText(chatData.getMessage());

                            }else
                            {
                                holder.left_message_layout.setVisibility(View.GONE);
                                holder.left_image_layout.setVisibility(View.VISIBLE);
                                holder.leftMessage.setVisibility(View.GONE);
                                holder.leftImagePost.setVisibility(View.VISIBLE);
                                /*Glide.with(mContext).load(chatData.getMessage())
                                        .diskCacheStrategy(DiskCacheStrategy.DATA)
                                    .into(holder.leftImagePost);*/
                                //Picasso.with(mContext).load(chatData.getMessage()).into(holder.leftImagePost);
                                Glide.with(mContext).load(chatData.getMessage()).placeholder(R.drawable.profile_pics).into(holder.leftImagePost);
                            }
                            try {
                                if (!Validations.isEmptyString(chatData.getMsgDate())) {
                                    String dateChange = Utility.formatToShowOnlyTime(chatData.getMsgDate());
                                    holder.leftDate.setText(dateChange);
                                    holder.leftImageDate.setText(dateChange);
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                   /* Glide.with(mContext)
                            .asBitmap().load(chatData.getMessage())
                            .into(new SimpleTarget<Bitmap>() {
                                @Override
                                public void onResourceReady(Bitmap resource, Transition<? super Bitmap> transition) {
                                    holder.userLeftImageView.setImageBitmap(resource);
                                }
                            });*/

                        } else {
                   /* Glide.with(mContext).load(chatData.getUserImage())
                            .into(holder.userRightImageView);*/
                            //Glide.with(mContext).load(chatData.getUserImage()).apply(new RequestOptions().centerCrop().placeholder(R.drawable.profile_pics)).into(holder.userRightImageView);

                            /* holder.leftUserLayout.setVisibility(View.GONE);*/
                            holder.rightUserLayout.setVisibility(View.VISIBLE);
                            holder.userRightImageView.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    mListener.onUserImageView(holder.userRightImageView,chatData.getUserImage());
                                }
                            });
                            holder.rightImagePost.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    mListener.onUserImageView(holder.rightImagePost,chatData.getMessage());
                                }
                            });
                            if (chatData.getFileType().equals(1)) {
                                holder.right_message_layout.setVisibility(View.VISIBLE);
                                holder.right_image_layout.setVisibility(View.GONE);
                                holder.rightMessage.setVisibility(View.VISIBLE);
                                holder.rightImagePost.setVisibility(View.GONE);
                                holder.rightMessage.setText(chatData.getMessage());
                            }else {
                                holder.right_message_layout.setVisibility(View.GONE);
                                holder.right_image_layout.setVisibility(View.VISIBLE);
                                holder.rightMessage.setVisibility(View.GONE);
                                holder.rightImagePost.setVisibility(View.VISIBLE);
                               /* Glide.with(mContext).load(chatData.getMessage())
                                        .diskCacheStrategy(DiskCacheStrategy.DATA)
                                        .into(holder.rightImagePost);*/
                                //Picasso.with(mContext).load(chatData.getMessage()).into(holder.rightImagePost);
                                Glide.with(mContext).load(chatData.getMessage()).placeholder(R.drawable.profile_pics).into(holder.rightImagePost);

                            }
                            try {
                                if (!Validations.isEmptyString(chatData.getMsgDate())) {
                                    String dateChange = Utility.formatToShowOnlyTime(chatData.getMsgDate());
                                    holder.rightDate.setText(dateChange);
                                    holder.rightImageDate.setText(dateChange);
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }                    /*Glide.with(mContext)
                            .asBitmap().load(chatData.getMessage())
                            .into(new SimpleTarget<Bitmap>() {
                                @Override
                                public void onResourceReady(Bitmap resource, Transition<? super Bitmap> transition) {
                                    holder.userRightImageView.setImageBitmap(resource);
                                }
                            });*/
                        }

                }catch (Exception e){
                    e.printStackTrace();
                }

                break;
            case LOADING:
                LoadingVH loadingVH= (LoadingVH) holder;
                if (retryPageLoad) {
                    loadingVH.mErrorLayout.setVisibility(View.VISIBLE);
                    loadingVH.mProgressBar.setVisibility(View.GONE);

                    loadingVH.mErrorTxt.setText(
                            errorMsg != null ?
                                    errorMsg :
                                    mContext.getString(R.string.error_msg_unknown));

                } else {
                   /* if(mFilteredList.get(mFilteredList.size()-1).getSelected()==null){
                        loadingVH.mErrorLayout.setVisibility(View.GONE);
                        loadingVH.mProgressBar.setVisibility(View.GONE);
                    }else{
                        loadingVH.mErrorLayout.setVisibility(View.GONE);
                        loadingVH.mProgressBar.setVisibility(View.VISIBLE);
                    }*/
                }
                break;
        }

    }

    @Override
    public int getItemCount() {
        return mFilteredList==null ? 0 : mFilteredList.size();
    }

    public void refreshData(ChatListResponse r) {
        if (r.getMessageId()!=null) {
            mFilteredList.add(r);
            notifyItemInserted(0);

            notifyDataSetChanged();

        }

    }
    public void add(ChatListResponse r) {
        if (r.getMessageId()!=null) {
            mFilteredList.add(r);
            notifyItemInserted(mFilteredList.size() - 1);
        }
    }


    public void addAll(List<ChatListResponse> viewPost_Results) {
        for (ChatListResponse result : viewPost_Results) {
            add(result);
        }
    }
    public void remove(ChatListResponse r) {
        int position = mFilteredList.indexOf(r);
        if (position > -1) {
            mFilteredList.remove(position);
            notifyItemRemoved(position);
        }
    }
    public void clear() {
        try {
            while (getItemCount() > 0) {
                remove(getItem(0));
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public void addLoadingFooter() {
        isLoadingAdded = true;
        add(new ChatListResponse());
    }

    public void removeLoadingFooter() {
        isLoadingAdded = false;

        int position = mFilteredList.size() - 1;
        ChatListResponse result = getItem(position);

        if (result != null) {
            mFilteredList.remove(position);
            notifyItemRemoved(position);
        }
    }
    protected class DateLoadingVH extends ViewHolder {

        LinearLayout dateLayout;
        TextView dateText;
        public DateLoadingVH(@NonNull View view) {
            super(view);
            dateLayout=view.findViewById(R.id.char_date_timeLayout);
            dateText=view.findViewById(R.id.group_date_txt);

        }
    }

    protected class LoadingVH extends ViewHolder implements View.OnClickListener {
        private ProgressBar mProgressBar;
        private ImageButton mRetryBtn;
        private TextView mErrorTxt;
        private LinearLayout mErrorLayout;

        public LoadingVH(View itemView) {
            super(itemView);
            mProgressBar = itemView.findViewById(R.id.loadmore_progress);
            mRetryBtn = itemView.findViewById(R.id.loadmore_retry);
            mErrorTxt = itemView.findViewById(R.id.loadmore_errortxt);
            mErrorLayout = itemView.findViewById(R.id.loadmore_errorlayout);
            mRetryBtn.setOnClickListener(this);
            mErrorLayout.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.loadmore_retry:
                case R.id.loadmore_errorlayout:
                    showRetry(false, null);
                    // mCallback.retryPageLoad();
                    break;
            }
        }
    }

    public void showRetry(boolean show, @Nullable String errorMsg) {
        retryPageLoad = show;
        notifyItemChanged(mFilteredList.size() - 1);

        if (errorMsg != null) this.errorMsg = errorMsg;
    }

    @Override
    public int getItemViewType(int position) {
        return (position == mFilteredList.size() - 1 && isLoadingAdded) ? LOADING : ITEM;
//        if(mFilteredList.size()<5){
//            return mFilteredList.size() - 1;
//        }else{
//            return (position == mFilteredList.size() - 1 && isLoadingAdded) ? LOADING : ITEM;
//        }
    }
    public ChatListResponse getItem(int position) {
        return mFilteredList.get(position);
    }

   public interface ChatClickListener{
       public void onUserImageView(View view,String imageString);
    }

}
