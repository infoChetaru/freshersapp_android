package com.chetaru.FreshersApp.view.ui.Activity;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;

import android.media.AudioManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.VibrationEffect;
import android.os.Vibrator;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;



import android.widget.TextView;


import com.android.billingclient.api.AcknowledgePurchaseParams;
import com.android.billingclient.api.AcknowledgePurchaseResponseListener;
import com.android.billingclient.api.BillingClient;
import com.android.billingclient.api.BillingClientStateListener;
import com.android.billingclient.api.BillingFlowParams;
import com.android.billingclient.api.BillingResult;


import com.android.billingclient.api.Purchase;
import com.android.billingclient.api.PurchasesUpdatedListener;
import com.android.billingclient.api.SkuDetails;
import com.android.billingclient.api.SkuDetailsParams;
import com.android.billingclient.api.SkuDetailsResponseListener;

import com.chetaru.FreshersApp.R;

import com.chetaru.FreshersApp.service.model.ConnListResponse;
import com.chetaru.FreshersApp.service.model.LikeByUser;

import com.chetaru.FreshersApp.service.model.MyProfileResponse;

import com.chetaru.FreshersApp.service.model.NotificationList;
import com.chetaru.FreshersApp.service.model.PendingRequestList;


import com.chetaru.FreshersApp.service.model.UserViewPost;
import com.chetaru.FreshersApp.service.retrofit.BaseRequest;
import com.chetaru.FreshersApp.service.retrofit.Functions;
import com.chetaru.FreshersApp.service.retrofit.RequestReciever;
import com.chetaru.FreshersApp.utility.BaseActivity;


import com.chetaru.FreshersApp.utility.Constant;
import com.chetaru.FreshersApp.utility.MarshMallowPermission;
import com.chetaru.FreshersApp.utility.SessionParam;
import com.chetaru.FreshersApp.utility.Utility;
import com.chetaru.FreshersApp.utility.Validations;
import com.chetaru.FreshersApp.view.callback.ConnectionListCallback;
import com.chetaru.FreshersApp.view.callback.HomepageCallback;

import com.chetaru.FreshersApp.view.callback.PaginationAdapterCallback;
import com.chetaru.FreshersApp.view.callback.RequestListCallback;
import com.chetaru.FreshersApp.view.ui.Fragment.AlreadyPremiumFragment;
import com.chetaru.FreshersApp.view.ui.Fragment.BlockUserFragment;
import com.chetaru.FreshersApp.view.ui.Fragment.ConnRequestTabFragment;
import com.chetaru.FreshersApp.view.ui.Fragment.HomeFragment;
import com.chetaru.FreshersApp.view.ui.Fragment.LikeCountUserFragment;
import com.chetaru.FreshersApp.view.ui.Fragment.LocaleFragment;
import com.chetaru.FreshersApp.view.ui.Fragment.MyLikesFragment;

import com.chetaru.FreshersApp.view.ui.Fragment.MyUploadFragment;
import com.chetaru.FreshersApp.view.ui.Fragment.NotificationsFragment;
import com.chetaru.FreshersApp.view.ui.Fragment.OtherTabFragment;
import com.chetaru.FreshersApp.view.ui.Fragment.OtherUserUploadFragment;
import com.chetaru.FreshersApp.view.ui.Fragment.PremiumFragment;
import com.chetaru.FreshersApp.view.ui.Fragment.PremiumMsgFragment;

import com.chetaru.FreshersApp.view.ui.Fragment.SettingFragment;
import com.chetaru.FreshersApp.view.ui.Fragment.TagImageFragment;
import com.chetaru.FreshersApp.view.ui.Fragment.UpgradeLocationFragment;
import com.chetaru.FreshersApp.view.ui.Fragment.UploadFragment;
import com.chetaru.FreshersApp.view.ui.Fragment.UserProfileFragment;
import com.chetaru.FreshersApp.view.ui.Fragment.ViewAllPostFragment;
import com.chetaru.FreshersApp.view.ui.Fragment.newProfileFragment;
import com.chetaru.FreshersApp.view.ui.PreviewDemo;

import com.google.android.material.bottomnavigation.BottomNavigationItemView;
import com.google.android.material.bottomnavigation.BottomNavigationMenuView;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.gson.Gson;
import com.google.gson.JsonObject;

import androidx.annotation.IdRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;

import androidx.core.app.ActivityOptionsCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;


import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;

import org.json.JSONObject;


import java.io.File;
import java.util.ArrayList;


import java.util.List;




import static android.Manifest.permission.ACCESS_COARSE_LOCATION;
import static android.Manifest.permission.ACCESS_FINE_LOCATION;

public class HomeActivity extends BaseActivity implements PaginationAdapterCallback,HomepageCallback, RequestListCallback,PremiumFragment.GoPremiumListener
                                   , PremiumMsgFragment.PremiumMsgListenerFragment, TagImageFragment.TagChildFragment,LocaleFragment.LocaleListener
                                    ,BlockUserFragment.BlockUserListener,NotificationsFragment.NotificationListenerFragment,newProfileFragment.newProfileListener
                                    ,MyUploadFragment.MyUploadListenerFragment, MyLikesFragment.MyLikeFragmentListener,AlreadyPremiumFragment.alreadyPremiumListener
                                    , SettingFragment.settingListener, OtherUserUploadFragment.OtherUploadListenerFragment,LikeCountUserFragment.likeCountListener
                                    , ConnectionListCallback,UserProfileFragment.otherProfileListener
                                    , UpgradeLocationFragment.UpgradeLocListener {

    private ArrayList<String> permissionsToRequest;
    private ArrayList<String> permissionsRejected = new ArrayList<>();
    private ArrayList<String> permissions = new ArrayList<>();

    private final static int ALL_PERMISSIONS_RESULT = 101;
    private static final int REQUEST_CAMERA_CAPTURE = 1;
    private static final int REQUEST_CROP = 3;
    final private int REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS = 124;
    private String TAG = HomeActivity.class.getSimpleName();
    private final static String TAG_FRAGMENT = "TAG_FRAGMENT";

    private Bitmap bitmap = null;
    boolean notiBack = false;
    int postId = 0;
    int backValue = 0;
    String backValueString = null;
    String imgBase64 = "";
    File sourceFile;
    Utility utility;
    boolean homeBackClick = true;
    SessionParam sessionParam;
    String tokenId = "";
    BaseRequest baseRequest;
    String FilePathString;
    Boolean cameraActive = false;
    MarshMallowPermission marshMallowPermission;
    BottomNavigationView bottomNavigation;
    Fragment fragment;
    TextView tv_notiCount;
    Context mContext;
    boolean premiumUser = false;
    int countValue = 0;
    List<SkuDetails> skuDetailsList;
    private BillingClient billingClient;
    List<SkuDetails> skuDetailsListGlobal;
    Activity activity;
    String defaultLocaleName = "";
    boolean otherProfileBack = false;
    int otherPersonId = 0;
    List<Purchase> purchasesUser;
    boolean hasSubscription = false;
    boolean likeCount = false;
    boolean viewAllPost = false;
    public List<UserViewPost> viewAllPostList;
    public  List<NotificationList> allNotificationList;
    public  int notiPosition;
    int postPosition;

    //notification back Handel
    int notiPostId=0;
    //Setting back Handel
    boolean requestBack=false,settingClick=false;
    boolean myUploadBack=false,myLikeBack=false,settingBlockBack=false,todayLike=false;
    //connection tab handel
    boolean backTochat=false;
    //chat handel
    int connectionId=0,otherUserId=0,connectionStatus=0;
    String parentClassCallBack="",parentClassPost="";
    //HashTag handel
    String parentTagname="";
    int parentTagId=0;
    boolean getPremium=true,subClick=true;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_home);
        bottomNavigation = findViewById(R.id.nav_view);
        this.mContext = this;
        purchasesUser = new ArrayList<>();
        utility = new Utility();
        sessionParam = new SessionParam(this);
        marshMallowPermission = new MarshMallowPermission(this);
        //BottomNavigationViewHelper.disableShiftMode(bottomNavigation);
        activity = this;
        premiumUser = sessionParam.premieUser;
        setUpBillingClient();
        getDefaultLocale();
        bottomNavigation.setOnNavigationItemSelectedListener(navigationItemSelectedListener);
        getBagesCount(bottomNavigation, mContext);

        String type = getIntent().getStringExtra("action");
        if (type != null) {
            switch (type) {
                case "notifyFrag":
                    /*Fragment fragment = new NotificationFragment();
                    FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                    transaction.replace(R.id.container, fragment).commit();*/
                    break;
            }
        }
        int connectionId = getIntent().getIntExtra("connectionId",0);
        int otherUserId = getIntent().getIntExtra("otherUserId",0);
        int userName = getIntent().getIntExtra("userName",0);
        int userProfile = getIntent().getIntExtra("otherUserProfile", 0);
        int connectionStatus = getIntent().getIntExtra("connection",0);






        if (connectionStatus > 0) {
             clearFragment();
            backTochat=true;
            otherPersonId=otherUserId;
            BottomNavigationView mBottomNavigationView = findViewById(R.id.nav_view);
            mBottomNavigationView.setSelectedItemId(R.id.navigation_tag);
            Bundle bundle = new Bundle();
            bundle.putSerializable("viewPost", null);
            bundle.putInt("position", 0);
            bundle.putInt("personId", otherUserId);
            bundle.putInt("connectionStatus", connectionStatus);
            bundle.putString("connection", "connection");

            OtherTabFragment fragment = new OtherTabFragment();
            fragment.setArguments(bundle);
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            transaction.replace(R.id.nav_host_fragment, fragment);
            transaction.addToBackStack(null);
            transaction.commit();

        }


        try {

            BottomNavigationMenuView bottomNavigationMenuView =
                    (BottomNavigationMenuView) bottomNavigation.getChildAt(0);
            View v = bottomNavigationMenuView.getChildAt(3);

            BottomNavigationItemView itemView = (BottomNavigationItemView) v;
            View badge = LayoutInflater.from(this)
                    .inflate(R.layout.notification_bages, bottomNavigationMenuView, false);
            tv_notiCount = badge.findViewById(R.id.notification_badge);

           /* View badgeChat=LayoutInflater.from(this).inflate(R.layout.notification_bages,bottomNavigationMenuView,false);
            tv_chatCount=badgeChat.findViewById(R.id.notification_badge);
*/
            itemView.addView(badge);
            // itemView.addView(badgeChat);

            tv_notiCount.setVisibility(View.GONE);
            //sceond Bage for chat Count

            //sceondView.addView(badgeChat);
            //showBadge(mContext,bottomNavigation,R.id.navigation_tag, countValue);

        } catch (Exception e) {
            e.printStackTrace();
        }
        // openFragment(HomeFragment.newInstance("", ""));
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
       /* try {
            AppBarConfiguration appBarConfiguration = new AppBarConfiguration.Builder(
                    R.id.navigation_home, R.id.navigation_camera, R.id.navigation_profile,
                    R.id.H,R.id.navigation_notification,R.id.navigation_chat)
                    .build();
            NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
            NavigationUI.setupActionBarWithNavController(this, navController, appBarConfiguration);
            NavigationUI.setupWithNavController(navView, navController);
        }catch (Exception e)
        {
            e.printStackTrace();
        }*/
       if (!premiumUser) {
           permission();
           permissions.add(ACCESS_FINE_LOCATION);
           permissions.add(ACCESS_COARSE_LOCATION);

           permissionsToRequest = findUnAskedPermissions(permissions);
           //get the permissions we have asked for before but are not granted..
           //we will store this in a global list to access later.
           if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
               if (permissionsToRequest.size() > 0)
                   requestPermissions(permissionsToRequest.toArray(new String[permissionsToRequest.size()]), ALL_PERMISSIONS_RESULT);
           }
       }



    }

    private void getDefaultLocale() {
        baseRequest = new BaseRequest(this);
        baseRequest.setBaseRequestListner(new RequestReciever() {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onSuccess(int requestCode, String Json, Object object) {
                try {

                    Gson gson = new Gson();
                    JSONObject jsonObject = new JSONObject(Json);
                    String localeName = jsonObject.getString("data");
                    if (!Validations.isEmptyString(localeName)) {
                        defaultLocaleName = localeName;
                    }


                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int requestCode, String errorCode, String message) {
                //utility.showToast(getBaseContext(),message);
            }

            @Override
            public void onNetworkFailure(int requestCode, String message) {

                utility.showToast(HomeActivity.this, message);
            }
        });

        JsonObject object = Functions.getClient().getJsonMapObject(
                "", "");
        baseRequest.callAPIPostWOLoader(1, object, getString(R.string.api_getDefaultLocale));

    }

    public static void showBadge(Context context, BottomNavigationView
            bottomNavigationView, @IdRes int itemId, int value) {
        removeBadge(bottomNavigationView, itemId, value);
        BottomNavigationItemView itemView = bottomNavigationView.findViewById(itemId);
        View badge = LayoutInflater.from(context).inflate(R.layout.notification_bages, bottomNavigationView, false);

        TextView text = badge.findViewById(R.id.notification_badge);
        text.setText(String.valueOf(value));
        itemView.addView(badge);
    }

    public static void removeBadge(BottomNavigationView bottomNavigationView, @IdRes int itemId, int value) {
        BottomNavigationItemView itemView = bottomNavigationView.findViewById(itemId);
        Log.d("TAG", "id:" + itemId + "getChildCount" + itemView.getChildCount());
        if (itemView.getChildCount() == 1) {
            itemView.removeViewAt(4);
        }
        try {


            if (value <= 0) {
                // BottomNavigationItemView v=itemView.getChildAt(4);
                if (itemView.getChildCount() > 3 && itemView.getChildCount() <= 5)
                    itemView.removeViewAt(4);

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void getBagesCount(BottomNavigationView bottomNavigation, Context mContext) {
        baseRequest = new BaseRequest(this);
        baseRequest.setBaseRequestListner(new RequestReciever() {
            @Override
            public void onSuccess(int requestCode, String Json, Object object) {
                try {

                    JSONObject jsonObject = new JSONObject(Json);

                    /*Integer count= jsonObject.getInt("data");
                    tv.setText(count);*/
                    int chatCount = 0;
                    try {
                        chatCount = jsonObject.getInt("chatCount");
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    int pendingCount = 0;
                    try {
                        pendingCount = jsonObject.getInt("pendingRequestCount");
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    tv_notiCount.setText("");

                    if (jsonObject.optInt("data") > 0) {
                        //tv_notiCount.setText(jsonObject.optInt("data") + "");
                        if (jsonObject.optInt("data") > 10) {
                            tv_notiCount.setText(jsonObject.optInt("data") + "+");
                        } else {
                            tv_notiCount.setText(jsonObject.optInt("data") + "");

                        }
                        // BottomMenuHelper.showBadge(this, mBottomNavigationView, R.id.action_news, "1");

                        //api_notificationListUnread();
                        tv_notiCount.setVisibility(View.VISIBLE);
                    } else {
                        tv_notiCount.setVisibility(View.GONE);
//                        api_notificationListUnread();
                    }
                    if (chatCount > 0 || pendingCount > 0) {
                        countValue = chatCount + pendingCount;
                       /* if (countValue>10){
                            tv_chatCount.setText(countValue+"+");
                        }else {
                            tv_chatCount.setText(countValue);
                        }*/
                        showBadge(mContext, bottomNavigation, R.id.navigation_tag, countValue);
                    } else {

                        removeBadge(bottomNavigation, R.id.navigation_tag, countValue);
                    }
                    // utility.showToast(getContext(),object.toString());
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int requestCode, String errorCode, String message) {
                //utility.showToast(getBaseContext(),message);
            }

            @Override
            public void onNetworkFailure(int requestCode, String message) {

                utility.showToast(getBaseContext(), message);
            }
        });


        JsonObject object = Functions.getClient().getJsonMapObject(
                "", "");
        baseRequest.callAPIPostWOLoader(1, object, getString(R.string.api_unreadNotificationCount));

    }

    private ArrayList<String> findUnAskedPermissions(ArrayList<String> wanted) {
        ArrayList<String> result = new ArrayList<String>();

        for (String perm : wanted) {
            if (!hasPermission(perm)) {
                result.add(perm);
            }
        }

        return result;
    }

    private boolean hasPermission(String permission) {
        if (canMakeSmores()) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                return (checkSelfPermission(permission) == PackageManager.PERMISSION_GRANTED);
            }
        }
        return true;
    }

    private boolean canMakeSmores() {
        return (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP_MR1);
    }

    /*request user for certain permission*/
    private void permission() {
        //datafinish = true;
        List<String> permissionsNeeded = new ArrayList<String>();
        final List<String> permissionsList = new ArrayList<String>();
        if (!addPermission(permissionsList, Manifest.permission.ACCESS_FINE_LOCATION))
            permissionsNeeded.add("GPS");
        if (!addPermission(permissionsList, Manifest.permission.ACCESS_NETWORK_STATE))
            permissionsNeeded.add("Network state");
        if (!addPermission(permissionsList, Manifest.permission.ACCESS_COARSE_LOCATION))
            permissionsNeeded.add("Location");
        if (!addPermission(permissionsList, Manifest.permission.INTERNET))
            permissionsNeeded.add("Internet");
        if (!addPermission(permissionsList, Manifest.permission.READ_PHONE_STATE))
            permissionsNeeded.add("phone status");
        if (!addPermission(permissionsList, Manifest.permission.CAMERA))
            permissionsNeeded.add("Camera");
        if (!addPermission(permissionsList, Manifest.permission.WRITE_EXTERNAL_STORAGE))
            permissionsNeeded.add("Write storage");
        if (permissionsList.size() > 0) {
            if (permissionsNeeded.size() > 0) {
                // Need Rationale
                String message = "You need to grant access to " + permissionsNeeded.get(0);
                for (int i = 1; i < permissionsNeeded.size(); i++)
                    message = message + ", " + permissionsNeeded.get(i);
                /*showMessageOKCancel(message, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            requestPermissions(permissionsList.toArray(new String[permissionsList.size()]), REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS);
                        }
                    }
                });*/
                return;
            }
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermissions(permissionsList.toArray(new String[permissionsList.size()]), REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS);
            }
            return;
        }
        //init();
    }

    private boolean addPermission(List<String> permissionsList, String permission) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(permission) != PackageManager.PERMISSION_GRANTED) {
                permissionsList.add(permission);
                //Check for Rationale Optiong
                if (!shouldShowRequestPermissionRationale(permission))
                    return false;
            }
        }
        return true;
    }


    @TargetApi(Build.VERSION_CODES.M)
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {

        switch (requestCode) {

            case ALL_PERMISSIONS_RESULT:
                for (String perms : permissionsToRequest) {
                    if (!hasPermission(perms)) {
                        permissionsRejected.add(perms);
                    }
                }

                if (permissionsRejected.size() > 0) {


                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        if (shouldShowRequestPermissionRationale(permissionsRejected.get(0))) {
                            showMessageOKCancel("These permissions are mandatory for the application. Please allow access.",
                                    new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                                requestPermissions(permissionsRejected.toArray(new String[permissionsRejected.size()]), ALL_PERMISSIONS_RESULT);
                                            }
                                        }
                                    });
                            return;
                        }
                    }

                }

                break;
        }

    }

    private void showMessageOKCancel(String message, DialogInterface.OnClickListener okListener) {
        new AlertDialog.Builder(HomeActivity.this)
                .setMessage(message)
                .setPositiveButton("OK", okListener)
                .setNegativeButton("Cancel", null)
                .create()
                .show();
    }

    ///handel http aouth url
    public void refreshToken(){
    try

    {
        HttpClient client = new DefaultHttpClient();
        String getURL = "http://www.google.com";
        HttpGet get = new HttpGet(getURL);
        HttpResponse responseGet = client.execute(get);
        HttpEntity resEntityGet = responseGet.getEntity();
        if (resEntityGet != null) {
            // do something with the response
            String response = EntityUtils.toString(resEntityGet);
            Log.i("GET RESPONSE", response);
        }
    } catch(
    Exception e)

    {
        e.printStackTrace();
    }

   }

    /*
    tagUser	When tag user.
hashTagUser	When tag hashtag user.
postLike	When user liked post.
pendingRequest	When connection request comes.
acceptRequest	When connection request accepted by user.
sendMessage	When user send message.
    * */
    public BroadcastReceiver myReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getStringExtra("path");
            if (action.equals("tagUser")){
                fragment=new TagImageFragment();
                loadFragment(fragment);

            }else if (action.equals("hashTagUser"))
            {

            }else if (action.equals("postLike")){
                fragment=new MyLikesFragment();
                loadFragment(fragment);

            }
            getBagesCount(bottomNavigation,mContext);

        }
    };
    @Override
    protected void onResume() {
        super.onResume();
        getBagesCount(bottomNavigation,mContext);
        registerReceiver(myReceiver, new IntentFilter("path"));
        //hideSoftKeyboard();
        //billingClient.queryPurchases();
    }

    @Override
    protected void onPause() {
        super.onPause();
        unregisterReceiver(myReceiver);
        getBagesCount(bottomNavigation,mContext);
        sessionParam=new SessionParam(this);
        try {
            premiumUser = sessionParam.premieUser;
        }catch (Exception e){
            e.printStackTrace();
        }
        //hideSoftKeyboard();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
    BottomNavigationView.OnNavigationItemSelectedListener navigationItemSelectedListener =
            new BottomNavigationView.OnNavigationItemSelectedListener() {
                @Override public boolean onNavigationItemSelected(@NonNull MenuItem item) {

                    getBagesCount(bottomNavigation,mContext);
                    switch (item.getItemId()) {

                        case R.id.navigation_home:
                            clearFragment();
                            // openFragment(HomeFragment);
                            fragment = new HomeFragment();
                            loadFragment(fragment);

                           //callImgeFragment();

                            return true;
                        case R.id.navigation_camera:
                            dispatchCameraIntent();
                            /*if(cameraActive){
                                callFragment(FilePathString);
                            }else {

                            }*/
                            /*fragment=new UploadFragment();
                            loadFragment(fragment);*/
                            /*Intent intent=new Intent(HomeActivity.this, UploadImageActivity.class);
                            startActivity(intent);*/
                            return true;
                        case R.id.navigation_profile:
                           // clearFragment();
                            // openFragment(NotificationFragment.newInstance("", ""));
                           /* fragment = new ProfileFragment();
                            loadFragment(fragment);*/
                           fragment=new newProfileFragment();
                           loadFragment(fragment);

                            /*Intent intent=new Intent(HomeActivity.this,profileActivity.class);
                            startActivity(intent);*/
                            return true;

                        case R.id.navigation_notification:
                           // clearFragment();
                            // openFragment(NotificationFragment.newInstance("", ""));
                            fragment = new NotificationsFragment();
                            loadFragment(fragment);

                            return true;
                        case R.id.navigation_tag:
                            //clearFragment();
                            // openFragment(NotificationFragment.newInstance("", ""));
                            fragment = new ConnRequestTabFragment();
                            loadFragment(fragment);

                            return true;
                    }
                    return false;
                }
            };

    private void callImgeFragment() {
        TagImageFragment fragment=TagImageFragment.newInstance(1,"name");
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.nav_host_fragment, fragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }

   /* @Override
    public void onBackPressed() {
       // super.onBackPressed();

            // if there is a fragment and the back stack of this fragment is not empty,
            // then emulate 'onBackPressed' behaviour, because in default, it is not working
           *//* FragmentManager fm = getSupportFragmentManager();
            for (Fragment frag : fm.getFragments()) {
                if (frag.isVisible()) {
                    FragmentManager childFm = frag.getChildFragmentManager();
                    if (childFm.getBackStackEntryCount() > 0) {
                        childFm.popBackStack();
                        return;
                    }
                }
            }*//*
            //super.onBackPressed();
        BottomNavigationView mBottomNavigationView = findViewById(R.id.nav_view);
       if (mBottomNavigationView.getSelectedItemId() == R.id.navigation_home)
        {
           // super.onBackPressed();

          //  finish();
            try {
                if (!Validations.isEmptyString(parentClassCallBack)){
                    ViewAllPostFragment fragment= ViewAllPostFragment.newInstance(postPosition,viewAllPostList,"parentClassCallBack");
                    addFragment(fragment);
                    parentClassCallBack="";
                }else if (viewAllPost){
                    if (parentClassPost.equals("hashTagClass")){
                        TagImageFragment fragment =  TagImageFragment.newInstance(parentTagId,parentTagname);
                        addFragment(fragment);

                    }else if(parentClassPost.equals("myUploadClass")){
                        myUploadBack=true;
                        fragment = new MyUploadFragment();
                        addFragment(fragment);
                    }else if(parentClassPost.equals("myLikeClass")){
                        myLikeBack=true;
                        fragment = new MyLikesFragment();
                        addFragment(fragment);
                    }else if(parentClassPost.equals("otherPostClass")){
                       *//* ViewAllPostFragment fragment= ViewAllPostFragment.newInstance(postPosition,viewAllPostList,"");
                        addFragment(fragment);*//*
                        Bundle bundle = new Bundle();
                        bundle.putSerializable("viewPost", null);
                        bundle.putInt("position", 0);
                        bundle.putInt("personId", otherPersonId);

                        OtherTabFragment fragment = new OtherTabFragment();
                        fragment.setArguments(bundle);
                        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                        transaction.add(R.id.nav_host_fragment, fragment);
                        transaction.addToBackStack(null);
                        transaction.commit();
                    }

                    viewAllPost=false;
                }else
                if (backTochat){

                    //clearFragment();
                    mBottomNavigationView.setSelectedItemId(R.id.navigation_tag);
                    ConnRequestTabFragment fragment =  ConnRequestTabFragment.newInstance("","chatOpen");
                   loadFragment(fragment);

                    backTochat=false;
                }else {
                    if (otherProfileBack) {
                        otherProfileBack = false;
                        Bundle bundle = new Bundle();
                        bundle.putSerializable("viewPost", null);
                        bundle.putInt("position", 0);
                        bundle.putInt("personId", otherPersonId);

                        OtherTabFragment fragment = new OtherTabFragment();
                        fragment.setArguments(bundle);
                        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                        transaction.replace(R.id.nav_host_fragment, fragment);
                        transaction.addToBackStack(null);
                        transaction.commit();
                    } else if (homeBackClick) {
                        HomeFragment fragment = new HomeFragment();
                        loadFragment(fragment);
                        homeBackClick = false;
                    } else {
                        HomeFragment fragment = new HomeFragment();
                        loadFragment(fragment);
                        homeBackClick = false;
                        //mBottomNavigationView.setSelectedItemId(R.id.navigation_home);
                   *//* new AlertDialog.Builder(this)
                            .setMessage(getString(R.string.Are_you_sure_you_want_to_exit))
                            .setCancelable(false)
                            .setPositiveButton(getString(R.string.Yes), new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    //finishAllActivities();
                                    finish();
                                }
                            })
                            .setNegativeButton(getString(R.string.No), new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    mBottomNavigationView.setSelectedItemId(R.id.navigation_home);
                                }
                            })
                            .show();*//*
                    }
                }
            }catch (Exception e){
                e.printStackTrace();
            }
           // mBottomNavigationView.setSelectedItemId(R.id.navigation_home);
        }else {
            if (mBottomNavigationView.getSelectedItemId() == R.id.navigation_notification) {


                if (otherProfileBack) {
                    otherProfileBack = false;
                    Bundle bundle = new Bundle();
                    bundle.putSerializable("viewPost", null);
                    bundle.putInt("position", 0);
                    bundle.putInt("personId", otherPersonId);

                    OtherTabFragment fragment = new OtherTabFragment();
                    fragment.setArguments(bundle);
                    FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                    transaction.add(R.id.nav_host_fragment, fragment);
                    transaction.addToBackStack(null);
                    transaction.commit();
                }if (likeCount){
                    LikeCountUserFragment fragment= LikeCountUserFragment.newInstance(postId,"");
                    addFragment(fragment);
                    likeCount=false;
                    backValue=0;
                    homeBackClick=true;
                }else if (viewAllPost){
                    Bundle bundle = new Bundle();
                    bundle.putSerializable("viewPost", null);
                    bundle.putInt("position", 1);
                    bundle.putInt("personId", otherPersonId);

                    OtherTabFragment fragment = new OtherTabFragment();
                    fragment.setArguments(bundle);
                    FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                    transaction.add(R.id.nav_host_fragment, fragment);
                    transaction.addToBackStack(null);
                    transaction.commit();
                    viewAllPost=false;
                }else if (parentClassCallBack.equals("SinglePostCallback")){
                    HomeFragment fragment = HomeFragment.newInstance(postId, null,0,"notificationParent");
                    addFragment(fragment);
                    parentClassCallBack="";
                }else if (allNotificationList.size()>0){
                     NotificationsFragment fragment= NotificationsFragment.newInstance(allNotificationList,notiPosition);
                    addFragment(fragment);
                }else{
                   // NotificationsFragment fragment= NotificationsFragment.newInstance(allNotificationList,notiPosition);
                   // loadFragment(fragment);
                    mBottomNavigationView.setSelectedItemId(R.id.navigation_notification);
                }
               *//* if (!notiBack) {
                    notiBack = true;
                }else {
                    notiBack=false;
                    mBottomNavigationView.setSelectedItemId(R.id.navigation_home);
                }*//*
               // clearFragment();
            } else if (mBottomNavigationView.getSelectedItemId()==R.id.navigation_profile){

               if (likeCount)
               {
                   LikeCountUserFragment fragment= LikeCountUserFragment.newInstance(otherPersonId,"");
                   addFragment(fragment);
                   likeCount=false;
               }else if (parentClassCallBack.equals("SinglePostCallback")){
                    todayLike=true;
                   // otherPersonId=postId;
                    HomeFragment fragment= HomeFragment.newInstance(otherPersonId,null,0,"");
                    addFragment(fragment);
                    parentClassCallBack="";
                }else {
                if (likeCount){
                    LikeCountUserFragment fragment= LikeCountUserFragment.newInstance(otherPersonId,"");
                    addFragment(fragment);
                    likeCount=false;
                    backValue=0;
                    homeBackClick=true;
                }else if (!Validations.isEmptyString(parentClassCallBack)){
                    ViewAllPostFragment fragment= ViewAllPostFragment.newInstance(postPosition,viewAllPostList,"parentClassCallBack");
                    addFragment(fragment);
                    parentClassCallBack="";
                }else if (viewAllPost){
                    if (parentClassPost.equals("hashTagClass")){
                        ViewAllPostFragment fragment= ViewAllPostFragment.newInstance(postPosition,viewAllPostList,"hashTagClass");
                        addFragment(fragment);

                    }else if(parentClassPost.equals("myUploadClass")){
                        myUploadBack=true;
                        fragment = new MyUploadFragment();
                        addFragment(fragment);
                    }else if(parentClassPost.equals("myLikeClass")){
                        myLikeBack=true;
                        fragment = new MyLikesFragment();
                        addFragment(fragment);
                    }else if(parentClassPost.equals("otherPostClass")){
                        ViewAllPostFragment fragment= ViewAllPostFragment.newInstance(postPosition,viewAllPostList,"");
                        addFragment(fragment);
                    }

                    viewAllPost=false;
                }else if (myUploadBack){
                    fragment = new SettingFragment();
                    addFragment(fragment);
                    myUploadBack=false;
                }
                else if (myLikeBack){
                    fragment = new SettingFragment();
                    addFragment(fragment);
                    myLikeBack=false;
                }
                else if (settingBlockBack){
                    fragment = new SettingFragment();
                    addFragment(fragment);
                    settingBlockBack=false;
                }else if (todayLike){
                    fragment= SettingFragment.newInstance("todayLike","");
                    addFragment(fragment);
                    todayLike=false;
                }else if (backValueString!=null){

                    fragment = new SettingFragment();
                    addFragment(fragment);
                    backValueString=null;
                }else {
                        mBottomNavigationView.setSelectedItemId(R.id.navigation_profile);
                }
                }
               // clearFragment();
            }else if (mBottomNavigationView.getSelectedItemId()==R.id.navigation_tag){
                //clearFragment();
                if (requestBack) {
                    ConnRequestTabFragment fragment =  ConnRequestTabFragment.newInstance("request","");
                    addFragment(fragment);
                    requestBack=false;
                }else {
                    mBottomNavigationView.setSelectedItemId(R.id.navigation_tag);

                }

            }else {
                mBottomNavigationView.setSelectedItemId(R.id.navigation_home);
                clearFragment();
            }
        }




       // }

    }*/

    @Override
    public void onBackPressed() {
      //  super.onBackPressed();
        int count = getSupportFragmentManager().getBackStackEntryCount();

        if (count == 0) {
           // super.onBackPressed();
            //additional code
            BottomNavigationView mBottomNavigationView = findViewById(R.id.nav_view);
            new AlertDialog.Builder(this)
                    .setMessage(getString(R.string.Are_you_sure_you_want_to_exit))
                    .setCancelable(false)
                    .setPositiveButton(getString(R.string.Yes), new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            //finishAllActivities();
                            //super.onBackPressed();
                            finish();
                        }
                    })
                    .setNegativeButton(getString(R.string.No), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                           // mBottomNavigationView.setSelectedItemId(R.id.navigation_home);

                        }
                    })
                    .show();
        } else {
            getSupportFragmentManager().popBackStack();
        }

    }

    /**
     * loading fragment into FrameLayout
     *
     * @param fragment
     */
    private void loadFragment(Fragment fragment) {
        // load fragment
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.nav_host_fragment, fragment, TAG_FRAGMENT);
        transaction.addToBackStack(null);
        transaction.commit();
    }
    private  void addFragment(Fragment fragment){
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.add(R.id.nav_host_fragment, fragment, TAG_FRAGMENT);
        transaction.addToBackStack(null);
        transaction.commit();
    }
    //for clear backStage entry
    private void clearFragment() {
        // load fragment
        FragmentManager fragmentManager = getSupportFragmentManager();
        //this will clear the back stack and displays no animation on the screen
        fragmentManager.popBackStackImmediate(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
    }

    /**
     * open a camera
     */
    private void dispatchCameraIntent() {

        if ((Build.VERSION.SDK_INT >= 23) && ContextCompat.checkSelfPermission(this,
                Manifest.permission.CAMERA)
                != PackageManager.PERMISSION_GRANTED) {

            // No explanation needed, we can request the permission.
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermissions(new String[]{Manifest.permission.CAMERA},
                        REQUEST_CAMERA_CAPTURE);
            }

            // REQUEST_CAMERA_CAPTURE is an
            // app-defined int constant. The callback method gets the
            // result of the request.
        } else {
            Intent takePictureIntent = new Intent(this, PreviewDemo.class);//new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            /*if (takePictureIntent.resolveActivity(this.getPackageManager()) != null) {

                outputFileUri = getCaptureImageOutputUri();
                if (outputFileUri != null) {
                    takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, outputFileUri);
                }
                startActivityForResult(takePictureIntent, REQUEST_CAMERA_CAPTURE);
            }*/
            startActivityForResult(takePictureIntent, REQUEST_CAMERA_CAPTURE);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(data==null) {
            Log.i(TAG, "onActivityResult: data==null");
            return;
        }

        Log.i(TAG, "onActivityResult: requestcode="+requestCode);
        switch (requestCode){

            case REQUEST_CAMERA_CAPTURE: {

                try {
                    Uri imageUri = getPickImageResultUri(data);
                    File imageFile = new File(imageUri.getPath());
                /*Intent intent = new Intent(this, UploadImageActivity.class);
                intent.putExtra("file", imageFile.getAbsolutePath());
                startActivityForResult(intent, REQUEST_CROP);*/
                    //imgBase64=utility.image_to_Base64(this,imageFile.getAbsolutePath());

                    //Bitmap bitmap=utility.getBitmap(imageFile.getAbsolutePath());

                    FilePathString=imageFile.getAbsolutePath();
                    callFragment(FilePathString);
                    if (Validations.isEmptyString(FilePathString)) {
                        cameraActive = false;
                        // profilePics.setImageBitmap(bitmap);
                    } else {
                        cameraActive = true;

                    }

                    // Bitmap b=imageCropView.getCroppedImage();
                    //bitmapConvertToFile(b);


                /*CropImage.activity(imageUri)
                        .setGuidelines(CropImageView.Guidelines.ON)
                        .start(getActivity(), SettingsFragment.this);*/

                }catch (Exception e){
                    e.printStackTrace();
                }
                break;
            }
            case REQUEST_CROP: {

                //profile_img.setImageDrawable(null);
                Uri uri = data.getData();
                //profile_img.setImageURI(uri);

                utility.getPath(uri,this);
                FilePathString=utility.BitMapToString(bitmap);
                //FilePathString = imageFile.getAbsolutePath();

                break;
            }

            default: {
                Log.i(TAG, "onActivityResult: default");
                break;
            }
        }
        /*if (requestCode == CAMERA_REQUEST && resultCode == Activity.RESULT_OK)
        {
            Bitmap photo = (Bitmap) data.getExtras().get("data");
            imageView.setImageBitmap(photo);
        }*/
    }

    private void callFragment(String filePathString) {
        Bundle bundle = new Bundle();

        bundle.putString("filePath", filePathString );

        UploadFragment fragment=UploadFragment.newInstance(filePathString,postId);
        //fragment.setArguments(bundle);
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.nav_host_fragment, fragment,TAG_FRAGMENT);
        transaction.addToBackStack(null);
        transaction.commit();
        postId=0;
    }

    public Uri getPickImageResultUri(Intent data) {
        boolean isCamera = true;
        if (data != null && data.getData() != null) {
            String action = data.getAction();
            isCamera = action != null && action.equals(MediaStore.ACTION_IMAGE_CAPTURE);
        }
        return isCamera ? getCaptureImageOutputUri() : data.getData();
    }

    private Uri getCaptureImageOutputUri() {
        Uri outputFileUri = null;
        File getImage = getExternalCacheDir();
        if (getImage != null) {
            outputFileUri = Uri.fromFile(new File(getImage.getPath(), "pickImageResult.jpeg"));
        }
        return outputFileUri;
    }


    @Override
    public void retryPageLoad() {

    }

    @Override
    public void onHashTagCall(int tagId, String tagName,String parentClass) {
        parentClassCallBack=parentClass;
        parentTagname=tagName;
        parentTagId=tagId;
        TagImageFragment fragment =  TagImageFragment.newInstance(tagId,tagName);
        addFragment(fragment);

    }

    @Override
    public void onConnectionCall(UserViewPost viewPost,String connectionName,String parentClass) {

        parentClassCallBack=parentClass;
        if (viewPost!=null) {
            if(connectionName.equals("like")||connectionName.equals("connection")) {
                try {

                    Vibrator vibe = (Vibrator) this.getSystemService(Context.VIBRATOR_SERVICE);
                    try {
                        AudioManager audio_mngr = (AudioManager) this.getSystemService(Context.AUDIO_SERVICE);
                        audio_mngr.setRingerMode(AudioManager.RINGER_MODE_VIBRATE);
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                    // Vibrate for 500 milliseconds
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                        vibe.vibrate(VibrationEffect.createOneShot(500, VibrationEffect.DEFAULT_AMPLITUDE));
                    } else {
                        //deprecated in API 26
                        vibe.vibrate(500);
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
            if (connectionName.equals("connection")) {
                if (sessionParam.id != (viewPost.getUserId())) {

                    otherPersonId=viewPost.getUserId();
                        Bundle bundle = new Bundle();
                        bundle.putSerializable("viewPost", viewPost);
                        bundle.putString("connection", connectionName);
                        bundle.putInt("personId", viewPost.getUserId());
                        bundle.putString("header", connectionName);
                        OtherTabFragment fragment = new OtherTabFragment();
                        fragment.setArguments(bundle);
                        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                        transaction.replace(R.id.nav_host_fragment, fragment);
                        transaction.addToBackStack(null);
                        transaction.commit();
                   /* }else {
                        PremiumMsgFragment fragment=  PremiumMsgFragment.newInstance("homeBack",
                                "You must have a premium membership to view their previous posts.",
                                "You are not yet connected to this user.");
                        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                        transaction.replace(R.id.nav_host_fragment, fragment, TAG_FRAGMENT);
                        transaction.addToBackStack(null);
                        transaction.commit();
                    }*/
                }
            }
            if (connectionName.equals("originalPost")){
                Bundle bundle=new Bundle();
                bundle.putInt("obj",viewPost.getId());
                HomeFragment fragment= HomeFragment.newInstance(viewPost.getId(),null,0,"");
                addFragment(fragment);
            }
            if (connectionName.equals("reply")){
                dispatchCameraIntent();
                postId=viewPost.getId();
            }
            if (connectionName.equals("headerLayout")){
                if (sessionParam.id!=viewPost.getUserId()){
                    otherPersonId=viewPost.getUserId();
                    Bundle bundle = new Bundle();
                    bundle.putSerializable("viewPost", viewPost);
                    bundle.putString("connection", connectionName);
                    bundle.putInt("personId", viewPost.getUserId());
                    OtherTabFragment fragment = new OtherTabFragment();
                    fragment.setArguments(bundle);
                    FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                    transaction.replace(R.id.nav_host_fragment, fragment);
                    transaction.addToBackStack(null);
                    transaction.commit();
                }else{
                    newProfileFragment fragment=new newProfileFragment();
                    addFragment(fragment);
                }
            }
        }
    }

    @Override
    public void onMentionCall(UserViewPost viewPost,int personId, int position,String parentClass) {
        parentClassCallBack=parentClass;
        if (sessionParam.id!=(personId)) {
            otherPersonId=personId;
                Bundle bundle = new Bundle();
                bundle.putSerializable("viewPost", viewPost);
                bundle.putInt("position", position);
                bundle.putInt("personId", personId);

                OtherTabFragment fragment = new OtherTabFragment();
                fragment.setArguments(bundle);
                FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                transaction.replace(R.id.nav_host_fragment, fragment);
                transaction.addToBackStack(null);
                transaction.commit();
        }else{
            newProfileFragment fragment=new newProfileFragment();
            addFragment(fragment);
        }

    }

    @Override
    public void onTagName(UserViewPost userViewPost,String parentClass) {

        parentClassCallBack=parentClass;
        if(userViewPost.getUserId().equals(sessionParam.id)){
            newProfileFragment fragment=new newProfileFragment();
            //fragment.setArguments(bundle);
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            transaction.replace(R.id.nav_host_fragment, fragment);
            transaction.addToBackStack(null);
            transaction.commit();
        }else {
            otherPersonId=userViewPost.getUserId();
                Bundle bundle = new Bundle();
                bundle.putSerializable("viewPost", userViewPost);
                bundle.putInt("position", 0);
                bundle.putInt("personId", userViewPost.getUserId());
                OtherTabFragment fragment = new OtherTabFragment();
                fragment.setArguments(bundle);
                FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                transaction.replace(R.id.nav_host_fragment, fragment);
                transaction.addToBackStack(null);
                transaction.commit();

        }

    }

    @Override
    public void onLikeCountUser(UserViewPost userViewPost,String parentClas) {
        parentClassCallBack=parentClas;
        postId=userViewPost.getId();
        LikeCountUserFragment fragment= LikeCountUserFragment.newInstance(userViewPost.getId(),"");
        //fragment.setArguments(bundle);
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.add(R.id.nav_host_fragment, fragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }

    @Override
    public void onButtonClick(String buttonName,String backAction) {
        if (buttonName.equals("locationButton")){
            if(sessionParam.premieUser) {
                LocaleFragment fragment = new LocaleFragment();
                FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                transaction.add(R.id.nav_host_fragment, fragment);
                transaction.addToBackStack(null);
                transaction.commit();
            }else{
                PremiumMsgFragment fragment=  PremiumMsgFragment.newInstance(backAction,
                        "You must have a premium membership to change your location.","Upgrade To Premium");
                FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                transaction.add(R.id.nav_host_fragment, fragment, TAG_FRAGMENT);
                transaction.addToBackStack(null);
                transaction.commit();
            }

        }else if (buttonName.equals("premiumMsg")){
            if(!backTochat) {
                UpgradeLocationFragment fragment = new UpgradeLocationFragment();
                loadFragment(fragment);
            }
        }
        else if (buttonName.equals("premiumUpgrade")){
            PremiumMsgFragment fragment=  PremiumMsgFragment.newInstance(backAction,
                    "Please upgrade to premium to view the users who have liked your post.","Upgrade To Premium");
           addFragment(fragment);
        }else if (buttonName.equals("postNowButton")){
            dispatchCameraIntent();
        }
    }


    @Override
    public void hashTagImageClick(List<UserViewPost> postList,int position) {
        viewAllPost=true;
        viewAllPostList=postList;
        postPosition=position;
        parentClassPost="hashTagClass";
        ViewAllPostFragment fragment= ViewAllPostFragment.newInstance(position,postList,"hashTagClass");
        addFragment(fragment);
    }

    @Override
    public void onTagClickPremium(int tagId,String tagName,String title, String desc) {
        parentTagId=tagId;
        parentTagname=tagName;
        PremiumMsgFragment fragment=  PremiumMsgFragment.newInstance("tagBack",
                desc,title);
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.add(R.id.nav_host_fragment, fragment, TAG_FRAGMENT);
        transaction.addToBackStack(null);
        transaction.commit();
    }

    @Override
    public void PremiumFragmentCall() {

        PremiumMsgFragment fragment=  PremiumMsgFragment.newInstance("",
                "You must be a premium user to use this feature.","Upgrade To Premium");
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.add(R.id.nav_host_fragment, fragment, TAG_FRAGMENT);
        transaction.addToBackStack(null);
        transaction.commit();
    }

    @Override
    public void getReadCount() {
        getBagesCount(bottomNavigation,mContext);
    }

    @Override
    public void likeFragmentCall(List<NotificationList> notiOldList,NotificationList notificationList, int position,String notificationType) {


        allNotificationList=notiOldList;
        notiPosition=position;
        postId=notificationList.getPostId();
        //funcnatality change to show like post
        /*Bundle bundle = new Bundle();
        bundle.putInt("obj", notificationList.getId());
        HomeFragment fragment = HomeFragment.newInstance(notificationList.getPostId(),null);
        loadFragment(fragment);*/
        if (notificationType.equals("tagUser")) {
            if (premiumUser) {
                Bundle bundle = new Bundle();
                bundle.putInt("obj", notificationList.getId());
                HomeFragment fragment = HomeFragment.newInstance(notificationList.getPostId(), null,0,"notificationParent");
                addFragment(fragment);
            } else if (!notificationList.getLocale()) {
                PremiumMsgFragment fragment = PremiumMsgFragment.newInstance("notiBack",
                        "This post is outside of your location."
                        , "Upgrade To Premium To See This Post");
                addFragment(fragment);
            } else if (notificationList.getPostExpired()) {
                PremiumMsgFragment fragment = PremiumMsgFragment.newInstance("notiBack",
                        "This post has expired from the feed."
                        , "Upgrade To Premium To See This Post");
                addFragment(fragment);
            } else {
                Bundle bundle = new Bundle();
                bundle.putInt("obj", notificationList.getId());
                HomeFragment fragment = HomeFragment.newInstance(notificationList.getPostId(), null,0,"notificationParent");
                addFragment(fragment);
            }
        }else{
            Bundle bundle = new Bundle();
            bundle.putInt("obj", notificationList.getId());
            HomeFragment fragment = HomeFragment.newInstance(notificationList.getPostId(), null,0,"notificationParent");
            addFragment(fragment);
        }
    }

    @Override
    public void BackPremiumFragment() {
       // NotificationsFragment fragment=new NotificationsFragment();
        NotificationsFragment fragment= NotificationsFragment.newInstance(allNotificationList,notiPosition);
        addFragment(fragment);
    }

    @Override
    public void upgradeButton(String premium, String premiumDesc,String title) {
        PremiumFragment fragment= PremiumFragment.newInstance("upgradeBack",premiumDesc,title);
        addFragment(fragment);
    }

    @Override
    public void premiumButton(String premium,String premiumDesc) {

        if (premium.equals("homeBack")){
            HomeFragment fragment=new HomeFragment();
            addFragment(fragment);
        }
        if (premium.equals("allPostBack")){

            ViewAllPostFragment fragment= ViewAllPostFragment.newInstance(postPosition,viewAllPostList,parentClassPost);
            addFragment(fragment);
        }
        if (premium.equals("cancel")){
            HomeFragment fragment=new HomeFragment();
            addFragment(fragment);
        }
        if (premium.equals("settingBack")){
            SettingFragment fragment=new SettingFragment();
            addFragment(fragment);
        }
        if (premium.equals("notiBack")){
           // NotificationsFragment fragment=new NotificationsFragment();
             NotificationsFragment fragment= NotificationsFragment.newInstance(allNotificationList,notiPosition);
            addFragment(fragment);
        }
        if (premium.equals("otherTabBack")){
            HomeFragment fragment=new HomeFragment();
            addFragment(fragment);
        }
        if (premium.equals("tagBack")){
            TagImageFragment fragment =  TagImageFragment.newInstance(parentTagId,parentTagname);
            addFragment(fragment);
        }

    }

    @Override
    public void blockUserFragment() {
        HomeFragment fragment=new HomeFragment();
        addFragment(fragment);
    }

    @Override
    public void mySinglePost(List<UserViewPost> post,int position) {
        viewAllPost=true;
        viewAllPostList=post;
        postPosition=position;
        parentClassPost="myUploadClass";
        ViewAllPostFragment fragment= ViewAllPostFragment.newInstance(position,post,"myUploadClass");
        addFragment(fragment);
    }

    @Override
    public void myUploadsBack() {
        SettingFragment fragment=new SettingFragment();
        addFragment(fragment);
    }

    @Override
    public void myLikeFragmentClick(List<UserViewPost> likePost,int position) {
        viewAllPost=true;
        viewAllPostList=likePost;
        postPosition=position;
        parentClassPost="myLikeClass";
        ViewAllPostFragment fragment= ViewAllPostFragment.newInstance(position,likePost,"myLikeClass");
        addFragment(fragment);
    }
    @Override
    public void otherUserPost(List<UserViewPost> otherUserPostList,int position,String otherUserPost) {
        viewAllPost=true;
        viewAllPostList=otherUserPostList;
        postPosition=position;
        parentClassPost="otherPostClass";
        ViewAllPostFragment fragment= ViewAllPostFragment.newInstance(position,otherUserPostList,"otherPostClass");
        addFragment(fragment);
    }


    @Override
    public void likeUpgradeClick() {
        initiatePayment(false);
    }

    @Override
    public void myLikeBack() {
        SettingFragment fragment=new SettingFragment();
        addFragment(fragment);
    }

    //call from Connection fragment to open chat Activity
    @Override
    public void ConnectionChatOpen(ConnListResponse connData) {
        Bundle bundle=new Bundle();
        bundle.putInt("connectionId",connData.getConnectionId());
        bundle.putInt("otherUserId",connData.getUserId());
        bundle.putString("date",connData.getCreatedAt());
        bundle.putString("userName",connData.getUserName());
        bundle.putString("otherUserProfile",connData.getUserImageUrl());
        bundle.putString("connectionStatus",connData.getUserName());
        Intent intent=new Intent(this,ChatActivity.class);
        intent.putExtras(bundle);
        startActivity(intent);
    }

    @Override
    public void userLogoClick(View view, String imageString) {

        switch (view.getId()) {
            case R.id.iv_logo:
                Intent intent = new Intent(this, UploadImageActivity.class);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    ActivityOptionsCompat options = ActivityOptionsCompat.
                            makeSceneTransitionAnimation(this,view, "MyTransition");
                    intent.putExtra("imageString",imageString);
                    startActivity(intent, options.toBundle());
                } else {
                    startActivity(intent);
                }
                break;
        }
    }

    @Override
    public void myProfileClick(View view,String imageString) {
        switch (view.getId()) {
            case R.id.new_profile_logo:
            Intent intent = new Intent(this, UploadImageActivity.class);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                ActivityOptionsCompat options = ActivityOptionsCompat.
                        makeSceneTransitionAnimation(this,view, "MyTransition");
                intent.putExtra("imageString",imageString);
                startActivity(intent, options.toBundle());
            } else {
                startActivity(intent);
            }
            break;
        }

    }



    @Override
    public void otherProfileClick(View view, String imageString) {
        switch (view.getId()){
            case R.id.user_profile_logo:
                Intent intent = new Intent(this, UploadImageActivity.class);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    ActivityOptionsCompat options = ActivityOptionsCompat.
                            makeSceneTransitionAnimation(this,view, "MyTransition");
                    intent.putExtra("imageString",imageString);
                    startActivity(intent, options.toBundle());
                } else {
                    startActivity(intent);
                }
                break;
        }
    }

    @Override
    public void requestLogoClick(View view, String stringImage) {
        switch (view.getId()){
            case R.id.iv_logo:
                Intent intent = new Intent(this, UploadImageActivity.class);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    ActivityOptionsCompat options = ActivityOptionsCompat.
                            makeSceneTransitionAnimation(this,view, "MyTransition");
                    intent.putExtra("imageString",stringImage);
                    startActivity(intent, options.toBundle());
                } else {
                    startActivity(intent);
                }
                break;
        }
    }
    private void setUpBillingClient() {

        billingClient = BillingClient.newBuilder(this).setListener(purchaseUpdateListener)
                .enablePendingPurchases().build();
        billingClient.startConnection(new BillingClientStateListener() {
            @Override
            public void onBillingSetupFinished(@NonNull BillingResult billingResult) {
               /* if (billingResult.getResponseCode() == BillingClient.BillingResponseCode.OK) {
                  //  Toast.makeText(activity, "connected to billing", Toast.LENGTH_SHORT).show();
                } else {
                   // Toast.makeText(activity, ""+billingResult.getResponseCode(), Toast.LENGTH_SHORT).show();
                }*/
                int responseCode = billingResult.getResponseCode();
                String debugMessage = billingResult.getDebugMessage();
                Log.d(TAG, "onBillingSetupFinished: " + responseCode + " " + debugMessage);
                if (responseCode == BillingClient.BillingResponseCode.OK) {
                    // The billing client is ready. You can query purchases here.
                    //querySkuDetails();
                    queryPurchases();
                }
            }

            @Override
            public void onBillingServiceDisconnected() {

            }
        });
    }

    private void queryPurchases() {
        if (!billingClient.isReady()) {
            Log.e(TAG, "queryPurchases: BillingClient is not ready");
        }
        Log.d(TAG, "queryPurchases: SUBS");
        Purchase.PurchasesResult result = billingClient.queryPurchases(BillingClient.SkuType.SUBS);
        if (result == null) {
            Log.i(TAG, "queryPurchases: null purchase result");
            //processPurchases(null);
        } else {
            if (result.getPurchasesList() == null) {
                Log.i(TAG, "queryPurchases: null purchase list");
               // processPurchases(null);
            } else {
                processPurchases(result.getPurchasesList());
                for (Purchase purchase : result.getPurchasesList()) {
                    if (purchase!=null)
                    handlePurchase(purchase);

                }

            }
        }
    }

        private void processPurchases(List<Purchase> purchasesList) {
        if (purchasesList != null) {
            if(purchasesList.size()>0) {
                Log.d(TAG, "processPurchases: " + purchasesList.size() + purchasesList.get(0).getOrderId() + " purchase(s)");
                try {
                    String purchaseToken = purchasesList.get(0).getPurchaseToken();
                    //subscriptionDetails( sku,  purchaseToken);
                    //add user subscription data to local server
                    if (!Validations.isEmptyString(purchaseToken)) {
                        addExpiryDateForMembership(purchaseToken);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    String orderId = purchasesList.get(0).getOrderId();
                    String packageName = purchasesList.get(0).getPackageName();
                    String productId = purchasesList.get(0).getSignature();
                    long purchaseTime = purchasesList.get(0).getPurchaseTime();
                    String purchaseToken = purchasesList.get(0).getPurchaseToken();
                    String developerPayload = purchasesList.get(0).getDeveloperPayload();
                    int purchaseState = purchasesList.get(0).getPurchaseState();
                    boolean acknowledged = purchasesList.get(0).isAcknowledged();
                    boolean autoRenewing = purchasesList.get(0).isAutoRenewing();
                    sessionParam = new SessionParam(this, orderId, packageName, productId, purchaseTime, purchaseToken
                            , developerPayload, purchaseState, acknowledged, autoRenewing);
                } catch (Exception e) {
                    e.printStackTrace();
                }

                Log.d(TAG, "processPurchases: " + purchasesList.size() + purchasesList.get(0).getOrderId() + " purchase(s)");
                String sku = "";


            }
        } else {
            Log.d(TAG, "processPurchases: with no purchases");
        }
        if (isUnchangedPurchaseList(purchasesList)) {
            Log.d(TAG, "processPurchases: Purchase list has not changed");
            return;
        }
       // purchaseUpdateEvent.postValue(purchasesList);
       // purchases.postValue(purchasesList);


        if (purchasesList != null) {
            if (purchasesList.size()>0) {
                logAcknowledgementStatus(purchasesList);
                purchasesUser = purchasesList;
                try {
                    String orderid = purchasesUser.get(0).getOrderId();
                    Long purchaseTime = purchasesUser.get(0).getPurchaseTime();
                    String purchaseToken = purchasesUser.get(0).getPurchaseToken();
                    String develoPaylod = purchasesUser.get(0).getDeveloperPayload();
                    String purchaseJson = purchasesUser.get(0).getOriginalJson();
                    Log.e(TAG, "purchase list data : " + " orderId " + orderid + " purchase Time:" + purchaseTime + " purchaseToken :" + purchaseToken + " developerPayload: " + develoPaylod + " json: " + purchaseJson);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
            Log.d(TAG," purchases Data: orderId:"+sessionParam.orderId+" packageName: "+sessionParam.packageName+" purchasesToken: "+sessionParam.purchaseToken+" developerPayload: "+sessionParam.developerPayload);

        }

    //add user subscription data to local server
    private void addExpiryDateForMembership(String purchaseToken) {
        baseRequest = new BaseRequest(this);
        baseRequest.setBaseRequestListner(new RequestReciever() {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onSuccess(int requestCode, String Json, Object object) {
                try {

                    Gson gson = new Gson();
                    //JSONObject jsonObject = new JSONObject(Json);
                   // String message=jsonObject.getString("message");
                    if (subClick) {
                        if (!sessionParam.premieUser)
                            getPremiumMsg();
                        subClick=false;
                    }
                    //Log.d(TAG,"purchaseToken response: "+message);


                }catch (Exception e){
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int requestCode, String errorCode, String message) {
                //utility.showToast(getBaseContext(),message);
            }

            @Override
            public void onNetworkFailure(int requestCode, String message) {

                utility.showToast(HomeActivity.this,message);
            }
        });

        JsonObject main_object = new JsonObject();
        main_object.addProperty("transactionId", purchaseToken);
        baseRequest.callAPIPostWOLoader(1, main_object, getString(R.string.api_addExpiryDateForMembership));

    }

    private void getPremiumMsg() {
        if(getPremium) {
            if (!backTochat) {
                if(!premiumUser) {
                    //set premium user true
                    sessionParam = new SessionParam(this, true);
       /* AlreadyPremiumFragment fragment=AlreadyPremiumFragment.newInstance("Your payment details will"
                ,"reflect withing 30 mins.");*/
                    AlreadyPremiumFragment fragment = AlreadyPremiumFragment.newInstance("It may take up to 30 minutes for premium"
                            , "features to become available to you.");
                    addFragment(fragment);
                    getPremium = false;
                }
            }
        }
    }


    private void subscriptionDetails(String sku, String purchaseToken) {
       // https://androidpublisher.googleapis.com/androidpublisher/v3/applications/{packageName}/purchases/subscriptions/{subscriptionId}/tokens/{token}
    }

    private void logAcknowledgementStatus(List<Purchase> purchasesList) {
        int ack_yes = 0;
        int ack_no = 0;
        for (Purchase purchase : purchasesList) {
            if (purchase.isAcknowledged()) {
                ack_yes++;
            } else {
                ack_no++;
            }
        }
        Log.d(TAG, "logAcknowledgementStatus: acknowledged=" + ack_yes +
                " unacknowledged=" + ack_no);
    }

    /**
     * Check whether the purchases have changed before posting changes.
     */
    private boolean isUnchangedPurchaseList(List<Purchase> purchasesList) {
        // TODO: Optimize to avoid updates with identical data.
        return false;
    }
    /*private void querySkuDetails() {
        Log.d(TAG,"querySkuDetails");
        List<String> skuList = new ArrayList<>();

        skuList.add("fresherspremiumuser");
        skuList.add("Premium membership");

        SkuDetailsParams.Builder params = SkuDetailsParams.newBuilder();
        params.setSkusList(skuList).setType(BillingClient.SkuType.SUBS);
       // billingClient.querySkuDetailsAsync(params.build(), this);
    }*/
    private PurchasesUpdatedListener purchaseUpdateListener = new PurchasesUpdatedListener() {
        @Override
        public void onPurchasesUpdated(BillingResult billingResult, List<Purchase> purchases) {
            if(billingResult==null){
                Log.wtf(TAG, "onPurchasesUpdated: null BillingResult");
                return;
            }
            int responseCode=billingResult.getResponseCode();
            String debugMessage=billingResult.getDebugMessage();
            switch (responseCode) {
                case BillingClient.BillingResponseCode.OK:
                    if (purchases == null) {
                        Log.d(TAG, "onPurchasesUpdated: null purchase list");
                        //processPurchases(null);
                    } else {
                        if(purchases.size()>0) {
                            processPurchases(purchases);
                        }
                        for (Purchase purchase : purchases) {
                            handlePurchase(purchase);

                        }

                        for (Purchase purchase : purchases) {
                            // TODO: Also check for the other SKU's if needed
                            if (Constant.PREMIUM_SKU.equals(purchase.getSku()) && purchase.getPurchaseState() == Purchase.PurchaseState.PURCHASED && purchase.isAcknowledged()) {
                                // This purchase is purchased and acknowledged, it is currently active!
                                Log.d("HomeActivity","purchases Data: orderId:"+purchase.getOrderId()+"packageName: "+purchase.getPackageName()+"purchasesToken: "+purchase.getPurchaseToken()+"developerPayload: "+purchase.getDeveloperPayload());

                                hasSubscription = true;
                               // callApi
                            }else {
                                hasSubscription=false;
                            }
                        }
                    }
                    break;
                case BillingClient.BillingResponseCode.USER_CANCELED:
                    Log.i(TAG, "onPurchasesUpdated: User canceled the purchase");
                    break;
                case BillingClient.BillingResponseCode.ITEM_ALREADY_OWNED:
                    Log.i(TAG, "onPurchasesUpdated: The user already owns this item");
                    break;
                case BillingClient.BillingResponseCode.DEVELOPER_ERROR:
                    Log.e(TAG, "onPurchasesUpdated: Developer error means that Google Play " +
                            "does not recognize the configuration. If you are just getting started, " +
                            "make sure you have configured the application correctly in the " +
                            "Google Play Console. The SKU product ID must match and the APK you " +
                            "are using must be signed with release keys."
                    );
                    break;
            }
            // To be implemented in a later section.
           /* if (purchases != null) {

                if(billingResult.getResponseCode()== BillingClient.BillingResponseCode.ITEM_ALREADY_OWNED){
                      Toast.makeText(activity, "Item already owned, kindly check login email", Toast.LENGTH_SHORT).show();
                    return;
                }if(billingResult.getResponseCode()== BillingClient.BillingResponseCode.USER_CANCELED){
                     Toast.makeText(activity, "Payment Caneled", Toast.LENGTH_SHORT).show();
                    return;
                }

                if (billingResult.getResponseCode() == BillingClient.BillingResponseCode.OK
                        && purchases != null) {
                    for (Purchase purchase : purchases) {
                        handlePurchase(purchase);
                    }
                } else if (billingResult.getResponseCode() == BillingClient.BillingResponseCode.USER_CANCELED) {
                    // Handle an error caused by a user cancelling the purchase flow.
                } else {
                    // Handle any other error codes.
                }


            }*/

        }
    };
    AcknowledgePurchaseResponseListener acknowledgePurchaseResponseListener =new AcknowledgePurchaseResponseListener() {
        @Override
        public void onAcknowledgePurchaseResponse(@NonNull BillingResult billingResult) {
            int responseCode = billingResult.getResponseCode();
            String debugMessage = billingResult.getDebugMessage();
            Log.d(TAG, "acknowledgePurchase: " + responseCode + " " + debugMessage);
        }
    };

    void handlePurchase(Purchase purchase) {
        // Purchase retrieved from BillingClient#queryPurchases or your PurchasesUpdatedListener.
        //Purchase purchase = ...;

        // Verify the purchase.
        // Ensure entitlement was not already granted for this purchaseToken.
        // Grant entitlement to the user.

      /*  ConsumeParams consumeParams =
                ConsumeParams.newBuilder()
                        .setPurchaseToken(purchase.getPurchaseToken())
                        .build();

        ConsumeResponseListener listener = new ConsumeResponseListener() {
            @Override
            public void onConsumeResponse(BillingResult billingResult, String purchaseToken) {
                if (billingResult.getResponseCode() == BillingClient.BillingResponseCode.OK) {
                    // Handle the success of the consume operation.
                }
            }
        };*/
        if (purchase.getPurchaseState() == Purchase.PurchaseState.PURCHASED) {
            if (!purchase.isAcknowledged()) {
                AcknowledgePurchaseParams acknowledgePurchaseParams =
                        AcknowledgePurchaseParams.newBuilder()
                                .setPurchaseToken(purchase.getPurchaseToken())
                                .build();
                billingClient.acknowledgePurchase(acknowledgePurchaseParams, acknowledgePurchaseResponseListener);
                Log.d(TAG,"purchaseToken: "+purchase.getPurchaseToken()+" purchaseOrderId: "+purchase.getOrderId());
            }
        }



      //  billingClient.consumeAsync(consumeParams, listener);
    }

    private void initiatePayment(final boolean z) {
        try {
            if (billingClient.isReady()) {
                List<String> skuList = new ArrayList<>();

                skuList.add("fresherspremiumuser");
                skuList.add("Premium membership");

                SkuDetailsParams.Builder params = SkuDetailsParams.newBuilder();
                params.setSkusList(skuList).setType(BillingClient.SkuType.SUBS);
                billingClient.querySkuDetailsAsync(params.build(), new SkuDetailsResponseListener() {
                    @Override
                    public void onSkuDetailsResponse(@NonNull BillingResult billingResult, @Nullable List<SkuDetails> list) {
                        HomeActivity act_ImageMap = HomeActivity.this;
                        int responseCode = billingResult.getResponseCode();
                        String debugMessage = billingResult.getDebugMessage();
                        act_ImageMap.skuDetailsListGlobal = list;
                        assert list != null;
                        if (billingResult.getResponseCode() == BillingClient.BillingResponseCode.OK && list.size() > 0) {
                            BillingFlowParams build = BillingFlowParams.newBuilder().setSkuDetails(HomeActivity.this.skuDetailsListGlobal.get(0)).build();
                            if (!z) {
                                HomeActivity.this.billingClient.launchBillingFlow(activity, build);
                                Log.d(TAG,"launchBillingFlow: sku: " + build.getSku() + ", oldSku: " + build.getOldSku());
                                return;
                            }
                            return;
                            // Toast.makeText(mContext, "cannot load product", Toast.LENGTH_SHORT).show();
                        }else {
                            switch (responseCode) {
                               /* case BillingClient.BillingResponseCode.OK:
                                    Log.i(TAG, "onSkuDetailsResponse: " + responseCode + " " + debugMessage);
                                    if (skuDetailsList == null) {
                                        Log.w(TAG, "onSkuDetailsResponse: null SkuDetails list");
                                        skusWithSkuDetails.postValue(Collections.<String, SkuDetails>emptyMap());
                                    } else {
                                        Map<String, SkuDetails> newSkusDetailList = new HashMap<String, SkuDetails>();
                                        for (SkuDetails skuDetails : skuDetailsList) {
                                            newSkusDetailList.put(skuDetails.getSku(), skuDetails);
                                        }
                                        skusWithSkuDetails.postValue(newSkusDetailList);
                                        Log.i(TAG, "onSkuDetailsResponse: count " + newSkusDetailList.size());
                                    }
                                    break;*/
                                case BillingClient.BillingResponseCode.SERVICE_DISCONNECTED:
                                case BillingClient.BillingResponseCode.SERVICE_UNAVAILABLE:
                                case BillingClient.BillingResponseCode.BILLING_UNAVAILABLE:
                                case BillingClient.BillingResponseCode.ITEM_UNAVAILABLE:
                                case BillingClient.BillingResponseCode.DEVELOPER_ERROR:
                                case BillingClient.BillingResponseCode.ERROR:
                                    Log.e(TAG, "onSkuDetailsResponse: " + responseCode + " " + debugMessage);
                                    break;
                                case BillingClient.BillingResponseCode.USER_CANCELED:
                                    Log.i(TAG, "onSkuDetailsResponse: " + responseCode + " " + debugMessage);
                                    break;
                                // These response codes are not expected.
                                case BillingClient.BillingResponseCode.FEATURE_NOT_SUPPORTED:
                                case BillingClient.BillingResponseCode.ITEM_ALREADY_OWNED:
                                case BillingClient.BillingResponseCode.ITEM_NOT_OWNED:
                                default:
                                    Log.wtf(TAG, "onSkuDetailsResponse: " + responseCode + " " + debugMessage);
                            }
                        }
                        /*if (billingResult.getResponseCode() == BillingClient.BillingResponseCode.OK && skuDetailsList != null) {
                            for (SkuDetails skuDetails : skuDetailsList) {
                                String sku = skuDetails.getSku();
                                String price = skuDetails.getPrice();

                                flowParams = BillingFlowParams.newBuilder().setSkuDetails(skuDetailsList.get(0)).build();


                                if ("coins_5".equals(sku)) {
                                    coins5Price = price;
                                } else if ("coins_15".equals(sku)) {
                                    coins15Price = price;
                                }
                            }


                        }*/

                    }
                });
            }
        }catch (Exception e){
        e.printStackTrace();
        }
    }

    @Override
    public void localeBackClick() {
        onBackPressed();
    }

    @Override
    public void goPremiumButton(String premium,String TextValue,String title) {
        if (premium.equals("upgrade")){
            initiatePayment(false);
        }
        if (premium.equals("cancel")){
            PremiumMsgFragment fragment=PremiumMsgFragment.newInstance("",TextValue,title);
            addFragment(fragment);
        }
        if (premium.equals("upgradeBack")){
            PremiumMsgFragment fragment=PremiumMsgFragment.newInstance(premium,TextValue,title);
            addFragment(fragment);
        }

        if (premium.equals("profileBack")){
            newProfileFragment fragment=newProfileFragment.newInstance("backReturn","");
            addFragment(fragment);
        }
        if (premium.equals("settingBack")){
            SettingFragment fragment=new SettingFragment();
            addFragment(fragment);
        }
        if (premium.equals("HomeBack")){
            HomeFragment fragment=new HomeFragment();
            addFragment(fragment);
        }
    }

    @Override
    public void newProfileClick(MyProfileResponse myProfile, String clickMenu) {
            settingClick=true;
            fragment=new SettingFragment();
            addFragment(fragment);
    }

    @Override
    public void alreadyPremiumBack() {
        fragment=new SettingFragment();
        addFragment(fragment);
    }

    @Override
    public void PremiumBackHome() {
        clearFragment();
        HomeFragment fragment=new HomeFragment();
        addFragment(fragment);
        onBackPressed();
    }

    @Override
    public void settingClick(String clickMenu,String postId) {
        backValue=1;
        backValueString=clickMenu;
                fragment = newProfileFragment.newInstance("", "profileEdit");
                addFragment(fragment);
    }

    @Override
    public void settingMyUpload(String clickName, boolean UploadBack) {
        myUploadBack=UploadBack;
            fragment = new MyUploadFragment();
            addFragment(fragment);
    }

    @Override
    public void settingMyLike(String clickName, boolean likeBack) {
        myLikeBack=likeBack;
        if (premiumUser) {
            fragment = new MyLikesFragment();
            addFragment(fragment);
        } else {
            fragment = PremiumMsgFragment.newInstance("settingBack",
                    "You must have a premium membership " +
                            "to view your previous liked posts"
                    , "Upgrade To Premium");
            addFragment(fragment);
        }
    }

    @Override
    public void settingBlockUser(String clickName, boolean blockBack) {
        settingBlockBack=blockBack;
        fragment = new BlockUserFragment();
        addFragment(fragment);
    }

    @Override
    public void settingPremiumUser(String clickName, String postId) {
        if (premiumUser) {
            fragment = new AlreadyPremiumFragment();
            addFragment(fragment);
        } else {
            fragment = PremiumFragment.newInstance("settingBack", "", "");
            addFragment(fragment);
        }
    }

    @Override
    public void settingTodayPremium(String clickName, String postId) {

            fragment = PremiumMsgFragment.newInstance("settingBack",
                    "You must have a premium membership to view the top liked post.",
                    "Upgrade To Premium");
            addFragment(fragment);

    }

    @Override
    public void todayLike(View view, String imageString,int todayPostId) {
        todayLike=true;
        postId=todayPostId;
        HomeFragment fragment= HomeFragment.newInstance(todayPostId,null,0,"SinglePostCallback");
           addFragment(fragment);

           /* Intent intent = new Intent(this, UploadImageActivity.class);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                ActivityOptionsCompat options = ActivityOptionsCompat.
                        makeSceneTransitionAnimation(this,view, "MyTransition");
                intent.putExtra("imageString",imageString);
                startActivity(intent, options.toBundle());
            } else {
                startActivity(intent);
            }*/
    }

    @Override
    public void userSinglePost(UserViewPost post, int personId) {
        otherProfileBack=true;
        otherPersonId=personId;

        HomeFragment fragment= HomeFragment.newInstance(post.getId(),null,0,"");
        addFragment(fragment);

    }

    @Override
    public void likeByUser(LikeByUser like,int likePostId) {
        likeCount=true;
        postId=likePostId;
        otherPersonId=like.getUserId();
        Bundle bundle = new Bundle();
        bundle.putSerializable("viewPost", null);
        bundle.putInt("position", 0);
        bundle.putInt("personId", like.getUserId());
        OtherTabFragment fragment = new OtherTabFragment();
        fragment.setArguments(bundle);
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.add(R.id.nav_host_fragment, fragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }
    //requestUserProfile
    public void requestUserProfile(PendingRequestList requestList){
        requestBack=true;
        otherPersonId=requestList.getUserId();
        Bundle bundle = new Bundle();
        bundle.putSerializable("viewPost", null);
        bundle.putInt("position", 0);
        bundle.putInt("personId", requestList.getUserId());
        bundle.putString("connection", "connection");
        OtherTabFragment fragment = new OtherTabFragment();
        fragment.setArguments(bundle);
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.nav_host_fragment, fragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }

    //show premium msg
    @Override
    public void upgradeLocMsgSHow() {

        PremiumFragment fragment= PremiumFragment.newInstance("HomeBack", "", "");
        addFragment(fragment);
    }
}
