package com.chetaru.FreshersApp.view.ui.Fragment;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.view.menu.MenuAdapter;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.chetaru.FreshersApp.R;
import com.chetaru.FreshersApp.service.model.DefaultData;
import com.chetaru.FreshersApp.service.model.LocaleData;
import com.chetaru.FreshersApp.service.model.MyProfileResponse;
import com.chetaru.FreshersApp.service.model.UserLoginRequest;
import com.chetaru.FreshersApp.service.model.UserViewPost;
import com.chetaru.FreshersApp.service.retrofit.BaseRequest;
import com.chetaru.FreshersApp.service.retrofit.Functions;
import com.chetaru.FreshersApp.service.retrofit.RequestReciever;
import com.chetaru.FreshersApp.utility.BaseActivity;
import com.chetaru.FreshersApp.utility.MarshMallowPermission;
import com.chetaru.FreshersApp.utility.SessionParam;
import com.chetaru.FreshersApp.utility.Utility;
import com.chetaru.FreshersApp.utility.Validations;

import com.chetaru.FreshersApp.view.adapter.LocaleAdapter;
import com.chetaru.FreshersApp.view.adapter.TodayLikeAdapter;
import com.chetaru.FreshersApp.view.customView.ItemOffsetDecoration;
import com.chetaru.FreshersApp.view.ui.Activity.RootActivity;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Optional;
import butterknife.Unbinder;
import id.zelory.compressor.Compressor;
import im.delight.android.location.SimpleLocation;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link newProfileFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class newProfileFragment extends Fragment implements View.OnClickListener, AdapterView.OnItemSelectedListener {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1="";
    private String mParam2="";
    //check sdk version
    final int sdk = android.os.Build.VERSION.SDK_INT;
    //set constant value for image click
    private int TakePicture = 1, SELECT_FILE = 2, RESULT_OK = -1;
    //check permission
    MarshMallowPermission marshMallowPermission;
    //handel image bitmap and string value
    private Bitmap bitmap = null;
    private String imgBase64 = "";
    private File sourceFile;

    //variable initilization
    private Boolean resultImage = false;
    private int userId;
    private BaseRequest baseRequest;
    Integer mYear, mMonth, mDay;
    //model instance
    MyProfileResponse myProfile;

    double longitude = 0.0;
    double latitude = 0.0;

    //handel class
    Utility utility;
    SessionParam sessionParam;
    SimpleLocation location;
    List<LocaleData> localeList;
    List<String> todayList;
    String profileImageString = "";
    Boolean expand = false,loginExpand=false;
    boolean editable = false;
    Integer gender,apiGender;
    int likeCount;

    //assign a layout id class
    private Unbinder unbinder;

    //initialize viewa
    @BindView(R.id.title_name_txt)
    TextView TitleNameTxt;
    @BindView(R.id.show_user_layout)
    LinearLayout showProfileLayout;



    @BindView(R.id.setting_image_view)
    ImageView settingImageView;
    @BindView(R.id.new_profile_logo)
    ImageView profileLogo;
    @Nullable
    @BindView(R.id.profile_male_image)
    ImageView maleIconImage;
    @Nullable
    @BindView(R.id.profile_female_image)
    ImageView femaleIconImage;
    @Nullable
    @BindView(R.id.profile_other_image)
    ImageView otherIconImage;
    @BindView(R.id.profile_male_text)
    TextView maleText;
    @BindView(R.id.profile_female_text)
    TextView femaleText;
    @BindView(R.id.profile_other_text)
    TextView otherText;
    @BindView(R.id.profile_userId_txt)
    TextView profileUserName;
    @BindView(R.id.profile_name_txt)
    EditText profileName;
    @BindView(R.id.profile_address)
    TextView profileAddress;
    @BindView(R.id.locale_spinner)
    Spinner localeSpinner;
    @BindView(R.id.profile_bio)
    TextView profileBio;
    @BindView(R.id.my_profile_image_cancel)
    Button cancelImage;
    @BindView(R.id.my_profile_image_done)
    Button doneImage;
    @BindView(R.id.edit_layout)
    LinearLayout editLayout;
    boolean setGender=false;
    List<String> localeName;
    ArrayAdapter<String> adapter;
    //TodayLikeAdapter mAdapter;
    int position=0;
    boolean premiumUser=false;
    String defaultName="",defaultUserName="",defaultLocaleName="",defaultBio="";
    int defaultGender=0;


    newProfileListener mListener;


    public newProfileFragment() {
        // Required empty public constructor
    }


    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment newProfileFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static newProfileFragment newInstance(String param1, String param2) {
        newProfileFragment fragment = new newProfileFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_new_profile, container, false);
        unbinder = ButterKnife.bind(this, view);
        utility = new Utility();
        localeName=new ArrayList<>();
        sessionParam = new SessionParam(getContext());
        userId = sessionParam.id;
        premiumUser=sessionParam.premieUser;
        defaultName=sessionParam.name;
        defaultUserName=sessionParam.userName;
        defaultLocaleName=sessionParam.localeName;
        defaultBio=sessionParam.bio;
        profileImageString=sessionParam.profileImage;
        profileName.setText(defaultName);
        profileUserName.setText(defaultUserName);
        profileBio.setText(defaultBio);
        gender=sessionParam.gender;
        profileLogo.setImageResource(R.drawable.profile_pics);
        if (!Validations.isEmptyString(profileImageString)) {
            Glide.with(getContext()).load(profileImageString).placeholder(R.drawable.profile_pics).into(profileLogo);
        }else {
            Glide.with(getContext()).load(R.drawable.profile_pics).into(profileLogo);
        }



        List<UserLoginRequest> loginUserList=sessionParam.retriveLoginData(getContext());
       if (loginUserList!=null)
       for (int i=0;i<loginUserList.size();i++) {
           Log.d("loginData", loginUserList.get(i).getEmail());
       }
        todayList=new ArrayList<>();
        location = new SimpleLocation(getContext());
        maleIconImage.setOnClickListener(this);
        femaleIconImage.setOnClickListener(this);
        otherIconImage.setOnClickListener(this);
        settingImageView.setOnClickListener(this);
        profileLogo.setOnClickListener(this);

        TitleNameTxt.setText("Profile");

        // Set Horizontal Layout Manager
        // for Recycler view
      /*  LinearLayoutManager layoutManager = new LinearLayoutManager( getContext(),LinearLayoutManager.HORIZONTAL,false);
        likeRecyclerView.setLayoutManager(layoutManager);
        likeRecyclerView.setHasFixedSize(true);
        ItemOffsetDecoration itemDecoration = new ItemOffsetDecoration(getContext(), R.dimen.item_offset_medium);
        likeRecyclerView.addItemDecoration(itemDecoration);
        mAdapter=new TodayLikeAdapter(todayList,getContext(),likeCount);
        likeRecyclerView.setAdapter(mAdapter);*/
        marshMallowPermission = new MarshMallowPermission(getActivity());
        if (!marshMallowPermission.checkPermissionForCamera() && !marshMallowPermission.checkPermissionForExternalStorage() && marshMallowPermission.checkPermissionForFineLoaction()) {
            marshMallowPermission.requestPermissionForCamera();
            marshMallowPermission.requestPermissionForExternalStorage();
            marshMallowPermission.requestPermissionForFineLoaction();
        } else if (!marshMallowPermission.checkPermissionForCamera()) {
            marshMallowPermission.requestPermissionForCamera();
        } else if (!marshMallowPermission.checkPermissionForExternalStorage()) {
            marshMallowPermission.requestPermissionForExternalStorage();
        } else if (!marshMallowPermission.checkPermissionForFineLoaction()) {
            marshMallowPermission.checkPermissionForFineLoaction();
        }
        profileName.setEnabled(false);
        profileAddress.setEnabled(false);
        profileBio.setEnabled(false);
        maleIconImage.setEnabled(false);
        femaleIconImage.setEnabled(false);
        otherIconImage.setEnabled(false);
        showProfileLayout.setVisibility(View.VISIBLE);
        //detailLayout.setVisibility(View.GONE);


        getProfile();
        //defaultSelect();
        if (premiumUser){
            localeSpinner.setEnabled(true);
            localeSpinner.setClickable(true);
        }else {
            localeSpinner.setEnabled(false);
            localeSpinner.setClickable(false);
        }

        try{
            if (mParam2.equals("profileEdit")){
                editable=true;
                TitleNameTxt.setText("Profile");
                settingImageView.setVisibility(View.VISIBLE);
                showProfileLayout.setVisibility(View.VISIBLE);
                cancelImage.setVisibility(View.VISIBLE);
                doneImage.setVisibility(View.VISIBLE);
                editLayout.setVisibility(View.VISIBLE);
                editable = true;
                if (!marshMallowPermission.checkPermissionForCamera() && !marshMallowPermission.checkPermissionForExternalStorage()) {
                    marshMallowPermission.requestPermissionForCamera();
                    marshMallowPermission.requestPermissionForExternalStorage();
                } else if (!marshMallowPermission.checkPermissionForCamera()) {
                    marshMallowPermission.requestPermissionForCamera();
                } else if (!marshMallowPermission.checkPermissionForExternalStorage()) {
                    marshMallowPermission.requestPermissionForExternalStorage();
                }
                setGender=true;
                if (gender == 1) {
                    selectMale();
                } else if (gender == 2) {
                    selectFemale();
                } else if (gender == 3) {
                    selectOther();

                }


                maleIconImage.setVisibility(View.VISIBLE);
                    maleText.setVisibility(View.VISIBLE);
                    femaleIconImage.setVisibility(View.VISIBLE);
                    femaleText.setVisibility(View.VISIBLE);
                    otherIconImage.setVisibility(View.VISIBLE);
                    otherText.setVisibility(View.VISIBLE);

                    profileName.setEnabled(true);
                    profileBio.setEnabled(true);
                    //et_email.setEnabled(true);
                    profileAddress.setEnabled(true);
                    profileLogo.setEnabled(true);
                    maleIconImage.setEnabled(true);

                    femaleIconImage.setEnabled(true);
                    otherIconImage.setEnabled(true);

            }
        }catch (Exception e){
            e.printStackTrace();
        }
        adapter = new ArrayAdapter<String>(getContext(),
                android.R.layout.simple_spinner_dropdown_item, localeName);
      //  adapter.setDropDownViewResource(R.layout.spinner_dropdown_item);
        localeSpinner.setOnItemSelectedListener(this);
        
        return view;
    }



    private void getProfile() {
        baseRequest = new BaseRequest(getContext());
        baseRequest.setBaseRequestListner(new RequestReciever() {
            @Override
            public void onSuccess(int requestCode, String Json, Object object) {
                try {

                    Gson gson = new Gson();
                    JSONObject jsonObject = new JSONObject(Json);
                    JSONObject response = (JSONObject) jsonObject.get("data");
                   /* try {
                        sessionParam = new SessionParam((JSONObject) response);
                        sessionParam.persist(getContext());
                    }catch (Exception e){
                        e.printStackTrace();
                    }*/
                    myProfile = gson.fromJson(response.toString(), MyProfileResponse.class);
                    setUserDetail(myProfile);
                    sessionParam = new SessionParam(getContext(),myProfile);
                    getLocale();
                    // sessionParam.persist(getContext());

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int requestCode, String errorCode, String message) {
              //  utility.showToast(getContext(), message);
            }

            @Override
            public void onNetworkFailure(int requestCode, String message) {

                utility.showToast(getContext(), message);
            }
        });



        JsonObject main_object = new JsonObject();
        main_object.addProperty("userId",sessionParam.id );
        baseRequest.callAPIPostWOLoader(1, main_object, getString(R.string.api_getUserProfile));
    }

    //get locale data
    private void updateUsersLocale(LocaleData newLocale) {

        baseRequest = new BaseRequest(getContext());
        baseRequest.setBaseRequestListner(new RequestReciever() {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onSuccess(int requestCode, String Json, Object object) {
                try {

                    Gson gson = new Gson();
                    JSONObject jsonObject = new JSONObject(Json);
                    //JSONObject response= (JSONObject) jsonObject.get("data");
                    sessionParam=new SessionParam(getContext() ,newLocale);


                }catch (Exception e){
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int requestCode, String errorCode, String message) {

                //utility.showToast(getContext(),message);
            }

            @Override
            public void onNetworkFailure(int requestCode, String message) {

                utility.showToast(getContext(),message);
            }
        });

        JsonObject main_object = new JsonObject();
        main_object.addProperty("localeId", newLocale.getId());
        baseRequest.callAPIPostWOLoader(1, main_object, getString(R.string.api_updateUsersLocale));

    }
    //get locale data
    private void getLocale() {

        baseRequest = new BaseRequest(getContext());
        baseRequest.setBaseRequestListner(new RequestReciever() {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onSuccess(int requestCode, String Json, Object object) {
                try {

                    Gson gson = new Gson();
                    JSONObject jsonObject = new JSONObject(Json);
                    //JSONObject response= (JSONObject) jsonObject.get("data");
                    JSONArray jsonArray = new JSONArray(object.toString());
                    localeList = baseRequest.getDataList(jsonArray, LocaleData.class);


                    for (int i=0;i<localeList.size();i++){

                        localeName.add(localeList.get(i).getName());
                        if (sessionParam.localeId==(localeList.get(i).getId())){
                           position=i;
                        }
                    }
                    localeSpinner.setAdapter(adapter);
                    if (sessionParam.localeId>0)
                    localeSpinner.setSelection(position);

                }catch (Exception e){
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int requestCode, String errorCode, String message) {

                utility.showToast(getContext(),message);
            }

            @Override
            public void onNetworkFailure(int requestCode, String message) {

                utility.showToast(getContext(),message);
            }
        });

        JsonObject object = Functions.getClient().getJsonMapObject(
                "","");
        baseRequest.callAPIPostWOLoader(1, object, getString(R.string.api_getAllLocales));

    }


    private void profileChange(String profileNameChange, String bod, String profileBioChange) {
        baseRequest = new BaseRequest(getContext());
        baseRequest.setBaseRequestListner(new RequestReciever() {
            @Override
            public void onSuccess(int requestCode, String Json, Object object) {
                try {

                    editable=false;
                    Gson gson = new Gson();
                    JSONObject jsonObject = new JSONObject(Json);

                    JSONObject response= (JSONObject) jsonObject.get("data");
                   /* try {
                        sessionParam = new SessionParam((JSONObject) response);
                        sessionParam.persist(getContext());
                    }catch (Exception e){
                        e.printStackTrace();
                    }*/
                    myProfile = gson.fromJson(response.toString(), MyProfileResponse.class);
                    changeUserDetail(myProfile);
                    sessionParam=new SessionParam(getContext(),myProfile);
                   // sessionParam.persist(getContext());
                    profileImageString=myProfile.getProfileImage();
                    // Glide.with(getContext()).load(myProfile.getProfileImage()).apply(new RequestOptions().centerCrop().placeholder(R.drawable.profile_pics)).into(profileLogo);




                }catch (Exception e){
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int requestCode, String errorCode, String message) {
               // utility.showToast(getContext(),message);
            }

            @Override
            public void onNetworkFailure(int requestCode, String message) {

                utility.showToast(getContext(),message);
            }
        });




        JsonObject object = Functions.getClient().getJsonMapObject(
                "name",profileNameChange,
                "bio",profileBioChange,
                "gender",gender+"",
                "type","",
                "countryId","",
               /* "latitude",latitude+"",
                "longitude",longitude+"",*/
                "profileImage",imgBase64+"" );
        baseRequest.callAPIPostWOLoader(1, object, getString(R.string.api_updateUserProfile));


    }

    private void defaultSelect() {
        if (sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
            otherIconImage.setImageResource( R.drawable.other_gray);
            maleIconImage.setImageResource( R.drawable.male);
            femaleIconImage.setImageResource( R.drawable.femalegray);
        } else {
            otherIconImage.setImageResource( R.drawable.other_gray);
            maleIconImage.setImageResource( R.drawable.male);
            femaleIconImage.setImageResource( R.drawable.femalegray);
        }
    }

    private void selectOther() {
        gender = 3;
        if (sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {

            otherIconImage.setImageResource(R.drawable.other);
            maleIconImage.setImageResource( R.drawable.malegray);
            femaleIconImage.setImageResource( R.drawable.femalegray);
        } else {
            otherIconImage.setImageResource( R.drawable.other);
            maleIconImage.setImageResource( R.drawable.malegray);
            femaleIconImage.setImageResource( R.drawable.femalegray);
        }
    }

    private void selectMale() {
        gender = 1;
        if (sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
            otherIconImage.setImageResource(R.drawable.other_gray);
            maleIconImage.setImageResource(R.drawable.male);
            femaleIconImage.setImageResource(R.drawable.femalegray);

              } else {
            otherIconImage.setImageResource(R.drawable.other_gray);
            maleIconImage.setImageResource(R.drawable.male);
            femaleIconImage.setImageResource(R.drawable.femalegray);
        }
    }

    private void selectFemale() {
        gender = 2;

        if (sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
            otherIconImage.setImageResource( R.drawable.other_gray);
            maleIconImage.setImageResource( R.drawable.malegray);
            femaleIconImage.setImageResource( R.drawable.female);
        } else {
            otherIconImage.setImageResource( R.drawable.other_gray);
            maleIconImage.setImageResource( R.drawable.malegray);
            femaleIconImage.setImageResource( R.drawable.female);
        }
    }

    private void setUserDetail(MyProfileResponse myProfile) {
        profileImageString = myProfile.getProfileImage();

        //Glide.with(getContext()).load(myProfile.getProfileImage()).apply(new RequestOptions().centerCrop().placeholder(R.drawable.profile_pics)).into(profileLogo);
        //Picasso.with(getContext()).load(myProfile.getProfileImage()).into(profileLogo);
        Glide.with(getContext()).load(myProfile.getProfileImage()).placeholder(R.drawable.profile_pics).into(profileLogo);

        profileUserName.setText(myProfile.getUserName());
        profileName.setText(myProfile.getName());
        profileBio.setText(myProfile.getBio());
        profileAddress.setText(myProfile.getLocaleName());
        gender = myProfile.getGender();
        apiGender = myProfile.getGender();
       // latitude = Double.parseDouble(myProfile.getLatitude());
        //longitude = Double.parseDouble(myProfile.getLongitude());
        premiumUser=myProfile.getPremieUser();
        if (premiumUser){
            localeSpinner.setEnabled(true);
            localeSpinner.setClickable(true);
        }else {
            localeSpinner.setEnabled(false);
            localeSpinner.setClickable(false);
        }
        if (!setGender) {
            if (gender == 1) {
                maleIconImage.setVisibility(View.VISIBLE);
                maleText.setVisibility(View.VISIBLE);
                selectMale();
                femaleIconImage.setVisibility(View.GONE);
                femaleText.setVisibility(View.GONE);
                otherIconImage.setVisibility(View.GONE);
                otherText.setVisibility(View.GONE);
            } else if (gender == 2) {
                femaleIconImage.setVisibility(View.VISIBLE);
                femaleText.setVisibility(View.VISIBLE);
                selectFemale();
                maleIconImage.setVisibility(View.GONE);
                maleText.setVisibility(View.GONE);
                otherIconImage.setVisibility(View.GONE);
                otherText.setVisibility(View.GONE);
            } else if (gender == 3) {
                otherIconImage.setVisibility(View.GONE);
                otherText.setVisibility(View.GONE);
                selectOther();
                maleIconImage.setVisibility(View.GONE);
                maleText.setVisibility(View.GONE);
                femaleIconImage.setVisibility(View.GONE);
                femaleText.setVisibility(View.GONE);
            }
        }
        likeCount = myProfile.getLikedUserCount();

       // mAdapter.setCount(likeCount);
        try{
            if (mParam2.equals("profileEdit")){
                maleIconImage.setVisibility(View.VISIBLE);
                maleText.setVisibility(View.VISIBLE);
                femaleIconImage.setVisibility(View.VISIBLE);
                femaleText.setVisibility(View.VISIBLE);
                otherIconImage.setVisibility(View.VISIBLE);
                otherText.setVisibility(View.VISIBLE);
            }
        }catch (Exception e){
            e.printStackTrace();
        }

        try {
            if (!Validations.isEmptyString(myProfile.getPostImageUrl())) {
                // ArrayList aList= new ArrayList(Arrays.asList(myProfile.getPostImageUrl().split(",")));
                // List<String> items = Arrays.asList(myProfile.getPostImageUrl().split(","));
                todayList = Arrays.asList(myProfile.getPostImageUrl().split("\\s*,\\s*"));
               // mAdapter.addAll(todayList);
            }
        }catch (Exception e){
            e.printStackTrace();
        }
       // mAdapter.notifyDataSetChanged();



    }
    private void changeUserDetail(MyProfileResponse myProfile) {
        profileImageString = myProfile.getProfileImage();

        //Glide.with(getContext()).load(myProfile.getProfileImage()).apply(new RequestOptions().centerCrop().placeholder(R.drawable.profile_pics)).into(profileLogo);
        //Picasso.with(getContext()).load(myProfile.getProfileImage()).into(profileLogo);
        Glide.with(getContext()).load(myProfile.getProfileImage()).placeholder(R.drawable.profile_pics).into(profileLogo);

        profileUserName.setText(myProfile.getUserName());
        profileName.setText(myProfile.getName());
        profileBio.setText(myProfile.getBio());
        profileAddress.setText(myProfile.getLocaleName());
        gender = myProfile.getGender();
        latitude = Double.parseDouble(myProfile.getLatitude());
        longitude = Double.parseDouble(myProfile.getLongitude());
        if (gender == 1) {
            maleIconImage.setVisibility(View.VISIBLE);
            maleText.setVisibility(View.VISIBLE);
            selectMale();
            femaleIconImage.setVisibility(View.GONE);
            femaleText.setVisibility(View.GONE);
            otherIconImage.setVisibility(View.GONE);
            otherText.setVisibility(View.GONE);
        } else if (gender == 2) {
            femaleIconImage.setVisibility(View.VISIBLE);
            femaleText.setVisibility(View.VISIBLE);
            selectFemale();
            maleIconImage.setVisibility(View.GONE);
            maleText.setVisibility(View.GONE);
            otherIconImage.setVisibility(View.GONE);
            otherText.setVisibility(View.GONE);
        } else if (gender == 3) {
            otherIconImage.setVisibility(View.VISIBLE);
            otherText.setVisibility(View.VISIBLE);
            selectOther();
            maleIconImage.setVisibility(View.GONE);
            maleText.setVisibility(View.GONE);
            femaleIconImage.setVisibility(View.GONE);
            femaleText.setVisibility(View.GONE);
        }
        likeCount = myProfile.getLikedUserCount();

       // mAdapter.setCount(likeCount);

        try {
            if (!Validations.isEmptyString(myProfile.getPostImageUrl())) {
                // ArrayList aList= new ArrayList(Arrays.asList(myProfile.getPostImageUrl().split(",")));
                // List<String> items = Arrays.asList(myProfile.getPostImageUrl().split(","));
                todayList = Arrays.asList(myProfile.getPostImageUrl().split("\\s*,\\s*"));
               // mAdapter.addAll(todayList);
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        //mAdapter.notifyDataSetChanged();



    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof newProfileListener) {
            mListener = (newProfileListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement TagChildFragment");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }
    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
        inflater.inflate(R.menu.setting_menu, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        resultImage = true;
        if (resultCode == RESULT_OK) {
            if (requestCode == TakePicture) {
                bitmap = (Bitmap) data.getExtras().get("data");
                ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
                profileLogo.setImageBitmap(bitmap);

                imgBase64 = utility.image_to_Base64(getContext(), utility.getPath(utility.getImageUri(getContext(), bitmap), getContext()));
            }
            if (requestCode == SELECT_FILE) {
                try {
                    //bitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), data.getData());
                    sourceFile = new File(utility.getPath(data.getData(), getContext()));
                    bitmap = new Compressor(getContext()).compressToBitmap(sourceFile);
                    imgBase64 = utility.image_to_Base64(getContext(), utility.getPath(data.getData(), getContext()));
                    profileLogo.setImageBitmap(bitmap);

                } catch (IOException e) {
                    e.printStackTrace();
                    utility.showToast(getContext(), "Image not set please try again");
                }


            }
                /*bitmap = utility.decodeSampledBitmapFromResource(data.getExtras().get("data").toString(), 100, 100);
                img_1.setVisibility(View.VISIBLE);
                img_1.setImageBitmap(bitmap);*/
        }
    }

    @Optional
    @OnClick({R.id.my_profile_premium_image, R.id.my_profile_image_edit, R.id.cancel_button_profile,
            R.id.upgrade_button_profile,R.id.menu_login_detail_tv,R.id.detail_expand_imageView,
            R.id.profile_male_image, R.id.profile_female_image, R.id.profile_other_image, R.id.expend_image_view, R.id.setting_image_view,
            R.id.new_profile_logo, R.id.my_profile_image_logout, R.id.my_profile_image_done, R.id.my_profile_image_cancel})
    @Override
    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.new_profile_logo:
                if (editable) {
                    selectImage();
                } else {

                    if (!Validations.isEmptyString(profileImageString))
                        mListener.myProfileClick(view, profileImageString);
                }
                break;
            case R.id.my_profile_image_done:
                String profileNameChange = profileName.getText().toString().trim();
                String profileAddressChange = profileAddress.getText().toString().trim();
                String profileBioChange = profileBio.getText().toString().trim();

                if (Validations.isEmptyString(profileNameChange)) {
                    utility.showToast(getContext(), "Please Enter name first");
                    return;
                } else if (Validations.isEmptyString(profileBioChange)) {
                    utility.showToast(getContext(), "Please enter bio");
                    return;
                } else {
                    profileChange(profileNameChange, "dob", profileBioChange);

                    profileName.setEnabled(false);
                    profileAddress.setEnabled(false);
                    profileBio.setEnabled(false);
                    maleIconImage.setEnabled(false);
                    femaleIconImage.setEnabled(false);
                    otherIconImage.setEnabled(false);
                    cancelImage.setVisibility(View.GONE);
                    doneImage.setVisibility(View.GONE);
                    editLayout.setVisibility(View.GONE);
                    if (gender == 1) {
                        maleIconImage.setVisibility(View.VISIBLE);
                        maleText.setVisibility(View.VISIBLE);
                        selectMale();
                        femaleIconImage.setVisibility(View.GONE);
                        femaleText.setVisibility(View.GONE);
                        otherIconImage.setVisibility(View.GONE);
                        otherText.setVisibility(View.GONE);
                    } else if (gender == 2) {
                        femaleIconImage.setVisibility(View.VISIBLE);
                        femaleText.setVisibility(View.VISIBLE);
                        selectFemale();
                        maleIconImage.setVisibility(View.GONE);
                        maleText.setVisibility(View.GONE);
                        otherIconImage.setVisibility(View.GONE);
                        otherText.setVisibility(View.GONE);
                    } else if (gender == 3) {
                        otherIconImage.setVisibility(View.VISIBLE);
                        otherText.setVisibility(View.VISIBLE);
                        selectOther();
                        maleIconImage.setVisibility(View.GONE);
                        maleText.setVisibility(View.GONE);
                        femaleIconImage.setVisibility(View.GONE);
                        femaleText.setVisibility(View.GONE);
                    }

                }
                break;
            case R.id.my_profile_image_cancel:
                editable = false;
                profileName.setEnabled(false);
                profileAddress.setEnabled(false);
                profileBio.setEnabled(false);
                maleIconImage.setEnabled(false);
                femaleIconImage.setEnabled(false);
                otherIconImage.setEnabled(false);
                cancelImage.setVisibility(View.GONE);
                doneImage.setVisibility(View.GONE);
                editLayout.setVisibility(View.GONE);
                gender=apiGender;

                cancelClickBackground();
                if (gender == 1) {
                    maleIconImage.setVisibility(View.VISIBLE);
                    maleText.setVisibility(View.VISIBLE);
                    selectMale();
                    femaleIconImage.setVisibility(View.GONE);
                    femaleText.setVisibility(View.GONE);
                    otherIconImage.setVisibility(View.GONE);
                    otherText.setVisibility(View.GONE);
                } else if (gender == 2) {
                    femaleIconImage.setVisibility(View.VISIBLE);
                    femaleText.setVisibility(View.VISIBLE);
                    selectFemale();
                    maleIconImage.setVisibility(View.GONE);
                    maleText.setVisibility(View.GONE);
                    otherIconImage.setVisibility(View.GONE);
                    otherText.setVisibility(View.GONE);
                } else if (gender == 3) {
                    otherIconImage.setVisibility(View.VISIBLE);
                    otherText.setVisibility(View.VISIBLE);
                    selectOther();
                    maleIconImage.setVisibility(View.GONE);
                    maleText.setVisibility(View.GONE);
                    femaleIconImage.setVisibility(View.GONE);
                    femaleText.setVisibility(View.GONE);
                }
                if(myProfile!=null){
                    //Picasso.with(getContext()).load(myProfile.getProfileImage()).into(profileLogo);
                    Glide.with(getContext()).load(myProfile.getProfileImage()).placeholder(R.drawable.profile_pics).into(profileLogo);
                    profileName.setText(myProfile.getName());
                    profileBio.setText(myProfile.getBio());

                }else {
                    getProfile();
                }
                break;
            case R.id.profile_male_image:
                selectMale();
                break;
            case R.id.profile_female_image:
                selectFemale();
                break;
            case R.id.profile_other_image:
                selectOther();
                break;
            case R.id.setting_image_view:
               /* TitleNameTxt.setText("Settings");
                settingImageView.setVisibility(View.GONE);
                recyclerViewLayout.setVisibility(View.VISIBLE);
                showProfileLayout.setVisibility(View.GONE);
                backProfileImage.setVisibility(View.VISIBLE);*/
                mListener.newProfileClick(myProfile,"settingClick");
                break;



        }
    }

    private void cancelClickBackground() {
        if(sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
            doneImage.setBackgroundDrawable(ContextCompat.getDrawable(getContext(), R.drawable.button_background_primary) );
            cancelImage.setBackgroundDrawable(ContextCompat.getDrawable(getContext(), R.drawable.button_white_color_border) );
            doneImage.setTextColor(ContextCompat.getColor(getContext(),R.color.white));
            cancelImage.setTextColor(ContextCompat.getColor(getContext(),R.color.colorPrimary));
        } else {
            doneImage.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.button_background_primary));
            cancelImage.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.button_white_color_border));
            doneImage.setTextColor(ContextCompat.getColor(getContext(),R.color.white));
            cancelImage.setTextColor(ContextCompat.getColor(getContext(),R.color.colorPrimary));
        }
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        unbinder.unbind();
    }

    private void selectImage() {
        //here user will get options to choose image
        final CharSequence[] items = {"Take Photo", "Choose from Library", "Cancel"};
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle("Add Photo!");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (items[item].equals("Take Photo")) {
                    if (!marshMallowPermission.checkPermissionForCamera() && !marshMallowPermission.checkPermissionForExternalStorage()) {
                        marshMallowPermission.requestPermissionForCamera();
                        marshMallowPermission.requestPermissionForExternalStorage();
                    } else if (!marshMallowPermission.checkPermissionForCamera()) {
                        marshMallowPermission.requestPermissionForCamera();
                    } else if (!marshMallowPermission.checkPermissionForExternalStorage()) {
                        marshMallowPermission.requestPermissionForExternalStorage();
                    } else {
                        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                        if (takePictureIntent.resolveActivity(getActivity().getPackageManager()) != null) {
                            startActivityForResult(takePictureIntent, TakePicture);
                        }
                    }
//                startActivityForResult(intent, actionCode);
                } else if (items[item].equals("Choose from Library")) {
                    if (!marshMallowPermission.checkPermissionForExternalStorage()) {
                        marshMallowPermission.requestPermissionForExternalStorage();
                    } else {
                        Intent intent = new Intent(
                                Intent.ACTION_PICK,
                                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                        intent.setType("image/*");
                        startActivityForResult(Intent.createChooser(intent, "Select File"),
                                SELECT_FILE);
                    }
                } else if (items[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
        String item = adapterView.getItemAtPosition(position).toString();
        //Log.d(TAG, item);
        Log.v("item selected", item+"dfdsf"+(String) adapterView.getItemAtPosition(position));

        try {
            if (localeList.size()>0) {
                for ( int k = 0;k<localeList.size();k++){
                    if (localeList.get(k).getName().equals(item)) {
                        LocaleData newLocale = localeList.get(k);
                        if (sessionParam.localeId!=newLocale.getId())
                        updateUsersLocale(newLocale);
                        break;
                    }
                }
            }

        }catch (Exception e){
            e.printStackTrace();
        }

        //
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }


    public interface newProfileListener{
        public void myProfileClick(View view,String imageString);
        public void newProfileClick(MyProfileResponse myProfile,String clickMenu);
    }
}