package com.chetaru.FreshersApp.view.ui.Activity;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.PopupMenu;
import androidx.cardview.widget.CardView;
import androidx.core.app.ActivityOptionsCompat;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;


import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.Gravity;

import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;





import com.bumptech.glide.Glide;
import com.chetaru.FreshersApp.R;
import com.chetaru.FreshersApp.service.model.ChatListResponse;
import com.chetaru.FreshersApp.service.retrofit.BaseRequest;

import com.chetaru.FreshersApp.service.retrofit.RequestReciever;

import com.chetaru.FreshersApp.utility.MarshMallowPermission;
import com.chetaru.FreshersApp.utility.SessionParam;
import com.chetaru.FreshersApp.utility.Utility;

import com.chetaru.FreshersApp.view.adapter.MessageListAdapter;

import com.google.gson.Gson;
import com.google.gson.JsonObject;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;
import id.zelory.compressor.Compressor;

public class ChatActivity extends AppCompatActivity implements MessageListAdapter.chatClickListener{

    Utility utility;
    SessionParam sessionParam;
    MarshMallowPermission marshMallowPermission;
    private int TakePicture = 1, SELECT_FILE = 2, RESULT_OK = -1;
    private Bitmap bitmap = null;
    String imgBase64 = "";
    File sourceFile;
    String changeit_id = "";
    private BaseRequest baseRequest;
    LinearLayoutManager linearLayoutManager;
    String post_type = "";
    String message = "";
    List<ChatListResponse> chatList;

    private static final int PAGE_START = 1;
    private boolean isLoading = false;
    private int currentPage = PAGE_START;
    private  int TOTAL_PAGES = 1;
    private boolean isLastPage = false;
    Date currentDate;
    String dateTime,createDate="",otherUserName,otherProfileImage;
    int connectionId,otherUserId;
    int fileType=0;
    //initilize  view
    @BindView(R.id.chat_recyclerview)
    RecyclerView chatRecyclerView;
    MessageListAdapter chatAdapter;
    @BindView(R.id.et_msg)
    EditText editMessage;
    @BindView(R.id.iv_attachment)
    ImageView attachImageView;
    @BindView(R.id.iv_send)
    ImageView sendImage;
    @BindView(R.id.user_name_chat_txt)
    TextView userName;
    @BindView(R.id.iv_logo)
    CircleImageView profileImage;
    @BindView(R.id.back_chat_image)
    ImageView backImage;
    @BindView(R.id.chat_menu_image)
    ImageView menuImage;
    @BindView(R.id.other_user_name_layout)
     LinearLayout nameHeaderLayout;
    @BindView(R.id.cv_sendmsg)
    CardView chatCardSendMsg;
    Context mContext;
   /* @BindView(R.id.toolbar)
    Toolbar toolbar;*/




    Boolean refreshValue=true;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);
        ButterKnife.bind(this);
        mContext=this;
        Bundle bundle = getIntent().getExtras();

        //Extract the data…
        if (bundle!=null)
         connectionId = bundle.getInt("connectionId");
        otherUserId = bundle.getInt("otherUserId");
        //createDate = bundle.getString("date");
        otherUserName = bundle.getString("userName");
        otherProfileImage = bundle.getString("otherUserProfile");
        //connectionId=44;
        chatList=new ArrayList<>();
        utility=new Utility();
        sessionParam=new SessionParam(this);
        marshMallowPermission = new MarshMallowPermission(this);

        Collections.reverse(chatList);
        chatAdapter=new MessageListAdapter(otherUserId,chatList,mContext,this);
         linearLayoutManager=new LinearLayoutManager(mContext,LinearLayoutManager.VERTICAL,false);
       //linearLayoutManager.setReverseLayout(true);
        linearLayoutManager.setStackFromEnd(true);
        chatRecyclerView.setLayoutManager(linearLayoutManager);
        chatRecyclerView.setItemAnimator(new DefaultItemAnimator());
        chatRecyclerView.setAdapter(chatAdapter);
        userName.setText(otherUserName);
        chatRecyclerView.addOnLayoutChangeListener(new View.OnLayoutChangeListener() {
            @Override

            public void onLayoutChange(View v, int left, int top, int right,int bottom, int oldLeft, int oldTop,int oldRight, int oldBottom)
            {

                chatRecyclerView.scrollToPosition(chatList.size()-1);

            }
        });
        //Picasso.with(mContext).load(otherProfileImage).into(profileImage);
        Glide.with(mContext).load(otherProfileImage).placeholder(R.drawable.profile_pics).into(profileImage);

        apiGetChat();
        chatRecyclerView.addOnScrollListener(new PaginationScrollList(linearLayoutManager) {
            @Override
            protected void loadMoreItems() {
                isLoading=true;
                currentPage+=1;
                /*try {
                      chatAdapter.clear();
                }catch (Exception e){
                    e.printStackTrace();
                }*/
                api_loadNextPage();
            }

            @Override
            public boolean isLastPage() {

                return isLastPage;
            }

            @Override
            public boolean isLoading() {

                return isLoading;
            }
        });

        attachImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectImage();
            }
        });
        sendImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (editMessage.getText().toString().trim().isEmpty()) {
                    utility.showToast(mContext, "Please enter your message");
                } else {
                    if (!editMessage.getText().toString().trim().isEmpty()) {
                        post_type = "msg";
                    }
                    apiSendChatMessage();
                   /* if (!changeit_id.isEmpty()) {
                        apiSendChatMessage();
                    } else {
                        utility.showToast(mContext, "something went wrong!");
                    }*/
                }
            }
        });
        backImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        menuImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                showPopup(menuImage);
            }
        });
        nameHeaderLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                getOtherUserProfile();
               // mListener.chatUserProfile();
            }
        });

       /* toolbar.inflateMenu(R.menu.setting_menu);
        toolbar.getContentInsetEnd();
        toolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {

               switch (item.getItemId()) {
                    case R.id.nav_disconnect:
                       // disconnectMenu();
                        return true;
                    case R.id.nav_block:
                        //blockMenu();
                        return true;
                    default:
                        return false;
                }
            }
        });*/


    }





    public void showPopup(View v) {
        PopupMenu popup = new PopupMenu(this, v,Gravity.RIGHT|Gravity.CENTER);
        MenuInflater inflater = popup.getMenuInflater();
        inflater.inflate(R.menu.setting_menu, popup.getMenu());
        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.nav_disconnect:
                         disconnectMenu();
                        return true;
                    case R.id.nav_block:
                        blockMenu();
                        return true;
                    default:
                        return false;
                }
            }
        });
        popup.show();
    }


    private void disconnectMenu() {
        baseRequest = new BaseRequest(this);
        baseRequest.setBaseRequestListner(new RequestReciever() {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onSuccess(int requestCode, String Json, Object object) {
                try {

                    Gson gson = new Gson();
                    JSONObject jsonObject = new JSONObject(Json);
                    String message=jsonObject.getString("message");
                    //utility.showToast(ChatActivity.this,message);
                    chatCardSendMsg.setVisibility(View.GONE);
                    menuImage.setVisibility(View.GONE);
                    JSONObject response= (JSONObject) jsonObject.get("data");
                    try {
                        int  connectionStatus=response.getInt("connectionStatus");
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                    try {
                    int    remaingDays=response.getInt("remainingDays");
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                    try{
                     int    newConnectionId=response.getInt("connectionId");
                    }catch (Exception e){
                        e.printStackTrace();
                    }

                    // JSONArray jsonArray = new JSONArray(object.toString());
                    //localeList = baseRequest.getDataList(jsonArray, LocaleList.class);
                    /*connectionStatus :- 1 for pending, 2 for accepted, 3 for decline, 4 for unfriend, 5 for blocked*/

                    //handelStatus(connectionStatus,remaingDays,connectionId,blockedId);

                }catch (Exception e){
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int requestCode, String errorCode, String message) {
                //utility.showToast(getBaseContext(),message);
            }

            @Override
            public void onNetworkFailure(int requestCode, String message) {

                utility.showToast(ChatActivity.this,message);
            }
        });

        //-requestStatus:- 2 for accept, 3 for reject, 4 for unfriend and 6 for cancel pending request
        JsonObject main_object = new JsonObject();
        main_object.addProperty("connectionId",connectionId );
        main_object.addProperty("requestStatus", "4");



        baseRequest.callAPIPostWOLoader(1, main_object, getString(R.string.api_AcceptOrDeclineUserReq));


    }
    private void blockMenu() {
        baseRequest = new BaseRequest(this);
        baseRequest.setBaseRequestListner(new RequestReciever() {
            @Override
            public void onSuccess(int requestCode, String Json, Object object) {
                try {

                    JSONObject jsonObject = new JSONObject(Json);
                    String message=jsonObject.getString("message");

                   // utility.showToast(ChatActivity.this,message);

                    chatCardSendMsg.setVisibility(View.GONE);
                    menuImage.setVisibility(View.GONE);
                }catch (Exception e){
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int requestCode, String errorCode, String message) {
                // utility.showToast(getContext(),message);
            }

            @Override
            public void onNetworkFailure(int requestCode, String message) {

                utility.showToast(ChatActivity.this,message);
            }
        });

       /* {
            "blockUserId":"6",
                "type":1
        }
        "type" - 1 for block , 2 for unblock*/
       /*if (blockUser){
           blockUser=false;
           type=1;
       }else {
           blockUser=true;
           type=2;
       }*/

        JsonObject main_object = new JsonObject();
        main_object.addProperty("blockUserId",otherUserId);
        main_object.addProperty("type", 1);
       /* JsonObject object = Functions.getClient().getJsonMapObject(
                "postId",
                "likeStatus",status );*/
        baseRequest.callAPIPostWOLoader(1, main_object, getString(R.string.api_blockUser));
    }







    private void selectImage() {
        //here user will get options to choose image
        final CharSequence[] items = {"Camera", "Gallery", "Cancel"};
       AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        builder.setTitle("Add Photo!");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (items[item].equals("Camera")) {
                    if (!marshMallowPermission.checkPermissionForCamera() && !marshMallowPermission.checkPermissionForExternalStorage()) {
                        marshMallowPermission.requestPermissionForCamera();
                        marshMallowPermission.requestPermissionForExternalStorage();
                    } else if (!marshMallowPermission.checkPermissionForCamera()) {
                        marshMallowPermission.requestPermissionForCamera();
                    } else if (!marshMallowPermission.checkPermissionForExternalStorage()) {
                        marshMallowPermission.requestPermissionForExternalStorage();
                    } else {
                        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
                            startActivityForResult(takePictureIntent, TakePicture);
                        }
                    }
//                startActivityForResult(intent, actionCode);
                } else if (items[item].equals("Gallery")) {
                    if (!marshMallowPermission.checkPermissionForExternalStorage()) {
                        marshMallowPermission.requestPermissionForExternalStorage();
                    } else {
                        Intent intent = new Intent(
                                Intent.ACTION_PICK,
                                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                        intent.setType("image/*");
                        startActivityForResult(Intent.createChooser(intent, "Select File"),
                                SELECT_FILE);
                    }
                } else if (items[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        String imageFileName = System.currentTimeMillis() + ".png";
        if (resultCode == RESULT_OK) {
            if (requestCode == TakePicture) {
                post_type = "img";
                bitmap = (Bitmap) data.getExtras().get("data");
                ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.PNG, 100, bytes);
                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                byte[] byteArray = byteArrayOutputStream.toByteArray();
                Bitmap decoded = BitmapFactory.decodeStream(new ByteArrayInputStream(bytes.toByteArray()));

                //imgBase64 = Base64.encodeToString(byteArray, Base64.DEFAULT);
                imgBase64 = utility.image_to_Base64(mContext, utility.getPath(utility.getImageUri(mContext, decoded), mContext));
                apiSendChatMessage();
               // apiSendMsg();
            }
            if (requestCode == SELECT_FILE) {
                ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                byte[] byteArray = byteArrayOutputStream.toByteArray();
                post_type = "img";
                //bitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), data.getData());
                sourceFile = new File(utility.getPath(data.getData(), mContext));
                try {
                    bitmap = new Compressor(mContext).compressToBitmap(sourceFile);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                bitmap.compress(Bitmap.CompressFormat.PNG, 100, bytes);
                Bitmap decoded = BitmapFactory.decodeStream(new ByteArrayInputStream(bytes.toByteArray()));
                imgBase64 = utility.image_to_Base64(mContext, utility.getPath(utility.getImageUri(mContext, decoded), mContext));
                //imgBase64 = Base64.encodeToString(byteArray, Base64.DEFAULT);
                apiSendChatMessage();
                // iv_DP.setImageBitmap(bitmap);

            }
                /*bitmap = utility.decodeSampledBitmapFromResource(data.getExtras().get("data").toString(), 100, 100);
                img_1.setVisibility(View.VISIBLE);
                img_1.setImageBitmap(bitmap);*/
        }
    }


    /*API call to get chat
     */
    public void apiGetChat() {
        currentPage=PAGE_START;
        chatAdapter.clear();
        if (chatList.size()>0)
        chatList.clear();
        baseRequest = new BaseRequest(mContext);
        baseRequest.setBaseRequestListner(new RequestReciever() {
            @Override
            public void onSuccess(int requestCode, String Json, Object object) {
                try {
                    /*JSONObject jsonObject = new JSONObject(object.toString());
                    JSONArray jsonArray;
                    jsonArray = (JSONArray) jsonObject.getJSONArray("messages");*/
                    JSONObject jsonObject=new JSONObject(Json);
                    JSONArray userJsonArray = jsonObject.getJSONArray("data");
                    chatList = baseRequest.getDataList(userJsonArray, ChatListResponse.class);
                    TOTAL_PAGES=jsonObject.getInt("totalPageCount");
                    Collections.reverse(chatList);
                   /* chatAdapter=new MessageListAdapter(otherUserId, chatList, mContext, new MessageListAdapter.chatClickListener() {
                        @Override
                        public void onUserImageView(View view, String imageString) {
                           getImageFullView(view,imageString);
                        }
                    });*/
                    chatAdapter.addAll(chatList);
                    chatRecyclerView.setAdapter(chatAdapter);

                    try {
                        if (chatList.size() > 0) {
                            dateTime = chatList.get(chatList.size()-1).getCreatedAt();
                        }
                    }catch (Exception e){
                        e.printStackTrace();
                    }

                      if (currentPage < TOTAL_PAGES)
                        chatAdapter.addLoadingFooter();
                    else
                        isLastPage = true;

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int requestCode, String errorCode, String message) {
               utility.showToast(mContext, message);
            }

            @Override
            public void onNetworkFailure(int requestCode, String message) {
                //errorLayout.showError(message);
                utility.showToast(mContext, message);
            }
        });

        JsonObject main_object = new JsonObject();
        main_object.addProperty("connectionId",connectionId+"" );
        main_object.addProperty("page", currentPage);
        main_object.addProperty("date", createDate);
       // main_object.addProperty("connectionStatus", connectionId);
        baseRequest.callAPIPost(1, main_object, getString(R.string.api_getUsersChat));

    }

    private void getImageFullView(View view, String imageString) {
        Intent intent=new Intent(this,UploadImageActivity.class);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            ActivityOptionsCompat options = ActivityOptionsCompat.
                    makeSceneTransitionAnimation(this,view, "MyTransition");
            intent.putExtra("imageString",imageString);
            startActivity(intent, options.toBundle());
        } else {
            startActivity(intent);
        }
    }
    public void getOtherUserProfile(){

        Intent intent=new Intent(this,HomeActivity.class);
        intent.putExtra("connectionId",connectionId);
        intent.putExtra("otherUserId",otherUserId);
        intent.putExtra("userName",otherUserName);
        intent.putExtra("otherUserProfile",otherProfileImage);
        intent.putExtra("connection",2);
        startActivity(intent);
        finish();

    }

    public void api_loadNextPage() {
        try {
           // chatAdapter.clear();
        }catch (Exception e){
            e.printStackTrace();
        }

        baseRequest = new BaseRequest(mContext);
        baseRequest.setBaseRequestListner(new RequestReciever() {
            @Override
            public void onSuccess(int requestCode, String Json, Object object) {
                try {
                    /*JSONObject jsonObject = new JSONObject(object.toString());
                    JSONArray jsonArray;
                    jsonArray = (JSONArray) jsonObject.getJSONArray("messages");*/


                    JSONObject jsonObject=new JSONObject(Json);
                    JSONArray userJsonArray = jsonObject.getJSONArray("data");
                    List<ChatListResponse> newChatList = baseRequest.getDataList(userJsonArray, ChatListResponse.class);
                   // Collections.reverse(newChatList);
                    TOTAL_PAGES=jsonObject.getInt("totalPageCount");
                    //chatAdapter=new ChatAdapter(otherUserId,chatList,mContext);
                    //chatRecyclerView.setAdapter(chatAdapter);
                    if (newChatList.size()>0){
                        dateTime=newChatList.get(newChatList.size()-1).getCreatedAt();
                    }

                    chatAdapter.removeLoadingFooter();
                    isLoading = false;
                    //load a image
                    /*for (int i=0;i<newChatList.size();i++){
                        chatList.add(i,newChatList.get(i));
                    }

                    chatAdapter.addAll(chatList);
                    chatRecyclerView.scrollToPosition(chatAdapter.getItemCount() - 1);*/

                    if (newChatList.size()>0) {
                        chatAdapter.addFirstAll(newChatList);
                        chatRecyclerView.setAdapter(chatAdapter);
                        chatRecyclerView.getLayoutManager().scrollToPosition(chatList.size() - 1);
                        chatRecyclerView.scrollToPosition(chatAdapter.getItemCount() - 1);
                    }

                    if (currentPage != TOTAL_PAGES) {
                        chatAdapter.addLoadingFooter();
                    }else {
                        isLastPage = true;
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int requestCode, String errorCode, String message) {
                //utility.showToast(mContext, message);
            }

            @Override
            public void onNetworkFailure(int requestCode, String message) {
                //errorLayout.showError(message);
                utility.showToast(mContext, message);
            }
        });

        JsonObject main_object = new JsonObject();
        main_object.addProperty("connectionId",connectionId );
        main_object.addProperty("page", currentPage);
        main_object.addProperty("date", dateTime);
        baseRequest.callAPIPostWOLoader(1, main_object, getString(R.string.api_getUsersChat));

    }

    public BroadcastReceiver myReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getStringExtra("action");
            //changeUi(action);
            if (refreshValue) {
                api_getChatRefresh();
                refreshValue = false;
            }
        }
    };
    @Override
    protected void onResume() {
        super.onResume();
        registerReceiver(myReceiver, new IntentFilter("sendMessage"));
        //hideSoftKeyboard();
    }

    @Override
    protected void onPause() {
        super.onPause();
        unregisterReceiver(myReceiver);
        //hideSoftKeyboard();
    }

    private void api_getChatRefresh() {
        baseRequest = new BaseRequest(mContext);
        baseRequest.setBaseRequestListner(new RequestReciever() {
            @Override
            public void onSuccess(int requestCode, String Json, Object object) {
                try {
                    refreshValue=true;
                    JSONObject jsonObject=new JSONObject(Json);
                    JSONArray userJsonArray = jsonObject.getJSONArray("data");
                    //chatList = baseRequest.getDataList(userJsonArray, ChatListResponse.class);
                    //Collections.reverse(chatList);
                    //chatAdapter=new ChatAdapter(otherUserId,chatList,mContext);
                    //chatRecyclerView.setAdapter(chatAdapter);

                    List<ChatListResponse>   newUploadList = baseRequest.getDataList(userJsonArray, ChatListResponse.class);
                    Collections.reverse(newUploadList);
                    //Collections.reverse(chatList);

                    if (newUploadList.size()>0) {
                        for (int i = 0; i < newUploadList.size(); i++) {
                            for (int j = 0; j < chatList.size(); j++) {
                                if (newUploadList.get(i).getMessageId().equals(chatList.get(j).getMessageId())) {
                                    newUploadList.remove(i);
                                }
                                /*if (newUploadList.get(i).getUserId().equals(connList.get(j).getUserId())) {
                                    connList.remove(j);
                                }*/
                            }
                        }
                    }
                    /*if (newUploadList.size()>0) {
                        for (int i = 0; i < newUploadList.size(); i++) {
                            chatList.add(chatList.size()-1, newUploadList.get(i));

                        }
                    }*/
                    if (newUploadList.size()>0) {
                        //Collections.reverse(newUploadList);
                       // chatAdapter.add(newUploadList.get(0));
                        chatAdapter.refreshData(newUploadList.get(0));
                       // chatRecyclerView.getRecyclerView().smoothScrollToPosition(0);

                    }

                    /*if(chatList.size()>0){
                        Collections.reverse(chatList);
                        chatAdapter.clear();
                        chatAdapter.addAll(chatList);
                    }*/
                    //chatAdapter.notifyDataSetChanged();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int requestCode, String errorCode, String message) {
               // utility.showToast(mContext, message);
                refreshValue=true;
            }

            @Override
            public void onNetworkFailure(int requestCode, String message) {
                //errorLayout.showError(message);
                utility.showToast(mContext, message);
                refreshValue=true;
            }
        });

        JsonObject main_object = new JsonObject();
        main_object.addProperty("connectionId",connectionId );
        main_object.addProperty("pageId", 0);
        main_object.addProperty("date", dateTime);
        baseRequest.callAPIPost(1, main_object, getString(R.string.api_getUsersChat));

    }

    /*API call to send chat message
     */
    public void apiSendChatMessage() {
        utility = new Utility();
        baseRequest = new BaseRequest(mContext);
        baseRequest.setBaseRequestListner(new RequestReciever() {
            @Override
            public void onSuccess(int requestCode, String Json, Object object) {
                try {

                    JSONObject jsonObject=new JSONObject(Json);
                    JSONArray userJsonArray = jsonObject.getJSONArray("data");
                   List<ChatListResponse> chatData=baseRequest.getDataList(userJsonArray,ChatListResponse.class);
                    editMessage.setText("");
                    if (chatData.size()>0)
                    chatAdapter.addAll(chatData);
                  //  chatAdapter.notifyDataSetChanged ();
                  //  chatAdapter.notifyItemInserted(chatList.size()-1);
                    chatRecyclerView.getLayoutManager().scrollToPosition(chatList.size()-1);
                    chatRecyclerView.scrollToPosition(chatAdapter.getItemCount() - 1);


                    // linearLayoutManager.setStackFromEnd(true);
                    //chatRecyclerView.setLayoutManager(linearLayoutManager);


                    // apiGetChat();
                   /* chatList = baseRequest.getDataList(jsonArray, ChatListResponse.class);
                    chatAdapter=new ChatAdapter(chatList,mContext);
                    chatRecyclerView.setAdapter(chatAdapter);
                    if (currentPage <= TOTAL_PAGES)
                        chatAdapter.addLoadingFooter();
                    else
                        isLastPage = true;
                    try {
                        if (chatList.size() > 0) {
                            dateTime = chatList.get(0).getCreatedAt();
                        }
                    }catch (Exception e){
                        e.printStackTrace();
                    }*/

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int requestCode, String errorCode, String message) {
                //utility.showToast(mContext, message);
            }

            @Override
            public void onNetworkFailure(int requestCode, String message) {
                //errorLayout.showError(message);
                utility.showToast(mContext, message);
            }
        });

        if (post_type.equals("img")) {
            message = imgBase64;
            fileType=2;
        } else {
            message = editMessage.getText().toString();
            fileType=1;
        }
        JsonObject main_object = new JsonObject();
        main_object.addProperty("sendFrom",sessionParam.id );
        main_object.addProperty("sendTo", otherUserId);
        main_object.addProperty("connectionId", connectionId);
        main_object.addProperty("fileType", fileType);
        main_object.addProperty("message", message);
        baseRequest.callAPIPost(1, main_object, getString(R.string.api_sendMessage));

    }

    @Override
    public void onUserImageView(View view, String imageString) {
       // getImageFullView(view,imageString);
        DialogFullView(view,imageString);
    }

    private void DialogFullView(View view, String imageString) {
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        //dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        //dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setContentView(R.layout.dialog_full_view);
        dialog.setCanceledOnTouchOutside(true);
        final ImageView imageView=dialog.findViewById(R.id.my_profile_logo);

        Glide.with(this).load(imageString).placeholder(R.drawable.profile_pics).into(imageView);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = dialog.getWindow();
        lp.copyFrom(window.getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.MATCH_PARENT;
        window.setBackgroundDrawableResource(R.color.colorPrimary);
        window.setAttributes(lp);
        dialog.show();
    }

    public abstract  class PaginationScrollList extends RecyclerView.OnScrollListener {

        private LinearLayoutManager layoutManager;

        public PaginationScrollList(LinearLayoutManager layoutManager) {
            this.layoutManager = layoutManager;
        }

        @Override
        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
            super.onScrolled(recyclerView, dx, dy);

            int visibleItemCount = layoutManager.getChildCount();
            int totalItemCount = layoutManager.getItemCount();
            int firstVisibleItemPosition = layoutManager.findFirstVisibleItemPosition();

            if (!isLoading() && !isLastPage()) {
                if ((visibleItemCount + firstVisibleItemPosition) >= totalItemCount
                        && firstVisibleItemPosition >= 0) {
                    loadMoreItems();
                }
            }
        }

        protected abstract void loadMoreItems();

        public abstract boolean isLastPage();

        public abstract boolean isLoading();

    }

}
