package com.chetaru.FreshersApp.view.ui.Fragment;


import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.viewpager.widget.ViewPager;

import com.chetaru.FreshersApp.R;
import com.chetaru.FreshersApp.service.model.MyProfileResponse;
import com.chetaru.FreshersApp.service.model.MyUploads;
import com.chetaru.FreshersApp.service.retrofit.ApiClient;
import com.chetaru.FreshersApp.service.retrofit.BaseRequest;
import com.chetaru.FreshersApp.service.retrofit.Functions;
import com.chetaru.FreshersApp.service.retrofit.RequestReciever;
import com.chetaru.FreshersApp.utility.BaseActivity;
import com.chetaru.FreshersApp.utility.SessionParam;
import com.chetaru.FreshersApp.utility.Utility;
import com.chetaru.FreshersApp.utility.Validations;
import com.chetaru.FreshersApp.viewModel.ProfileViewModel;
import com.chetaru.FreshersApp.view.ui.LoginActivity;
import com.google.android.material.tabs.TabLayout;
import com.google.gson.Gson;
import com.google.gson.JsonObject;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProfileFragment extends Fragment {

    private ProfileViewModel dashboardViewModel;
    Button logoutButton;
    Utility utility;
    SessionParam sessionParam;
    String tokenId="";
    BaseRequest baseRequest;
    private TabLayout tabLayout;
    private ViewPager viewPager;
     TextView userName;
     int userId;
     int personId=0,position=0;
    MyProfileResponse myProfileResponse;
    List<MyUploads> uploadList;
    String dateTime,connection="";

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        dashboardViewModel =
                ViewModelProviders.of(this).get(ProfileViewModel.class);
        Bundle args = getArguments();
        if (args != null) {
            personId=getArguments().getInt("personId");
             position=getArguments().getInt("position");
             connection=getArguments().getString("connection");
        }
        View root = inflater.inflate(R.layout.fragment_profile, container, false);
        tabLayout=root.findViewById(R.id.tab_layout);
        viewPager=root.findViewById(R.id.pager);
        tabLayout.setupWithViewPager(viewPager);
        sessionParam=new SessionParam(getContext());
        utility=new Utility();
        userId=sessionParam.id;
        uploadList=new ArrayList<>();

       // dateTime = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).format(new Date());
        myProfileResponse=new MyProfileResponse();
        //setupTabTitle();
        setupViewPager(viewPager);
        userName = root.findViewById(R.id.profile_user_name);
        //call Api getProfile
        geProfile();

        dashboardViewModel.getText().observe(getViewLifecycleOwner(), new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {

                //userName.setText(s);
            }
        });
        utility=new Utility();
        sessionParam=new SessionParam(getContext());
        tokenId=sessionParam.token;
        utility.getProgerss(getContext());

        return root;
    }

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getChildFragmentManager());
        adapter.addFrag(MyProfileFragment.newInstance(myProfileResponse), "My Profile");
        adapter.addFrag(new MyUploadFragment(), "My Uploads");
        adapter.addFrag(new MyLikesFragment(), "My likes");

        viewPager.setAdapter(adapter);
    }
    private void setupTabTitle() {
        //tabLayout.setSelectedTabIndicatorColor(Color.parseColor("#FF00FF"));
        tabLayout.setSelectedTabIndicatorColor(ContextCompat.getColor(getContext(), R.color.colorPrimary));
        tabLayout.setSelectedTabIndicatorHeight((int) (2 * getResources().getDisplayMetrics().density));
        //tabLayout.setTabTextColors(getResources().getColor(R.color.colorPrimary));
       /* tabLayout.getTabAt(0).set(tabIcons[0]);
        tabLayout.getTabAt(1).setIcon(tabIcons[1]);
        tabLayout.getTabAt(2).setIcon(tabIcons[2]);*/
    }

    static class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }


        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFrag(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }


    public void geProfile(){
        baseRequest = new BaseRequest(getContext());
        baseRequest.setBaseRequestListner(new RequestReciever() {
            @Override
            public void onSuccess(int requestCode, String Json, Object object) {
                try {

                    Gson gson = new Gson();
                    JSONObject jsonObject = new JSONObject(Json);
                    JSONObject response= (JSONObject) jsonObject.get("data");
                     myProfileResponse = gson.fromJson(response.toString(), MyProfileResponse.class);
                     //utility.showToast(getContext(),myProfileResponse.toString());
                }catch (Exception e){
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int requestCode, String errorCode, String message) {
                //utility.showToast(getBaseContext(),message);
            }

            @Override
            public void onNetworkFailure(int requestCode, String message) {

                utility.showToast(getContext(),message);
            }
        });




        JsonObject main_object = new JsonObject();
        main_object.addProperty("userId",sessionParam.id );
        baseRequest.callAPIPostWOLoader(1, main_object, getString(R.string.api_getUserProfile));

    }
    private void getMyUpload() {
        baseRequest = new BaseRequest(getContext());
        baseRequest.setBaseRequestListner(new RequestReciever() {
            @Override
            public void onSuccess(int requestCode, String Json, Object object) {
                try {
                    Gson gson = new Gson();
                    JSONObject jsonObject = new JSONObject(Json);
                    //ArrayList myUploadList= (ArrayList) jsonObject.get("data");
                    JSONArray userJsonArray = jsonObject.getJSONArray("data");

                    uploadList = baseRequest.getDataList(userJsonArray, MyUploads.class);
                  //  uploadAdapter.addAll(uploadList);
                     /*JSONObject jsonObject = new JSONObject(object.toString());
                        JSONArray userJsonArray = jsonObject.getJSONArray("data");*/
                    // ArrayList<ModelTribeometerGraph> list = new ArrayList<>();

                }catch (Exception e){
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int requestCode, String errorCode, String message) {
                //utility.showToast(getContext(),message);
            }

            @Override
            public void onNetworkFailure(int requestCode, String message) {

                utility.showToast(getContext(),message);
            }
        });

                /*
                * //In case of 0
        {
          "userId":2,
          "page":0,
          "date":"2020-05-15 10:02:00"
        }

        //In case of page1
        {
          "userId":2,
          "page":1
        }*/
        JsonObject main_object = new JsonObject();

        main_object.addProperty("userId", userId);
        main_object.addProperty("page", 0);
        main_object.addProperty("date", dateTime);
       /* JsonObject object = Functions.getClient().getJsonMapObject(
                "userId",userId,
                "page",currentPage+"",
                "date",dateTime
        );*/
        baseRequest.callAPIPostWOLoader(1, main_object, getString(R.string.api_getUserUploads));

    }


}
