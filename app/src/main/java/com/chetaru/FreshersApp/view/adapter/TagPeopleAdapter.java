package com.chetaru.FreshersApp.view.adapter;

import android.content.Context;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.chetaru.FreshersApp.R;
import com.chetaru.FreshersApp.service.model.TagList;
import com.chetaru.FreshersApp.service.model.TagPeopleResponse;
import com.chetaru.FreshersApp.service.model.UserList;
import com.chetaru.FreshersApp.utility.SessionParam;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

public class TagPeopleAdapter extends RecyclerView.Adapter<TagPeopleAdapter.ViewHolder> implements Filterable {
    List<UserList> userList = new ArrayList<>();
    private Context context;
    private List<UserList>  peopleListFiltered;
    int selectedPosition=-1;
    private TapPeopleListener listener;
    SessionParam sessionParam;
    int userId;


    public  class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.country_name_tv)
        TextView countryNameTxt;
        @BindView(R.id.check_selection_image)
        ImageView checkSelectImage;
        @BindView(R.id.upload_image_adapter)
        CircleImageView profileImage;
        private View view;

        public ViewHolder(View v) {
            super(v);
            view=v;
            ButterKnife.bind(this,v);
            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    // send selected contact in callback
                    //listener.onPeopleSelected(contactListFiltered.get(getAdapterPosition()));
                    listener.onPeopleSelected(userList.get(getAdapterPosition()));
                }
            });
        }
    }

    public TagPeopleAdapter(List<UserList> userList, Context context, TapPeopleListener listener) {
        this.userList = userList;
        this.context = context;
        this.listener=listener;
        this.peopleListFiltered = userList;
        sessionParam=new SessionParam(context);
        userId=sessionParam.id;
    }
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_tag_pepole_list, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        try {
            if (userId!=(userList.get(position).getId())) {
                holder.countryNameTxt.setText(userList.get(position).getName());
                Glide.with(context)
                        .load(userList.get(position).getImageUrl())
                        .apply(RequestOptions.circleCropTransform())
                        .placeholder(R.drawable.profile_logo)
                        .into(holder.profileImage);


                //holder.bind should not trigger onCheckedChanged, it should just update UI

                final UserList model = userList.get(position);
                holder.countryNameTxt.setText(model.getName());
                //holder.view.setBackgroundColor(model.isSelected() ? Color.CYAN : Color.WHITE);
                holder.checkSelectImage.setVisibility(model.isSelected() ? View.VISIBLE : View.INVISIBLE);
                holder.itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        model.setSelected(!model.isSelected());
                        // holder.view.setBackgroundColor(model.isSelected() ? Color.CYAN : Color.WHITE);
                        holder.checkSelectImage.setVisibility(model.isSelected() ? View.VISIBLE : View.INVISIBLE);
                    }
                });
            }


        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {

        try {
            return userList.size();
        }catch (Exception e){
            e.printStackTrace();
            return 0;
        }
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    userList = peopleListFiltered;
                } else {
                    List<UserList> filteredList = new ArrayList<>();
                    for (UserList row : peopleListFiltered) {

                        // name match condition. this might differ depending on your requirement
                        // here we are looking for name or phone number match
                        if (row.getName().toLowerCase().contains(charString.toLowerCase()) ) {
                            filteredList.add(row);
                        }
                    }

                    userList = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = userList;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                userList = (ArrayList<UserList>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }


    public interface TapPeopleListener {
        void onPeopleSelected(UserList tagPeople);
    }

}
