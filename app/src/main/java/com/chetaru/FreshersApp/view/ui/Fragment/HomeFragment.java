package com.chetaru.FreshersApp.view.ui.Fragment;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.media.AudioManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;

import android.os.VibrationEffect;
import android.os.Vibrator;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.renderscript.Allocation;
import android.renderscript.Element;
import android.renderscript.RenderScript;
import android.renderscript.ScriptIntrinsicBlur;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;


import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;


import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.PagerSnapHelper;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.SnapHelper;

import com.chetaru.FreshersApp.R;


import com.chetaru.FreshersApp.service.model.TagList;
import com.chetaru.FreshersApp.service.model.UserList;
import com.chetaru.FreshersApp.service.model.UserTagList;
import com.chetaru.FreshersApp.service.model.UserViewPost;
import com.chetaru.FreshersApp.service.retrofit.BaseRequest;
import com.chetaru.FreshersApp.service.retrofit.RequestReciever;
import com.chetaru.FreshersApp.utility.SessionParam;
import com.chetaru.FreshersApp.utility.Utility;
import com.chetaru.FreshersApp.utility.Validations;
import com.chetaru.FreshersApp.view.adapter.AdminHomeAdapter;
import com.chetaru.FreshersApp.view.adapter.HomePaginationAdapter;
import com.chetaru.FreshersApp.view.adapter.HomeSingleAdapter;
import com.chetaru.FreshersApp.view.adapter.MyUploadsHomeAdapter;
import com.chetaru.FreshersApp.view.callback.HomepageCallback;
import com.chetaru.FreshersApp.view.callback.PaginationListenerCallback;
import com.chetaru.FreshersApp.view.callback.PaginationScrollListener;
import com.chetaru.FreshersApp.view.customView.HorizontalSwipeRefreshLayout;
import com.chetaru.FreshersApp.viewModel.HomeViewModel;
import com.google.gson.Gson;
import com.google.gson.JsonObject;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import im.delight.android.location.SimpleLocation;


public class HomeFragment extends Fragment
        implements LocationListener,HorizontalSwipeRefreshLayout.OnRefreshListener
                   ,PaginationListenerCallback ,View.OnClickListener {

    private HomeViewModel homeViewModel;
    private static final String TAG = "LocationFragment";
    Context mContext;
    Button logoutButton;
    Utility utility;
    SessionParam sessionParam;
    String tokenId = "";
    private BaseRequest baseRequest;
    private double longitude=0.0;
    private double latitude=0.0;
    Boolean firstCall=true;
    List<UserViewPost> userViewPostList;
    UserViewPost userViewPost;
    HorizontalSwipeRefreshLayout mSwipeRefreshLayout;
    RecyclerView mRecyclerView;
    //LinearLayoutManager mLayoutManager;
    HomePaginationAdapter mAdapter;

    HomeSingleAdapter singleAdapter;
    private LinearLayout showMsgLayout,showDataLayout,longClickLayout,tagImageLayout;
    private LocationManager mLocationManager;
    private SimpleLocation location;
    private static int TOTAL_PAGES = 1;
    public static final int PAGE_START = 1;
    private static final float BLUR_RADIUS = 25f;

    private int currentPage = PAGE_START;
    private int homePage = 0;
    private boolean isLastPage = false;
    private int totalPage = 1;
    private boolean isLoading = false;
    int itemCount = 0;
    boolean mileExpand=true;
    UserViewPost selectedViewPost;
    Boolean blockUser=true;
    Integer type=1;
    HomepageCallback mListener;
    Fragment childFragment=null;
    LinearLayout blockLayout,reportLayout;
    TextView postMsgText;
    Button locationButton,postNowButton;
    int userId;
    String userName;
    boolean premiumUser=false;
    //home post reCreate
    List<UserViewPost> myUploadList= new ArrayList<>();
    int retainPostPosition=0;

    //save home Feed position
    // current page, selectedpostindex, loadmore, totapage count
    int homeFeedId=0,homeFeedPosition=0,homeTotalPage=0,homeCurrentPage=0;



    ImageView blockUserImage,termsImage,privacyPolicyImage,contactUsImage,cancelImage;


    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private static final String ARG_PARAM3 = "param3";
    private static final String ARG_PARAM4 = "param4";

    // TODO: Rename and change types of parameters
    private Integer postId=0;
    private String parentClassName=null;

    List<UserViewPost> oldViewList;




    public HomeFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @return A new instance of fragment UploadFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static HomeFragment newInstance(Integer param1, List<UserViewPost> postList,int postPosition,String parentClass) {
        HomeFragment fragment = new HomeFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_PARAM1, param1);
        args.putSerializable(ARG_PARAM2, (Serializable) postList);
        args.getString(ARG_PARAM3,parentClass);
        args.getInt(ARG_PARAM4,postPosition);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
        if (getArguments() != null) {
            postId = getArguments().getInt(ARG_PARAM1);
            myUploadList = (List<UserViewPost>) getArguments().getSerializable(ARG_PARAM2);
            parentClassName=getArguments().getString(ARG_PARAM3);
            retainPostPosition=getArguments().getInt(ARG_PARAM4);

        }
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (savedInstanceState!=null){
            myUploadList= (List<UserViewPost>) savedInstanceState.getSerializable("viewPostList");
        }
    }

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        if (savedInstanceState!=null) {
            onRestoreInstanceState(savedInstanceState);
        }
        homeViewModel =
                ViewModelProviders.of(this).get(HomeViewModel.class);
        View root = inflater.inflate(R.layout.fragment_home, container, false);
        mContext=getContext();
        sessionParam=new SessionParam(getContext());
        tokenId=sessionParam.token;
         userId=(sessionParam.id);
         userName=sessionParam.name;

         premiumUser=sessionParam.premieUser;
        this.mListener = (HomepageCallback) getContext();
        utility=new Utility();
        utility.getProgerss(getContext());
        userViewPostList=new ArrayList<>();


        homeViewModel.getText().observe(getViewLifecycleOwner(), new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
                // textView.setText(s);
            }
        });
        try{
            if (childFragment!=null) {
               /* FragmentTransaction transaction = getActivity().getSupportFragmentManager()
                        .beginTransaction();

                transaction.remove(childFragment);

                transaction.commit();*/
                FragmentManager manager = getActivity().getSupportFragmentManager();
                FragmentTransaction trans = manager.beginTransaction();
                trans.remove(childFragment);
                trans.commit();
                manager.popBackStack();
            }
        }catch(Exception e){
            e.printStackTrace();
        }

        // construct a new instance of SimpleLocation
        location = new SimpleLocation(getContext());

        // if we can't access the location yet
        if (!location.hasLocationEnabled()) {
            // ask the user to enable location access
            //SimpleLocation.openSettings(getContext());
            if (!sessionParam.premieUser)
            ShowSettingAlert();
        }


        showMsgLayout=root.findViewById(R.id.show_msg_layout);
        showDataLayout=root.findViewById(R.id.home_image_layout);
        longClickLayout=root.findViewById(R.id.home_long_click_layout);
        tagImageLayout=root.findViewById(R.id.tag_image_layout);
        blockLayout=root.findViewById(R.id.block_user_layout);
        reportLayout=root.findViewById(R.id.report_layout);
        locationButton=root.findViewById(R.id.home_location_button);
        postNowButton=root.findViewById(R.id.post_now_button);
        postMsgText=root.findViewById(R.id.post_msg_text);

       // mSwipeRefreshLayout = root.findViewById(R.id.swipeRefresh);
        mRecyclerView = root.findViewById(R.id.mRecyclerView);
        location.setListener(new SimpleLocation.Listener() {

            public void onPositionChanged() {
                // new location data has been received and can be accessed
                latitude = location.getLatitude();
                longitude = location.getLongitude();
            }

        });
         latitude = location.getLatitude();
        longitude = location.getLongitude();

        if (latitude==0.0){

         //ShowSettingAlert();
        }
        showMsgLayout.setVisibility(View.GONE);
        showDataLayout.setVisibility(View.GONE);
        longClickLayout.setVisibility(View.GONE);
        tagImageLayout.setVisibility(View.GONE);
      //  if (premiumUser){
            postNowButton.setText("Post Now");
            postMsgText.setText("Change your location or post to the feed.");
        /*}else{
            postNowButton.setText("Restart");
            postMsgText.setText("Change your location or restart today's feed.");
        }*/
        //userViewPost();
        //setUpPaginationLahyout
        setUp();
        //setUpImagesLayout
        setupImage(root);

        return root;
    }

    //Here you can restore saved data in onSaveInstanceState Bundle
    private void onRestoreInstanceState(Bundle savedInstanceState){
        if(savedInstanceState!=null){
         oldViewList = (List<UserViewPost>) savedInstanceState.getSerializable("title");
        }
    }
    //Here you Save your data
    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putSerializable("viewPostList", (Serializable) userViewPostList);
    }

    @Override
    public void onViewStateRestored(@Nullable Bundle savedInstanceState) {
        super.onViewStateRestored(savedInstanceState);
        if(savedInstanceState!=null)
        oldViewList = (List<UserViewPost>) savedInstanceState.getSerializable("viewPostList");
    }

    private void setupImage(View root) {
        blockUserImage=root.findViewById(R.id.block_user_image);
        termsImage=root.findViewById(R.id.terms_of_use_image);
        privacyPolicyImage=root.findViewById(R.id.privacy_policy_image);
        contactUsImage=root.findViewById(R.id.contact_us_image);
        cancelImage=root.findViewById(R.id.cancel_image);
        blockUserImage.setOnClickListener(this);
        termsImage.setOnClickListener(this);
        privacyPolicyImage.setOnClickListener(this);
        contactUsImage.setOnClickListener(this);
        cancelImage.setOnClickListener(this);
        locationButton.setOnClickListener(this);
        postNowButton.setOnClickListener(this);
        blockLayout.setOnClickListener(this);
        reportLayout.setOnClickListener(this);

    }

    private void setUp() {
       /* mLayoutManager = new LinearLayoutManager(getContext());
        mLayoutManager.setOrientation(RecyclerView.HORIZONTAL);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
        mAdapter = new HomePaginationAdapter(new ArrayList<UserViewPost>(),getContext());*/

       // mSwipeRefreshLayout.setOnRefreshListener(this);
        /*singleAdapter = new HomeSingleAdapter(userViewPostList,getContext(),this);
        mRecyclerView.setAdapter(singleAdapter);*/
        mAdapter = new HomePaginationAdapter(userViewPostList,getContext(),this);
        mRecyclerView.setHasFixedSize(true);
        /*mLayoutManager = new LinearLayoutManager(getContext(),LinearLayoutManager.HORIZONTAL,false);
        mLayoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        mRecyclerView.setLayoutManager(mLayoutManager);*/
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false);
        SnapHelper snapHelper = new PagerSnapHelper();
        mRecyclerView.setLayoutManager(layoutManager);
        snapHelper.attachToRecyclerView(mRecyclerView);
       // mRecyclerView.addItemDecoration(new EqualSpacingItemDecoration(4, EqualSpacingItemDecoration.HORIZONTAL));

        //show post on user or Admin with there role

           mRecyclerView.setAdapter(mAdapter);

           /*homeFeedId=sessionParam.homePostId;
           homeCurrentPage=sessionParam.homeCurrentPage;
           homeTotalPage=sessionParam.homeTotalPage;
           homeFeedPosition=sessionParam.homePostPosition;
           if (homeCurrentPage>0){
               currentPage=homeCurrentPage;
           }*/


       // sessionParam.SavePost(getContext(),homeFeedId,homeCurrentPage,homeTotalPage,homeFeedPosition);



        if (postId > 0) {
                userSingleValue(postId, this);
                parentClassName="SinglePostCallback";
                showMsgLayout.setVisibility(View.GONE);
            } else {
                showMsgLayout.setVisibility(View.VISIBLE);
                if(!sessionParam.premieUser) {
                    if (latitude>0.0&&longitude>0.0)
                    userViewPost();
                }else{
                    userViewPost();
                }
            }

        /**
         * add scroll listener while user reach in bottom load more will call
         */
        mRecyclerView.addOnScrollListener(new PaginationScrollList(layoutManager) {
            @Override
            protected void loadMoreItems() {
                isLoading = true;
                currentPage++;
                homePage=currentPage;
                userViewPost();
            }

            @Override
            protected void loadPreviousItems() {
               /* if (currentPage>=1&& currentPage<=totalPage) {
                    isLoading = true;
                    currentPage--;
                    previousUserViewPost();
                }*/
            }

            @Override
            public int getTotalPageCount() {
                return totalPage;
            }

            @Override
            public boolean isLastPage() {
                return isLastPage;
            }
            @Override
            public boolean isLoading() {
                return isLoading;
            }
        });

       // String address=getCompleteAddressString(latitude,longitude);

    }

    private void previousUserViewPost() {
        baseRequest = new BaseRequest(getContext());
        baseRequest.setBaseRequestListner(new RequestReciever() {
            @Override
            public void onSuccess(int requestCode, String Json, Object object) {
                try {

                    //utility.dismiss();
                    JSONObject jsonObject = new JSONObject(Json);
                    totalPage = jsonObject.optInt("totalPageCount");
                    int   currentPage = jsonObject.optInt("currentPage");
                    JSONArray jsonArray = new JSONArray(object.toString());
                  List<UserViewPost>  PostList = baseRequest.getDataList(jsonArray, UserViewPost.class);
                    // utility.showToast(getContext(),object.toString());
                    firstCall=false;
                    homeCurrentPage=currentPage;
                    homeTotalPage=totalPage;
                    if(PostList.size()>0) {

                        mAdapter.showLocation();
                        if (currentPage != PAGE_START)
                            mAdapter.removeLoading();

                        List<UserViewPost> list = new ArrayList<>();
                        try {
                            if (userViewPostList.size() > 0) {

                                for (int i = 0; i < PostList.size(); i++) {
                                    for (int j = 0; j < userViewPostList.size(); j++) {
                                        if (userViewPostList.get(j).getId().equals(PostList.get(i).getId())) {
                                            // list.add(PostList.get(i));
                                            PostList.remove(i);
                                        }
                                    }
                                }
                            }
                        }catch (Exception e){
                            e.printStackTrace();
                        }

                        if (PostList.size()>0){
                        for (int i=0;i<PostList.size();i++){
                          //  userViewPostList.add(i,PostList.get(i));
                            mAdapter.addFirst(PostList.get(i));
                        }
                        }
                       // mAdapter.addAll(userViewPostList);

                        mAdapter.notifyDataSetChanged();
                        // mSwipeRefreshLayout.setRefreshing(false);

                        isLoading = false;
                        if (homeFeedPosition>0){
                            setCurrentItem(homeFeedPosition,true);
                        }


                    }else {
                        showMsgLayout.setVisibility(View.VISIBLE);
                        showDataLayout.setVisibility(View.GONE);
                        longClickLayout.setVisibility(View.GONE);
                        tagImageLayout.setVisibility(View.GONE);
                    }
                    // mAdapter.addAll(userViewPostList);
                    //mRecyclerView.setAdapter(mAdapter);

                }catch (Exception e){
                    e.printStackTrace();
                    utility.dismiss();
                }
            }

            @Override
            public void onFailure(int requestCode, String errorCode, String message) {
                showMsgLayout.setVisibility(View.VISIBLE);
                showDataLayout.setVisibility(View.GONE);
                longClickLayout.setVisibility(View.GONE);
                tagImageLayout.setVisibility(View.GONE);

                utility.dismiss();
                try {
                    /*if (message.equals("You are not in the range of 5 miles."))
                        showPremiumMsg();*/
                }catch (Exception e){
                    e.printStackTrace();
                }
            }

            @Override
            public void onNetworkFailure(int requestCode, String message) {
                // errorLayout.showError(message);
                utility.dismiss();
                try {
                    showMsgLayout.setVisibility(View.VISIBLE);
                    showDataLayout.setVisibility(View.GONE);
                    longClickLayout.setVisibility(View.GONE);
                    tagImageLayout.setVisibility(View.GONE);
                }catch (Exception e)
                {e.printStackTrace();}

                utility.showToast(getContext(),message);
            }
        });

        JsonObject main_object = new JsonObject();
        // premiumUser=true;
        if (premiumUser){
            // main_object.addProperty("latitude", latitude);
            //main_object.addProperty("longitude", longitude);
            main_object.addProperty("localeId",sessionParam.localeId);
        }else {
            main_object.addProperty("latitude", latitude);
            main_object.addProperty("longitude", longitude);
        }


        main_object.addProperty("page", currentPage);
        Log.d(TAG, "loadItemFromServer: " +" page:- "+ currentPage+" lat:-"+latitude+" long:- "+longitude);
       /* if(object!=null)
        Log.d(TAG, "loadItemFromServer: " + object.toString());*/
        baseRequest.callAPIPost(1, main_object, getString(R.string.api_userViewPosts));


    }


    @Override
    public void onResume() {
        super.onResume();
// make the device update its location
        try {
            location.beginUpdates();
        }catch (Exception e){
            e.printStackTrace();
        }

    }
    private int getCurrentItem(){
        return ((LinearLayoutManager)mRecyclerView.getLayoutManager())
                .findFirstVisibleItemPosition();
    }

    private void setCurrentItem(int position, boolean smooth){
        if (smooth)
            mRecyclerView.smoothScrollToPosition(position);
        else
            mRecyclerView.scrollToPosition(position);
    }

    @Override
    public void onPause() {
        // stop location updates (saves battery)
        location.endUpdates();
        /*if (userViewPostList.size()>0) {

            int position = getCurrentItem();
            homeFeedPosition=position;
            if (homeFeedPosition>0) {
                if (position < userViewPostList.size() && position != 0) {
                    UserViewPost post = userViewPostList.get(position);
                    homeFeedId = post.getId();
                }

                //view context, postId,  viewPage, totalPage , position
                sessionParam.SavePost(getContext(), homeFeedId, homeCurrentPage, homeTotalPage, homeFeedPosition);
            }
        }*/
        // ...
        super.onPause();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        mLocationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
        try{
            if (childFragment!=null) {
               /* FragmentTransaction transaction = getActivity().getSupportFragmentManager()
                        .beginTransaction();

                transaction.remove(childFragment);

                transaction.commit();*/
                FragmentManager manager = getActivity().getSupportFragmentManager();
                FragmentTransaction trans = manager.beginTransaction();
                trans.remove(childFragment);
                trans.commit();
                manager.popBackStack();
            }
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    //api call when single Id click
    private void userSingleValue(Integer postId,PaginationListenerCallback listener) {
        baseRequest = new BaseRequest(getContext());
        baseRequest.setBaseRequestListner(new RequestReciever() {
            @Override
            public void onSuccess(int requestCode, String Json, Object object) {
                try {
                    Gson gson = new Gson();
                    userViewPostList.clear();
                    //singleAdapter.clear();

                    try {
                      //CountryResponse  response = gson.fromJson(CountryResponse.toString(), CountryResponse.class);
                    UserViewPost    userViewPost = gson.fromJson(object.toString(), UserViewPost.class);

                   /* JSONObject jsonObject = new JSONObject(Json);
                  //  totalPage = jsonObject.optInt("totalPageCount");
                    JSONArray jsonArray = new JSONArray(object.toString());
                    userViewPostList = baseRequest.getDataList(jsonArray, UserViewPost.class);*/

                    // utility.showToast(getContext(),object.toString());
                    if(userViewPost.getId()>0) {
                        showMsgLayout.setVisibility(View.GONE);
                        longClickLayout.setVisibility(View.GONE);
                        showDataLayout.setVisibility(View.VISIBLE);
                        tagImageLayout.setVisibility(View.GONE);
                       // userViewPostList.add(userViewPost);
                        //mAdapter.add(userViewPost);
                        // mSwipeRefreshLayout.setRefreshing(false);
                       // singleAdapter.add(userViewPost);
                        userViewPostList.add(userViewPost);
                        singleAdapter=new HomeSingleAdapter(userViewPostList,getContext(),listener);
                        mRecyclerView.setAdapter(singleAdapter);

                    }else {
                        showMsgLayout.setVisibility(View.VISIBLE);
                        showDataLayout.setVisibility(View.GONE);
                        longClickLayout.setVisibility(View.GONE);
                        tagImageLayout.setVisibility(View.GONE);
                    }
                  }catch (Exception e){
                      e.printStackTrace();
                  }
                    // mAdapter.addAll(userViewPostList);
                    //mRecyclerView.setAdapter(mAdapter);

                }catch (Exception e){
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int requestCode, String errorCode, String message) {
                showMsgLayout.setVisibility(View.VISIBLE);
                showDataLayout.setVisibility(View.GONE);
                longClickLayout.setVisibility(View.GONE);
                tagImageLayout.setVisibility(View.GONE);

                //  showLocationDialod();
            }

            @Override
            public void onNetworkFailure(int requestCode, String message) {
                // errorLayout.showError(message);
                utility.showToast(getContext(), message);
                try {
                    showMsgLayout.setVisibility(View.VISIBLE);
                    showDataLayout.setVisibility(View.GONE);
                    longClickLayout.setVisibility(View.GONE);
                    tagImageLayout.setVisibility(View.GONE);

                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        });

        JsonObject main_object = new JsonObject();

        main_object.addProperty("postId", postId);
       /* if(object!=null)
        Log.d(TAG, "loadItemFromServer: " + object.toString());*/
        baseRequest.callAPIPost(1, main_object, getString(R.string.api_getPostDetails));
    }

    /*is api call used for calling userPostList API
     */
    public void userViewPost() {
       // mSwipeRefreshLayout.setRefreshing(true);
        //utility.loading();
        baseRequest = new BaseRequest(getContext());
        baseRequest.setBaseRequestListner(new RequestReciever() {
            @Override
            public void onSuccess(int requestCode, String Json, Object object) {
                try {

                    //utility.dismiss();
                    JSONObject jsonObject = new JSONObject(Json);
                    totalPage = jsonObject.optInt("totalPageCount");
                    int   currentPage = jsonObject.optInt("currentPage");
                    JSONArray jsonArray = new JSONArray(object.toString());
                  List<UserViewPost>  viewPostList = baseRequest.getDataList(jsonArray, UserViewPost.class);
                   // utility.showToast(getContext(),object.toString());
                    firstCall=false;
                    homeCurrentPage=currentPage;
                    homeTotalPage=totalPage;
                    if(viewPostList.size()>0) {
                        showMsgLayout.setVisibility(View.GONE);
                        longClickLayout.setVisibility(View.GONE);
                        showDataLayout.setVisibility(View.VISIBLE);
                        tagImageLayout.setVisibility(View.GONE);
                            mAdapter.showLocation();
                            if (currentPage != PAGE_START)
                                mAdapter.removeLoading();
                        List<UserViewPost> list = new ArrayList<>();
                            if (userViewPostList.size()>0) {

                                for (int i = 0; i < viewPostList.size(); i++) {
                                    for (int j = 0; j < userViewPostList.size(); j++) {
                                        if (userViewPostList.get(j).getId().equals(viewPostList.get(i).getId())) {
                                           // list.add(viewPostList.get(i));
                                            viewPostList.remove(i);
                                        }
                                    }
                                }

                            }/*else {
                                mAdapter.addAll(viewPostList);
                            }*/
                        if (viewPostList.size()>0)
                            mAdapter.addAll(viewPostList);
                            mAdapter.notifyDataSetChanged();
                            // mSwipeRefreshLayout.setRefreshing(false);
                            if (currentPage < totalPage) {
                                mAdapter.addLoading();
                            }
                            else {
                                isLastPage = true;
                            }
                            isLoading = false;
                            if (homeFeedPosition>0){
                                setCurrentItem(homeFeedPosition,true);
                            }


                    }else {
                        showMsgLayout.setVisibility(View.VISIBLE);
                        showDataLayout.setVisibility(View.GONE);
                        longClickLayout.setVisibility(View.GONE);
                        tagImageLayout.setVisibility(View.GONE);
                    }
                   // mAdapter.addAll(userViewPostList);
                    //mRecyclerView.setAdapter(mAdapter);

                }catch (Exception e){
                    e.printStackTrace();
                    utility.dismiss();
                }
            }

            @Override
            public void onFailure(int requestCode, String errorCode, String message) {
                showMsgLayout.setVisibility(View.VISIBLE);
                showDataLayout.setVisibility(View.GONE);
                longClickLayout.setVisibility(View.GONE);
                tagImageLayout.setVisibility(View.GONE);

                utility.dismiss();
                try {
                    if (message.equals("You are not in the range of 5 miles."))
                            showPremiumMsg();
                }catch (Exception e){
                    e.printStackTrace();
                }
            }

            @Override
            public void onNetworkFailure(int requestCode, String message) {
                // errorLayout.showError(message);
                utility.dismiss();
                try {
                    showMsgLayout.setVisibility(View.VISIBLE);
                    showDataLayout.setVisibility(View.GONE);
                    longClickLayout.setVisibility(View.GONE);
                    tagImageLayout.setVisibility(View.GONE);
                }catch (Exception e)
                {e.printStackTrace();}

                utility.showToast(getContext(),message);
            }
        });

        JsonObject main_object = new JsonObject();
       // premiumUser=true;
        if (premiumUser){
           // main_object.addProperty("latitude", latitude);
            //main_object.addProperty("longitude", longitude);
            main_object.addProperty("localeId",sessionParam.localeId);
        }else {
            main_object.addProperty("latitude", latitude);
            main_object.addProperty("longitude", longitude);
           /* main_object.addProperty("latitude", "39.892844");
             main_object.addProperty("longitude", "4.800408");*/
        }




        //  main_object.addProperty("latitude", "29.695357");
       // main_object.addProperty("longitude", "27.865982");
        //main_object.addProperty("latitude", "22.695357");
        //main_object.addProperty("longitude", "75.865982");
       // kyro location  "latitude": "22.695357",
        //        "longitude": "75.865982",
        main_object.addProperty("page", currentPage);
        Log.d(TAG, "loadItemFromServer: " +" page:- "+ currentPage+" lat:-"+latitude+" long:- "+longitude);
       /* if(object!=null)
        Log.d(TAG, "loadItemFromServer: " + object.toString());*/
        baseRequest.callAPIPost(1, main_object, getString(R.string.api_userViewPosts));
    }

    private void showPremiumMsg() {
        mListener.onButtonClick("premiumMsg","homeBack");
    }






    private void getLikeClick(UserViewPost viewPost,final Boolean likeValue,int position) {
        baseRequest = new BaseRequest(getContext());
        baseRequest.setBaseRequestListner(new RequestReciever() {
            @Override
            public void onSuccess(int requestCode, String Json, Object object) {
                  try {

                    JSONObject jsonObject = new JSONObject(Json);

                    // utility.showToast(getContext(),object.toString());

                      modifyItem(position,likeValue,viewPost);

                }catch (Exception e){
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int requestCode, String errorCode, String message) {
                //utility.showToast(getContext(),message);
                boolean value=false;
                if (likeValue){
                    value=false;
                }else {
                    value=true;
                }
                modifyItem(position,value,viewPost);
            }

            @Override
            public void onNetworkFailure(int requestCode, String message) {

                utility.showToast(getContext(),message);
                boolean value=false;
//                if (likeValue){
//                    value=false;
//                }else {
//                    value=true;
//                }
//                modifyItem(position,value,viewPost);
            }
        });

        Integer status;
        if (likeValue){
            status=1;
        }else {
            status=2;
        }
        JsonObject main_object = new JsonObject();
        main_object.addProperty("postId", viewPost.getId());
        main_object.addProperty("likeStatus", status);
       /* JsonObject object = Functions.getClient().getJsonMapObject(
                "postId",
                "likeStatus",status );*/
        baseRequest.callAPIPostWOLoader(1, main_object, getString(R.string.api_userLikedPost));
    }
    public void modifyItem(final int position, Boolean likeValue, final UserViewPost  updateList) {
        updateList.setLikedStatus(likeValue);
        userViewPostList.set(position, updateList);
        mAdapter.notifyItemChanged(position);
    }

    @Override
    public void onLocationChanged(Location location) {
        Log.i(TAG, String.valueOf(location.getLatitude()));
        Log.i(TAG, String.valueOf(location.getLongitude()));
        latitude=location.getLatitude();
        longitude=location.getLongitude();

    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {

    }

    @Override
    public void onProviderEnabled(String s) {

    }

    @Override
    public void onProviderDisabled(String s) {

    }

    @Override
    public void onRefresh() {
       /* itemCount = 0;
        currentPage = PAGE_START;
        isLastPage = false;
        if (userViewPostList.size()>0)
        userViewPostList.clear();
        mAdapter.clear();
        userViewPost();*/
    }

    @Override
    public void onPaginationClick(UserViewPost viewPost, Boolean likeValue,int position,String enterValue) {

        if(enterValue.equals("like")||enterValue.equals("connection")) {
            try {

                try {
                    try {
                        AudioManager audio_mngr = (AudioManager) getContext().getSystemService(Context.AUDIO_SERVICE);
                        switch (audio_mngr.getRingerMode()) {
                            case AudioManager.RINGER_MODE_SILENT:
                                Log.i("MyApp","Silent mode");
                                audio_mngr.setRingerMode(AudioManager.RINGER_MODE_VIBRATE);
                                break;
                            case AudioManager.RINGER_MODE_VIBRATE:
                                Log.i("MyApp","Vibrate mode");
                                audio_mngr.setRingerMode(AudioManager.RINGER_MODE_VIBRATE);
                                break;
                            case AudioManager.RINGER_MODE_NORMAL:
                                Log.i("MyApp","Normal mode");
                                break;
                        }

                       // audio_mngr.setStreamVolume(AudioManager.STREAM_NOTIFICATION, 0, AudioManager.FLAG_REMOVE_SOUND_AND_VIBRATE);


                        audio_mngr.setVibrateSetting(AudioManager.VIBRATE_TYPE_RINGER,
                                AudioManager.VIBRATE_SETTING_ON);
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
                Vibrator vibe = (Vibrator) getContext().getSystemService(Context.VIBRATOR_SERVICE);
                // Vibrate for 500 milliseconds
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    vibe.vibrate(VibrationEffect.createOneShot(500, VibrationEffect.DEFAULT_AMPLITUDE));
                } else {
                    //deprecated in API 26
                    vibe.vibrate(500);
                }
            }catch (Exception e){
                e.printStackTrace();
            }
        }
        if (enterValue.equals("reply")){
            mListener.onConnectionCall(viewPost,enterValue,parentClassName);
        }
        if (enterValue.equals("originalPost")){
            userSingleValue(viewPost.getOriginalPostId(),this);
        }


        if (enterValue.equals("connection")) {
            mListener.onConnectionCall(viewPost,enterValue,parentClassName);
        }
        if (enterValue.equals("like")) {
        getLikeClick(viewPost, likeValue, position);
        }
        if (enterValue.equals("headerLayout")){
            mListener.onConnectionCall(viewPost,enterValue,parentClassName);
        }
        if (enterValue.equals("tagName")){
            mListener.onTagName(viewPost,parentClassName);
        }
        if (enterValue.equals("likeCount")){
            mListener.onLikeCountUser(viewPost,parentClassName);
        }
        if (enterValue.equals("premiumUpgrade")){
            mListener.onButtonClick("premiumUpgrade","homeBack");
        }
    }

    @Override
    public void onPaginationLongClick(UserViewPost viewPost,int position,String viewPostDetail) {
        showMsgLayout.setVisibility(View.GONE);
        showDataLayout.setVisibility(View.VISIBLE);
        longClickLayout.setVisibility(View.VISIBLE);
        tagImageLayout.setVisibility(View.GONE);
            Log.d("userName","session: "+userId+" "+"viewPost: "+viewPost.getName().toLowerCase().trim());
        if ((userId==(viewPost.getUserId()))){
            blockLayout.setVisibility(View.GONE);
        }else {
            blockLayout.setVisibility(View.VISIBLE);
        }
        if (viewPostDetail.equals("admin")){
            blockLayout.setVisibility(View.GONE);
        }else {
            blockLayout.setVisibility(View.VISIBLE);
        }

       selectedViewPost= viewPost;
       //mListener.onBlockFragmentCall(viewPost,position);

    }

    @Override
    public void autoLinkListener(int linkType,String autoLink, String matchedText,UserViewPost viewPost) {
       // showDialog(autoLink, matchedText);
        matchedText=matchedText.replace("#","");
        //strTag.set(i,strTag.get(i).replace("#", ""));.
        //LinkType 1=HashTag,2=Mention
        if (linkType==1){
           // if (autoLink.equals("")){
            if (!Validations.isEmptyString(matchedText)) {
                List<TagList> tagList = new ArrayList<>();

                for (int i = 0; i < viewPost.getHashTagList().size(); i++) {
                    if ((matchedText.toLowerCase().trim()).contains(viewPost.getHashTagList().get(i).getName().toLowerCase().trim())) {
                        tagList.add(viewPost.getHashTagList().get(i));
                        break;
                    }
                }
                if (tagList.size()>0) {
                   // showTagList(tagList.get(0));
                    mListener.onHashTagCall(tagList.get(0).getId(),tagList.get(0).getName(),parentClassName);

                   /* showMsgLayout.setVisibility(View.GONE);
                    showDataLayout.setVisibility(View.GONE);
                    longClickLayout.setVisibility(View.GONE);
                    tagImageLayout.setVisibility(View.VISIBLE);
                     childFragment =  TagImageFragment.newInstance(tagList.get(0).getId(),tagList.get(0).getName());
                    FragmentTransaction transaction = getChildFragmentManager().beginTransaction();
                    transaction.addToBackStack(null);
                    transaction.replace(R.id.child_fragment, childFragment,"child").commit();*/



                }

            }
        }
        if (linkType==2){
            if (!Validations.isEmptyString(matchedText.trim())) {
                List<UserList> userList = new ArrayList<>();
                UserList personData=new UserList();
                matchedText=matchedText.replace("@", "");
                matchedText=matchedText.replace("_"," ");
                for(UserList playerName : viewPost.getUserTagList()){
                    if(playerName.getName().trim().equalsIgnoreCase(matchedText.trim())){
                        personData.setId(playerName.getId());
                        userList.add(playerName);
                        break;
                    }
                }
                if (userList.size()>0) {
                    mListener.onMentionCall(viewPost, personData.getId(), 1,parentClassName);
                }
            }


        }


    }

    @Override
    public void ResetClickListener(int position,String locationChange) {
      /*  smoothScroller.setTargetPosition(position);
        layoutManager.startSmoothScroll(smoothScroller);*/
      if (!Validations.isEmptyString(locationChange)){
          if (locationChange.equals("locationChange")){
              mListener.onButtonClick("locationButton","homeBack");
          }
      }else {
          mRecyclerView.getLayoutManager().scrollToPosition(0);
      }


    }


    private void showDialog(String title, String message) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setMessage(message)
                .setTitle(title)
                .setCancelable(false)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                    }
                });
        final AlertDialog alert = builder.create();
        alert.show();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.block_user_layout:
            case R.id.block_user_image:
                getUserBlock(selectedViewPost);
                break;
            case R.id.terms_of_use_image:
                termsAndCondition();
                break;
            case R.id.privacy_policy_image:
                privacyApi();
                break;
            case R.id.report_layout:
            case R.id.contact_us_image:
                feedbackDialog(selectedViewPost);
                break;
            case R.id.cancel_image:
                showMsgLayout.setVisibility(View.GONE);
                showDataLayout.setVisibility(View.VISIBLE);
                longClickLayout.setVisibility(View.GONE);
                tagImageLayout.setVisibility(View.GONE);
                break;
            case R.id.home_location_button:
                mListener.onButtonClick("locationButton","homeBack");
                break;
            case R.id.post_now_button:
                mListener.onButtonClick("postNowButton","homeBack");
                break;
        }

    }

    private void feedbackDialog(UserViewPost selectedViewPost) {

        //SearchView searchView;

        final Dialog dialog = new Dialog(getContext());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.feedback_dialog);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCanceledOnTouchOutside(true);

        final Button submitButton = dialog.findViewById(R.id.submit_feed_button);
        final EditText feedEt = dialog.findViewById(R.id.feed_et_comment);
        final TextView titleName = dialog.findViewById(R.id.title_name);
        titleName.setText("Report");

        submitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String feedComment=feedEt.getText().toString().trim();
                if (!Validations.isEmptyString(feedComment)) {
                    contactUsApi(feedComment,selectedViewPost);
                    dialog.cancel();
                }else {
                    utility.showToast(getContext(),"Please Enter feedback");
                }
            }
        });



        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = dialog.getWindow();
        lp.copyFrom(window.getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);
        dialog.show();

    }
    private void contactUsApi(String feedComment, UserViewPost selectedViewPost) {
        baseRequest = new BaseRequest(getContext());
        baseRequest.setBaseRequestListner(new RequestReciever() {
            @Override
            public void onSuccess(int requestCode, String Json, Object object) {
                try {

                    JSONObject jsonObject = new JSONObject(Json);
                    String message=jsonObject.getString("message");
                    utility.showToast(getContext(),message);

                }catch (Exception e){
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int requestCode, String errorCode, String message) {
                //utility.showToast(getContext(),message);
            }

            @Override
            public void onNetworkFailure(int requestCode, String message) {

                utility.showToast(getContext(),message);
            }
        });

        /*{
            "postId":"10",
                "summary":"donated to was uploaded upon the single's release"
        }*/


        JsonObject main_object = new JsonObject();
        main_object.addProperty("postId", selectedViewPost.getId());
        main_object.addProperty("summary", feedComment);
        main_object.addProperty("userId", userId);
       /* JsonObject object = Functions.getClient().getJsonMapObject(
                "postId",
                "likeStatus",status );*/
        baseRequest.callAPIPostWOLoader(1, main_object, getString(R.string.api_userReportPost));

    }

    private void privacyApi() {
       try{
           //Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://fresherappdemo.chetaru.co.uk/privacyPolicy"));
           Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://freshersapp.chetaru.co.uk/privacyPolicy"));
           startActivity(browserIntent);
       }catch (Exception e){
           e.printStackTrace();
       }

    }



    private void termsAndCondition() {
        try{
           /* Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://freshersapp.chetaru.co.uk/termsOfServices" +
                    "\n" +
                    "\n" +
                    "\n" +
                    "Terms Of Services\n" +
                    "https://freshersapp.chetaru.co.uk\n"));*/
            Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://freshersapp.chetaru.co.uk/termsOfServices"));
            startActivity(browserIntent);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private void getUserBlock(UserViewPost selectedViewPost) {
        baseRequest = new BaseRequest(getContext());
        baseRequest.setBaseRequestListner(new RequestReciever() {
            @Override
            public void onSuccess(int requestCode, String Json, Object object) {
                try {

                    JSONObject jsonObject = new JSONObject(Json);
                    String message=jsonObject.getString("message");

                     utility.showToast(getContext(),message);

                }catch (Exception e){
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int requestCode, String errorCode, String message) {
               // utility.showToast(getContext(),message);
            }

            @Override
            public void onNetworkFailure(int requestCode, String message) {

                utility.showToast(getContext(),message);
            }
        });

       /* {
            "blockUserId":"6",
                "type":1
        }
        "type" - 1 for block , 2 for unblock*/
       /*if (blockUser){
           blockUser=false;
           type=1;
       }else {
           blockUser=true;
           type=2;
       }*/

        JsonObject main_object = new JsonObject();
        main_object.addProperty("blockUserId",selectedViewPost.getUserId());
        main_object.addProperty("type", 1);
       /* JsonObject object = Functions.getClient().getJsonMapObject(
                "postId",
                "likeStatus",status );*/
        baseRequest.callAPIPostWOLoader(1, main_object, getString(R.string.api_blockUser));
    }

    @Override
    public void onDestroyView() {

        try {
            if (childFragment != null) {
                FragmentManager mFragmentMgr = getActivity().getSupportFragmentManager();
                FragmentTransaction mTransaction = mFragmentMgr.beginTransaction();
                Fragment childFragment = mFragmentMgr.findFragmentByTag("child");
                mTransaction.remove(childFragment);
                mTransaction.commit();
            }
        }catch (Exception e){
            e.printStackTrace();
        }
            super.onDestroyView();


      /*  try{
            if (childFragment!=null) {
                FragmentTransaction transaction = getActivity().getSupportFragmentManager()
                        .beginTransaction();

                transaction.remove(childFragment);

                transaction.commit();
            }
        }catch(Exception e){
        }*/

        super.onDestroyView();
    }

    private String getCompleteAddressString(double LATITUDE, double LONGITUDE) {
        String strAdd = "";
        Geocoder geocoder = new Geocoder(getContext(), Locale.getDefault());
        try {
            List<Address> addresses = geocoder.getFromLocation(LATITUDE, LONGITUDE, 1);
            if (addresses != null) {
                Address returnedAddress = addresses.get(0);
                StringBuilder strReturnedAddress = new StringBuilder("");

                for (int i = 0; i <= returnedAddress.getMaxAddressLineIndex(); i++) {
                    strReturnedAddress.append(returnedAddress.getAddressLine(i)).append("\n");
                }
                strAdd = strReturnedAddress.toString();

                if (addresses != null && addresses.size() > 0) {
                    String address = addresses.get(0).getAddressLine(0);
                    String city = addresses.get(0).getLocality();
                    String state = addresses.get(0).getAdminArea();
                    String country = addresses.get(0).getCountryName();
                    String postalCode = addresses.get(0).getPostalCode();
                    String knownName = addresses.get(0).getFeatureName();

                    //.setText(address + " " + city + " " + country);
                }
                Log.w("My  loction address", strReturnedAddress.toString());
            } else {
                Log.w("My  loction address", "No Address returned!");
            }
        } catch (Exception e) {
            e.printStackTrace();
            Log.w("My  loction address", "Canont get Address!");
        }
        return strAdd;
    }


        public void ShowSettingAlert(){
            AlertDialog.Builder builder =new AlertDialog.Builder(getContext());
            builder.setTitle("Allow FreshersApp to access your location?");
            builder.setMessage("FresherApp would like to use Your current location in order to post feed.");
            builder.setPositiveButton("Setting", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    Intent intent=new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                    mContext.startActivity(intent);
                }
            });
            builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int i) {

                    dialog.cancel();
                }
            });
            builder.show();
        }

    public interface homeFragmentListener{
        public void mentionCallListener(UserList userList);
    }
    public abstract class PaginationScrollList extends RecyclerView.OnScrollListener {

        private LinearLayoutManager layoutManager;
        private static final int PAGE_SIZE = 5;

        public PaginationScrollList(LinearLayoutManager layoutManager) {
            this.layoutManager = layoutManager;
        }

        @Override
        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
            super.onScrolled(recyclerView, dx, dy);
            try {
                int scrollPre = 0;
                scrollPre = dx;
                int visibleItemCount = layoutManager.getChildCount();
                int totalItemCount = layoutManager.getItemCount();
                int firstVisibleItemPosition = layoutManager.findFirstVisibleItemPosition();

                if (!isLoading() && !isLastPage()) {
                    if ((visibleItemCount + firstVisibleItemPosition) >= totalItemCount
                            && firstVisibleItemPosition >= 0
                            && totalItemCount >= PAGE_SIZE) {
                        loadMoreItems();
                    }
                }
               // if (!isLoading() && !isLastPage()) {
                  /*  if (firstVisibleItemPosition <= 0
                            && visibleItemCount < 2
                            && scrollPre < 0
                            && firstVisibleItemPosition >= 0
                            && totalItemCount >= PAGE_SIZE) {
                        loadPreviousItems();
                    }*/
              //  }
                   /* if (totalItemCount<5&&currentPage>1){
                        loadPreviousItems();
                    }*/


            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        protected abstract void loadMoreItems();

        protected abstract void loadPreviousItems();

        public abstract int getTotalPageCount();

        public abstract boolean isLastPage();

        public abstract boolean isLoading();
    }

    public Bitmap blur(Bitmap image) {
        if (null == image) return null;

        Bitmap outputBitmap = Bitmap.createBitmap(image);
        final RenderScript renderScript = RenderScript.create(getActivity());
        Allocation tmpIn = Allocation.createFromBitmap(renderScript, image);
        Allocation tmpOut = Allocation.createFromBitmap(renderScript, outputBitmap);

//Intrinsic Gausian blur filter
        ScriptIntrinsicBlur theIntrinsic = ScriptIntrinsicBlur.create(renderScript, Element.U8_4(renderScript));
        theIntrinsic.setRadius(BLUR_RADIUS);
        theIntrinsic.setInput(tmpIn);
        theIntrinsic.forEach(tmpOut);
        tmpOut.copyTo(outputBitmap);
        return outputBitmap;
    }



}

