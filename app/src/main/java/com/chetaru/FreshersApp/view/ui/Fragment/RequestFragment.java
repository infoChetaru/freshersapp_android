package com.chetaru.FreshersApp.view.ui.Fragment;

import android.content.Context;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.chetaru.FreshersApp.R;
import com.chetaru.FreshersApp.service.model.PendingRequestList;
import com.chetaru.FreshersApp.service.retrofit.BaseRequest;
import com.chetaru.FreshersApp.service.retrofit.RequestReciever;
import com.chetaru.FreshersApp.utility.SessionParam;
import com.chetaru.FreshersApp.utility.Utility;

import com.chetaru.FreshersApp.view.adapter.RequestAdapter;
import com.chetaru.FreshersApp.view.callback.RequestListCallback;
import com.chetaru.FreshersApp.view.customView.ItemOffsetDecoration;
import com.google.gson.Gson;
import com.google.gson.JsonObject;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link RequestFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class RequestFragment extends Fragment implements RequestAdapter.requestAdListener {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    public RequestFragment() {
        // Required empty public constructor
    }

    private Unbinder unbinder;
    List<PendingRequestList> connList;

    Utility utility;
    SessionParam sessionParam;
    String tokenId="";
    BaseRequest baseRequest;
    Context mContext;
    private static final int PAGE_START = 1;
    private boolean isLoading = false;
    private int currentPage = PAGE_START;
    private static int TOTAL_PAGES = 1;
    private boolean isLastPage = false;
    String dateTime;

    //Initilize View
    @BindView(R.id.request_refresh)
    SwipeRefreshLayout requestRefresh;
    @BindView(R.id.request_recycler_view)
    RecyclerView requestRecyclerView;

    @BindView(R.id.no_request_layout)
    LinearLayout noRequestLayout;
    @BindView(R.id.request_recycler_layout)
    LinearLayout recyclerLayout;

    LinearLayoutManager layoutManager;
    RequestAdapter requestAdapter;
    RequestListCallback mListener;

    int remaningDays=0;
    int connectionId=0;

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment RequestFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static RequestFragment newInstance(String param1, String param2) {
        RequestFragment fragment = new RequestFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_request, container, false);
        unbinder= ButterKnife.bind(this,view);
        utility=new Utility();
        sessionParam=new SessionParam(getContext());
        connList=new ArrayList<>();
        mContext=getContext();
        this.mListener= (RequestListCallback) getContext();
         /*for (int i=0;i<10;i++){
            PendingRequestList viewPost=new PendingRequestList();
            viewPost.setId(i+1);
            viewPost.setName("Ritesh");
            viewPost.setOpeningMessage("dsfds");
            viewPost.setImageUrl("https://production.chetaru.co.uk/freshersApp/public/uploads/postImages/post_5eaa73782290c1588228984.png");
            connList.add(viewPost);
        }*/
        requestAdapter=new RequestAdapter(connList,mContext,this);
        layoutManager = new GridLayoutManager(getContext(),2);
        requestRecyclerView.setLayoutManager(layoutManager);
        requestRecyclerView.setHasFixedSize(true);
        ItemOffsetDecoration itemDecoration = new ItemOffsetDecoration(getContext(), R.dimen.item_offset);
        requestRecyclerView.addItemDecoration(itemDecoration);
        requestRecyclerView.setAdapter(requestAdapter);
        //Visible or hide a layout
        noRequestLayout.setVisibility(View.VISIBLE);
        recyclerLayout.setVisibility(View.GONE);

        getPendingRequest();

        requestRecyclerView.addOnScrollListener(new PaginationScrollList(layoutManager) {
            @Override
            protected void loadMoreItems() {
                isLastPage=true;
                currentPage+=1;
                api_loadNextPage();
            }

            @Override
            public boolean isLastPage() {
                return isLastPage;
            }

            @Override
            public boolean isLoading() {
                return isLoading;
            }
        });
        requestRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                requestRefresh.setRefreshing(true);
                if(connList.size()>0) {
                    MyUploadRefresh();
                }else {
                    getPendingRequest();
                }
                requestRefresh.setRefreshing(false);
            }
        });

        return view;
    }

    private void MyUploadRefresh() {
        baseRequest=new BaseRequest(getContext());
        baseRequest.setBaseRequestListner(new RequestReciever() {
            @Override
            public void onSuccess(int requestCode, String Json, Object object) {

                try{
                    requestRefresh.setRefreshing(false);
                    Gson gson=new Gson();
                    JSONObject jsonObject = new JSONObject(Json);
                    JSONArray userJsonArray = jsonObject.getJSONArray("data");

                    List<PendingRequestList>   newUploadList = baseRequest.getDataList(userJsonArray, PendingRequestList.class);

                    JSONArray idsArray=new JSONArray();
                    try{
                        idsArray=jsonObject.getJSONArray("connectionList");
                        // connectionIds = (List<Integer>) idsArray;
                        //connectionIds= (List<Integer>) jsonObject.get("connectionList");

                    }catch (Exception e){
                        e.printStackTrace();
                    }
                    if (newUploadList.size()>0) {
                        for (int i = 0; i < newUploadList.size(); i++) {
                            for (int j = 0; j < connList.size(); j++) {
                                if (newUploadList.get(i).getId().equals(connList.get(j).getId())) {
                                    connList.remove(j);
                                    requestRecyclerView.removeViewAt(j);
                                    requestAdapter.notifyItemRemoved(j);
                                }
                               /* if (newUploadList.get(i).getUserId().equals(connList.get(j).getUserId())) {
                                    connList.remove(j);
                                }*/
                            }
                        }


                    }
                    if (idsArray.length()>0) {
                        for (int p = 0; p < idsArray.length(); p++) {
                            for (int q = 0; q < connList.size(); q++) {
                                if (idsArray.length()<=1) {
                                    if (idsArray.equals(connList.get(q).getId())) {
                                        connList.remove(q);
                                        requestRecyclerView.removeViewAt(q);
                                        requestAdapter.notifyItemRemoved(q);
                                    }
                                }else
                                if (idsArray.get(p).equals(connList.get(q).getId())) {
                                    connList.remove(q);
                                    requestRecyclerView.removeViewAt(q);
                                    requestAdapter.notifyItemRemoved(q);
                                }


                            }
                        }
                    }
                    if (newUploadList.size()>0){
                        for (int k=0;k<newUploadList.size();k++){
                            if (connList.size()>0) {
                                connList.add(k, newUploadList.get(k));
                            }else {
                                connList.add(newUploadList.get(k));
                            }
                        }
                    }
                    // uploadAdapter.addAll(uploadList);
                    if(connList.size()>0){
                        requestAdapter.clear();
                        requestAdapter.addAll(connList);
                    }
                    requestAdapter.notifyDataSetChanged();
                }catch (Exception  e){
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int requestCode, String errorCode, String message) {
               // utility.showToast(mContext,message);
            }

            @Override
            public void onNetworkFailure(int requestCode, String message) {

                utility.showToast(mContext,message);
            }
        });
        JsonObject main_object = new JsonObject();

        main_object.addProperty("page", 0);
        main_object.addProperty("date", dateTime);
        baseRequest.callAPIPost(1, main_object, getString(R.string.api_getUsersPendingRequestList));
    }


    @Override
    public void onResume() {
        super.onResume();
        if (connList.size()>0){
            //getPendingRequest();
        }
    }

    private void getPendingRequest() {
        currentPage=PAGE_START;
        baseRequest=new BaseRequest(mContext);
        baseRequest.setBaseRequestListner(new RequestReciever() {
            @Override
            public void onSuccess(int requestCode, String Json, Object object) {
               // connList.clear();
                Gson gson=new Gson();
                try{
                    JSONObject jsonObject=new JSONObject(Json);
                    TOTAL_PAGES=jsonObject.optInt("totalPageCount");
                    JSONArray jsonArray=new JSONArray(object.toString());
                    JSONArray userJsonArray = jsonObject.getJSONArray("data");
                    connList=baseRequest.getDataList(userJsonArray,PendingRequestList.class);
                    if (connList.size()>0){
                        noRequestLayout.setVisibility(View.GONE);
                        recyclerLayout.setVisibility(View.VISIBLE);
                    }else {
                        noRequestLayout.setVisibility(View.VISIBLE);
                        recyclerLayout.setVisibility(View.GONE);
                    }

                    /*for (int i=0;i<10;i++){
                        PendingRequestList viewPost=new PendingRequestList();
                        viewPost.setId(i+1);
                        viewPost.setName("Ritesh");
                        viewPost.setOpeningMessage("dsfds");
                        viewPost.setImageUrl("https://production.chetaru.co.uk/freshersApp/public/uploads/postImages/post_5eaa73782290c1588228984.png");
                        connList.add(viewPost);
                    }*/
                    requestAdapter.addAll(connList);
                  //  requestAdapter = new RequestAdapter(connList, getContext(),this::Req);

                    if (currentPage<TOTAL_PAGES)
                        requestAdapter.addLoadingFooter();
                    else isLastPage=true;

                    try {
                        if (connList.size() > 0) {
                            dateTime = connList.get(0).getUpdatedAt();
                        }
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int requestCode, String errorCode, String message) {
                //utility.showToast(mContext,message);
                noRequestLayout.setVisibility(View.VISIBLE);
                recyclerLayout.setVisibility(View.GONE);
            }

            @Override
            public void onNetworkFailure(int requestCode, String message) {
                utility.showToast(mContext,message);
            }
        });
        JsonObject main_object = new JsonObject();
        main_object.addProperty("page", currentPage);
        baseRequest.callAPIPost(1, main_object, getString(R.string.api_getUsersPendingRequestList));

    }
    private void api_loadNextPage() {
        currentPage=PAGE_START;
        baseRequest=new BaseRequest(mContext);
        baseRequest.setBaseRequestListner(new RequestReciever() {
            @Override
            public void onSuccess(int requestCode, String Json, Object object) {
                // connList.clear();
                Gson gson=new Gson();
                try{
                    JSONObject jsonObject=new JSONObject(Json);
                    TOTAL_PAGES=jsonObject.optInt("totalPageCount");
                    JSONArray jsonArray=new JSONArray(object.toString());
                    connList=baseRequest.getDataList(jsonArray,PendingRequestList.class);
                    //connAdapter.addAll(connList);
                    requestAdapter.removeLoadingFooter();
                    isLoading = false;
                    requestAdapter.addAll(connList);
                    //rv_list_archive.setAdapter(ad_notificationList);
                    if (currentPage != TOTAL_PAGES) requestAdapter.addLoadingFooter();
                    else isLastPage = true;
                }catch (Exception e){
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int requestCode, String errorCode, String message) {
                //utility.showToast(mContext,message);
            }

            @Override
            public void onNetworkFailure(int requestCode, String message) {
                utility.showToast(mContext,message);
            }
        });
        JsonObject main_object = new JsonObject();
        main_object.addProperty("page", currentPage);
        baseRequest.callAPIPost(1, main_object, getString(R.string.api_getUsersPendingRequestList));

    }
    @Override
    public void acceptRejectClick(PendingRequestList requestList,int value) {
        if (value==1) {
            mListener.requestUserProfile(requestList);
        }else {
            acceptAndDeclineConnection(requestList.getId(), value);
        }
    }

    @Override
    public void requestLogoClick(View view, String imageString) {

        mListener.requestLogoClick(view,imageString);
    }

    private void acceptAndDeclineConnection(int connectionId,int reqStatus) {
        baseRequest = new BaseRequest(getContext());
        baseRequest.setBaseRequestListner(new RequestReciever() {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onSuccess(int requestCode, String Json, Object object) {
                try {

                    Gson gson = new Gson();
                    JSONObject jsonObject = new JSONObject(Json);
                    String message=jsonObject.getString("message");
                    utility.showToast(getContext(),message);
                    requestAdapter.notifyDataSetChanged();
                    connList.clear();
                    requestAdapter.clear();
                    getPendingRequest();
                    JSONObject response= (JSONObject) jsonObject.get("data");
                    int  connectionStatus=response.getInt("connectionStatus");
                    try {
                        remaningDays=response.getInt("remainingDays");
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                    try{
                       int connectionId=response.getInt("connectionId");
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                    // JSONArray jsonArray = new JSONArray(object.toString());
                    //localeList = baseRequest.getDataList(jsonArray, LocaleList.class);
                    /*connectionStatus :- 1 for pending, 2 for accepted, 3 for decline, 4 for unfriend, 5 for blocked*/

                   // handelStatus(connectionStatus,remaingDays,connectionId);

                }catch (Exception e){
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int requestCode, String errorCode, String message) {
               // utility.showToast(getContext(),message);
            }

            @Override
            public void onNetworkFailure(int requestCode, String message) {
                utility.showToast(getContext(),message);
            }
        });

        //-requestStatus:- 2 for accept, 3 for reject, 4 for unfriend and 6 for cancel pending request
        JsonObject main_object = new JsonObject();
        main_object.addProperty("connectionId",connectionId );
        main_object.addProperty("requestStatus", reqStatus);

        baseRequest.callAPIPostWOLoader(1, main_object, getString(R.string.api_AcceptOrDeclineUserReq));

    }



    public abstract class PaginationScrollList extends RecyclerView.OnScrollListener {

        private LinearLayoutManager layoutManager;

        public PaginationScrollList(LinearLayoutManager layoutManager) {
            this.layoutManager = layoutManager;
        }

        @Override
        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
            super.onScrolled(recyclerView, dx, dy);

            int visibleItemCount = layoutManager.getChildCount();
            int totalItemCount = layoutManager.getItemCount();
            int firstVisibleItemPosition = layoutManager.findFirstVisibleItemPosition();

            if (!isLoading() && !isLastPage()) {
                if ((visibleItemCount + firstVisibleItemPosition) >= totalItemCount
                        && firstVisibleItemPosition >= 0) {
                    loadMoreItems();
                }
            }
        }

        protected abstract void loadMoreItems();

        public abstract boolean isLastPage();

        public abstract boolean isLoading();

    }
}
