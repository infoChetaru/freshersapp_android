package com.chetaru.FreshersApp.view.ui.Fragment;

import android.content.Context;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.widget.PopupMenu;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;




import android.os.LocaleList;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.chetaru.FreshersApp.R;
import com.chetaru.FreshersApp.service.model.ChatListResponse;
import com.chetaru.FreshersApp.service.model.ConnListResponse;
import com.chetaru.FreshersApp.service.model.MyProfileResponse;
import com.chetaru.FreshersApp.service.model.UserViewPost;
import com.chetaru.FreshersApp.service.retrofit.BaseRequest;

import com.chetaru.FreshersApp.service.retrofit.RequestReciever;
import com.chetaru.FreshersApp.utility.Constant;
import com.chetaru.FreshersApp.utility.SessionParam;
import com.chetaru.FreshersApp.utility.Utility;
import com.chetaru.FreshersApp.utility.Validations;
import com.chetaru.FreshersApp.view.adapter.MessageListAdapter;
import com.chetaru.FreshersApp.view.callback.ConnectionListCallback;
import com.chetaru.FreshersApp.view.ui.Activity.ChatActivity;
import com.google.gson.Gson;
import com.google.gson.JsonObject;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Optional;
import butterknife.Unbinder;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link ConnectFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ConnectFragment extends Fragment implements View.OnClickListener,MessageListAdapter.chatClickListener{
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private static final String VIEWPOST = "viewPost";

    // TODO: Rename and change types of parameters
    private int personId;
    private String mParam2;
    int connectionStatus;
    Context mContext;


    private Unbinder unbinder;
    BaseRequest baseRequest;
    Utility utility;
    SessionParam sessionParam;
    //assign a layout id class

    //LinearLayout initialize
    /*conn_status1_layout
            conn_status8_layout
    connection_status7_layout
            show_opening_msg_layout
    response_layout*/


    @BindView(R.id.date_text)
    TextView showMsgDateTxt;
    @BindView(R.id.user_left_message_txt)
    TextView leftMsgTxt;
    @BindView(R.id.user_left_date_txt)
    TextView leftDateTxt;

    //call menu option for block user
    @BindView(R.id.chat_menu_image)
    ImageView menuImage;
    @BindView(R.id.conn_status1_layout)
    LinearLayout status1Layout;
    @BindView(R.id.conn_status8_layout)
    RelativeLayout status8Layout;
    @BindView(R.id.connection_status7_layout)
    LinearLayout status7Layout;
    //layout for show editText and button
    @BindView(R.id.show_opening_msg_layout)
    LinearLayout openingLayout;
    //connection message response handel with layout
    @BindView(R.id.disconnect_layout)
    LinearLayout disconnectLayout;
    @BindView(R.id.button_msg_layout)
    LinearLayout buttonLayout;
    @BindView(R.id.connection_status1_text)
            TextView status1TextView;
    @BindView(R.id.connection_status8_text)
    TextView status8TextView;
    @BindView(R.id.connection_status7_text)
    TextView connectionStatus7TextView;
    @BindView(R.id.disconnect_msg_text)
    TextView disconnectMsg;
    @BindView(R.id.connection_msg_name)
    TextView ConnectionDefaultMsg;

    @BindView(R.id.cancel_connect_button)
     Button cancelButton;
    @BindView(R.id.accept_connect_button)
     Button acceptButton;
    @BindView(R.id.reject_connect_button)
    Button rejectButton;
    @BindView(R.id.connect_now_button)
    Button connectButton;
    @BindView(R.id.disconnect_connection_button)
    Button disConnectButton;
    int reqStatus;
    //edit text msg
    @BindView(R.id.connect_message)
    EditText connectMessage;

    int userId=0;
    int remaingDays=0;
    int connectionId=0;
    int blockedId=0;
    int type=1;
    boolean blockUser=true;
    Menu menuOpts=null;

    UserViewPost viewPost;
    //Call back
    ConnectionListCallback mListener;
    MyProfileResponse  myProfile;
    ConnListResponse connList;

    List<ChatListResponse> chatList;

    private static final int PAGE_START = 1;
    private boolean isLoading = false;
    private int currentPage = PAGE_START;
    private  int TOTAL_PAGES = 1;
    private boolean isLastPage = false;

    String createDate="",dateTime="",msgDateShow="";
    public ConnectFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment ConnectFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static ConnectFragment newInstance(UserViewPost viewPost,int param1, String param2) {
        ConnectFragment fragment = new ConnectFragment();
        Bundle args = new Bundle();
        args.putSerializable(VIEWPOST,viewPost);
        args.putInt(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {

            personId = getArguments().getInt(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
            viewPost= (UserViewPost) getArguments().getSerializable(VIEWPOST);

        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_connect, container, false);
        unbinder = ButterKnife.bind(this, view);
        myProfile=new MyProfileResponse();
        mContext=getContext();
        chatList=new ArrayList<>();
        connList=new ConnListResponse();
        if (personId>0)
        getUserProfile(personId);
        utility=new Utility();
        sessionParam=new SessionParam(getContext());
        this.mListener= (ConnectionListCallback) getContext();
        connectButton.setOnClickListener(this);
        menuImage.setOnClickListener(this);
        userId=sessionParam.id;
        //chat msg show
        Collections.reverse(chatList);



        /*LinearLayout status1Layout;
        LinearLayout status8Layout;
        LinearLayout status7Layout;
        //layout for show editText and button
        LinearLayout openingLayout;
        //connection message response handel with layout
        LinearLayout connLayout;
        LinearLayout buttonLayout;*/

        status1Layout.setVisibility(View.GONE);
        status7Layout.setVisibility(View.GONE);
        status8Layout.setVisibility(View.GONE);
        openingLayout.setVisibility(View.GONE);
        disconnectLayout.setVisibility(View.GONE);



        getConnectionStatus();
        return view;
    }

    private void getUserProfile(int personId) {
        baseRequest = new BaseRequest(getContext());
        baseRequest.setBaseRequestListner(new RequestReciever() {
            @Override
            public void onSuccess(int requestCode, String Json, Object object) {
                try {

                    Gson gson = new Gson();
                    JSONObject jsonObject = new JSONObject(Json);
                    JSONObject response= (JSONObject) jsonObject.get("data");

                    myProfile = gson.fromJson(response.toString(), MyProfileResponse.class);

                   // connectionList.setConnectionId(myProfile.getConnectionStatus());


                }catch (Exception e){
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int requestCode, String errorCode, String message) {
                // utility.showToast(getContext(),message);
            }

            @Override
            public void onNetworkFailure(int requestCode, String message) {
                utility.showToast(getContext(),message);
            }
        });

        JsonObject main_object = new JsonObject();
        main_object.addProperty("userId", personId);

        /*JsonObject object = Functions.getClient().getJsonMapObject(
                "personId",personId);*/
        baseRequest.callAPIPostWOLoader(1, main_object, getString(R.string.api_getUserProfile));


    }

    @Override public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Optional
    @OnClick({R.id.connect_now_button,R.id.disconnect_connection_button,R.id.cancel_connect_button,
            R.id.accept_connect_button,R.id.reject_connect_button,R.id.chat_menu_image})
    @Override
    public void onClick(View view) {
        switch (view.getId()){
            /**
             * Connect now click - sendConnectionRequestToUser api
             * Cancel connection click - acceptOrDeclineUserRequest api with type = 6
             * Disconnect click - acceptOrDeclineUserRequest api with type = 4
             * Accept click - acceptOrDeclineUserRequest api with type = 2
             * Accept click -
             * Reject click - acceptOrDeclineUserRequest api with type = 3
             */
            //-requestStatus:- 2 for accept, 3 for reject, 4 for unfriend and 6 for cancel pending request

            case R.id.connect_now_button:
                //sendConnectionRequestToUser
                String message=connectMessage.getText().toString().trim();
                if (!Validations.isEmptyString(message)){
                    getConnect(message);
                }else{
                    utility.showToast(getContext(),"Please enter opening message");
                }
                break;
            case R.id.disconnect_connection_button:
                reqStatus=4;
                //change disconnect button to open chat option
                //acceptAndDeclineConnection(reqStatus);
                if (connectionId>0) {
                    ConnListResponse connectionList = new ConnListResponse();
                    connectionList.setConnectionId(connectionId);
                    connectionList.setUserId(myProfile.getId());
                    //connectionList.setCreatedAt();
                    connectionList.setUserName(myProfile.getName());
                    connectionList.setUserImageUrl(myProfile.getProfileImage());
               /* bundle.putInt("connectionId",connData.getConnectionId());
                bundle.putInt("otherUserId",connData.getUserId());
                bundle.putString("date",connData.getCreatedAt());
                bundle.putString("userName",connData.getUserName());
                bundle.putString("otherUserProfile",connData.getUserImageUrl());
                bundle.putString("connectionStatus",connData.getUserName());*/

                     mListener.ConnectionChatOpen(connectionList);
                }
                break;
            case R.id.cancel_connect_button:
                reqStatus=6;
                acceptAndDeclineConnection(reqStatus);
                break;
            case R.id.accept_connect_button:
                setAcceptBackground();
                reqStatus=2;
                acceptAndDeclineConnection(reqStatus);
                break;
            case R.id.reject_connect_button:
                setRejectBackground();
                reqStatus=3;
                acceptAndDeclineConnection(reqStatus);
                break;
            case R.id.chat_menu_image:

                showPopup(menuImage);

                break;



        }
    }

    @Override
    public void onPause() {
        super.onPause();

    }

    private void getConnectionStatus() {

        baseRequest = new BaseRequest(getContext());
        baseRequest.setBaseRequestListner(new RequestReciever() {
            @Override
            public void onSuccess(int requestCode, String Json, Object object) {
                try {

                    Gson gson = new Gson();
                    JSONObject jsonObject = new JSONObject(Json);
                    JSONObject response= (JSONObject) jsonObject.get("data");
                    try {
                        connectionStatus  =response.getInt("connectionStatus");
                    }catch (Exception e){
                        e.printStackTrace();
                    }

                    try {
                        remaingDays=response.getInt("remainingDays");
                    }catch (Exception e){
                        e.printStackTrace();
                    }

                    try {
                        blockedId=response.getInt("blockedUserId");

                    }catch (Exception e){
                        e.printStackTrace();
                    }
                    try{
                        connectionId=response.getInt("connectionId");
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                   // JSONArray jsonArray = new JSONArray(object.toString());
                    //localeList = baseRequest.getDataList(jsonArray, LocaleList.class);
                    /*connectionStatus :- 1 for pending, 2 for accepted, 3 for decline, 4 for unfriend, 5 for blocked*/
                    apiGetChat();
                   handelStatus(connectionStatus,remaingDays,connectionId,blockedId);


                }catch (Exception e){
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int requestCode, String errorCode, String message) {
                //utility.showToast(getBaseContext(),message);
            }

            @Override
            public void onNetworkFailure(int requestCode, String message) {
                utility.showToast(getContext(),message);
            }
        });



        JsonObject main_object = new JsonObject();
        main_object.addProperty("userId",personId );
        baseRequest.callAPIPostWOLoader(1, main_object, getString(R.string.api_ConnectionStatus));

    }

    public void apiGetChat() {
        currentPage=PAGE_START;
        if (chatList.size()>0)
            chatList.clear();
        baseRequest = new BaseRequest(mContext);
        baseRequest.setBaseRequestListner(new RequestReciever() {
            @Override
            public void onSuccess(int requestCode, String Json, Object object) {
                try {
                    /*JSONObject jsonObject = new JSONObject(object.toString());
                    JSONArray jsonArray;
                    jsonArray = (JSONArray) jsonObject.getJSONArray("messages");*/
                    JSONObject jsonObject=new JSONObject(Json);
                    JSONArray userJsonArray = jsonObject.getJSONArray("data");
                    chatList = baseRequest.getDataList(userJsonArray, ChatListResponse.class);
                    //TOTAL_PAGES=jsonObject.getInt("totalPageCount");
                    Collections.reverse(chatList);


                    try {
                        if (chatList.size() > 0) {
                            msgDateShow=chatList.get(0).getMsgDate();
                            String newDate = Utility.formatTodayYesterday(chatList.get(0).getMsgDate());
                            showMsgDateTxt.setText(newDate);
                            leftMsgTxt.setText(chatList.get(0).getMessage());
                            String dateChange = Utility.formatToShowOnlyTime(chatList.get(0).getMsgDate());
                            leftDateTxt.setText(dateChange);
                        }
                    }catch (Exception e){
                        e.printStackTrace();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int requestCode, String errorCode, String message) {
                Log.d("msg",message);
               // utility.showToast(mContext, message);
            }

            @Override
            public void onNetworkFailure(int requestCode, String message) {
                //errorLayout.showError(message);
                utility.showToast(mContext, message);
            }
        });

        JsonObject main_object = new JsonObject();
        main_object.addProperty("connectionId",connectionId+"" );
        main_object.addProperty("page", currentPage);
        main_object.addProperty("date", createDate);
         main_object.addProperty("connectionStatus", connectionStatus);
        baseRequest.callAPIPost(1, main_object, getString(R.string.api_getUsersChat));

    }

    private void handelStatus(int connectionStatus,int remaingDays, int connectionId,final int blockedUserId) {
        if(blockedUserId>0&&menuOpts!=null){
            menuOpts.getItem(1).setTitle("Un Block");
        }
        switch (connectionStatus){
            //0,4,6
            case Constant.kCnaSend:
            case Constant.kUnFriend:
            case Constant.kCancel:
                ConnectionDefaultMsg.setVisibility(View.VISIBLE);
                openingLayout.setVisibility(View.VISIBLE);
                disconnectLayout.setVisibility(View.GONE);
                status1Layout.setVisibility(View.GONE);
                status7Layout.setVisibility(View.GONE);
                status8Layout.setVisibility(View.GONE);
                break;
            //1
            case Constant.kPending:
                status1Layout.setVisibility(View.VISIBLE);
                status1TextView.setText("Connection sent.");
                setCancelButton("Cancel Connection");
                cancelButton.setText("Cancel Connection");

                openingLayout.setVisibility(View.GONE);
                disconnectLayout.setVisibility(View.GONE);
                status7Layout.setVisibility(View.GONE);
                status8Layout.setVisibility(View.GONE);
                //-requestStatus:- 2 for accept, 3 for reject, 4 for unfriend and 6 for cancel pending request

                break;
            //2
            case Constant.kAccepted:
                disconnectLayout.setVisibility(View.VISIBLE);
                disconnectMsg.setText("You are Connected.");
                setDisConnectButton("Disconnect");
                disConnectButton.setText("Return To Conversation");
                //cancelButton.setTextColor(ContextCompat.getColor(getContext(),R.color.colorPrimary));
                //reqStatus=2;
                openingLayout.setVisibility(View.GONE);
                status1Layout.setVisibility(View.GONE);
                status7Layout.setVisibility(View.GONE);
                status8Layout.setVisibility(View.GONE);
                break;
            //3
            case Constant.kDecline:
                status7Layout.setVisibility(View.VISIBLE);
                connectionStatus7TextView.setText("Your connection request is rejected. You can reconnect after "+remaingDays+" days.");
                openingLayout.setVisibility(View.GONE);
                status1Layout.setVisibility(View.GONE);
                status8Layout.setVisibility(View.GONE);
                disconnectLayout.setVisibility(View.GONE);
                break;
            //5
            case Constant.kBlocked:
                status7Layout.setVisibility(View.VISIBLE);
                //get block user id
                if (blockedUserId!=(userId)){
                    connectionStatus7TextView.setText("User Blocked.");
                }else {
                    connectionStatus7TextView.setText("You are blocked by the user.");
                }
                openingLayout.setVisibility(View.GONE);
                status1Layout.setVisibility(View.GONE);
                status8Layout.setVisibility(View.GONE);
                disconnectLayout.setVisibility(View.GONE);
                break;
            //7
            case Constant.kStatus7:
                status7Layout.setVisibility(View.VISIBLE);
                ConnectionDefaultMsg.setVisibility(View.GONE);
                openingLayout.setVisibility(View.VISIBLE);
                connectionStatus7TextView.setText("You rejected user's request previously. Do you want to reconnect?");
                status1Layout.setVisibility(View.GONE);
                status8Layout.setVisibility(View.GONE);
                disconnectLayout.setVisibility(View.GONE);
                break;
            case Constant.kStatus8:
                status8Layout.setVisibility(View.VISIBLE);
                status8TextView.setText("Connection Request Received");
                acceptButton.setText("Accept");
                rejectButton.setText("Reject");
                setAcceptBackground();
                // setRejectBackground();
                openingLayout.setVisibility(View.GONE);
                status1Layout.setVisibility(View.GONE);
                status7Layout.setVisibility(View.GONE);
                disconnectLayout.setVisibility(View.GONE);
                break;
            case Constant.kStatus9:
                break;

        }
    }

    private void setDisConnectButton(String disconnect) {
        final int sdk = android.os.Build.VERSION.SDK_INT;
        if(sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
            cancelButton.setBackgroundDrawable(ContextCompat.getDrawable(getContext(), R.drawable.button_white_color_border) );
            cancelButton.setTextColor(ContextCompat.getColor(getContext(),R.color.colorPrimary));
            cancelButton.setText(disconnect);
        } else {
            acceptButton.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.button_white_color_border));
            cancelButton.setTextColor(ContextCompat.getColor(getContext(),R.color.colorPrimary));
            cancelButton.setText(disconnect);
        }
    }

    private void setCancelButton(String textValue) {
        final int sdk = android.os.Build.VERSION.SDK_INT;
        if(sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
            cancelButton.setBackgroundDrawable(ContextCompat.getDrawable(getContext(), R.drawable.button_white_color_border) );
            cancelButton.setTextColor(ContextCompat.getColor(getContext(),R.color.colorPrimary));
            cancelButton.setText(textValue);
        } else {
            acceptButton.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.button_white_color_border));
            cancelButton.setTextColor(ContextCompat.getColor(getContext(),R.color.colorPrimary));
            cancelButton.setText(textValue);
        }
    }

    private void setAcceptBackground() {
        final int sdk = android.os.Build.VERSION.SDK_INT;
        if(sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
            rejectButton.setBackgroundDrawable(ContextCompat.getDrawable(getContext(), R.drawable.button_background_primary) );
            acceptButton.setBackgroundDrawable(ContextCompat.getDrawable(getContext(), R.drawable.button_white_color_border) );
            rejectButton.setTextColor(ContextCompat.getColor(getContext(),R.color.white));
            acceptButton.setTextColor(ContextCompat.getColor(getContext(),R.color.colorPrimary));
        } else {
            rejectButton.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.button_background_primary));
            acceptButton.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.button_white_color_border));
            rejectButton.setTextColor(ContextCompat.getColor(getContext(),R.color.white));
            acceptButton.setTextColor(ContextCompat.getColor(getContext(),R.color.colorPrimary));
        }
    }
    private void setRejectBackground(){
        final int sdk = android.os.Build.VERSION.SDK_INT;
        if(sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
            acceptButton.setBackgroundDrawable(ContextCompat.getDrawable(getContext(), R.drawable.button_background_primary) );
            rejectButton.setBackgroundDrawable(ContextCompat.getDrawable(getContext(), R.drawable.button_white_color_border) );
            acceptButton.setTextColor(ContextCompat.getColor(getContext(),R.color.white));
            rejectButton.setTextColor(ContextCompat.getColor(getContext(),R.color.colorPrimary));
        } else {
            acceptButton.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.button_background_primary));
            rejectButton.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.button_white_color_border));
            acceptButton.setTextColor(ContextCompat.getColor(getContext(),R.color.white));
            rejectButton.setTextColor(ContextCompat.getColor(getContext(),R.color.colorPrimary));
        }

    }

    /*
    * et kCanSend = "0"
let kPending = "1"
let kAccepted = "2"
let kDecline = "3"
let kUnfriend = "4"
let kBlocked = "5"
let kCancel = "6"*/

    private void getConnect(String message) {
        baseRequest = new BaseRequest(getContext());
        baseRequest.setBaseRequestListner(new RequestReciever() {
            @Override
            public void onSuccess(int requestCode, String Json, Object object) {
                try {

                    openingLayout.setVisibility(View.GONE);
                    disconnectLayout.setVisibility(View.VISIBLE);
                    Gson gson = new Gson();
                    JSONObject jsonObject = new JSONObject(object.toString());
                    String message=jsonObject.getString("message");
                    int connectionStatus=jsonObject.getInt("connection_status");
                    try {
                        remaingDays = jsonObject.getInt("remainingDays");
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                    try{
                        connectionId=jsonObject.getInt("connectionId");
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                    try {
                        blockedId=jsonObject.getInt("blockedUserId");

                    }catch (Exception e){
                        e.printStackTrace();
                    }
                    handelStatus(connectionStatus,remaingDays,connectionId,blockedId);
                   /* switch (connectionStatus){
                        case Constant.kCnaSend:
                        case Constant.kUnFriend:
                            disConnectButton.setText("Connect Now");
                            disconnectMsg.setText("Send an opening message:");
                        break;
                        case Constant.kPending:
                            disConnectButton.setText("Cancel Connection");
                            disconnectMsg.setText("Connection already sent");
                            break;
                        case Constant.kAccepted:
                            disConnectButton.setText("Disconnect");
                            disconnectMsg.setText("You are connected");
                        break;
                        case Constant.kDecline:
                            buttonLayout.setVisibility(View.GONE);
                            disconnectMsg.setText("Your connection was rejected. You can reconnect after"+ strDaysRemaingToResend+"days");

                            break;
                        case Constant.kBlocked:
                            buttonLayout.setVisibility(View.GONE);
                            disconnectMsg.setText("your are blocked by the user.");
                            break;


                    }
                    utility.showToast(getContext(),message);*/



                }catch (Exception e){
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int requestCode, String errorCode, String message) {
                //utility.showToast(getContext(),message);
            }

            @Override
            public void onNetworkFailure(int requestCode, String message) {

                utility.showToast(getContext(),message);
            }
        });


        JsonObject main_object = new JsonObject();
        main_object.addProperty("toUserId",personId );
        main_object.addProperty("openingMessage", message);

        baseRequest.callAPIPost(1, main_object, getString(R.string.api_sendConnectionRequest));
    }

    private void acceptAndDeclineConnection(int reqStatus) {
        baseRequest = new BaseRequest(getContext());
        baseRequest.setBaseRequestListner(new RequestReciever() {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onSuccess(int requestCode, String Json, Object object) {
                try {

                    Gson gson = new Gson();
                    JSONObject jsonObject = new JSONObject(Json);
                    String message=jsonObject.getString("message");
                    utility.showToast(getContext(),message);
                    JSONObject response= (JSONObject) jsonObject.get("data");
                    connectionStatus=response.getInt("connectionStatus");
                    try {
                        remaingDays=response.getInt("remainingDays");
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                    try{
                        connectionId=response.getInt("connectionId");
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                    try {
                        blockedId=response.getInt("blockedUserId");

                    }catch (Exception e){
                        e.printStackTrace();
                    }
                    // JSONArray jsonArray = new JSONArray(object.toString());
                    //localeList = baseRequest.getDataList(jsonArray, LocaleList.class);
                    /*connectionStatus :- 1 for pending, 2 for accepted, 3 for decline, 4 for unfriend, 5 for blocked*/

                    handelStatus(connectionStatus,remaingDays,connectionId,blockedId);

                }catch (Exception e){
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int requestCode, String errorCode, String message) {
                //utility.showToast(getBaseContext(),message);
            }

            @Override
            public void onNetworkFailure(int requestCode, String message) {

                utility.showToast(getContext(),message);
            }
        });

        //-requestStatus:- 2 for accept, 3 for reject, 4 for unfriend and 6 for cancel pending request
        JsonObject main_object = new JsonObject();
        main_object.addProperty("connectionId",connectionId );
        main_object.addProperty("requestStatus", reqStatus);

        baseRequest.callAPIPostWOLoader(1, main_object, getString(R.string.api_AcceptOrDeclineUserReq));

    }

    @Override
    public void onUserImageView(View view, String imageString) {
      //  getImageFullView(view,imageString);
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.setting_menu, menu);
        this.menuOpts=menu;
    }

    public void showPopup(View v) {
        PopupMenu popup = new PopupMenu(getContext(), v, Gravity.RIGHT|Gravity.CENTER);
        MenuInflater inflater = popup.getMenuInflater();
        inflater.inflate(R.menu.setting_menu, popup.getMenu());
         menuOpts=popup.getMenu();
         if (connectionStatus==2) {
             menuOpts.getItem(0).setVisible(true);
         }else {
             menuOpts.getItem(0).setVisible(false);
         }
        if (blockedId>0){
            blockUser=false;
            menuOpts.getItem(1).setTitle("Unblock");
        }else if (blockUser){
            menuOpts.getItem(1).setTitle("Block");
        }else {
            menuOpts.getItem(1).setTitle("Unblock");
        }
        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.nav_disconnect:
                       disconnectMenu();
                        return true;
                    case R.id.nav_block:

                        blockMenu(menuOpts);

                        return true;
                    default:
                        return false;
                }
            }
        });
        popup.show();
    }


    private void disconnectMenu() {
        baseRequest = new BaseRequest(getContext());
        baseRequest.setBaseRequestListner(new RequestReciever() {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onSuccess(int requestCode, String Json, Object object) {
                try {

                    Gson gson = new Gson();
                    JSONObject jsonObject = new JSONObject(Json);
                    String message=jsonObject.getString("message");
                    //utility.showToast(ChatActivity.this,message);
                   // chatCardSendMsg.setVisibility(View.GONE);
                    //menuImage.setVisibility(View.GONE);
                    JSONObject response= (JSONObject) jsonObject.get("data");
                    try {
                          connectionStatus=response.getInt("connectionStatus");
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                    try {
                            remaingDays=response.getInt("remainingDays");
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                    try{
                        int    newConnectionId=response.getInt("connectionId");
                    }catch (Exception e){
                        e.printStackTrace();
                    }

                    // JSONArray jsonArray = new JSONArray(object.toString());
                    //localeList = baseRequest.getDataList(jsonArray, LocaleList.class);
                    /*connectionStatus :- 1 for pending, 2 for accepted, 3 for decline, 4 for unfriend, 5 for blocked*/

                    handelStatus(connectionStatus,remaingDays,connectionId,blockedId);

                }catch (Exception e){
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int requestCode, String errorCode, String message) {
                //utility.showToast(getBaseContext(),message);
            }

            @Override
            public void onNetworkFailure(int requestCode, String message) {

                utility.showToast(getContext(),message);
            }
        });

        //-requestStatus:- 2 for accept, 3 for reject, 4 for unfriend and 6 for cancel pending request
        JsonObject main_object = new JsonObject();
        main_object.addProperty("connectionId",connectionId );
        main_object.addProperty("requestStatus", "4");



        baseRequest.callAPIPostWOLoader(1, main_object, getString(R.string.api_AcceptOrDeclineUserReq));


    }
    private void blockMenu(Menu menuOpts) {

        baseRequest = new BaseRequest(mContext);
        baseRequest.setBaseRequestListner(new RequestReciever() {
            @Override
            public void onSuccess(int requestCode, String Json, Object object) {
                try {

                    JSONObject jsonObject = new JSONObject(Json);
                    String message=jsonObject.getString("message");
                   // {"code":200,"status":true,"service_name":"block-user","message":"User blocked.","data":""}
                    if (message.equals("User blocked.")){
                        menuOpts.getItem(1).setTitle("Block");
                    }else {
                        menuOpts.getItem(1).setTitle("Un Block");
                    }

                    // utility.showToast(ChatActivity.this,message);

                    //chatCardSendMsg.setVisibility(View.GONE);
                    //menuImage.setVisibility(View.GONE);
                    getConnectionStatus();
                }catch (Exception e){
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int requestCode, String errorCode, String message) {
                // utility.showToast(getContext(),message);
            }

            @Override
            public void onNetworkFailure(int requestCode, String message) {

                utility.showToast(getContext(),message);
            }
        });

       /* {
            "blockUserId":"6",
                "type":1
        }
        "type" - 1 for block , 2 for unblock*/
       if (blockUser){
           blockUser=false;
           type=1;
       }else {
           blockUser=true;
           type=2;
       }

        JsonObject main_object = new JsonObject();
        main_object.addProperty("blockUserId",personId);
        main_object.addProperty("type", type);
       /* JsonObject object = Functions.getClient().getJsonMapObject(
                "postId",
                "likeStatus",status );*/
        baseRequest.callAPIPostWOLoader(1, main_object, getString(R.string.api_blockUser));
    }


}
