package com.chetaru.FreshersApp.view.ui;

import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;

import android.widget.LinearLayout;
import android.widget.TextView;

import com.chetaru.FreshersApp.R;

import com.chetaru.FreshersApp.service.repository.factory.VerifyModelFactory;
import com.chetaru.FreshersApp.utility.BaseActivity;
import com.chetaru.FreshersApp.utility.MarshMallowPermission;
import com.chetaru.FreshersApp.utility.RecyclerItemClickListener;
import com.chetaru.FreshersApp.utility.SessionParam;
import com.chetaru.FreshersApp.utility.Utility;
import com.chetaru.FreshersApp.utility.Validations;
import com.chetaru.FreshersApp.view.adapter.CountryListDialog;

import com.chetaru.FreshersApp.viewModel.CountryList;
import com.chetaru.FreshersApp.viewModel.CountryResponse;


import com.chetaru.FreshersApp.viewModel.VerifyViewModel;


import org.json.JSONObject;


import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Optional;
import im.delight.android.location.SimpleLocation;




public class Verification_Activity extends BaseActivity implements View.OnClickListener{

    VerifyViewModel viewModel;
    @Nullable
    @BindView(R.id.ver_email_et)
    EditText VerEmailIdEdit;
    @Nullable
    @BindView(R.id.var_mobile_et)
    EditText verMobileEdit;
    @Nullable
    @BindView(R.id.ver_verification_code_et)
    EditText verificationCodeEdit;
    @Nullable
    @BindView(R.id.ver_password_et)
    EditText verPasswordEdit;
    @Nullable
    @BindView(R.id.ver_confrim_Password_et)
    EditText verCofrimPasswordEdit;
    @Nullable
    @BindView(R.id.country_code_tv)
    TextView countryCodeTextView;

    @Nullable
    @BindView(R.id.submit_email_veri_btn)
    Button subEmailButton;
    @Nullable
    @BindView(R.id.submit_veri_code_btn)
    Button subVerCodeButton;
    @Nullable
    @BindView(R.id.check_verification_btn)
    Button confrimButton;

    @Nullable
    @BindView(R.id.sign_up_txt)
    TextView createAccountTextView;
    @Nullable
    @BindView(R.id.ver_mobile_layout)
    LinearLayout mobileLayout;

    @Nullable
    @BindView(R.id.ver_extra_txt)
    TextView extraOrText;
    @BindView(R.id.tv_error)
    TextView textView_error;
    @BindView(R.id.user_name_verification_code_txt)
    TextView userNamTtextView;

    @BindView(R.id.progressbar)
    View progressBar;

    private ArrayList<String> permissionsToRequest;
    private ArrayList<String> permissionsRejected = new ArrayList<>();
    private ArrayList<String> permissions = new ArrayList<>();

    private final static int ALL_PERMISSIONS_RESULT = 101;
    final private int REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS = 124;
    MarshMallowPermission marshMallowPermission;
    private SimpleLocation location;

    String countryId="",countryName="United Kingdom";
    //default uk country code
    String phoneCode="44";
    Integer type=0;
    String value="";
    LifecycleOwner lifecycleOwner;
    List<CountryList> countryLists;
    SessionParam sessionParam;
    ProgressDialog loading = null;
    Utility utility;
    private double longitude;
    private double latitude;
    String email1Data="";
    final int sdk = android.os.Build.VERSION.SDK_INT;
    Integer flag=0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_verification);
        ButterKnife.bind(this);
        sessionParam=new SessionParam(this);
        getProgerss(this);
        utility=new Utility();
        marshMallowPermission = new MarshMallowPermission(this);
        countryLists=new ArrayList<>();
        permission();

        // Create a ViewModel the first time the system calls an activity's onCreate() method.
        // Re-created activities receive the same MyViewModel instance created by the first activity.
        viewModel = ViewModelProviders.of(this, new VerifyModelFactory(this.getApplication(), "countryApi","")).get(VerifyViewModel.class);
        try{
            loading.show();
            viewModel.getCountryCode().observe(this, new Observer<CountryResponse>() {
                @Override
                public void onChanged(@Nullable CountryResponse countryRes) {
                    loading.dismiss();
                    if (countryRes!=null) {
                        //CountryResponse  response = gson.fromJson(CountryResponse.toString(), CountryResponse.class);
                        //userModel = gson.fromJson(object.toString(),User.class);
                        countryLists = countryRes.getCountryList();



                    }
                }
            });
            initView();

            //setHasStableIds(true);
        }catch (Exception e){
            e.printStackTrace();
        }


    }

    private void getProgerss(Context mContext) {
        loading = new ProgressDialog(mContext);
        loading.setCancelable(false);
        loading.setMessage("Loading....");
        loading.setProgressStyle(ProgressDialog.STYLE_SPINNER);
    }

    @Optional
    private void initView() {
        VerEmailIdEdit.addTextChangedListener(new MyTextWatcher(VerEmailIdEdit));
        verMobileEdit.addTextChangedListener(new MyTextWatcher(verMobileEdit));
        verificationCodeEdit.addTextChangedListener(new MyTextWatcher(verificationCodeEdit));
        verPasswordEdit.addTextChangedListener(new MyTextWatcher(verPasswordEdit));
        verCofrimPasswordEdit.addTextChangedListener(new MyTextWatcher(verCofrimPasswordEdit));

        subEmailButton.setOnClickListener(this);
        subVerCodeButton.setOnClickListener(this);
        confrimButton.setOnClickListener(this);
        VerEmailIdEdit.setOnClickListener(this);
        verMobileEdit.setOnClickListener(this);
        mobileLayout.setOnClickListener(this);
        countryCodeTextView.setOnClickListener(this);

        extraOrText.setVisibility(View.VISIBLE);
        VerEmailIdEdit.setVisibility(View.VISIBLE);
        subEmailButton.setVisibility(View.VISIBLE);
        mobileLayout.setVisibility(View.VISIBLE);
        getMobileAndEmilVisible();
        //location = new SimpleLocation(this);

        // if we can't access the location yet
        /*if (!location.hasLocationEnabled()) {
            // ask the user to enable location access
            //SimpleLocation.openSettings(getContext());
            ShowSettingAlert();
        }
        location.setListener(new SimpleLocation.Listener() {

            public void onPositionChanged() {
                // new location data has been received and can be accessed
                latitude = location.getLatitude();
                longitude = location.getLongitude();
            }

        });
        latitude = location.getLatitude();
        longitude = location.getLongitude();*/

        // countryName=getCompleteAddressString(latitude,longitude);
         // phoneCode=GetCountryZipCode();

          countryCodeTextView.setText("+ "+phoneCode);
         if (countryLists.size()>0) {
             if (!Validations.isEmptyString(countryName)) {
                 for (int i = 0; i < countryLists.size(); i++) {
                     if (countryName.toLowerCase().equals(countryLists.get(i).getCountryName().toLowerCase())) {
                         countryCodeTextView.setText("+"+countryLists.get(i).getPhonecode());
                         countryId=countryLists.get(i).getId()+"";
                     }
                 }
             }
         }
    }

    public String GetCountryZipCode(){
        String CountryID="";
        String CountryZipCode="";

        TelephonyManager manager = (TelephonyManager) this.getSystemService(Context.TELEPHONY_SERVICE);
        //getNetworkCountryIso
        CountryID= manager.getSimCountryIso().toUpperCase();
        String[] rl=this.getResources().getStringArray(R.array.CountryCodes);
        for(int i=0;i<rl.length;i++){
            String[] g=rl[i].split(",");
            if(g[1].trim().equals(CountryID.trim())){
                CountryZipCode=g[0];
                break;
            }
        }
        return CountryZipCode;
    }

    public void ShowSettingAlert(){
        AlertDialog.Builder builder =new AlertDialog.Builder(this);
        builder.setTitle("Allow FreshersApp to access your location?");
        builder.setMessage("FresherApp would like to use Your current location in order to post feed.");
        builder.setPositiveButton("Setting", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                Intent intent=new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                mContext.startActivity(intent);
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int i) {

                dialog.cancel();
            }
        });
        builder.show();
    }

    @OnClick({R.id.check_verification_btn,R.id.submit_email_veri_btn,R.id.submit_veri_code_btn,
            R.id.ver_email_et,R.id.var_mobile_et,R.id.country_code_tv,R.id.ver_mobile_layout})
    @Override
    public void onClick(View view) {

        switch (view.getId()){
            case R.id.check_verification_btn:
                hideKeyboard();
                String password=verPasswordEdit.getText().toString().trim();
                String confrimPassword=verCofrimPasswordEdit.getText().toString().trim();
                if(Validations.isEmptyString(password)){
                    utility.showToast(Verification_Activity.this, "Please enter Password.");
                return;
                }
                if (Validations.isEmptyString(confrimPassword)){
                    utility.showToast(Verification_Activity.this, "Please enter Confirm Password.");
                return;
                }
                if (password.equals(confrimPassword)){
                    sessionParam.password = password;
                    //step 1
                    extraOrText.setVisibility(View.VISIBLE);
                    VerEmailIdEdit.setVisibility(View.VISIBLE);
                    subEmailButton.setVisibility(View.VISIBLE);
                    mobileLayout.setVisibility(View.VISIBLE);
                    //step 2
                    verificationCodeEdit.setVisibility(View.GONE);
                    subVerCodeButton.setVisibility(View.GONE);
                    //step 3
                    verPasswordEdit.setVisibility(View.GONE);
                    verCofrimPasswordEdit.setVisibility(View.GONE);
                    confrimButton.setVisibility(View.GONE);
                    userNamTtextView.setVisibility(View.GONE);

                    Intent intent=new Intent(this,SignUpActivity.class);
                    //Create the bundle
                    Bundle b = new Bundle();
                        //Add your data to bundle

                    b.putString("userName",email1Data );
                    b.putString("userValue",value );
                    b.putString("countryId",countryId );
                    b.putString("password",password );
                    b.putInt("type",type );
                    //b.putString("deviceId",deviceId );
                    intent.putExtras(b);
                    startActivity(intent);
                    finish();
                }else {
                    utility.showToast(Verification_Activity.this, "Password and Confirm Password did not match.");
                }
                break;
            case R.id.submit_email_veri_btn:
                hideKeyboard();
                String mobile=verMobileEdit.getText().toString().trim();
                String email=VerEmailIdEdit.getText().toString().trim();
                if(flag==0){
                    utility.showToast(Verification_Activity.this, "Please enter Phone Number/Email.");
                    return;
                }
                if(flag==1){
                    if (Validations.isEmptyString(email)) {
                        if (Validations.isEmptyString(mobile))
                        utility.showToast(Verification_Activity.this, "Please enter Email.");
                    return;
                    }
                }
                if (flag==2)
                {
                    if (Validations.isEmptyString(countryId)){
                        if (countryLists.size()>0) {
                            if (!Validations.isEmptyString(countryName)) {
                                for (int i = 0; i < countryLists.size(); i++) {
                                    if (countryName.toLowerCase().equals(countryLists.get(i).getCountryName().toLowerCase())) {
                                        assert countryCodeTextView != null;
                                        countryCodeTextView.setText("+"+countryLists.get(i).getPhonecode().toString());
                                        countryId=countryLists.get(i).getId()+"";
                                    }
                                }
                            }
                        }
                    }
                    if (Validations.isEmptyString(mobile)) {
                        if (Validations.isEmptyString(email))
                        utility.showToast(Verification_Activity.this, "Please enter Phone Number");
                    return;
                    }
                }



                if (!Validations.isEmptyString(mobile)) {
                    if (Validations.isValidPhone(mobile)) {
                        if (!Validations.isEmptyString(email)) {
                            if (Validations.isValidEmail(email)) {
                                verMobileEdit.setText("");
                            } else {
                                VerEmailIdEdit.setText("");
                            }
                        }
                    } else {
                        verMobileEdit.setText("");
                    }
                }else if (!Validations.isEmptyString(email)){

                        if (Validations.isValidEmail(email)) {
                            if (!Validations.isEmptyString(mobile)) {
                                if (Validations.isValidPhone(mobile)) {

                                    VerEmailIdEdit.setText("");
                                } else {
                                    verMobileEdit.setText("");
                                }
                            }
                        } else {
                            VerEmailIdEdit.setText("");
                        }
                }/*else {
                    utility.showToast(Verification_Activity.this,"Please enter Email.");
                    return;
                }*/



                if (!Validations.isEmptyString(email)){
                    if (!Validations.isValidEmail(email)){
                        utility.showToast(Verification_Activity.this, "Please enter valid Email.");
                        return;
                    }else {
                        type = 1;
                        value = email;
                        countryId = "";
                        sessionParam.signupType = type;
                    }
                }

                if (!Validations.isEmptyString(mobile)){
                   /* if (!Validations.isValidPhone(mobile)){
                        utility.showToast(Verification_Activity.this, "Please enter valid Phone Number.");
                        return;
                    }else*/


                   /* if (mobile.substring(0, 1).contains("0")) {
                        mobile= mobile.substring(1, 11);

                        String firstValue=mobile;

                    }*/

                    if (mobile.startsWith("0")) {
                        mobile=mobile.substring(1,mobile.length());
                    }

                        type = 2;
                        value = mobile;
                        sessionParam.signupType = type;
                        if (Validations.isEmptyString(countryId)){
                            utility.showToast(Verification_Activity.this," Please select valid country code.");
                            return;
                        }

                }
                loading.show();
                //viewModel.fetchVeri(type,value,countryId);
                viewModel.getCode(type,value,countryId).observe(this, new Observer<JSONObject>() {
                    @Override
                    public void onChanged(JSONObject repos) {

                        loading.dismiss();
                        //{"code":200,"status":true,"service_name":"verify-username","message":"Verification code send to email.",
                        // "data":{"randomNumber":2125,"email":"ritesh.sonar111@gmail.com"}}
                           try {
                               Log.d("codeJson",repos.toString());
                               JSONObject jsonObject = (JSONObject) repos;
                               String message = jsonObject.optString("message");
                               Boolean status = jsonObject.optBoolean("status");

                               if (status) {
                                   utility.showToast(Verification_Activity.this, message);
                                   //Toast.makeText(Verification_Activity.this, message, Toast.LENGTH_SHORT).show();
                                   JSONObject object = new JSONObject();
                                   object = repos.getJSONObject("data");
                                   Integer code = object.getInt("randomNumber");


                                   try {
                                       email1Data = object.getString("email");

                                       if (!Validations.isEmptyString(email1Data)) {
                                           sessionParam.userName = email1Data;
                                       }
                                   } catch (Exception e) {
                                       e.printStackTrace();
                                   }
                                   try {
                                       email1Data = object.getString("phoneNumber");

                                       if (!Validations.isEmptyString(email1Data)) {
                                           sessionParam.userName = email1Data;
                                       }
                                   } catch (Exception e) {
                                       e.printStackTrace();
                                   }

                                   sessionParam.verificationCode = code;


                                   extraOrText.setVisibility(View.GONE);
                                   subEmailButton.setVisibility(View.GONE);
                                   verificationCodeEdit.setVisibility(View.VISIBLE);
                                   subVerCodeButton.setVisibility(View.VISIBLE);
                                   if (!Validations.isEmptyString(email1Data)) {
                                       sessionParam.userName = email1Data;
                                       userNamTtextView.setVisibility(View.VISIBLE);
                                       mobileLayout.setVisibility(View.GONE);
                                       VerEmailIdEdit.setVisibility(View.GONE);
                                       userNamTtextView.setText(email1Data);

                                   } else {
                                       extraOrText.setVisibility(View.VISIBLE);
                                       subEmailButton.setVisibility(View.VISIBLE);
                                       verificationCodeEdit.setVisibility(View.GONE);
                                       subVerCodeButton.setVisibility(View.GONE);
                                   }
                               }else {
                                   utility.showToast(Verification_Activity.this, message);
                               }

                        }catch (Exception e){
                            e.printStackTrace();
                               utility.showToast(Verification_Activity.this,  "Error loading repos");
                        }

                        /*if (repos.getMessage()!=null) {
                            Integer code = repos.getCode();
                            Toast.makeText(Verification_Activity.this, repos.getMessage(), Toast.LENGTH_SHORT).show();
                            Log.d("getverificationCode", "code " + code);
                        }*/
                    }
                });

                break;
            case R.id.submit_veri_code_btn:
                try {
                    hideKeyboard();
                    Integer getcode = Integer.valueOf(verificationCodeEdit.getText().toString().trim());
                    Integer resCode = sessionParam.verificationCode;
                    if (!Validations.isEmptyString(String.valueOf(getcode))) {
                        if (getcode.equals(resCode)) {

                            verificationCodeEdit.setVisibility(View.GONE);
                            subVerCodeButton.setVisibility(View.GONE);
                            verPasswordEdit.setVisibility(View.VISIBLE);
                            verCofrimPasswordEdit.setVisibility(View.VISIBLE);
                            confrimButton.setVisibility(View.VISIBLE);
                        } else {
                            utility.showToast(Verification_Activity.this,"Please enter valid code.");
                        }
                    } else {
                        utility.showToast(Verification_Activity.this,"Please enter valid code.");

                    }
                }catch (Exception e){
                    e.printStackTrace();
                    utility.showToast(Verification_Activity.this,"Please enter valid code.");

                }
                break;
            case R.id.country_code_tv:

                if (countryLists.size()>0) {
                    countryDialogList(countryLists);
                }else{
                    try{
                        loading.show();
                        viewModel.getCountryCode().observe(this, new Observer<CountryResponse>() {
                            @Override
                            public void onChanged(@Nullable CountryResponse countryRes) {
                                loading.dismiss();
                                if (countryRes!=null) {
                                    //CountryResponse  response = ~gson.fromJson(CountryResponse.toString(), CountryResponse.class);
                                    //userModel = gson.fromJson(object.toString(),User.class);
                                    countryLists = countryRes.getCountryList();
                                    countryDialogList(countryLists);
                                }
                            }
                        });


                        //setHasStableIds(true);
                    }catch (Exception e){
                        e.printStackTrace();
                    }

                }
                break;
            case R.id.ver_mobile_layout:
            case R.id.var_mobile_et:
                VerEmailIdEdit.setText("");
                /*
                int imgResource = R.drawable.your_drawable;
setFeeling.setCompoundDrawablesWithIntrinsicBounds(imgResource, 0, 0, 0);
setFeeling.setCompoundDrawablePadding(8)
                * */
                getmobileVisible();

                break;
            case R.id.ver_email_et:
                verMobileEdit.setText("");
                countryCodeTextView.setText("+");
                countryId="";
               getEmilVisible();
                break;

        }

    }

    private void getMobileAndEmilVisible() {
        flag=0;
        if(sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
            mobileLayout.setBackgroundDrawable(ContextCompat.getDrawable(this, R.drawable.rnd_primary_edit) );
            VerEmailIdEdit.setBackgroundDrawable(ContextCompat.getDrawable(this, R.drawable.rnd_primary_edit) );
            verMobileEdit.setBackgroundDrawable(ContextCompat.getDrawable(this, R.drawable.rnd_primary_edit) );
        } else {
            mobileLayout.setBackground(ContextCompat.getDrawable(this, R.drawable.rnd_primary_edit));
            VerEmailIdEdit.setBackground(ContextCompat.getDrawable(this, R.drawable.rnd_primary_edit));
            verMobileEdit.setBackground(ContextCompat.getDrawable(this, R.drawable.rnd_primary_edit));
        }
    }
    private void getEmilVisible() {
        flag=1;
        final int sdk = android.os.Build.VERSION.SDK_INT;
        if(sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
            mobileLayout.setBackgroundDrawable(ContextCompat.getDrawable(this, R.drawable.background_gry_btn) );
            verMobileEdit.setBackgroundDrawable(ContextCompat.getDrawable(this, R.drawable.background_gry_btn) );
            VerEmailIdEdit.setBackgroundDrawable(ContextCompat.getDrawable(this, R.drawable.rnd_primary_edit) );
        } else {
            mobileLayout.setBackground(ContextCompat.getDrawable(this, R.drawable.background_gry_btn));
            VerEmailIdEdit.setBackground(ContextCompat.getDrawable(this, R.drawable.rnd_primary_edit));
            verMobileEdit.setBackground(ContextCompat.getDrawable(this, R.drawable.background_gry_btn));

        }
    }

    private void getmobileVisible() {
        flag=2;
        final int sdk = android.os.Build.VERSION.SDK_INT;
        if(sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
            verMobileEdit.setBackgroundDrawable(ContextCompat.getDrawable(this, R.drawable.rnd_primary_edit) );
            mobileLayout.setBackgroundDrawable(ContextCompat.getDrawable(this, R.drawable.rnd_primary_edit) );
            VerEmailIdEdit.setBackgroundDrawable(ContextCompat.getDrawable(this, R.drawable.background_gry_btn) );
        } else {
            verMobileEdit.setBackground(ContextCompat.getDrawable(this, R.drawable.rnd_primary_edit));
            mobileLayout.setBackground(ContextCompat.getDrawable(this, R.drawable.rnd_primary_edit));
            VerEmailIdEdit.setBackground(ContextCompat.getDrawable(this, R.drawable.background_gry_btn));
        }
    }


    //call on text change for validation
    private class MyTextWatcher implements TextWatcher {

        private View view;

        private MyTextWatcher(View view) {
            this.view = view;
        }

        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {


        }

        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        public void afterTextChanged(Editable editable) {

            switch (view.getId()) {
                case R.id.ver_email_et:
                    getEmilVisible();
                    if (Validations.isValidEmail(VerEmailIdEdit.getText().toString().trim())){
                        verMobileEdit.setText("");
                        countryCodeTextView.setText("+");
                        countryId="";
                        getEmilVisible();
                       // hideKeyboard();
                    }

                    break;
                case R.id.var_mobile_et:
                    getmobileVisible();
                    /*if (Validations.isValidPhone(verMobileEdit.getText().toString().trim())){
                        VerEmailIdEdit.setText("");

                        getmobileVisible();
                        //hideKeyboard();
                    }*/
                   /* if (editable.toString().length() == 1 && editable.toString().startsWith("0")) {
                        editable.clear();
                    }*/
                    break;
                case R.id.ver_verification_code_et:
                    Validations.isValidNull(verificationCodeEdit.getText().toString().trim());
                    break;
                case R.id.ver_password_et:
                    Validations.isValidPassword(verPasswordEdit.getText().toString().trim());
                    break;
                case R.id.ver_confrim_Password_et:
                    Validations.isValidPassword(verCofrimPasswordEdit.getText().toString().trim());
                    break;
            }
        }
    }

    private void requestFocus(View view) {
        if (view.requestFocus()) {
            if (this!=null)
                this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }



    /*pop to select user*/
    public void countryDialogList(final List<CountryList> countryData) {


            final Dialog dialog = new Dialog(this);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(R.layout.dialog_country_list);
            dialog.setCanceledOnTouchOutside(true);
            final RecyclerView rv_list = dialog.findViewById(R.id.rv_list);
            final TextView cancelTxt = dialog.findViewById(R.id.dialog_cancel_text);
            RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
            rv_list.setLayoutManager(layoutManager);
            Log.d("country", countryData.toString());
            rv_list.setAdapter(new CountryListDialog(countryData, this));

            rv_list.addOnItemTouchListener(new RecyclerItemClickListener(this, rv_list, new RecyclerItemClickListener.OnItemClickListener() {
                @Override
                public void onItemClick(View view, int position) {
                    countryId = countryLists.get(position).getId() + "";
                    String countryCode = countryLists.get(position).getPhonecode() + "";
                    countryCodeTextView.setVisibility(View.VISIBLE);
                    countryCodeTextView.setText("+" + countryCode + " ");
                    countryCodeTextView.setTextColor(getResources().getColor(R.color.colorPrimary));
                    sessionParam.countryId = countryId;
                    dialog.dismiss();
                }

                @Override
                public void onLongItemClick(View view, int position) {

                }
            }));
            cancelTxt.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dialog.cancel();
                }
            });


            WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
            Window window = dialog.getWindow();
            lp.copyFrom(window.getAttributes());
            lp.width = WindowManager.LayoutParams.MATCH_PARENT;
            lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
            window.setAttributes(lp);
            dialog.show();

    }

    private void permission() {
        //datafinish = true;
        List<String> permissionsNeeded = new ArrayList<String>();
        final List<String> permissionsList = new ArrayList<String>();
        if (!addPermission(permissionsList, Manifest.permission.ACCESS_FINE_LOCATION))
            permissionsNeeded.add("GPS");
        if (!addPermission(permissionsList, Manifest.permission.ACCESS_NETWORK_STATE))
            permissionsNeeded.add("Network state");
        if (!addPermission(permissionsList, Manifest.permission.ACCESS_COARSE_LOCATION))
            permissionsNeeded.add("Location");
        if (!addPermission(permissionsList, Manifest.permission.INTERNET))
            permissionsNeeded.add("Internet");
        if (!addPermission(permissionsList, Manifest.permission.READ_PHONE_STATE))
            permissionsNeeded.add("phone status");
        if (!addPermission(permissionsList, Manifest.permission.CAMERA))
            permissionsNeeded.add("Camera");
        if (!addPermission(permissionsList, Manifest.permission.WRITE_EXTERNAL_STORAGE))
            permissionsNeeded.add("Write storage");
        if (permissionsList.size() > 0) {
            if (permissionsNeeded.size() > 0) {
                // Need Rationale
                String message = "You need to grant access to " + permissionsNeeded.get(0);
                for (int i = 1; i < permissionsNeeded.size(); i++)
                    message = message + ", " + permissionsNeeded.get(i);
                /*showMessageOKCancel(message, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            requestPermissions(permissionsList.toArray(new String[permissionsList.size()]), REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS);
                        }
                    }
                });*/
                return;
            }
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermissions(permissionsList.toArray(new String[permissionsList.size()]), REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS);
            }
            return;
        }
        //init();
    }

    private boolean addPermission(List<String> permissionsList, String permission) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(permission) != PackageManager.PERMISSION_GRANTED) {
                permissionsList.add(permission);
                //Check for Rationale Optiong
                if (!shouldShowRequestPermissionRationale(permission))
                    return false;
            }
        }
        return true;
    }
    private boolean hasPermission(String permission) {
        if (canMakeSmores()) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                return (checkSelfPermission(permission) == PackageManager.PERMISSION_GRANTED);
            }
        }
        return true;
    }
    private boolean canMakeSmores() {
        return (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP_MR1);
    }

    @TargetApi(Build.VERSION_CODES.M)
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {

        switch (requestCode) {

            case ALL_PERMISSIONS_RESULT:
                for (String perms : permissionsToRequest) {
                    if (!hasPermission(perms)) {
                        permissionsRejected.add(perms);
                    }
                }

                if (permissionsRejected.size() > 0) {


                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        if (shouldShowRequestPermissionRationale(permissionsRejected.get(0))) {
                            showMessageOKCancel("These permissions are mandatory for the application. Please allow access.",
                                    new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                                requestPermissions(permissionsRejected.toArray(new String[permissionsRejected.size()]), ALL_PERMISSIONS_RESULT);
                                            }
                                        }
                                    });
                            return;
                        }
                    }

                }

                break;
        }

    }
    private void showMessageOKCancel(String message, DialogInterface.OnClickListener okListener) {
        new AlertDialog.Builder(Verification_Activity.this)
                .setMessage(message)
                .setPositiveButton("OK", okListener)
                .setNegativeButton("Cancel", null)
                .create()
                .show();
    }

    protected void hideKeyboard()
    {
        View view = this.getCurrentFocus();
        if (view != null)
        {
            ((InputMethodManager) this.getSystemService(Context.INPUT_METHOD_SERVICE))
                    .hideSoftInputFromWindow(view.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        }
    }

    private String getCompleteAddressString(double LATITUDE, double LONGITUDE) {
        String strAdd = "";
        String countryName="";
        Geocoder geocoder = new Geocoder(this, Locale.getDefault());
        try {

            List<Address> addresses = geocoder.getFromLocation(LATITUDE, LONGITUDE, 1);
            if (addresses != null) {
                Address returnedAddress = addresses.get(0);
                StringBuilder strReturnedAddress = new StringBuilder("");

                for (int i = 0; i <= returnedAddress.getMaxAddressLineIndex(); i++) {
                    strReturnedAddress.append(returnedAddress.getAddressLine(i)).append("\n");
                }
                strAdd = strReturnedAddress.toString();


                if (addresses != null && addresses.size() > 0) {
                    String address = addresses.get(0).getAddressLine(0);
                    String city = addresses.get(0).getLocality();
                    String state = addresses.get(0).getAdminArea();
                     countryName = addresses.get(0).getCountryName();
                    String countryCode = addresses.get(0).getCountryCode();
                    String postalCode = addresses.get(0).getPostalCode();
                    String knownName = addresses.get(0).getFeatureName();

                    // profileAddress.setText(address + " " + city + " " + country);
                }
                Log.w("My  location address", strReturnedAddress.toString());
            } else {
                Log.w("My  location address", "No Address returned!");
            }
        } catch (Exception e) {
            e.printStackTrace();
            Log.w("My  location address", "Cannot get Address!");
        }
        return countryName;
    }
}
