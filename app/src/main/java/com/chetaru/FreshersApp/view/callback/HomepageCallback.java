package com.chetaru.FreshersApp.view.callback;

import com.chetaru.FreshersApp.service.model.UserList;
import com.chetaru.FreshersApp.service.model.UserViewPost;

public interface HomepageCallback {
        void onConnectionCall(UserViewPost userViewPost,String connectName,String parentClass);
        void onMentionCall(UserViewPost userViewPost,int personId, int position,String parentClass);
        void onTagName(UserViewPost userViewPost,String parentClass);
        void onLikeCountUser(UserViewPost userViewPost,String parenClass);
        void onButtonClick(String buttonName,String actionBack);
        void onHashTagCall(int tagId,String tagName,String parentclass);
    }