package com.chetaru.FreshersApp.view.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Filter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.chetaru.FreshersApp.R;
import com.chetaru.FreshersApp.service.model.NotificationList;
import com.chetaru.FreshersApp.service.model.PendingRequestList;
import com.chetaru.FreshersApp.utility.SessionParam;
import com.chetaru.FreshersApp.utility.Utility;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class RequestAdapter extends RecyclerView.Adapter<RequestAdapter.ViewHolder> {

    List<PendingRequestList> list = new ArrayList<>();
    private List<PendingRequestList> mFilteredList;
    private Context context;
    private boolean isLoadingAdded = false;
    // View Types
    private static final int ITEM = 0;
    private static final int LOADING = 1;
    private boolean retryPageLoad = false;
    private String errorMsg;
   // private PaginationAdapterCallback mCallback;
    Utility utility;
    SessionParam sessionParam;
   // NotificationListener listener;
   // NotificationList updateList;
    requestAdListener mListener;


    public static class ViewHolder extends RecyclerView.ViewHolder {
        CircleImageView logoImage;
        TextView openingMsg,userName;
        Button acceptButton,rejectButton;
        public ViewHolder(@NonNull View view) {
            super(view);
            logoImage=view.findViewById(R.id.iv_logo);
            openingMsg=view.findViewById(R.id.tv_opening_msg);
            userName=view.findViewById(R.id.tv_request_user_name);
            acceptButton=view.findViewById(R.id.conn_request_accept_btn);
            rejectButton=view.findViewById(R.id.conn_request_reject_btn);
        }
    }

    public RequestAdapter(List<PendingRequestList> list,Context mContext,requestAdListener mListener){
        this.mFilteredList=list;
        this.list=list;
        this.context=mContext;
        this.mListener=mListener;
        utility=new Utility();
        sessionParam=new SessionParam(context);
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ViewHolder viewHolder=null;
        LayoutInflater inflater=LayoutInflater.from(parent.getContext());
        switch (viewType){
            case ITEM:
                View viewItem=inflater.inflate(R.layout.conn_request_list,parent,false);
                viewHolder=new ViewHolder(viewItem);
                break;
            case LOADING:
                View viewLoading=inflater.inflate(R.layout.item_progress,parent,false);
                viewHolder=new LoadingVH(viewLoading);
                break;
            default:
                throw new IllegalStateException("Unexpected value: " + viewType);
        }
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        switch (getItemViewType(position)){
            case ITEM:

                PendingRequestList requestList = mFilteredList.get(position); //data

                holder.openingMsg.setText(requestList.getOpeningMessage());
                holder.userName.setText(requestList.getName());
                Glide.with(context).load(requestList.getImageUrl()).apply(new RequestOptions().centerCrop().placeholder(R.drawable.profile_pics)).into(holder.logoImage);
                holder.acceptButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        mListener.acceptRejectClick(requestList,2);
                    }
                });
                holder.rejectButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        mListener.acceptRejectClick(requestList,3);
                    }
                });
                holder.logoImage.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        mListener.requestLogoClick(holder.logoImage,requestList.getImageUrl());
                    }
                });
                holder.itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        mListener.acceptRejectClick(requestList,1);
                    }
                });

                break;
            case LOADING:
                LoadingVH loadingVH= (LoadingVH) holder;
                if (retryPageLoad){
                    loadingVH.mErrorLayout.setVisibility(View.VISIBLE);
                    loadingVH.mProgressBar.setVisibility(View.VISIBLE);
                    loadingVH.mErrorTxt.setText(
                            errorMsg!=null?
                                    errorMsg :
                                    context.getString(R.string.error_msg_unknown));
                }else {
                     /* if(mFilteredList.get(mFilteredList.size()-1).getSelected()==null){
                        loadingVH.mErrorLayout.setVisibility(View.GONE);
                        loadingVH.mProgressBar.setVisibility(View.GONE);
                    }else{
                        loadingVH.mErrorayout.setVisibility(View.GONE);
                        loadingVH.mProgressBar.setVisibility(View.VISIBLE);
                    }*/
                }

                break;
        }
    }

    protected class LoadingVH extends ViewHolder implements View.OnClickListener {
        private ProgressBar mProgressBar;
        private ImageButton mRetryBtn;
        private TextView mErrorTxt;
        private LinearLayout mErrorLayout;

        public LoadingVH(View itemView) {
            super(itemView);
            mProgressBar = itemView.findViewById(R.id.loadmore_progress);
            mRetryBtn = itemView.findViewById(R.id.loadmore_retry);
            mErrorTxt = itemView.findViewById(R.id.loadmore_errortxt);
            mErrorLayout = itemView.findViewById(R.id.loadmore_errorlayout);
            mRetryBtn.setOnClickListener(this);
            mErrorLayout.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.loadmore_retry:
                case R.id.loadmore_errorlayout:
                    showRetry(false, null);
                   // mCallback.retryPageLoad();
                    break;
            }
        }
    }

    public void showRetry(boolean show, @Nullable String errorMsg) {
        retryPageLoad = show;
        notifyItemChanged(mFilteredList.size() - 1);

        if (errorMsg != null) this.errorMsg = errorMsg;
    }


    @Override
    public int getItemCount() {
        return mFilteredList == null ? 0 : mFilteredList.size();
    }

    public Filter getFilter() {

        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {

                String charString = charSequence.toString();

                if (charString.isEmpty()) {

                    mFilteredList = list;
                } else {
                    ArrayList<PendingRequestList> filteredList = new ArrayList<>();

                    for (PendingRequestList addActionUser : list) {

                        if (addActionUser.getOpeningMessage().toLowerCase().contains(charString.toLowerCase()) ||
                                addActionUser.getOpeningMessage().toUpperCase().contains(charString.toUpperCase())) {
                            filteredList.add(addActionUser);
                        }
                    }
                    mFilteredList = filteredList;

                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = mFilteredList;
                return filterResults;

            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                mFilteredList = (ArrayList<PendingRequestList>) filterResults.values;
                notifyDataSetChanged();


            }
        };
    }

    public void add(PendingRequestList r) {
        mFilteredList.add(r);
        notifyItemInserted(mFilteredList.size() - 1);
    }

    public void addAll(List<PendingRequestList> noti_Results) {
        for (PendingRequestList result : noti_Results) {
            add(result);
        }
    }

    public void remove(PendingRequestList r) {
        int position = mFilteredList.indexOf(r);
        if (position > -1) {
            mFilteredList.remove(position);
            notifyItemRemoved(position);
        }
    }
    public void clear() {
        try {
            while (getItemCount() > 0) {
                remove(getItem(0));
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public void addLoadingFooter() {
        isLoadingAdded = true;
        add(new PendingRequestList());
    }

    public void removeLoadingFooter() {
        isLoadingAdded = false;

        int position = mFilteredList.size() - 1;
        PendingRequestList result = getItem(position);

        if (result != null) {
            mFilteredList.remove(position);
            notifyItemRemoved(position);
        }
    }


    @Override
    public int getItemViewType(int position) {
        return (position == mFilteredList.size() - 1 && isLoadingAdded) ? LOADING : ITEM;
//        if(mFilteredList.size()<5){
//            return mFilteredList.size() - 1;
//        }else{
//            return (position == mFilteredList.size() - 1 && isLoadingAdded) ? LOADING : ITEM;
//        }
    }

    public PendingRequestList getItem(int position) {
        return mFilteredList.get(position);
    }



    public interface requestAdListener{
        public void acceptRejectClick(PendingRequestList pendingValue,int value);
        public void requestLogoClick(View view,String imageString);
    }
}
