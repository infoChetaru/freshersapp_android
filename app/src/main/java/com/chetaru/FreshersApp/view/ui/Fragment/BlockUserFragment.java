package com.chetaru.FreshersApp.view.ui.Fragment;

import android.content.Context;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import android.widget.LinearLayout;

import com.chetaru.FreshersApp.R;
import com.chetaru.FreshersApp.service.model.BlockUser;

import com.chetaru.FreshersApp.service.model.UserViewPost;
import com.chetaru.FreshersApp.service.retrofit.BaseRequest;
import com.chetaru.FreshersApp.service.retrofit.Functions;
import com.chetaru.FreshersApp.service.retrofit.RequestReciever;
import com.chetaru.FreshersApp.utility.SessionParam;
import com.chetaru.FreshersApp.utility.Utility;
import com.chetaru.FreshersApp.view.adapter.BlockListAdapter;

import com.google.gson.JsonObject;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Optional;
import butterknife.Unbinder;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link BlockUserFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class BlockUserFragment extends Fragment implements View.OnClickListener, BlockListAdapter.blockListener {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private Unbinder unbinder;

    //call other Class Instance
    Utility utility;
    SessionParam sessionParam;
    Context mContext;
    Boolean blockUser=false;
    Integer type=1;
    List<BlockUser> blockUserList;

    //listener
    private BlockUserListener mListener;

    //RequestCall
    BaseRequest baseRequest;

    //UI Add TO VIEW

    @BindView(R.id.block_user_layout)
    LinearLayout noUserLayout;
    @BindView(R.id.block_recycler_layout)
    LinearLayout blockLayout;

    @BindView(R.id.block_recycler_view)
    RecyclerView blockRecyclerView;

    //model instance
    UserViewPost userViewPost;
    BlockListAdapter listAdapter;



    public BlockUserFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *

     * @return A new instance of fragment BlockUserFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static BlockUserFragment newInstance(UserViewPost viewPost) {
        BlockUserFragment fragment = new BlockUserFragment();
       // Bundle args = new Bundle();
        //args.putString(ARG_PARAM1, viewPost);
        //args.putString(ARG_PARAM2, param2);
        Bundle bundle = new Bundle();
        bundle.putSerializable(ARG_PARAM1, viewPost);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            /*mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);*/
            userViewPost = (UserViewPost) getArguments().getSerializable(
                    mParam1);
           /* Bundle bundle = getArguments();
            userViewPost = (UserViewPost)getArguments().getSerializable(obj);*/
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_block_user, container, false);
        unbinder = ButterKnife.bind(this, view);
        this.mContext=getContext();
        //get Instance
        utility = new Utility();
        sessionParam=new SessionParam(getContext());
        blockUserList=new ArrayList<>();
        initView();
        listAdapter = new BlockListAdapter(blockUserList, getContext(),this);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        blockRecyclerView.setLayoutManager(linearLayoutManager);
        blockRecyclerView.setItemAnimator(new DefaultItemAnimator());
        blockRecyclerView.setAdapter(listAdapter);
        noUserLayout.setVisibility(View.VISIBLE);
        blockLayout.setVisibility(View.GONE);
        getBlockUserApi();
        return view;
    }

    private void initView() {


    }


    @Optional
    @OnClick({})
    @Override
    public void onClick(View view) {
        switch (view.getId()){


        }


    }

    //get user block or unblock
    private void getUserBlock(BlockUser blockList) {
        baseRequest = new BaseRequest(getContext());
        baseRequest.setBaseRequestListner(new RequestReciever() {
            @Override
            public void onSuccess(int requestCode, String Json, Object object) {
                try {

                    JSONObject jsonObject = new JSONObject(Json);
                    String message=jsonObject.getString("message");
                    listAdapter.clear();
                    blockUserList.clear();
                    listAdapter.notifyDataSetChanged();
                    getBlockUserApi();
                    utility.showToast(getContext(),message);

                }catch (Exception e){
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int requestCode, String errorCode, String message) {
                //utility.showToast(getContext(),message);
            }

            @Override
            public void onNetworkFailure(int requestCode, String message) {

                utility.showToast(getContext(),message);
            }
        });

       /* {
            "blockUserId":"6",
                "type":1
        }
        "type" - 1 for block , 2 for unblock*/
        /*if (blockUser){
            blockUser=false;
            type=1;
        }else {
            blockUser=true;
            type=2;
        }*/

        JsonObject main_object = new JsonObject();
        main_object.addProperty("blockUserId",blockList.getId());
        main_object.addProperty("type", 2);
       /* JsonObject object = Functions.getClient().getJsonMapObject(
                "postId",
                "likeStatus",status );*/
        baseRequest.callAPIPost(1, main_object, getString(R.string.api_blockUser));
    }

    private void getBlockUserApi(){

        baseRequest = new BaseRequest(getContext());
        baseRequest.setBaseRequestListner(new RequestReciever() {
            @Override
            public void onSuccess(int requestCode, String Json, Object object) {
                try {



                    JSONObject jsonObject = new JSONObject(Json);
                 //   String message=jsonObject.getString("message");
                   // utility.showToast(getContext(),message);
                    JSONArray userJsonArray = jsonObject.getJSONArray("data");
                    blockUserList=baseRequest.getDataList(userJsonArray, BlockUser.class);
                    if (blockUserList.size()>0){
                        noUserLayout.setVisibility(View.GONE);
                        blockLayout.setVisibility(View.VISIBLE);
                    }else {
                        noUserLayout.setVisibility(View.VISIBLE);
                        blockLayout.setVisibility(View.GONE);
                    }
                    listAdapter.addAll(blockUserList);


                }catch (Exception e){
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int requestCode, String errorCode, String message) {
               // noUserLayout.setVisibility(View.VISIBLE);
                blockLayout.setVisibility(View.GONE);
                //utility.showToast(getContext(),message);
            }

            @Override
            public void onNetworkFailure(int requestCode, String message) {
              //  noUserLayout.setVisibility(View.VISIBLE);
                //blockLayout.setVisibility(View.GONE);
                utility.showToast(getContext(),message);
            }
        });


        /*JsonObject main_object = new JsonObject();
        main_object.addProperty("blockUserId",selectedViewPost.getId());
        main_object.addProperty("type", type);*/
        JsonObject object = Functions.getClient().getJsonMapObject(
                "","" );
        baseRequest.callAPIPost(1, object, getString(R.string.api_getBlockedUserList));

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof BlockUserListener) {
            mListener = (BlockUserListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement TagChildFragment");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void unblockUser(BlockUser blockUser) {
        getUserBlock(blockUser);
    }

    public interface BlockUserListener{
        void blockUserFragment();
    }
}
