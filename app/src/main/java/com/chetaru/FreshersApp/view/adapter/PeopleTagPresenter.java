package com.chetaru.FreshersApp.view.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.chetaru.FreshersApp.R;
import com.chetaru.FreshersApp.service.model.TagList;
import com.chetaru.FreshersApp.service.model.UserList;
import com.chetaru.FreshersApp.utility.SessionParam;
import com.otaliastudios.autocomplete.RecyclerViewPresenter;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

public class PeopleTagPresenter extends RecyclerViewPresenter<UserList> {
    @SuppressWarnings("WeakerAccess")
    protected Adapter adapter;
    Context mContext;
    List<UserList> hashList;
    SessionParam sessionParam;
    int userId;

    public PeopleTagPresenter(Context context, List<UserList> hash) {
        super(context);
        this.mContext=context;
        this.hashList=hash;
        sessionParam=new SessionParam(context);
        userId=sessionParam.id;
    }

    @Override
    protected RecyclerView.Adapter instantiateAdapter() {
        adapter = new Adapter();
        return adapter;
    }
    public List<UserList> getUserLists() {
        return hashList;
    }

    @Override
    protected void onQuery(@Nullable CharSequence query) {
        List<UserList> all = hashList;
        if (TextUtils.isEmpty(query)) {
            adapter.setData(all);
        } else {
            query = query.toString().toLowerCase();
            List<UserList> list = new ArrayList<>();
            for (UserList u : all) {
                if (u.getName().toLowerCase().contains(query) ||
                        u.getName().toLowerCase().contains(query)) {
                    list.add(u);
                }
            }
            adapter.setData(list);
            Log.e("UserPresenter", "found "+list.size()+" users for query "+query);
        }
        adapter.notifyDataSetChanged();
    }

    protected class Adapter extends RecyclerView.Adapter<Adapter.Holder> {

        private List<UserList> tagData;

        @SuppressWarnings("WeakerAccess")
        protected class Holder extends RecyclerView.ViewHolder {
            private View root;
            /*private TextView fullname;
            private TextView username;*/

            @BindView(R.id.country_name_tv)
            TextView countryNameTxt;
            @BindView(R.id.check_selection_image)
            ImageView checkSelectImage;
            @BindView(R.id.upload_image_adapter)
            CircleImageView profileImage;
            private View view;
            Holder(View itemView) {
                super(itemView);
                root = itemView;
                ButterKnife.bind(this,itemView);
            }
        }

        @SuppressWarnings("WeakerAccess")
        public void setData(@Nullable List<UserList> data) {
            this.tagData = data;
        }


        @Override
        public int getItemCount() {
            //return (isEmpty()) ? 1 : data.size();
            try {
                return tagData.size();
            }catch (Exception e){
                e.printStackTrace();
                return 0;
            }
        }

        @NonNull
        @Override
        public Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            return new Holder(LayoutInflater.from(getContext()).inflate(R.layout.row_tag_pepole_list, parent, false));
        }

        private boolean isEmpty() {
            return tagData == null || tagData.isEmpty();
        }

        @SuppressLint("SetTextI18n")
        @Override
        public void onBindViewHolder(@NonNull Holder holder, int position) {
            final UserList tagList = tagData.get(position);

            if (userId!=((tagList.getId()))) {

                try {

                    //holder.fullname.setText(tagList.getTagName());
                    String itemname = tagList.getName().toString().trim();
                    itemname = itemname.replaceAll(" ", "_");
                    holder.countryNameTxt.setText(itemname);
                    holder.root.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            tagList.setSelected(!tagList.isSelected());
                            dispatchClick(tagList);

                        }
                    });

                    Glide.with(mContext)
                            .load(tagList.getImageUrl())
                            .apply(RequestOptions.circleCropTransform())
                            .placeholder(R.drawable.profile_logo)
                            .into(holder.profileImage);

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
