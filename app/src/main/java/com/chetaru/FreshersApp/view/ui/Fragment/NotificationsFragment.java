package com.chetaru.FreshersApp.view.ui.Fragment;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Parcelable;
import android.preference.PreferenceManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.IdRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.chetaru.FreshersApp.R;
import com.chetaru.FreshersApp.service.model.NotificationList;
import com.chetaru.FreshersApp.service.model.TagList;
import com.chetaru.FreshersApp.service.model.UserList;
import com.chetaru.FreshersApp.service.model.UserViewPost;
import com.chetaru.FreshersApp.service.retrofit.ApiClient;
import com.chetaru.FreshersApp.service.retrofit.BaseRequest;
import com.chetaru.FreshersApp.service.retrofit.Functions;
import com.chetaru.FreshersApp.service.retrofit.RequestReciever;
import com.chetaru.FreshersApp.utility.BaseActivity;
import com.chetaru.FreshersApp.utility.MyDividerItemDecoration;
import com.chetaru.FreshersApp.utility.SessionParam;
import com.chetaru.FreshersApp.utility.Utility;
import com.chetaru.FreshersApp.utility.Validations;
import com.chetaru.FreshersApp.view.adapter.NotificatoinAdapter;
import com.chetaru.FreshersApp.view.callback.PaginationAdapterCallback;
import com.chetaru.FreshersApp.viewModel.NotificationsViewModel;
import com.google.android.material.bottomnavigation.BottomNavigationItemView;
import com.google.android.material.bottomnavigation.BottomNavigationMenuView;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.gson.Gson;
import com.google.gson.JsonObject;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.ContentValues.TAG;


public class NotificationsFragment extends Fragment implements NotificatoinAdapter.NotificationListener{

    private Unbinder unbinder;
    private NotificationsViewModel notificationsViewModel;

    @BindView(R.id.notification_list)
    RecyclerView rv_list_archive;
    @BindView(R.id.notification_refresh)
    SwipeRefreshLayout mSwipeRefreshLayout;

    Context mContext;
    Button logoutButton;
    Utility utility;
    SessionParam sessionParam;
    String tokenId="";
    BaseRequest baseRequest;
    private static final int PAGE_START = 1;
    private boolean isLoading = false;
    private int currentPage = PAGE_START;
    private static int TOTAL_PAGES = 1;
    private boolean isLastPage = false;
    NotificatoinAdapter ad_notificationList;
    List<NotificationList> list = new ArrayList<>();
    List<NotificationList> notiList;
    private NotificationListenerFragment mListener;
    String firstRowData="";
    LinearLayout notiLayout,recyclerLayout;
    TextView tv_notiCount;
    String  LIST_STATE_KEY="SAVEVALUE";
    int countValue=0;

    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
     List<NotificationList> oldNotificationList=new ArrayList<>();
    public int oldPosition;
    LinearLayoutManager mLayoutManager;
    private static Bundle mBundleRecyclerViewState;


    public NotificationsFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *

     * @return A new instance of fragment ConnectionListFeagment.
     */
    // TODO: Rename and change types and number of parameters
    public static NotificationsFragment newInstance(List<NotificationList> oldList, int position) {
        NotificationsFragment fragment = new NotificationsFragment();
        Bundle args = new Bundle();
        args.putSerializable(ARG_PARAM1, (Serializable) oldList);
        args.putInt(ARG_PARAM2, position);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try {
            if (getArguments() != null) {
                oldNotificationList = (List<NotificationList>) getArguments().getSerializable(ARG_PARAM1);
                oldPosition = getArguments().getInt(ARG_PARAM2);
            }
        }catch (Exception e){
            e.printStackTrace();
        }

    }





    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        notificationsViewModel =
                ViewModelProviders.of(this).get(NotificationsViewModel.class);
        View root = inflater.inflate(R.layout.fragment_notifications, container, false);
        unbinder = ButterKnife.bind(this, root);
       /* if (savedInstanceState!=null) {
            onRestoreInstanceState(savedInstanceState);
            Log.d("saveState",savedInstanceState.toString());
            rv_list_archive.getLayoutManager().onRestoreInstanceState(mListState);
        }else {
            Log.d("NotSaveState","NotSave");
        }*/


        final TextView textView = root.findViewById(R.id.text_logout);
        rv_list_archive = root.findViewById(R.id.notification_list);

        notiLayout=root.findViewById(R.id.my_notification_layout);
        recyclerLayout=root.findViewById(R.id.noti_recycler_layout);
        mContext=getContext();
        sessionParam=new SessionParam(getContext());
        tokenId=sessionParam.token;
        utility=new Utility();
        notiList=new ArrayList<>();

        notificationsViewModel.getText().observe(getViewLifecycleOwner(), new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {

                //textView.setText(s);
            }
        });
        mLayoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        rv_list_archive.setLayoutManager(mLayoutManager);
        rv_list_archive.setItemAnimator(new DefaultItemAnimator());

        if (oldNotificationList.size() > 0){
           // ad_notificationList.addAll(oldNotificationList);

            ad_notificationList = new NotificatoinAdapter(oldNotificationList, mContext,this);
            rv_list_archive.setAdapter(ad_notificationList);
            rv_list_archive.getLayoutManager().scrollToPosition(oldPosition);
            notiLayout.setVisibility(View.GONE);
            recyclerLayout.setVisibility(View.VISIBLE);
        }else {
            ad_notificationList = new NotificatoinAdapter(notiList, mContext,this);
            rv_list_archive.setAdapter(ad_notificationList);
            api_notificationList();
            notiLayout.setVisibility(View.VISIBLE);
            recyclerLayout.setVisibility(View.GONE);
        }
        mSwipeRefreshLayout.setRefreshing(false);
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                mSwipeRefreshLayout.setRefreshing(true);
                //ad_notificationList.clear();
                //ad_notificationList.notifyDataSetChanged();
                api_notificationRefresh();
               // isLoading = true;
                mSwipeRefreshLayout.setRefreshing(false);
            }
        });
        rv_list_archive.addOnScrollListener(new PaginationScrollList(mLayoutManager) {
            @Override
            protected void loadMoreItems() {
                isLoading = true;
                currentPage += 1;
                api_loadnextPage();
            }

           /* @Override
            public int getTotalPageCount() {
                //Toast.makeText(mContext, "getTotalPageCount", Toast.LENGTH_SHORT).show();
                return TOTAL_PAGES;
            }*/

            @Override
            public boolean isLastPage() {
                //Toast.makeText(mContext, "isLastPage", Toast.LENGTH_SHORT).show();
                return isLastPage;
            }

            @Override
            public boolean isLoading() {
                //Toast.makeText(mContext, "isLoading", Toast.LENGTH_SHORT).show();
                return isLoading;
            }
        });



        return root;
    }






    //this api we used for archive
    /*API call to get all notification list
     */
    public void api_notificationList() {
        //hideErrorView();
        currentPage = PAGE_START;
        baseRequest = new BaseRequest(mContext);
        baseRequest.setBaseRequestListner(new RequestReciever() {
            @Override
            public void onSuccess(int requestCode, String Json, Object object) {
                list.clear();
                Gson gson = new Gson();
                try {
                    notiList.clear();
                    JSONObject jsonObject = new JSONObject(Json);
                    TOTAL_PAGES = jsonObject.optInt("totalPageCount");
                  String  getCurrentpage=jsonObject.optString("currentPage");
                    JSONArray jsonArray = new JSONArray(object.toString());
                    notiList = baseRequest.getDataList(jsonArray, NotificationList.class);
                    ad_notificationList.addAll(notiList);
                    if (notiList.size()>0){
                        notiLayout.setVisibility(View.GONE);
                        recyclerLayout.setVisibility(View.VISIBLE);
                    }else {
                        notiLayout.setVisibility(View.VISIBLE);
                        recyclerLayout.setVisibility(View.GONE);
                    }
                    if (notiList.size()>0){
                        firstRowData=notiList.get(0).getCreatedAt();

                    }

                    if (currentPage < TOTAL_PAGES)
                        ad_notificationList.addLoadingFooter();
                    else {
                        isLastPage = true;
                    }

                    //rv_list_archive.setAdapter(ad_notificationListP);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }


            @Override
            public void onFailure(int requestCode, String errorCode, String message) {
                //errorLayout.showError(message);
                //utility.showToast(mContext, message);
                notiLayout.setVisibility(View.VISIBLE);
                recyclerLayout.setVisibility(View.GONE);
            }

            @Override
            public void onNetworkFailure(int requestCode, String message) {
                //errorLayout.showError(message);
                utility.showToast(mContext, message);
            }
        });

        JsonObject object = Functions.getClient().getJsonMapObject(
                "page", currentPage + ""
        );
        baseRequest.callAPIPostWOLoader(1, object, getString(R.string.api_getNotificationList));
    }


    //this method is used to paging in our notification list
    public void api_loadnextPage() {
        baseRequest = new BaseRequest(mContext);
        baseRequest.setBaseRequestListner(new RequestReciever() {
            @Override
            public void onSuccess(int requestCode, String Json, Object object) {
                Gson gson = new Gson();
                try {

                    JSONObject jsonObject = new JSONObject(Json);
                    TOTAL_PAGES = jsonObject.optInt("totalPageCount");
                    JSONArray jsonArray = new JSONArray(object.toString());
                    notiList = baseRequest.getDataList(jsonArray, NotificationList.class);
                    // utility.showToast(getContext(),object.toString());

                    /*JSONArray jsonArray = new JSONArray(object.toString());
                    list = baseRequest.getDataList(jsonArray, UserList.class);*/

                    //ad_notificationListP = new Ad_NotificationList_pagination(rv_list,list, mContext);
                    ad_notificationList.removeLoadingFooter();
                    isLoading = false;
                    /*for(int i=0;i<5;i++){
                        NotificationList list=new NotificationList();
                        list.setNotificationType("customNotification");
                        list.setIsRead(notiList.get(i).getIsRead());
                        list.setCreatedAt(notiList.get(i).getCreatedAt());
                        if (i==2){
                            list.setDescription("First step involves compiling the resources folder (/res) using the aapt (android asset packaging tool) tool. These are compiled to a single class file called R.java. This is a class that just contains constants.");
                        }else {
                            list.setDescription(notiList.get(i).getDescription());
                        }
                        list.setId(notiList.get(i).getId());
                        list.setImage(notiList.get(i).getImage());
                        list.setPostId(notiList.get(i).getPostId());
                        notiList.set(i,list);
                    }*/
                    ad_notificationList.addAll(notiList);
                    //rv_list_archive.setAdapter(ad_notificationList);
                    if (currentPage != TOTAL_PAGES) ad_notificationList.addLoadingFooter();
                    else isLastPage = true;

                    //rv_list_archive.setAdapter(ad_notificationListP);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int requestCode, String errorCode, String message) {
                //errorLayout.showError(message);
               // utility.showToast(mContext, message);
                mSwipeRefreshLayout.setRefreshing(false);

            }

            @Override
            public void onNetworkFailure(int requestCode, String message) {
                //errorLayout.showError(message);
                utility.showToast(mContext, message);
                try {
                    mSwipeRefreshLayout.setRefreshing(false);
                }catch (Exception e){
                    e.printStackTrace();
                }

            }
        });

        JsonObject object = Functions.getClient().getJsonMapObject(
                "page", currentPage + ""
        );

        baseRequest.callAPIPostWOLoader(1, object, getString(R.string.api_getNotificationList));
    }
    private void api_notificationRefresh(){
        baseRequest=new BaseRequest(mContext);
        baseRequest.setBaseRequestListner(new RequestReciever() {
            @Override
            public void onSuccess(int requestCode, String Json, Object object) {
                try {
                    JSONArray jsonArray = new JSONArray(object.toString());
                    notiList = baseRequest.getDataList(jsonArray, NotificationList.class);
                    mSwipeRefreshLayout.setRefreshing(false);
                }catch (Exception e){
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(int requestCode, String errorCode, String message) {
               // utility.showToast(mContext,message);
                mSwipeRefreshLayout.setRefreshing(false);

            }

            @Override
            public void onNetworkFailure(int requestCode, String message) {
                utility.showToast(mContext,message);
                try {
                    mSwipeRefreshLayout.setRefreshing(false);
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        });

        JsonObject main_object=new JsonObject();
        main_object.addProperty("page",0);
        main_object.addProperty("date",firstRowData);
        baseRequest.callAPIPost(1,main_object,getString(R.string.api_getNotificationList));
    }



    @Override
    public void notiCall(List<NotificationList> oldList,NotificationList notificationList ,int position) {
        //utility.showToast(getContext(),"call Api Read");
        if (!notificationList.getIsRead()) {
            readNotificationApi(notificationList, position);
            mListener.getReadCount();
        }
        /*if (!notificationList.getLocale()){
            //getPremiumDailog();
            mListener.PremiumFragmentCall();
            //mListener.likeFragmentCall(notificationList,position);
        }*/
        if (notificationList.getNotificationType().equals("postLike")){
            // utility.showToast(getContext(),"callViewPost");
            mListener.likeFragmentCall(oldList,notificationList,position,notificationList.getNotificationType());
        }
        if(notificationList.getNotificationType().equals("tagUser")){
            mListener.likeFragmentCall(oldList,notificationList,position,notificationList.getNotificationType());
        }
        if (notificationList.getNotificationType().equals("customNotification")){
            // utility.showToast(getContext(),"callViewPost");
            adminCustomDialog(notificationList);
        }
    }

    private void adminCustomDialog(NotificationList notificationList) {
        final Dialog dialog = new Dialog(getContext());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setContentView(R.layout.dialog_admin_noti);
        dialog.setCanceledOnTouchOutside(true);
        final TextView desTxt = dialog.findViewById(R.id.admin_desc_noti_txt);
        final TextView closeTxt = dialog.findViewById(R.id.tv_close);
        EditText serchET = dialog.findViewById(R.id.tag_search_view_edt);
        // listening to search query text change

        desTxt.setText(notificationList.getDescription());

        closeTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.cancel();
            }
        });


        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = dialog.getWindow();
        lp.copyFrom(window.getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setBackgroundDrawableResource(R.color.primary_trans);
        window.setAttributes(lp);
        dialog.show();
    }

    private void readNotificationApi(NotificationList notificationList, int position) {
        baseRequest = new BaseRequest(mContext);
        baseRequest.setBaseRequestListner(new RequestReciever() {
            @Override
            public void onSuccess(int requestCode, String Json, Object object) {
                Gson gson = new Gson();
                try {
                    JSONObject jsonObject = new JSONObject(Json);
                    String message=jsonObject.getString("message");
                    //Call notification count badges
                    modifyItem(position,true,notificationList);

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int requestCode, String errorCode, String message) {
                //errorLayout.showError(message);
                utility.showToast(mContext, message);
                modifyItem(position,false,notificationList);
            }

            @Override
            public void onNetworkFailure(int requestCode, String message) {
                //errorLayout.showError(message);
                utility.showToast(mContext, message);
                try {
                    modifyItem(position, false, notificationList);
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        });

        JsonObject object = Functions.getClient().getJsonMapObject(
                "notificationId", notificationList.getId() + ""
        );

        baseRequest.callAPIPostWOLoader(1, object, getString(R.string.api_markNotificationAsRead));

    }

    public void modifyItem(final int position,final Boolean readValue, final NotificationList updateList) {

       try {
           updateList.setIsRead(readValue);
           notiList.set(position, updateList);
           ad_notificationList.notifyItemChanged(position);
       }catch (Exception e){
           e.printStackTrace();
       }
    }
    public abstract class PaginationScrollList extends RecyclerView.OnScrollListener {

        private LinearLayoutManager layoutManager;

        public PaginationScrollList(LinearLayoutManager layoutManager) {
            this.layoutManager = layoutManager;
        }

        @Override
        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
            super.onScrolled(recyclerView, dx, dy);

            int visibleItemCount = layoutManager.getChildCount();
            int totalItemCount = layoutManager.getItemCount();
            int firstVisibleItemPosition = layoutManager.findFirstVisibleItemPosition();

            if (!isLoading() && !isLastPage()) {
                if ((visibleItemCount + firstVisibleItemPosition) >= totalItemCount
                        && firstVisibleItemPosition >= 0) {
                    loadMoreItems();
                }
            }
        }

        protected abstract void loadMoreItems();

        public abstract boolean isLastPage();

        public abstract boolean isLoading();

    }

    private void getPremiumDailog() {
        final Dialog dialog = new Dialog(getContext());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.premium_layout_msg);
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(true);
        final Button upgradeButton = dialog.findViewById(R.id.premium_upgrade_button);
        final Button homeButton = dialog.findViewById(R.id.premium_home_button);
        upgradeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                final int sdk = android.os.Build.VERSION.SDK_INT;
                if(sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
                    upgradeButton.setBackgroundDrawable(ContextCompat.getDrawable(getContext(), R.drawable.button_background_primary) );
                    homeButton.setBackgroundDrawable(ContextCompat.getDrawable(getContext(), R.drawable.button_white_color_border) );
                    upgradeButton.setTextColor(ContextCompat.getColor(getContext(),R.color.white));
                    homeButton.setTextColor(ContextCompat.getColor(getContext(),R.color.colorPrimary));
                } else {
                    upgradeButton.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.button_background_primary));
                    homeButton.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.button_white_color_border));
                    upgradeButton.setTextColor(ContextCompat.getColor(getContext(),R.color.white));
                    homeButton.setTextColor(ContextCompat.getColor(getContext(),R.color.colorPrimary));
                }
                utility.showToast(getContext(),"Coming Soon");
                dialog.cancel();
            }
        });
        homeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final int sdk = android.os.Build.VERSION.SDK_INT;
                if(sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
                    upgradeButton.setBackgroundDrawable(ContextCompat.getDrawable(getContext(), R.drawable.button_white_color_border) );
                    homeButton.setBackgroundDrawable(ContextCompat.getDrawable(getContext(), R.drawable.button_background_primary) );
                    upgradeButton.setTextColor(ContextCompat.getColor(getContext(),R.color.colorPrimary));
                    homeButton.setTextColor(ContextCompat.getColor(getContext(),R.color.white));
                } else {
                    upgradeButton.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.button_white_color_border));
                    homeButton.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.button_background_primary));
                    upgradeButton.setTextColor(ContextCompat.getColor(getContext(),R.color.colorPrimary));
                    homeButton.setTextColor(ContextCompat.getColor(getContext(),R.color.white));
                }
                //tagLayout.setVisibility(View.GONE);

                dialog.cancel();
            }
        });

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = dialog.getWindow();
        lp.copyFrom(window.getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.MATCH_PARENT;
        window.setAttributes(lp);
        dialog.show();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof NotificationListenerFragment) {
            mListener = (NotificationListenerFragment) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement TagChildFragment");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        //getArguments().remove("myData");
        unbinder.unbind();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    public interface NotificationListenerFragment{
        void getReadCount();
        void PremiumFragmentCall();
        void likeFragmentCall(List<NotificationList> notiList,NotificationList notificationList, int position,String notificationType);
    }
}

