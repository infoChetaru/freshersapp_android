package com.chetaru.FreshersApp.view.adapter;

import android.animation.ObjectAnimator;
import android.content.Context;
import android.graphics.Bitmap;
import android.os.Build;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.chetaru.FreshersApp.R;
import com.chetaru.FreshersApp.service.model.UserViewPost;
import com.chetaru.FreshersApp.utility.SessionParam;
import com.chetaru.FreshersApp.utility.Utility;
import com.chetaru.FreshersApp.view.callback.PaginationListenerCallback;
import com.jackandphantom.blurimage.BlurImage;
import com.tylersuehr.socialtextview.SocialTextView;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class MyUploadsHomeAdapter extends RecyclerView.Adapter {
    public static  final int VIEW_TYPE_USER=2;

    private List<UserViewPost> mNewsItemList=new ArrayList<>();

    Context mContext;
    Boolean click=true,expand=false;
    Utility utility;
    SessionParam sessionParam;

    public PaginationListenerCallback listener;
    Integer userId;
    int maxline=0,height=0;
    Boolean premiumUser;
    int sensitivePostsAllowed=2;
    int sensitivePost=2;

    public MyUploadsHomeAdapter(List<UserViewPost> mNewsItemList, Context mContext, PaginationListenerCallback listener) {
        this.mNewsItemList = mNewsItemList;
        this.mContext = mContext;
        this.listener = listener;
        utility=new Utility();
        //this.expandableTextView=new ExpandableTextView(mContext);
        sessionParam=new SessionParam(mContext);
        sensitivePostsAllowed=sessionParam.sensitivePostsAllowed;
        userId=(sessionParam.id);
        premiumUser=sessionParam.premieUser;
        //premiumUser=true;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.home_userpost_list,parent,false);
        return new UserViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        UserViewPost viewPost=mNewsItemList.get(position);
        ((UserViewHolder)holder).bind(viewPost,position);
    }

    public class UserViewHolder extends RecyclerView.ViewHolder{

        SocialTextView hashTextView;
        ImageView newsImage,imageUpper,imageSceond,linkConnectionImage;
        public ImageView imageShowMore;

        ImageView likeImage,replyImage;
        CircleImageView iv_logo;
        TextView senPostMsg,originalPostTextView;
        TextView homeUserTextView,dateTextView,likeCountTxt;
        LinearLayout layoutMoreTxt,changeLayout,connection_link_layout,likeCountLayout,reply_link_layout;
        LinearLayout root_layout,headerLayout,like_layout,OriginalPostLayout;

        public UserViewHolder(@NonNull View itemView) {
            super(itemView);
            iv_logo = itemView.findViewById(R.id.iv_logo);
            hashTextView = itemView.findViewById(R.id.social_text_view);
            homeUserTextView = itemView.findViewById(R.id.home_user_name_text);
            changeLayout = itemView.findViewById(R.id.set_height_layout);
            imageShowMore = itemView.findViewById(R.id.image_show_more);
            layoutMoreTxt = itemView.findViewById(R.id.layout_showmore_txt);
            newsImage = itemView.findViewById(R.id.upload_image_public);
            imageUpper = itemView.findViewById(R.id.upload_image_upper);
            imageSceond = itemView.findViewById(R.id.upload_image_sceond);
            likeImage = itemView.findViewById(R.id.like_image);
            linkConnectionImage = itemView.findViewById(R.id.link_image);
            replyImage = itemView.findViewById(R.id.reply_image);
            dateTextView = itemView.findViewById(R.id.upload_image_date_txt);
            senPostMsg = itemView.findViewById(R.id.post_msg_view);
            likeCountLayout = itemView.findViewById(R.id.like_count_layout);
            OriginalPostLayout = itemView.findViewById(R.id.view_original_post_layout);
            originalPostTextView = itemView.findViewById(R.id.view_original_post_txt);

            likeCountTxt = itemView.findViewById(R.id.like_count_txt);
            connection_link_layout = itemView.findViewById(R.id.connection_link_layout);
            reply_link_layout = itemView.findViewById(R.id.reply_link_layout);
            root_layout = itemView.findViewById(R.id.root_layout);
            headerLayout = itemView.findViewById(R.id.post_top_layout);
            like_layout = itemView.findViewById(R.id.like_layout);
        }
        public  void bind(UserViewPost mNewsItem, int position){
            try {
                if (mNewsItem.getId()!=null) {
                    try {
                        sensitivePost=mNewsItem.getSensitivePostStatus();
                        //Picasso.with(mContext).load(mNewsItem.getUserImage()).into(iv_logo);
                        Glide.with(mContext).load(mNewsItem.getUserImage()).placeholder(R.drawable.profile_pics).into(iv_logo);
                        homeUserTextView.setText(mNewsItem.getName());
                        likeImage.setVisibility(View.VISIBLE);
                        senPostMsg.setVisibility(View.GONE);
                        /*Picasso.with(mContext).load(mNewsItem.getImageUrl())
                                .into(imageUpper);*/
                        if (userId!=mNewsItem.getUserId()) {
                            if (sensitivePostsAllowed == 1) {
                                senPostMsg.setVisibility(View.GONE);
                                /*Picasso.with(mContext).load(mNewsItem.getImageUrl())
                                        .into(imageUpper);*/
                                Glide.with(mContext).load(mNewsItem.getImageUrl())
                                        .placeholder(R.drawable.profile_pics)
                                        .into(imageUpper);
                                Thread thread = new Thread(new Runnable() {
                                    @Override
                                    public void run() {
                                        Bitmap bitmapFromURL = Utility.getBitmapFromURL(mNewsItem.getImageUrl());
                                        BlurImage.with(mContext).load(bitmapFromURL).intensity(25).Async(true).into(imageSceond);
                                        BlurImage.with(mContext).load(bitmapFromURL).intensity(25).Async(true).into(newsImage);

                                    }
                                });

                                thread.start();
                            } else {
                                if (sensitivePost == 1) {
                                    senPostMsg.setVisibility(View.VISIBLE);
                                    Thread thread = new Thread(new Runnable() {
                                        @Override
                                        public void run() {
                                            Bitmap bitmapFromURL = Utility.getBitmapFromURL(mNewsItem.getImageUrl());
                                            BlurImage.with(mContext).load(bitmapFromURL).intensity(25).Async(true).into(imageSceond);
                                            BlurImage.with(mContext).load(bitmapFromURL).intensity(25).Async(true).into(newsImage);

                                        }
                                    });

                                    thread.start();

                                } else {
                                    senPostMsg.setVisibility(View.GONE);
                                    /*Picasso.with(mContext).load(mNewsItem.getImageUrl())
                                            .into(imageUpper);*/
                                    Glide.with(mContext).load(mNewsItem.getImageUrl()).placeholder(R.drawable.profile_pics)
                                            .into(imageUpper);
                                    Thread thread = new Thread(new Runnable() {
                                        @Override
                                        public void run() {
                                            Bitmap bitmapFromURL = Utility.getBitmapFromURL(mNewsItem.getImageUrl());
                                            BlurImage.with(mContext).load(bitmapFromURL).intensity(25).Async(true).into(imageSceond);
                                            BlurImage.with(mContext).load(bitmapFromURL).intensity(25).Async(true).into(newsImage);

                                        }
                                    });

                                    thread.start();
                                }
                            }
                        }else {
                            senPostMsg.setVisibility(View.GONE);
                                /*Picasso.with(mContext).load(mNewsItem.getImageUrl())
                                        .into(imageUpper);*/
                            Glide.with(mContext).load(mNewsItem.getImageUrl()).placeholder(R.drawable.profile_pics)
                                    .into(imageUpper);
                            Thread thread = new Thread(new Runnable() {
                                @Override
                                public void run() {
                                    Bitmap bitmapFromURL = Utility.getBitmapFromURL(mNewsItem.getImageUrl());
                                    BlurImage.with(mContext).load(bitmapFromURL).intensity(25).Async(true).into(imageSceond);
                                    BlurImage.with(mContext).load(bitmapFromURL).intensity(25).Async(true).into(newsImage);

                                }
                            });

                            thread.start();
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    try{
                        if (mNewsItem.getOriginalPostId()!=0){
                            originalPostTextView.setVisibility(View.VISIBLE);
                            OriginalPostLayout.setVisibility(View.VISIBLE);
                        }else {
                            originalPostTextView.setVisibility(View.GONE);
                            OriginalPostLayout.setVisibility(View.GONE);
                        }
                        originalPostTextView.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                listener.onPaginationClick(mNewsItem, click, position, "originalPost");
                            }
                        });
                        headerLayout.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                listener.onPaginationClick(mNewsItem,false,position,"headerLayout");
                            }
                        });
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                    try {

                        hashTextView.setLinkText(mNewsItem.getCaption());
                        hashTextView.setOnLinkClickListener(new SocialTextView.OnLinkClickListener() {
                            @Override
                            public void onLinkClicked(int linkType, String matchedText) {
                                //linkType 1=hashTag,2==mention
                                listener.autoLinkListener(linkType, "", matchedText, mNewsItem);
                            }
                        });


                        changeLayout.post(new Runnable() {
                            @Override
                            public void run() {
                                //maybe also works height = ll.getLayoutParams().height;
                                height = changeLayout.getHeight();
                            }
                        });
                        layoutMoreTxt.post(new Runnable() {
                            @Override
                            public void run() {

                            }
                        });
                        hashTextView.setLines(2);
                        hashTextView.post(new Runnable() {

                            @Override
                            public void run() {
                                Log.v("Line count: ", hashTextView.getLineCount() + "");
                                maxline = hashTextView.getLineCount();

                            /*if (maxline > 3) {
                                imageShowMore.setVisibility(View.VISIBLE);
                            } else {
                                imageShowMore.setVisibility(View.GONE);
                            }*/
                            }
                        });

                        //hashTextView.setMovementMethod(ScrollingMovementMethod.getInstance());
                    /*hashTextView.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                        @Override
                        public void onGlobalLayout() {
                            if (expand) {
                                expand = false;
                                if (maxline > 3) {
                                    btShowMore.setVisibility(View.GONE);
                                    ObjectAnimator animation = ObjectAnimator.ofInt(hashTextView, "maxLines", 40);
                                    animation.setDuration(0).start();

                                *//*LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
                                params.height = 250;
                                changeLayout.setLayoutParams(params);*//*
                                    int height_in_pixels = hashTextView.getLineCount() * hashTextView.getLineHeight();
                                    LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) changeLayout.getLayoutParams();
                                    if (height > height_in_pixels) {
                                        params.height = height * 2;
                                    } else {
                                        params.height = height_in_pixels;
                                    }
                                    changeLayout.setLayoutParams(params);
                                    //newTitle.setMovementMethod(ScrollingMovementMethod.getInstance());

                                } else {
                                    btShowMore.setVisibility(View.GONE);
                                    ViewGroup.LayoutParams params = (ViewGroup.LayoutParams) changeLayout.getLayoutParams();
                                    params.height = height;
                                    changeLayout.setLayoutParams(params);
                                }
                            }
                        }
                    });*/


                        imageShowMore.setOnClickListener(new View.OnClickListener() {
                            public void onClick(View v) {
                                if (!expand){
                                    expand=true;
                                    dateTextView.setVisibility(View.VISIBLE);
                                    imageShowMore.setImageResource(R.drawable.ic_expand_more_white_24dp);

                                    ObjectAnimator animation = ObjectAnimator.ofInt(hashTextView, "maxLines", 40);
                                    animation.setDuration(100).start();
                                    int height_in_pixels = hashTextView.getLineCount() * hashTextView.getLineHeight();
                                    LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) changeLayout.getLayoutParams();
                                     /*if (height > height_in_pixels) {
                                        params.height = height +height_in_pixels;
                                    } else {
                                       // params.height = (height*2)+((height_in_pixels+100)/2);
                                        params.height = (height)+((height_in_pixels+100)/2);
                                    }*/
                                    final int sdk = android.os.Build.VERSION.SDK_INT;
                                    if (sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
                                        params.height=height*2;
                                        if (params.height<height_in_pixels){
                                            params.height=params.height+height_in_pixels;
                                        }

                                    }else if (sdk< Build.VERSION_CODES.O){
                                        if (params.height<0){
                                            params.height=height_in_pixels+((height_in_pixels + 100) / 2);

                                        }else {
                                            if (height > height_in_pixels) {
                                                params.height = height +height_in_pixels;
                                            } else if (params.height>height_in_pixels){
                                                params.height = (params.height) + ((height_in_pixels + 100) / 2);
                                            }else {
                                                params.height=height+(height_in_pixels + 150);
                                            }
                                        }
                                    }else {
                                        if (height > height_in_pixels) {
                                            params.height = height +height_in_pixels;
                                        } else {
                                            // params.height = (height*2)+((height_in_pixels+100)/2);
                                            if (params.height<0){
                                                params.height=height_in_pixels+((height_in_pixels + 100) / 2);
                                            }else {
                                                params.height = (height) + ((height_in_pixels + 100) / 2);
                                            }
                                            if (height<(height_in_pixels+100)){
                                                params.height=height+height_in_pixels;
                                            }
                                        }
                                    }

                                    changeLayout.setLayoutParams(params);
                                }else{
                                    expand=false;
                                    dateTextView.setVisibility(View.GONE);
                                    imageShowMore.setImageResource(R.drawable.ic_expand_less_white_24dp);

                                    ObjectAnimator animation = ObjectAnimator.ofInt(hashTextView, "maxLines", 3);
                                    animation.setDuration(100).start();
                                    ViewGroup.LayoutParams params = (ViewGroup.LayoutParams) changeLayout.getLayoutParams();
                                    int height_in_pixels = hashTextView.getLineCount() * hashTextView.getLineHeight();

                                    final int sdk = android.os.Build.VERSION.SDK_INT;
                                    if (sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
                                        params.height=params.height+height_in_pixels;

                                    }else if (sdk< Build.VERSION_CODES.O){
                                        params.height=height_in_pixels+5;
                                    }else {
                                        params.height = height;
                                    }
                                    changeLayout.setLayoutParams(params);

                                }
                               /* expand = !expand;
                                advancedView.setVisibility( isExpanded ?  View.VISIBLE : View.GONE );
                                expandCtrl.setCompoundDrawablesWithIntrinsicBounds( isExpanded ? icon_collapse : icon_expand, 0, 0, 0 );
*/

                            }
                        });

                    } catch (Exception e) {
                        e.printStackTrace();
                    }


                    try {
                        Log.d("userName", "session: " + userId + " " + "viewPost: " + mNewsItem.getName().toLowerCase().trim());

                        if (!(userId.equals(mNewsItem.getUserId()))) {
                            linkConnectionImage.setVisibility(View.VISIBLE);
                            replyImage.setVisibility(View.VISIBLE);
                            connection_link_layout.setVisibility(View.VISIBLE);
                            reply_link_layout.setVisibility(View.VISIBLE);
                            likeCountLayout.setVisibility(View.GONE);
                            likeCountTxt.setVisibility(View.GONE);


                        }else {
                            linkConnectionImage.setVisibility(View.GONE);
                            replyImage.setVisibility(View.GONE);
                            connection_link_layout.setVisibility(View.GONE);
                            reply_link_layout.setVisibility(View.GONE);
                            likeCountTxt.setVisibility(View.VISIBLE);
                            likeCountLayout.setVisibility(View.VISIBLE);
                            /*ViewGroup.MarginLayoutParams marginParams = new ViewGroup.MarginLayoutParams(imageShowMore.getLayoutParams());
                            marginParams.setMargins(0, 0, 8, 0);
                            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(marginParams);
                            imageShowMore.setLayoutParams(layoutParams);*/
                            //show like count user list
                            likeCountTxt.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    if (premiumUser) {
                                        listener.onPaginationClick(mNewsItem, click, position, "likeCount");
                                    }else {
                                        listener.onPaginationClick(mNewsItem,click,position,"premiumUpgrade");
                                    }
                                }
                            });


                        }


                        final int sdk = android.os.Build.VERSION.SDK_INT;

                        if (mNewsItem.getLikedStatus()) {
                            click = false;
                            if (sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
                                likeImage.setBackgroundDrawable(ContextCompat.getDrawable(mContext, R.mipmap.admin_post_like));
                            } else {
                                likeImage.setBackground(ContextCompat.getDrawable(mContext, R.mipmap.admin_post_like));
                            }
                        } else {
                            click = true;
                            if (sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
                                likeImage.setBackgroundDrawable(ContextCompat.getDrawable(mContext, R.mipmap.like_primary_color));
                            } else {
                                likeImage.setBackground(ContextCompat.getDrawable(mContext, R.mipmap.like_primary_color));
                            }
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    try{
                        if (mNewsItem.getLikedCount()>0) {
                            likeCountTxt.setVisibility(View.VISIBLE);
                            likeCountTxt.setText(mNewsItem.getLikedCount().toString() + " Likes");
                        }else {
                            likeCountTxt.setVisibility(View.VISIBLE);
                            likeCountTxt.setText( "No Likes");
                        }
                    }catch (Exception e){
                        e.printStackTrace();
                    }

                    linkConnectionImage.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            listener.onPaginationClick(mNewsItem, click, position, "connection");
                        }
                    });
                    replyImage.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            listener.onPaginationClick(mNewsItem,click,position,"reply");
                        }
                    });
                    likeImage.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {


                            //likeImage.setBackground(click ?ContextCompat.getDrawable(mContext, R.drawable.like_true):ContextCompat.getDrawable(mContext, R.drawable.like));
                            if (userId.equals(mNewsItem.getUserId())){
                                if (premiumUser) {
                                    listener.onPaginationClick(mNewsItem, click, position, "likeCount");
                                }else {
                                    listener.onPaginationClick(mNewsItem,click,position,"premiumUpgrade");
                                }

                            }else {
                                listener.onPaginationClick(mNewsItem, click, position, "like");
                                final int sdk = android.os.Build.VERSION.SDK_INT;
                                if (click) {
                                    click = false;
                                    if (sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
                                        likeImage.setBackgroundDrawable(ContextCompat.getDrawable(mContext, R.mipmap.admin_post_like));
                                    } else {
                                        likeImage.setBackground(ContextCompat.getDrawable(mContext, R.mipmap.admin_post_like));
                                    }
                                } else {
                                    click = true;
                                    if (sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
                                        likeImage.setBackgroundDrawable(ContextCompat.getDrawable(mContext, R.mipmap.like_primary_color));
                                    } else {
                                        likeImage.setBackground(ContextCompat.getDrawable(mContext, R.mipmap.like_primary_color));
                                    }
                                }
                            }
                        }
                    });


                    itemView.setOnLongClickListener(new View.OnLongClickListener() {
                        @Override
                        public boolean onLongClick(View view) {
                            //utility.showToast(mContext,"Long Click");
                            if (!userId.equals(mNewsItem.getUserId()))
                                listener.onPaginationLongClick(mNewsItem, position,"user");
                            return false;
                        }
                    });


                    //newTitle.setText(mNewsItem.getCaption());




                }
            }catch (Exception e){
                e.printStackTrace();
            }
        }
    }


    @Override
    public int getItemCount() {
        try {
            return mNewsItemList == null ? 0 : mNewsItemList.size();
        }catch (Exception e){
            e.printStackTrace();
            return 0;
        }
    }

    @Override
    public int getItemViewType(int position) {
        UserViewPost viewPost=mNewsItemList.get(position);

        return VIEW_TYPE_USER;


    }

    public void add(UserViewPost response) {
        mNewsItemList.add(response);
        notifyItemInserted(mNewsItemList.size() - 1);
    }
    public void addAll(List<UserViewPost> postItems) {
        for (UserViewPost response : postItems) {
            add(response);
        }
    }
    private void remove(UserViewPost postItems) {
        int position = mNewsItemList.indexOf(postItems);
        if (position > -1) {
            mNewsItemList.remove(position);
            notifyItemRemoved(position);
        }
    }

    public void clear() {
        try {
            while (getItemCount() > 0) {
                remove(getItem(0));
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }
    UserViewPost getItem(int position) {
        return mNewsItemList.get(position);

    }

}
