package com.chetaru.FreshersApp.view.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.chetaru.FreshersApp.CustomImage.RoundRectCornerImageView;
import com.chetaru.FreshersApp.R;
import com.chetaru.FreshersApp.service.model.TodaysLike;
import com.chetaru.FreshersApp.utility.SessionParam;
import com.chetaru.FreshersApp.utility.Utility;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class TodayLikeAdapter extends RecyclerView.Adapter<TodayLikeAdapter.MyViewHolder> {

    List<TodaysLike> list = new ArrayList<>();
    private List<TodaysLike> mFilteredList=new ArrayList<>();
    private Context context;
    private boolean isLoadingAdded = false;
    // View Types
    private static final int ITEM = 0;
    private static final int LOADING = 1;
    private boolean retryPageLoad = false;
    private String errorMsg;
    Utility utility;
    SessionParam sessionParam;
    int likeCount;
    todayLikeListener mListener;

    public TodayLikeAdapter(List<TodaysLike> todayList,Context context,int likeCount,todayLikeListener mListener){
        this.mFilteredList=todayList;
        this.list=todayList;
        this.context=context;
        this.likeCount=likeCount;
        notifyDataSetChanged();
        utility=new Utility();
        sessionParam=new SessionParam(context);
        this.mListener=mListener;

    }

    public void setCount(int likeCount) {
        this.likeCount=likeCount;
        notifyDataSetChanged();
    }

    public class MyViewHolder  extends RecyclerView.ViewHolder{
        RoundRectCornerImageView myUploadImage;
        TextView likeCount;
        LinearLayout likeLayout;
        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            myUploadImage=itemView.findViewById(R.id.my_upload_image);
            likeLayout=itemView.findViewById(R.id.like_count_layout);
            likeCount=itemView.findViewById(R.id.my_upload_like_count_text);
        }
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        MyViewHolder viewHolder=null;
        LayoutInflater layoutInflater=LayoutInflater.from(parent.getContext());
        switch (viewType){
            case ITEM:
                View view=layoutInflater.inflate(R.layout.today_like_list,parent,false);
                viewHolder=new MyViewHolder(view);
                break;
            case LOADING:
                View viewLoading=layoutInflater.inflate(R.layout.item_progress,parent,false);
                viewHolder=new LoadingVH(viewLoading);
                break;
        }
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {

        switch (getItemViewType(position)){
            case ITEM:
                holder.itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        mListener.todayLikeImage(mFilteredList.get(position).getProfileImage(),
                                mFilteredList.get(position).getPostId());
                    }
                });
                try {


                    //Picasso.with(context).load(mFilteredList.get(position)).into(holder.myUploadImage);
                    Glide.with(context).load(mFilteredList.get(position).getProfileImage()).placeholder(R.drawable.profile_pics).into(holder.myUploadImage);
                    holder.likeCount.setText(String.valueOf(likeCount));

                }catch (Exception e){
                    e.printStackTrace();
                }

                break;
            case LOADING:
                LoadingVH loadingVH= (LoadingVH) holder;
                if (retryPageLoad) {
                    loadingVH.mErrorLayout.setVisibility(View.VISIBLE);
                    loadingVH.mProgressBar.setVisibility(View.GONE);

                    loadingVH.mErrorTxt.setText(
                            errorMsg != null ?
                                    errorMsg :
                                    context.getString(R.string.error_msg_unknown));

                } else {
                   /* if(mFilteredList.get(mFilteredList.size()-1).getSelected()==null){
                        loadingVH.mErrorLayout.setVisibility(View.GONE);
                        loadingVH.mProgressBar.setVisibility(View.GONE);
                    }else{
                        loadingVH.mErrorLayout.setVisibility(View.GONE);
                        loadingVH.mProgressBar.setVisibility(View.VISIBLE);
                    }*/
                }
                break;
        }
    }

    @Override
    public int getItemCount() {
        return mFilteredList == null ? 0 : mFilteredList.size();
    }




    public void add(TodaysLike r) {
            mFilteredList.add(r);
            notifyItemInserted(mFilteredList.size() - 1);

    }

    public void addAll(List<TodaysLike> viewPost_Results) {
        for (TodaysLike result : viewPost_Results) {
            add(result);
        }
    }
    public void remove(TodaysLike r) {
        int position = mFilteredList.indexOf(r);
        if (position > -1) {
            mFilteredList.remove(position);
            notifyItemRemoved(position);
        }
    }
    public void clear() {
        try {
            while (getItemCount() > 0) {
                remove(getItem(0));
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public void addLoadingFooter() {
        isLoadingAdded = true;
        add(new TodaysLike());
    }

    public void removeLoadingFooter() {
        isLoadingAdded = false;

        int position = mFilteredList.size() - 1;
        TodaysLike result = getItem(position);

        if (result != null) {
            mFilteredList.remove(position);
            notifyItemRemoved(position);
        }
    }

    protected class LoadingVH extends MyViewHolder implements View.OnClickListener {
        private ProgressBar mProgressBar;
        private ImageButton mRetryBtn;
        private TextView mErrorTxt;
        private LinearLayout mErrorLayout;

        public LoadingVH(View itemView) {
            super(itemView);
            mProgressBar = itemView.findViewById(R.id.loadmore_progress);
            mRetryBtn = itemView.findViewById(R.id.loadmore_retry);
            mErrorTxt = itemView.findViewById(R.id.loadmore_errortxt);
            mErrorLayout = itemView.findViewById(R.id.loadmore_errorlayout);
            mRetryBtn.setOnClickListener(this);
            mErrorLayout.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.loadmore_retry:
                case R.id.loadmore_errorlayout:
                    showRetry(false, null);
                    // mCallback.retryPageLoad();
                    break;
            }
        }
    }

    public void showRetry(boolean show, @Nullable String errorMsg) {
        retryPageLoad = show;
        notifyItemChanged(mFilteredList.size() - 1);

        if (errorMsg != null) this.errorMsg = errorMsg;
    }

    @Override
    public int getItemViewType(int position) {
        return (position == mFilteredList.size() - 1 && isLoadingAdded) ? LOADING : ITEM;
//        if(mFilteredList.size()<5){
//            return mFilteredList.size() - 1;
//        }else{
//            return (position == mFilteredList.size() - 1 && isLoadingAdded) ? LOADING : ITEM;
//        }
    }

    public TodaysLike getItem(int position) {
        return mFilteredList.get(position);
    }

    public interface todayLikeListener{
        public void todayLikeImage(String images,int postId);
    }

}
