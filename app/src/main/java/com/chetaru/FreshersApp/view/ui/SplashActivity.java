package com.chetaru.FreshersApp.view.ui;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import com.chetaru.FreshersApp.R;

import com.chetaru.FreshersApp.utility.SessionParam;
import com.chetaru.FreshersApp.utility.Validations;
import com.chetaru.FreshersApp.view.ui.Activity.HomeActivity;
import com.chetaru.FreshersApp.view.ui.Activity.RootActivity;

public class SplashActivity extends AppCompatActivity {
    private static int SPLASH_TIME_OUT = 3000;
    SessionParam sessionParam;
    String tokenId="";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        sessionParam = new SessionParam(this);
        tokenId=sessionParam.token;
        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                if (Validations.isEmptyString(tokenId)) {
                    // This method will be executed once the timer is over
                    // Start your app main activity
                    Intent i = new Intent(SplashActivity.this, RootActivity.class);
                    startActivity(i);
                    // close this activity
                    finish();
                }else{
                    Intent i=new Intent(SplashActivity.this, HomeActivity.class);
                    startActivity(i);
                    finish();
                }
            }
        }, SPLASH_TIME_OUT);

    }
}
