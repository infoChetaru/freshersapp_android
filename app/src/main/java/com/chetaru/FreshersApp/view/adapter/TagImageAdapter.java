package com.chetaru.FreshersApp.view.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;
import com.chauthai.swipereveallayout.SwipeRevealLayout;
import com.chetaru.FreshersApp.R;
import com.chetaru.FreshersApp.service.model.UserViewPost;
import com.chetaru.FreshersApp.utility.SessionParam;
import com.chetaru.FreshersApp.utility.Utility;
import com.chetaru.FreshersApp.view.customView.SquareImageView;
import com.jackandphantom.blurimage.BlurImage;

import java.util.ArrayList;
import java.util.List;

public class TagImageAdapter extends RecyclerView.Adapter<TagImageAdapter.ViewHolder> {

    List<UserViewPost> list = new ArrayList<>();
    private List<UserViewPost> mFilteredList;
    private Context context;
    private boolean isLoadingAdded = false;
    // View Types
    private static final int ITEM = 0;
    private static final int LOADING = 1;
    private boolean retryPageLoad = false;
    private String errorMsg;
    public TagImageAdapterCallback mListener;
    Utility utility;
    SessionParam sessionParam;

    public static class ViewHolder extends RecyclerView.ViewHolder {
        SquareImageView iv_logo;
        SquareImageView tagSecondImage,tagThirdTmage;
        TextView tagMsgTxt;
        public ViewHolder(View v) {
            super(v);
            iv_logo = v.findViewById(R.id.tag_image_list);
            tagSecondImage = v.findViewById(R.id.tag_upload_image_sceond);
            tagThirdTmage = v.findViewById(R.id.tag_upload_image_third);
            tagMsgTxt = v.findViewById(R.id.tag_post_msg_view);

        }
    }

    public TagImageAdapter(List<UserViewPost> list, Context context,TagImageAdapterCallback mListener) {
        this.list = list;
        this.mFilteredList = list;
        notifyDataSetChanged();
        this.context = context;
        this.mListener=mListener;
        utility=new Utility();
        sessionParam = new SessionParam(context);
    }

    public TagImageAdapter(Context context) {
        this.list = new ArrayList<>();
        this.mFilteredList = list;
        this.context = context;
        sessionParam = new SessionParam(context);
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ViewHolder viewHolder=null;
        LayoutInflater inflater=LayoutInflater.from(parent.getContext());
        switch (viewType){
            case ITEM:
                View viewItem=inflater.inflate(R.layout.tag_image_list,parent,false);
                viewHolder=new ViewHolder(viewItem);
                break;
            case LOADING:
                View viewLoading=inflater.inflate(R.layout.item_progress,parent,false);
                viewHolder=new LoadingVH(viewLoading);
                break;
        }
        return viewHolder;
    }


    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        switch (getItemViewType(position)) {

            case ITEM:
                UserViewPost viewPost = mFilteredList.get(position);

                /*Glide.with(context)
                        .load(viewPost.getImageUrl())
                        // .apply(RequestOptions.circleCropTransform())
                        .placeholder(R.drawable.profile)
                        .into(holder.iv_logo);*/


                if (viewPost.getPostExpired()&&!sessionParam.premieUser) {
                    holder.tagMsgTxt.setVisibility(View.VISIBLE);
                    holder.tagSecondImage.setVisibility(View.VISIBLE);
                    holder.tagThirdTmage.setVisibility(View.VISIBLE);
                    BlurImage.with(context).load(R.drawable.default_background).intensity(25).Async(true).into(holder.tagThirdTmage);
                    Thread thread = new Thread(new Runnable(){
                        @Override
                        public void run() {
                            Bitmap bitmapFromURL=Utility.getBitmapFromURL(viewPost.getImageUrl());
                           // String changeString=utility.BitMapToStringBlur(bitmapFromURL);
                           // Bitmap changeQulity=utility.convertBase64ToBitmap(changeString);
                            BlurImage.with(context).load(bitmapFromURL).intensity(25).Async(true).into(holder.tagSecondImage);
                           // BlurImage.with(context).load(changeQulity).intensity(25).Async(true).into(holder.iv_logo);
                        }
                    });

                    thread.start();

                }else {
                    holder.tagMsgTxt.setVisibility(View.GONE);
                    holder.tagSecondImage.setVisibility(View.GONE);
                    holder.tagThirdTmage.setVisibility(View.GONE);
                    Glide.with(context)
                            .asBitmap().load(viewPost.getImageUrl())
                            .into(new SimpleTarget<Bitmap>() {
                                @Override
                                public void onResourceReady(Bitmap resource, Transition<? super Bitmap> transition) {
                                    holder.iv_logo.setImageBitmap(resource);
                                }
                            });


                }
                holder.itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                       // mListener.onTagImageClick(mFilteredList, position);
                        if (!sessionParam.premieUser) {
                            if (viewPost.getPostExpired() ) {
                                mListener.onTagPremiumUser("   Upgrade To Premium To See This Post   ",
                                        "This post has expired from the feed.");
                            } else {
                                List<UserViewPost> newTagList = new ArrayList<>();
                                for (int i = 0; i < mFilteredList.size(); i++) {
                                    if (mFilteredList.get(i).getPostExpired() != null) {
                                        if (!mFilteredList.get(i).getPostExpired()) {
                                            newTagList.add(mFilteredList.get(i));
                                        }
                                    }
                                }
                                mListener.onTagImageClick(newTagList, position);
                            }
                        }else {
                            mListener.onTagImageClick(mFilteredList, position);
                        }

                    }
                });
              //  Glide.with(context).load(viewPost.getImageUrl()).apply(new RequestOptions().centerCrop().placeholder(R.drawable.profile_pics)).into(holder.iv_logo);

                break;
            case LOADING:
                LoadingVH loadingVH = (LoadingVH) holder;

                if (retryPageLoad) {
                    loadingVH.mErrorLayout.setVisibility(View.VISIBLE);
                    loadingVH.mProgressBar.setVisibility(View.GONE);

                    loadingVH.mErrorTxt.setText(
                            errorMsg != null ?
                                    errorMsg :
                                    context.getString(R.string.error_msg_unknown));

                } else {
                   /* if(mFilteredList.get(mFilteredList.size()-1).getSelected()==null){
                        loadingVH.mErrorLayout.setVisibility(View.GONE);
                        loadingVH.mProgressBar.setVisibility(View.GONE);
                    }else{
                        loadingVH.mErrorLayout.setVisibility(View.GONE);
                        loadingVH.mProgressBar.setVisibility(View.VISIBLE);
                    }*/
                }
                break;
        }

    }

    protected class LoadingVH extends ViewHolder implements View.OnClickListener {
        private ProgressBar mProgressBar;
        private ImageButton mRetryBtn;
        private TextView mErrorTxt;
        private LinearLayout mErrorLayout;

        public LoadingVH(View itemView) {
            super(itemView);
            mProgressBar = itemView.findViewById(R.id.loadmore_progress);
            mRetryBtn = itemView.findViewById(R.id.loadmore_retry);
            mErrorTxt = itemView.findViewById(R.id.loadmore_errortxt);
            mErrorLayout = itemView.findViewById(R.id.loadmore_errorlayout);
            mRetryBtn.setOnClickListener(this);
            mErrorLayout.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.loadmore_retry:
                case R.id.loadmore_errorlayout:
                    showRetry(false, null);
                   // mCallback.retryPageLoad();
                    break;
            }
        }
    }

    public void showRetry(boolean show, @Nullable String errorMsg) {
        retryPageLoad = show;
        notifyItemChanged(mFilteredList.size() - 1);

        if (errorMsg != null) this.errorMsg = errorMsg;
    }
    @Override
    public int getItemCount() {

        return mFilteredList == null ? 0 : mFilteredList.size();
    }

    public void add(UserViewPost r) {
        mFilteredList.add(r);
        notifyItemInserted(mFilteredList.size()-1);
    }

    public void addAll(List<UserViewPost> viewPost_Results) {
        for (UserViewPost result : viewPost_Results) {
            add(result);
        }
    }
    public void remove(UserViewPost r) {
        int position = mFilteredList.indexOf(r);
        if (position > -1) {
            mFilteredList.remove(position);
            notifyItemRemoved(position);
        }
    }

    public void addLoadingFooter() {
        isLoadingAdded = true;
        add(new UserViewPost());
    }

    public void removeLoadingFooter() {
        isLoadingAdded = false;

        int position = mFilteredList.size() - 1;
        UserViewPost result = getItem(position);

        if (result != null) {
            mFilteredList.remove(position);
            notifyItemRemoved(position);
        }
    }


    @Override
    public int getItemViewType(int position) {
        return (position == mFilteredList.size() - 1 && isLoadingAdded) ? LOADING : ITEM;
//        if(mFilteredList.size()<5){
//            return mFilteredList.size() - 1;
//        }else{
//            return (position == mFilteredList.size() - 1 && isLoadingAdded) ? LOADING : ITEM;
//        }
    }

    public UserViewPost getItem(int position) {
        return mFilteredList.get(position);
    }

    public interface TagImageAdapterCallback {
        void onTagImageClick(List<UserViewPost> tagPeople, int position);
        void onTagPremiumUser(String title,String text);
    }


}
