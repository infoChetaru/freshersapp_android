package com.chetaru.FreshersApp.view.customView;

import android.content.Context;
import android.graphics.Bitmap;
import android.renderscript.Allocation;
import android.renderscript.Element;
import android.renderscript.RenderScript;
import android.renderscript.ScriptIntrinsicBlur;

import androidx.annotation.NonNull;

import com.bumptech.glide.load.engine.bitmap_recycle.BitmapPool;
import com.bumptech.glide.load.resource.bitmap.BitmapTransformation;

import java.security.MessageDigest;

public class BlurTransformation extends BitmapTransformation {
    private RenderScript rs;

    public BlurTransformation(Context context) {
        super();

        rs = RenderScript.create(context);
    }
    @Override
    protected Bitmap transform(@NonNull BitmapPool pool, @NonNull Bitmap toTransform, int outWidth, int outHeight) {
        Bitmap blurredBitmap = toTransform.copy(Bitmap.Config.ARGB_8888, true);

        // Allocate memory for Renderscript to work with
        Allocation input = Allocation.createFromBitmap(rs, blurredBitmap, Allocation.MipmapControl.MIPMAP_FULL, Allocation.USAGE_SHARED);
        Allocation output = Allocation.createTyped(rs, input.getType());

        // Load up an instance of the specific script that we want to use.
        ScriptIntrinsicBlur script = ScriptIntrinsicBlur.create(rs, Element.U8_4(rs));
        script.setInput(input);

        // Set the blur radius
        script.setRadius(10);

        // Start the ScriptIntrinisicBlur
        script.forEach(output);

        // Copy the output to the blurred bitmap
        output.copyTo(blurredBitmap);

        return blurredBitmap;

    }

    @Override
    public void updateDiskCacheKey(@NonNull MessageDigest messageDigest) {
        messageDigest.update("blur transformation".getBytes());
    }

    /**
     *  @Override
     *     public Bitmap transform(Bitmap bitmap) {
     *         bitmap = cropBitmapWidthToMultipleOfFour(bitmap);
     *         Bitmap argbBitmap = convertBitmap(bitmap, Config.ARGB_8888);
     *
     *         Bitmap blurredBitmap = Bitmap.createBitmap(argbBitmap.getWidth(), argbBitmap.getHeight(),
     *                 Bitmap.Config.ARGB_8888);
     *         // Initialize RenderScript and the script to be used
     *         RenderScript renderScript = RenderScript.create(context);
     *         ScriptIntrinsicBlur script = ScriptIntrinsicBlur
     *                 .create(renderScript, Element.U8_4(renderScript));
     *         // Allocate memory for Renderscript to work with
     *         Allocation input = Allocation.createFromBitmap(renderScript, argbBitmap);
     *         Allocation output = Allocation.createFromBitmap(renderScript, blurredBitmap);
     *
     *         script.setInput(input);
     *         script.setRadius(radius);
     *         script.forEach(output);
     *         output.copyTo(blurredBitmap);
     *
     *         renderScript.destroy();
     *         argbBitmap.recycle();
     *         return blurredBitmap;
     *     }
     *
     *     @Override
     *     public String key() {
     *         return "blur(" + String.valueOf(radius) + ")";
     *     }
     *
     *     private static Bitmap cropBitmapWidthToMultipleOfFour(Bitmap bitmap) {
     *         int bitmapWidth = bitmap.getWidth();
     *         if (bitmapWidth % 4 != 0) {
     *             // This is the place to actually crop the bitmap,
     *             // but I don't have the necessary method in this demo project
     *
     *             // bitmap = BitmapUtils.resize(bitmap, bitmapWidth - (bitmapWidth % 4), bitmap.getHeight(),
     *             //         ScaleType.CENTER);
     *         }
     *         return bitmap;
     *     }
     *
     *     private static Bitmap convertBitmap(Bitmap bitmap, Config config) {
     *         if (bitmap.getConfig() == config) {
     *             return bitmap;
     *         } else {
     *             Bitmap argbBitmap;
     *             argbBitmap = bitmap.copy(config, false);
     *             bitmap.recycle();
     *             if (argbBitmap == null) {
     *                 throw new UnsupportedOperationException(
     *                         "Couldn't convert bitmap from config " + bitmap.getConfig() + " to "
     *                                 + config);
     *             }
     *             return argbBitmap;
     *         }
     *     }
     * */
}
