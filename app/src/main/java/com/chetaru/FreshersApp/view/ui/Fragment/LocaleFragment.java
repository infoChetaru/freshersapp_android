package com.chetaru.FreshersApp.view.ui.Fragment;

import android.content.Context;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.LocaleList;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.SearchView;

import com.chetaru.FreshersApp.R;
import com.chetaru.FreshersApp.service.model.LocaleData;
import com.chetaru.FreshersApp.service.retrofit.BaseRequest;
import com.chetaru.FreshersApp.service.retrofit.Functions;
import com.chetaru.FreshersApp.service.retrofit.RequestReciever;
import com.chetaru.FreshersApp.utility.SessionParam;
import com.chetaru.FreshersApp.utility.Utility;
import com.chetaru.FreshersApp.utility.Validations;
import com.chetaru.FreshersApp.view.adapter.LocaleAdapter;
import com.chetaru.FreshersApp.view.customView.EqualSpacingItemDecoration;
import com.chetaru.FreshersApp.view.customView.ItemOffsetDecoration;
import com.chetaru.FreshersApp.view.ui.Activity.HomeActivity;
import com.google.gson.Gson;
import com.google.gson.JsonObject;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link LocaleFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class LocaleFragment extends Fragment {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    Utility utility;
    SessionParam sessionParam;
    //assign a layout id class
    private Unbinder unbinder;
    private String userId;

    private BaseRequest baseRequest;
    List<LocaleData> localeList;

    LocaleListener mListener;
    LocaleAdapter mAdapter;


    @BindView(R.id.no_locale_layout)
    LinearLayout noLocaleLayout;
    @BindView(R.id.recycler_view_layout)
    LinearLayout recyclerLayout;
    @BindView(R.id.locale_mRecyclerView)
    RecyclerView localeRecycler;
    @BindView(R.id.locale_search_view)
    SearchView likeSearchView;
    String defaultLocaleName="";

    public LocaleFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment LocaleFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static LocaleFragment newInstance(String param1, String param2) {
        LocaleFragment fragment = new LocaleFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_locale, container, false);
        unbinder = ButterKnife.bind(this, view);
        utility=new Utility();
        sessionParam=new SessionParam(getContext());
        /*if (!Validations.isEmptyString(mParam1)){
            defaultLocaleName=mParam1;
        }else {
            getDefaultLocale();

        }*/
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        localeRecycler.setLayoutManager(linearLayoutManager);
        localeRecycler.setItemAnimator(new DefaultItemAnimator());
        //localeRecycler.addItemDecoration(new ItemOffsetDecoration(getContext(), R.dimen.item_offset));
        getLocale(defaultLocaleName);
        search(likeSearchView);
        return  view;
    }

    private void search(SearchView likeSearchView) {
        likeSearchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {
               mAdapter.getFilter().filter(s);
                return true;
            }
        });
    }
    private void getDefaultLocale() {
        baseRequest = new BaseRequest(getContext());
        baseRequest.setBaseRequestListner(new RequestReciever() {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onSuccess(int requestCode, String Json, Object object) {
                try {

                    Gson gson = new Gson();
                    JSONObject jsonObject = new JSONObject(Json);
                    defaultLocaleName=jsonObject.getString("data");
                    String messsage=jsonObject.getString("message");
                    if (Validations.isEmptyString(defaultLocaleName)) {
                      //  utility.showToast(getContext(), messsage);
                    }
                    getLocale(defaultLocaleName);



                }catch (Exception e){
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int requestCode, String errorCode, String message) {
                //utility.showToast(getBaseContext(),message);
            }

            @Override
            public void onNetworkFailure(int requestCode, String message) {

                utility.showToast(getContext(),message);
            }
        });

        JsonObject object = Functions.getClient().getJsonMapObject(
                "","");
        baseRequest.callAPIPostWOLoader(1, object, getString(R.string.api_getDefaultLocale));

    }
    private void getLocale(String defaultLocaleName) {

        baseRequest = new BaseRequest(getContext());
        baseRequest.setBaseRequestListner(new RequestReciever() {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onSuccess(int requestCode, String Json, Object object) {
                try {

                    Gson gson = new Gson();
                    JSONObject jsonObject = new JSONObject(Json);
                    //JSONObject response= (JSONObject) jsonObject.get("data");
                    JSONArray jsonArray = new JSONArray(object.toString());
                    localeList = baseRequest.getDataList(jsonArray, LocaleData.class);
                    mAdapter=new LocaleAdapter(defaultLocaleName,localeList, getContext(), new LocaleAdapter.LocaleListener() {
                        @Override
                        public void updateLocale(LocaleData locale) {
                            updateSelectLocale(defaultLocaleName,locale);
                        }
                    });
                    localeRecycler.setAdapter(mAdapter);

                }catch (Exception e){
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int requestCode, String errorCode, String message) {
                //utility.showToast(getBaseContext(),message);
            }

            @Override
            public void onNetworkFailure(int requestCode, String message) {

                utility.showToast(getContext(),message);
            }
        });

        JsonObject object = Functions.getClient().getJsonMapObject(
                "","");
        baseRequest.callAPIPostWOLoader(1, object, getString(R.string.api_getAllLocales));

    }

    //Update default Locale
    private void updateSelectLocale(String defaultLocaleName, LocaleData locale) {

        baseRequest = new BaseRequest(getContext());
        baseRequest.setBaseRequestListner(new RequestReciever() {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onSuccess(int requestCode, String Json, Object object) {
                try {

                    Gson gson = new Gson();
                    JSONObject jsonObject = new JSONObject(Json);
                    //JSONObject response= (JSONObject) jsonObject.get("data");
                   // getDefaultLocale();
                    sessionParam=new SessionParam(getContext(),locale);
                    mListener.localeBackClick();


                }catch (Exception e){
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int requestCode, String errorCode, String message) {
                //utility.showToast(getBaseContext(),message);
            }

            @Override
            public void onNetworkFailure(int requestCode, String message) {

                utility.showToast(getContext(),message);
            }
        });

        JsonObject main_object = new JsonObject();
        main_object.addProperty("localeId", locale.getId());
        baseRequest.callAPIPostWOLoader(1, main_object, getString(R.string.api_updateUsersLocale));

    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof LocaleListener) {
            mListener = (LocaleListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement TagChildFragment");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }
    @Override public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }
    public interface LocaleListener{
        public void localeBackClick();
    }

}