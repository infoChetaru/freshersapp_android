package com.chetaru.FreshersApp.view.ui.Fragment;

import android.content.Context;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.chetaru.FreshersApp.R;
import com.chetaru.FreshersApp.service.model.NotificationList;
import com.chetaru.FreshersApp.service.model.UserViewPost;
import com.chetaru.FreshersApp.service.retrofit.BaseRequest;
import com.chetaru.FreshersApp.service.retrofit.Functions;
import com.chetaru.FreshersApp.service.retrofit.RequestReciever;
import com.chetaru.FreshersApp.utility.SessionParam;
import com.chetaru.FreshersApp.utility.Utility;
import com.chetaru.FreshersApp.view.adapter.TagImageAdapter;
import com.chetaru.FreshersApp.view.customView.GridSpacingItemDecoration;
import com.chetaru.FreshersApp.view.customView.ItemOffsetDecoration;
import com.google.gson.Gson;
import com.google.gson.JsonObject;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link TagImageFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class TagImageFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    private TagChildFragment mListener;


    // TODO: Rename and change types of parameters
    private Integer tagId;
    private String mParam2;
    TextView titleMsg;
    RecyclerView tagImageRecyclerView;
    LinearLayoutManager layoutManager;
    TagImageAdapter tagAdapter;
    List<UserViewPost> tagList;
    Utility utility;
    SessionParam sessionParam;
    String tokenId = "";
    BaseRequest baseRequest;
    private static final int PAGE_START = 1;
    private boolean isLoading = false;
    private int currentPage = PAGE_START;
    private static int TOTAL_PAGES = 1;
    private boolean isLastPage = false;

    ImageView backImage;
    LinearLayout recyclerLayout,noImageLayout;
    public TagImageFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment TagImageFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static TagImageFragment newInstance(Integer param1, String param2) {
        TagImageFragment fragment = new TagImageFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            tagId = getArguments().getInt(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_tag_image, container, false);
        utility=new Utility();
        sessionParam=new SessionParam(getContext());
        tagList=new ArrayList<>();
        initView(view);

        return view;
    }

    private void initView(View view) {
        titleMsg=view.findViewById(R.id.tag_title_txt);
        tagImageRecyclerView=view.findViewById(R.id.tag_image_recyclerview);
        backImage=view.findViewById(R.id.back_image);
        recyclerLayout=view.findViewById(R.id.recycler_layout);
        noImageLayout=view.findViewById(R.id.no_image_layout);

        titleMsg.setText("# "+mParam2);
        backImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
            /* FragmentManager fm = getActivity().getSupportFragmentManager();
                for(int i = 0; i < fm.getBackStackEntryCount(); ++i) {
                    fm.popBackStack();
                }*/

            }
        });


        //GridSpacingItemDecoration decoration=new GridSpacingItemDecoration(3,R.dimen.item_offset ,true,getActivity());

        tagAdapter = new TagImageAdapter(tagList, getContext(),new  TagImageAdapter.TagImageAdapterCallback() {
            @Override
            public void onTagImageClick(List<UserViewPost> tagImages, int position) {
                mListener.hashTagImageClick(tagImages,position);
            }

            @Override
            public void onTagPremiumUser(String title, String text) {
                mListener.onTagClickPremium(tagId,mParam2,title,text);
            }
        });
        tagImageRecyclerView.setAdapter(tagAdapter);
        layoutManager = new GridLayoutManager(getContext(),3);
        tagImageRecyclerView.setLayoutManager(layoutManager);
        tagImageRecyclerView.setHasFixedSize(true);
        ItemOffsetDecoration itemDecoration = new ItemOffsetDecoration(getActivity(), R.dimen.item_offset);
        tagImageRecyclerView.addItemDecoration(itemDecoration);
        //Visible or hide a layout
        noImageLayout.setVisibility(View.VISIBLE);
        recyclerLayout.setVisibility(View.GONE);

        //tagAdapter.notifyDataSetChanged();
        api_tagImageList(tagId);
        tagImageRecyclerView.addOnScrollListener(new PaginationScrollList(layoutManager) {
            @Override
            protected void loadMoreItems() {
                isLoading = true;
                currentPage += 1;
                api_loadnextPage(tagId);
            }

           /* @Override
            public int getTotalPageCount() {
                //Toast.makeText(mContext, "getTotalPageCount", Toast.LENGTH_SHORT).show();
                return TOTAL_PAGES;
            }*/

            @Override
            public boolean isLastPage() {
                //Toast.makeText(mContext, "isLastPage", Toast.LENGTH_SHORT).show();
                return isLastPage;
            }

            @Override
            public boolean isLoading() {
                //Toast.makeText(mContext, "isLoading", Toast.LENGTH_SHORT).show();
                return isLoading;
            }
        });

    }

    private void api_tagImageList(Integer tagId) {
        currentPage = PAGE_START;
        baseRequest = new BaseRequest(getContext());
        baseRequest.setBaseRequestListner(new RequestReciever() {
            @Override
            public void onSuccess(int requestCode, String Json, Object object) {
                tagList.clear();
                try {

                    //JSONObject jsonObject = new JSONObject(object);
                   // totalPage = jsonObject.optInt("totalPageCount");
                    JSONObject jsonObject = new JSONObject(Json);
                    TOTAL_PAGES = jsonObject.optInt("totalPageCount");
                    String  getCurrentpage=jsonObject.optString("currentPage");
                    //JSONArray jsonArray = new JSONArray(object.toString());

                    JSONArray jsonArray = new JSONArray(object.toString());
                    tagList = baseRequest.getDataList(jsonArray, UserViewPost.class);
                    // JSONObject jsonObject = new JSONObject(Json);
                    /*for (int i=0;i<10;i++){
                        UserViewPost viewPost=new UserViewPost();
                        viewPost.setImageUrl("https://production.chetaru.co.uk/freshersApp/public/uploads/postImages/post_5eaa73782290c1588228984.png");
                    tagList.add(viewPost);
                    }*/
                    if (tagList.size()>0) {
                        noImageLayout.setVisibility(View.GONE);
                        recyclerLayout.setVisibility(View.VISIBLE);
                        //notiList = baseRequest.getDataList(jsonArray, NotificationList.class);
                        tagAdapter = new TagImageAdapter(tagList, getContext(), new TagImageAdapter.TagImageAdapterCallback() {
                            @Override
                            public void onTagImageClick(List<UserViewPost> tagImages, int position) {
                                mListener.hashTagImageClick(tagImages,position);
                            }

                            @Override
                            public void onTagPremiumUser(String title, String text) {
                                mListener.onTagClickPremium(tagId,mParam2,title,text);
                            }
                        });
                        tagImageRecyclerView.setAdapter(tagAdapter);
                        //tagAdapter.addAll(tagList);
                        // tagAdapter.notifyDataSetChanged();

                        if (currentPage < TOTAL_PAGES)
                            tagAdapter.addLoadingFooter();
                        else
                            isLastPage = true;

                    }else {
                        noImageLayout.setVisibility(View.VISIBLE);
                        recyclerLayout.setVisibility(View.GONE);
                    }




                }catch (Exception e){
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int requestCode, String errorCode, String message) {
                //utility.showToast(getContext(),message);
                noImageLayout.setVisibility(View.VISIBLE);
                recyclerLayout.setVisibility(View.GONE);
            }

            @Override
            public void onNetworkFailure(int requestCode, String message) {

                utility.showToast(getContext(),message);
            }
        });


        JsonObject main_object = new JsonObject();
        main_object.addProperty("hashTagId", tagId+"");
        main_object.addProperty("page", currentPage);
       /* JsonObject object = Functions.getClient().getJsonMapObject(
                "postId",
                "likeStatus",status );*/
        baseRequest.callAPIPost(1, main_object, getString(R.string.api_hashTagUser));

    }
    public void api_loadnextPage(int tagId) {
        baseRequest = new BaseRequest(getContext());
        baseRequest.setBaseRequestListner(new RequestReciever() {
            @Override
            public void onSuccess(int requestCode, String Json, Object object) {
                Gson gson = new Gson();
                try {

                    JSONObject jsonObject = new JSONObject(Json);
                    TOTAL_PAGES = jsonObject.optInt("totalPageCount");
                    JSONArray jsonArray = new JSONArray(object.toString());
                    tagList = baseRequest.getDataList(jsonArray, UserViewPost.class);
                    // utility.showToast(getContext(),object.toString());

                    /*JSONArray jsonArray = new JSONArray(object.toString());
                    list = baseRequest.getDataList(jsonArray, UserList.class);*/

                    //ad_notificationListP = new Ad_NotificationList_pagination(rv_list,list, mContext);
                    tagAdapter.removeLoadingFooter();
                    isLoading = false;
                    tagAdapter.addAll(tagList);
                    //rv_list_archive.setAdapter(ad_notificationList);
                    if (currentPage != TOTAL_PAGES) tagAdapter.addLoadingFooter();
                    else isLastPage = true;

                    //rv_list_archive.setAdapter(ad_notificationListP);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int requestCode, String errorCode, String message) {
                //errorLayout.showError(message);
                utility.showToast(getContext(), message);
            }

            @Override
            public void onNetworkFailure(int requestCode, String message) {
                //errorLayout.showError(message);
                utility.showToast(getContext(), message);
            }
        });

        JsonObject main_object = new JsonObject();
        main_object.addProperty("hashTagId", tagId+"");
        main_object.addProperty("page", currentPage);
       /* JsonObject object = Functions.getClient().getJsonMapObject(
                "postId",
                "likeStatus",status );*/
        baseRequest.callAPIPost(1, main_object, getString(R.string.api_hashTagUser));
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof TagChildFragment) {
            mListener = (TagChildFragment) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement TagChildFragment");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }


    public abstract class PaginationScrollList extends RecyclerView.OnScrollListener {

        private LinearLayoutManager layoutManager;

        public PaginationScrollList(LinearLayoutManager layoutManager) {
            this.layoutManager = layoutManager;
        }

        @Override
        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
            super.onScrolled(recyclerView, dx, dy);

            int visibleItemCount = layoutManager.getChildCount();
            int totalItemCount = layoutManager.getItemCount();
            int firstVisibleItemPosition = layoutManager.findFirstVisibleItemPosition();

            if (!isLoading() && !isLastPage()) {
                if ((visibleItemCount + firstVisibleItemPosition) >= totalItemCount
                        && firstVisibleItemPosition >= 0) {
                    loadMoreItems();
                }
            }
        }

        protected abstract void loadMoreItems();

        public abstract boolean isLastPage();

        public abstract boolean isLoading();

    }

    public interface TagChildFragment{
        public void hashTagImageClick(List<UserViewPost> postList,int position);
        public void onTagClickPremium(int tagId,String tagName,String title,String desc);
    }

}
