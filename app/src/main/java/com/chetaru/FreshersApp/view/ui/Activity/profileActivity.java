package com.chetaru.FreshersApp.view.ui.Activity;

import androidx.annotation.IdRes;
import androidx.annotation.NonNull;

import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import com.android.billingclient.api.BillingClient;
import com.android.billingclient.api.SkuDetails;
import com.chetaru.FreshersApp.R;
import com.chetaru.FreshersApp.service.retrofit.BaseRequest;
import com.chetaru.FreshersApp.service.retrofit.Functions;
import com.chetaru.FreshersApp.service.retrofit.RequestReciever;
import com.chetaru.FreshersApp.utility.BaseActivity;
import com.chetaru.FreshersApp.utility.MarshMallowPermission;
import com.chetaru.FreshersApp.utility.SessionParam;
import com.chetaru.FreshersApp.utility.Utility;
import com.chetaru.FreshersApp.utility.Validations;
import com.chetaru.FreshersApp.view.ui.Fragment.ConnRequestTabFragment;
import com.chetaru.FreshersApp.view.ui.Fragment.HomeFragment;
import com.chetaru.FreshersApp.view.ui.Fragment.NotificationsFragment;

import com.chetaru.FreshersApp.view.ui.Fragment.UploadFragment;
import com.chetaru.FreshersApp.view.ui.Fragment.newProfileFragment;
import com.chetaru.FreshersApp.view.ui.PreviewDemo;
import com.google.android.material.bottomnavigation.BottomNavigationItemView;
import com.google.android.material.bottomnavigation.BottomNavigationMenuView;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.gson.JsonObject;

import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import static android.Manifest.permission.ACCESS_COARSE_LOCATION;
import static android.Manifest.permission.ACCESS_FINE_LOCATION;

public class profileActivity extends BaseActivity {

    private ArrayList<String> permissionsToRequest;
    private ArrayList<String> permissionsRejected = new ArrayList<>();
    private ArrayList<String> permissions = new ArrayList<>();

    private final static int ALL_PERMISSIONS_RESULT = 101;
    private static final int REQUEST_CAMERA_CAPTURE = 1;
    private static final int REQUEST_CROP = 3;
    final private int REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS = 124;
    private String TAG = HomeActivity.class.getSimpleName();
    private final static String TAG_FRAGMENT = "TAG_FRAGMENT";

    private Bitmap bitmap = null;
    boolean notiBack=false;
    String imgBase64 = "";
    File sourceFile;
    Utility utility;
    SessionParam sessionParam;
    String tokenId = "";
    BaseRequest baseRequest;
    String FilePathString;
    Boolean cameraActive=false;
    MarshMallowPermission marshMallowPermission;
    BottomNavigationView bottomNavigation;
    Fragment fragment;
    TextView tv_notiCount,tv_chatCount;
    Context mContext;
    int countValue=0;
    List<SkuDetails> skuDetailsList;
    private BillingClient billingClient;
    List<SkuDetails> skuDetailsListGlobal;
    Activity activity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try {
            requestWindowFeature(Window.FEATURE_NO_TITLE);
            this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        }catch (Exception e){
            e.printStackTrace();
        }
        setContentView(R.layout.activity_profile);
        bottomNavigation = findViewById(R.id.nav_view);
        this.mContext=this;
        utility=new Utility();
        sessionParam=new SessionParam(this);
        marshMallowPermission = new MarshMallowPermission(this);
        //BottomNavigationViewHelper.disableShiftMode(bottomNavigation);
        activity=this;
        bottomNavigation.setOnNavigationItemSelectedListener(navigationItemSelectedListener);
        getBagesCount(bottomNavigation,mContext);

        try {

            BottomNavigationMenuView bottomNavigationMenuView =
                    (BottomNavigationMenuView) bottomNavigation.getChildAt(0);
            View v = bottomNavigationMenuView.getChildAt(3);

            BottomNavigationItemView itemView = (BottomNavigationItemView) v;
            View badge = LayoutInflater.from(this)
                    .inflate(R.layout.notification_bages, bottomNavigationMenuView, false);
            tv_notiCount = badge.findViewById(R.id.notification_badge);

           /* View badgeChat=LayoutInflater.from(this).inflate(R.layout.notification_bages,bottomNavigationMenuView,false);
            tv_chatCount=badgeChat.findViewById(R.id.notification_badge);
*/
            itemView.addView(badge);
            // itemView.addView(badgeChat);

            tv_notiCount.setVisibility(View.GONE);
            //sceond Bage for chat Count

            //sceondView.addView(badgeChat);
            tv_chatCount.setVisibility(View.GONE);
            //showBadge(mContext,bottomNavigation,R.id.navigation_tag, countValue);

        }catch (Exception e){
            e.printStackTrace();
        }
        permission();
        permissions.add(ACCESS_FINE_LOCATION);
        permissions.add(ACCESS_COARSE_LOCATION);

        permissionsToRequest = findUnAskedPermissions(permissions);
        //get the permissions we have asked for before but are not granted..
        //we will store this in a global list to access later.


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (permissionsToRequest.size() > 0)
                requestPermissions(permissionsToRequest.toArray(new String[permissionsToRequest.size()]), ALL_PERMISSIONS_RESULT);
        }
    }
    BottomNavigationView.OnNavigationItemSelectedListener navigationItemSelectedListener =
            new BottomNavigationView.OnNavigationItemSelectedListener() {
                @Override public boolean onNavigationItemSelected(@NonNull MenuItem item) {

                    switch (item.getItemId()) {
                        case R.id.navigation_home:
                            // openFragment(HomeFragment);
                            fragment = new HomeFragment();
                            loadFragment(fragment);
                            //callImgeFragment();

                            return true;
                        case R.id.navigation_camera:
                            dispatchCameraIntent();
                            /*if(cameraActive){
                                callFragment(FilePathString);
                            }else {

                            }*/
                            /*fragment=new UploadFragment();
                            loadFragment(fragment);*/
                            /*Intent intent=new Intent(HomeActivity.this, UploadImageActivity.class);
                            startActivity(intent);*/
                            return true;
                        case R.id.navigation_profile:
                            // openFragment(NotificationFragment.newInstance("", ""));
                            fragment = new newProfileFragment();
                            loadFragment(fragment);
                            return true;

                        case R.id.navigation_notification:
                            // openFragment(NotificationFragment.newInstance("", ""));
                            fragment = new NotificationsFragment();
                            loadFragment(fragment);
                            return true;
                        case R.id.navigation_tag:
                            // openFragment(NotificationFragment.newInstance("", ""));
                            fragment = new ConnRequestTabFragment();
                            loadFragment(fragment);
                            return true;
                    }
                    return false;
                }
            };

    public static void showBadge(Context context, BottomNavigationView
            bottomNavigationView, @IdRes int itemId, int value) {
        removeBadge(bottomNavigationView, itemId,value);
        BottomNavigationItemView itemView = bottomNavigationView.findViewById(itemId);
        View badge = LayoutInflater.from(context).inflate(R.layout.notification_bages, bottomNavigationView, false);

        TextView text = badge.findViewById(R.id.notification_badge);
        text.setText(String.valueOf(value));
        itemView.addView(badge);
    }

    public static void removeBadge(BottomNavigationView bottomNavigationView, @IdRes int itemId,int value) {
        BottomNavigationItemView itemView = bottomNavigationView.findViewById(itemId);
        Log.d("TAG","id:"+itemId+"getChildCount"+itemView.getChildCount());
        if (itemView.getChildCount() == 1) {
            itemView.removeViewAt(4);
        }
        try {


            if (value <= 0) {
                itemView.removeViewAt(4);
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private void getBagesCount(BottomNavigationView bottomNavigation,Context mContext) {
        baseRequest = new BaseRequest(this);
        baseRequest.setBaseRequestListner(new RequestReciever() {
            @Override
            public void onSuccess(int requestCode, String Json, Object object) {
                try {

                    JSONObject jsonObject = new JSONObject(Json);

                    /*Integer count= jsonObject.getInt("data");
                    tv.setText(count);*/
                    int chatCount=0;
                    try {
                        chatCount = jsonObject.getInt("chatCount");
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                    int pendingCount=0;
                    try {
                        pendingCount = jsonObject.getInt("pendingRequestCount");
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                    tv_notiCount.setText("");

                    if (jsonObject.optInt("data") > 0) {
                        //tv_notiCount.setText(jsonObject.optInt("data") + "");
                        if(jsonObject.optInt("data") > 10){
                            tv_notiCount.setText(jsonObject.optInt("data") + "+");
                        }else {
                            tv_notiCount.setText(jsonObject.optInt("data") + "");

                        }
                        // BottomMenuHelper.showBadge(this, mBottomNavigationView, R.id.action_news, "1");

                        //api_notificationListUnread();
                        tv_notiCount.setVisibility(View.VISIBLE);
                    } else {
                        tv_notiCount.setVisibility(View.GONE);
//                        api_notificationListUnread();
                    }
                    if (chatCount>0||pendingCount>0){
                        countValue=chatCount+pendingCount;
                       /* if (countValue>10){
                            tv_chatCount.setText(countValue+"+");
                        }else {
                            tv_chatCount.setText(countValue);
                        }*/
                        showBadge(mContext,bottomNavigation,R.id.navigation_tag,countValue);
                    }else {

                        removeBadge(bottomNavigation,R.id.navigation_tag,countValue);
                    }
                    // utility.showToast(getContext(),object.toString());
                }catch (Exception e){
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int requestCode, String errorCode, String message) {
                //utility.showToast(getBaseContext(),message);
            }

            @Override
            public void onNetworkFailure(int requestCode, String message) {

                utility.showToast(getBaseContext(),message);
            }
        });



        JsonObject object = Functions.getClient().getJsonMapObject(
                "","");
        baseRequest.callAPIPostWOLoader(1, object, getString(R.string.api_unreadNotificationCount));

    }

    private ArrayList<String>   findUnAskedPermissions(ArrayList<String> wanted) {
        ArrayList<String> result = new ArrayList<String>();

        for (String perm : wanted) {
            if (!hasPermission(perm)) {
                result.add(perm);
            }
        }

        return result;
    }

    private boolean hasPermission(String permission) {
        if (canMakeSmores()) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                return (checkSelfPermission(permission) == PackageManager.PERMISSION_GRANTED);
            }
        }
        return true;
    }

    private boolean canMakeSmores() {
        return (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP_MR1);
    }
    /*request user for certain permission*/
    private void permission() {
        //datafinish = true;
        List<String> permissionsNeeded = new ArrayList<String>();
        final List<String> permissionsList = new ArrayList<String>();
        if (!addPermission(permissionsList, Manifest.permission.ACCESS_FINE_LOCATION))
            permissionsNeeded.add("GPS");
        if (!addPermission(permissionsList, Manifest.permission.ACCESS_NETWORK_STATE))
            permissionsNeeded.add("Network state");
        if (!addPermission(permissionsList, Manifest.permission.ACCESS_COARSE_LOCATION))
            permissionsNeeded.add("Location");
        if (!addPermission(permissionsList, Manifest.permission.INTERNET))
            permissionsNeeded.add("Internet");
        if (!addPermission(permissionsList, Manifest.permission.READ_PHONE_STATE))
            permissionsNeeded.add("phone status");
        if (!addPermission(permissionsList, Manifest.permission.CAMERA))
            permissionsNeeded.add("Camera");
        if (!addPermission(permissionsList, Manifest.permission.WRITE_EXTERNAL_STORAGE))
            permissionsNeeded.add("Write storage");
        if (permissionsList.size() > 0) {
            if (permissionsNeeded.size() > 0) {
                // Need Rationale
                String message = "You need to grant access to " + permissionsNeeded.get(0);
                for (int i = 1; i < permissionsNeeded.size(); i++)
                    message = message + ", " + permissionsNeeded.get(i);
                /*showMessageOKCancel(message, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            requestPermissions(permissionsList.toArray(new String[permissionsList.size()]), REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS);
                        }
                    }
                });*/
                return;
            }
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermissions(permissionsList.toArray(new String[permissionsList.size()]), REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS);
            }
            return;
        }
        //init();
    }

    private boolean addPermission(List<String> permissionsList, String permission) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(permission) != PackageManager.PERMISSION_GRANTED) {
                permissionsList.add(permission);
                //Check for Rationale Optiong
                if (!shouldShowRequestPermissionRationale(permission))
                    return false;
            }
        }
        return true;
    }


    @TargetApi(Build.VERSION_CODES.M)
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {

        switch (requestCode) {

            case ALL_PERMISSIONS_RESULT:
                for (String perms : permissionsToRequest) {
                    if (!hasPermission(perms)) {
                        permissionsRejected.add(perms);
                    }
                }

                if (permissionsRejected.size() > 0) {


                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        if (shouldShowRequestPermissionRationale(permissionsRejected.get(0))) {
                            showMessageOKCancel("These permissions are mandatory for the application. Please allow access.",
                                    new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                                requestPermissions(permissionsRejected.toArray(new String[permissionsRejected.size()]), ALL_PERMISSIONS_RESULT);
                                            }
                                        }
                                    });
                            return;
                        }
                    }

                }

                break;
        }

    }

    private void showMessageOKCancel(String message, DialogInterface.OnClickListener okListener) {
        new AlertDialog.Builder(profileActivity.this)
                .setMessage(message)
                .setPositiveButton("OK", okListener)
                .setNegativeButton("Cancel", null)
                .create()
                .show();
    }

    /**
     * loading fragment into FrameLayout
     *
     * @param fragment
     */
    private void loadFragment(Fragment fragment) {
        // load fragment
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.nav_host_fragment, fragment, TAG_FRAGMENT);
        transaction.addToBackStack(null);
        transaction.commit();
    }

    /**
     * open a camera
     */
    private void dispatchCameraIntent() {

        if ((Build.VERSION.SDK_INT >= 23) && ContextCompat.checkSelfPermission(this,
                Manifest.permission.CAMERA)
                != PackageManager.PERMISSION_GRANTED) {

            // No explanation needed, we can request the permission.
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermissions(new String[]{Manifest.permission.CAMERA},
                        REQUEST_CAMERA_CAPTURE);
            }

            // REQUEST_CAMERA_CAPTURE is an
            // app-defined int constant. The callback method gets the
            // result of the request.
        } else {
            Intent takePictureIntent = new Intent(this, PreviewDemo.class);//new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            /*if (takePictureIntent.resolveActivity(this.getPackageManager()) != null) {

                outputFileUri = getCaptureImageOutputUri();
                if (outputFileUri != null) {
                    takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, outputFileUri);
                }
                startActivityForResult(takePictureIntent, REQUEST_CAMERA_CAPTURE);
            }*/
            startActivityForResult(takePictureIntent, REQUEST_CAMERA_CAPTURE);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(data==null) {
            Log.i(TAG, "onActivityResult: data==null");
            return;
        }

        Log.i(TAG, "onActivityResult: requestcode="+requestCode);
        switch (requestCode){

            case REQUEST_CAMERA_CAPTURE: {

                try {
                    Uri imageUri = getPickImageResultUri(data);
                    File imageFile = new File(imageUri.getPath());
                /*Intent intent = new Intent(this, UploadImageActivity.class);
                intent.putExtra("file", imageFile.getAbsolutePath());
                startActivityForResult(intent, REQUEST_CROP);*/
                    //imgBase64=utility.image_to_Base64(this,imageFile.getAbsolutePath());

                    //Bitmap bitmap=utility.getBitmap(imageFile.getAbsolutePath());

                    FilePathString=imageFile.getAbsolutePath();
                    callFragment(FilePathString);
                    if (Validations.isEmptyString(FilePathString)) {
                        cameraActive = false;
                        // profilePics.setImageBitmap(bitmap);
                    } else {
                        cameraActive = true;

                    }

                    // Bitmap b=imageCropView.getCroppedImage();
                    //bitmapConvertToFile(b);


                /*CropImage.activity(imageUri)
                        .setGuidelines(CropImageView.Guidelines.ON)
                        .start(getActivity(), SettingsFragment.this);*/

                }catch (Exception e){
                    e.printStackTrace();
                }
                break;
            }
            case REQUEST_CROP: {

                //profile_img.setImageDrawable(null);
                Uri uri = data.getData();
                //profile_img.setImageURI(uri);

                utility.getPath(uri,this);
                FilePathString=utility.BitMapToString(bitmap);
                //FilePathString = imageFile.getAbsolutePath();

                break;
            }

            default: {
                Log.i(TAG, "onActivityResult: default");
                break;
            }
        }
        /*if (requestCode == CAMERA_REQUEST && resultCode == Activity.RESULT_OK)
        {
            Bitmap photo = (Bitmap) data.getExtras().get("data");
            imageView.setImageBitmap(photo);
        }*/
    }

    private void callFragment(String filePathString) {
        Bundle bundle = new Bundle();

        bundle.putString("filePath", filePathString );

        UploadFragment fragment=UploadFragment.newInstance(filePathString,0);
        //fragment.setArguments(bundle);
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.nav_host_fragment, fragment,TAG_FRAGMENT);
        transaction.addToBackStack(null);
        transaction.commit();
    }

    public Uri getPickImageResultUri(Intent data) {
        boolean isCamera = true;
        if (data != null && data.getData() != null) {
            String action = data.getAction();
            isCamera = action != null && action.equals(MediaStore.ACTION_IMAGE_CAPTURE);
        }
        return isCamera ? getCaptureImageOutputUri() : data.getData();
    }

    private Uri getCaptureImageOutputUri() {
        Uri outputFileUri = null;
        File getImage = getExternalCacheDir();
        if (getImage != null) {
            outputFileUri = Uri.fromFile(new File(getImage.getPath(), "pickImageResult.jpeg"));
        }
        return outputFileUri;
    }
}