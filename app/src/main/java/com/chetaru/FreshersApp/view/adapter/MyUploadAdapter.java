package com.chetaru.FreshersApp.view.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;
import com.chetaru.FreshersApp.R;
import com.chetaru.FreshersApp.service.model.UserViewPost;
import com.chetaru.FreshersApp.service.retrofit.RequestReciever;
import com.chetaru.FreshersApp.utility.SessionParam;
import com.chetaru.FreshersApp.utility.Utility;
import com.chetaru.FreshersApp.utility.Validations;
import com.chetaru.FreshersApp.view.customView.SquareImageView;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;
import java.util.zip.Inflater;

public class MyUploadAdapter extends RecyclerView.Adapter<MyUploadAdapter.MyViewHolder> {

    List<UserViewPost> list = new ArrayList<>();
    private List<UserViewPost> mFilteredList;
    private Context context;
    private boolean isLoadingAdded = false;
    // View Types
    private static final int ITEM = 0;
    private static final int LOADING = 1;
    private boolean retryPageLoad = false;
    private String errorMsg;
    private String fragName;
    Utility utility;
    SessionParam sessionParam;
    MyUploadsListener mListener;

    public MyUploadAdapter(List<UserViewPost> uploadList, Context context,MyUploadsListener mListener,String fragmentName) {
        this.mFilteredList=uploadList;
        this.list=uploadList;
        this.context=context;
        this.fragName=fragmentName;
        this.mListener=mListener;
        notifyDataSetChanged();
        utility=new Utility();
        sessionParam=new SessionParam(context);
    }



    public static class MyViewHolder extends RecyclerView.ViewHolder{

        ImageView cancelImage;
        SquareImageView myUploadImage;
        TextView likeCount;
        LinearLayout likeLayout;
        public MyViewHolder(@NonNull View view) {
            super(view);
            myUploadImage=view.findViewById(R.id.my_upload_image);
            cancelImage=view.findViewById(R.id.my_upload_cancel_image);
            likeCount=view.findViewById(R.id.my_upload_like_count_text);
            likeLayout=view.findViewById(R.id.like_count_layout);

        }
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        MyViewHolder viewHolder=null;
        LayoutInflater layoutInflater=LayoutInflater.from(parent.getContext());
        switch (viewType){
            case ITEM:
                View viewItem= layoutInflater.inflate(R.layout.my_upload_list,parent,false);
                viewHolder=new MyViewHolder(viewItem);
                break;
            case LOADING:
                View viewLoading=layoutInflater.inflate(R.layout.item_progress,parent,false);
                viewHolder=new LoadingVH(viewLoading);
                break;
        }
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        switch (getItemViewType(position)){
            case ITEM:

                UserViewPost viewPost=mFilteredList.get(position);
                //holder.likeCount.setText("12");
                if (!Validations.isEmptyString(fragName)){
                    if (fragName.equals("otherUploads")){
                        holder.cancelImage.setVisibility(View.GONE);
                        holder.likeLayout.setVisibility(View.GONE);
                    }else {
                        holder.likeLayout.setVisibility(View.VISIBLE);
                        holder.cancelImage.setVisibility(View.VISIBLE);
                    }
                }

                holder.likeCount.setText(String.valueOf(viewPost.getLikedCount()));
                holder.cancelImage.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        mListener.MyUploadCancelClick(viewPost,position);
                    }
                });
                holder.likeLayout.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        mListener.MyUploadViewPost(mFilteredList,position);
                    }
                });
                holder.myUploadImage.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        mListener.MyUploadViewPost(mFilteredList,position);
                    }
                });
                /*if (viewPost.getLikesCount()>0){
                    holder.likeLayout.setVisibility(View.VISIBLE);
                    holder.likeCount.setText(String.valueOf(viewPost.getLikesCount()));
                }else{
                    holder.likeLayout.setVisibility(View.GONE);
                }*/

                //Picasso.with(context).load(viewPost.getImageUrl()).into(holder.myUploadImage);
                Glide.with(context).load(viewPost.getImageUrl()).placeholder(R.drawable.profile_pics).into(holder.myUploadImage);


                //Glide.with(context).load(viewPost.getImageUrl()).apply(new RequestOptions().placeholder(R.drawable.profile)).into(holder.myUploadImage);

                break;
            case LOADING:
                LoadingVH loadingVH= (LoadingVH) holder;
                if (retryPageLoad) {
                    loadingVH.mErrorLayout.setVisibility(View.VISIBLE);
                    loadingVH.mProgressBar.setVisibility(View.GONE);

                    loadingVH.mErrorTxt.setText(
                            errorMsg != null ?
                                    errorMsg :
                                    context.getString(R.string.error_msg_unknown));

                } else {
                   /* if(mFilteredList.get(mFilteredList.size()-1).getSelected()==null){
                        loadingVH.mErrorLayout.setVisibility(View.GONE);
                        loadingVH.mProgressBar.setVisibility(View.GONE);
                    }else{
                        loadingVH.mErrorLayout.setVisibility(View.GONE);
                        loadingVH.mProgressBar.setVisibility(View.VISIBLE);
                    }*/
                }
                break;
        }

    }

    @Override
    public int getItemCount() {

        return mFilteredList == null ? 0 : mFilteredList.size();
    }

    public void add(UserViewPost r) {
        if (r.getId()!=null) {
            mFilteredList.add(r);
            notifyItemInserted(mFilteredList.size() - 1);
        }
    }

    public void addAll(List<UserViewPost> viewPost_Results) {
        for (UserViewPost result : viewPost_Results) {
            add(result);
        }
    }
    public void remove(UserViewPost r) {
        int position = mFilteredList.indexOf(r);
        if (position > -1) {
            mFilteredList.remove(position);
            notifyItemRemoved(position);
        }
    }
    public void clear() {
        try {
            while (getItemCount() > 0) {
                remove(getItem(0));
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public void addLoadingFooter() {
        isLoadingAdded = true;
        add(new UserViewPost());
    }

    public void removeLoadingFooter() {
        isLoadingAdded = false;

        int position = mFilteredList.size() - 1;
        UserViewPost result = getItem(position);

        if (result != null) {
            mFilteredList.remove(position);
            notifyItemRemoved(position);
        }
    }

    protected class LoadingVH extends MyViewHolder implements View.OnClickListener {
        private ProgressBar mProgressBar;
        private ImageButton mRetryBtn;
        private TextView mErrorTxt;
        private LinearLayout mErrorLayout;

        public LoadingVH(View itemView) {
            super(itemView);
            mProgressBar = itemView.findViewById(R.id.loadmore_progress);
            mRetryBtn = itemView.findViewById(R.id.loadmore_retry);
            mErrorTxt = itemView.findViewById(R.id.loadmore_errortxt);
            mErrorLayout = itemView.findViewById(R.id.loadmore_errorlayout);
            mRetryBtn.setOnClickListener(this);
            mErrorLayout.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.loadmore_retry:
                case R.id.loadmore_errorlayout:
                    showRetry(false, null);
                    // mCallback.retryPageLoad();
                    break;
            }
        }
    }

    public void showRetry(boolean show, @Nullable String errorMsg) {
        retryPageLoad = show;
        notifyItemChanged(mFilteredList.size() - 1);

        if (errorMsg != null) this.errorMsg = errorMsg;
    }

    @Override
    public int getItemViewType(int position) {
        return (position == mFilteredList.size() - 1 && isLoadingAdded) ? LOADING : ITEM;
//        if(mFilteredList.size()<5){
//            return mFilteredList.size() - 1;
//        }else{
//            return (position == mFilteredList.size() - 1 && isLoadingAdded) ? LOADING : ITEM;
//        }
    }

    public UserViewPost getItem(int position) {
        return mFilteredList.get(position);
    }

    public  interface MyUploadsListener{
        void MyUploadCancelClick(UserViewPost myUploads,int position);
        void MyUploadViewPost(List<UserViewPost> myUploads,int position);
    }



}
