package com.chetaru.FreshersApp.view.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.chetaru.FreshersApp.R;
import com.chetaru.FreshersApp.viewModel.CountryList;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CountryListDialog extends RecyclerView.Adapter<CountryListDialog.ViewHolder> {
    List<CountryList> list = new ArrayList<>();
    private Context context;

    public static final class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.country_name_tv)
        TextView countryNameTxt;
        @BindView(R.id.country_code_tv)
        TextView countryCodeText;

        public ViewHolder(View v) {
            super(v);
            ButterKnife.bind(this,v);
        }
    }

    public CountryListDialog(List<CountryList> list, Context context) {
        this.list = list;
        notifyDataSetChanged();
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_country_list, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        try {
            holder.countryNameTxt.setText(list.get(position).getCountryName());
            holder.countryCodeText.setText(String.valueOf(list.get(position).getPhonecode()));
            Log.d("country",list.get(position).getCountryName().toString());
            Log.d("country",list.get(position).getId().toString());
            Log.d("country",list.get(position).getPhonecode().toString());
            //holder.countryCodeText.setText(list.get(position).getId());

        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {

        try {
            return list.size();
        }catch (Exception e){
            e.printStackTrace();
            return 0;
        }
    }
}
