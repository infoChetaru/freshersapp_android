package com.chetaru.FreshersApp.view.ui.Fragment;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.provider.Settings;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.Transformation;
import com.bumptech.glide.request.RequestOptions;
import com.chetaru.FreshersApp.CustomImage.ImageCropView;
import com.chetaru.FreshersApp.R;

import com.chetaru.FreshersApp.service.model.TagList;
import com.chetaru.FreshersApp.service.model.UserAddPostResponse;
import com.chetaru.FreshersApp.service.model.UserList;
import com.chetaru.FreshersApp.service.retrofit.BaseRequest;
import com.chetaru.FreshersApp.service.retrofit.Functions;
import com.chetaru.FreshersApp.service.retrofit.RequestReciever;
import com.chetaru.FreshersApp.utility.MarshMallowPermission;
import com.chetaru.FreshersApp.utility.MyDividerItemDecoration;
import com.chetaru.FreshersApp.utility.SessionParam;
import com.chetaru.FreshersApp.utility.Utility;
import com.chetaru.FreshersApp.utility.Validations;
import com.chetaru.FreshersApp.view.adapter.HashPresenter;
import com.chetaru.FreshersApp.view.adapter.HashTagPresenter;
import com.chetaru.FreshersApp.view.adapter.PeopleTagPresenter;
import com.chetaru.FreshersApp.view.adapter.TagAdapter;
import com.chetaru.FreshersApp.view.adapter.TagPeopleAdapter;
import com.chetaru.FreshersApp.view.callback.IOnBackPressed;
import com.chetaru.FreshersApp.view.customView.BlurBuilder;
import com.chetaru.FreshersApp.view.ui.Activity.HomeActivity;
import com.chetaru.FreshersApp.viewModel.UploadViewModel;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.jackandphantom.blurimage.BlurImage;
import com.otaliastudios.autocomplete.Autocomplete;
import com.otaliastudios.autocomplete.AutocompleteCallback;
import com.otaliastudios.autocomplete.AutocompletePolicy;
import com.otaliastudios.autocomplete.AutocompletePresenter;
import com.otaliastudios.autocomplete.CharPolicy;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Optional;
import butterknife.Unbinder;
import id.zelory.compressor.Compressor;
import im.delight.android.location.SimpleLocation;
import jp.wasabeef.picasso.transformations.BlurTransformation;
import jp.wasabeef.picasso.transformations.gpu.BrightnessFilterTransformation;
import jp.wasabeef.picasso.transformations.gpu.KuwaharaFilterTransformation;

import static androidx.constraintlayout.widget.Constraints.TAG;
import static com.bumptech.glide.request.RequestOptions.bitmapTransform;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link UploadFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class UploadFragment extends Fragment implements View.OnClickListener,TagAdapter.TagAdapterListener
                            ,TagPeopleAdapter.TapPeopleListener, LocationListener, IOnBackPressed {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private int postId=0;
    private int TakePicture = 1, SELECT_FILE = 2, RESULT_OK = -1;
    final private int REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS = 124;
    private static final int REQUEST_CAMERA = 0;
    static final int LOCATION_SETTINGS_REQUEST = 1;


    private MarshMallowPermission marshMallowPermission;
    private Bitmap bitmap = null;
    private String imgBase64 = "";
    private File sourceFile;
    private Utility utility;
    private String tokenId = "";
    private Unbinder unbinder;
    private UploadViewModel uploadViewModel;

    private SimpleLocation location;

    private BaseRequest baseRequest;

    @BindView(R.id.upload_image_public)
    ImageView uploadImage;
    @BindView(R.id.upload_image_upper)
    ImageView imageUpper;

    @BindView(R.id.post_button)
    ImageView postButton;
    @BindView(R.id.write_a_caption_txt)
    EditText writeCaptionET;





    //initiliaze model class
    ArrayList<UserList> userList = new ArrayList<>();
    ArrayList<UserList> selectedPeople = new ArrayList<>();
    ArrayList<TagList> tagList = new ArrayList<>();
    ArrayList<TagList> selectedtag = new ArrayList<>();
    ArrayList<String> newSelectedtag = new ArrayList<>();
    ArrayList<String> newFinalHash = new ArrayList<>();
    ArrayList<Integer> selectedTagInteger = new ArrayList<>();
    ArrayList<Integer> selectedPeopleInteger = new ArrayList<>();
    String newHashTag;
    String writeCaption;
    SessionParam sessionParam;
    private Autocomplete userAutocomplete;
    private Autocomplete maleFemaleAutocomplete;
    private Autocomplete hashAutocomplete;

    ImageCropView imageCropView;
    Location _currentLocation;
    LocationManager _locationManager;
    String _locationProvider;

    Boolean myCondition=false;
    double longitude;
    double latitude;

    Context context;

//initalise your array here
    //reesults array
    private ArrayList<String> resultsArray = new ArrayList();




    public UploadFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment UploadFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static UploadFragment newInstance(String param1, int param2) {
        UploadFragment fragment = new UploadFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putInt(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            postId = getArguments().getInt(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_upload, container, false);

        unbinder = ButterKnife.bind(this, view);

        // Text to display in the view
        String example = "Really neat stuff @tylersuehr7! #Android #GitHub";

        /*// Create an instance of the view and set the text above
        SocialTextView textView = new SocialTextView(getActivity());
        textView.setLinkText(example);
        // set any other properties you want social text view
        getActivity().setContentView(textView);*/
        uploadViewModel =
                ViewModelProviders.of(this).get(UploadViewModel.class);
        utility = new Utility();
        utility.getProgerss(getContext());
         context=getContext();
        permission();
        /*String hashtags;
       List<String> = new arry;
        hashtags.add("#developed");
        socialTextView.setLinkedHashtag(hashtags);*/
        marshMallowPermission=new MarshMallowPermission(getActivity());
        if (!marshMallowPermission.checkPermissionForCamera() && !marshMallowPermission.checkPermissionForExternalStorage() &&marshMallowPermission.checkPermissionForFineLoaction()){
            marshMallowPermission.requestPermissionForCamera();
            marshMallowPermission.requestPermissionForExternalStorage();
            marshMallowPermission.requestPermissionForFineLoaction();
        }else if(!marshMallowPermission.checkPermissionForCamera()){
            marshMallowPermission.requestPermissionForCamera();
        }else if (!marshMallowPermission.checkPermissionForExternalStorage()){
            marshMallowPermission.requestPermissionForExternalStorage();
        }else if (!marshMallowPermission.checkPermissionForFineLoaction()){
            marshMallowPermission.checkPermissionForFineLoaction();
        }


        sessionParam=new SessionParam(getContext());
        tokenId=sessionParam.token;
        tokenId = "Bearer " + tokenId;
        Log.d("toeknId",tokenId);

        // construct a new instance of SimpleLocation
        location = new SimpleLocation(getContext());

        // if we can't access the location yet
        if (!location.hasLocationEnabled()) {
            // ask the user to enable location access
           // SimpleLocation.openSettings(getContext());
            ShowSettingAlert();
        }
        latitude = location.getLatitude();
        longitude = location.getLongitude();
        userPostList();
        initView(view);
        return view;
    }


    /*request user for certain permission*/
    private void permission() {
        //datafinish = true;
        List<String> permissionsNeeded = new ArrayList<String>();
        final List<String> permissionsList = new ArrayList<String>();
        if (!addPermission(permissionsList, Manifest.permission.ACCESS_FINE_LOCATION))
            permissionsNeeded.add("GPS");
        if (!addPermission(permissionsList, Manifest.permission.ACCESS_NETWORK_STATE))
            permissionsNeeded.add("Network state");
        if (!addPermission(permissionsList, Manifest.permission.ACCESS_COARSE_LOCATION))
            permissionsNeeded.add("Location");
        if (!addPermission(permissionsList, Manifest.permission.INTERNET))
            permissionsNeeded.add("Internet");
        if (!addPermission(permissionsList, Manifest.permission.READ_PHONE_STATE))
            permissionsNeeded.add("phone status");
        if (!addPermission(permissionsList, Manifest.permission.CAMERA))
            permissionsNeeded.add("Camera");
        if (!addPermission(permissionsList, Manifest.permission.WRITE_EXTERNAL_STORAGE))
            permissionsNeeded.add("Write storage");
        if (permissionsList.size() > 0) {
            if (permissionsNeeded.size() > 0) {
                // Need Rationale
                String message = "You need to grant access to " + permissionsNeeded.get(0);
                for (int i = 1; i < permissionsNeeded.size(); i++)
                    message = message + ", " + permissionsNeeded.get(i);
                /*showMessageOKCancel(message, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            requestPermissions(permissionsList.toArray(new String[permissionsList.size()]), REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS);
                        }
                    }
                });*/
                return;
            }
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermissions(permissionsList.toArray(new String[permissionsList.size()]), REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS);
            }
            return;
        }
        //init();
    }

    private boolean addPermission(List<String> permissionsList, String permission) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (getContext().checkSelfPermission(permission) != PackageManager.PERMISSION_GRANTED) {
                permissionsList.add(permission);
                //Check for Rationale Optiong
                if (!shouldShowRequestPermissionRationale(permission))
                    return false;
            }
        }
        return true;
    }


    public void ShowSettingAlert(){
        AlertDialog.Builder builder =new AlertDialog.Builder(getContext());
        builder.setTitle("Allow FreshersApp to access your location?");
        builder.setMessage("FresherApp would like to use Your current location in order to post feed.");
        builder.setPositiveButton("Setting", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
               /* Intent intent=new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                getContext().startActivity(intent);
                startActivityForResult(intent,LOCATION_SETTINGS_REQUEST);*/
                Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS, Uri.fromParts("package",
                        context.getPackageName(), null));
                //i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivityForResult(intent, 1001);

            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int i) {

                dialog.cancel();
            }
        });
        builder.show();
    }

    private void initView(View view) {
        postButton.setOnClickListener(this);
        try {
            if (!Validations.isEmptyString(mParam1)){

                bitmap=utility.getBitmap(mParam1);
                /*int screenWidth = getResources().getDisplayMetrics().widthPixels;
                float ratio = (float)bitmap.getWidth() /  (float)bitmap.getHeight();
                int screenHeight = (int)(ratio*screenWidth);
                Bitmap scaled = Bitmap.createScaledBitmap(bitmap, screenHeight, screenWidth, true);
*/
                //uploadImage.setImageBitmap(bitmap);
               /* Picasso.get()
                        .load(bitmap)
                        .transform(new BlurTransformation(getContext(), 25, 1))
                        .into(uploadImage);*/

                String bitMapString=BitMapString(bitmap);
                //Picasso.with(getContext()).load(bitMapString).into(imageUpper);
                Glide.with(getContext()).load(bitMapString).into(imageUpper);
                /*Picasso.get()
                        .load(bitMapString)
                        .transform(new BlurTransformation(getContext(),25,2))
                        //.transform(new SwirlFilterTransformation(mContext))
                        .transform(new BrightnessFilterTransformation(getContext(), 0.3f))
                        .transform(new KuwaharaFilterTransformation(getContext(), 25))
                        //.transform(new PixelationFilterTransformation(mContext))
                        //.transform(new VignetteFilterTransformation(mContext))
                        //.transform(new ToonFilterTransformation(mContext))

                        //.into(holder.newsImage);
                        .into(uploadImage);*/

                Glide.with(getContext()).load(bitmap)
                        .into(imageUpper);
               /* Bitmap blurredBitmap = BlurBuilder.blur(getContext(),bitmap);
                uploadImage.setImageBitmap(blurredBitmap);*/
               // scaleImage(uploadImage);
                //Glide.with(getActivity()).load(bitmap).apply(new RequestOptions().centerCrop().placeholder(R.drawable.profile_pics)).into(uploadImage);

                //imgBase64=utility.image_to_Base64(getContext(),mParam1);

                BlurImage.with(getContext()).load(bitmap).intensity(25).Async(true).into(uploadImage);
            }

        }catch (Exception e){
            e.printStackTrace();
        }
        //SlectImageFromCamera();
        //tagLayout.setVisibility(View.VISIBLE);

        uploadViewModel.getUserPostResp(tokenId).observe(getViewLifecycleOwner(), new Observer<UserAddPostResponse>() {
            @Override
            public void onChanged(@Nullable UserAddPostResponse userPost) {
                try {
                    if (userPost.getStatus()) {
                        utility.showToast(getContext(), userPost.getMessage());
                        //userList=userPost.getUserTagList().getUserList();
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
                // textView.setText(s);
            }
        });

        if(!Validations.isEmptyString(bitmap.toString()));
        convertBitMap(bitmap);
    }

    private void convertBitMap(Bitmap bitmap) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        // profilePics.setImageBitmap(bitmap);

        imgBase64 = utility.image_to_Base64(getContext(), utility.getPath(utility.getImageUri(getContext(), bitmap), getContext()));

        //imgBase64="iVBORw0KGgoAAAANSUhEUgAAALsAAAAWCAYAAACYEu1aAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyFpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuNi1jMTQyIDc5LjE2MDkyNCwgMjAxNy8wNy8xMy0wMTowNjozOSAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENDIChXaW5kb3dzKSIgeG1wTU06SW5zdGFuY2VJRD0ieG1wLmlpZDo3MEE2MDc5QTY0QUUxMUU4OUM4MkEwNTkxMDhFRERBRiIgeG1wTU06RG9jdW1lbnRJRD0ieG1wLmRpZDo3MEE2MDc5QjY0QUUxMUU4OUM4MkEwNTkxMDhFRERBRiI+IDx4bXBNTTpEZXJpdmVkRnJvbSBzdFJlZjppbnN0YW5jZUlEPSJ4bXAuaWlkOjcwQTYwNzk4NjRBRTExRTg5QzgyQTA1OTEwOEVEREFGIiBzdFJlZjpkb2N1bWVudElEPSJ4bXAuZGlkOjcwQTYwNzk5NjRBRTExRTg5QzgyQTA1OTEwOEVEREFGIi8+IDwvcmRmOkRlc2NyaXB0aW9uPiA8L3JkZjpSREY+IDwveDp4bXBtZXRhPiA8P3hwYWNrZXQgZW5kPSJyIj8+zbdQTQAADnFJREFUeNrUWwmQFcUZ7nnHsizLtesCS6mAgoDgiUeCYoAopDyiScQjJTFqvEpjNBoSrcQzRkolnpSo8UiVZkEtSzQWgopnIhFXxAMNFK4iosu1CMiyx3uT73/9ta/fMD1v3rJQm6731/TMdPf08f1n9/OWKqWalU4JULnOlrUqdT6uF3hKLcX76cOVWrYHy30BWg2q4P02pdJppc5D2Qtx+yGuN7UrtXy0VSYsfc52KkGrQD1BffHLKDXGV2oybkeCBoCyoC9B7+H5K6AP0NemDPuMb3soMNzXt771Cbnfyk/lkkcyCW1U4TLQy2WVXSyBhj4agfY26QZ6YG6G4dl40MEosLfubq79lUmlFm9RamGbUp9WK9WC8as+oGtBc9R3jee+pXb8lvR5eeC5PYaRLOPb/eP0bVJdJ2Ep1LDC6VUruH5xkj1Wkz4poX5k8mywJwXlWFQA/W1k9/esHgOIp+yn1NxkAOzoRQV69h/QaFU4yskHKbWgWwywC4N9q1QNvn0VaJpfrNP6uzNQ7jZQo6/7/jGej3CUr/A4zJC2X8WzHwQfCrMCvMOB6ATG8lvkb0vG6BdoPcqfhev8FOfuSN2epHtBlziq14K+DnkuMmado85N5KeukoYS3HYC76uNMevXgNYGnvXtLIZO2DdY9N5turP729JPFq1JqWfAYlMM+1LM9ATolgeBzjS/QamTigHX18Ad3wuD7BYD6KYO+nclOv816JgKPZCzfTdjjGjBtcUSnex/Jej7Ye33UOpUMLi3QalnBeipmP3Ct/bAN19A9g4A3OuNzKX5IreBJoJm8X496FzQBNAGR7Oy0MeDfg5q5bOZoB+D7lddK4n8OlRpDS9pu0NbuZIwxcGgMwPaQXWqZAdYeoP+LUB3FRaw9MPioNwjYL/ukMKLsMAHusrLSLHYZ0AvzUm6JftxkOwLOjoAXzPfIMzIaj+nIIwlVgD2y/H+LsPdFocfAno32CbKboPtVL0Z2gxzU5foYN/w3Ysg1u5fS7Q25V+NBf2LKvqAvOAvKpi+odUnDLpIdd0kFulm5vuw3/aSnERNVmeVs5OMcQvzvaz8zkl2SvBqMQOigK6IIrDew9CpVwNgC6OALqm77uXsZeDUtvDVE7W1YKe4FYS2XwVA0wDXzV64FjhB7Po0TTU//+4EB6pmwEFIglnvTzi+mdCM/hiy70egcxbmq6cY90+DBhXatiG8V3SotrLtyimqf7IEc6ndhkTY/p2exAnria+v8TWnFZWiST2Sv+DyvTjlu2l7/B8QYad+KN4r6CMSgDAzwqYXxXA2VngI2tkbdJpDCkgaAi0zCqZHnUPnHQdG6J2hTqS5IYx+QZj22lOpx/cCI7ZpqRKU+mLiTILEPgr5qWjrEDjoY7OF5tF3hG9MER/nMN1uGHC9DoDd6+Jgj+pfxrLrt+7OsaZatTQs21WjppkhptKTyNf42k6V1B+MMyUZ4jR62nYb5dNh8/QMfQFAzU/rqEX/gLkgAzgXIJwGblibyFlbO6RhaGexAXxal9krROxsBOBXAOhHJxwiC0b0+xhEtlwDPztQqbegpycB9EOTHI6XH/+SNPVw2+4FXA9tRebk03YCq7nENnqxDZ+CZnMEKOO4WykaCGVWH+2IVFhqt8r0oYnTxr5sK9WMOXt3zDw1woFpgh80PuEYIZ5d7AciEwT8ZgD7d2EzCO1xcSMmAbN4u0NUHN9Gd/8gPeM/DItnoX4d2snCW8ymHcyLfj+X1tqih1kl9O1F5O/D81lCKRLyS7q50ZAtIawWt9wRoNkE95eM6q4lMMRvGRyjDVFEj9PWXsUAnOQXgsawzGCaI89JSJjOt+eYMkn7so2vLCHzLOgNmoJDIqAzgt/YyP58pZc8ZwrVxpbsoOcD3u+uTMus/KSIcgtD7O6cOPC0+dtC7vb5vgzPWzEr7cjPA90aMuvnAy03AahZgu8sB6M9mNLXT3z37B8uvgbebwKilm7QDvhLqLcamnI7fBV/PUMn/Rk7nMHVigBDzGBPpAMrjH4F718HvcXorlhQvwRdRjqLYHaFM//IvFidLzIiJGHFqaB3GG1ppqNpTM63i/Rf1uu/dOWGsr/9eV+hdrQuspTqsxiJkim9gyaQAPxX7IfQqAC2wm2jeqWqfB3HTexioE9L6Li4WZh6X4eZgqmBnNwaRINnAT/C0Ev5WqL1C0FLP8z4OgCzMqnHXB4M84nTDCndTt35AAB8folG4xw0Ogui9A2Inkw1V/RyikCm8aBXqIr/SkB2j2izlUrpegqoYygR7fQw6ByZV9DPlLWRZklIiYLeafwY0EuBMiKdr2L+WAodv1Dx5RjhT9QclZTup9nCR+nNsw2OaEyKUSiR9IeTeZKBEGMVtZJZH5m+uwN9kTqi5W/h/FRR2kdK9o10AleoXWS7o+2rPT2RNnhd6uczzwrF+RbIY8bgpe5DoKuDXhFmfRxm5OmVcCpRrjwExNMhotpli7Nai6tLP9e+w9gSAH866p0OY7Qe6D0Fq7B6DcWQiJ6VO9rE13fCFB9IoL/HsGarwzG8i9MpEnI+JWoL3w+3gH4ApXoY011L3Jj5/dbxvTjmWFOMWPqd7HfYeKYzUHIy6GLlMGFt1adoB+0b4R3vjFs+zdOdCgK2NWb9XJ02UnuRWczq8o86ipxoSbUwRnkiy+9UavOjFcCfgPvr0I9tpUh4oGEMxFIDxE9VhkZwzY7FRHqNpFSqiaBqqv7tjs+ZRT4nxrzeS7MkQcYw6Tpeb3UAXQXKNloS1isNErFCjAabM4q0Z8Z+WrF+2KaLbMoMojPSWRL9Mn9HiW6yaxzVBuNlOhjCK8WozWgB2hjU4dCl+8BGqgYQjw6ZFdFsq0Rvimf8MRGB+1aUvRFt1gL0xyb07mWsvqBOCtL9qRqqzBCDewu/20Sb1EXimH3q4PUeZF4pszRG19otAJ3Ca3eaPpLui9GGyINHQzDUmclYGd8UKbfKcoC7lRL838hDTh+W4uU62PdctPVIBBZE5R4ZUm8IQFIJaklaM+nlr/Lop1bI3IwjJdv0KY3RDPK324zGVdkTz2/wA9/l/sFdeJ9hWDKHnG2cvbReXYkEvexpuhKPBuL+RE87T0e4Fh1tT4A47gtvrikbLmxSKt6WuOtojvFNhE9fKBLGM2A3En2i1Ybhx3Uxl1h8ht93gZi+mdaKiDkKBbviOo+iozOkgx0Qz//vKjoo+7LKn6EIljsCNM8CeK6jXEWJLDzp+K7YnZuSegbmqgDYhft9x0EsX0elckjwNXJkwy0JwGdTlr7NaGC2oC8NoHtwc09WH2Kb6fPsUMhqDMgWnBbo1JS2FntSzDqNdGDN7m83y7SMexalWXWtlChmxri2dZsI+FfCpG+RNAWL/pQXEjoMkQyhIh9lH8IKjkB+c0BEeZ7bhtuEd59Z8bkG+oP7xtBCSwDyz9tVwQ7rJUl9xHmrXwgsiTLIUZecFuirkbJujT5CMcWxCu27MNRlq/ka3nsxwZGxzCmjHcoifAM77aP+z1LUGjT7+jRefQntCQieCtO/xiQxhPtGT8dww1Ktr09TTvD0Dl4PahmJDZ8appcBuNmHQjKNYfvNOub+UJxV93Us97udTy8fZRhNb9/QGErPGwUUqOABGQmI1HKg5jcRccP1vYsZlB1PGyyn1PjyrTFou8pv6orpYo7WxtXmU0OiKx1w63Z58uKA3aiqcSreCbvJvmV62PGhMsbYKi3CvQ9JeGGEzuzv6TivSOytnnbQzoyI2Ny8hSLKzw/s2ZhG39O26C7T9esiVuIavGtG2ZfhPb72GfIQib92lF0M769pQaHn73dgwV112q3w5fSYbQ1juLK71cbfSmhD6h4TMYZEAALB5UoUYRS/gwwRLGuOO4jGS8TRriLhxxmb1gG2cV7E6cWEJTFtwvMG0Hk7y7qwxa7ArK2WQzPm7z7cBf3YLx5dejOj/3CRq9eaD3M2g87MuG2CBDXf0UW0xy8kbj/HChuofGw7W4KNnFXusO3dvEo4f0qRds7gNC0NhP5u4fVHdDy9CId4cSAyE6Zs7WhRUP4Z87k6ItoT1lZYanGUFaDvT2dcTPLRcU1JaeQnoGdC3kkk4k1Xj/tQjMgx10EW7U1vcqRSDwM0F3VUn6Hen0F3muME6cJwpQDkgSJNTLfO6+TEQJ88zYbEvmYn/j0wtVapT+TA/Ly8TS3DNtvsEl+fzEVJRURh5DjPBCscJ/sFh7K+MbnMbvQTSm8a7UnJXcbrYKXNtTrOi5xPsQ92bWWbRrqLOTrUsuMryUiNvJ9rBQWkXv9ASLWB+T+wn5WW9DX+mvRzP9Y1lu5BKr8fojhX+4VEWtKE0WRLpk5U+ZPUQ9mHQ6hUK7x6qwd+wMjxA3EsT4f35vg69CdJ/iv6rq2TEtZ1O0cR5SEKkJborHTqQS9/0KhYWu9rKSWhwNzIeVgs931rDBK3b3CZP2DIKhMpSZJz7bROi8HJ5TqMWhuTAevR5oVARL3Y6dfmHRmRwC5zZ4AK7A0wRf0t7wZVuAMrAH4sYGIEk3TlIuX+Z9Qo8uZejvdrCUgB4GvW8y8INAOXowJCcIEFzCoDQDtSpvTeS3PM+RF/6gNHWcOgsv/wDoX18JLAzlOKnkhSiKE6LOiibMAAC4K9H1kswnnLxb9oZyfxvaGof4anGapvoLg0+c+EZrj3MIIWYyKlHZEf6W+7Ph4QPGQp39rsWQe+MmTOQYEY3Qq9KnLYbDjKnOzpfwoNU/mzG+IML5P/nYJh5qFMA8aV6UVkTMw3N1Ars4JzJCYqssihslP0mzKBJSrjYjeGWI1DuNBjqU1Wsn0B5+oYdnCazvjpBK20+Tq1Rj3734sS3WxNyF5c8DBWLYcvEvh5VfhHF9E2h5Gk/Zk0SQ4PmHYJzssHgfkpp2zyLBPPzOVbvA7gkgrMlv9PgAEAachIu3P65GIAAAAASUVORK5CYII=";
    }
    public String BitMapString(Bitmap bitmap) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        // profilePics.setImageBitmap(bitmap);

        imgBase64 = utility.image_to_Base64(getContext(), utility.getPath(utility.getImageUri(getContext(), bitmap), getContext()));

        //imgBase64="iVBORw0KGgoAAAANSUhEUgAAALsAAAAWCAYAAACYEu1aAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyFpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuNi1jMTQyIDc5LjE2MDkyNCwgMjAxNy8wNy8xMy0wMTowNjozOSAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENDIChXaW5kb3dzKSIgeG1wTU06SW5zdGFuY2VJRD0ieG1wLmlpZDo3MEE2MDc5QTY0QUUxMUU4OUM4MkEwNTkxMDhFRERBRiIgeG1wTU06RG9jdW1lbnRJRD0ieG1wLmRpZDo3MEE2MDc5QjY0QUUxMUU4OUM4MkEwNTkxMDhFRERBRiI+IDx4bXBNTTpEZXJpdmVkRnJvbSBzdFJlZjppbnN0YW5jZUlEPSJ4bXAuaWlkOjcwQTYwNzk4NjRBRTExRTg5QzgyQTA1OTEwOEVEREFGIiBzdFJlZjpkb2N1bWVudElEPSJ4bXAuZGlkOjcwQTYwNzk5NjRBRTExRTg5QzgyQTA1OTEwOEVEREFGIi8+IDwvcmRmOkRlc2NyaXB0aW9uPiA8L3JkZjpSREY+IDwveDp4bXBtZXRhPiA8P3hwYWNrZXQgZW5kPSJyIj8+zbdQTQAADnFJREFUeNrUWwmQFcUZ7nnHsizLtesCS6mAgoDgiUeCYoAopDyiScQjJTFqvEpjNBoSrcQzRkolnpSo8UiVZkEtSzQWgopnIhFXxAMNFK4iosu1CMiyx3uT73/9ta/fMD1v3rJQm6731/TMdPf08f1n9/OWKqWalU4JULnOlrUqdT6uF3hKLcX76cOVWrYHy30BWg2q4P02pdJppc5D2Qtx+yGuN7UrtXy0VSYsfc52KkGrQD1BffHLKDXGV2oybkeCBoCyoC9B7+H5K6AP0NemDPuMb3soMNzXt771Cbnfyk/lkkcyCW1U4TLQy2WVXSyBhj4agfY26QZ6YG6G4dl40MEosLfubq79lUmlFm9RamGbUp9WK9WC8as+oGtBc9R3jee+pXb8lvR5eeC5PYaRLOPb/eP0bVJdJ2Ep1LDC6VUruH5xkj1Wkz4poX5k8mywJwXlWFQA/W1k9/esHgOIp+yn1NxkAOzoRQV69h/QaFU4yskHKbWgWwywC4N9q1QNvn0VaJpfrNP6uzNQ7jZQo6/7/jGej3CUr/A4zJC2X8WzHwQfCrMCvMOB6ATG8lvkb0vG6BdoPcqfhev8FOfuSN2epHtBlziq14K+DnkuMmado85N5KeukoYS3HYC76uNMevXgNYGnvXtLIZO2DdY9N5turP729JPFq1JqWfAYlMM+1LM9ATolgeBzjS/QamTigHX18Ad3wuD7BYD6KYO+nclOv816JgKPZCzfTdjjGjBtcUSnex/Jej7Ye33UOpUMLi3QalnBeipmP3Ct/bAN19A9g4A3OuNzKX5IreBJoJm8X496FzQBNAGR7Oy0MeDfg5q5bOZoB+D7lddK4n8OlRpDS9pu0NbuZIwxcGgMwPaQXWqZAdYeoP+LUB3FRaw9MPioNwjYL/ukMKLsMAHusrLSLHYZ0AvzUm6JftxkOwLOjoAXzPfIMzIaj+nIIwlVgD2y/H+LsPdFocfAno32CbKboPtVL0Z2gxzU5foYN/w3Ysg1u5fS7Q25V+NBf2LKvqAvOAvKpi+odUnDLpIdd0kFulm5vuw3/aSnERNVmeVs5OMcQvzvaz8zkl2SvBqMQOigK6IIrDew9CpVwNgC6OALqm77uXsZeDUtvDVE7W1YKe4FYS2XwVA0wDXzV64FjhB7Po0TTU//+4EB6pmwEFIglnvTzi+mdCM/hiy70egcxbmq6cY90+DBhXatiG8V3SotrLtyimqf7IEc6ndhkTY/p2exAnria+v8TWnFZWiST2Sv+DyvTjlu2l7/B8QYad+KN4r6CMSgDAzwqYXxXA2VngI2tkbdJpDCkgaAi0zCqZHnUPnHQdG6J2hTqS5IYx+QZj22lOpx/cCI7ZpqRKU+mLiTILEPgr5qWjrEDjoY7OF5tF3hG9MER/nMN1uGHC9DoDd6+Jgj+pfxrLrt+7OsaZatTQs21WjppkhptKTyNf42k6V1B+MMyUZ4jR62nYb5dNh8/QMfQFAzU/rqEX/gLkgAzgXIJwGblibyFlbO6RhaGexAXxal9krROxsBOBXAOhHJxwiC0b0+xhEtlwDPztQqbegpycB9EOTHI6XH/+SNPVw2+4FXA9tRebk03YCq7nENnqxDZ+CZnMEKOO4WykaCGVWH+2IVFhqt8r0oYnTxr5sK9WMOXt3zDw1woFpgh80PuEYIZ5d7AciEwT8ZgD7d2EzCO1xcSMmAbN4u0NUHN9Gd/8gPeM/DItnoX4d2snCW8ymHcyLfj+X1tqih1kl9O1F5O/D81lCKRLyS7q50ZAtIawWt9wRoNkE95eM6q4lMMRvGRyjDVFEj9PWXsUAnOQXgsawzGCaI89JSJjOt+eYMkn7so2vLCHzLOgNmoJDIqAzgt/YyP58pZc8ZwrVxpbsoOcD3u+uTMus/KSIcgtD7O6cOPC0+dtC7vb5vgzPWzEr7cjPA90aMuvnAy03AahZgu8sB6M9mNLXT3z37B8uvgbebwKilm7QDvhLqLcamnI7fBV/PUMn/Rk7nMHVigBDzGBPpAMrjH4F718HvcXorlhQvwRdRjqLYHaFM//IvFidLzIiJGHFqaB3GG1ppqNpTM63i/Rf1uu/dOWGsr/9eV+hdrQuspTqsxiJkim9gyaQAPxX7IfQqAC2wm2jeqWqfB3HTexioE9L6Li4WZh6X4eZgqmBnNwaRINnAT/C0Ev5WqL1C0FLP8z4OgCzMqnHXB4M84nTDCndTt35AAB8folG4xw0Ogui9A2Inkw1V/RyikCm8aBXqIr/SkB2j2izlUrpegqoYygR7fQw6ByZV9DPlLWRZklIiYLeafwY0EuBMiKdr2L+WAodv1Dx5RjhT9QclZTup9nCR+nNsw2OaEyKUSiR9IeTeZKBEGMVtZJZH5m+uwN9kTqi5W/h/FRR2kdK9o10AleoXWS7o+2rPT2RNnhd6uczzwrF+RbIY8bgpe5DoKuDXhFmfRxm5OmVcCpRrjwExNMhotpli7Nai6tLP9e+w9gSAH866p0OY7Qe6D0Fq7B6DcWQiJ6VO9rE13fCFB9IoL/HsGarwzG8i9MpEnI+JWoL3w+3gH4ApXoY011L3Jj5/dbxvTjmWFOMWPqd7HfYeKYzUHIy6GLlMGFt1adoB+0b4R3vjFs+zdOdCgK2NWb9XJ02UnuRWczq8o86ipxoSbUwRnkiy+9UavOjFcCfgPvr0I9tpUh4oGEMxFIDxE9VhkZwzY7FRHqNpFSqiaBqqv7tjs+ZRT4nxrzeS7MkQcYw6Tpeb3UAXQXKNloS1isNErFCjAabM4q0Z8Z+WrF+2KaLbMoMojPSWRL9Mn9HiW6yaxzVBuNlOhjCK8WozWgB2hjU4dCl+8BGqgYQjw6ZFdFsq0Rvimf8MRGB+1aUvRFt1gL0xyb07mWsvqBOCtL9qRqqzBCDewu/20Sb1EXimH3q4PUeZF4pszRG19otAJ3Ca3eaPpLui9GGyINHQzDUmclYGd8UKbfKcoC7lRL838hDTh+W4uU62PdctPVIBBZE5R4ZUm8IQFIJaklaM+nlr/Lop1bI3IwjJdv0KY3RDPK324zGVdkTz2/wA9/l/sFdeJ9hWDKHnG2cvbReXYkEvexpuhKPBuL+RE87T0e4Fh1tT4A47gtvrikbLmxSKt6WuOtojvFNhE9fKBLGM2A3En2i1Ybhx3Uxl1h8ht93gZi+mdaKiDkKBbviOo+iozOkgx0Qz//vKjoo+7LKn6EIljsCNM8CeK6jXEWJLDzp+K7YnZuSegbmqgDYhft9x0EsX0elckjwNXJkwy0JwGdTlr7NaGC2oC8NoHtwc09WH2Kb6fPsUMhqDMgWnBbo1JS2FntSzDqNdGDN7m83y7SMexalWXWtlChmxri2dZsI+FfCpG+RNAWL/pQXEjoMkQyhIh9lH8IKjkB+c0BEeZ7bhtuEd59Z8bkG+oP7xtBCSwDyz9tVwQ7rJUl9xHmrXwgsiTLIUZecFuirkbJujT5CMcWxCu27MNRlq/ka3nsxwZGxzCmjHcoifAM77aP+z1LUGjT7+jRefQntCQieCtO/xiQxhPtGT8dww1Ktr09TTvD0Dl4PahmJDZ8appcBuNmHQjKNYfvNOub+UJxV93Us97udTy8fZRhNb9/QGErPGwUUqOABGQmI1HKg5jcRccP1vYsZlB1PGyyn1PjyrTFou8pv6orpYo7WxtXmU0OiKx1w63Z58uKA3aiqcSreCbvJvmV62PGhMsbYKi3CvQ9JeGGEzuzv6TivSOytnnbQzoyI2Ny8hSLKzw/s2ZhG39O26C7T9esiVuIavGtG2ZfhPb72GfIQib92lF0M769pQaHn73dgwV112q3w5fSYbQ1juLK71cbfSmhD6h4TMYZEAALB5UoUYRS/gwwRLGuOO4jGS8TRriLhxxmb1gG2cV7E6cWEJTFtwvMG0Hk7y7qwxa7ArK2WQzPm7z7cBf3YLx5dejOj/3CRq9eaD3M2g87MuG2CBDXf0UW0xy8kbj/HChuofGw7W4KNnFXusO3dvEo4f0qRds7gNC0NhP5u4fVHdDy9CId4cSAyE6Zs7WhRUP4Z87k6ItoT1lZYanGUFaDvT2dcTPLRcU1JaeQnoGdC3kkk4k1Xj/tQjMgx10EW7U1vcqRSDwM0F3VUn6Hen0F3muME6cJwpQDkgSJNTLfO6+TEQJ88zYbEvmYn/j0wtVapT+TA/Ly8TS3DNtvsEl+fzEVJRURh5DjPBCscJ/sFh7K+MbnMbvQTSm8a7UnJXcbrYKXNtTrOi5xPsQ92bWWbRrqLOTrUsuMryUiNvJ9rBQWkXv9ASLWB+T+wn5WW9DX+mvRzP9Y1lu5BKr8fojhX+4VEWtKE0WRLpk5U+ZPUQ9mHQ6hUK7x6qwd+wMjxA3EsT4f35vg69CdJ/iv6rq2TEtZ1O0cR5SEKkJborHTqQS9/0KhYWu9rKSWhwNzIeVgs931rDBK3b3CZP2DIKhMpSZJz7bROi8HJ5TqMWhuTAevR5oVARL3Y6dfmHRmRwC5zZ4AK7A0wRf0t7wZVuAMrAH4sYGIEk3TlIuX+Z9Qo8uZejvdrCUgB4GvW8y8INAOXowJCcIEFzCoDQDtSpvTeS3PM+RF/6gNHWcOgsv/wDoX18JLAzlOKnkhSiKE6LOiibMAAC4K9H1kswnnLxb9oZyfxvaGof4anGapvoLg0+c+EZrj3MIIWYyKlHZEf6W+7Ph4QPGQp39rsWQe+MmTOQYEY3Qq9KnLYbDjKnOzpfwoNU/mzG+IML5P/nYJh5qFMA8aV6UVkTMw3N1Ars4JzJCYqssihslP0mzKBJSrjYjeGWI1DuNBjqU1Wsn0B5+oYdnCazvjpBK20+Tq1Rj3734sS3WxNyF5c8DBWLYcvEvh5VfhHF9E2h5Gk/Zk0SQ4PmHYJzssHgfkpp2zyLBPPzOVbvA7gkgrMlv9PgAEAachIu3P65GIAAAAASUVORK5CYII=";
        return imgBase64;
    }


    private void setupHashAutocomplete(ArrayList<TagList> tagList) {
        float elevation = 6f;
        List<String> hash = new ArrayList<>();
        hash.add("hash");
        hash.add("happy");
        hash.add("enjoy");
        hash.add("sad");
        hash.add("party");
        hash.add("bash");
        hash.add("color");
        hash.add("define");
        Drawable backgroundDrawable = new ColorDrawable(Color.WHITE);
        AutocompletePolicy hashpolicy = new CharPolicy('#'); // Look for #hash
        AutocompletePresenter<TagList> hashpresenter = new HashPresenter(getContext(),tagList);

        AutocompleteCallback<TagList> hashcallback = new AutocompleteCallback<TagList>() {
            @Override
            public boolean onPopupItemClicked(Editable editable, TagList item) {
                // Replace query text with the full name.
                int[] range = CharPolicy.getQueryRange(editable);
                if (range == null) return false;
                int start = range[0];
                int end = range[1];
                String replacement = item.getTagName()+" ";
                editable.replace(start, end, replacement);

                // This is better done with regexes and a TextWatcher, due to what happens when
                // the user clears some parts of the text. Up to you.
//                editable.setSpan(new StyleSpan(Typeface.BOLD), start, start+replacement.length(),
//                        Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                Log.d(TAG,"AutoComplete text value---->"+editable.toString());
                if (item.isSelected()) {
                    //text += item.getTagName()+", ";
                    selectedtag.add(item);
                    if (!Validations.isEmptyString(String.valueOf(item.getId()))) {
                        selectedTagInteger.add(item.getId());
                    }else {
                        newSelectedtag.add(item.getTagName());
                    }
                }
                return true;
            }
            public void onPopupVisibilityChanged(boolean shown) {


            }

        };
        /*String text = "";
        selectedTagInteger.clear();
        newSelectedtag.clear();
        List<TagList> TagList =hashcallback.getTagLists();
        for (TagList model : TagList) {
            if (model.isSelected()) {
                text += model.getTagName()+", ";
                selectedtag.add(model);
                if (!Validations.isEmptyString(String.valueOf(model.getId()))) {
                    selectedTagInteger.add(model.getId());
                }else {
                    newSelectedtag.add(model.getTagName());
                }
            }
        }*/

        hashAutocomplete = Autocomplete.<TagList>on(writeCaptionET)
                .with(elevation)
                .with(backgroundDrawable)
                .with(hashpolicy)
                .with(hashpresenter)
                .with(hashcallback)
                .build();
    }

    private void setupPeopleAutocomplete(ArrayList<UserList> userList) {
        float elevation = 6f;
        List<String> hash = new ArrayList<>();
        hash.add("hash");
        hash.add("happy");
        hash.add("enjoy");
        hash.add("sad");
        hash.add("party");
        hash.add("bash");
        hash.add("color");
        hash.add("define");
        Drawable backgroundDrawable = new ColorDrawable(Color.WHITE);
        AutocompletePolicy hashpolicy = new CharPolicy('@'); // Look for #hash
        AutocompletePresenter<UserList> hashpresenter = new PeopleTagPresenter(getContext(),userList);
        AutocompleteCallback<UserList> hashcallback = new AutocompleteCallback<UserList>() {
            @Override
            public boolean onPopupItemClicked(Editable editable, UserList item) {
                // Replace query text with the full name.
                int[] range = CharPolicy.getQueryRange(editable);
                if (range == null) return false;
                int start = range[0];
                int end = range[1];
                String itemname = item.getName();
                itemname = itemname.replaceAll(" ", "_");
                String replacement = itemname+" ";
                editable.replace(start, end, replacement);
                // This is better done with regexes and a TextWatcher, due to what happens when
                // the user clears some parts of the text. Up to you.
//                editable.setSpan(new StyleSpan(Typeface.BOLD), start, start+replacement.length(),
//                        Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                Log.d(TAG,"AutoComplete text value---->"+editable.toString());
                if (item.isSelected()) {
                   // text += item.getName()+", ";
                    selectedPeople.add(item);
                    selectedPeopleInteger.add(item.getId());
                }
                return true;
            }
            public void onPopupVisibilityChanged(boolean shown) {


            }
        };

        hashAutocomplete = Autocomplete.<UserList>on(writeCaptionET)
                .with(elevation)
                .with(backgroundDrawable)
                .with(hashpolicy)
                .with(hashpresenter)
                .with(hashcallback)
                .build();
    }

    @Override public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }


    public void scaleImage(ImageView view) {
        Drawable drawing = view.getDrawable();
        if (drawing == null) {
            return;
        }
        Bitmap bitmap = ((BitmapDrawable) drawing).getBitmap();

        int width = bitmap.getWidth();
        int height = bitmap.getHeight();
        int xBounding = ((View) view.getParent()).getWidth();//EXPECTED WIDTH
        int yBounding = ((View) view.getParent()).getHeight();//EXPECTED HEIGHT

        float xScale = ((float) xBounding) / width;
        float yScale = ((float) yBounding) / height;

        Matrix matrix = new Matrix();
        matrix.postScale(xScale, yScale);

        Bitmap scaledBitmap = Bitmap.createBitmap(bitmap, 0, 0, width, height, matrix, true);
        width = scaledBitmap.getWidth();
        height = scaledBitmap.getHeight();
        BitmapDrawable result = new BitmapDrawable(getContext().getResources(), scaledBitmap);

        view.setImageDrawable(result);

        LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) view.getLayoutParams();
        params.width = width;
        params.height = height;
        view.setLayoutParams(params);
    }
    @Override
    public void onPause() {
        try {
            // stop location updates (saves battery)
            location.endUpdates();
        }catch (Exception e){
            e.printStackTrace();
        }

        // ...

        super.onPause();
    }
    @Override
    public void onResume() {
        super.onResume();

        try {
            // make the device update its location
             location.beginUpdates();
             if(location.hasLocationEnabled()){
                 latitude=location.getLatitude();
                 longitude=location.getLongitude();
             }

        }catch (Exception e){
            e.printStackTrace();
        }

        // ...
    }
    @Optional
    @OnClick({R.id.upload_image_public,R.id.post_button})
    @Override
    public void onClick(View view) {
        switch (view.getId()){



            case R.id.post_button:
                //getWritePOst();

                String captionWrite=writeCaptionET.getText().toString().trim();

                if (Validations.isEmptyString(captionWrite)){
                    utility.showToast(getContext(),"Please enter valid caption");
                    return;
                }
                String captionValue =searchFor(writeCaptionET.getText().toString().trim());
                List<String> strTag=getHashTagFromString(writeCaptionET.getText().toString().trim());
                List<String> strUserTag=getPeopleTagFromString(writeCaptionET.getText().toString().trim());
              try {


               for (int i=0;i<strTag.size();i++){
                   strTag.set(i,strTag.get(i).replace("#", ""));
                   newFinalHash.add(strTag.get(i).replace("#", ""));
               }

                        for (int j = 0; j < strTag.size(); j++) {
                            if (selectedtag.size()>0){
                            for (int i=0;i<selectedtag.size();i++) {
                            if (selectedtag.get(i).getTagName().contains(strTag.get(j))) {
                                break;
                            }else {
                                newSelectedtag.add(strTag.get(j));
                                break;
                            }
                            }
                           }else{
                                newSelectedtag.add(strTag.get(j));
                            }
                    }
                  if (selectedtag.size()>0) {
                      for (int m = 0; m < selectedtag.size(); m++) {

                          if (newSelectedtag.size()>0) {
                              for (int n = 0; n < newSelectedtag.size(); n++) {
                                  if (selectedtag.get(m).getTagName().contains(newSelectedtag.get(n))) {
                                      newSelectedtag.remove(n);
                                      break;
                                  }
                              }
                          }
                      }
                  }

              }catch (Exception e){
                  e.printStackTrace();
              }

                if (longitude ==0.0){
                    ShowSettingAlert();
                }else {
                   // Log.d("lat long","longitude: "+longitude+" latitude: "+latitude+" ");
                    userPost(captionValue);
                }
                break;

            case R.id.upload_image_public:
                //getActivity().finish();
                break;



        }

    }


    public static List<String> getPeopleTagFromString(String str) {
        Pattern MY_PATTERN = Pattern.compile("(@[a-zA-Z0-9ğüşöçıİĞÜŞÖÇ]{2,50}\\b)");
        Matcher mat = MY_PATTERN.matcher(str);
        List<String> strs = new ArrayList<>();
        while (mat.find()) {
            strs.add(mat.group(1));
        }

        return strs;
    }

        public static List<String> getHashTagFromString(String str) {
            Pattern MY_PATTERN = Pattern.compile("(#[a-zA-Z0-9ğüşöçıİĞÜŞÖÇ]{2,50}\\b)");
            Matcher mat = MY_PATTERN.matcher(str);
            List<String> strs = new ArrayList<>();
            while (mat.find()) {
                strs.add(mat.group(1));
            }

            return strs;
        }
    private String searchFor(String data){
        resultsArray.clear();
        //notifiy adapter
        String[] a = data.split(" ");
        String fullString="";
        List<String> captionList=new ArrayList<>();
        selectedPeopleInteger.clear();
        for(int i = 0; i < a.length; i++){
            // you got all name here use what ever you want.
            a[i]=a[i].replace("@", "");
            a[i]=a[i].replace("_"," ");
            captionList.add(a[i]);
        }
        for (int j=0;j<captionList.size();j++){
            for (int k=0;k<selectedPeople.size();k++) {
                if (captionList.get(j).equals(selectedPeople.get(k).getName())) {
                    captionList.set(j, "@" + selectedPeople.get(k).getId());
                    selectedPeopleInteger.add(selectedPeople.get(k).getId());
                }
            }
        }
        for (int j=0;j<captionList.size();j++){
            fullString+=captionList.get(j)+" ";
        }
        return fullString;
    }


    private void getWritePOst() {
        String input =writeCaptionET.getText().toString().trim();
        try {
            JSONArray jsonArray = new JSONArray(input);
            String[] strArr = new String[jsonArray.length()];

            for (int i = 0; i < jsonArray.length(); i++) {
                if (!jsonArray.getString(i).equals("@*")&&!jsonArray.getString(i).equals("#*")) {
                    strArr[i] = jsonArray.getString(i);
                }
            }
            utility.showToast(getContext(), Arrays.toString(strArr));
            writeCaption=Arrays.toString(strArr);
        }catch (Exception e){
            e.printStackTrace();
        }
    }


    /*is api call used for calling userPostList API
     */
    public void userPostList() {
        baseRequest = new BaseRequest(getContext());
        baseRequest.setBaseRequestListner(new RequestReciever() {
            @Override
            public void onSuccess(int requestCode, String Json, Object object) {
                try {
                    JSONObject jsonObject = new JSONObject(object.toString());
                    JSONArray userJsonArray = jsonObject.getJSONArray("userList");

                   // ArrayList<ModelTribeometerGraph> list = new ArrayList<>();
                    userList = baseRequest.getDataList(userJsonArray, UserList.class);
                    JSONArray tagJsonArray = jsonObject.getJSONArray("hashTagsList");
                    tagList = baseRequest.getDataList(tagJsonArray, TagList.class);
                    selectedTagInteger.clear();
                    newSelectedtag.clear();
                    setupHashAutocomplete(tagList);
                    setupPeopleAutocomplete(userList);
                    Log.d("userList",userList.toString());
                    //apiTellSidLogin();
                }catch (Exception e){
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int requestCode, String errorCode, String message) {
                //errorLayout.showError(message);
                //utility.showToast(getContext(),message);
            }

            @Override
            public void onNetworkFailure(int requestCode, String message) {
                // errorLayout.showError(message);
                utility.showToast(getContext(),message);
            }
        });
        JsonObject object = Functions.getClient().getJsonMapObject(" "," ");
        baseRequest.callAPIPost(1, object, getString(R.string.getDataForPost));
    }



    @Override
    public void onTagSelected(TagList tagList) {



    }
    public String removeLastChar(String s) {
        if (s == null || s.length() == 0) {
            return s;
        }
        return s.substring(0, s.length()-1);
    }

    @Override
    public void onNewTagSelected(String newHash) {
        /*TagList newAdd=new TagList();
        newAdd.setTagName(newHash);
        newHashTag=newHash;
        tagList.set(0,newAdd);
        newSelectedtag.add(newHash);*/
       /* String tag=tagTextView.getText().toString().trim();
        tag += newHash+", ";
        tagTextView.setText(tag);
        newSelectedtag.add(newHash);*/

    }

    @Override
    public void onPeopleSelected(UserList tagPeople) {

    }


    @Override
    public void onLocationChanged(Location location) {
        Log.d(TAG,"location:"+location.getLatitude());
        Log.d(TAG,"location"+location.getLongitude());
        latitude=location.getLatitude();
        longitude=location.getLongitude();

    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {

    }

    @Override
    public void onProviderEnabled(String s) {

    }

    @Override
    public void onProviderDisabled(String s) {

    }
    /*is api call used for calling userPostList API
     */
    public void userPost(String captionValue) {
        baseRequest = new BaseRequest(getContext());
        baseRequest.setBaseRequestListner(new RequestReciever() {
            @Override
            public void onSuccess(int requestCode, String Json, Object object) {
                try {

                    JSONObject jsonObject = new JSONObject(object.toString());
                    String message=jsonObject.getString("message");
                    utility.showToast(getContext(),message);
                    writeCaptionET.setText("");
                    selectedPeopleInteger.clear();
                    selectedTagInteger.clear();
                    newSelectedtag.clear();
                    Intent intent=new Intent(getContext(), HomeActivity.class);
                    startActivity(intent);
                    getActivity().finish();
                   // JSONObject jsonObject = new JSONObject(object.toString());
                   // getActivity().finish();
                    /*JSONArray userJsonArray = jsonObject.getJSONArray("userList");




                    // ArrayList<ModelTribeometerGraph> list = new ArrayList<>();
                    userList = baseRequest.getDataList(userJsonArray, UserList.class);
                    JSONArray tagJsonArray = jsonObject.getJSONArray("hashTagsList");
                    tagList = baseRequest.getDataList(tagJsonArray, TagList.class);
                    Log.d("userList",userList.toString());*/
                    //apiTellSidLogin();
                }catch (Exception e){
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int requestCode, String errorCode, String message) {
                writeCaptionET.setText("");
                       selectedPeopleInteger.clear();
                        selectedTagInteger.clear();
                      newSelectedtag.clear();
                utility.showToast(getContext(),message);

               // showLocationDialod();
            }

            @Override
            public void onNetworkFailure(int requestCode, String message) {
                // errorLayout.showError(message);
                utility.showToast(getContext(),message);
            }
        });

        String writeCaption=captionValue.toString().trim();
        JsonObject main_object = new JsonObject();
        JsonArray user_id_array = new JsonArray();
        JsonArray hash_id_array = new JsonArray();
        JsonArray new_hash_array = new JsonArray();
        JsonArray modify_hash_array = new JsonArray();

        for (int i = 0; i < selectedPeopleInteger.size(); i++) {

            user_id_array.add(selectedPeopleInteger.get(i));
        }

        for (int i = 0; i < selectedTagInteger.size(); i++) {
            hash_id_array.add(selectedTagInteger.get(i));
        }
        for (int i = 0; i < newSelectedtag.size(); i++) {
            new_hash_array.add(newSelectedtag.get(i));
        }
        /*for (int i = 0; i < newFinalHash.size(); i++) {
            modify_hash_array.add(newFinalHash.get(i));
        }*/

       /* JSONArray Obj = new JSONArray();
        try {
            for(int i = 0; i < 3; i++) {
                // 1st object
                JSONObject list1 = new JSONObject();
                list1.put("val1",i+1);
                list1.put("val2",i+2);
                list1.put("val3",i+3);
                Obj.put(list1);
            }

        } catch (JSONException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }*/
       /*"replyPost":1,
  "originalPostId":250*/
        main_object.addProperty("caption", writeCaption);
        main_object.add("tagUserId", user_id_array);
        main_object.add("existingHashTagId", hash_id_array);
        main_object.add("newHashTags", new_hash_array);
        main_object.addProperty("postImage", imgBase64);
        main_object.addProperty("latitude", latitude);
        main_object.addProperty("longitude", longitude);
       //testing paris location
       // main_object.addProperty("latitude", "39.892844");
        //main_object.addProperty("longitude", "4.800408");

        if(postId>0){
        main_object.addProperty("replyPost", 1);
            main_object.addProperty("originalPostId", postId);
        }else {
            main_object.addProperty("replyPost", 0);
        }

        /*main_object.addProperty("latitude", 22.7441);
        main_object.addProperty("longitude", 75.894);*/

        /*JsonObject sub_object = new JsonObject();
        for (int i = 0; i < valueList.size(); i++) {
            if (valueList.get(i).isSelected()) {
                sub_object.addProperty("dotBeliefId", valueList.get(i).getBeliefId());
                sub_object.addProperty("dotValueId", valueList.get(i).getId() + "");
                sub_object.addProperty("dotValueNameId", valueList.get(i).getValueId());
                options_array.add(sub_object);
                sub_object = new JsonObject();
            }
        }
        main_object.add("options", options_array);*/
        /*JsonObject object = Functions.getClient().getJsonMapObject(
                "caption",writeCaptionET.
                        getText().toString().trim()
            ,"tagUserId",selectedPeopleInteger.toString(),
                "existingHashTagId",selectedTagInteger.toString(),
                "newHashTags",newSelectedtag.toString(),
                "postImage",  imgBase64,
                "latitude", 22.750994+"",
                "longitude",75.895719+"");*/

        baseRequest.callAPIPost(1, main_object, getString(R.string.getPost));
    }

    private void showLocationDialod() {
        final Dialog dialog = new Dialog(getContext());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.lat_long_msg_layout);
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(true);
        final Button upgradeButton = dialog.findViewById(R.id.upgrade_button);
        final Button homeButton = dialog.findViewById(R.id.home_button);
        upgradeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                final int sdk = android.os.Build.VERSION.SDK_INT;
                if(sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
                    upgradeButton.setBackgroundDrawable(ContextCompat.getDrawable(getContext(), R.drawable.button_background_primary) );
                    homeButton.setBackgroundDrawable(ContextCompat.getDrawable(getContext(), R.drawable.button_white_color_border) );
                    upgradeButton.setTextColor(ContextCompat.getColor(getContext(),R.color.white));
                    homeButton.setTextColor(ContextCompat.getColor(getContext(),R.color.colorPrimary));
                } else {
                    upgradeButton.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.button_background_primary));
                    homeButton.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.button_white_color_border));
                    upgradeButton.setTextColor(ContextCompat.getColor(getContext(),R.color.white));
                    homeButton.setTextColor(ContextCompat.getColor(getContext(),R.color.colorPrimary));
                }
                utility.showToast(getContext(),"Coming Soon");
                dialog.cancel();
            }
        });
        homeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final int sdk = android.os.Build.VERSION.SDK_INT;
                if(sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
                    upgradeButton.setBackgroundDrawable(ContextCompat.getDrawable(getContext(), R.drawable.button_white_color_border) );
                    homeButton.setBackgroundDrawable(ContextCompat.getDrawable(getContext(), R.drawable.button_background_primary) );
                    upgradeButton.setTextColor(ContextCompat.getColor(getContext(),R.color.colorPrimary));
                    homeButton.setTextColor(ContextCompat.getColor(getContext(),R.color.white));
                } else {
                    upgradeButton.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.button_white_color_border));
                    homeButton.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.button_background_primary));
                    upgradeButton.setTextColor(ContextCompat.getColor(getContext(),R.color.colorPrimary));
                    homeButton.setTextColor(ContextCompat.getColor(getContext(),R.color.white));
                }
                //tagLayout.setVisibility(View.GONE);
                myCondition=false;
                userPostList();

                onBackPressed();
                dialog.cancel();
            }
        });

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = dialog.getWindow();
        lp.copyFrom(window.getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);
        dialog.show();
    }
    private void tagDialog(ArrayList<TagList> tagLists) {

        //SearchView searchView;

        final Dialog dialog = new Dialog(getContext());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_tag_list);
        dialog.setCanceledOnTouchOutside(true);
        final RecyclerView rv_list = dialog.findViewById(R.id.tag_list);
        final TextView cancelTxt = dialog.findViewById(R.id.dialog_cancel_text);
        final TextView doneTxt = dialog.findViewById(R.id.dialog_done_text);
        EditText serchET = dialog.findViewById(R.id.tag_search_view_edt);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getContext());
        rv_list.setLayoutManager(mLayoutManager);
        rv_list.setItemAnimator(new DefaultItemAnimator());
        rv_list.addItemDecoration(new MyDividerItemDecoration(getContext(), DividerItemDecoration.VERTICAL, 36));
        TagAdapter  mAdapter = new TagAdapter(tagLists,getContext(),this);
        rv_list.setAdapter(mAdapter);

        // listening to search query text change
        serchET.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                mAdapter.getFilter().filter(s);

            }

            @Override
            public void afterTextChanged(Editable s) {


            }
        });

        cancelTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.cancel();
            }
        });
        doneTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String text = "";
                selectedTagInteger.clear();
                newSelectedtag.clear();
                List<TagList> TagList =mAdapter.getTagLists();
                for (TagList model : TagList) {
                    if (model.isSelected()) {
                        text += model.getTagName()+", ";
                        selectedtag.add(model);
                        if (!Validations.isEmptyString(String.valueOf(model.getId()))) {
                            selectedTagInteger.add(model.getId());
                        }else {
                            newSelectedtag.add(model.getTagName());
                        }
                    }
                }

                mAdapter.notifyDataSetChanged();
                String selectedTag= removeLastChar(text.trim());
                dialog.cancel();
            }
        });


        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = dialog.getWindow();
        lp.copyFrom(window.getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);
        dialog.show();

    }



    private void SelectImageFromLibrary() {
        if (!marshMallowPermission.checkPermissionForExternalStorage()) {
            marshMallowPermission.requestPermissionForExternalStorage();
        } else {
            Intent intent = new Intent(
                    Intent.ACTION_PICK,
                    android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            intent.setType("image/*");
            startActivityForResult(Intent.createChooser(intent, "Select File"),
                    SELECT_FILE);
        }
    }

    private void SlectImageFromCamera() {
        if (!marshMallowPermission.checkPermissionForCamera() && !marshMallowPermission.checkPermissionForExternalStorage()) {
            marshMallowPermission.requestPermissionForCamera();
            marshMallowPermission.requestPermissionForExternalStorage();
        } else if (!marshMallowPermission.checkPermissionForCamera()) {
            marshMallowPermission.requestPermissionForCamera();
        } else if (!marshMallowPermission.checkPermissionForExternalStorage()) {
            marshMallowPermission.requestPermissionForExternalStorage();
        } else {
            Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            if (takePictureIntent.resolveActivity(getActivity().getPackageManager()) != null) {
                startActivityForResult(takePictureIntent, TakePicture);
            }
        }
    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        // resultImage=true;
        String imageFileName = System.currentTimeMillis() + ".png";
        if (resultCode == RESULT_OK) {
            if (requestCode == TakePicture) {
                bitmap = (Bitmap) data.getExtras().get("data");
                ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
                uploadImage.setImageBitmap(bitmap);
                Glide.with(getActivity()).load(bitmap).apply(new RequestOptions().centerCrop().placeholder(R.drawable.profile_pics)).into(uploadImage);
                //imgBase64 = utility.image_to_Base64(getContext(), utility.getPath(utility.getImageUri(getContext(), bitmap), getContext()));

            }
            if (requestCode == SELECT_FILE) {
                try {
                    //bitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), data.getData());
                    sourceFile = new File(utility.getPath(data.getData(), getContext()));
                    bitmap = new Compressor(getContext()).compressToBitmap(sourceFile);
                    //imgBase64 = utility.image_to_Base64(getContext(), utility.getPath(data.getData(), getContext()));
                    uploadImage.setImageBitmap(bitmap);
                    Glide.with(getActivity()).load(sourceFile).apply(new RequestOptions().circleCrop().placeholder(R.drawable.profile_pics)).into(uploadImage);

                } catch (IOException e) {
                    e.printStackTrace();
                    Toast.makeText(getContext(), "Image not set please try again", Toast.LENGTH_SHORT).show();
                }

            }
                /*bitmap = utility.decodeSampledBitmapFromResource(data.getExtras().get("data").toString(), 100, 100);
                img_1.setVisibility(View.VISIBLE);
                img_1.setImageBitmap(bitmap);*/
        }
        if (requestCode == LOCATION_SETTINGS_REQUEST) {
            // user is back from location settings - check if location services are now enabled
            if (location.hasLocationEnabled()){
                latitude = location.getLatitude();
                longitude = location.getLongitude();
            }
        }
        if (requestCode==1001) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED ||
                        ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED
                        ) {

                    if (location.hasLocationEnabled()){
                        latitude = location.getLatitude();
                        longitude = location.getLongitude();
                    }
                } else {
                    requestPermissions(new String[]{Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION,
                            }, 1001);
                }
            }
        }
    }

    /*pop to select user*/
    public void tagPeopleDialog(List<UserList> userList) {

        final Dialog dialog = new Dialog(getContext());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_tag_people);
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(true);
        final RecyclerView rv_list = dialog.findViewById(R.id.tag_people_list);
        final TextView cancelTxt = dialog.findViewById(R.id.dialog_cancel_text);
        final TextView DoneTxt = dialog.findViewById(R.id.dialog_done_text);
        final EditText serchET = dialog.findViewById(R.id.tag_people_search_view_edt);

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getContext());
        rv_list.setLayoutManager(mLayoutManager);
        rv_list.setItemAnimator(new DefaultItemAnimator());
        rv_list.addItemDecoration(new MyDividerItemDecoration(getContext(), DividerItemDecoration.VERTICAL, 36));
        TagPeopleAdapter  mAdapter = new TagPeopleAdapter(userList,getContext(),  this);
        rv_list.setAdapter(mAdapter);

       /* RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getContext());
        rv_list.addItemDecoration(new MyDividerItemDecoration(getContext(), DividerItemDecoration.VERTICAL, 36));
        rv_list.setLayoutManager(layoutManager);
        rv_list.setAdapter(new TagPeopleAdapter(userList, getContext(), new TagPeopleAdapter.TapPeopleListener() {
            @Override
            public void onPeopleSelected(UserList tagPeople) {
                *//*String text = "";
                for (UserList model : userList) {
                    if (model.isSelected()) {
                        text +=model.getName()+", ";
                    }
                }
                Log.d("TAG","Output : " + text);*//*
                utility.showToast(getContext(),tagPeople.getName());
                Log.d("data","Output : " +tagPeople.getName());
            }
        }));*/

        DoneTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //String text = "";
                //List<TagList> TagList =mAdapter.getTagLists();
                selectedPeople.clear();
                selectedTagInteger.clear();
                String text = "";
                for (UserList model : userList) {
                    if (model.isSelected()) {
                        text += model.getName()+", ";
                        selectedPeople.add(model);
                        selectedPeopleInteger.add(model.getId());
                    }
                }
                String selectedTag= removeLastChar(text.trim());
                Log.d("TAG","Output : " + text);

                dialog.cancel();
            }
        });
        // listening to search query text change
        serchET.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                mAdapter.getFilter().filter(s);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });


        cancelTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.cancel();
            }
        });


        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = dialog.getWindow();
        lp.copyFrom(window.getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);
        dialog.show();

    }


    @Override
    public boolean onBackPressed() {
        if (myCondition) {
            //action not popBackStack
            return true;
        } else {
            getActivity().getSupportFragmentManager().popBackStack();
            return false;
        }
    }
}
