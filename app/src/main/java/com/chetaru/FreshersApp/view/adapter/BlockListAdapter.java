package com.chetaru.FreshersApp.view.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Filter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.chetaru.FreshersApp.R;
import com.chetaru.FreshersApp.service.model.BlockUser;
import com.chetaru.FreshersApp.utility.SessionParam;
import com.chetaru.FreshersApp.utility.Utility;
import com.chetaru.FreshersApp.view.ui.Fragment.BlockUserFragment;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class BlockListAdapter extends RecyclerView.Adapter<BlockListAdapter.MyViewHolder> {

    private List<BlockUser> blockUserList=new ArrayList<>();
    List<BlockUser> mFilteredList;
    Context mContext;
    blockListener mListener;

    private boolean isLoadingAdded = false;
    // View Types
    private static final int ITEM = 0;
    private static final int LOADING = 1;
    private boolean retryPageLoad = false;
    private String errorMsg;
    Utility utility;
    SessionParam sessionParam;

    public BlockListAdapter(List<BlockUser> blockUserList, Context context,blockListener mListener) {
        this.blockUserList=blockUserList;
        this.mFilteredList=blockUserList;
        this.mContext=context;
        this.utility=new Utility();
        this.sessionParam=new SessionParam(mContext);
        this.mListener=mListener;
    }


    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        MyViewHolder viewHolder=null;
        LayoutInflater inflater=LayoutInflater.from(parent.getContext());
        switch (viewType){
            case ITEM:
                View view=inflater.inflate(R.layout.block_list,parent,false);
                viewHolder=new MyViewHolder(view);
                break;
            case LOADING:
                View viewLoading=inflater.inflate(R.layout.item_progress,parent,false);
                viewHolder=new LoadingVH(viewLoading);
        }
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {

        try {
            BlockUser list = mFilteredList.get(position);
            holder.userName.setText(list.getName().toString());
            //Picasso.with(mContext).load(list.getImageUrl()).into(holder.imageLogo);
            Glide.with(mContext).load(list.getImageUrl()).placeholder(R.drawable.profile_pics).into(holder.imageLogo);


            holder.unBlockTv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mListener.unblockUser(list);
                }
            });
        }catch (Exception e){
            e.printStackTrace();
        }

    }



    public class MyViewHolder extends RecyclerView.ViewHolder {
        ImageView imageLogo;
        TextView userName,unBlockTv;
        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            userName=itemView.findViewById(R.id.block_user_Name);
            imageLogo=itemView.findViewById(R.id.iv_logo);
            unBlockTv=itemView.findViewById(R.id.unblock_tv);
        }
    }

    public class LoadingVH extends MyViewHolder implements View.OnClickListener

    {
        private ProgressBar mProgressBar;
        private ImageButton mRetryBtn;
        private TextView mErrorTxt;
        private LinearLayout mErrorLayout;

        public LoadingVH(View itemView) {
            super(itemView);
            mProgressBar = itemView.findViewById(R.id.loadmore_progress);
            mRetryBtn = itemView.findViewById(R.id.loadmore_retry);
            mErrorTxt = itemView.findViewById(R.id.loadmore_errortxt);
            mErrorLayout = itemView.findViewById(R.id.loadmore_errorlayout);
            mRetryBtn.setOnClickListener(this);
            mErrorLayout.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.loadmore_retry:
                case R.id.loadmore_errorlayout:
                    showRetry(false, null);
                    //mCallback.retryPageLoad();
                    break;
            }
        }
    }
    public void showRetry(boolean show, @Nullable String errorMsg) {
        retryPageLoad = show;
        notifyItemChanged(mFilteredList.size() - 1);

        if (errorMsg != null) this.errorMsg = errorMsg;
    }


    @Override
    public int getItemCount() {

        return mFilteredList == null ? 0 : mFilteredList.size();
    }



    public Filter getFilter() {

        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {

                String charString = charSequence.toString();

                if (charString.isEmpty()) {

                    mFilteredList = blockUserList;
                } else {
                    ArrayList<BlockUser> filteredList = new ArrayList<>();

                    for (BlockUser addActionUser : blockUserList) {

                        if (addActionUser.getName().toLowerCase().contains(charString.toLowerCase()) ||
                                addActionUser.getName().toUpperCase().contains(charString.toUpperCase())) {
                            filteredList.add(addActionUser);
                        }
                    }
                    mFilteredList = filteredList;

                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = mFilteredList;
                return filterResults;

            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                mFilteredList = (ArrayList<BlockUser>) filterResults.values;
                notifyDataSetChanged();


            }
        };
    }

    public void add(BlockUser r) {
        if (r.getId()!=null) {
            mFilteredList.add(r);
            notifyItemInserted(mFilteredList.size() - 1);
        }
    }

    public void addAll(List<BlockUser> noti_Results) {
        for (BlockUser result : noti_Results) {
            add(result);
        }
    }

    public void remove(BlockUser r) {
        int position = mFilteredList.indexOf(r);
        if (position > -1) {
            mFilteredList.remove(position);
            notifyItemRemoved(position);
        }
    }
    public void clear() {
        try {
            while (getItemCount() > 0) {
                remove(getItem(0));
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public void addLoadingFooter() {
        isLoadingAdded = true;
        add(new BlockUser());
    }

    public void removeLoadingFooter() {
        isLoadingAdded = false;

        int position = mFilteredList.size() - 1;
        BlockUser result = getItem(position);

        if (result != null) {
            mFilteredList.remove(position);
            notifyItemRemoved(position);
        }
    }


    @Override
    public int getItemViewType(int position) {
        return (position == mFilteredList.size() - 1 && isLoadingAdded) ? LOADING : ITEM;
//        if(mFilteredList.size()<5){
//            return mFilteredList.size() - 1;
//        }else{
//            return (position == mFilteredList.size() - 1 && isLoadingAdded) ? LOADING : ITEM;
//        }
    }

    public BlockUser getItem(int position) {
        return mFilteredList.get(position);
    }

    public interface blockListener{
        public void unblockUser(BlockUser blockUser);
    }
}
