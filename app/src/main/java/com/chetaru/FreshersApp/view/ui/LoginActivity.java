package com.chetaru.FreshersApp.view.ui;

import androidx.annotation.Nullable;

import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;


import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;

import android.os.Build;
import android.os.Bundle;

import android.text.Editable;


import android.text.TextWatcher;

import android.util.Log;
import android.view.View;

import android.view.WindowManager;
import android.widget.Button;

import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;


import com.chetaru.FreshersApp.R;

import com.chetaru.FreshersApp.service.model.UserLoginRequest;
import com.chetaru.FreshersApp.service.model.UserResponse;
import com.chetaru.FreshersApp.service.repository.MyViewModelFactory;

import com.chetaru.FreshersApp.service.retrofit.BaseRequest;
import com.chetaru.FreshersApp.service.retrofit.Functions;
import com.chetaru.FreshersApp.service.retrofit.RequestReciever;
import com.chetaru.FreshersApp.utility.BaseActivity;
import com.chetaru.FreshersApp.utility.ErrorLayout;
import com.chetaru.FreshersApp.utility.MarshMallowPermission;
import com.chetaru.FreshersApp.utility.SessionParam;
import com.chetaru.FreshersApp.utility.Utility;
import com.chetaru.FreshersApp.utility.Validations;
import com.chetaru.FreshersApp.view.adapter.LoginDataAdapter;
import com.chetaru.FreshersApp.view.ui.Activity.HomeActivity;

import com.chetaru.FreshersApp.viewModel.LoginViewModel;


import com.google.gson.JsonObject;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Optional;



public class LoginActivity extends BaseActivity implements View.OnClickListener,LoginDataAdapter.loginListener{

    BaseRequest baseRequest;

    LoginViewModel viewModel;
    @Nullable
    @BindView(R.id.user_name_et)
    EditText emailEditText;
    @Nullable
    @BindView(R.id.password_et)
    EditText passwordEditText;
    @Nullable
    @BindView(R.id.login_button)
    Button loginButton;
    @Nullable
    @BindView(R.id.sign_up_txt)
    TextView createAccountTextView;
    @Nullable
    @BindView(R.id.sign_up_sceond_txt)
    TextView signUpTextView;
    @Nullable
    @BindView(R.id.switch_sceond_txt)
    TextView switchAccoiuntTextView;

    @Nullable
    @BindView(R.id.forget_password_txt)
    TextView forgetPasswordTextView;
    @BindView(R.id.remember_login_recyclerview)
    RecyclerView login_recyclerView;
    @BindView(R.id.show_login_recycler_layout)
    LinearLayout showRecyclerViewLayout;
    @BindView(R.id.show_login_layout)
    LinearLayout showLoginLayout;
    LifecycleOwner lifecycleOwner;
    MarshMallowPermission marshMallowPermission;
    String deviceId = "";
    SessionParam sessionParam;
    final private int REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS = 124;

    @BindView(R.id.rb_user)
    RadioButton rbUser;
    @BindView(R.id.rb_admin)
     RadioButton rbAdmin;

    LoginDataAdapter mAdapter;
    List<UserLoginRequest> loginUserList;

    HashMap<String ,String> loginDetails;
    Utility utility;
    String role="2";
    private ErrorLayout errorLayout;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
        loginUserList=new ArrayList<>();
        loginDetails=new HashMap<>();
        sessionParam = new SessionParam(this);
        sessionParam.clearPreferences(this);

        try {
            loginUserList = sessionParam.retriveLoginData(this);
        }catch (Exception e){
            e.printStackTrace();

        }
        marshMallowPermission = new MarshMallowPermission(this);
       /* for (int i=0;i<5;i++){
            UserLoginRequest request=new UserLoginRequest();
            request.setEmail("sdas");
            request.setName("ritesh");
            loginUserList.add(i,request);
        }*/
       // errorLayout = new ErrorLayout(findViewById(R.id.error_rl));
        utility=new Utility();
        utility.getProgerss(this);

       // showLoginLayout.setVisibility(View.GONE);
       // showRecyclerViewLayout.setVisibility(View.VISIBLE);
        if (loginUserList!=null) {
            mAdapter=new LoginDataAdapter(loginUserList,this,this);
            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
            login_recyclerView.setLayoutManager(linearLayoutManager);
            login_recyclerView.setItemAnimator(new DefaultItemAnimator());
            login_recyclerView.setAdapter(mAdapter);
            if (loginUserList.size() > 0) {
                showLoginLayout.setVisibility(View.GONE);
                showRecyclerViewLayout.setVisibility(View.VISIBLE);
            } else {
                showLoginLayout.setVisibility(View.VISIBLE);
                showRecyclerViewLayout.setVisibility(View.GONE);
            }
        }else {
            showLoginLayout.setVisibility(View.VISIBLE);
            showRecyclerViewLayout.setVisibility(View.GONE);
        }
        // Create a ViewModel the first time the system calls an activity's onCreate() method.
        // Re-created activities receive the same MyViewModel instance created by the first activity.
        //viewModel = new  ViewModelProvider(this).get(LoginViewModel.class);
        //viewModel = ViewModelProviders.of(this, new MyViewModelFactory(this.getApplication(), "","")).get(LoginViewModel.class);
        viewModel = ViewModelProviders.of(this, new MyViewModelFactory(this.getApplication())).get(LoginViewModel.class);

        //SharedPreferencesManager.init(BaseApplication.instance());
        emailEditText=findViewById(R.id.user_name_et);
        passwordEditText=findViewById(R.id.password_et);
        createAccountTextView=findViewById(R.id.sign_up_txt);
        forgetPasswordTextView=findViewById(R.id.forget_password_txt);
        loginButton=findViewById(R.id.login_button);


       // createAccountTextView.setPaintFlags(createAccountTextView.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG|Paint.FAKE_BOLD_TEXT_FLAG);
        //forgetPasswordTextView.setPaintFlags(forgetPasswordTextView.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG|Paint.FAKE_BOLD_TEXT_FLAG);
        /*SpannableString text = new SpannableString("forgetPassword");
        text.setSpan(new UnderlineSpan(), 25, 6, 0);
        forgetPasswordTextView.setText(text);*/

       // permission();

        deviceId= utility.getAndroidID(this);
        sessionParam.saveDeviceId(this, deviceId);
        initView();
    }

    @Nullable
    private void initView() {
        emailEditText.addTextChangedListener(new MyTextWatcher(emailEditText));
        passwordEditText.addTextChangedListener(new MyTextWatcher(passwordEditText));
        signUpTextView.setOnClickListener(this);
        switchAccoiuntTextView.setOnClickListener(this);
        createAccountTextView.setOnClickListener(this);
        forgetPasswordTextView.setOnClickListener(this);
        rbAdmin.setOnClickListener(this);
        rbUser.setOnClickListener(this);
        loginButton.setOnClickListener(this);



        /*viewModel = ViewModelProviders.of(this, new ViewModelProvider.Factory() {
            @SuppressWarnings("unchecked")
            @Override
            public <T extends ViewModel> T create(final Class<T> modelClass) {
                if (modelClass.equals(LoginViewModel.class)) {
                    return (T) new LoginViewModel("","");
                } else {
                    return null;
                }
            }
        }).get(LoginViewModel.class);*/

    }

    @Override
    public void removeRecord(UserLoginRequest loginUser) {
        if (loginUserList!=null) {
            for (int i=0;i<loginUserList.size();i++){
                if (loginUser.getEmail().equalsIgnoreCase(loginUserList.get(i).getEmail())){
                    loginUserList.remove(i);
                }
            }
        }
        mAdapter.notifyDataSetChanged();

    }

    @Override
    public void loginUserWith(UserLoginRequest loginUser) {

        if (loginUser!=null){
            loginApi(loginUser.getEmail(),loginUser.getPassword());
        }
    }

    //call on text change for validation
    private class MyTextWatcher implements TextWatcher {

        private View view;

        private MyTextWatcher(View view) {
            this.view = view;
        }

        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void afterTextChanged(Editable editable) {
            switch (view.getId()) {
                case R.id.user_name_et:
                    Validations.isValidEmail(emailEditText.getText().toString().trim());
                    break;
                case R.id.password_et:
                    Validations.isValidPassword(passwordEditText.getText().toString().trim());
                    break;
            }
        }
    }

    private void requestFocus(View view) {
        if (view.requestFocus()) {
            if (this!=null)
                this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }

   /* //password validation
    private boolean validatePassword() {
        if (passwordEditText.getText().toString().trim().isEmpty()) {
            passwordEditText.setError(getString(R.string.err_msg_password));
            requestFocus(passwordEditText);
            return false;
        } else {
            passwordEditText.setErrorEnabled(false);
        }

        return true;
    }

    //Email validation
    private boolean validateEmail() {
        String email = emailEditText.getText().toString().trim();

        if (email.isEmpty() || !Validations.isValidEmail(email)) {
            layoutEmail.setError(getString(R.string.err_msg_email));
            requestFocus(emailEditText);
            return false;
        } else {
            layoutEmail.setErrorEnabled(false);
        }

        return true;
    }*/

    /*request user for certain permission*/
    private void permission() {
        //datafinish = true;
        List<String> permissionsNeeded = new ArrayList<String>();
        final List<String> permissionsList = new ArrayList<String>();
        if (!addPermission(permissionsList, Manifest.permission.ACCESS_FINE_LOCATION))
            permissionsNeeded.add("GPS");
        if (!addPermission(permissionsList, Manifest.permission.ACCESS_NETWORK_STATE))
            permissionsNeeded.add("Network state");
        if (!addPermission(permissionsList, Manifest.permission.ACCESS_COARSE_LOCATION))
            permissionsNeeded.add("Location");
        if (!addPermission(permissionsList, Manifest.permission.INTERNET))
            permissionsNeeded.add("Internet");
        if (!addPermission(permissionsList, Manifest.permission.READ_PHONE_STATE))
            permissionsNeeded.add("phone status");
        if (!addPermission(permissionsList, Manifest.permission.CAMERA))
            permissionsNeeded.add("Camera");
        if (!addPermission(permissionsList, Manifest.permission.WRITE_EXTERNAL_STORAGE))
            permissionsNeeded.add("Write storage");
        if (permissionsList.size() > 0) {
            if (permissionsNeeded.size() > 0) {
                // Need Rationale
                String message = "You need to grant access to " + permissionsNeeded.get(0);
                for (int i = 1; i < permissionsNeeded.size(); i++)
                    message = message + ", " + permissionsNeeded.get(i);
                /*showMessageOKCancel(message, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            requestPermissions(permissionsList.toArray(new String[permissionsList.size()]), REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS);
                        }
                    }
                });*/
                return;
            }
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermissions(permissionsList.toArray(new String[permissionsList.size()]), REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS);
            }
            return;
        }
        //init();
    }

    private boolean addPermission(List<String> permissionsList, String permission) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(permission) != PackageManager.PERMISSION_GRANTED) {
                permissionsList.add(permission);
                //Check for Rationale Optiong
                if (!shouldShowRequestPermissionRationale(permission))
                    return false;
            }
        }
        return true;
    }

    @Optional
    @OnClick({R.id.login_button,R.id.sign_up_txt,R.id.forget_password_txt,R.id.rb_user,R.id.rb_admin})
    @Override
    public void onClick(View view) {

        switch (view.getId()){
            case R.id.login_button:
                String email=emailEditText.getText().toString().trim();
                String password=passwordEditText.getText().toString().trim();
                if (Validations.isEmptyString(email)){
                    utility.showToast(LoginActivity.this, "Please enter Phone Number/Email.");
                    //Toast.makeText(this, "please enter email", Toast.LENGTH_SHORT).show();
                    return;
                }
                if (Validations.isEmptyString(password)){
                    utility.showToast(LoginActivity.this, "Please enter Password.");
                    return;
                }
               // loginPerform(email,password);
                loginApi(email,password);
                //observeViewModel();
                break;
            case R.id.sign_up_sceond_txt:
            case R.id.sign_up_txt:
                Intent intent1=new Intent(this,Verification_Activity.class);
                startActivity(intent1);
                break;
            case R.id.switch_sceond_txt:
                showRecyclerViewLayout.setVisibility(View.GONE);
                showLoginLayout.setVisibility(View.VISIBLE);
                break;
            case R.id.forget_password_txt:
                Intent intent=new Intent(this, ForgetPassword_Activity.class);
                startActivity(intent);
                finish();
                break;
            case R.id.rb_user:
                role="2";
                break;
            case R.id.rb_admin:
                role="1";
                break;


        }
    }

    private void loginPerform(String email,String password) {

        String myToken="";
        try {
            utility.loading();
            viewModel.getUser(email,password,deviceId,myToken).observe(this, new Observer<UserResponse>() {
                @Override
                public void onChanged(UserResponse user) {
                    utility.dismiss();
                    //Set UI

                    try {
                        if (user.getCode()==200) {
                            Log.d("loginData", user.getMessage());
                            utility.showToast(LoginActivity.this,user.getMessage());
                            JSONObject object=new JSONObject();
                            object=user.getUserData();
                            sessionParam = new SessionParam((JSONObject) object);
                            sessionParam.persist(LoginActivity.this);
                            sessionParam.token=user.getUserData().getToken();
                            Intent intent=new Intent(LoginActivity.this, DefalutActivity.class);
                            startActivity(intent);
                            finish();
                        }else {
                            utility.showToast(LoginActivity.this,user.getMessage());
                        }

                    }catch (Exception e){
                        e.printStackTrace();
                        utility.dismiss();
                    }
                }
            });
            viewModel.getError().observe(this, isError -> {


            });
            viewModel.getLoading().observe(this, isLoading -> {
            });
        }catch (Exception e){
            e.printStackTrace();
            utility.dismiss();
        }
        /*if (!validateEmail()){
            return;
        }
        if (!Validations.isValidEmail(email)) {

            layoutEmail.setError(getString(R.string.err_msg_email));
            requestFocus(emailEditText);
            return;
        }
        else {
            layoutEmail.setErrorEnabled(false);
        }
        if (!validatePassword()) {
            return;
        }*/
        // userLogin(email,password);
        //initViewModels(email,password);
        // mViewModel.getcustomerInfo();

       // new LoginViewModel(email,password);
       // listViewModel.getCustomer();

    }
    /*private void observeViewModel() {
        //before Lambdas
        *//*
        created anonymous single class that has a single method
        that has a single value
         *//*
        viewModel.getRepos().observe(this, repos -> {
            if (repos != null) {
                //listview.setVisibility(View.VISIBLE);
                listview.setHasFixedSize(true);
                Log.d("LoginActivity",repos.getMessage());
            }
        });

        viewModel.getError().observe(this, isError -> {
            if (isError) {
                textView_error.setVisibility(View.VISIBLE);
                listview.setVisibility(View.GONE);
                textView_error.setText(R.string.api_error_repos);
            } else {
                textView_error.setVisibility(View.GONE);
                textView_error.setText(null);
            }

        });

        viewModel.getLoading().observe(this, isLoading -> {
            progressBar.setVisibility(isLoading ? View.VISIBLE : View.GONE);
        });
    }*/


    /*is api call used for calling login API
     */
    public void loginApi(String email, String password) {
        baseRequest = new BaseRequest(this);
        baseRequest.setBaseRequestListner(new RequestReciever() {
            @Override
            public void onSuccess(int requestCode, String Json, Object object) {
                try {
                    sessionParam = new SessionParam((JSONObject) object);
                    try {
                        sessionParam.persist(mContext);
                    }catch (Exception e){
                        e.printStackTrace();
                    }

                        //finishAllActivities();
                        sessionParam.saveToken(LoginActivity.this, "y");
                        String token=sessionParam.token;
                        //Save login creidential


                        if (loginUserList!=null) {
                            boolean newValue=false;

                                for (UserLoginRequest log:loginUserList){
                                   if (log.getEmail().equalsIgnoreCase(email)){
                                       newValue=false;
                                   }else
                                   {
                                       newValue=true;
                                   }
                                }


                            if (newValue){
                                for (int i=0;i<loginUserList.size();i++) {
                                    if (email.equalsIgnoreCase(loginUserList.get(i).getEmail())) {
                                        UserLoginRequest newUser = new UserLoginRequest();
                                        newUser.setProfileImage(((JSONObject) object).getString("profileImage"));
                                        newUser.setName(((JSONObject) object).getString("name"));
                                        newUser.setEmail(email);
                                        newUser.setPassword(password);
                                        loginUserList.set(i,newUser);
                                        break;
                                    }else {
                                        UserLoginRequest newUser = new UserLoginRequest();
                                        newUser.setProfileImage(((JSONObject) object).getString("profileImage"));
                                        newUser.setName(((JSONObject) object).getString("name"));
                                        newUser.setEmail(email);
                                        newUser.setPassword(password);
                                        loginUserList.add(newUser);
                                    }
                                }
                            }

                            loginUserList=removeDuplicates(loginUserList);
                            sessionParam.loginUserListSave(LoginActivity.this,loginUserList);
                        }else {
                            sessionParam.loginSave(LoginActivity.this,email,password,(JSONObject) object);
                        }
                        //sessionParam.loginUserListSave();
                        Log.d("LoginToken",token);
                    //sessionParam.saveFCM_Token(LoginActivity.this,token);
                    Intent intent=new Intent(LoginActivity.this,HomeActivity.class);
                        startActivity(intent);
                        finish();
                        BaseActivity.finishAllActivitiesStatic();
                        //startActivity(new Intent(mContext, HomeActivity.class));

                        //apiTellSidLogin();




                }catch (Exception e){
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int requestCode, String errorCode, String message) {
                //errorLayout.showError(message);
                passwordEditText.setText("");
                utility.showToast(LoginActivity.this,message);
            }

            @Override
            public void onNetworkFailure(int requestCode, String message) {
               // errorLayout.showError(message);
                utility.showToast(LoginActivity.this,message);
            }
        });
        JsonObject object = Functions.getClient().getJsonMapObject("userName", email,
                "password", password,
                "role", 2 +"" ,
                "deviceId", deviceId,
                "fcmToken", sessionParam.fcm_token
        );
        baseRequest.callAPIPost(1, object, getString(R.string.login));
    }

    public List<UserLoginRequest> removeDuplicates(List<UserLoginRequest> list) {
        // Set set1 = new LinkedHashSet(list);
        Set set = new TreeSet(new Comparator() {
            @Override
            public int compare(Object o1, Object o2) {
                if(((UserLoginRequest)o1).getEmail().equalsIgnoreCase(((UserLoginRequest)o2).getEmail()) ) {
                    return 0;
                }
                return 1;
            }
        });
        set.addAll(list);
        final List newList = new ArrayList(set);
        return newList;
    }
}
