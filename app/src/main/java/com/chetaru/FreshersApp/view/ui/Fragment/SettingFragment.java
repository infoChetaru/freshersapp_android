package com.chetaru.FreshersApp.view.ui.Fragment;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.chetaru.FreshersApp.R;
import com.chetaru.FreshersApp.service.model.MyProfileResponse;
import com.chetaru.FreshersApp.service.model.TodaysLike;
import com.chetaru.FreshersApp.service.retrofit.BaseRequest;
import com.chetaru.FreshersApp.service.retrofit.Functions;
import com.chetaru.FreshersApp.service.retrofit.RequestReciever;
import com.chetaru.FreshersApp.utility.BaseActivity;
import com.chetaru.FreshersApp.utility.SessionParam;
import com.chetaru.FreshersApp.utility.Utility;
import com.chetaru.FreshersApp.utility.Validations;
import com.chetaru.FreshersApp.view.adapter.TodayLikeAdapter;
import com.chetaru.FreshersApp.view.customView.ItemOffsetDecoration;
import com.chetaru.FreshersApp.view.ui.Activity.RootActivity;
import com.google.gson.Gson;
import com.google.gson.JsonObject;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link SettingFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class SettingFragment extends Fragment implements View.OnClickListener, TodayLikeAdapter.todayLikeListener {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    //assign a layout id class
    private Unbinder unbinder;
    //handel class
    Utility utility;
    SessionParam sessionParam;
    int userId,fcmId;
    private BaseRequest baseRequest;
    MyProfileResponse myProfile;
    Boolean expand = false,loginExpand=false;
    List<TodaysLike> todayList;
    int likeCount;
    View viewString;

    //menu option Initialize

    @BindView(R.id.back_setting_image)
    ImageView backImageClick;
    @BindView(R.id.like_post_layout)
    LinearLayout likePostLayout;

    @BindView(R.id.today_msg_txt)
    TextView todayMsgText;
    @BindView(R.id.no_top_like_layout)
    LinearLayout noTopLikeLayout;
    @BindView(R.id.today_like_recycler_layout)
    LinearLayout todayRecyclerLayout;
    @BindView(R.id.like_image_recyclerView)
    RecyclerView likeRecyclerView;
    @BindView(R.id.expand_imageView)
    ImageView expendImage;
    @BindView(R.id.detail_expand_imageView)
    ImageView detailExpand;
    @BindView(R.id.menu_edit_profile_tv)
    TextView menuEditProfile;
    @BindView(R.id.menu_login_detail_tv)
    TextView menuLoginDetail;
    @BindView(R.id.menu_my_upload_tv)
    TextView menuMyUpload;
    @BindView(R.id.menu_my_likes_tv)
    TextView menuMyLikes;
    @BindView(R.id.menu_premium_tv)
    TextView menuPremium;
    @BindView(R.id.menu_today_like_tv)
    TextView menuTodayLike;
    @BindView(R.id.menu_block_user_tv)
    TextView menuBlockUser;
    @BindView(R.id.menu_terms_of_use_tv)
    TextView menuTermsUse;
    @BindView(R.id.menu_privacy_policy_tv)
    TextView menuPrivacyPolicy;
    @BindView(R.id.menu_contact_us_tv)
    TextView menuContactUs;
    @BindView(R.id.menu_logout_tv)
    TextView menuLogout;
    @BindView(R.id.login_detail_layout)
    LinearLayout detailLayout;
    @BindView(R.id.login_detail_name)
    TextView loginDetailName;
    @BindView(R.id.setting_check_box)
    CheckBox checkBoxValue;
    TodayLikeAdapter mAdapter;
    settingListener mListener;
    int sensitiveContent=2,changeContent=2;

    public SettingFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment SettingFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static SettingFragment newInstance(String param1, String param2) {
        SettingFragment fragment = new SettingFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_setting, container, false);
        unbinder = ButterKnife.bind(this, view);
        utility = new Utility();
        sessionParam = new SessionParam(getContext());
        myProfile=new MyProfileResponse();
        userId = sessionParam.id;
        fcmId = sessionParam.fcmId;
        todayList=new ArrayList<>();
        viewString=view;
        if (sessionParam.premieUser){
            menuPremium.setText("You are Premium Member");
        }else {
            menuPremium.setText("Upgrade to premium");
        }
        //call api
        geProfile();
        detailLayout.setVisibility(View.GONE);
        initView();
        // Set Horizontal Layout Manager
        // for Recycler view
        LinearLayoutManager layoutManager = new LinearLayoutManager( getContext(),LinearLayoutManager.HORIZONTAL,false);
        likeRecyclerView.setLayoutManager(layoutManager);
        likeRecyclerView.setHasFixedSize(true);
        ItemOffsetDecoration itemDecoration = new ItemOffsetDecoration(getContext(), R.dimen.item_offset_medium);
        likeRecyclerView.addItemDecoration(itemDecoration);
        mAdapter=new TodayLikeAdapter(todayList,getContext(),likeCount,this);
        likeRecyclerView.setAdapter(mAdapter);
       // setProfileData( myProfile);
        return view;
    }

    @Override
    public void onPause() {
        super.onPause();
        if (myProfile==null){
            geProfile();
        }
    }

    private void setProfileData(MyProfileResponse myProfile) {

        likeCount = myProfile.getLikedUserCount();


        sensitiveContent=myProfile.getSensitivePostsAllowed();
        if (sensitiveContent==1)
        {
            checkBoxValue.setChecked(true);
        }else {
            checkBoxValue.setChecked(false);
        }

        if (myProfile.getPremieUser()){
            menuPremium.setText("You are Premium Member");
        }else {
            menuPremium.setText("Upgrade to premium");
        }
        try {
            if (!Validations.isEmptyString(myProfile.getPostImageUrl())) {
                // ArrayList aList= new ArrayList(Arrays.asList(myProfile.getPostImageUrl().split(",")));
                // List<String> items = Arrays.asList(myProfile.getPostImageUrl().split(","));
                List<String> profileImages=Arrays.asList(myProfile.getPostImageUrl().split("\\s*,\\s*"));
                List<String> postIds = Arrays.asList(myProfile.getPostId().split(","));
                todayList.clear();

                for (int i=0;i<profileImages.size();i++){
                    TodaysLike like=new TodaysLike();
                    like.setPostId(Integer.valueOf(postIds.get(i)));
                    like.setProfileImage(profileImages.get(i));
                    todayList.add(like);
                }


               // mAdapter.addAll(todayList);
            }

        }catch (Exception e){
            e.printStackTrace();
        }
        mAdapter=new TodayLikeAdapter(todayList,getContext(),likeCount,this);
        likeRecyclerView.setAdapter(mAdapter);
        mAdapter.notifyDataSetChanged();
    }

    private void geProfile() {
        baseRequest = new BaseRequest(getContext());
        baseRequest.setBaseRequestListner(new RequestReciever() {
            @Override
            public void onSuccess(int requestCode, String Json, Object object) {
                try {

                    Gson gson = new Gson();
                    JSONObject jsonObject = new JSONObject(Json);
                    JSONObject response = (JSONObject) jsonObject.get("data");
                   /* try {
                        sessionParam = new SessionParam((JSONObject) response);
                        sessionParam.persist(getContext());
                    }catch (Exception e){
                        e.printStackTrace();
                    }*/
                    myProfile = gson.fromJson(response.toString(), MyProfileResponse.class);
                    setProfileData(myProfile);
                    if (!Validations.isEmptyString(mParam1)){
                        if (myProfile.getPremieUser())
                        getExpendClick();
                        mParam1="";
                    }
                    sessionParam = new SessionParam(getContext(),myProfile);
                     //sessionParam.persist(getContext());

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int requestCode, String errorCode, String message) {
                //utility.showToast(getContext(), message);
            }

            @Override
            public void onNetworkFailure(int requestCode, String message) {

                utility.showToast(getContext(), message);
            }
        });



        JsonObject main_object = new JsonObject();
        main_object.addProperty("userId",sessionParam.id );
        baseRequest.callAPIPostWOLoader(1, main_object, getString(R.string.api_getUserProfile));
    }
    private void initView() {
        backImageClick.setOnClickListener(this);
        expendImage.setOnClickListener(this);
        menuEditProfile.setOnClickListener(this);
        menuMyUpload.setOnClickListener(this);
        menuMyLikes.setOnClickListener(this);
        menuPremium.setOnClickListener(this);
        menuTodayLike.setOnClickListener(this);
        menuBlockUser.setOnClickListener(this);
        menuTermsUse.setOnClickListener(this);
        menuPrivacyPolicy.setOnClickListener(this);
        menuContactUs.setOnClickListener(this);
        menuLogout.setOnClickListener(this);
        detailExpand.setOnClickListener(this);
        menuLoginDetail.setOnClickListener(this);
        checkBoxValue.setOnClickListener(this);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        unbinder.unbind();
    }
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof settingListener) {
            mListener = (settingListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement TagChildFragment");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }
    private void changeSenContent(int changeContent) {
        baseRequest = new BaseRequest(getContext());
        baseRequest.setBaseRequestListner(new RequestReciever() {
            @Override
            public void onSuccess(int requestCode, String Json, Object object) {
                try {

                    Gson gson = new Gson();
                    JSONObject jsonObject = new JSONObject(Json);
                    String msg=jsonObject.getString("message");

                    geProfile();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int requestCode, String errorCode, String message) {
               // utility.showToast(getContext(), message);
            }

            @Override
            public void onNetworkFailure(int requestCode, String message) {

                utility.showToast(getContext(), message);
            }
        });

        JsonObject main_object = new JsonObject();
        main_object.addProperty("sensitivePostsAllowed",changeContent );

        baseRequest.callAPIPostWOLoader(1, main_object, getString(R.string.api_updateUserSensitivePostStatus));
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.setting_check_box:
                if (checkBoxValue.isChecked()){
                    sensitiveContent=1;
                    changeContent=1;
                    changeSenContent(changeContent);
                }else {
                    sensitiveContent=2;
                    changeContent=2;
                    changeSenContent(changeContent);
                }

                break;
            case R.id.back_setting_image:
                //mListener.settingClick("backClick");
                break;
            //menu Option click
            case R.id.menu_edit_profile_tv:
                /*editable=true;
                TitleNameTxt.setText("Profile");
                settingImageView.setVisibility(View.VISIBLE);
                recyclerViewLayout.setVisibility(View.GONE);
                showProfileLayout.setVisibility(View.VISIBLE);
                cancelImage.setVisibility(View.VISIBLE);
                doneImage.setVisibility(View.VISIBLE);
                editable = true;
                if (!marshMallowPermission.checkPermissionForCamera() && !marshMallowPermission.checkPermissionForExternalStorage()) {
                    marshMallowPermission.requestPermissionForCamera();
                    marshMallowPermission.requestPermissionForExternalStorage();
                } else if (!marshMallowPermission.checkPermissionForCamera()) {
                    marshMallowPermission.requestPermissionForCamera();
                } else if (!marshMallowPermission.checkPermissionForExternalStorage()) {
                    marshMallowPermission.requestPermissionForExternalStorage();
                } else {
                    maleIconImage.setVisibility(View.VISIBLE);
                    maleText.setVisibility(View.VISIBLE);
                    femaleIconImage.setVisibility(View.VISIBLE);
                    femaleText.setVisibility(View.VISIBLE);
                    otherIconImage.setVisibility(View.VISIBLE);
                    otherText.setVisibility(View.VISIBLE);

                    profileName.setEnabled(true);
                    profileBio.setEnabled(true);
                    //et_email.setEnabled(true);
                    profileAddress.setEnabled(true);
                    profileLogo.setEnabled(true);
                    maleIconImage.setEnabled(true);

                    femaleIconImage.setEnabled(true);
                    otherIconImage.setEnabled(true);
                }*/
                mListener.settingClick("profileEdit","");
                break;
            case R.id.menu_my_upload_tv:
                mListener.settingMyUpload("MyUploads",true);
                break;
            case R.id.menu_my_likes_tv:
                mListener.settingMyLike("MyLikes",true);
                break;
            case R.id.menu_premium_tv:
                mListener.settingPremiumUser("PremiumUser","premiumBack");
                break;
            case R.id.expand_imageView:
            case R.id.menu_today_like_tv:
                if(sessionParam.premieUser) {
                    getExpendClick();
                }else {
                    mListener.settingTodayPremium("todayPremiumUser","");
                }

                break;
            case R.id.menu_login_detail_tv:
            case R.id.detail_expand_imageView:
                getDetailExpand();
                break;
            case R.id.menu_block_user_tv:
                mListener.settingBlockUser("BlockedUsers",true);
                break;
            case R.id.menu_terms_of_use_tv:
                // mListener.newProfileClick(myProfile,"TermsOfUse");
                termsAndCondition();
                break;
            case R.id.menu_privacy_policy_tv:
                //mListener.newProfileClick(myProfile,"PrivacyPolicy");
                privacyApi();
                break;
            case R.id.menu_contact_us_tv:
                //mListener.newProfileClick(myProfile,"ContactUs");
                feedbackDialog();
                break;
            case R.id.menu_logout_tv:
                logoutDialog();
                break;
        }

    }

    public void logoutDialog() {
        final Dialog dialog = new Dialog(getContext());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_logout);
        dialog.setCanceledOnTouchOutside(true);
        TextView tv_cancel = dialog.findViewById(R.id.tv_cancel);
        TextView tv_logout = dialog.findViewById(R.id.tv_logout);
        tv_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        tv_logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getContext());
                SharedPreferences.Editor editor = prefs.edit();
                editor.clear();
                editor.commit();
                //apiTellSidLogout();
                api_logout();
            }
        });

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = dialog.getWindow();
        lp.copyFrom(window.getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);
        dialog.show();
    }
    public void api_logout() {
        baseRequest = new BaseRequest(getContext());
        baseRequest.setBaseRequestListner(new RequestReciever() {
            @Override
            public void onSuccess(int requestCode, String Json, Object object) {
                sessionParam.clearPreferences(getContext());
                startActivity(new Intent(getContext(), RootActivity.class));
                BaseActivity.finishAllActivitiesStatic();
            }

            @Override
            public void onFailure(int requestCode, String errorCode, String message) {
               // utility.showToast(getContext(), message);
            }

            @Override
            public void onNetworkFailure(int requestCode, String message) {
                utility.showToast(getContext(), message);
            }
        });
        JsonObject main_object = new JsonObject();
        main_object.addProperty("fcmId", fcmId);
        baseRequest.callAPIPost(1, main_object, getString(R.string.api_logout));
    }
    private void getDetailExpand() {
        if (!loginExpand) {
            loginExpand = true;
            detailExpand.setImageResource(R.drawable.ic_expand_less_white_24dp);
            detailLayout.setVisibility(View.VISIBLE);

            try {
                loginDetailName.setText(myProfile.getUserName());
                sensitiveContent = myProfile.getSensitivePostsAllowed();
                if (sensitiveContent == 1) {
                    checkBoxValue.setChecked(true);
                } else {
                    checkBoxValue.setChecked(false);
                }
            }catch (Exception e){
                e.printStackTrace();
            }
        }else {
            detailExpand.setImageResource(R.drawable.ic_expand_more_white_24dp);
            detailLayout.setVisibility(View.GONE);
            loginExpand=false;
        }
    }
    private void getExpendClick() {
        if (myProfile!=null) {
            if (!expand) {
                expand = true;
                likePostLayout.setVisibility(View.VISIBLE);
                expendImage.setImageResource(R.drawable.ic_expand_less_white_24dp);
                if (Validations.isEmptyString(myProfile.getPostId())) {
                    noTopLikeLayout.setVisibility(View.VISIBLE);
                    todayRecyclerLayout.setVisibility(View.GONE);
                    todayMsgText.setText("No post for the day.");
                } else if (myProfile.getLikedUserCount() == 0) {
                    noTopLikeLayout.setVisibility(View.VISIBLE);
                    todayRecyclerLayout.setVisibility(View.GONE);
                    todayMsgText.setText("No top liked posts for the day.");
                } else {
                    todayRecyclerLayout.setVisibility(View.VISIBLE);
                    noTopLikeLayout.setVisibility(View.GONE);
                }

            } else {
                likePostLayout.setVisibility(View.GONE);
                if (Validations.isEmptyString(myProfile.getPostId())) {
                    todayRecyclerLayout.setVisibility(View.GONE);
                    noTopLikeLayout.setVisibility(View.GONE);
                    todayMsgText.setText("No post for the day.");
                } else if (myProfile.getLikedUserCount() == 0) {
                    todayMsgText.setText("No top liked posts for the day.");
                } else {
                    noTopLikeLayout.setVisibility(View.GONE);
                    todayRecyclerLayout.setVisibility(View.GONE);
                }
                expand = false;

                expendImage.setImageResource(R.drawable.ic_expand_more_white_24dp);
            }
        }
    }

    private void feedbackDialog() {

        //SearchView searchView;

        final Dialog dialog = new Dialog(getContext());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.feedback_dialog);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        // dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);

        dialog.setCanceledOnTouchOutside(true);

        final Button submitButton = dialog.findViewById(R.id.submit_feed_button);
        final EditText feedEt = dialog.findViewById(R.id.feed_et_comment);

        submitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String feedComment=feedEt.getText().toString().trim();
                if (!Validations.isEmptyString(feedComment)) {
                    contactUsApi(feedComment);
                    dialog.cancel();
                }else {
                    utility.showToast(getContext(),"Please Enter feedback");
                }
            }
        });



        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = dialog.getWindow();
        lp.copyFrom(window.getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.MATCH_PARENT;
        window.setBackgroundDrawableResource(R.color.primary_trans);

        window.setAttributes(lp);
        dialog.show();

    }
    private void contactUsApi(String feedComment) {
        baseRequest = new BaseRequest(getContext());
        baseRequest.setBaseRequestListner(new RequestReciever() {
            @Override
            public void onSuccess(int requestCode, String Json, Object object) {
                try {

                    JSONObject jsonObject = new JSONObject(Json);
                  //  String message=jsonObject.getString("message");
                    //utility.showToast(getContext(),message);
                    suscessDialog();
                }catch (Exception e){
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int requestCode, String errorCode, String message) {
                //utility.showToast(getContext(),message);
            }

            @Override
            public void onNetworkFailure(int requestCode, String message) {

                utility.showToast(getContext(),message);
            }
        });

        /*{
            "postId":"10",
                "summary":"donated to was uploaded upon the single's release"
        }*/


        JsonObject main_object = new JsonObject();
        //main_object.addProperty("postId", selectedViewPost.getId());
        main_object.addProperty("summary", feedComment);
       /* JsonObject object = Functions.getClient().getJsonMapObject(
                "postId",
                "likeStatus",status );*/
        baseRequest.callAPIPost(1, main_object, getString(R.string.api_userContactUs));

    }

    private void suscessDialog() {
        final Dialog dialog = new Dialog(getContext());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.done_dialog);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        // dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        final Button submitButton = dialog.findViewById(R.id.done_dialog_button);

        dialog.setCanceledOnTouchOutside(true);
        submitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                    dialog.cancel();
            }
        });
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = dialog.getWindow();
        lp.copyFrom(window.getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
       // window.setBackgroundDrawableResource(R.color.primary_trans);

        window.setAttributes(lp);
        dialog.show();

    }

    private void privacyApi() {
        try{
            Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://freshersapp.chetaru.co.uk/privacyPolicy"));
            startActivity(browserIntent);
        }catch (Exception e){
            e.printStackTrace();
        }

    }



    private void termsAndCondition() {
        try{
           /* Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://freshersapp.chetaru.co.uk/termsOfServices" +
                    "\n" +
                    "\n" +
                    "\n" +
                    "Terms Of Services\n" +
                    "https://freshersapp.chetaru.co.uk\n"));*/
            Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://freshersapp.chetaru.co.uk/termsOfServices"));
            startActivity(browserIntent);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public void todayLikeImage(String images,int postId) {
       // mListener.settingClick("PremiumUser",myProfile.getPostId());
        mListener.todayLike(viewString,images,postId);
    }

    public interface settingListener{
        public void settingClick(String clickName,String postId);
        public void settingMyUpload(String clickName,boolean postId);
        public void settingMyLike(String clickName,boolean postId);
        public void settingBlockUser(String clickName,boolean postId);
        public void settingPremiumUser(String clickName,String postId);
        public void settingTodayPremium(String clickName,String postId);
        public void todayLike(View view ,String image,int postId);
    }
}