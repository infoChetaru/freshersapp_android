package com.chetaru.FreshersApp.viewModel;

import android.app.Application;
import android.content.Intent;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.chetaru.FreshersApp.R;
import com.chetaru.FreshersApp.service.model.User;

import com.chetaru.FreshersApp.service.model.UserResponse;
import com.chetaru.FreshersApp.service.repository.LoginRepository;
import com.chetaru.FreshersApp.service.retrofit.ApiClient;
import com.chetaru.FreshersApp.service.retrofit.BaseRequest;
import com.chetaru.FreshersApp.service.retrofit.Functions;
import com.chetaru.FreshersApp.service.retrofit.RequestReciever;
import com.chetaru.FreshersApp.utility.Validations;
import com.google.gson.JsonObject;

import org.json.JSONObject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginViewModel extends AndroidViewModel {

   // private final LiveData<User> projectListObservable;
   private LiveData<UserResponse> user;

    private String TAG=this.getClass().getSimpleName();
     String userName,password;
    //For retrofit when calling api
    private final MutableLiveData<Boolean> repoLoadError = new MutableLiveData<>();
    //For retrofit when calling api
    private final MutableLiveData<Boolean> loading = new MutableLiveData<>();

    BaseRequest baseRequest;

    public LiveData<Boolean> getError(){
        return repoLoadError;
    }

    public LiveData<Boolean> getLoading(){
        return loading;
    }

    public LoginViewModel(@NonNull Application application) {
        super(application);
    }


    public LoginViewModel(@NonNull Application application, String  userName, String password,String deviceId,String myToken) {
        super(application);
       new LoginViewModel(application);



    }



    public LiveData<UserResponse> getUser(String email,String password,String deviceId,String myToken) {
        LiveData<UserResponse> reps= new MutableLiveData<>();
        loading.setValue(true);
        reps = LoginRepository.getInstance().fetchUser(email,password,deviceId,myToken);
        repoLoadError.setValue(false);
        loading.setValue(false);
        return reps;
    }




    /* {
  "userName":"shweta@mailinator.com",
  "password":"123",
  "deviceId":"7C4A9817-775F-43EC-BC84-0BDBF3F6A3E7",
  "fcmToken":"fcg7s6u0x9s:APA91bFy57Q6Yyn-fUSUoY1297oxkFqxiFsdvulcBCT5nj5kiuOiCTP-lyB7-XbdYvEQ_op-mybiKKGNnFaqxD1WjB9jbecU6pYDHJV8xGLtDQBc-iqJlejYUr_4OeusRa9BJ6QHo42M",
  "role":2
}*/


}
