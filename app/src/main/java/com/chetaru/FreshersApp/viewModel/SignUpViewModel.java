package com.chetaru.FreshersApp.viewModel;

import android.app.Application;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.chetaru.FreshersApp.service.model.UserRequest;
import com.chetaru.FreshersApp.service.model.UserResponse;
import com.chetaru.FreshersApp.service.repository.Repo.SignUpRepository;
import com.chetaru.FreshersApp.service.repository.Repo.VerifyRepository;
import com.chetaru.FreshersApp.service.retrofit.ApiClient;
import com.chetaru.FreshersApp.utility.Utility;
import com.chetaru.FreshersApp.utility.Validations;
import com.google.gson.JsonObject;

import org.json.JSONObject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SignUpViewModel extends AndroidViewModel {
    private String TAG=this.getClass().getSimpleName();


    private MutableLiveData<UserResponse> repos;
    //For retrofit when calling api
    private final MutableLiveData<Boolean> repoLoadError = new MutableLiveData<>();
    //For retrofit when calling api
    private final MutableLiveData<Boolean> loading = new MutableLiveData<>();
    //our local list of repos
    private Call<UserResponse> repoCall;

     /*
    {
  "name":"shweta",
  "userName":"45343785372",
  "password":"123",
  "dob":"1995-07-22",
  "bio":"test",
  "gender":2,
  "type":2,
  "countryId":"54",
  "profileImage":"",
  "deviceId":"7C4A9817-775F-43EC-BC84-0BDBF3F6A3E7",
  "fcmToken":"fcg7s6u0x9s:APA91bFy57Q6Yyn-fUSUoY1297oxkFqxiFsdvulcBCT5nj5kiuOiCTP-lyB7-XbdYvEQ_op-mybiKKGNnFaqxD1WjB9jbecU6pYDHJV8xGLtDQBc-iqJlejYUr_4OeusRa9BJ6QHo42M"


}
    * */

    String name="";
    String userName="";
    String password="";
    String dob="";
    String bio="";
    Integer gender=0;
    String type="";
    String countryId="";
    String profileImage="";
    String deviceId="";
    String myToken="";
    public SignUpViewModel(@NonNull Application application) {
        super(application);
    }
    public SignUpViewModel(@NonNull Application application, Object[] mParams){
        super(application);
        new SignUpViewModel(application);

    }



    public LiveData<JSONObject> getUserSignUp(String name,String userName, String password,
                                          String dob,String bio,Integer gender,Integer type,
                                          String countryId,String profileImage,String deviceId, String myToken){
        LiveData<JSONObject> UserData=new MutableLiveData<>();
        UserData = SignUpRepository.getInstance().signUpUser(name,userName,password,dob
                ,bio,gender,type,countryId,profileImage,deviceId,myToken);
        return UserData;
    }



    //our cleanup method
    @Override
    protected void onCleared() {
        if(repoCall != null){
            repoCall.cancel();
            repoCall = null;
        }
    }
}
