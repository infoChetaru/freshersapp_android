package com.chetaru.FreshersApp.viewModel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.chetaru.FreshersApp.service.model.UserAddPostResponse;
import com.chetaru.FreshersApp.service.repository.Repo.UploadRepository;

public class UploadViewModel extends AndroidViewModel {

    private LiveData<UserAddPostResponse> postDataRespo;
    public UploadViewModel(@NonNull Application application) {
        super(application);

    }

    public LiveData<UserAddPostResponse> getUserPostResp(String tokenId) {
        postDataRespo= UploadRepository.getInstance().festUserPost(tokenId);
        return postDataRespo;
    }
}
