package com.chetaru.FreshersApp.viewModel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.chetaru.FreshersApp.service.model.User;
import com.chetaru.FreshersApp.service.model.UserResponse;
import com.chetaru.FreshersApp.service.repository.Repo.ForgetRepository;
import com.chetaru.FreshersApp.service.repository.Repo.VerifyRepository;
import com.chetaru.FreshersApp.utility.Validations;

public class ForgetPassModel extends AndroidViewModel {

    private LiveData<User> resp;
    public ForgetPassModel(@NonNull Application application) {
        super(application);
    }
    public ForgetPassModel(@NonNull Application application, String  userName, String password,String deviceId,String myToken) {
        super(application);
        new ForgetPassModel(application);

    }
    public LiveData<UserResponse> getpassword(String userName){
        LiveData<UserResponse> codeData=new MutableLiveData<>();
        codeData= ForgetRepository.getInstance().getMail(userName);
        return codeData;
    }
    public LiveData<UserResponse> getpasswordReset(String userName,String password,String token){
        LiveData<UserResponse> codeData=new MutableLiveData<>();
        codeData= ForgetRepository.getInstance().getPasswordReset(userName,password,token);
        return codeData;
    }

}
