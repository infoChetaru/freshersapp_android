package com.chetaru.FreshersApp.viewModel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.chetaru.FreshersApp.service.model.UserResponse;
import com.chetaru.FreshersApp.service.repository.LoginRepository;
import com.chetaru.FreshersApp.service.repository.Repo.VerifyRepository;
import com.chetaru.FreshersApp.utility.Validations;

import org.json.JSONObject;

public class VerifyViewModel extends AndroidViewModel {

    private LiveData<UserResponse> user;
    private LiveData<CountryResponse> counrtyData;

    private String TAG=this.getClass().getSimpleName();
    String userName,password;

    public VerifyViewModel(@NonNull Application application) {
        super(application);

    }

    public VerifyViewModel(@NonNull Application application, String  userName, String password) {
        super(application);
        new VerifyViewModel(application);
        counrtyData=VerifyRepository.getInstance().fetchCountry();
        /*if (!Validations.isEmptyString(userName))
            user = VerifyRepository.getInstance().fetchVeriCode(userName,password);*/

    }


    public VerifyViewModel(Application mApplication, Object[] mParams) {
        super(mApplication);
        new VerifyViewModel(mApplication);

        try{
            if (((String) mParams[0]).equals("countryApi")) {
                counrtyData=VerifyRepository.getInstance().fetchCountry();
               // user = VerifyRepository.getInstance().fetchVeriConfrim(userName,password);
            }else if (((String) mParams[2]).equals("getCodeApi")) {
                //user=VerifyRepository.getInstance().fetchVeriCode((Integer)mParams[1],(String)mParams[2],(String)mParams[3]);
            }
        }catch (Exception e){
            e.printStackTrace();
        }


    }
public LiveData<JSONObject> getCode(Integer type, String value, String countryId){
        LiveData<JSONObject> codeData=new MutableLiveData<>();
    codeData=VerifyRepository.getInstance().fetchVeriCode(type, value, countryId);
    return codeData;
}
    public LiveData<UserResponse> getVeriCode() {
        return user;
    }

    public LiveData<CountryResponse> getCountryCode() {
        return counrtyData;
    }
}
