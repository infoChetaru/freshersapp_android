package com.chetaru.FreshersApp.viewModel;

import android.util.Log;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.chetaru.FreshersApp.service.model.UserRequest;
import com.chetaru.FreshersApp.service.model.UserResponse;
import com.chetaru.FreshersApp.service.retrofit.ApiClient;
import com.chetaru.FreshersApp.utility.Utility;
import com.chetaru.FreshersApp.utility.Validations;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;

import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.Collection;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class VerificationViewModel extends ViewModel {
    private String TAG=this.getClass().getSimpleName();
    private MutableLiveData<UserResponse> repos  = new MutableLiveData<>();
    private MutableLiveData<CountryResponse> countryRepos;

    //For retrofit when calling api
    private final MutableLiveData<Boolean> repoLoadError = new MutableLiveData<>();
    //For retrofit when calling api
    private final MutableLiveData<Boolean> loading = new MutableLiveData<>();
    //our local list of repos
    private Call<UserResponse> repoCall;
    private Call<CountryResponse> countCall;

    public VerificationViewModel() {

        if (countryRepos == null) {
            countryRepos = new MutableLiveData<>();
        }
        getCountry();
    }

   public LiveData<Boolean> getError(){
        return repoLoadError;
    }

    public LiveData<Boolean> getLoading(){
        return loading;
    }

    public LiveData<CountryResponse> getCountryCode(){
        Log.d("countryRespos",countryRepos.getValue().getMessage().toString());
        return countryRepos;
    }

    private void getCountry() {

        loading.setValue(true);
        countCall = ApiClient.getInstance().getCountry();
        countCall.enqueue(new Callback<CountryResponse>() {
            @Override
            public void onResponse(Call<CountryResponse> call, Response<CountryResponse> response) {
                repoLoadError.setValue(false);
                Log.d("country",response.body().getCountryList().toString());
                Log.d("country",response.toString());
                Log.d("country",response.body().getMessage().toString());
                repoLoadError.setValue(false);
                countryRepos.setValue(response.body());
                loading.setValue(false);
                countCall = null;
            }

            @Override
            public void onFailure(Call<CountryResponse> call, Throwable t) {
                Log.e(getClass().getSimpleName(), "Error loading repos", t);
                repoLoadError.setValue(true);
                loading.setValue(false);
                countCall = null;
            }
        });
    }
     public LiveData<UserResponse>getVeriStatus(){
        return repos;
     }
    public LiveData<UserResponse> getVeri(Integer type, String value,String countryId) {
        if (repos==null) {
            fetchVeri(type, value, countryId);
        }
        Log.d("countryRespos",countryRepos.getValue().getMessage().toString());
        return repos;
    }

    public void fetchVeri(Integer type,String value,String countryId) {
        loading.setValue(true);
        /*
        {
  "type":2, //1 is for email,2 for phone no.
  "value":"9806816293",
  "countryId":"99" //Country Id like india have 99 as country id,It gives 91 as phone code of country.
}
         */
        UserRequest object=new UserRequest();
        object.setType(type);
        object.setValue(value);
        if (!Validations.isEmptyString(countryId)) {
            object.setCountryId(countryId);
        }
        Call<ResponseBody> repoCall = ApiClient.getInstance().getVerifyUser(object);
        repoCall.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    JSONObject json = new JSONObject(response.body().string());

                    String data=response.body().toString();

                    Gson gson = new Gson(); // Or use new GsonBuilder().create();
                    String jsonData = gson.toJson(json); // serializes target to Json
                    UserResponse responseData = gson.fromJson(jsonData, UserResponse.class);
                    repoLoadError.setValue(false);
                    repos.setValue(responseData);
                    loading.setValue(false);
                    //repoCall = null;
                }catch (Exception e){
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.e(getClass().getSimpleName(), "Error loading repos", t);
                repoLoadError.setValue(true);
                loading.setValue(false);
                //repoCall = null;
            }
        });
    }

    //our cleanup method
    @Override
    protected void onCleared() {
        if(repoCall != null){
            repoCall.cancel();
            repoCall = null;
        }
    }
}
